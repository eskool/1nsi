import re
import markdown
import subprocess
import base64

fichier = "monfichier.md"

# INITIAL 
# BLOCK_RE_GRAVE_ACCENT = re.compile(
#         r'^```graphviz[ ]* (?P<command>\w+)\s+(?P<filename>[^\s]+)\s*\n(?P<content>.*?)```\s*$',
#     re.MULTILINE | re.DOTALL)

# MOI 1
BLOCK_RE_GRAVE_ACCENT = re.compile(
        r'^[\s]*```graphviz[\s]+(?P<command>\w+)\s+(?P<filename>[^\s]+)\s*\n(?P<content>.*?)```\n$',
    re.MULTILINE | re.DOTALL)

# BLOCK_RE_GRAVE_ACCENT = re.compile(
#         r'^[ ]*```graphviz[ ]+(?P<content>.*?)```\s*$',
#     re.MULTILINE | re.DOTALL)


# BLOCK_RE_GRAVE_ACCENT_DOT = re.compile(
#         r'^[ 	]*```dot\n(?P<content>.*?)```\s*$',
#     re.MULTILINE | re.DOTALL)

def get_md(filename=fichier)->str:
  with open(filename, "r") as f:
    text = f.read()
  return text

text = get_md(fichier)
# mgraph = BLOCK_RE_GRAVE_ACCENT.search(text)
mgraph = BLOCK_RE_GRAVE_ACCENT.findall(text)
# match_graph = BLOCK_RE_GRAVE_ACCENT.match("digraph")
# mgraph_content = mgraph.group("content")

# mdot = BLOCK_RE_GRAVE_ACCENT_DOT.search(text)
# mdot = BLOCK_RE_GRAVE_ACCENT_DOT.findall(text)
# match_dot = BLOCK_RE_GRAVE_ACCENT_DOT.match("digraph")
# mdot_content = mdot.group("content")

print("type(text)=",type(text))

# print("graphviz[0]", ord(mgraph_content[0]))
# print("graphviz[-1]", ord(mgraph_content[-1]))
print("GRAPHVIZ SEARCH TEXT", mgraph)
print("NB GRAPHVIZ DETECTÉS=", len(mgraph))
# print("mgraph_content =", mgraph_content)
# print("match_graph =", match_graph)

# print("dot[0]", ord(mdot_content[0]))
# print("dot[-1]", ord(mdot_content[-1]))

# print("DOT SEARCH TEXT", mdot)
# print("NB DOT DETECTÉS=", len(mdot))

# print("mdot_content =", mdot_content)
# print("match_dot =", match_dot)

