# 1NSI : <rbl>Corrigé</rbl> des Exercices Correction des algorithmes

!!! ex
    On considère l'**algorithme de multiplication d'un entier $a$ par un entier strictement positif $b$** suivant :

    ```python
    def multiplie(a:int,b:int)->int:
        x = a
        y = b
        z = 0
        while y !=0:
            z = z + x
            y = y - 1
        return z
    ```

    1. Déterminer un *invariant de boucle*, pour la boucle `while`

        ??? corr "Réponse"
            La propriété "En début de chaque itération, $z=a(b-y)$" est un **invariant de boucle**, car:

            * **Initialisation** : Avant la 1ère itération, on a $z=0$ et $a(b-y)=a\times 0=0$, donc $z=(b-y)$
            * **Hérédité** : Si $z=a(b-y)$ en entrant dans la $i$-ème itération, Alors pour la $(i+1)$-ème itération (càd la suivante), la nouvelle valeur $z'$ (de $z$) vaut $z'=z+a$, et la nouvelle valeur $y'$ (de $y$) vaut $y'=y-1$, donc la nouvelle valeur de $a(b-y)$ vaut $a(b-y')=a(b-(y-1))=a(b-y+1)=a(b-y)+a=z+a=z'$. En sortant de la $i$-ème itération, càd en entrant dans la $(i+1)$-ème itération, on a donc encore $z'=a(b-y')$

    2. En déduire/Démontrer que l'algorithme est *Correct* (càd que l'entier $z$ renvoyé est bien le produit de $a$ par $b$)

        ??? corr "Réponse"
            * **Conclusion** : En particulier, cet invariant de boucle reste vrai en sortant de la dernière itération, càd lorsque $y=0$. En sortant de la boucle donc, on a: $z=a(b-0)=ab$, donc la fonction renvoie bien le produit de $a$ par $b$.

!!! ex
    On considère l'**algorithme de Tri par Sélection** suivant :

    ```python
    def tri_selection(A:list)->list:
        n=len(A)
        for i in range(n-1):
            mini = i
            for j in range(i+1,n):
                if A[j]<A[mini]:
                    mini = j
            A[i], A[mini] = A[mini], A[i]
        return A
    ```

    1. Déterminer un *invariant de boucle*, pour la première boucle `for`

        ??? corr "Réponse"
            « la sous-liste (de longueur ) des $k$ premières valeurs est triée dans l'ordre croissant.» est un invariant de boucle.

    2. En déduire/Démontrer que l'algorithme est *Correct* (càd que la liste renvoyée est bien triée)

        ??? corr "Réponse"
            En sortant de la dernière itération (de la boucle `for` la plus externe), l'invariant de boucle reste vrai, donc en particulier: « la sous-liste (de longueur $n$) des $n$ premières valeurs est triée dans l'ordre croissant, càd que la totalité de la liste est triée dans l'ordre croissant»
