from timeit import timeit
# import time

def tri_insertion(L:list):
    l = list(L)
    '''tri_insertione en place la liste l donnée en paramètre'''
    for i in range(1, len(l)):           
        cle = l[i]                       
        k = i - 1                        
        while k >= 0 and l[k] > cle :    
            l[k + 1] = l[k]              
            k = k -1                     
        l[k + 1] = cle                   


la = [k for k in range(100,0,-1)]
lb = [k for k in range(200,0,-1)]

ta = timeit("tri_insertion(la)", globals=globals(), number=100)
tb = timeit("tri_insertion(lb)", globals=globals(), number=100)
print("ta=",ta)
print("tb=",tb)

# AVEC LE MODULE TIME

# start = time.time()
# tri_insertion(la)
# end = time.time()
# print("ta=",end-start)

# start = time.time()
# tri_insertion(lb)
# end = time.time()
# print("tb=",end-start)
