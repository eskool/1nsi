from matplotlib import pyplot as plt
import time
from random import randint

def tri_insertion(l:list):
    """trie en place la liste l donnée en paramètre
    et en plus ca fait çà
    """
    for i in range(1, len(l)):
        cle = l[i]
        k = i - 1
        while k >= 0 and l[k] > cle:
            l[k + 1] = l[k]
            k = k -1
        l[k + 1] = cle

def liste_alea(n:int,a:int,b:int)->list:
    return [randint(a,b) for i in range(n)]

def creer_toutes_listes(a:int,b:int)->list:
    l = []
    for i in range(10,210,10):
        l.append(liste_alea(i, a, b))
    return l

def calcul_tous_x(listes):
    x = []
    for l in listes:
        x.append(len(l))
    return x

def calcul_tous_y(listes):
    y = []
    for l in listes:
        start = time.time()
        tri_insertion(l)
        end = time.time()
        temps = end-start
        y.append(temps)
    return y

listes = creer_toutes_listes(20, 50)
x = calcul_tous_x(listes)
y = calcul_tous_y(listes)

plt.suptitle("Complexité du Tri par insertion")
plt.grid(True)
plt.xlabel("n = taille de la liste à trier")
plt.ylabel("C(n) = Complexité en Temps pour trier une liste de taille n")
plt.plot(x,y)

plt.show()