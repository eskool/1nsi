# import time
from timeit import timeit

def tri_selection(L:list) :
    l = list(L)
    for k in range(len(l)-1):
        indice_min = k
        for i in range(k+1, len(l)) :
            if l[i] < l[indice_min]:
                indice_min = i
        l[k], l[indice_min] = l[indice_min], l[k]

la = [k for k in range(100,0,-1)]
lb = [k for k in range(200,0,-1)]

ta = timeit("tri_selection(la)", globals=globals(), number=1000)
tb = timeit("tri_selection(lb)", globals=globals(), number=1000)
print("ta=",ta)
print("tb=",tb)


# AVEC LE MODULE TIME

# start = time.time()
# tri_selection(la)
# end = time.time()
# print("ta=",end-start)

# start = time.time()
# tri_selection(lb)
# end = time.time()
# print("tb=",end-start)
