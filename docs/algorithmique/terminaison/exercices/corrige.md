# 1NSI : <rbl>Corrigé</rbl> des Exercices Terminaison des algorithmes

!!! ex
    On considère l'algorithme suivant :

    ```pseudocode
    i prend la valeur 1000
    Tant que i > 0 :
        i prend la valeur i-1
    afficher i
    ```

    1. Déterminer un variant de boucle pour la boucle `Tantque`

        ??? corr "Réponse"
            `v=i` est un variant de boucle, car :

            * $v$ est un entier
            * A l'entrée de la boucle, $v=1000>0$
            * A chaque itération, $v=i$ décroît strictement de $1$ (une unité), car $i$ décroît strictement de $1$
            * $v\le 0 \Leftrightarrow i \le 0$ provoque bien la sortie de la boucle 

    2. En déduire que l'algorithme *Termine*

        ??? corr "Réponse"
            Une boucle possédant un variant de boucle termine...

!!! ex
    On considère l'algorithme suivant, qui détermine le premier entier $n$ tel que $3n+5>100$

    ```pseudocode
    i prend la valeur 0
    Tant que 3*i+5 <= 100 :
        i prend la valeur i+1
    afficher(i)
    ```

    1. Déterminer un variant de boucle pour la boucle `Tantque`

        ??? corr "Réponse"
            Remarquons que, pour tout nombre entier $n$ :  
            $3i+5 \le 100 \Leftrightarrow 100-5 -3i \ge 0 \Leftrightarrow 95 -3i \ge 0 \Leftrightarrow 95 -3i +1\ge 1 \Leftrightarrow 96 -3i\gt 0$
            Ainsi `v=96-3i` est un **variant de boucle**, car :

            * $v$ est un entier
            * A l'entrée de la boucle, $v=96-3\times 0=96>0$
            * A chaque itération, $v=96-3i$ décroît strictement (de $3$ unités), car $i$ croît strictement de $1$ unités
            * $v\le 0 \Leftrightarrow 3i+5>100$ provoque bien la sortie de la boucle 

    2. En déduire que l'algorithme *Termine*

        ??? corr "Réponse"
            Une boucle possédant un variant de boucle termine...

!!! ex
    ```pseudocode
    i prend la valeur 100
    m prend la valeur "bonjour"
    Tant que i > 5 ET longueur(m) > 2 :
        i prend la valeur i-1
    afficher(i)
    ```

    1. Déterminer un variant de boucle pour la boucle `Tantque`

        ??? corr "Réponse"
            Avant tout, Commençons par remarquer que la variable `m` n'est pas modifiée (la condition `longueur(m)>2` sera toujours vraie) à l'intérieur de la boucle, donc un variant de boucle `v` ne dépend PAS de la variable `m`. Un variant de boucle `v` ne dépendra donc que de la variable `i`.  
            De plus, pour tout nombre entier $n$ :  
            $i \gt 5 \Leftrightarrow i - 5 \gt 0$
            Ainsi `v = i-5` est un **variant de boucle**, car :

            * $v$ est un entier
            * A l'entrée de la boucle, $v=i-5>0$
            * A chaque itération, $v = i-5$ décroît strictement (de $1$ unités), car $i$ croît strictement de $1$ unités
            * $v\le 0 \Leftrightarrow i-5 \le 0 \Leftrightarrow i \le 5$ provoque bien la sortie de la boucle 

    2. En déduire que l'algorithme *Termine*

        ??? corr "Réponse"
            Une boucle possédant un variant de boucle termine...

!!! ex
    On considère l'**algorithme de multiplication d'un entier $a$ par un entier strictement positif $b$** suivant :

    ```python
    def multiplie(a:int,b:int)->int:
        x = a
        y = b
        z = 0
        while y !=0:
            z = z + x
            y = y - 1
        return z
    ```

    1. Déterminer un *variant de boucle* pour la boucle `while`

        ??? corr "Réponse"
            Nous supposerons que $b$ est (bien) un entier strictement positif.
            Remarquons que $y\ne 0 \Leftrightarrow y\gt 0$
            Ainsi `v=y` est un **variant de boucle**, car :

            * $v$ est un entier
            * A l'entrée de la boucle, $v=y=b>0$ par définition
            * A chaque itération, $v=y$ décroît strictement de $1$ unité
            * $v\le 0 \Leftrightarrow y\le 0 \Leftrightarrow y= 0$, car initialement, $y$ est un entier $>0$, et décroît d'un cran à chaque itération, donc, $v\le 0$ provoque bien la sortie de la boucle 

    2. En déduire/Démontrer que l'algorithme *Termine*

        ??? corr "Réponse"
            Une boucle possédant un variant de boucle termine...

    3. Déterminer un *invariant de boucle*, pour la boucle `while`

        ??? corr "Réponse"
            La propriété "En début de chaque itération, $z=a(b-y)$" est un **invariant de boucle**, car:

            * **Initialisation** : Avant la 1ère itération, on a $z=0$ et $a(b-y)=a\times 0=0$, donc $z=(b-y)$
            * **Hérédité** : Si $z=a(b-y)$ en entrant dans la $i$-ème itération, Alors pour la $(i+1)$-ème itération (càd la suivante), la nouvelle valeur $z'$ (de $z$) vaut $z'=z+a$, et la nouvelle valeur $y'$ (de $y$) vaut $y'=y-1$, donc la nouvelle valeur de $a(b-y)$ vaut $a(b-y')=a(b-(y-1))=a(b-y+1)=a(b-y)+a=z+a=z'$. En sortant de la $i$-ème itération, càd en entrant dans la $(i+1)$-ème itération, on a donc encore $z'=a(b-y')$

    4. En déduire/Démontrer que l'algorithme est *Correct* (càd que l'entier $z$ renvoyé est bien le produit de $a$ par $b$)

        ??? corr "Réponse"
            * **Conclusion** : En particulier, cet invariant de boucle reste vrai en sortant de la dernière itération, càd lorsque $y=0$. En sortant de la boucle donc, on a: $z=a(b-0)=ab$, donc la fonction renvoie bien le produit de $a$ par $b$.

!!! ex
    On considère l'**algorithme de Tri par Sélection** suivant :

    ```python
    def tri_selection(A:list)->list:
        n=len(A)
        for i in range(n-1):
            mini = i
            for j in range(i+1,n):
                if A[j]<A[mini]:
                    mini = j
            A[i], A[mini] = A[mini], A[i]
        return A
    ```

    1. Déterminer un *variant de boucle* pour la (première) boucle `for`

        ??? corr "Réponse"
            Nous supposerons que la liste contient au moins deux valeurs, càd que $n\ge 2$.  
            Dans ce cas, pour tout nombre entier $0\le i\lt n-1 (\Leftrightarrow$ `i in range(n-1)`), l'entier $v=n-1-i$ est un **variant de boucle**, car :

            * $v$ est un entier
            * A l'entrée de la boucle, $v=n-1-0=n-1>0$ donc $n\gt1$ càd $n\ge 2$, car on suppose que la liste contient au moins deux valeurs
            * A chaque itération, $v=n-1-i$ décroît strictement d' $1$ unité, car $i$ croît strictement d' $1$ unité
            * $v\le 0 \Leftrightarrow n-1-i\le 0 \Leftrightarrow i\ge n-1$ provoque bien la sortie de la boucle

    2. En déduire/Démontrer que l'algorithme *Termine*

        ??? corr "Réponse"
            Une boucle possédant un variant de boucle termine...

    3. Déterminer un *invariant de boucle*, pour la première boucle `for`

        ??? corr "Réponse"
            « la sous-liste (de longueur ) des $k$ premières valeurs est triée dans l'ordre croissant.» est un invariant de boucle.

    4. En déduire/Démontrer que l'algorithme est *Correct* (càd que la liste renvoyée est bien triée)

        ??? corr "Réponse"
            En sortant de la dernière itération (de la boucle `for` la plus externe), l'invariant de boucle reste vrai, donc en particulier: « la sous-liste (de longueur $n$) des $n$ premières valeurs est triée dans l'ordre croissant, càd que la totalité de la liste est triée dans l'ordre croissant»
