from random import randint
from timeit import timeit
import time

def getTab(n,mini,maxi):
  tableau=[]
  for i in range(n):
    valeur=randint(mini,maxi)
    tableau.append(valeur)
  return tableau

def parcours_sequentiel(C, L) :
    for k in range(len(L)) :
        if L[k] == C:
            return k
    return "non trouvé"

def recherche_dicho(C,L):
  n = len(L)
  gauche = 0
  droite = n-1
  while gauche <= droite:
    milieu = (gauche + droite)//2
    element_central = L[milieu]
    if C > L[milieu]:
      gauche = milieu + 1
    elif C < L[milieu]:
      droite = milieu - 1
    else: # trouvé!
      return milieu
  return None

# C=14
# L = [2, 3, 6, 7, 11, 14, 18, 19, 24]

# C = 299474
# f = open("input_centmille.txt",'r')
C = 2999306
f = open("input_million.txt",'r')

l = f.readlines()
L = []
for k in l :
    L.append(int(k[:-1]))

# L=getTab(10,1,10)
# L.sort()

# AVEC TIMEIT
# POUR 100 000 VALEURS
# C = 299474
# centmille_sequentiel = timeit("parcours_sequentiel(C, L)", globals=globals(), number=1000)  # number = 1000000,
# print("Temps Parcours Séquentiel = ", centmille_sequentiel)
# centmille = timeit("recherche_dicho(C, L)", globals=globals(), number=1000)  # number = 1000000,
# print("Temps Algo par Dichotomie = ", centmille)

# POUR 1 000 000 VALEURS
# C = 2999306
million_sequentiel = timeit("parcours_sequentiel(C, L)", globals=globals(), number=100)  # par défaut : number = 1000000,
print("Temps Parcours Séquentiel = ", million_sequentiel)
million = timeit("recherche_dicho(C, L)", globals=globals(), number=100)  # number = 1000000,
print("Temps Algo par Dichotomie = ", million)


# AVEC LE MODULE TIME
# start = time.time()
# trouve=recherche_dicho(C,L)
# end = time.time()
# print("Temps écoulé = ", end-start)

# if trouve != None:
#   print("Trouvé! en i=",trouve)
# else:
#   print("NON trouvé :(")