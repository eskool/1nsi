# 1NSI: Algorithmique - Recherche par Dichotomie

<center>

| Contenus | Capacités Attendues | Commentaires |
|:-:|:-:|:-:|
| Recherche dichotomique<br/>dans un tableau trié | Montrer la terminaison de la<br/>recherche dichotomique à l’aide<br/>d’un variant de boucle. | Des assertions peuvent être<br/>utilisées.<br/>La preuve de la correction peut<br/>être présentée par le professeur. |

</center>

## Introduction

### Problématique

Comment rechercher **efficacement** dans un Tableau (une liste en Python) **préalablement TRIÉ** ?  

### Rappel : Parcours Séquentiel d'un Tableau (NON trié)

Rappelons que lorsqu'un Tableau n'est pas trié, nous disposons déjà de l'algorithme de Parcours Séquentiel d'un Tableau (non trié). 

<env>Complexité / Coût</env>  
L'entier $n$ désignant la taille du tableau, càd le nombre d'éléments qu'il contient.
La complexité/coût de cet algorithme était linéaire (en $n$), càd en $O(n)$.

La méthode que nous allons utiliser implique que les valeurs aient été triées auparavant.

## Un Exemple : le Jeu du Nombre Mystère

### Le Jeu

On souhaite jouer au jeu suivant, dit "**du nombre Mystère**" : si je choisis un nombre entre $1$ et $100$, quelle est la stratégie optimale pour deviner ce nombre le plus vite possible ? (à chaque étape, une indication -trop grand, trop petit- permet d'affiner la proposition suivante)

???- "Réponse attendue"
    Intuitivement, la meilleure stratégie est de *couper en deux* à chaque fois l'intervalle d'étude. On démarre de 50, puis 75 ou 25, etc.  
    Il convient toutefois de remettre en question cette méthode qui paraît *naturellement* optimale : si je propose 90 comme nombre de départ, j'ai certes moins de chance que le nombre soit entre 90 et 100, mais s'il l'est, j'ai gagné un gros avantage car mon nouvel intervalle est très réduit.

On peut alors rappeler la notion d'**espérance probabiliste**.

!!! exp
    On lance un dé, s'il tombe sur le 6 vous recevez 2 euros, sinon vous me donnez 1 euro. Voulez-vous jouer ?

### Quelques Statistiques de Jeu

Le graphique ci-dessous représente le nombre de coups moyens (sur 10 000 parties simulées)

![image](./img/fig1.png)

<env>Interprétations et remarques</env>  

* si le choix se porte *toujours* sur le nombre situé à la moitié de l'intervalle (0.5), le nombre de coups moyen avant la victoire (sur 10 000 parties) est environ 6.
* si le choix se porte *toujours* sur le nombre situé à 90 % de l'intervalle (0.9), le nombre de coups moyen avant la victoire (sur 10 000 parties) est environ 11.
* l'asymétrie de la courbe (qui devrait être symétrique) est due aux arrondis par défaut dans le cas de nombres non entiers.

### Conclusion

La stratégie optimale est donc bien de diviser en deux à chaque étape l'intervalle d'étude. On appelle cela une méthode par **dichotomie**, du grec ancien διχοτομία, dikhotomia (« division en deux parties »).

La méthode de dichotomie fait partie des méthodes dites Diviser Pour Régner ou quelquefois Diviser pour mieux Régner.

En algorithmique [^1], les méthodes **diviser pour régner** (du latin « Divide ut imperes », divide and conquer en anglais) sont des techniques qui se décomposent usuellement en $3$ étapes :

1. <env>Diviser</env> càd découper un problème initial en sous-problèmes
2. <env>Régner</env> càd résoudre les sous-problèmes (récursivement ou directement s'ils sont assez petits)
3. <env>Combiner</env> càd calculer une solution au problème initial à partir des solutions des sous-problèmes.

## Généralisation à un Tableau Trié

### Animation

<link rel="stylesheet" type="text/css" href="./animation/boxes.css" />
<script src="./animation/jquery-1.7.2.min.js"></script>
<script src="./animation/jquery-ui.min.js"></script>
<script src="./animation/jquery.ui.touch-punch.min.js"></script>
<script src="./animation/jquery.alerts.js"></script> 
<link href="./animation/jquery.alerts.css" rel="stylesheet" type="text/css" media="screen" />

<script type="text/javascript" src="./animation/GetElementPosition.js"></script>
<link rel="stylesheet" type="text/css" href="./animation/codecolor.css" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="./animation/style.css" />
<script src="./animation/script.js"></script>

<div id="animation" onresize="" style="position:relative;" >
    <div style="height: 100px!important; font-size: 200%"></div>
    <!-- <div id ="lowValue" style="position: absolute; font-size: 70%;">gauche: 0</div> -->
    <div id ="lowValue" style="position: absolute; font-size: 100%;">gauche: 0</div>
    <div id ="lowPosition" style="position: absolute; font-size: 200%">&#8595;</div>
    <!-- <div id ="midValue" style="position: absolute; font-size: 70%;">milieu: 1</div> -->
    <div id ="midValue" style="position: absolute; font-size: 100%;">milieu: 1</div>
    <div id ="midPosition" style="position: absolute; font-size: 200%">&#8595;</div>
    <!-- <div id ="highValue" style="position: absolute; font-size: 70%;">droite: 1</div> -->
    <div id ="highValue" style="position: absolute; font-size: 100%;">droite: 1</div>
    <div id ="highPosition" style="position: absolute; font-size: 200%">&#8595;</div>
    <div style="display: table; overflow: hidden; width: 90%; margin: 0 auto;">
      <script>
        SIZE = 16;
        for (var i = 0; i < SIZE; i++) { // pour affichier le tableau de cellules
            document.writeln('<div style="display: table-cell; vertical-align: middle;' +
                    'border: 1px var(--md-default-fg-color) solid;' +
                    'width: 30px!important; height: 30px!important; min-width: 30px!important; max-width: 30px!important; text-align: center;">' +
                    '<div id="list' + i + '" style="color: #37826c; weight: bold">45</div>' +
                    '</div>');
        }
      </script>
    </div>
    <div style="display: table; overflow: hidden; width: 90%; margin: 0 auto;">
      <script>
        SIZE = 16;
        for (var i = 0; i < SIZE; i++) { // pour les croix de vérification
            document.writeln('<div style="display: table-cell; vertical-align: middle;' +
                    'width: 30px!important; height: 10px!important; min-width: 30px!important; max-width: 30px!important; text-align: center;">' +
                    '<div id="check' + i + '" style="color: #EB0D1B; weight: bold"></div>' +
                    '</div>');
        }
      </script>
    </div>
    <div style="display: table-cell; vertical-align: middle; width: 30px!important; height: 45px!important; min-width: 30px!important; max-width: 30px!important; text-align: center;">
      <div id="highlight" style="display: table-cell; vertical-align: middle; width: 30px!important; height: 45px!important; min-width: 30px!important; max-width: 30px!important; text-align: center;"></div>
    </div>
    <div style="text-align: center; margin-top: 1em">
      <span id = "remark" style = "background-color: chocolate; color: white; alignment-adjust: central; text-align: center; max-width: 800px; margin-left: auto; margin-right: auto">
        Une liste est remplie avec des entiers aléatoires.
      </span>
    </div>
    <div align="center" style="font-size:1.2em;">
      Valeur Cible: <input type="text" size="5" value="24" id="value" style="font-size: 1.2em; text-align: right"></input>
      <button type="button" class="button" onclick="step()">Étape Suivante</button>
      <button type="button" class="button" onclick="reset()">Reset</button></div>
    <div style="text-align: center; margin-top: 1em">
      <span id = "status" style = "background-color: chocolate; color: white; alignment-adjust: central; text-align: center; max-width: 800px; margin-left: auto; margin-right: auto">
      </span>
    </div>
    <div style="text-align: center; margin-top: 1em">
    Inspiré par <a href="https://yongdanielliang.github.io/animation/web/BinarySearchNew.html"><em>Binary Search</em>, by Yong Daniel Liang, Georgia Southern University</a>
    </div>
</div>
<script>
    init();
</script>

<envpink>Comment utiliser l'Animation ci-dessus ?</envpink>

* Modifiez (ou pas) la **Valeur Cible** (à rechercher dans la liste aléatoire)
* Cliquez sur le bouton <env>Étape Suivante</env> pour réaliser la comparaison suivante
* Répétez l'<env>Étape Suivante</env> autant de fois que nécéssaire, jusqu'à atteindre la valeur cible (ou pas, si elle n'est pas dans la liste)
* Cliquez sur le bouton <env>Reset</env> pour réinitialiser les entiers aléatoires. Vous pouvez en profiter pour modifier une nouvelle Valeur Cible.

### Principe de la Recherche Dichotomique

Le Principe de la Recherche par Dichotomie dans un Tableau Trié :fr: / **Algorithme de Recherche Dichotomique** (dans un Tableau trié) / **Algorithme par Dichotomie** (dans un Tableau trié), ou Binary Search (in ordered Table) :gb: est la généralisation de la méthode précédente du nombre mystère. Mais Comment la réaliser en pratique ?  

Considérons par exemple la liste L suivante : ![image](./img/fig0.png)

```python
L = [2, 3, 6, 7, 11, 14, 18, 19, 24]
```

L'objectif est de définir un algorithme de recherche **efficace** d'une valeur cible (arbitrairement choisie à l'avance) présente dans cette liste.

#### Détaillons un exemple spécifique

Par exemple, Recherchons par Dichotomie la valeur $14$ dans notre liste triée `L`.

![image](./img/fig3.png){.center}

* <env>Étape $1$</env> Toute la liste est initialement à traiter. Dans le cas (comme initialement ici) d'un **nombre impair** ($9$) d'éléments restant à traiter (correspondant à des indices compris entre $0$ et $8$ inclus), **il existe un élément central** qui *tombe juste* (ici $11$), qui sépare la partie strictement à sa gauche (`[2,3,6,7]`) et la partie strictement à sa droite (`[14,18,18,24]`) en deux listes exactement de même taille. Ici, l'indice ($milieu=4$) de l'élément central ($11$). Mais comment a-t-on calculé cet indice $milieu=4$ ? On utilise :
    * La formule mathématique :  
    <center><enc>$milieu = \lfloor \dfrac{gauche+droite}{2} \rfloor$</enc></center>
    * ce qui s'écrit en Python :
    <center>
    ```python
    milieu = (gauche + droite) // 2
    ```
    </center>
    en convenant de noter :  
        * `milieu` l'indice *central* du prochain élément/valeur à tester (au "*milieu*" de *gauche* et *droite*)
        * `gauche` désigne l'**indice le plus à gauche / extrémité gauche** des éléments restants (à traiter) de la liste
        * `droite` désigne l'**indice le plus à droite / extrémité droite** des éléments restants (à traiter) de la liste
        * $\lfloor x \rfloor$ désigne la **partie entière** du flottant/réel $x$  
        (par exemple : $\lfloor 4,8 \rfloor=4$; $\lfloor 2,35 \rfloor=2$; $\lfloor 3 \rfloor=3$; $\lfloor -1,3 \rfloor=-2$)
    * L'indice $milieu=4$ qui correspond au prochain élément/valeur à tester, ($11$) de la liste, se calcule donc par la formule :  
    $milieu = \lfloor \dfrac{0+8}{2} \rfloor = \lfloor 4 \rfloor = 4 \Rightarrow$ le prochain est l'élément d'indice $4$ dans la liste
* <env>Étape $2$</env> On compare $11$, d'indice $4$, à la valeur cible recherchée ($14$) : or $14 \gt 11$, donc il faut garder tout ce qui est (strictement) supérieur à $11$. On pose donc `gauche=5` et `droite=8`. Il reste donc un nombre pair ($4$) éléments à tester.
* <env>Étape $3$</env> Dans le cas d'un **nombre pair** (ici, $4$) d'éléments restants à tester, il n'y a **pas réellement d'élément central** (cela *ne tombe pas juste*). Néanmoins, la **même méthode** peut s'appliquer pour calculer l'indice (`milieu`) de l'élément/valeur suivant à tester :  
$milieu = \lfloor \dfrac{gauche+droite}{2} \rfloor = \lfloor \dfrac{5+8}{2} \rfloor = \lfloor \dfrac{13}{2} \rfloor = \lfloor 6,5 \rfloor = 6 \Rightarrow$ Le prochain élément/valeur à tester correspond à l'indice $6$ : c'est le nombre $18$
* <env>Étape $4$</env> On compare la valeur $18$, d'indice $6$, à la valeur cible recherchée ($14$). Or $18>14$ donc on garde ce qui est à gauche (en se souvenant tout de même que $14>11$). On pose donc `gauche=5` et `droite=5` : Il n'y a plus qu'une seule valeur à tester : c'est le nombre $14$ qui se trouve à l'indice $5$
* <env>Étape $5$</env> On se place sur la valeur $14$ (car $milieu = \lfloor \dfrac {5+5}{2} \rfloor = \lfloor \dfrac {10}{2} \rfloor = \lfloor 5 \rfloor=5$) et on compare avec $14$. La valeur cible ($14$) a bien été trouvée!

#### Résumé

Résumé d'un Algorithme de Recherche par Dichotomie :

* on se place *au milieu* de la liste.
* on regarde si la valeur cible recherchée est inférieure ou supérieure à la valeur du milieu.
* on ne garde que la bonne moitié de la liste **restante** qui nous intéresse, et 
* on recommence jusqu'à trouver la valeur cible. ou pas (si elle n'est pas dans la liste..)

### L'Algorithme de Recherche Dichotomique en Python

Contre-intuitivement, Bien que les idées derrière l'Algorithme de Dichotomie semblent simples et directes par rapport à d'autres, les détails de son implémentation pratique peuvent étonnamment poser des problèmes d'une difficulté inattendue (Ce n'est pas moi qui le dit, c'est quelqu'un d'intelligent..)

> Although the basic idea of binary search is comparatively straightforward, the details can be surprisingly tricky
>
> Donald Knuth, 1998, Searching an ordered Table

Nous allons travailler avec deux variables `gauche` et `droite` qui vont délimiter les indices de la liste à étudier. Ces indices sont représentés en bleu sur la figure ci-dessous. La valeur du `milieu` (représenté en rouge) sera égale à `(gauche + droite) // 2`

![image](./img/fig4.png){.center}

Le programme s'arrête lorsque la valeur cherchée a été trouvée, ou lorsque `droite` devient strictement inférieur à `gauche`.

```python linenums="1"
def recherche_dico(C, L) :
    """Renvoie l'indice de la valeur cible C si elle appartient au Tableau L,
    sinon, renvoie 'None' (si la valeur cible C n'a pas été trouvée)
    """
    gauche = ...
    droite = ...
    while gauche <= droite :
        milieu = (... + ...) // 2     # on calcule l'indice central
        element_central = L[...]      # on prend la valeur de l'élément central
        if ... > element_central :    # si l'élément central est trop petit...
            gauche = milieu + 1
        elif ... < element_central :  # si l'élément central est trop grand...
            droite = milieu - 1
        else: # l'élément central `milieu` est la valeur cible C cherchée
            return milieu
    return None
```

```python
L = [2, 3, 6, 7, 11, 14, 18, 19, 24]
print(recherche_dico(L,14))
print(recherche_dico(L,2))
print(recherche_dico(L,24))
print(recherche_dico(L,1976))
```

```console
5
0
8
None
```

<iframe width="100%" height="650" frameborder="0" src="https://pythontutor.com/iframe-embed.html#code=L%20%3D%20%5B2,%203,%206,%207,%2011,%2014,%2018,%2019,%2024%5D%0A%0Adef%20recherche_dicho%28C,L%29%3A%0A%20%20n%20%3D%20len%28L%29%0A%20%20gauche%20%3D%200%0A%20%20droite%20%3D%20n-1%0A%20%20while%20gauche%20%3C%3D%20droite%3A%0A%20%20%20%20milieu%20%3D%20%28gauche%20%2B%20droite%29//2%0A%20%20%20%20element_central%20%3D%20L%5Bmilieu%5D%0A%20%20%20%20if%20C%20%3E%20L%5Bmilieu%5D%3A%0A%20%20%20%20%20%20gauche%20%3D%20milieu%20%2B%201%0A%20%20%20%20elif%20C%20%3C%20L%5Bmilieu%5D%3A%0A%20%20%20%20%20%20droite%20%3D%20milieu%20-%201%0A%20%20%20%20else%3A%20%23%20trouv%C3%A9!%0A%20%20%20%20%20%20return%20milieu%0A%20%20return%20None%0A%20%20%0Aprint%28recherche_dicho%2814,L%29%29&codeDivHeight=400&codeDivWidth=350&cumulative=false&curInstr=0&heapPrimitives=nevernest&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false"> </iframe>

En cas de problème de visualisation, cf l'animation sur [cette page](https://pythontutor.com/iframe-embed.html#code=L%20%3D%20%5B2,%203,%206,%207,%2011,%2014,%2018,%2019,%2024%5D%0A%0Adef%20recherche_dicho%28C,L%29%3A%0A%20%20n%20%3D%20len%28L%29%0A%20%20gauche%20%3D%200%0A%20%20droite%20%3D%20n-1%0A%20%20while%20gauche%20%3C%3D%20droite%3A%0A%20%20%20%20milieu%20%3D%20%28gauche%20%2B%20droite%29//2%0A%20%20%20%20element_central%20%3D%20L%5Bmilieu%5D%0A%20%20%20%20if%20C%20%3E%20L%5Bmilieu%5D%3A%0A%20%20%20%20%20%20gauche%20%3D%20milieu%20%2B%201%0A%20%20%20%20elif%20C%20%3C%20L%5Bmilieu%5D%3A%0A%20%20%20%20%20%20droite%20%3D%20milieu%20-%201%0A%20%20%20%20else%3A%20%23%20trouv%C3%A9!%0A%20%20%20%20%20%20return%20milieu%0A%20%20return%20None%0A%20%20%0Aprint%28recherche_dicho%2814,L%29%29&codeDivHeight=400&codeDivWidth=350&cumulative=false&curInstr=0&heapPrimitives=nevernest&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false) du site **PythonTutor**

## Terminaison de l'algorithme

Est-on sûr que l'algorithme va se terminer ?  
<env>Problèmes Possibles</env>  
La boucle `while` utilisée doit nous inciter à la prudence, il y a en effet plusieurs risques (voir [cours](https://eskool.gitlab.io/1nsi/python/boucle_while/) sur la boucle `while`) :

* de ne **jamais entrer** dans la boucle `while`
* de ne **jamais sortir** de la boucle `while` (càd de rentrer dans une boucle infinie)

Alors pourquoi est-on sûr que ce n'est pas le cas ?

<env>Idée intuitive initiale</env> Bien observer la position des deux flèches bleues lors de l'exécution de l'algorithme : leur distance diminue strictement à chaque nouvelle étape/itération.

![image](./img/fig4.png){.center}

<env>Démonstration de la Terminaison</env>  
On note <enc>$v=droite-gauche+1$</enc>  
Nous allons montrer que cette quantité est un Variant de Boucle, càd qu'il vérifie les conditions suivantes :

* $v$ est un **nombre entier**
* $v$ est initialement **strictement positif**, car initialement:
    * $gauche=0$ et $droite\ge 0$, donc
    * $v = droite-gauche+1 = droite-0+1 \ge 0 + 1>0$
* $v=droite-gauche+1$ **décroit d'au moins un cran à chaque itération** (ou bien on a trouvé la valeur cible), car on est forcément dans l'une des $3$ situations suivantes :
    * ou bien c'est la gauche qui augmente d'au moins $1$ cran (Ligne $11$), car $milieu+1\ge gauche+1$, et pendant ce temps la droite est inchangée. Auquel cas, la valeur $v=droite-gauche+1$ **diminue d'au moins 1 cran** (car on enlève davantage)
    * ou bien c'est la droite qui diminue d'au moins $1$ cran (Ligne $13$), car $milieu-1\le droite-1$, et pendant ce temps la gauche est inchangée. Auquel cas, $v=droite-gauche+1$ **diminue d'au moins $1$ cran**.
    * ou bien on a trouvé la bonne solution (ligne $15$)
* la condition $v\le 0 \Leftrightarrow droite-gauche+1 \le 0 \Leftrightarrow droite -gauche \le -1 <0$ correspond bien à la sortie de la boucle

## Correction de l'Algorithme

L'algorithme fait-il ce qu'il censé faire? Est-il correct?

Notons provisoirement `g=gauche`, `d=droite` et `m=milieu`.  
L'algorithme est plus subtil qu'il n'y paraît de prime abord.

<env>Les Bornes restent entre 0 et la taille initiale</env>  
Commençons par montrer que l'algorithme ne va pas échouer en raison d'un dépassement (trop à gauche ou trop à droite) des bornes du tableau L :

* Tout d'abord, remarquons que chacune des inégalités `g>=0` et `d <= n-1` est un invariant de boucle
* Ensuite, à l'intérieur de la boucle (puisque `g <= d`, on a toujours : `0 <= g <= d <= n-1`
* Enfin, le tableau `L` n'est accédé que par la valeur de `m=(g+d)//2` qui est le milieu de gauche et droite (arrondi à l'entier inférieur), ce qui garantit un accès légal au tableau

<env>Démonstration de la Correction</env>  
On note <enc>Inv : &laquo; la valeur `C` ne peut se trouver que dans `L[g..d]` &raquo;</enc>.  
Montrons qu'il s'agit bien d'un invariant de boucle :

* **Initialisation** : Inv est vrai initialement, car on a initialisé les variables à `g=0` et `d=n-1`
* **Hérédité/Conservation** : Supposons que Inv soit vrai en entrant dans une certaine itération :
    * Si `g` est modifié, par `g=m+1` alors l'invariant Inv est préservé, car le tableau est trié et `C>L[m]`
    * Si `d` est modifié, par `d=m-1` alors l'invariant Inv est préservé, car le tableau est trié et `C<L[m]`
    * Il y a donc bien conservation en fin d'itération, donc en début de la suivante
* **Terminaison** :
    * soit, durant une certaine itération, on renvoie un entier `m` (ligne 12) qui vérifie alors obligatoirement `L[m]=C` d'après les 2 tests précédents (à la ligne 7 et ligne 9), donc `m` est bien le bon indice correspondant à `C` dans `L`. Ceci prouve que l'algorithme est correct dans le cas où la valeur cible `C` appartienne bien au tableau `L`.
    * soit, on renvoie `None`: Remarquons que dans ce cas on est obligatoirement allé jusqu'au bout de la boucle while le plus longtemps possible, et que dans ce cas forcément, on avait `g=d` durant le dernier tour (sinon, si on avait `d>=g+1`, alors on aurait pu refaire encore un tour). De plus, on sait d'après l'hérédité que `C` se trouve obligatoirement dans `L[g..d]=L[g..g]`, ceci prouve que si on renvoie `None`, c'est que ce dernier test a échoué durant la dernier itération (donc `C!= L[g]` et `L[g]` était la dernière valeur restante à tester). Ensuite, on sort de la boucle (en incrémentant `g` de 1, ou décrémentant `d` de 1 selon la situation) pour parvenir à la ligne $13$. Cela prouve donc que la valeur cible `C` n'appartient pas au tableau `L`, et donc que l'**algorithme est correct** dans le cas où la valeur `C` n'appartienne pas au tableau `L`.

## Complexité de l'Algorithme

### Calcul de la Complexité/Coût

A une constante multiplicative près, il revient au même de compter le nombre d'opérations que le nombre d'étapes. Nous nous concentrerons sur le nombre d'étapes réalisées : mais combien d'étapes (au maximum, càd dans le pire des cas) sont-elles nécessaires pour arriver à la fin de l'algorithme ?  

<env>Raisonnement</env>  
Ce qui suit est un raisonnement plus qu'une démonstration. Nous nous en satisferons.
Imaginons que la liste initiale possède $8$ valeurs :

* Après $1$ étape, il ne reste que $4$ valeurs à traiter
* Après $2$ étapes, il ne reste que $2$ valeurs
* Après $3$ étapes, il ne reste qu' $1$ seule valeur

A chaque étape, le nombre de valeurs restantes à traiter est divisée par $2$.
Il y a donc $3$ étapes avant de trouver la valeur cherchée, car $2^3=8$.

!!! ex
    1. Remplissez le tableau ci-dessous :

        | taille de la liste | 1 | 2 | 4 | 8 | 16 | 32 | 64 | 128 | 256 |
        | :----------------- |:-:|:-:|:-:|:-:|:-: |:-: |:-: | :-: | :-: |
        | nombre d'étapes    | _ | _ | _ | 3 |  _ |  _ |  _ |  _  |  _  |

    2. Pouvez-vous deviner le nombre d'étapes nécessaires pour une liste de $4096$ termes ?
    3. Pour une liste de $2^p$ termes, quel est le nombre d'étapes ?
    4. Pour une liste de $n$ termes, quel est le nombre d'étapes ?

!!! notation
    On note <enc>$\log_2(n)$</enc> le plus petit (/le premier) entier $p$ tel que $2^p>n$.  
    Ce nombre est appelé le logarithme binaire de $n$, ou logarithme en base $2$ de $n$.

<env>Conclusion</env>  
Le nombre d'étapes nécessaires, càd la complexité/coût en temps de l'Algorithme par Dichotomie, est donc (un multiple de) $log_2(n)$.

!!! ex
    Créer une fonction `log2(n:int)->int` qui renvoie le logarithme binaire $p=log_2(n)$ d'un entier $n$

???- note "Une démonstration plus mathématique? + Master Theorem"
    Nous allons montrer qu'à chaque nouvelle itération (en supposant qu'on ne trouve pas `C` au milieu à cette itération), on divise par $2$ le nombre de possibilités restantes à tester (en arrondissant à l'entier directement inférieur).

    Notons provisoirement <enc>$n=d-g+1$</enc> le nombre d'éléments dans `L[g..d]`, càd le nombre d'éléments restants à tester à une certaine itération.

    * Ou bien, la valeur `C` est la valeur au milieu entre `g` et `d`, càd que `C` est la valeur cible et elle sera trouvée à l'itération suivante. Cela correspond au cas où `C = L[m]` où `m=(g+d)//2`. Dans ce cas, pas besoin d'étudier la diminution de la taille du tableau des éléments restants à traiter entre eux itérations, puisque l'algorithme s'arrête à l'itération suivante. Nous supposerons que nous ne sommes pas dans ce cas.
    * Ou bien `C` n'est pas la valeur située au milieu de `g` et `d` (càd supposons que `C!=L[m]` avec `m = (g+d)//2`), il ne reste alors plus que deux possiblités:
        * soit `C>L[m]` auquel cas `C` se trouve forcément dans `L[m+1..d]`, donc le nombre d'éléments restants au tour suivant est un nombre entier $n'$ tel que $n'=d-(m+1)+1=d-m$. Ensuite, De deux choses l'une:
            * Ou bien $n=d-g+1=2p$ est un **nombre pair**, auquel cas, $n'=d-m=\dfrac{n}{2}=p \leq \dfrac{n}{2}$
            * Ou bien $n=d-g+1=2p+1$ est un **nombre impair**, auquel cas, $n'=d-m=\dfrac{n-1}{2}=p \leq \dfrac{n}{2}$
        Dans tous les cas (pair ou impair), au tour suivant, il reste au maximum $\dfrac{n}{2}$ : La taille des éléments à tester a donc été divisée par $2$
        * soit `C<L[m]` auquel cas `C` se trouve forcément dans `L[g..m-1]`, donc le nombre d'éléments restants au tour suivant est un nombre entier $n'$ tel que $n'=(m-1)-g+1=m-g$. Ensuite, De deux choses l'une:
            * Ou bien $n=d-g+1=2p$ est un **nombre pair**, auquel cas, $n'=m-g\lt \dfrac{n}{2}$
            * Ou bien $n=d-g+1=2p+1$ est un **nombre impair**, auquel cas, $n'=m-g=\dfrac{n-1}{2}=p \leq \dfrac{n}{2}$
        Dans tous les cas (pair ou impair), au tour suivant, il reste au maximum $\dfrac{n}{2}$ nombres à traiter: La taille des éléments à tester a donc été divisée par $2$ à chaque nouvelle itération.
        * Conclusion : Le nombre d'éléments restants à tester à l'itération suivante a été divisé par $2$ par rapport à l'itération précédente.
    * Notons $n$ la taille des données en entrée de l'algorithme, càd le nombre d'éléments (déjà triés) dans le tableau initial, et <enc>$C(n)$</enc> la complexité/coût en temps de l'algorithme, càd le nombre total d'itérations à réaliser pour terminer l'algorithme. Le résultat précédent montre que $C(n)$ vérifie la relation: $C(n)=C\left( \dfrac{n}{2} \right)+1$, ou ce qui revient au même :  
    <center><enc>$C(2n)=C(n)+1$</enc></center>  
    D'après le master theorem, on sait que cette relation correspond à une complexité logarithmique, en $O(log_2(n))$ : il s'agit donc d'une recherche rapide

!!! pte
    Pour l'algorithme par Dichotomie, la taille des données en entrée, càd le nombre d'entiers restants à traiter, est divisé par $2$ à chaque nouvelle itération. On dit dans ce cas que l'Algorithme par Dichotomie a une **complexité/coût/vitesse logarithmique**, càd en $O(log_2(n)$. Cette dernière notation veut dire que le nombre total d'étapes (d'opérations) pour terminer l'algorithme est égal à une certaine constante multipliée par $log_2(n)$.

### Vérifications Expérimentales

Nous allons comparer les complexités/coûts en temps, dans le pire des cas, de l'algorithme par Dichotomie avec celle de l'Algorihtme de parcours séquentiel (par balayage). Dans les deux algorithmes, le pire des cas correspond au cas où l'on recherche le dernier élément (le plus à droite).

#### Recherche dans 100 000 Valeurs triées

##### Récupérer la liste de 100 000 valeurs 

Télécharger localement le fichier [`input_centmille.txt`](.data/input_centmille.txt), et placer-le dans le même répertoire que votre script Python.

```python
# Le snippet suivant permet de transformer le contenu du fichier `input_centmille.txt`
# en une liste L de 100 000 entiers.

f = open("input_centmille.txt",'r')
l = f.readlines()
L = []
for k in l :
    L.append(int(k[:-1]))
```

##### 100 000 valeurs avec Parcours Séquentiel

<env>Rappel: Parcours Séquentiel</env>

```python
def parcours_sequentiel(C, L) :
    for k in range(len(L)) :
        if L[k] == C:
            return k
    return "non trouvé"
```

Mesurons le temps nécessaire pour trouver l'indice de la dernière valeur de la liste (qui est $299474$) avec la **méthode de parcours séquentiel** :

```python
from timeit import timeit
C = 299474
centmille_sequentiel = timeit("parcours_sequentiel(C, L)", globals=globals(), number=1000)  # par défaut : number = 1000000,
print("Temps Parcours Séquentiel = ", centmille_sequentiel)
```

##### 100 000 valeurs par Dichotomie

Mesurons le temps nécessaire pour trouver l'indice de la dernière valeur de la liste (qui est $299474$) avec la **méthode par dichotomie** :

```python
# from timeit import timeit
# C = 299474
centmille = timeit("recherche_dicho(C, L)", globals=globals(), number=1000)  # number = 1000000,
print("Temps Algo par Dichotomie = ", centmille)
```

##### Comparaison Dichotomie vs Parcours Séquentiel

<env>Exemple de Résultats</env>  
```console
# Pour 100 000 valeurs
Temps Parcours Séquentiel =  0.2935811650022515
Temps Algo par Dichotomie =  0.00019076699391007423
```

<env>Conclusion</env>  
L'**algorithme dichotomique** est bien plus rapide que l'algorithme de parcours séquentiel :  
Pour $n=100\,000=10^5$ valeurs à trier, le quotient $\dfrac{temps \,\, séquentiel}{temps \,\, dichotomie}=\dfrac{n}{log_2(n)}$ a pour ordre de grandeur environ $10^3$.  
En effet, $\dfrac{n}{log(n)} = \dfrac{10^5}{log_2(10^5)} \approx 6020.60$ et $\dfrac{0.2935811650022515}{0.00019076699391007423} \approx 1538.95$  
Pour **cent mille** valeurs à trier, l'algorithme par dichotomie est environ $1\,000$ fois plus rapide que l'algorithme de parcours séquentiel.

#### Recherche dans 1 000 000 Valeurs triées

##### Récupérer la liste de 1 000 000 de valeurs

Télécharger localement le fichier [`input_million.txt`](.data/input_million.txt), et placer-le dans le même répertoire que votre script Python.

```python
# Le snippet suivant permet de transformer le contenu du fichier `input_million.txt`
# en une liste L de 1 000 000 entiers.

f = open("input_million.txt",'r')
l = f.readlines()
L = []
for k in l :
    L.append(int(k[:-1]))
```

##### 1 000 000 valeurs avec Parcours Séquentiel

Mesurons le temps nécessaire pour trouver l'indice de la dernière valeur de la liste (qui est $2999306$) avec la **méthode de parcours séquentiel** :

```python
from timeit import timeit
C = 2999306
million_sequentiel = timeit("parcours_sequentiel(C, L)", globals=globals(), number=1000)  # par défaut : number = 1000000,
print("Temps Parcours Séquentiel = ", million_sequentiel)
```

##### 1 000 000 valeurs par Dichotomie

Mesurons le temps nécessaire pour trouver l'indice de la dernière valeur de la liste (qui est $2999306$) avec la **méthode par dichotomie** :

```python
# from timeit import timeit
# C = 2999306
million = timeit("recherche_dicho(C, L)", globals=globals(), number=1000)  # number = 1000000,
print("Temps Algo par Dichotomie = ", million)
```

##### Comparaison Dichotomie vs Parcours Séquentiel

<env>Exemple de Résultats</env>  
```console
# Pour 1 000 000 valeurs
Temps Parcours Séquentiel = 2.6699887700015097
Temps Algo par Dichotomie = 0.0001729260038700886
```

<env>Conclusion</env>  
L'**algorithme dichotomique** est *bien plus* rapide que l'algorithme de parcours séquentiel :  
Pour $n=1\,000\,000=10^6$ valeurs à trier, le quotient $\dfrac{temps \,\, séquentiel}{temps \,\, dichotomie} = \dfrac{n}{log_2(n)}$ a pour ordre de grandeur environ $10^4$.  
En effet, $\dfrac{n}{log_2(n)} = \dfrac{10^6}{log_2(10^6)} \approx 50171.67$ et $\dfrac{2.6699887700015097}{0.0001729260038700886} \approx 15440.07$  
Pour **un million** de valeurs à trier, l'algorithme par dichotomie est environ $10\,000$ fois plus rapide que l'algorithme de parcours séquentiel.

### Taille de la liste & Vitesse de chaque méthode

* méthode avec **parcours séquentiel**: la recherche dans une liste 10 fois plus grande prend environ $10$ fois plus de temps : la vitesse de l'algorithme est bien proportionnelle à la taille $n$ de la liste. $\dfrac{10^6}{10^5} = 10$ et $\dfrac{2.6699887700015097}{0.2935811650022515} \approx 9.09$
* méthode **par dichotomie**: la recherche dans une liste $10$ fois plus grande prend environ $1,2$ fois plus de temps : la vitesse de l'algorithme est bien proportionnelle au **logarithme** de la taille $n$ de la liste : $\dfrac{log_2(1\,000\,000)}{log_2(100\,000)} = 1.2$ et $\dfrac{0.0001729260038700886}{0.00019076699391007423} \approx 0.91$

## Précaution Finale

Il ne faut toutefois pas oublier que la **méthode dichotomique**, bien que beaucoup plus rapide, nécessite que la liste ait été **auparavant triée**. Ce qui rajoute du temps de calcul ! (cf [tri par insertion](https://eskool.gitlab.io/1nsi/algorithmique/tris/insertion/) ou [tri par sélection](https://eskool.gitlab.io/1nsi/algorithmique/tris/selection/) )


## Références & Notes

### Références

* Cette page est inspirée de [Programmation de la méthode de dichotomie, Gilles Lassus](https://glassus.github.io/premiere_nsi/T4_Algorithmique/4.5_Dichotomie/cours/)
* L'animation est inspirée de [Binary Search, by Yong Daniel Liang](https://yongdanielliang.github.io/animation/web/BinarySearchNew.html)

### Notes

[^1]: [Diviser pour Régner, Wikipedia](https://fr.wikipedia.org/wiki/Diviser_pour_r%C3%A9gner_(informatique))
