function init() {
    reset();
}

function get_var(variable) {
    let el = document.querySelector("[data-md-color-scheme]");
    return getComputedStyle(el).getPropertyValue(variable);
}

function reset() {
    low = 0;
    high = SIZE - 1;
    i = 0;
    document.getElementById('highlight').style.visibility = 'hidden';
    document.getElementById('lowPosition').style.visibility = 'hidden';
    document.getElementById('lowValue').style.visibility = 'hidden';
    document.getElementById('midPosition').style.visibility = 'hidden';
    document.getElementById('midValue').style.visibility = 'hidden';
    document.getElementById('highPosition').style.visibility = 'hidden';
    document.getElementById('highValue').style.visibility = 'hidden';
    setRandomValue();
    resetColor();
    document.getElementById('remark').innerHTML = 'Une nouvelle liste aléatoire a été créée';
    document.getElementById('value').disabled = false;
    for (var j = 0; j < SIZE; j++) {
        id = 'check' + j;
        document.getElementById(id).innerHTML = "";
    }
}

function resetColor() {
    for (var i = 0; i < SIZE; i++) {
        id = 'list' + i;
        let bgcolor = get_var("--md-default-bg-color");
        let color = "#34a4ff";
        // document.getElementById(id).style.backgroundColor = "white";
        document.getElementById(id).style.backgroundColor = bgcolor;
        document.getElementById(id).style.fontWeight = "bold";
        // document.getElementById(id).style.color = "#05a3f3";
        document.getElementById(id).style.color = color;
    }
}

function changeTheme() {
    for (var i = 0; i < SIZE; i++) {
        id = 'list' + i;
        let bgcolor = get_var("--md-default-bg-color");
        let color = "#34a4ff";
        // document.getElementById(id).style.backgroundColor = "white";
        if (document.getElementById(id).style.backgroundColor != "lightcoral") {
            document.getElementById(id).style.backgroundColor = bgcolor;
            document.getElementById(id).style.fontWeight = "bold";
            // document.getElementById(id).style.color = "#05a3f3";
            document.getElementById(id).style.color = color;
        } 
    }
}

const deviceType = () => {
    const ua = navigator.userAgent;
    if (/(tablet|ipad|playbook|silk)|(android(?!.*mobi))/i.test(ua)) {
        return "tablet";
    }
    else if (/Mobile|Android|iP(hone|od)|IEMobile|BlackBerry|Kindle|Silk-Accelerated|(hpw|web)OS|Opera M(obi|ini)/.test(ua)) {
        return "mobile";
    }
    return "desktop";
};

function sortNumber(a, b) {
    return a - b;
}

function setRandomValue() {
    listValues = [];
    for (var i = 0; i < SIZE; i++) {
        listValues.push(Math.floor(Math.random() * 100));
    }
    listValues.sort(sortNumber);
    for (var i = 0; i < SIZE; i++) {
        id = 'list' + i;
        document.getElementById(id).innerHTML =
                listValues[i];
    }
}

function draw() {
    var count = 0;
    for (var i = 0; i < 1; i++) {
        for (var j = 0; j < 20; j++) {
            id = 'cell' + j;
            // document.write(id.toString());
            document.getElementById(id).style.top = (i + 1) * 40 + 30 + "px";
            document.getElementById(id).style.left = (j + 1) * 40 + "px";
//                  if (count++ % 2 == 0)
//                      document.getElementById(id).style.backgroundColor = "#37826c";
//                  else
//                      document.getElementById(id).style.backgroundColor = "lightGray";

            document.getElementById(id).innerHTML = "2";
        }
        count++;
    }
}

function step() {
    document.getElementById('lowPosition').style.visibility = 'visible';
    document.getElementById('lowValue').style.visibility = 'visible';
    document.getElementById('highPosition').style.visibility = 'visible';
    document.getElementById('highValue').style.visibility = 'visible';
    
    var idLow = 'list' + low;
    posLow = getElementPos(document.getElementById(idLow))
    offsetAnimation = getElementPos(document.getElementById("animation"));
    console.log("offsetAnimation.y="+offsetAnimation.y);
    document.getElementById('lowPosition').style.top
            // = posLow.y - 30 + "px";
            = posLow.y - offsetAnimation.y - 100 + "px";
    document.getElementById('lowPosition').style.left
            // = posLow.x + posLow.w / 2 - 5 + "px";
            = posLow.x - offsetAnimation.x + posLow.w / 2 - 3 + "px";
    document.getElementById('lowValue').style.top
            // = posLow.y - 43 + "px";
            = posLow.y - offsetAnimation.y - 110 + "px";
    document.getElementById('lowValue').style.left
            // = posLow.x + posLow.w / 2 - 18 + "px";
            = posLow.x - offsetAnimation.x + posLow.w / 2 - 18 + "px";
    document.getElementById('lowValue').innerHTML = "gauche: " + low;


    var idHigh = 'list' + high;
    posHigh = getElementPos(document.getElementById(idHigh));

    if (high != low) {
        document.getElementById('highPosition').style.top
                // = posHigh.y - offsetAnimation.y - 48 + "px";
                = posHigh.y - offsetAnimation.y - 100 + "px";
        document.getElementById('highPosition').style.left
                // = posHigh.x - offsetAnimation.y + posHigh.w / 2 - 5 + "px";
                = posHigh.x - offsetAnimation.x + posHigh.w / 2 - 5 + "px";
    } else {
        document.getElementById('highPosition').style.visibility = 'hidden';
    }

    document.getElementById('highValue').style.top
            // = posHigh.y - offsetAnimation.y - 63 + "px";
            = posHigh.y - offsetAnimation.y - 150 + "px";
    document.getElementById('highValue').style.left
            // = posHigh.x - offsetAnimation.y + posHigh.w / 2 - 18 + "px";
            = posHigh.x - offsetAnimation.x + posHigh.w / 2 - 18 + "px";
    document.getElementById('highValue').innerHTML = "droite: " + high;


    if (high < low) {
        var id1 = 'check' + i;
        document.getElementById(id1).innerHTML = "&#x2717;";
        document.getElementById('remark').innerHTML =
                'Valeur Cible NON trouvée, et Fin de la Recherche.';
        jAlert("Valeur Cible NON trouvée. Recherche terminée. La méthode renverra " + (-1 - low) + ".");

        return;
    }

    document.getElementById('midPosition').style.visibility = 'visible';
    document.getElementById('midValue').style.visibility = 'visible';

    mid = Math.floor((low + high) / 2);

    document.getElementById('value').disabled = true;
    var key = document.getElementById('value').value;
    i = mid;
    var id = 'list' + i;
    posLoc = getElementPos(document.getElementById(id));

    if (mid == low || mid == high) {
        document.getElementById('midPosition').style.visibility = 'hidden';
    }
    else {
        document.getElementById('midPosition').style.top
                = posLoc.y - offsetAnimation.y - 100 + "px";
        document.getElementById('midPosition').style.left
                = posLoc.x - offsetAnimation.x + posLoc.w / 2 - 5 + "px";
    }

    document.getElementById('midValue').style.top
            = posLoc.y - offsetAnimation.y - 130 + "px";
    document.getElementById('midValue').style.left
            = posLoc.x - offsetAnimation.x + posLoc.w / 2 - 18 + "px";
    document.getElementById('midValue').innerHTML = "milieu: " + i;

    document.getElementById('highlight').style.top =
            posLoc.y - offsetAnimation.y + posLoc.h - 18 + "px";
    document.getElementById('highlight').style.left =
            posLoc.x - offsetAnimation.x + "px";
    document.getElementById('highlight').style.width =
            posLoc.w + "px";
    document.getElementById('highlight').style.height =
            posLoc.h + 10 + "px";
    document.getElementById('highlight').innerHTML = key;
    document.getElementById('highlight').style.visibility = 'visible';
    resetColor();
    document.getElementById(id).style.backgroundColor =
            "lightCoral";
    document.getElementById(id).style.color = "black";
    if (key == document.getElementById(id).innerHTML) {
        var id1 = 'check' + i;
        document.getElementById(id1).innerHTML = "&#x2713;"; // Symbole gagné
        document.getElementById('remark').innerHTML =
            'Valeur Cible trouvée';
        jAlert("Valeur Cible trouvée à l'indice " + i);
        return;
    }
    else if (key < listValues[mid]) {
        high = mid - 1;
        var id1 = 'check' + i;
        document.getElementById(id1).innerHTML = "&#x2717;";
        document.getElementById('remark').innerHTML = 'Puisque ' +
                key + ' est inférieur à ' + document.getElementById(id).innerHTML + ', ' +
                'La recherche se poursuit sur la gauche.';
    }
    else {
        low = mid + 1;
        var id1 = 'check' + i;
        document.getElementById(id1).innerHTML = "&#x2717;";
        document.getElementById('remark').innerHTML = 'Puisque ' +
                key + ' est supérieur à ' + document.getElementById(id).innerHTML + ', ' +
                'La recherche se poursuit sur la droite.';
    }
}

function init1() {
    posLoc = getElementPos(document.getElementById('program'));
    x = posLoc.x;
    y = posLoc.y;
    for (var i = 0; i < 1; i++) {
        for (var j = 0; j < 20; j++) {
            var id = 'cell' + j;
//                document.getElementById(id).style.visibility = "hidden";
            document.getElementById(id).style.top = y + i * 40 + "px";
            document.getElementById(id).style.left = x + j * 40 + "px";
//                  $(id).css("top", y + 90 + j * 40)
//                          .css("left", x + 115 - 3 / 2 + i * 40);
        }
    }
}

const mutationCallback = (mutationsList) => {
    for (const mutation of mutationsList) {
      if (
        mutation.type !== "attributes" ||
        mutation.attributeName !== "data-md-color-scheme"
      ) {
        return
      }
      // console.log('old:', mutation.oldValue);
      // console.log('new:', mutation.target.getAttribute("data-md-color-scheme"));
      changeTheme();
    }
  };

const observer = new MutationObserver(mutationCallback);

let themeChange = document.querySelector("body");
observer.observe(themeChange, { attributes: true });