import time

# VALEUR DE LA SOLUTION DE L'ALGORITHME GLOUTON ITÉRATIF

def minPieces(S:tuple,X:int)->int:
    if X==0:
        return 0
    else:
        mini=0
        while X>0:
            for i in range(len(S)):
                if S[i]<=X:
                    plusGrosse = S[i]
            X-=plusGrosse
            mini+=1
        return mini

S=(1,2,5,10,20,50,100,200)
X=33

# count = 0
start=time.process_time()
m = minPieces(S,X)
stop=time.process_time()
t = stop - start

print("Nombre min de pièces, m=",m,"en t= ",t,"sec")
