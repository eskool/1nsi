# TD de Huffman

##  Introduction

Le codage historique des caractères, par exemple **le codage ASCII**, utilise une même longueur pour représenter un caractère en mémoire : on parle de **codage à longueur fixe**. Ce mode de stockage présente l'avantage d'être simple à représenter en mémoire, mais lors de la transmission de caractères via un canal de transmission, on se retrouve dans une situation où des caractères très usités prennent la même bande passante que des caractères très peu usités.

Le **codage de Huffman** (1952) est une méthode de compression de données qui permet de réduire la longueur du codage d’un alphabet. Il substitue `a un code de longueur fixe (le code ASCII, par exemple, où chaque caractère est codé sur 8 bits) un code de longueur variable. Ce principe de compression est utilisé dans de nombreuses applications.

Le principe général du codage de Huffman est d’associer `a chaque symbole du texte `a enco-
der un code binaire qui est d’autant plus court que le caractère correspondant a un nombre
d’occurrences élevé dans le texte `a encoder.
Par exemple, dans le texte ”
les crépuscules dans cet enfer africain se révélaient fameux
” (extrait du
“Voyage au bout de la nuit” de Céline), le caractère ’ ’ (espace) qui apparaˆıt 8 fois pourra ˆetre
codé avec 3 bits ”
011
”, le caractère ’e’ qui apparaˆıt 8 fois pourra ˆetre codé avec 3 bits ”
100
”, le
caractère ’s’ qui apparaˆıt 5 fois pourra ˆetre codé avec 4 bits ”
1111
” ; le caractère ’p’ qui apparaˆıt
1 fois pourra ˆetre codé avec 6 bits ”
110110
”, etc. Voici le mˆeme texte codé par le codage de
Huffman :
0010100111101110101100010011011000111111101000110010100111101111010011101011111101110101000101101110010110000100
1100011111000001100000110101110000110110111111100011110001001101110100001011100001100101101011011000011101101011
00001101010
La longueur du texte ainsi codé est de 235 bits, qui peuvent se représenter avec 30 octets alors
que le texte non codé occupe 60 octets.
Les codes étant de longueur variable, il est nécessaire, pour pouvoir décoder un texte, que le
code de chaque caractère ne soit pas préfixe du code d’un autre caractère. Pour obtenir cette
propriété, on représente un code de Huffman par un arbre binaire dont les feuilles sont étiquetées
par des couples
<
caractère, fréquence
>
1
. Voici l’arbre de Huffman qui permet de coder puis de
décoder les caractères du texte ci-dessus :
Fig.
1 – arbre de (dé)codage de Huffman
n,4
r,4
c,4
.,8
.,8
.,4
.,16
.,18
.,34
.,10
a,5
s,5
.,2
.,2
v,1
d,1
p,1
m,1
e,8
f,3
i,3
u,3
é,3
l,3
.,6
.,6
.,6
.,3
t,2
x,1
.,12
.,14
.,26
.,60
esp,8
0
1
0
0
0
0
0
0
0
0
0
0
0
0
0
0
0
0
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
Le code d’un caractère est donné par le chemin suivi depuis la racine jusqu’`a la feuille où il est
situé, en associant un ”
0
” au fils gauche et un ”
1
” au fils droit ; on voit donc que le code du
caractère ’f’ est ”
0000
”, celui du caractère ’x’ est ”
01010
” et celui du ’v’ est ”
110111
”.
1
l’étiquette des sommets internes n’est pas significative pour le (dé)codage
1                version du 2 décembre 2005
Université Rennes 1 - IFSIC              DIIC1                         Programmation
2
 ́
Etapes du codage et du décodage
Cette partie indique les grandes étapes du cadage et du décodage ; la réalisation concrète est
décrite plus loin (voir 4 et 5).
2.1  Codage
1. Lire le texte contenu dans le fichier `a coder, calculer la fréquence d’apparition de chaque
caractère et construire une
table de fréquence
s, enregistrer cette table dans le fichier codé
de sortie pour permettre de reconstruire l’arbre de Huffmann lors du décodage.
2. Construire l’
arbre de Huffman
, puis en déduire une
table de codage
.
3. Lire une deuxième fois le texte, le coder avec la table de codage puis enregistrer le résultat
dans le fichier codé de sortie.
2.2  Décodage
1. Lire dans le fichier `a décoder la table de fréquences.
2. Construire l’arbre de Huffman.
3. Lire le texte codé, le décoder avec l’arbre de Huffman puis enregistrer le résultat dans le
fichier de sortie.
3  Outils fournis
Copiez tous les fichiers du répertoire
/share/diic1/prog/huffman
dans le répertoire de votre TP.
Pour tester vos fonctions
au fur et `a mesure de leur achèvement
, vous pouvez utiliser les fonc-
tions fournies dans la classe
OutilsHuffman
. Pour cela, il suffit de préfixer leur nom par celui de la
classe
OutilsHuffman
. Il vous sera demandé d’écrire vous-mˆeme certaines de ces fonctions : pour
utiliser vos fonctions `a la place de celles fournies, il suffira d’enlever le préfixe
OutilsHuffman
.
Dans la suite, on appelle
texte  codé
une chaˆıne de caractères composée de ’0’ et de ’1’ qui
représente un texte dont les caractères sont codés par le codage de Huffman.
1. Liste des fonctions disponibles :
(a) pour le codage
–
static int [ ] calculerFrequences(String nomFichier)
: cette fonction calcule le nombre
d’occurrences de chaque caractère du texte contenu dans le fichier, construit et
renvoie la table des fréquences ; c’est un tableau indicé par les caractères du code
ASCII.
–
static String lireTexte(String nomFichier)
: lit le texte dans le fichier donné et le renvoie
dans une chaˆıne de caractères.
–
static void enregistrerTableFrequences(int [ ] tabfreq, String nomFichierCode)
: enregistre
la table de fréquences dans le fichier codé de sortie.
–
static void enregistrerTexteCode(String texteCode, String nomFichierCode)
: enregistre le
texte codé dans le fichier codé de sortie.
(b) pour le décodage
2                version du 2 décembre 2005
Université Rennes 1 - IFSIC              DIIC1                         Programmation
–
static int [ ] lireTableFrequences(String nomFichierCode)
: cette fonction lit et renvoie la
table de fréquences présente dans le fichier codé de nom donné ; c’est un tableau
indicé par les caractères du code ASCII.
–
static String lireTexteCode(String nomFichierCode)
: lit le texte codé dans le fichier
donné et le renvoie dans une chaˆıne de caractères composée de ’0’ et de ’1’.
–
static void enregistrerTexte(String texte, String nomFichier)
: enregistre le texte (non
codé) dans le fichier indiqué.
(c) pour les deux
–
static ABinHuffman construireArbreCodageHuffman(int [ ] tableFrequences)
: construit et
renvoie l’arbre de (dé)codage de Huffman qui correspond `a la table de fréquence
donnée.
(d) divers
–
static ABinHuffman consArbre(Couple
<
Character, Integer
>
x, ABinHuffman abhg, ABin-
Huffman abhd)
: étant donné un couple
<
caractère, fréquence
>
x
et deux arbres
binaires de Huffman
abhg
et
abhd
, cette fonction construit et renvoie un nouvel
arbre binaire de Huffman d’étiquette
x
, dont le fils gauche est
abhg
, le fils droit
abhd
.
–
static long getInstantPresent()
: donne un nombre de millisecondes correspondant `a
l’instant présent ; peut servir pour mesurer le temps que dure chaque traitement.
–
static long tailleFichier(String nomFichier)
: donne la taille (en octets) d’un fichier ;
sert pour les calculs de taux de compression.
2. Liste des types fournis :
La spécification de chacun de ces types est celle vue en cours.
–
ABinHuffman
: arbre binaire de Huffman utilisé pour le (dé)codage ; l’implémentation
est fournie.
–
Couple
<
Character, Integer
>
: type des étiquettes d’un arbre binaire de Huffman ; le ca-
ractère est celui qu’on veut (dé)coder, l’entier est le nombre d’occurrences du caractère
dans le texte `a (dé)coder ; l’implémentation est fournie.
–
ListeABH
: liste dont les éléments sont des arbres binaires de Huffman ; utilisée pour
construire l’arbre de (dé)codage ; l’implémentation est fournie.
3. Quelques méthodes de la classe
String
(l’indice d’un caractère dans une chaˆıne est toujours
compris entre 0 et la longueur de la chaˆıne - 1) :
–
char charAt(int idx)
: donne le caractère d’indice
idx
de la chaˆıne courante.
–
boolean endsWith(String suffixe)
: vrai si la chaˆıne courante se termine par la chaˆıne
suffixe
.
–
int indexOf(char c)
: indice de la première occurrence du caractère
c
dans la chaˆıne
courante ; rend -1 si
c
est absent.
–
int length()
: longueur de la chaˆıne courante.
–
String substring(int beginIndex, int endIndex
) : rend une sous-chaˆıne de la chaˆıne courante
qui début au caractère d’indice
beginIndex
et se termine
avant
le caractère d’indice
endIndex
On rappelle que l’opérateur
+
permet de concaténer deux chaˆınes ou une chaˆıne et un
caractère.
4. Liste des fichiers de test :
– fichiers de suffixe ”
.txt
” : différents textes `a coder ;
exemple.txt
correspond `a l’exemple
présenté.
– fichiers de suffixe ”
.txt.code
” : les mˆemes textes codés ; peuvent servir `a tester vos
fonctions de décodage.
3                version du 2 décembre 2005
Université Rennes 1 - IFSIC              DIIC1                         Programmation
4  Décodage
Faire une classe
DecodageHuffman
.
1.
 ́
Ecrire une fonction
public static String decoderTexte(String texteCode, ABinHuffman abh) ;
qui, étant donné un texte codé (disponible dans une chaˆıne de caractères) et l’arbre de
Huffman associé rend le texte décodé dans une chaˆıne de caractères ; aidez-vous de deux
fonctions auxiliaires qui permettent de consulter les composantes de l’étiquette d’un arbre
de Huffman.
2. Avec les fonctions de la classe
OutilsHuffman
, faire un programme de test pour décoder le
contenu du fichier
exemple.txt.code
; ce programme enchaˆınera les opérations suivantes :
– lire la table des fréquences dans le fichier ;
– construire l’arbre de décodage associé ;
– lire le texte codé dans le fichier ;
– décoder le texte et l’afficher au terminal ; vous devriez obtenir la phrase donnée dans
l’introduction (voir partie 1).
3.
 ́
Ecrire (et tester) dans la classe
DecodageHuffman
la procédure
public static void affiche-
rHuffman(ABinHuffman a) ;
qui affiche au terminal l’étiquette de chaque feuille de l’arbre de
Huffman (caractère et fréquence), ainsi que le code correspondant.
Vous pouvez ensuite tester votre décodage sur les différents fichiers fournis.
4                version du 2 décembre 2005
Université Rennes 1 - IFSIC              DIIC1                         Programmation
5  Codage
Faire une classe
CodageHuffman
.
5.1  Construction de l’arbre de Huffman
5.1.1  principe
1.
`
A partir du tableau de fréquences et
en  ne  retenant  que  les  caractères  effectivement
présents  dans  le  texte
, construire une liste dont les éléments sont des arbres binaires
de couples
<
caractère, fréquence
>
(voir figure 2) ; cette liste doit ˆetre triée par fréquence
croissante.
2. Construire l’arbre de Huffman :
– prendre les deux éléments (arbres) minimaux de la liste ci-dessus, en faire un nouvel
arbre dont la racine a pour étiquette un couple dont le caractère est quelconque (sans
signification) et la fréquence est la somme des fréquences des deux éléments retirés de
la liste.
– insérer l’arbre obtenu
`a sa place
dans la liste triée (voir figure 3).
– répéter juqu’`a ce que la liste ne contienne plus qu’un élément (voir figure 4, page 8) :
l’arbre obtenu est l’arbre de (dé)codage associé au texte.
Fig.
2 – liste initiale triée d’arbres binaires de Huffman
d,1
m,1
esp,8
e,8
p,1
x,1
v,1
t,2
f,3
i,3
l,3
u,3
é,3
c,4
n,4
r,4
a,5
s,5
Fig.
3 – construction d’un nouvel arbre et insertion dans la liste
d,1
m,1
.,2
p,1
x,1
v,1
t,2
f,3
i,3
l,3
u,3
é,3
c,4
n,4
r,4
a,5
s,5
esp,8
e,8
d,1
m,1
.,2
5.1.2  réalisation
 ́
Ecrire les fonctions suivantes :
1.
private static ListeABH faireListeAbinHuffman(int [ ] tableFrequences) :
étant donné une table
de fréquences, construit et renvoie une liste triée selon le principe décrit au paragraphe
5.1.1.
2.
public static ABinHuffman construireArbreCodageHuffman(int [ ] tableFrequences) :
construit et
renvoie l’arbre binaire de codage de Huffman correspondant `a la table de fréquence donnée
en paramètre.
Utiliser les deux fonctions écrites dans la partie 4 qui permettent de consulter les composantes
de l’étiquette d’un arbre de Huffman.
Pour tester le bon fonctionnement de la construction de l’arbre de Huffman, il suffit de reprendre
votre programme de décodage et d’y remplacer l’appel de la fonction
construireArbreCodageHuff-
man
de la classe
OutilsHuffman
par l’appel de la vˆotre.
5                version du 2 décembre 2005
Université Rennes 1 - IFSIC              DIIC1                         Programmation
5.2  Codage du texte
5.2.1  principe
1. On commence par construire une
table de codage
`a l’aide de l’arbre de Huffman : cette
table est indicée par les caractères du code ASCII ; pour chaque caractère, la valeur dans
la table est :
– sa chaˆıne de codage si le caractère est présent dans l’arbre de Huffman,
–
null
, si le caractère n’est pas dans l’arbre.
exemple : voici un extrait de la table de codage associée `a l’arbre de Huffman de la
figure 1 ; les caractères ’
b
’, ’
g
’ et ’
h
’ qui ne sont pas dans l’arbre n’ont pas de code associé.
indice
...
’a’
’b’
’c’
’d’
’e’
’f’
’g’
’h’
’i’
...
chaˆıne de codage
...
”
1110
”
null
”
1010
”
”
110100
”
”
100
”
”
0000
”
null
null
”
0001
”
...
2. On code ensuite chaque caractère du texte `a l’aide de la table de codage.
5.2.2  réalisation
1.
 ́
Ecrire les fonctions suivantes :
(a)
static String [ ] construireTableCodage(ABinHuffman abh) :
étant donné un arbre de codage
de Huffman, construit et retourne la table de codage associée.
Pour tester cette fonction, écrire un programme qui enchaˆıne les opérations sui-
vantes :
– lire la table de fréquences
– construire l’arbre de Huffman et l’afficher
– construire et afficher la table de codage
(b)
static String coderTexte(String texte, String [ ] tablecodage) :
étant donné un texte `a
coder disponible dans une chaˆıne de caractères et la table de codage associée, rend
une chaˆıne de caractères contenant le texte codé.
2. Faire un programme pour tester l’ensemble du codage :
– calculer la table de fréquences
– enregistrer la table de fréquences dans le fichier de sortie
– construire l’arbre de Huffman
– construire la table de codage
– lire le texte `a coder
– coder le texte
– l’enregistrer dans le fichier de sortie
Tester le codage sur les différents fichiers fournis et vérifier que le décodage produit un fichier
identique ... (Sous Unix et Linux, on peut comparer deux fichiers texte avec la commande
diff
).
Vérifier que votre programme de décodage décode correctement les textes que votre programme
de codage a codés.
6                version du 2 décembre 2005
Université Rennes 1 - IFSIC              DIIC1                         Programmation
6  Extensions
Programmer les opérations suivantes :
1.
static ABinHuffman consArbre(Couple
<
Character, Integer
>
x, ABinHuffman g, ABinHuffman d)
:
étant donné un couple
<
caractère, fréquence
>
x
et deux arbres binaires de Huffman
abhg
et
abhd
, cette fonction construit et renvoie un nouvel arbre binaire de Huffman d’étiquette
x
, dont le fils gauche est
abhg
, le fils droit
abhdd
;
Pour tester cette fonction, il suffit de remplacer l’appel de la fonction de la classe
Outil-
sHuffman
par la vˆotre.
2.
static int [ ] calculerFrequences(String nomFichier)
: lit le fichier (non codé) indiqué, calcule
le nombre d’occurrences (ou fréquence) de chaque caractère et le place dans un tableau
indicé par les caractères du code ASCII puis renvoie le tableau.
Pour lire les données d’un fichier, il faut ajouter cette directive en tˆete de votre fichier
source :
import java.io.InputStream ;
et utiliser les fonctions suivantes de la classe
Lecture
:
(a)
static InputStream ouvrir(String nomFichier)
:
ouvre
en
lecture
le fichier dont le nom est
donné et renvoie un identifiant (classe
InputStream
) avec lequel on peut effectuer les
autres opérations sur le fichier.
(b)
static boolean finFichier(InputStream f)
: renvoie
vrai
si toutes les données du fichier
désigné par l’identifiant
f
ont été lues.
(c)
static char lireChar(InputStream f)
: lit un caractère dans le fichier désigné par
f
et le
renvoie ; pré-condition :
non finFichier(f)
.
(d)
static void fermer(InputStream f)
:
ferme
le fichier désigné par
f
; après fermeture, on
ne peut plus accéder au contenu du fichier. Si on veut lire `a nouveau son contenu, il
faut le réouvrir.
3.
static void enregistrerTexte(String texte, String nomFichier)
: enregistre le texte (non codé) dans
le fichier indiqué.
Pour écrire dans un fichier, il faut ajouter cette directive en tˆete de votre fichier source :
import java.io.OutputStream ;
et utiliser les fonctions suivantes de la classe
Ecriture
:
(a)
static OutputStream ouvrir(String nomFichier)
:
ouvre
en
écriture
le fichier dont le nom
est donné et renvoie un identifiant (classe
OutputStream
) avec lequel on peut effectuer
les autres opérations sur le fichier.
(b)
static void ecrireString(OutputStream f, String texte)
: écrit la chaˆıne
texte
dans le fichier
désigné par
f
.
(c)
static void fermer(OutputStream f)
:
ferme
le fichier désigné par
f
; après fermeture, on
ne peut plus accéder au contenu du fichier. Cette opération est indispensable pour
s’assurer que le contenu du fichier est cohérent.
4. Calculer le taux de compression (ratio taille fichier codé / taille fichier non codé) et la
durée des différentes opérations.
7                version du 2 décembre 2005
