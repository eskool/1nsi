def moyenne(l:list)->float:
    '''renvoie la moyenne (non coefficientée) de l'''
    num = 0
    for el in l:
        num += el
    return num / len(l)

reponse=moyenne([12, 8, 14, 15])
print(f"Moyenne NON Coefficientée = {reponse}")