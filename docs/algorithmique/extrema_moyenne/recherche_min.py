def recherche_min(l:list)->list:
    '''renvoie le minimum de la liste l'''
    mini = l[0]           # 
    for el in l:
        if el < mini:
            mini = el
    return mini

reponse = recherche_min([12,8,14,15])
print(f"Minimum = {reponse}")