def recherche_max(l:list)->list:
    '''renvoie le maximum de la liste l'''
    maxi = l[0]           # 
    for el in l:
        if el > maxi:
            maxi = el
    return maxi

reponse = recherche_max([12,8,14,15])
print(f"Maximum = {reponse}")