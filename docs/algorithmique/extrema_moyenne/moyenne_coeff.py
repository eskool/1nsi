def moyenne_coeff(l:list, c:list)->float:
    '''renvoie la moyenne coefficientée de l avec les coeffs c'''
    assert len(l)==len(c), "ERREUR : l et c doivent être de même longueur"
    num = 0
    denom = 0
    for i in range(len(l)):
        num += c[i]*l[i]
        denom += c[i]
    return num / denom

reponse=moyenne_coeff([12, 8, 14, 15], [2, 4, 1, 3])
print(f"Moyenne Coefficientée = {reponse}")