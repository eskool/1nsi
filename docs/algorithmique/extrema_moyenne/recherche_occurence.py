def recherche_occurrence(el, l:list)->list:
    '''renvoie la liste (éventuellement vide)
    des indices de el dans l'''
    liste_indice = []
    for i in range(len(l)):
        if l[i] == el:
            liste_indice.append(i)
    return liste_indice

reponse = recherche_occurrence(5, [12,5,8,14,3,5,15])
print(f"Occurences = {reponse}")