# 1NSI : Javascript - Programmation Événementielle & IHM

## Événements & IHM

Les <bred>événements</bred> :fr: / <bred>events</bred> :gb: sont des **réactions** à des **actions** entre l'Humain et la Machine/page web, qui vont donner lieu à une interactivité. Le **DOM - Document Object Model** joue donc le rôle d'une <bred>Interface Homme Machine (IHM)</bred> pour les pages webs.

<env>Des Exemples d'Événements</env>

* un **clic** sur un noeud
* un **survol** d'un noeud
* la **sortie** d'un noeud
* le **focus** d'un noeud
* l'**appui** (keydown) sur une touche, etc..
* etc..

<env>Des Exemples Concrets d'Événements</env>

* L'utilisateur clique avec la souris sur un certain élément où en place le curseur sur un certain élément.
* L'utilisateur appuie sur une touche du clavier.
* L'utilisateur redimensionne ou ferme la fenêtre du navigateur.
* Une page web finissant de se charger.
* Un formulaire en cours de soumission
* Une vidéo en cours de lecture, en pause ou en fin de lecture.
* Une erreur qui survient.

## Programmation Événementielle

La **détection** et la **gestion** des **événements** est gérée par le DOM, selon un mode/paradigme de programmation appelé la programmation événementielle.

!!! def "Programmation Événementielle"
    Javascript est un langage de <bred>Programmation Événementielle</bred> (qui permet la gestion d'**événements**)

!!! def "fonction de Callback"
    Le principe de la programmation événementielle en javascript, est le suivant:

    * Lorsqu'un événement est déclenché sur un noeud
    * une certaine fonction appelée fonction de <bred>Callback</bred> :gb:, ou juste **une callback**, est exécutée

## Propogation des Événements

!!! pte "Propagation d'un Événement"
    * Un événement est **par défaut** *propagé* vers "*le haut*" (càd de proche en proche, d'élément parent en élément parent jusqu'à arriver à la racine de la page web).
    * Pour éviter cette propagation, il faut donc spécialement lui indiquer que nous le traitons, cela stoppera sa propagation vers l'élement parent (récursivement).

