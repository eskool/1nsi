# TD MathJax : Écrire des Mathématiques dans une page Web

## Qu'est-ce que MathJax ?

<red>MathJax</red> est un **moteur d'affichage de formules mathématiques**, Open Source, et écrit en Javascript, permettant d'afficher des mathématiques avec les langages <red>LaTeX</red> (conseillé), ou <red>MathML</red> ou <red>AsciiMath</red>.

## Configuration / Intégration dans une page web

Le plus simple pour intégrer MathJax dans une page web, c'est-à-dire pour pouvoir écrire des formules mathématiques dans une page web, c'est d'utiliser un :gb: **CDN - Content Delivery Network** / :fr: **Réseau de Livraison de Contenu** (en direct), càd d'obtenir le code source de MathJax en le téléchargeant directement depuis un serveur (faisant partie du réseau de livraison).

Voici un :gb: **MWE - Minimal Working Example** / :fr: **Exemple Fonctionnel Minimal** pour pouvoir écrire du LaTeX avec les notations `$` et/ou `$$`, usuelles en LaTeX.

La configuration se fait en deux temps :

* Dans le fichier `index.html` : Ajouter les balises `<script>` ci-dessous dans la balise `<head>` de chaque fichier `index.html` dans lequel vous souhaitez écrire des mathématiques.
* Ajout d'un fichier `MathJax.js` : Le placer à côté du fichier `index.html` (en adaptant éventuellement le chemin du fichier `MathJax.js` si vos fichiers `.html` sont dans des sous-dossiers)

=== "index.html"

    ```html
    <!DOCTYPE html>
    <html>
    <head>
    <title>MathJax TeX Test Page</title>
    <script src="MathJax.js"></script>
    <script src="https://polyfill.io/v3/polyfill.min.js?features=es6"></script>
    <script type="text/javascript" id="MathJax-script" async
    src="https://cdn.jsdelivr.net/npm/mathjax@3.2.2/es5/tex-chtml.js">
    </script>
    </head>
    <body>
    <p>
        Lorsque $a \ne 0$, et $\Delta = b^2-4ac \gt 0$, alors Il existe deux solutions à l'équation \(ax^2 + bx + c = 0\) qui sont
        $$x_1 = {\dfrac {-b - \sqrt{\Delta} }{2a} }$$ et $$x_2 = {\dfrac {-b + \sqrt{\Delta} }{2a} }$$
    </p>
    </body>
    </html>
    ```

=== "MathJax.js"

    ```javascript
    MathJax = {
        tex: {
            inlineMath: [['$', '$'], ['\\(', '\\)']],
            displayMath: [['$$', '$$'], ["\\[", "\\]"]],
            },
        svg: {
            fontCache: 'global'
        }
    };
    ```

=== "Rendu"
    Lorsque $a \ne 0$, et $\Delta = b^2-4ac \gt 0$, alors Il existe deux solutions à l'équation \(ax^2 + bx + c = 0\) qui sont  

    $$x_1 = {\dfrac {-b - \sqrt{\Delta} }{2a} }$$

    et  

    $$x_2 = {\dfrac {-b + \sqrt{\Delta} }{2a} }$$

Pour Télécharger les deux fichiers `index.html` et `MathJax.js` de démonstration (au format `zip`) : $\rightarrow$ [Cliquer ici](mathjax.zip)

## Utilisation

Vous pouvez alors taper une formule LaTeX de deux manières différentes appelées des **modes** (d'affichage) :

* Le mode **inline** est utilisé pour écrire des formules au milieu d'une phrase (un seul dollar) :  
`$\sqrt 2$`
* Le mode **display** force la formule mathématique à être placée toute seule sur une ligne séparée (deux dollars) : `$$\sqrt 2$$`

| Formule LaTeX | Rendu |
|:-:|:-:|
| `$\sqrt 2$` | $\sqrt 2$ |
| `$x^2$` | $x^2$ |
| `$\frac{2}{3}$` | $\frac {2}{3}$ |
| `$\dfrac{2}{3}$` | $\dfrac {2}{3}$ |
| `$\delta$` | $\delta$ |
| `$\Delta$` | $\Delta$ |


## Références

* Page Officielle de MathJax.js : https://www.mathjax.org/
* Formulaires LaTeX :
    * [Symboles Mathématiques usuels en LaTeX(Rice University)](https://www.cmor-faculty.rice.edu/~heinken/latex/symbols.pdf)
    * [Ecrire des Mathématiques (Wikibook)](https://fr.wikibooks.org/wiki/LaTeX/%C3%89crire_des_math%C3%A9matiques)
    * [Formules TeX (Wikipedia)](https://fr.wikipedia.org/wiki/Aide:Formules_TeX)