# 1NSI : Javascript<br/>Instructions Conditionnelles `if` - `else`

## Opérateurs de Comparaison

<center>

| Opérateur | Exemple.s | Signification | 
|:-:|:-:|:-:|
| `<` | `2<3` renvoie `true`<br/>`4<3` renvoie `false` |  strictement inférieur à |
| `<=` | `4<=5` renvoie `true`<br/>`7<=2` renvoie `false` | inférieur ou égal à |
| `>` | `5>4` renvoie `true`<br/>`4>6` renvoie `false` |  strictement supérieur à |
| `>=` | `9>=6` renvoie `true`<br/>`4>=8` renvoie `false` | supérieur ou égal à |
| `==` | `2==6/3` renvoie `true`<br/>`2==2.0` renvoie `true`<br/>`1=="1"` renvoie `true`<br/>`0==""` renvoie `true`<br/>`0==false` renvoie `true`<br/>`"0"==false` renvoie `true`<br/>`"0"==""` renvoie `false`<br/>`2==3` renvoie `false` | égal à<br/><bred>Opérateur de Comparaison/Égalité Abstraite</bred><br/>L'Opérateur `==` <ul><li><b>L'opérateur `==` convertit</b> les données vers un <b>même type</b> de données</li><li><b>PUIS</b>, compare les valeurs</li></ul> |
| `!=` | `2!=3` renvoie `true`<br/>`2!=2.0` renvoie `false`<br/>`2!=6/3` renvoie `false` | non (abstraitement) égal à<br/>(différent)<br/>renvoie `true` lorsque `==` renvoie `false`<br/> et réciproquement |
| `===` | `2===6/3` renvoie `true`<br/>`2==2.0` renvoie `true`<br/>`2==3` renvoie `false` | égal à<br/><bred>Opérateur de Comparaison/Égalité Stricte</bred><br/>L'Opérateur `===` <b>NE CONVERTIT AUCUNE DONNÉE</b> pour comparer.<br/>L'opérateur `===` renvoie `true` Lorsque:<br/> <ul><li>les valeurs sont égales</li><li>ET les types de données sont identiques</li></ul> |
| `!==` | `2!=3` renvoie `true`<br/>`2!=2.0` renvoie `false`<br/>`2!=6/3` renvoie `false` | non (strictement) égal à<br/>(différent)<br/>renvoie `true` lorsque `==` renvoie `false`<br/> et réciproquement |

</center>

## Instruction Conditionnelle `if`

```js linenums="0"
let i=0;
if (i==0) {
  console.log("i vaut zéro");
}
```

## Instruction Conditionnelle `if .. else ..`

```js linenums="0"
let i=0;
if (i == 0) {
  console.log("i vaut zéro");
} else {
  console.log("i ne vaut pas zéro");
}
```

## Instruction Conditionnelle `if .. else if ..`

```js linenums="0"
let i=0;
if (i==0) {
  console.log("i vaut zéro");
} else if (i==1) {
  console.log("i vaut un");
} else {
  console.log("i ne vaut ni zéro ni un");
}
```

