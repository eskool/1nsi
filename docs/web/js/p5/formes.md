# 1NSI : p5.js - Formes de Base

## `line()` / `stroke()` / `strokeWeight()`

`line(x1, y1, x2, y2);` trace une **ligne** / un **trait** du point de coordonnées `(x1, y1)` au point de coordonnées `(x2, y2)`

Et quelques fonctions complémentaires:

* `stroke(r,g,b);` définit une **couleur** (r,g,b) pour le trait.
* `strokeWeight(n);` définit l'épaisseur du trait (en nombre de pixels)

```js linenums="0"
function setup() {
  createCanvas(400, 400);
  background(50);
  stroke(255, 255, 255);
  strokeWeight(3);
}

function draw() {
  line(0,200,400,200);
}
```

<iframe src="https://editor.p5js.org/rod2ik/full/PzNSUTgZn" width="100%" height="600"></iframe>

## `rect()` / `fill()`

`rect(x1, y1, largeur, hauteur);` trace un **rectangle** dont `(x1, y1)` est le point **en haut à gauche** du rectangle, et tel que la `largeur` et la `hauteur` sont données.

Et quelques fonctions complémentaires :

* `stroke(r,g,b);` définit une **couleur** (r,g,b) pour le bord du rectangle
* `strokeWeight(n);` définit l'épaisseur du bord du rectangle (en nombre de pixels)
* `fill(couleur);` définit la couleur du fond intérieur pour le rectangle

```js linenums="0"
function setup() {
  createCanvas(400, 400);
  background(50);
  stroke(255, 255, 255);
  strokeWeight(3);
}

function draw() {
  fill(255,0,0);
  rect(50,50,200,100);
}
```

<iframe src="https://editor.p5js.org/rod2ik/full/JcD3A7rpt" width="100%" height="600"></iframe>

## `ellipse()` / `fill()`

### Définition

`ellipse(xc, yc, largeur, hauteur);` trace un **rectangle** dont `(xc, yc)` est le centre de l'ellipse (un cercle *écrasé*), et tel que la `largeur` et la `hauteur` sont données.

Et quelques fonctions complémentaires :

* `stroke(r,g,b);` définit une **couleur** (r,g,b) pour le bord de l'ellipse
* `strokeWeight(n);` définit l'épaisseur du bord de l'ellipse (en nombre de pixels)
* `fill(couleur);` définit la couleur du fond intérieur pour l'ellipse

```js linenums="0"
function setup() {
  createCanvas(400, 400);
  background(50);
  stroke(255, 255, 255);
  strokeWeight(3);
}

function draw() {
  fill(255,0,0);
  ellipse(200,100,200,100);
}
```

<iframe src="https://editor.p5js.org/rod2ik/full/seizB48pI" width="100%" height="600"></iframe>

### Une Animation avec une Balle

```js linenums="0"
let x = 0;

function setup() {
  createCanvas(400, 400);
  background(50); 
}

function draw() {
  ellipse(x, 200, 20, 20);
  x = x + 5;
}
```

<iframe src="https://editor.p5js.org/rod2ik/full/Gcd9cc0XU" width="100%" height="600"></iframe>

!!! ex "Personnaliser l'animation"
    1°) Ne pas afficher la trace de la balle...(Comment ?)  
    2°) Augmenter/Diminuer l'incrémentation (de $5$ à ..)  
    3°) Faire rebondir la "*balle*" lorsqu'elle est arrivée sur le bord droit  
    4°) Faire rebondir la "*balle*" lorsqu'elle est arrivée sur le bord gauche  
    5°) On souhaite déplacer la balle autrement qu'horizontalement.    
    On fixe pour cela deux variables `let dx=+2;` et `let dy=+3;`.
    Déplacer la balle en répétant ce déplacement initial.  
    6°) Faire rebondir la balle comme en vrai, càd de sorte que son futur sens de déplacement après le rebond soit symétrique par rapport à la perpendiculaire au bord.  
    7°) Créer une fonction `nbAlea(a,b)` qui renvoie un nombre entier aléatoire entre `a` et `b`. On pourra utiliser les deux fonctions natives suivantes en javascript:  

    * `Math.floor(x)` qui renvoie la partie entière du nombre (réel) `x`
    * `Math.random()` qui renvoie un nombre aléatoire (réel) entre `0` et `1`  
    8°) Créer une fonction `initDeplacement(vitesse)` qui initialise de manière aléatoire de sorte que:

    * la variable aléatoire `dx` est comprise entre `-vitesse` et `+vitesse`
    * `dy` est calculée par :
        * son `signe` (`+1` ou `-1`) est calculé aléatoirement, de manière équiprobable
        * sa valeur (absolue) `dy` est calculée par la formule: `dy = signe*Math.sqrt(vitesse**2-(dx)**2)`  
    9°) La balle devrait être initialisée avec un sens de parcours initial aléatoire. Puis rebondir sur les murs


## `circle()`

`circle(x , y, d)` trace le cercle de centre `(x,y)` et de diamètre `d`

<iframe src="https://editor.p5js.org/rod2ik/full/iD9JA2Prt"></iframe>

## `arc()`

`arc(x, y, largeur, hauteur, angle_départ, angle_arrivée, [mode], [détail])` trace un **arc d'ellipse** tel que (un arc de cercle est obtenu comme cas particulier) :

* `x` et `y` sont (respectiement) les abscisses et ordonnées du centre de l'ellipse à laquelle appartient l'arc
* `largeur` désigne la largeur de l'ellipse
* `hauteur` désigne la hauteur de l'ellipse
* `angle_départ` désigne la valeur de départ de l'angle (en radians)
    * L'angle `0 radians` correspond à un axe des $x$ horizontal, passant par l'origine du repère `(x,y)` et orienté de gauche à droite
    * un angle **positif** correspond à une orientation dans le sens des aiguilles d'une montre
* `angle_arrivée` désigne la valeur d'arrivée de l'angle (en radians)
* `mode` est un paramètre optionnel, constant, qui joue sur la manière de tracer l'arc :
    * `CHORD`
    * `PIE`
    * `OPEN`
* `détail` est un paramètre optionnel, qui est un nombre entier utile uniquement pour le mode WebGL. Il sert à spécifier le nombre de sommets servant à déterminer le périmètre de l'arc. Valeur par défaut : $25$ (les entiers supérieurs $50$ provoquent un tracé non continu de l'arc)

## D'autres Formes

CF *Shape* sur la [page de Référence](https://p5js.org/reference/)
