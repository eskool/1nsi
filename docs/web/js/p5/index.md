# 1NSI : Introduction à p5.js

<bred>p5.js</bred> est une librairie javascript (intuitivement, ce sont des fonctionnalités/fonctions supplémentaires au javascript) orientée codage créatif, càd pour les artistes, designers, éducateurs, débutants (en JS). La libriaire *p5.js* permet notamment de créer simplement des figures/graphiques animés, en particulier de petits jeux vidéos.

Il n'est pas nécessaire d'installer quoi que ce soit, car il existe un éditeur *p5.js* en ligne : https://editor.p5js.org/

* La Documentation Générale :gb: est [ici](https://p5js.org/) : https://p5js.org/
* La Documentation de Référence :gb: est [ici](https://p5js.org/reference/) : https://p5js.org/reference/

## Les fonctions `setup()` et `draw()`

* La fonction `setup()` **inititalise** le document/canevas : cette fonction n'est exécutée qu'**une seule fois**
* La fonction `draw()` s'**exécute régulièrement**, et permet d'animer le document: cette fonction est **exécutée périodiquement (selon un certain framerate), à l'infini**, jusqu'à ce qu'elle soit arrêtée (bouton carré)

## Canevas & Fonds d'Écran

```js linenums="0"
function setup() {
  // cette fonction n'est exécutée qu'une seule fois
  createCanvas(400, 400);
  background(50);
}

function draw() {
  // cette fonction boucle à l'infini, jusqu'à ce qu'elle soit arrêtée (bouton carré)
}
```

où 

* `createCanvas(Largeur, Longueur)` crée un **canevas** :fr: / **canvas** :gb: (sorte de tableau sur lequel on peut écrire et animer des objets)
* `background(couleur)` définit cette `couleur` comme **couleur de fond**, où la syntaxe est à choisir parmi :
    * `background(r,g,b)` où `r`, `g`, `b` sont les composantes RGB -  Rouge, Vert, Bleu de la couleur, chacune d'enttre elles étant un nombre compris entre $0$ et $255$.
    * `background(c)` signifie `background (c,c,c)` donc un dégradé de gris
    * `background("#ABCDEF")` où `"#ABCDEF"` désigne une couleur *HTML* en Hexadécimal

## Une Première Animation

```js linenums="0"
let x = 0;

function setup() {
  createCanvas(400, 400);
  background(50); 
}

function draw() {
  ellipse(x, 200, 20, 20);
  x = x + 5;
}
```

<iframe src="https://editor.p5js.org/rod2ik/full/Gcd9cc0XU" width="100%" height="600"></iframe>

!!! ex "Personnaliser l'animation"
    1°) Augmenter/Diminuer l'incrémentation (de $1$ à ..)
    2°) Faire rebondir la "*balle*" lorsqu'elle est arrivée sur le bord droit
    3°) Faire rebondir la "*balle*" lorsqu'elle est arrivée sur le bord gauche
    4°) On souhaite déplacer la balle autrement qu'horizontalement.  
    On fixe pour cela deux variables `let dx=+2;` et `let dy=+3;`.
    Déplacer la balle en répétant ce déplacement initial.
    5°) Faire rebondir la balle comme en vrai, càd de sorte que son futur sens de déplacement après le rebond soit symétrique par rapport à la perpendiculaire au bord.
    6°) Créer une fonction `nbAlea(a,b)` qui renvoie un nombre entier aléatoire entre `a` et `b`. On pourra utiliser les deux fonctions natives suivantes en javascript:  

    * `Math.floor(x)` qui renvoie la partie entière du nombre (réel) `x`
    * `Math.random()` qui renvoie un nombre aléatoire (réel) entre `0` et `1`
    7°) Créer une fonction `initDeplacement(vitesse)` qui initialise de manière aléatoire de sorte que:

    * la variable aléatoire `dx` est comprise entre `-vitesse` et `+vitesse`
    * `dy` est calculée par :
        * son `signe` (`+1` ou `-1`) est calculé aléatoirement, de manière équiprobable
        * sa valeur (absolue) `dy` est calculée par la formule: `dy = signe*Math.sqrt(vitesse**2-(dx)**2)`
    8°) La balle devrait être initialisée avec un sens de parcours initial aléatoire. Puis rebondir sur les murs

## Une deuxième Animation avec la Souris : `mouseX` & `mouseY`

```js linenums="0"
function setup() {
  createCanvas(400, 400);
  background(50); 
}

function draw() {
  ellipse(mouseX, mouseY, 20, 20);
}
```

Survoler le canevas ci-dessous avec la souris, en la déplaçant à l'intérieur du canevas

<iframe src="https://editor.p5js.org/rod2ik/full/ffVA-d6vn" width="100%" height="600"></iframe>

!!! ex "Raquette de Pong"
    1. Créer une raquette de Pong rectangulaire, blanche, de taille 15px en largeur, sur 70px en hauteur  
    2. Animer la raquette de sorte que celle-ci suive le mouvement de la souris (sur une même verticale: par ex. à droite du canevas)  
    3. Ajouter une balle qui se déplace aléatoirement  
    4. Programmer la détection de la balle sur la raquette  
    5. Faire rebondir la balle dans ce cas


## Répère Informatique

* **Le point de coordonnées $(0,0)$ se trouve en haut à gauche**
* L'axe des `x` est orienté de gauche à droite
* L'axe des `y` est orienté de haut en bas

## Références & Notes

### Références

#### Wiki

* https://github.com/processing/p5.js/wiki/p5.js-overview

#### MOOCs & Channels YouTube

* [Channel YouTube de The Coding Train](https://www.youtube.com/playlist?list=PLRqwX-V7Uu6Zy51Q-x9tMWIv9cueOFTFA) :gb:
* [Le MOOC Kadenze de UCLA (Univ Californie): p5 pour les Arts Visuels](https://www.kadenze.com/courses/introduction-to-programming-for-the-visual-arts-with-p5-js/info) :gb: :es: :ru: :cn: :po: