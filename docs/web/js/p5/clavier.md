# 1NSI : p5.js - Touches du Clavier

!!! warning
    **ATTENTION** : Il faut **OBLIGATOIREMENT CLIQUER** sur les zones blanches de `p5*` ci-dessous, pour que celles-ci **aient le focus**, et donc que l'appui sur les touches correspondantes soit bien détecté.

## variable `keyIsPressed`

La variable booléenne `keyIsPressed` peut prendre deux valeurs :

* `true` lorsque n'importe quelle touche est en train d'être appuyée (aussi longtemps qu'elle l'est)
* `false` sinon

## `keyPressed()`

La fonction `keyPressed()` est appelée **à chaque fois qu'une touche est appuyée** : à vous de renseigner dans cette fonction, ce que vous souhaitez faire lorsque cela se produit. Pour ce faire, il est fréquent de commencer par détecter quelle touche précisément a été appuyée, pour réagir différemment selon que ce soit une touche ou une autre qui ait été appuyée. On peut détecter de quelle il s'agit grâce à:

* la variable `key`, qui contient une chaîne de caractère ou bien
* la variable `keyCode` qui contient un nombre entier (le code ASCII du caractère appuyé)

```js linenums="0"
let value = 0;

function setup() {
  createCanvas(400, 400);
}

function draw() {
  fill(value);
  rect(25, 25, 50, 50);
}

function keyPressed() {
  if (key === "ArrowLeft") {
    value = "red";
  } else if (key==="ArrowRight") {
    value = "blue";
  } else if (key===" ") {
    value = "green";
  }
}
```

<iframe src="https://editor.p5js.org/rod2ik/full/WUOXAxaqH" width="100%" height="600"></iframe>

## la variable `key`

La variable `key` contient une **chaîne de caractère** correspondant à la touche pressée :

* Soit la touche est un **caractère imprimable** (lettre, nombre, ou autre caractère affichable du clavier `$ù=&é"` etc..) : la chaîne `key` contient alors cet unique caractère
* Soit la touche du clavier porte un **nom spécial** car le caractère n'est pas affichable: `Delete`, `Enter`, etc.. cf tableau ci-dessous

<center>

| Touche | key |
|:-:|:-:|
| Backspace <br/>(Retour Arrière $\leftarrow$) | `"Backspace"` |
| Suppr | `"Delete"` |
| Entrée | `"Enter"` |
| Retour à la Ligne | `"Return"` |
| Tab <br/>(Tabulation) | `"Tab"` |
| Échap / Esc <br/>(Échappe / Escape) | `"Escape"` |
| Shift/Maj | `"Shift"` |
| Contrôle | `"Control"` |
| Windows | `"OS"` |
| Alt | `"Alt"` |
| AltGr | `"AltGraph"` |
| Flèche Haut | `"ArrowUp"` |
| Flèche Bas | `"ArrowDown"` |
| Flèche Gauche | `"ArrowLeft"` |
| Flèche Droite | `"ArrowRight"` |

</center>

## Variable `keyCode`

La varaible `keyCode` contient un nombre entier correspondant à la touche appuyée.
Ce nombre entier est en fait le **[code ASCII](https://fr.wikipedia.org/wiki/American_Standard_Code_for_Information_Interchange)** (du caractère) de la touche appuyée.

Plusieurs manières pour connaître ce keyCode (nombre entier) :

* Ou bien vous lisez une [table ASCII](https://fr.wikipedia.org/wiki/American_Standard_Code_for_Information_Interchange) par exemple sur internet
* Ou bien, Le site https://keycode.info/ permet de connaître le keycode de n'importe quelle touche online et en direct
* Ou bien, certaines touches (mais pas toutes) disposent d'une variable en majuscules (c'est une constante) facile à mémoriser :

<center>

| Touche | keyCode |
|:-:|:-:|
| Backspace <br/>(Retour Arrière $\leftarrow$) | `BACKSPACE` |
| Suppr | `DELETE` |
| Entrée | `ENTER` |
| Retour à la Ligne | `RETURN` |
| Tab <br/>(Tabulation) | `TAB` |
| Échap / Esc <br/>(Échappe / Escape) | `ESCAPE` |
| Shift/Maj | `SHIFT` |
| Contrôle | `CONTROL` |
| Windows | `OPTION` |
| Alt | `ALT` |
| Flèche Haut | `UP_ARROW` |
| Flèche Bas | `DOWN_ARROW` |
| Flèche Gauche | `LEFT_ARROW` |

</center>

Par exemple, pour la **touche espace** l'entier corresonpdant est `32`, mais il n'y a pas de nom spécial.

<iframe src="https://editor.p5js.org/rod2ik/full/1HftCoEFc" width="100%" height="600"></iframe>

