# 1NSI : p5.js - Textes


## `textSize()`

`textSize(taille)` fixe la taille de la police de caractère que vous allez utiliser

## `text()`

`text(uneChaine, x, y)` affiche dans le canevas, la chaîne de caractère `uneChaine` à la position `(x, y)` en pixels.

Exemples:

<iframe src="https://editor.p5js.org/rod2ik/full/geBxt2BBJ" width="100%" height="600"></iframe>