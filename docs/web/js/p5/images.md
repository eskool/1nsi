# 1NSI : p5.js - Images

## Procédure pour Ajouter une Image au Canevas

Ajouter une image au canevas peut se faire en $3$ étapes:

1°) Je **téléverse** l'image souhaitée dans https://editor.p5js.org (un nouveau fichier doit être visible dans la partie editeur de fichiers (à gauche). Idéalement, l'image doit être déjà à la bonne taille, pour simplifier.
Par exemple: utiliser l'une de ces deux images ci-dessous, qui ont une taille de $64$x$40$ px

<center>

![Image Logo DVD](./img/logo-dvd.png){style="margin-right:1em;"}
![Image Logo DVD](./img/logo-dvd-blanc.png)

</center>

2°) J'**initialise** une variable `img` avec `loadImage()`, pour pouvoir la manipuler dans p5.js  
3°) J'**insère** l'image dans le canevas avec `image(img, x, y)`

## Téléverser un fichier image sur p5.js webeditor

* Étendre le menu Latéral de gauche (en cliquant sur la flèche *vers la droite*) :

![Etendre](./img/etendre-menu-lateral.png){.center}

* Étendre le menu vertical, puis choisir **Téléverser** votre image :

![Etendre](./img/televerser-image.png){.center}

* Vérifier que l'image a bien été téléversée/téléchargée dans https://editor.p5js.org (ici `logo-dvd.png`) :

![Image Téléversée dans editor.p5js.org](./img/image-televersee.png){.center}


## Initialiser une variable `img` avec `loadImage()`, puis insérer l'image avec `image(img,x,y)`

<iframe src="https://editor.p5js.org/rod2ik/full/lwzknYkB6" width="100%" height="600"></iframe>

où :

* `img = loadImage(chaineContenantNomDeLimage);` initialise la variable `img` pour manipuler l'image dans le code javascript
* `image(img, x, y);` affiche l'image dont le nom est stocké dans la variable `img`, dont le coin supérieur-gauche est le point de coordonnées `(x,y)`

!!! ex "Animer et Faire rebondir l'Image DVD"
    Animer et Faire rebondir l'image *DVD* sur les côtés du canevas (au lieu d'un rectangle)