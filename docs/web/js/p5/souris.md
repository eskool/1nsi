# 1NSI : p5.js

## `mouseX` et `mouseY`

Les variables `mouseX` et `mouseY` contiennent en permance les cooordonnées en (X,Y)  de la souris

## Événements Souris

### Variable Booléenne `mouseIsPressed`

La variable booléenne `mouseIsPressed` peut prendre deux valeurs:

* `true` lorsqu'un clic de souris a été pressé
* `false` sinon

### Variable `mouseButton` 

La variable `mousePressed` contient $3$ valeurs possibles:

* `LEFT`
* `RIGHT`
* `CENTER`

### fonction `mouseClicked()`

La fonction `mouseClicked()` est appelée **à chaque fois qu'un clic (gauche)** de souris a été **pressé ET relâché**.

### fonction `mousePressed()`

La fonction `mousePressed()` est appelée **à chaque fois qu'un clic** de souris a été **pressé**.

<iframe src="https://editor.p5js.org/rod2ik/full/KiHV5UaOE" width="100%" height="600"></iframe>

