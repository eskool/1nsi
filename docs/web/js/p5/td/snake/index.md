# 1NSI : p5.js - TD Snake

On part d'un canevas de taille `(600, 600)` avec un fond `background(50)`

1. Créer un nouveau fichier `snake.js` (s minuscule) contenant

```js linenums="0"
class Snake { // S MAJUSCULE
  constructor() {
    this.x=0;
    this.y=0;
    this.vx = 1;
    this.vy = 0;
  }
  
  update() {
    this.x = this.x + this.vx;
    this.y = this.y + this.vy;

  }
  
  show() {
    fill(255);
    rect(this.x, this.y, 10, 10)
  } 
}
```

2. Ajouter la ligne suivante dans le fichier `index.html`

```html linenums="0"
<body>
  <script src="snake.js"></script>
  <script src="sketch.js"></script>
</body>
```