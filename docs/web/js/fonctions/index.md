# 1NSI : Javascript - Fonctions

Une **fonction** est un sous-programme qui peut être apelée par du code extérieur à la fonction (ou du code intérieur dans le cas de la récursivité).

## Syntaxe Générale

On utilise le mot-clé `function` pour définir une nouvelle fonction [^2]. Les fonctions sont des objets de première classe en javascript: elles peuvent être manipulées et échangées, qu'elles peuvent avoir:

* des **propriétés** : des **variables** contenant des données, liées spécifiquement à cette fonction
* des **méthodes** : des **fonctions** liées spécifiquement à cette fonction

 Leur type de données est `function`.

```js linenums="0"
function maFonction([param1 [, param2, ..]]) {
  // corps de la fonction
  //return ...  FACULTATIF
}
```

<env>Exemple</env>

```js linenums="0"
function carre(nb) {
  return nb**2;
}
```

!!! pte "Paramètres: Passage par Valeur vs passage par Référence"
    * Les paramètres **primitifs** (comme les nombres) sont passés à la fonction **par valeur** : càd que si la fonction change la valeur du paramètre, alors cela n'aura pas d'impact au niveau global ou au niveau de ce qui a appelé la fonction.
    * Les paramètres **non primitifs** (comme un Array ou un objet défini par l'utilisateur) sont passés à la fonction **par référence** : càd que si la fonction change les propriétés de cet objet, alors ces changements seront visibles en dehors de la fonction. 


## Fonctions Anonymes

Le **nom** d'une fonction n'est même pas obligatoire: on peut créer des <bred>fonctions anonymes</bred> (sans nom) :

```js linenums="0"
function (x) {
  return 2+x;
}
```

On pourrait se demander à quoi sert une fonction sans nom? donc on ne pourrait même pas l'appeler..
Deux réponses (très) partielles:

* **D'une part** : On **peut** stocker des fonctions anonymes dans des variables... et donc les appeler par la suite, par le nom des variables dans lesquelles elles ont été stockées.. 
    ```js linenums="0"
    let a = function (x) {
      return 2+x;
    };
    console.log(a(3));
    ```
* **D'autre part** : dans certaines parties du code, il arrive qu'on ne souhaite exécuter une fonction qu'une seule fois. Comme le corps de cette fonction est censé ne plus être réexécuté plus tard/ailleurs, on ne se donne même pas la peine d'inventer un nom pour celle-ci. Ce phénomène est plus fréquent qu'on ne pourrait le penser en javascript. CF par exemple la syntaxe de la boucle **forEach()** depuis un **Array**.

## Fonctions Flèchées ou Arrow Fonctions

Depuis la version ECMAScript$6$ (ES$6$) en $2015$, javascript autorise la notation des <bred>Fonctions Fléchées</bred>[^2] :fr: / <bred>Arrow Functions</bred>, pour définir des fonctions.

* Fonction Fléchée Anonyme avec un argument:
    ```js linenums="0"
    let f = (x) => {
      return 2+x;
    }
    ```
    Mathématiquement, on peut la voir comme la fonction `f`, qui à `x` associe `2+x`.  
    Ce que les mathématiciens notent $f: x \mapsto 2+x$
* Fonction Fléchée Sans arguments:
    ```js linenums="0"
    let hey = () => {
      console.log("Bonjour");
    }
    ```


## Références et Notes

[^1]: [Mozilla MDN Web Docs : Fonctions](https://developer.mozilla.org/fr/docs/Web/JavaScript/Guide/Functions)
[^2]: [Mozilla MDN Web Docs : Fonctions Fléchées](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Functions/Arrow_functions)