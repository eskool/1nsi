# 1NSI : Javascript - Variables & Constantes

## Définition

En javascript, comme dans tout langage de programmation, les variables permettent de stocker en mémoire des données. Il existe différents types de données primitifs en Javascript.

!!! def "Variable"
    Une <bred>variable</bred> est une référence pointant vers une zone mémoire qui contient la donnée.

## Le mot-clé `var`

La création d'une variable et l'assignation/affectation d'une valeur/donnée à cette variable se fait en **deux étapes** en Javascript:

* On **déclare** la variable à javascript, avec le mot-clé `var`, PUIS
* on **assigne** la valeur à la variable

```js linenums="0"
var a; // étape 1 : déclaration de a. Sur cette ligne, a est toujours 'undefined'
a=2;   // étape 2 : assignation de 2 dans a
```

est équivalent à (plus rapide: les deux étapes d'un coup)

```js linenums="0"
var b=2;
```

!!! hint "A Savoir"
    La première étape est indépendante de la deuxième.. ce qui peut mener à des variables déclarées (connues du javascript), mais non assignées (aucune valeur dedans) : ce type de variables contient `undefined`

!!! pte "Portée de `var`"
    L'instruction `var` du javascript a une **portée globale** : toute variable JS déclarée avec `var` existera :

    * dans tout autre bloc de code
    * à l'intérieur ou à l'extérieur de toute autre fonction

## Le mot-clé `let`

Depuis **ECMAScript** $6$ (ES$6$, en $2015$) , Il existe un autre mot-clé pour déclarer une variable en Javascript, c'est le mot-clé `let`.

```js linenums="0"
let c; // étape 1 : déclaration de a. Sur cette ligne, a est toujours 'undefined'
c=2;   // étape 2 : assignation de 2 dans a
```

est équivalent à (plus rapide: les deux étapes d'un coup) :

```js linenums="0"
let d=2;
```

!!! pte "Portée de `let`"
    L'instruction `let` admet une **portée locale** : du **bloc courant** de code uniquement.

## Le mot-clé `const`

Le mot-clé `const` permet de définir des constantes.

```js linenums="0"
const e=2;
```

Toute tentative de réaffectation/réassignation future de la constante `e` se soldera par une erreur :

```console linenums="0"
Uncaught TypeError: invalid assignment to const 'e'
```