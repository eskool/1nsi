# 1NSI : Javascript - `null` vs `undefined`

* `undefined` :
    * `undefined` est un **type de données**
    * `undefined` est une **propriété** de l'objet global 
* `null` :
    * `null` est une *valeur spéciale* du type de données `object`
    ```js linenums="0"
    >> typeof(null);
    <- "object"
    ```
    * `null` modélise le vide : il est souvent utilisé en valeur de retour lorsqu'un objet est attendu mais qu'aucun objet ne convient.
    * `null` n'est PAS un type de données
    * `null` est un **littéral** (contrairement à `undefined` qui est une propriété de l'objet global)

<center>

| Test | Résultat |
|:-:|:-:|
| `null==""` | `false` (normal) |
| `undefined==""` | `false` (normal) |
| `null==0` | `false` (normal) |
| `undefined==0` | `false` (normal) |
| `null==undefined` | `true` (inattendu?) |
| `null===undefined` | `false` (normal) |
| `null=={}` | `false` (inattendu?) |
| `null==[]` | `false` (inattendu?) |

</center>