# 1NSI : Javascript `typeof`

L'instruction `typeof(a)` renvoie le **type de données** contenues dans la variable `a`

Quelques types de données primitifs en Javascript:

* `boolean`
* `undefined`
* `number`
* `string`
