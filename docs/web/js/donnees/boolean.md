# 1NSI : Javasacript - Type de Données `Boolean` / Booléen

Les deux valeurs booléennes en Javascript sont `true` et `false` qui sont notées **en minuscules**.

## `true`

```js
let a = true;
```

## `false`

```js
let a = false;
```

