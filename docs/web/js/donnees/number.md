# 1NSI : Javascript - Type de Données `number` / Nombre & `NaN`

Référence: https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Global_Objects/Number

## Le type de données `number`

`number` est un type de données primitif, qui est **numérique**.

Le type JavaScript `number` utilise une **représentation binaire à précision double sur 64 bits** telle que décrite par le standard [IEEE 754](https://fr.wikipedia.org/wiki/IEEE_754). Il permet de représenter des nombres:

* de presque zéro ($\approx 10^{-308}$ environ)
* à $\approx 10^{+308}$ environ

## Le mot-clé `NaN`

### Définition de `NaN`

**NaN - Not a Number** est un mot-clé réservé de Javascript qui indique qu'un nombre **n'est pas un nombre "*conforme*"**, au sens Javascript.
En pratique, il s'agit d'une valeur renvoyée lorsque javascript rencontre **un problème durant un calcul avec les nombres**.

!!! hint "Bizarre"
    Au sens strict, Le type de données `NaN` est `number`...
    ```js linenums="0"
    typeof NaN
    number
    ```

### `isNaN ` : Tester si un nombre est `NaN`, ou pas

```js linenums="0"
>> isNaN(5)
false
>> isNaN(NaN)
true
```

## Le mot-clé `Infinity`

`Infinity` est un mot-clé réservé de Javascript qui modélise l'**infini**.
Infinity est un type de données `number`.

```js linenums="0"
typeof Infinity
number
```