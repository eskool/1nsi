# 1NSI : Javascript -> Symbol

Un **symbole** est un type de données primitif de javascript représentant une donnée unique et inchangeable qui peut être utilisée afin de représenter des identifiants pour des propriétés d'un objet. L'objet **Symbol** est un conteneur objet implicite pour le type de données primitif symbole.