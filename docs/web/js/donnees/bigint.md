# 1NSI : Javascript - Type de Données `Bigint`

`Bigint` est un objet natif de Javascript permettant de représenter des nombres entiers supérieurs à $2^53$ (la plus grande valeur entière pouvant être représentée par le type primitif `Number`)

!!! note "Bigint est une proposition de niveau 3"
    BigInt est actuellement une proposition de niveau 3 pour la spécification ECMAScript.

    Lorsque cette proposition atteindra le niveau 4 (soit la spécification finale), BigInt sera le deuxième type natif disponible en JavaScript pour représenter des valeurs numériques.

    BigInt sera ainsi le prochain type primitif ajouté à JavaScript depuis Symbol avec ES2015
