# 1NSI : Javascript - Type de données `undefined`

## Le type de données `undefined`

En Javascript, au sens des variables, `undefined` signifie que :

* une **variable a été déclarée**
* mais qu'**aucune valeur ne lui a été assignée**

```js linenums="0"
>> let a;
<- undefined
>> console.log(a);
<- undefined
```

!!! pte
    * `undefined` modélise donc un **état d'assignation** d'une variable
    * `undefined` est néanmoins un **type de données** à part entière
    ```js linenums="0"
    >> typeof(undefined);
    <- "undefined"
    ```

<env>Remarque</env>  
Dans une **console javascript**, une syntaxe "*deux d'un coup*" (étape 1 de création, et étape 2 d'assignation) affiche `undefined`, ce qui peut sembler erroné. Que s'est-il passé? Javascript autorise effectivement la syntaxe "*deux d'un coup*", mais il n'en reste pas moins que JS réalise ces deux étapes séparément en amont: la premièr étape renvoie donc toujours `undefined`.

```js linenums="0"
>> let a=2;
<- undefined
```

## `undefined` et `typeof`

On peut tester si une variable a bien été assignée/affectée dans Javascript avec le mot-clé `typeof` :

```js linenums="0"
>> let a;
<- undefined
>> console.log(a);
<- undefined
>> typeof(a);
<- "undefined"
>> typeof(a) == "undefined";
<- true
>> typeof(a) === "undefined";
<- true
```

!!! hint "Hint / Astuce"
    On peut tester si une variable `a` a bien été déclarée, ou pas, grâce à un test de comparaison (ATTENTION: guillemets obligatoires)

    * `typeof(a) == "undefined";`
    * `typeof(a) === "undefined";`

