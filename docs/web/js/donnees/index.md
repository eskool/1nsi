# 1NSI : Javascript - Types de Données Primitifs

Tous les types, sauf les objets, définissent des valeurs **immuables** (qu'on ne peut modifier). Ainsi, contrairement au C, **les chaînes de caractères sont immuables en JavaScript**. Les valeurs immuables pour chacun de ces types sont appelées « valeurs primitives ».