# 1NSI : Javascript - Type de Données `Array` / Tableau

## Créer un Tableau Vide

```js
let a = [];
let a = Array();
let a = new Array();
```

!!! pte "Indices"
    Les arrays ont des **indices numériques**

## Ajouter un élément

Ajouter un élément en dernière position (la plus à droite) dans un Array/Tableau.

```js linenums="0"
a.push("nouvel élément");
a[1] = "un"
a[2] = "deux"
```

## Supprimer un élément

Supprimer le dernier élément (le plus à droite) de l'Array/Tableau.

```js
a.pop();
```

## La Propriété Longueur `length` d'un Tableau

```js
>> a.length;
3
```

## Appartenance à un Tableau / le mot clé `includes`

La fonction `tableau.includes(element)` renvoie:

* `true` si l'`element` est inclus dans le tablau/array
* `false` sinon

```js
a.includes("nouvel élément");
```

## Reconnaître un Array

```js
>> Array.isArray(a);
true
>> a instanceof Array
true
```
