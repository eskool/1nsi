# 1NSI : Javascript - L'objet `null`

## Définition de `null`

!!! def "`null`"
    * En Javascript, `null` est une **valeur spéciale** du type de données `object`.
    * `null` modélise le **vide** : il est souvent utilisé en valeur de retour lorsqu'un objet est attendu mais qu'aucun objet ne convient.

!!! pte
    * Le type de données de `null` est `object`.  
    * `null` est un littéral (contrairement à `undefined`, qui est une propriété de l'objet global).  
    * `null` n'est **PAS un type de données**.  
    * C'est une des **valeurs primitives** de Javascript.

```js linenums="0"
>> typeof(null);
<- "object"
```

```js linenums="0"
>> let a=null;
<- undefined
>> typeof(a);
<- "object"
```

## Comparaisons avec la valeur `null`

!!! warning "ATTENTION"
    Certaines Comparaisons ne sont pas toujours intuitives :

    <center>

    | Test | Résultat |
    |:-:|:-:|
    | `null==undefined` | `true` (inattendu?) |
    | `null===undefined` | `false` (normal) |
    | `null==""` | `false` (normal) |
    | `null==0` | `false` (normal) |

    </center>