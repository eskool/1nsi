# 1NSI : Javascript - Type de Données `object` & Notation JSON

## Type de Données `Object`

Le Type de Données `object` est un type primitif de Javascript, qui contient des données structurées. Il utilise une notation appelée <bred>JSON - JavaScript Object Notation</bred> : 

<env>Syntaxe Générale</env>

```js linenums="0"
nomObjet = {
  propriete1: valeur1,
  propriete2: valeur2,
  ...
  proprieteN1: valeurN1,
  proprieteN2: valeurN2
}
```

!!! pte "Indices"
    Les Objects ont des **indices nommés** appelés des <bred>propriétés</bred>

```js linenums="0"
// >> let personne = { prenom:"Paul", nom:"Martin", age:25 };
>> let personne = {
  prenom: "Paul", 
  nom: "Dumont", 
  age: 25
  };
>> typeof(personne);
"object"
```

## Accès aux Éléments (`propriétés`:`valeurs`) d'un Objet `object`

```js linenums="0"
>> personne.prenom;
"Paul"
>> personne["prenom"];
"Paul"
>> personne.age;
25
>> personne["age"];
25
```

## Modification d'un Objet `object`

```js linenums="0"
>> personne["age"] = 27;
>> personne["age"];
27
```

## Ajout d'une nouvelle propriété dans un Objet `object`

```js linenums="0"
>> personne.ville = "Marseille";
"Marseille"
>> personne["ville"] = "Marseille";
>> personne["ville"];
"Marseille"
```















