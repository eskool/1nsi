# 1NSI : Javascript `getElementsByClassName()`

La méthode `getElementsByClassName()` est accessible depuis le point d'entrée du DOM: `document`.

<env>Syntaxe</env> `document.getElementsByClassName("bleu");`

!!! def "`getElementsByClassName()` renvoie un type HTMLCollection"
    La méthode `getElementsByClassName("bleu");` renvoie TOUS les Éléments HTML (donc toutes les balises) du DOM, disposant d'une classe `class="bleu"`. Il peut y en avoir plusieurs (d'où le `s` dans getElement<bred>s</bred>ByClassName). Javascript renvoie un résultat sous la forme d'un type de données primitif appelé une <bred>HTMLCollection</bred> :gb: : une collection/paquet de plusieurs balises HTML.

<p class="codepen" data-height="800" data-default-tab="html,result" data-slug-hash="VwrGjgx" data-editable="true" data-user="rod2ik" style="height: 300px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;">
  <span>See the Pen <a href="https://codepen.io/rod2ik/pen/VwrGjgx">
  Untitled</a> by rod2ik (<a href="https://codepen.io/rod2ik">@rod2ik</a>)
  on <a href="https://codepen.io">CodePen</a>.</span>
</p>
<script async src="https://cpwebassets.codepen.io/assets/embed/ei.js"></script>

!!! ex "Visualiser le résultat avec vos outils web"
    Visualiser le résultat de l'instruction `document.getElementsByClassName("bleu")` dans les outils web de votre navigateur: Vous devriez voir deux paragraphes : `<p class="bleu">` encapsulés dans un type de données primitif **HTMLCollection** :
    ![getElementsByClassName](../img/getElementsByClassName.png){.center style="width:100%;"}
