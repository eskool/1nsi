# 1NSI : Javascript `getElementsByName()`

La méthode `getElementsByName()` est accessible depuis le point d'entrée du DOM: `document`.

<env>Syntaxe</env> `document.getElementsByName("jaune");`

!!! def "`getElementsByName()` renvoie un type HTMLCollection"
    La méthode `getElementsByName("jaune");` renvoie TOUS les Noeuds du DOM qui sont des paragraphes `p`, disposant d'un attribut `name="jaune"`, càd tous les `<p name="jaune">lorem ..</p>`. Il peut y en avoir plusieurs (d'où le `s` dans getElement<bred>s</bred>ByTagName). Javascript renvoie (pour la première fois) un résultat sous la forme d'un (nouveau) type de données primitif appelé une <bred>NodeList</bred> :gb: : une liste/paquet de plusieurs <bred>noeuds</bred> javascript.

!!! note "Première Obtention d'une NodeList"
    Remarquer que c'est la première fois que Javascript nous renvoie un type de données primitif **NodeList**. Jusqu'à présent, nous avions obtenu des HTMLCollection. 

<p class="codepen" data-height="800" data-default-tab="html,result" data-slug-hash="zYPJKZP" data-editable="true" data-user="rod2ik" style="height: 300px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;">
  <span>See the Pen <a href="https://codepen.io/rod2ik/pen/zYPJKZP">
  getElementsByName</a> by rod2ik (<a href="https://codepen.io/rod2ik">@rod2ik</a>)
  on <a href="https://codepen.io">CodePen</a>.</span>
</p>
<script async src="https://cpwebassets.codepen.io/assets/embed/ei.js"></script>

!!! ex "Visualiser le résultat avec vos outils web"
    Visualiser le résultat de l'instruction `document.getElementsByName("p")` dans les outils web de votre navigateur: Vous devriez voir tous les paragraphes encapsulés dans un type de données primitif **HTMLCollection**: 
    ![getElementsByName](../img/getElementsByName.png){.center style="width:100%;"}

!!! pte "NodeList vs HTMLCollection"
    On peut visualiser:comprendre une différence entre une HTMLCollection et une NodeList, par exemple, en étendant le menu du premier paragraphe jaune (celui d'indice `0`). On voit par exemple que ce premier paragraphe jaune admet deux types d'**enfants** au sens de l'arborescence HTML de la page :
    
    * `ChildNodes` est une **NodeList** qui contient $3$ noeuds :
        * le noeud textuel AVANT la balise `span` (incluse dans le `<p name="jaune">`)
        * la balise/l'élément HTML qui est le `span`
        * le noeud textuel APRÈS la balise `span` (incluse dans le `<p name="jaune">`)
    * `children` est une **HTMLCollection** qui ne contient qu'UN seul Élément HTML:
        * UNIQUEMENT la balise `span`
    * Conclusion: Comparés aux **NodeList**, les **HTMLCollection** ne contiennent QUE les éléments HTML, mais les HTMLCollection ne contiennent en particulier PAS les noeuds textuels.
    ![Éléments vs Noeuds](../img/elements-vs-noeuds.png){.center style="width:100%;"}
