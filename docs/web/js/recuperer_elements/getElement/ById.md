# 1NSI : Javascript `getElementById()`

La méthode `getElementById()` est accessible depuis le point d'entrée du DOM: `document`.

<env>Syntaxe</env> `document.getElementById("vert");`

!!! def "`getElementById()` renvoie un (unique) Élément HTML"
    La méthode `getElementById("vert");` renvoie l'unique Élément HTML (donc l'unique balise) du **DOM - Document Object Model**, disposant de l'ID `id="vert"`.

<p class="codepen" data-height="800" data-default-tab="html,result" data-slug-hash="zYPJqYm" data-editable="true" data-user="rod2ik" style="height: 600px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;">
  <span>See the Pen <a href="https://codepen.io/rod2ik/pen/zYPJqYm">
  Untitled</a> by rod2ik (<a href="https://codepen.io/rod2ik">@rod2ik</a>)
  on <a href="https://codepen.io">CodePen</a>.</span>
</p>
<script async src="https://cpwebassets.codepen.io/assets/embed/ei.js"></script>

!!! hint "Astuce"
    L'exécution des 3 fichiers (HTML, CSS et JS) ci-dessous du code ci-dessous est visible dans votre navigateur personnel, directement, en affichant :
    
    * Les <bred>Outils de Développement Web</bred>, appelés plus simplement <bred>outils web</bred> avec le raccourci clavier ++ctrl+shift+i++ sur **Firefox**/**Chrome**, puis cliquer sur l'onglet **Console**.
    * ou bien, **pour ouvrir directement l'onglet Console** des outils Web, avec le raccourci:
       * ++ctrl+shift+k++ sur Firefox
       * ++ctrl+shift+j++ sur Chrome

!!! ex "Visualiser le résultat avec vos outils web"
    Visualiser le résultat de l'instruction `document.getElementById("vert")` dans les outils web de votre navigateur: Vous devriez voir un seul paragraphe : `<p id="vert">` :
    ![getElementById](../img/getElementById.png){.center style="width:100%;"}