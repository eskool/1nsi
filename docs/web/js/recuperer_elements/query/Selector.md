# 1NSI : Javascript `querySelector()`

La méthode `querySelector()` est accessible depuis le point d'entrée du DOM: `document`.

<env>Syntaxe</env> `document.querySelector("jaune");`

!!! def "`querySelector()` renvoie un unique Élément HTML"
    La méthode `querySelector("p.bleu");` renvoie **UNIQUEMENT LE PREMIER** élément HTML qui correspond au **Sélecteur CSS** `p.bleu` (càd uniquement le premier paragraphe `p` de classe `bleu`). Il n'y en a qu'un seul par définition. 

<p class="codepen" data-height="800" data-default-tab="html,result" data-slug-hash="eYeLdPV" data-editable="true" data-user="rod2ik" style="height: 300px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;">
  <span>See the Pen <a href="https://codepen.io/rod2ik/pen/eYeLdPV">
  querySelector</a> by rod2ik (<a href="https://codepen.io/rod2ik">@rod2ik</a>)
  on <a href="https://codepen.io">CodePen</a>.</span>
</p>
<script async src="https://cpwebassets.codepen.io/assets/embed/ei.js"></script>

!!! ex "Visualiser le résultat avec vos outils web"
    Visualiser le résultat de l'instruction `document.querySelector("p.bleu")` dans les outils web de votre navigateur: Vous devriez voir un unique paragraphe dont la classe est `bleu`: 
    ![querySelector](../img/querySelector-pBleu.png){.center style="width:100%;"}
