# 1NSI : Javascript `querySelectorAll()`

La méthode `querySelectorAll()` est accessible depuis le point d'entrée du DOM: `document`.

<env>Syntaxe</env> `document.querySelectorAll("p.bleu");`

!!! def "`querySelectorAll()` renvoie un type NodeList"
    La méthode `querySelectorAll("p.bleu");` renvoie TOUS les Noeuds du DOM qui sont des paragraphes `p`, disposant d'un attribut `class="bleu"`, càd tous les `<p class="bleu">lorem ..</p>`. Il peut y en avoir plusieurs (d'où le `All`). Javascript renvoie un résultat sous la forme d'une <bred>NodeList</bred> :gb: : une liste/paquet de plusieurs <bred>noeuds</bred> javascript.

<p class="codepen" data-height="800" data-default-tab="html,result" data-slug-hash="wvPEzZz" data-editable="true" data-user="rod2ik" style="height: 300px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;">
  <span>See the Pen <a href="https://codepen.io/rod2ik/pen/wvPEzZz">
  querySelectorAll</a> by rod2ik (<a href="https://codepen.io/rod2ik">@rod2ik</a>)
  on <a href="https://codepen.io">CodePen</a>.</span>
</p>
<script async src="https://cpwebassets.codepen.io/assets/embed/ei.js"></script>

!!! ex "Visualiser le résultat avec vos outils web"
    Visualiser le résultat de l'instruction `document.querySelectorAll("p")` dans les outils web de votre navigateur: Vous devriez voir tous les paragraphes encapsulés dans un type de données primitif **NodeList**: 
    ![querySelectorAll](../img/querySelectorAll.png){.center style="width:100%;"}
