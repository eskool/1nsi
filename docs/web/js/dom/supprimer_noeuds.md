# 1NSI : Javascript - Supprimer et Remplacer des Noeuds dans le DOM

Cette page recense quelques techniques pour **supprimer définitivement des noeuds/éléments** dans le DOM.

### removeChild

`parentNode.removeChild(<element>);`

Référence: https://developer.mozilla.org/fr/docs/Web/API/Node/removeChild

### replaceChild

`parentNode.replaceChild(newChild, oldChild);`

Référence: https://developer.mozilla.org/fr/docs/Web/API/Node/replaceChild
