# 1NSI : Javascript - Ajouter des Noeuds dans le DOM

Cette page recense quelques techniques pour **ajouter de nouveaux noeuds/éléments** dans le DOM.

## Principe Général

!!! mth "Ajout de nouveaux Noeuds dans le DOM"
    Le principe général, pour ajouter de nouveaux noeuds dans le DOM tient en plusieurs étapes:

    * **Création** d'un nouveau noeud : de manière séparée et indépendante du DOM
    * **Stockage** de ce nouveau noeud dans une variable Javascript
    * **Insertion** du nouveau noeud en tant qu'enfant, **avant** ou **après** un certain noeud, que l'on aura préalablement déterminé/sélectionné en JS

## Création d'un noeud avec `createElement`

## Ajout d'un enfant après un noeud avec `appendChild`

