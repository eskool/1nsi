# 1NSI : Javascript. Le DOM - Document Object Model

## Le DOM - Document Object Model

<img src="../img/modele-dom.svg" style="float:right; width:40%;">

Le langage **Javascript** modélise le code HTML d'une page web comme un arbre composé de (**briques élémentaires** appelées des) <bred>Noeuds</bred> :fr: / <bred>Nodes</bred> :gb:.  
**Javascript** permet la manipulation de cet arbre, grâce une **Interface de Programmation** qui s'appelle le <bred>DOM - Document Object Model</bred>. Intuitivement, le DOM peut être vu simplement comme l'arbre modélisant la page web, muni des fonctionnalités du Javascript permettant de le manipuler.

<clear></clear>

!!! pte "Enfants de noeuds"
    Chaque noeud peut avoir un ou plusieurs enfants, qui sont eux-mêmes des noeuds.

## Le mot-clé `document`

!!! def "Le mot-clé `document`"
    Le mot clé `document` en javascript fait référence au point d'entrée dans le contenu de la page web, dans le <bred>DOM - Document Object Model</bred> :gb:.

## Différents Types de Noeuds du DOM

Les noeuds du DOM peuvent être de plusieurs types. Il en existe une douzaine de types différents, mais seulement quelques uns sont vraiment utiles :

* Le type <bred>Élément HTML</bred> :fr: / <bred>HTML Element</bred> :gb: : ce sont les **balises HTML** (et leurs enfants).
* Le type <bred>Noeud Texte</bred> :fr: / <bred>TextNode</bred> :gb: : ce sont les noeuds des textes inclus dans la page web: qu'ils soient inclus dans une balise, ou pas. Au sens du DOM, Les textes sont des contenus distincts des balises.
* Le type <bred>Noeud d'Attribut</bred> :fr: / <bred>AttributeNode</bred> :gb: : ce sont les noeuds des attributs des balises de la page web. Les textes sont des contenus distincts des balises.

## Interactions avec les Noeuds

Javascript doit avant tout récupérer les **noeuds**, les stocker dans des variables Javascript, pour ensuite pouvoir **interagir** avec eux via les variables Javascript.  
Des exemples d'**interactions** avec des noeuds sont :

* La modification du **contenu** d'un noeud
* La modificaction du **style CSS**, depuis le Javascript, qui survient **après** l'application des règles du/des fichiers CSS (Les règles CSS **calculées** par le Javascript auront donc le dernier mot).
* La **création** et/ou la **suppression** de noeuds
* La **détection** et la **gestion** des Interactions entre l'utilisateur (Humain) et la Machine (IHM), via les <bred>événements</bred> :fr: / <bred>events</bred> :gb:. 

Des exemples d'événements sont :

* un **clic** sur un noeud
* un **survol** d'un noeud
* la **sortie** d'un noeud
* le **focus** d'un noeud
* l'**appui** (keydown) sur une touche, etc..
* etc..

!!! def "Programmation Événementielle"
    On dit que Javascript est un langage de <bred>Programmation Événementielle</bred> (qui permet la détection et la gestion d'**événements**)

Nous allons commencer par apprendre à récupérer des **Éléments HTML** : des balises et leur contenu donc.
