# 1NSI : Javascript - Modifier le DOM

Cette page recense quelques techniques pour **modifier des noeuds/éléments déjà existants dans le DOM.**

## Modifier un contenu avec `innerHTML` & `textContent`

## Modifier le `style` CSS depuis le Javascript


## Modifier des Classes

### `classList`

Cette propriété `classList` fournit aussi une série de fonctions permettant de modifier cette liste de classes.

### `add`

### `remove`

### `contains`

### `replace`


## Modifier les Attributs

### `setAttribute`

### `getAttribute`

### `removeAttribute`

