// const wordnikAPI = "https://api.wordnik.com/v4/words.json/randomWord?&minLength=5&maxLength=-1&api_key=UTILISEZ_VOTRE_CLE_API_PERSO_A_DEMANDER_A_WORDNIK"
const wordnikAPI = `https://api.wordnik.com/v4/words.json/randomWord?&minLength=5&maxLength=-1&api_key=${process.env.WORDNIK_API_KEY}`

// const giphyAPI = "https://api.giphy.com/v1/gifs/search?rating=G&api_key=UTILISEZ_VOTRE_CLE_API_PERSO_A_DEMANDER_A_GIPHY&q="
const giphyAPI = `https://api.giphy.com/v1/gifs/search?rating=G&api_key=${process.env.GIPHY_API_KEY}&q=`

// ce script PEUT renvoyer une erreur SI AUCUN GIF correspondant au mot aléatoire renvoyé par WORDNIK n'a été trouvé dans GIPHY..
// ne pas hésiter à relancer le script

const wordGIF = async () => {
    const reponse1 = await fetch(wordnikAPI);
    const donneesJson1 = await reponse1.json(); 
    const reponse2 = await fetch(giphyAPI + donneesJson1.word);
    const donneesJson2 = await reponse2.json();
    const img_url = donneesJson2.data[0].images['fixed_height_small'].url;
    return {
        word: donneesJson1.word,
        img: img_url
    }
}

wordGIF().then((reponse) => { 
        console.log(reponse.word);
        console.log(reponse.img);
    } )
    .catch( (err) => console.log(err) );