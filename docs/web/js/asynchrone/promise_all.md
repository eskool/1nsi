# 1NSI: `Promise.all()`

## Introduction

Imaginons maintenant que l'on souhaite charger $3$ (ou $4$, etc..) images différentes, et ne les afficher que lorsqu'on les as reçues toutes les trois, mais pas avant.

On pourrait imaginer plusieurs scénarii :

* **On chaîne chaque appel/promesse** : on en télécharge UNE, PUIS (lorsque la première est arrivée), on en télécharge une autre, etc... Ce scénario :
    * est intéressant car **il garantit que l'ordre d'arrivée des résolutions de promesses soit le même que l'ordre d'émission des promesses**.
    * mais **il n'est clairement pas très optimal au sens de la complexité algorithmique** (lorsque l'on veut gagner du temps).
* ou bien: On lance **en parallèle chacune des promesses**, par exemple en copiant/collant le même script l'un en dessous de l'autre (car javascript est synchrone). Ce scénario est le contraire du scénario précédent:
    * est **intéressant au sens de la complexité algorithmique** (pour gagner du temps),
    * mais **il ne garantit pas qur l'ordre d'arrivée des résolutions de promesses soit le même que l'ordre d'émission des promesses**.
* Mais alors, est-il possible d'avoir tous les avantages ?
    * De lancer en parallèle toutes les promesses,
    * **ET de garantir que l'ordre d'arrivée des résolutions de promesses soit le même que l'ordre d'émission des promesses**

La réponse est OUI, c'est possible, et c'est d'ailleurs exactement à çà que sert la fonction `Promise.all()`

## Comment fonctionne `Promise.all()` ?

`Promise.all()` **est (elle-même) une promesse** contruite à partir de plusieurs promesses placées dans un tableau javascript. `Promise.all()` permet de :

* Appeler de manière asynchrone plusieurs promesses en parallèle, toutes placées dans un tableau ordonné : `let promesses = [promesse1, promesse2, .., promesseN]` (par exemple)
* Attendre la résolution de toutes les promesses du tableau
* Attendre la résolution de Promise.all(), càd conserver la résolution de chacune des promesses DANS LE MÊME ORDRE QUE LE TABLEAU

## Un exemple avec les deux APIs wordknik et giphy

Pour faire fonctionner ce script, il vous **FAUT OBLIGATOIREMENT** des **clés API personnelles** de **wordnik** et **giphy**, que vous pouvez demander sur les deux sites officiels :

* [wordnik for developers](https://developer.wordnik.com/) : https://developer.wordnik.com/
    * remplacer votre clé d'API personnelle (de developpeur) **wordnik** dans : `const WORDNIK_API_KEY = "xxxxxxxxxx"`
* [giphy for developers](https://developers.giphy.com/) : https://developers.giphy.com/
    * remplacer votre clé d'API personnelle (de developpeur) **giphy** dans : `const GIPHY_API_KEY = "yyyyyyyyyyyy"`

<iframe height="800" style="width: 100%;" scrolling="no" title="Untitled" src="https://codepen.io/nsiperier/embed/PoMQrrG?default-tab=html%2Cresult" frameborder="no" loading="lazy" allowtransparency="true" allowfullscreen="true">
  See the Pen <a href="https://codepen.io/nsiperier/pen/PoMQrrG">
  Untitled</a> by nsiperier (<a href="https://codepen.io/nsiperier">@nsiperier</a>)
  on <a href="https://codepen.io">CodePen</a>.
</iframe>