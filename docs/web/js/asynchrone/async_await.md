# 1NSI: `Async` / `Await`

Ces deux fonctions Javascript `async` et `await` datent ("seulement" de) **ES8 / ES2017 / ECMA2017**.

Il s'agit principalement de <red>sucre syntaxique</red> pour rédiger les **requêtes asynchrones**.

On suppose que l'on dispose déjà d'une fonction qui renvoie une promesse.
On pourrait utiliser `fetch()`, ou bien notre propre fonction `setTimeoutPromise()` que nous avons créée dans le [paragraphe précédent sur les Promesses Personnelles](./promesses_perso.md).

La question que l'on se pose ici est d'améliorer la rédaction de l'appel d'une fonction renvoyant une promesse, de sorte que le code soit plus agréable qu'avec `.then()` (éventuellement plusieurs `.then()` consécutifs ) et `.catch()`.

## Await

Le mot clé `await` peut/doit être placé devant une fonction renvoyant une promesse, de sorte à ce que javascript sache qu'il faut attendre la fin de l'exécution de cette fonction (par ex. attendre la réception de données asynchrones).

!!! ex
    ```js linenums="1
    const setTimeoutPromise = (duree) => {
        // on suppose ici que l'on dispose déjà de cette fonction
        // 'setTimeoutPromise' qui renvoie une Promesse,
        // et qui viendrait par exemple d'une librairie JS
        return new Promise( (resolve, reject) => {
            if (isNaN(duree)) { // Si duree n'est pas un nombre
                reject(new Error('duree doit être un nombre')) // rejet de la promesse
            } // sinon ..
            setTimeout(resolve, duree)  // appelle la fonction resolve,
                                        // càd résoud la promesse ..
        } )
    }

    const setTimeoutPromiseES8 = (duree) => {
        // La fonction suivante renvoie une promesse
        await setTimeoutPromise(duree)
        return
    }

    setTimeoutPromiseES8(1000)
        .then( () => console.log("Bonjour") )
        .catch( (err) => console.log(err) )
    ```"

Malheureusement le code précédent renvoie une erreur : `SyntaxError: await is only valid in async functions and the top level bodies of modules`, qui singifie que le mot-clé `await` ne peut être utilisé QUE :

* à l'intérieur d'une fonction asynchrone, définie en tant que telle avec le mot-clé `async`
* ou bien, dans les *corps racines des *modules** (on peut raisonnablement oublier cette seconde possibilité, pour le moment)

D'où la syntaxe correcte avec async :

## `Async`

Le mot-clé `async` permet de **définir une fonction comme asynchrone**.
**Par défaut** (sans écrire ce mot-clé dans la définition d'une fonction), les fonctions en javascript sont **synchrones** (donc par défaut, on ne peut PAS `await` à l'intérieur..)

```js
// ancienne syntaxe d'une fonction javascript
async function maFonctionAsynchrone() {
    await fonctionQuiRenvoieUnePromesse()
}

// nouvelle syntaxe : fonctions fléchées
const maFonctionAsynchrone = async () => {
    await fonctionQuiRenvoieUnePromesse()
}
```

Voici deux exemples d'utilisation :

!!! exp
    ```js linenums="1"
    const setTimeoutPromise = (duree) => {
        // on suppose ici que l'on dispose déjà de cette fonction
        // 'setTimeoutPromise' qui renvoie une Promesse,
        // et qui viendrait par exemple d'une librairie JS
        return new Promise( (resolve, reject) => {
            if (isNaN(duree)) { // Si duree n'est pas un nombre
                reject(new Error('duree doit être un nombre')) // rejet de la promesse
            } // sinon ..
            setTimeout(resolve, duree)  // appelle la fonctoin resolve,
                                        // càd résoud la promesse ..
        } )
    }

    const setTimeoutPromiseES8 = async (duree) => {
        // La fonction suivante renvoie une promesse
        await setTimeoutPromise(duree)
        return
    }

    setTimeoutPromiseES8(1000)
        .then( () => console.log("Bonjour") )
        .catch( (err) => console.log(err) )
    ```

!!! exp "Promesse d'un mot Aléatoire grâce à l'API wordnik + Promesse d'un GIF Animé qui correspond avec GIPHY"
    ```js linenums="1"
    // ce code PEUT renvoyer une erreur si AUCUN GIF correspondant à ce mot n'a été trouvé..
    // ne pas hésiter à relancer le script
    const wordnikAPI = "https://api.wordnik.com/v4/words.json/randomWord?&minLength=5&maxLength=-1&api_key=UTILISEZ_VOTRE_CLE_API_PERSO_A_DEMANDER_A_WORDNIK"
    const giphyAPI = "https://api.giphy.com/v1/gifs/search?rating=G&api_key=UTILISEZ_VOTRE_CLE_API_PERSO_A_DEMANDER_A_GIPHY&q="

    const wordGIF = async () => {
        const reponse1 = await fetch(wordnikAPI);
        const donneesJson1 = await reponse1.json(); 
        const reponse2 = await fetch(giphyAPI + donneesJson1.word);
        const donneesJson2 = await reponse2.json();
        const img_url = donneesJson2.data[0].images['fixed_height_small'].url;
        return {
            word: donneesJson1.word,
            img: img_url
        }
    }

    wordGIF().then((reponse) => { 
            console.log(reponse.word);
            console.log(reponse.img);
        } )
        .catch( (err) => console.log(err) );
    ```


## Pourquoi est-ce bien plus agréable à écrire ?

Lorsque plusieurs fonctions asynchrones sont appelées via le réseau, on peut écrire par exemple :

```js linenums="1"
maFonctionAsynchrone()
    .then( () => console.log("bonjour"))
    .catch( (err) => console.log(err))


const maFonctionAsynchrone = async () => {
    await fonctionQuiRenvoieUnePromesse1()
    await fonctionQuiRenvoieUnePromesse2()
    await fonctionQuiRenvoieUnePromesse3()
    let rep = await fonctionQuiRenvoieUnePromesse4()
    return rep
}
```

Inutile donc de rédiger pleins de `.then()` et de `.catch()` qui se suivent dans ma fonction asynchrone. Plusieurs `await` sont beaucoup plus simples et lire, comprendre, rédiger et maintenir.