// const wordnikAPI = "https://api.wordnik.com/v4/words.json/randomWord?&minLength=5&maxLength=-1&api_key=UTILISEZ_VOTRE_CLE_API_PERSO_A_DEMANDER_A_WORDNIK"
const wordnikAPI = `https://api.wordnik.com/v4/words.json/randomWord?&minLength=5&maxLength=-1&api_key=${process.env.WORDNIK_API_KEY}`

let promise = fetch(wordnikAPI);

promise.then( (reponse) => { return reponse.json() } )
    .then( (donneesJson) => { console.log(donneesJson) } )
    .catch( (err) => console.log(err) );