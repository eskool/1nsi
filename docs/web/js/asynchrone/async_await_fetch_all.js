// const wordnikAPI = "https://api.wordnik.com/v4/words.json/randomWord?&minLength=5&maxLength=-1&api_key=UTILISEZ_VOTRE_CLE_API_PERSO_A_DEMANDER_A_WORDNIK"
let wordnikStartAPI = "https://api.wordnik.com/v4/words.json/randomWord?&minLength="

// const giphyAPI = "https://api.giphy.com/v1/gifs/search?rating=G&api_key=UTILISEZ_VOTRE_CLE_API_PERSO_A_DEMANDER_A_GIPHY&q="
const giphyAPI = `https://api.giphy.com/v1/gifs/search?rating=G&api_key=${GIPHY_API_KEY}&q=`

// ce script PEUT renvoyer une erreur SI AUCUN GIF correspondant au mot aléatoire renvoyé par WORDNIK n'a été trouvé dans GIPHY..
// ne pas hésiter à relancer le script

const wordGIF = async (nombre) => {
    wordnikAPI = `${wordnikStartAPI}${nombre}&maxLength=${nombre}&api_key=${WORDNIK_API_KEY}`
    const reponse1 = await fetch(wordnikAPI);
    const donneesJson1 = await reponse1.json();
    const reponse2 = await fetch(giphyAPI + donneesJson1.word);
    const donneesJson2 = await reponse2.json();
    const img_url = donneesJson2.data[0].images['fixed_height_small'].url;
    return {
        word: donneesJson1.word,
        img: img_url
    }
}

let promesses = [wordGIF(3), wordGIF(4), wordGIF(5)]
Promise.all(promesses)
    .then((results) => {
        for (let i = 0; i < results.length; i++) {
            createP(results[i].word);
            createImg(results[i].img);
        }
    })
    .catch((err) => { console.log(err) })

const createP = (texte) => {
    let p = document.createElement("p");
    let mot = document.createTextNode(texte);
    p.appendChild(mot)
    document.body.appendChild(p)
    return p
}

const createImg = (src, alt) => {
    if (!alt) {
        alt = ""
    }

    let img = document.createElement("img");
    img.setAttribute('src', src);
    img.setAttribute('alt', alt);
    document.body.appendChild(img)
    return img
}
