const setTimeoutPromise = (duree) => {
    return new Promise( (resolve, reject) => {
        if (isNaN(duree)) { // Si duree n'est pas un nombre
            reject(new Error('duree doit être un nombre')) // rejet de la promesse
        } // sinon ..
        setTimeout(resolve, duree)  // appelle la fonction resolve,
                                    // càd résoud la promesse ..
    } )
}

setTimeoutPromise(1000)
    .then( () => console.log("Bonjour") )
    .catch( (err) => console.log(err) )