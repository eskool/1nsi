const setTimeoutPromise = (duree) => {
    // on suppose ici que l'on dispose déjà de cette fonction
    // 'setTimeoutPromise' qui renvoie une Promesse,
    // et qui viendrait par exemple d'une librairie JS
    return new Promise( (resolve, reject) => {
        if (isNaN(duree)) { // Si duree n'est pas un nombre
            reject(new Error('duree doit être un nombre')) // rejet de la promesse
        } // sinon ..
        setTimeout(resolve, duree)  // appelle la fonctoin resolve,
                                    // càd résoud la promesse ..
    } )
}

const setTimeoutPromiseES8 = async (duree) => {
    // La fonction suivante renvoie une promesse
    await setTimeoutPromise(duree)
    return
}

setTimeoutPromiseES8(1000)
    .then( () => console.log("Bonjour") )
    .catch( (err) => console.log(err) )