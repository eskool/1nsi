# 1NSI: JS est un Langage Asynchrone

## Introduction

**Javascript** est un **langage de programmation** <red>asynchrone</red>.
**Ce n'est PAS le cas d'autres langages de programmation**, tels que **Python** par exemple.. Mais qu'est-ce qu'un langage asynchrone ?

!!! def "(Langage de) Programmation Asynchrone"
    La <red>programmation asynchrone</red>[^1] est une technique qui permet à un programme de démarrer une tâche à l'exécution potentiellement longue et, au lieu d'avoir à attendre la fin de la tâche, de pouvoir continuer à réagir aux autres évènements pendant l'exécution de cette tâche. Une fois la tâche terminée, le programme en reçoit le résultat.

 en pratique, cela se traduit par le fait que si une ligne prend plus de temps d'exécution que raisonnable, alors **Javascript poursuit l'EXÉCUTION DES LIGNES SUIVANTES, AVANT même que l'exécution de la ou des lignes précédentes ne soient finies**.
Ce n'est pas le cas du **Langage Python** (sauf à utiliser certaines librairies spécifiques, spécialement prévues pour cela).


## Références

[^1]: [Introduction au Javascript Asynchrone, Microsoft](https://developer.mozilla.org/fr/docs/Learn/JavaScript/Asynchronous/Introducing)

