# 1NSI : Javascript - boucle `for`

## Syntaxe générale

```js linenums="0"
for (let i=0; i<5; i++) {
  console.log(i);
}
```

affiche dans la console javascript (outils webs) :

```console linenums="0"
0
1
2
3
4
```

Remarques:

* Il FAUT OBLIGATOIREMENT déclarer la variable `i` avec `let`
* on part de `i=0`
* et *tant que* `i<5`, on continue les itérations
* `i++` incrémente de `1`: cette syntaxe javascript (comme en C++), est équivalente à `i = i+1`

## Méthode 1 `for` : Parcours de Noeuds, avec indices

<p class="codepen" data-height="800" data-default-tab="html,result" data-slug-hash="JjOaaMZ" data-editable="true" data-user="rod2ik" style="height: 300px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;">
  <span>See the Pen <a href="https://codepen.io/rod2ik/pen/JjOaaMZ">
  querySelectorAll paragraphs</a> by rod2ik (<a href="https://codepen.io/rod2ik">@rod2ik</a>)
  on <a href="https://codepen.io">CodePen</a>.</span>
</p>
<script async src="https://cpwebassets.codepen.io/assets/embed/ei.js"></script>

Cette syntaxe **for** en javascript :

* récupère tous les paragraphes, et les stocke dans la variable javascript `pTous`
* Le type de données de `pTous` est une NodeList (ce n'est pas ce qui est important), 
* `pTous[i]` est le `i`-ème élément de la NodeList `pTous` : En particulier, cela veut dire que **les éléments d'une NodeList (mais aussi ceux d'un HTMLCollection) sont accessibles donc itérables via un indice** `i` (ça c'est plus important)
* ce noeud est stylisable en CSS, depuis Javascript avec la propriété `style`

## Méthode 2 `for` : Parcours de Noeuds, sans indices, noeud par noeud

<p class="codepen" data-height="800" data-default-tab="html,result" data-slug-hash="zYPJJjR" data-editable="true" data-user="rod2ik" style="height: 300px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;">
  <span>See the Pen <a href="https://codepen.io/rod2ik/pen/zYPJJjR">
  for methode 2</a> by rod2ik (<a href="https://codepen.io/rod2ik">@rod2ik</a>)
  on <a href="https://codepen.io">CodePen</a>.</span>
</p>
<script async src="https://cpwebassets.codepen.io/assets/embed/ei.js"></script>

Cette syntaxe **for** en javascript :

* récupère tous les paragraphes, et les stocke dans la variable javascript `pTous`
* Le type de données de `pTous` est une NodeList (ce n'est pas ce qui est important), 
* On peut itérer sur chaque paragraphe `p` de la NodeList `pTous` avec le mot-clé `of` : En particulier, cela veut dire que **les noeuds/éléments d'une NodeList (mais aussi ceux d'un HTMLCollection) sont itérables** avec `for .. of ..` (ça c'est plus important)
* ce noeud est stylisable en CSS, depuis Javascript avec la propriété `style`
