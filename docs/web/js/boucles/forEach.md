# 1NSI : Javascript - Boucle `forEach()` depuis un Array

## Syntaxe Générale

```js linenums="0"
// let tableau = Array(3, 10, "Bonjour");
let tableau = [3, 10, "Bonjour"];
tableau.forEach( (el) => {
  console.log(el);
});
```

affiche dans la console javascript (outils web) :

```console linenums="0"
3
10
Bonjour
```

## Méthode 1 `forEach` : Parcours des éléments avec conversion depuis une NodeList

<p class="codepen" data-height="800" data-default-tab="html,result" data-slug-hash="abVaaRK" data-editable="true" data-user="rod2ik" style="height: 300px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;">
  <span>See the Pen <a href="https://codepen.io/rod2ik/pen/abVaaRK">
  forEach</a> by rod2ik (<a href="https://codepen.io/rod2ik">@rod2ik</a>)
  on <a href="https://codepen.io">CodePen</a>.</span>
</p>
<script async src="https://cpwebassets.codepen.io/assets/embed/ei.js"></script>

Cette syntaxe **forEach()** en javascript:

* Utilise :warning: **OBLIGATOIREMENT** :warning: un **Array** (en particulier PAS une NodeList, NI une HTMLCollection)
* récupère tous les paragraphes, et les stocke dans la variable javascript `pTous`
* Le type de données de `pTous` est une NodeList (c'est tout de même important ici, car il va falloir commencer par la convertir en Array), 
* **commence par convertir la NodeList en Array** (on pourrait tout aussi bien le faire aussi avec une HTMLCollection, cf méthode 2)
* On peut itérer sur chaque paragraphe `p` de l'Array `Array.from(pTous)` avec un `forEach( (p) => {..} )` : En particulier, cela veut dire que **les éléments d'un Array sont itérables avec un** `forEach` (ça c'est plus important)
* ce noeud est stylisable en CSS, depuis Javascript avec la propriété `style`

## Méthode 2 `forEach` : Parcours des éléments avec conversion depuis une HTMLCollection

<p class="codepen" data-height="800" data-default-tab="html,result" data-slug-hash="wvPEYGg" data-editable="true" data-user="rod2ik" style="height: 300px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;">
  <span>See the Pen <a href="https://codepen.io/rod2ik/pen/wvPEYGg">
  Untitled</a> by rod2ik (<a href="https://codepen.io/rod2ik">@rod2ik</a>)
  on <a href="https://codepen.io">CodePen</a>.</span>
</p>
<script async src="https://cpwebassets.codepen.io/assets/embed/ei.js"></script>

Cette syntaxe **forEach()** en javascript:

* :warning: Utilise **OBLIGATOIREMENT** un **Array** (en particulier PAS une NodeList, NI une HTMLCollection)
* récupère tous les paragraphes, et les stocke dans la variable javascript `pTous`
* Le type de données de `pTous` est une NodeList (c'est tout de même important ici, car il va falloir commencer par la convertir en Array), 
* **commence par convertir le type HTMLCollection en Array** (on pourrait tout aussi bien le faire aussi avec une NodeList, cf méthode 1)
* On peut itérer sur chaque paragraphe `p` de l'Array `Array.from(pTous)` avec un `forEach( (p) => {..} )` : En particulier, cela veut dire que **les éléments d'un Array sont itérables avec un** `forEach` (ça c'est plus important)
* ce noeud est stylisable en CSS, depuis Javascript avec la propriété `style`

