# 1NSI : Javascript - Boucle `while`

## Syntaxe Générale

```js linenums="0"
let i=0;
while (i<5) {
  console.log(i);
  i++;
}
```

affiche dans la console javascript (outils web) :

```console linenums="0"
0
1
2
3
4
```

## Parcours de Noeuds/Éléments avec while

```js linenums="0"
var pTous = document.querySelectorAll("p.bleu");
console.log(pTous);
let i=0;
while (i<pTous.length) {
  pTous[i].style.color = "blue";
  i++;
}
```

<p class="codepen" data-height="800" data-default-tab="html,result" data-slug-hash="dyZqqLL" data-editable="true" data-user="rod2ik" style="height: 300px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;">
  <span>See the Pen <a href="https://codepen.io/rod2ik/pen/dyZqqLL">
  while parcours noeuds</a> by rod2ik (<a href="https://codepen.io/rod2ik">@rod2ik</a>)
  on <a href="https://codepen.io">CodePen</a>.</span>
</p>
<script async src="https://cpwebassets.codepen.io/assets/embed/ei.js"></script>

Dans cet exemple:

* On récupère tous les paragraphes `p` de class "bleu" (`p.bleu`) du DOM dans la variable `pTous`
* `pTous` est une NodeList, donc chaque paragraphe est accessible via un indice `i` 
* le `i`-ème paragraphe est `pTous[i]`
* on fait varier les indices `i` de `0` (le premier indice de la NodeList) jusqu'à l'indice du dernier noeud récupéré `pTous.length`
