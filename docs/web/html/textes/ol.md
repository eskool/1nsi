# Listes Ordonnées / Ordered Lists / Listes Numérotées

<iframe height="500" style="width: 100%;" scrolling="no" title="ol" src="https://codepen.io/nsiperier/embed/jOXLKyq?default-tab=html%2Cresult&editable=true" frameborder="no" loading="lazy" allowtransparency="true" allowfullscreen="true">
  See the Pen <a href="https://codepen.io/nsiperier/pen/jOXLKyq">
  ol</a> by nsiperier (<a href="https://codepen.io/nsiperier">@nsiperier</a>)
  on <a href="https://codepen.io">CodePen</a>.
</iframe>