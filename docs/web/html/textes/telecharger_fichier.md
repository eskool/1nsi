# Télécharger un Fichier

## Commencer par créer un fichier

Il nous faut avant tout créer un fichier en local, et le placer dans le même répertoire que votre fichier `.html` (pour simplifier) de sorte à pourvoir le télécharger.

!!! remarque
    Le fichier à télécharger depuis votre site ne doit pas être *trop simple* : 

    * Les images ne sont pas téléchargées, mais plutôt visualisées directement dans le navigateur
    * Les fichiers textes *très simples*, comme par exemple ceux ayant des extensions `.txt`

    Vous devez disposer (en local) d'un fichier *pas trop simple*, par exemple un fichier

    * un fichier Libre Office Writer comme <a href="https://gitlab.com/eskool/1nsi/-/raw/main/docs/web/html/textes/fichier.odt?ref_type=heads&inline=false">celui-ci</a>
    * un fichier zippé comme <a href="https://gitlab.com/eskool/1nsi/-/raw/main/docs/web/html/textes/fichier.zip?ref_type=heads&inline=false">celui-ci</a>

## Code HTML pour télécharger un fichier

<iframe height="500" style="width: 100%;" scrolling="no" title="telechargerUnFichier" src="https://codepen.io/nsiperier/embed/wvRqYKV?default-tab=html%2Cresult&editable=true" frameborder="no" loading="lazy" allowtransparency="true" allowfullscreen="true">
  See the Pen <a href="https://codepen.io/nsiperier/pen/wvRqYKV">
  telechargerUnFichier</a> by nsiperier (<a href="https://codepen.io/nsiperier">@nsiperier</a>)
  on <a href="https://codepen.io">CodePen</a>.
</iframe>