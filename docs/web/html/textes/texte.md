# Autres Balises de Texte en HTML

* la balise `<strong>important</strong>` **donne de l'importance** au texte contenu dans cette balise. En pratique, cela signifie que le texte sera mis **en gras**.
* la balise `<em>en valeur</em>` **met en valeur** le texte contenu dans cette balise. En pratique, cela signifie que le texte sera mis en *italique*.
* la balise `<mark>jaune fluorescent</mark>` **marque / surligne avec un fond fluo** le texte contenu dans cette balise. En pratique, cela signifie que le texte sera surligné au marqueur <mark style="background-color:yellow;color:black;">jaune fluo</mark>.

!!! remarque
    Il ne faut **PAS** utiliser ces balises HTML pour leur implémentation pratique (càd de rendre le contenu en gras, en italique, ou en fond jaune fluo), mais plutôt pour **donner du sens** à leur contenu. En effet, si on veut être sûr du rendu, il est plutôt conseillé d'utiliser le langage **CSS - Cascading Style Sheets** pour styliser les élements d'une page web, car :

    * HTML est un langage **sémantique** : il est utilisé pour **donner du sens** au contenu des éléments / balises (et non pas pour prévoir le rendu)
    * CSS sert spécialement à garantir le rendu visuel
    * le rendu avec CSS est plus sûr : en effet, le rendu de ces balises en HTML peut varier selon l'implémentation de chaque navigateur internet.

<iframe height="500" style="width: 100%;" scrolling="no" title="autresBalisesTexte" src="https://codepen.io/nsiperier/embed/Vwqzojp?default-tab=html%2Cresult&editable=true" frameborder="no" loading="lazy" allowtransparency="true" allowfullscreen="true">
  See the Pen <a href="https://codepen.io/nsiperier/pen/Vwqzojp">
  autresBalisesTexte</a> by nsiperier (<a href="https://codepen.io/nsiperier">@nsiperier</a>)
  on <a href="https://codepen.io">CodePen</a>.
</iframe>