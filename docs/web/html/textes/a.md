# Liens Hypertextes `a`

## Liens Externes

<iframe height="500" style="width: 100%;" scrolling="no" title="Untitled" src="https://codepen.io/nsiperier/embed/VwqzByv?default-tab=html%2Cresult&editable=true" frameborder="no" loading="lazy" allowtransparency="true" allowfullscreen="true">
  See the Pen <a href="https://codepen.io/nsiperier/pen/VwqzByv">
  Untitled</a> by nsiperier (<a href="https://codepen.io/nsiperier">@nsiperier</a>)
  on <a href="https://codepen.io">CodePen</a>.
</iframe>

## Liens Internes

### page1.html

<iframe height="500" style="width: 100%;" scrolling="no" title="lienInternePage1" src="https://codepen.io/nsiperier/embed/YzdxjvW?default-tab=html%2Cresult&editable=true" frameborder="no" loading="lazy" allowtransparency="true" allowfullscreen="true">
  See the Pen <a href="https://codepen.io/nsiperier/pen/YzdxjvW">
  lienInternePage1</a> by nsiperier (<a href="https://codepen.io/nsiperier">@nsiperier</a>)
  on <a href="https://codepen.io">CodePen</a>.
</iframe>

### page2.html

<iframe height="500" style="width: 100%;" scrolling="no" title="lienInternePage2" src="https://codepen.io/nsiperier/embed/WNLEKyz?default-tab=html%2Cresult&editable=true" frameborder="no" loading="lazy" allowtransparency="true" allowfullscreen="true">
  See the Pen <a href="https://codepen.io/nsiperier/pen/WNLEKyz">
  lienInternePage2</a> by nsiperier (<a href="https://codepen.io/nsiperier">@nsiperier</a>)
  on <a href="https://codepen.io">CodePen</a>.
</iframe>

## Ancres vers un `id` d'une page HTML

<iframe height="500" style="width: 100%;" scrolling="no" title="ancres" src="https://codepen.io/nsiperier/embed/WNLEgPV?default-tab=html%2Cresult&editable=true" frameborder="no" loading="lazy" allowtransparency="true" allowfullscreen="true">
  See the Pen <a href="https://codepen.io/nsiperier/pen/WNLEgPV">
  ancres</a> by nsiperier (<a href="https://codepen.io/nsiperier">@nsiperier</a>)
  on <a href="https://codepen.io">CodePen</a>.
</iframe>