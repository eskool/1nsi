# Introduction à HTML

## Qu'est-ce que l'HTML ?

**HTML - HyperText Markup Language** :gb: est le Langage Standard de **création des pages Web**. Avec le langage HTML vous pouvez créer votre propre site web, et même créer des applications Android/iOS (en y ajoutant tout de même d'autres technologies).<br/>

HTML est un langage de Balisage (Markup) utilisant des liens HyperTexte. Le Langage HTML utilise donc des **éléments** appelés **Balises** :fr: / **Tags** :gb: qui sont classiquement de deux sortes :

### Balises Ouvrantes et Fermantes

Le principe général dans ce cas est d'avoir une `<balise>` qui ouvre, et une `</balise>` qui ferme (un peu comme des parenthèses).

* `<balise>` pour une **balise ouvrante**
* `</balise>` pour une **balise fermante**

### Balises Auto-Fermantes

Il existe également quelques (rares) balises dites **auto-fermantes**:

* `<balise />`

## Références / Aller Plus loin

* Le Réseau de **Développeurs de Mozilla** : https://developer.mozilla.org/fr/docs/Web/HTML
* Le Réseau de **w3schools.com** : https://www.w3schools.com/html/





