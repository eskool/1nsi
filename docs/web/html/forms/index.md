# 1NSI : Les Formulaires :fr: / Forms :gb: en HTML

Un Formulaire :fr: /Form :gb: HTML est défini par balise HTML `form`, qui permet de créer un formulaire demandant des informations à l'utilisateur. Les formulaires/forms HTML permettent donc une **IHM - Interaction de l'Homme et de la Machine** en HTML.

```html
<form id="ajout">
    <label for="title">Titre:</label>
    <input type="text" name="title">
    <label for="author">Auteur:</label>
    <input type="text" name="author">
    <button>Ajouter un Livre</button>
</form>
```

```js
const ajoutFormulaire = document.querySelector("#ajout");
ajoutFormulaire.addEventListener("submit", (e) => {
    e.preventDefault();
    console.log("Bien ! Tu as bien 'soumis'/envoyé les données de ton Formulaire, en Javascript");
    console.log(ajoutFormulaire.title.value);
    console.log(ajoutFormulaire.author.value);
    // ajoutFormulaire.reset()
})
```
