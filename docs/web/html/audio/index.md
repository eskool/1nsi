# Balises Audio en HTML

## Avec un fichier audio en Local

Vous devez disposer du/des fichiers audio en local.  
Pour vous simplifier la vie de développeur, vous pouvez télécharger des exemples de fichiers `.mp3` et `.ogg` ci-dessous (pour cela faire "*Clic Droit -> Enregister la cible du lien sous..."*) :

* [monfichier.mp3](https://file-examples.com/storage/fe7fa6fa10650d95e925ca2/2017/11/file_example_MP3_1MG.mp3)
* [monfichier.ogg](https://file-examples.com/storage/fe7fa6fa10650d95e925ca2/2017/11/file_example_OOG_2MG.ogg)

Renommer les en `monfichier.mp3` et `monfichier.ogg`.
Placez les deux fichiers audio dans le même répertoire que votre fichier `index.html`, puis tapez les codes suivants :

<iframe height="500" style="width: 100%;" scrolling="no" title="audioLocal" src="https://codepen.io/nsiperier/embed/rNoJMdQ?default-tab=html%2Cresult&editable=true" frameborder="no" loading="lazy" allowtransparency="true" allowfullscreen="true">
  See the Pen <a href="https://codepen.io/nsiperier/pen/rNoJMdQ">
  audioLocal</a> by nsiperier (<a href="https://codepen.io/nsiperier">@nsiperier</a>)
  on <a href="https://codepen.io">CodePen</a>.
</iframe>

## Avec un fichier audio via Http

On peut aussi récupérer un fichier audio en direct sur internet, via le protocole `http` ou `https` avec la même syntaxe que précédemment :

<iframe height="500" style="width: 100%;" scrolling="no" title="audio" src="https://codepen.io/nsiperier/embed/wvRrvwW?default-tab=html%2Cresult&editable=true" frameborder="no" loading="lazy" allowtransparency="true" allowfullscreen="true">
  See the Pen <a href="https://codepen.io/nsiperier/pen/wvRrvwW">
  audio</a> by nsiperier (<a href="https://codepen.io/nsiperier">@nsiperier</a>)
  on <a href="https://codepen.io">CodePen</a>.
</iframe>