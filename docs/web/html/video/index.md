# Balises Vidéo en HTML

## Avec un fichier vidéo en local

Vous devez disposer du/des fichiers vidéo en local.  
Pour vous simplifier la vie de développeur, vous pouvez télécharger des exemples de fichiers `.mp4` et `.webm`ci-dessous (pour cela faire "*Clic Droit -> Enregister la cible du lien sous..."*) :

* [monfichier.mp4](https://file-examples.com/storage/fe7fa6fa10650d95e925ca2/2017/04/file_example_MP4_640_3MG.mp4)
* [monfichier.webm](https://file-examples.com/storage/fe7fa6fa10650d95e925ca2/2020/03/file_example_WEBM_640_1_4MB.webm)

Renommer les en `monfichier.mp4` et `monfichier.webm`.
Placez les deux fichiers vidéos dans le même répertoire que votre fichier `index.html`, puis tapez les codes suivants :

<iframe height="500" style="width: 100%;" scrolling="no" title="video" src="https://codepen.io/nsiperier/embed/MWZrXWR?default-tab=html%2Cresult&editable=true" frameborder="no" loading="lazy" allowtransparency="true" allowfullscreen="true">
  See the Pen <a href="https://codepen.io/nsiperier/pen/MWZrXWR">
  video</a> by nsiperier (<a href="https://codepen.io/nsiperier">@nsiperier</a>)
  on <a href="https://codepen.io">CodePen</a>.
</iframe>

!!! remarque
    * Cette méthode n'est pas idéale, car cela force à télécharger en local des vidéos qui sont très lourdes en mémoire.
    * La méthode suivante, consistant à utiliser un iframe d'une vidéo placée sur un site de streaming (YouTube, DailyMotion, Vimeo, etc..) est bien supérieure

## Avec un Iframe

Choisir une vidéo qui vous plaît sur YouTube/DailyMotion/Vimeo, nous choisirons par exemple celle-ci.

Puis, sous la vidéo, Aller dans **Partager $\Rightarrow$ Intégrer** ouis copier-coller le code HTML offert par la plateforme contenant `<iframe>...</iframe>` (vous pouvez personnaliser ce code si vous le comprenez un peu.. notament la hauteur `height` et/ou la largeur `width`)

<iframe height="500" style="width: 100%;" scrolling="no" title="iframe" src="https://codepen.io/nsiperier/embed/wvRpXKX?default-tab=html%2Cresult&editable=true" frameborder="no" loading="lazy" allowtransparency="true" allowfullscreen="true">
  See the Pen <a href="https://codepen.io/nsiperier/pen/wvRpXKX">
  iframe</a> by nsiperier (<a href="https://codepen.io/nsiperier">@nsiperier</a>)
  on <a href="https://codepen.io">CodePen</a>.
</iframe>