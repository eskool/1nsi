# Images

Pour insérer une image, on utilise :

* la balise `<img />`
* on ajoute l'attribut `src` (la source) pour préciser **le chemin** de l'image à afficher
* on ajoute l'attribut `alt` (texte alternatif) pour ajouter **un texte alternatif** à l'image. Ce texte alternatif est utilisé par :
    * les logiciels des non-voyants et malvoyants afin de leur faciliter le surf sur internet
    * Les robots (crawlers) de Google (par exemple) afin d'améliorer le **Référencement** :fr: / **SEO - Search Engine Optimization** :gb: de votre site
* On *peut* ajouter l'attribut `title` si vous souhaitez ajouter une **infobulle** :fr: / **tooltip** :gb:

## Insérer une image située en local

Ici, l'image `bateau.jpg` ne se trouve pas sur le serveur de `Codepen`.., donc `Codepen` ne peut trouver l'image, donc le serveur de `Codepen` affiche le texte alternatif à la place de l'image (c'est "normal").  
Néanmoins, si vous téléchargez une image en local, l'image s'affichera correctement dans votre navigateur (si le chemin vers votre image locale est correct)

<iframe height="500" style="width: 100%;" scrolling="no" title="image1" src="https://codepen.io/nsiperier/embed/vYvevKg?default-tab=html%2Cresult&editable=true" frameborder="no" loading="lazy" allowtransparency="true" allowfullscreen="true">
  See the Pen <a href="https://codepen.io/nsiperier/pen/vYvevKg">
  image1</a> by nsiperier (<a href="https://codepen.io/nsiperier">@nsiperier</a>)
  on <a href="https://codepen.io">CodePen</a>.
</iframe>

## Insérer une image située sur le web

<iframe height="500" style="width: 100%;" scrolling="no" title="image2" src="https://codepen.io/nsiperier/embed/YzdrdxZ?default-tab=html%2Cresult&editable=true" frameborder="no" loading="lazy" allowtransparency="true" allowfullscreen="true">
  See the Pen <a href="https://codepen.io/nsiperier/pen/YzdrdxZ">
  image2</a> by nsiperier (<a href="https://codepen.io/nsiperier">@nsiperier</a>)
  on <a href="https://codepen.io">CodePen</a>.
</iframe>

## Les balises `figure` et `figcaption`

Depuis HTML5, il est préconisé d'utiliser :

* une balise `<figure></figure>` pour contenir/englober l'image
* une balise `<figcaption></figcaption>` pour ajouter une légende à l'image

<iframe height="500" style="width: 100%;" scrolling="no" title="image3 - figure - figcaption" src="https://codepen.io/nsiperier/embed/MWZELjK?default-tab=html%2Cresult&editable=true" frameborder="no" loading="lazy" allowtransparency="true" allowfullscreen="true">
  See the Pen <a href="https://codepen.io/nsiperier/pen/MWZELjK">
  image3 - figure - figcaption</a> by nsiperier (<a href="https://codepen.io/nsiperier">@nsiperier</a>)
  on <a href="https://codepen.io">CodePen</a>.
</iframe>

!!! remarque
    Remarquer que ni l'image, ni la légende ne sont centrées par défaut.
    On peut, pour corriger cela :

    * utiliser la balise `<center></center>` pour centrer tous les contenus compris à l'intérieur de la balise `center` (non préconisé)
    * utiliser du CSS pour centrer les contenus (préconisé, mais plus difficile)

<iframe height="500" style="width: 100%;" scrolling="no" title="image3 - figure - figcaption CENTRE" src="https://codepen.io/nsiperier/embed/wvRrNgb?default-tab=html%2Cresult&editable=true" frameborder="no" loading="lazy" allowtransparency="true" allowfullscreen="true">
  See the Pen <a href="https://codepen.io/nsiperier/pen/wvRrNgb">
  image3 - figure - figcaption CENTRE</a> by nsiperier (<a href="https://codepen.io/nsiperier">@nsiperier</a>)
  on <a href="https://codepen.io">CodePen</a>.
</iframe>