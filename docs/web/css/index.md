# Introduction au CSS

Le langage <red>CSS - Cascading Style Sheet</red> :gb: / <red>Feuilles de Styles en Cascade</red> :fr: est le langage utilisé pour styliser / relooker une page web HTML.

## 3 manières d'insérer du CSS dans une page HTML

### Méthode 1

* Créer un fichier `style.css`
* Insérer une balise `link` contenant un attribut `href='style.css'`, **à l'intérieur** de la balise `head` **obligatoirement**, avec un attribut qui renvoie vers le fichier `style.css`

<iframe height="300" style="width: 100%;" scrolling="no" title="Untitled" src="https://codepen.io/nsiperier/embed/bGOLBmz?default-tab=html%2Cresult&editable=true" frameborder="no" loading="lazy" allowtransparency="true" allowfullscreen="true">
  See the Pen <a href="https://codepen.io/nsiperier/pen/bGOLBmz">
  Untitled</a> by nsiperier (<a href="https://codepen.io/nsiperier">@nsiperier</a>)
  on <a href="https://codepen.io">CodePen</a>.
</iframe>



## Références

* Mozilla Developer Network (MDN) : https://developer.mozilla.org/fr/docs/Learn/CSS/First_steps
* Cours CSS sur W3 Schools : https://www.w3schools.com/css/