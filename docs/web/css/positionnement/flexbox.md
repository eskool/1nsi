# La Méthode CSS de Positionnement Flexbox ```display:flex;```

## Vidéos

<iframe width="560" height="315" src="https://www.youtube.com/embed/UcC76tcvLgA?si=cgLUH0XsVGLSo-7q" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

## Résumé

La propriété css ```display:flex;``` pour l'élément conteneur affiche les *elements* en ligne, de gauche à droite par défaut.

| Propriété CSS à placer<br/>dans Élément #conteneur | Valeurs Possibles<br/><red>( valeur par défaut )</red> | Description / Résultat |
|:-:|:-:|:-:|
|$\varnothing$ par défaut|$\varnothing$|Affiche éléments verticalement,<br/> de haut en bas|  |
|**display**|flex|Affiche éléments horizontalement, de gauche à droite|  |
|**flex-direction**<br/>Définit l'**axe principal** d'affichage<br/>($=$ l'axe <bred>horizontal</bred> par défaut)|<bred>row</bred>|horizontal, de gauche à droite|  |
|$\,$|row-reverse|horizontal, de droite à gauche|  |
|$\,$|column|vertical, de haut en bas|  |
|$\,$|column-reverse|vertical, de bas en haut|  |
|**justify-content**<br/>Définit le positionnement des éléments<br/> sur l'**axe principal**|<bred>flex-start</bred>|de gauche vers droite, espace à droite si non fini||
|$\,$|flex-end|de droite vers gauche, espace à gauche si non fini||
|$\,$|center|centré, sans espace entre les éléments||
|$\,$|space-between|centré, avec un espace **entre** les éléments,<br/> mais **pas aux extrémités**||
|$\,$|space-around|centré, avec un espace **entre** les éléments,<br/> et **aussi aux extrémités**||
|$\,$|space-evenly|centré, avec un espace **ÉGAL entre** les éléments,<br/> et **aussi aux extrémités**||
|**align-items**<br/>Définit le positionnement des éléments<br/> sur l'**axe secondaire**<br/>(perpendiculaire à l'axe principal)|<bred>stretch</bred>|de haut en bas, espace en bas si non fini||
|$\,$|flex-start|de haut en bas, espace en bas si non fini||
|$\,$|flex-end|de bas en haut, espace en haut si non fini||
|$\,$|center|centré sur axe secondaire, espaces autour des éléments||
|$\,$|baseline|de haut en bas, espace en bas si non fini||
|**flex-wrap**<br/>Modifie le comportement des éléments<br/> en cas d'overflow|<bred>nowrap</bred>|les éléments ne vont pas à la ligne en cas d'overflow<br/> Ils tentent de se resserer pour rester sur la même ligne||
|$\,$|wrap|les éléments vont à la ligne en cas d'overflow||
|$\,$|wrap-reverse|les éléments vont à la ligne en cas d'overflow, <br/>en sens inverse||