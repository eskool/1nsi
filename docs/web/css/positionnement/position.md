# 1NSI : CSS -> Position
<!-- https://medium.com/@mautayro/understanding-css-position-display-float-87f9727334b2 -->

La propriété CSS `position` détermine la manière dont un **élément HTML** (typiquement une balise) est positionné sur la page web, ou relativement à un autre item de la page. **Par défaut**, `position` est défini avec la valeur `static`, qui signifie que les éléments de la page (web) sont affichés dans l'ordre où ils apparaissent dans le document (du code source), mais voyons quelques autres styles de position:

## STATIC

`position: static;` : l'élément s'affiche sur la page web dans l'ordre où il apparaît dans le *flux du document*;

!!! danger
    Les propriétés `top`, `right`, `bottom`, `left` et/ou `z-index` n'ont aucun effet lorsqu'utilisées avec `position: static;`

## RELATIVE

`position: relative;` : Similaire à `static` mais compatible avec `top`, `right`, `bottom`, `left`, et `z-index`.

## ABSOLUTE

`position: absolute;` : L'élément est positionné **relativement/par rapport à son premier ancêtre non static**. Compatible avec `top`, `right`, `bottom`, `left`, et `z-index` (de même que `relative`).

## FIXED

`position: fixed;` : Similaire à `absolute`, mais positionné relativement à la fenêtre du navigateur. Le scroll n'a aucun effet sur cet élément.

## STICKY

`position:sticky;` (sticky :gb: = collant :fr:) L'élémént est :

* Initialement positionné (au chargement de la page) comme `position:relative;`
* Ensuite, cet élément est fixé à cette position de manière insensible au scroll


