# 1NSI : CSS -> DISPLAY



When you create an element to display on the page, it renders as a box & we have seen how to position those boxes on the page, but there’s more we can do with them. CSS’s Display property specifies the type of rendering box that is created from our HTML. Let’s check out some of the different display styles we can use.

BLOCK — Element starts on a new line & takes up the entire width. May contain other block or inline elements. Elements that are block-level by default include <div>, <p>, <h1>-<h6>, <ul>, <li>, & <canvas>.

INLINE —Element can start anywhere on an existing line. Height and width properties have no effect. Elements that are inline by default include <span>, <input>, <button>, &<img>.

GRID — Element is displayed block-level with inner content in grid layout.

FLEX — Element is displayed block-level with inner content in flexbox layout.

INLINE-BLOCK — Element is displayed inline but height and width may be defined. May also be used with grid, flex, or table as these are block-level elements.

NONE — Removes this element & all children (to just make an item invisible but still take up space on the page use the visibility property)

It can be a lot to digest at first, but once you take some time to study display & positioning styles, layout isn’t that hard!

Take the time at the beginning of your project to lay everything out on paper or in your head and think about what display and positioning each item should have. For each element determine whether you want a block-level or an inline element, and then what type of positioning you want it to have on the page.

One more layout tip for the road: CSS’s float property: