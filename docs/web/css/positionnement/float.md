FLOAT

While float defaults to none, setting this property to left or right ‘floats’ an element to the left or right of a container respectively, wrapping the rest of the content around it.

Consider an inline element such as an <img> nested within a <p>. Setting the float value to ‘left’ will put the image on the left side & the text to the right, wrapping to the full line below where the image ends.

Setting a clear property on another element (such as a <p> near a floated <img>) will disallow floating on one or both sides.
