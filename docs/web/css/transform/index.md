# 1NSI : CSS `transform` avec `translate`, `rotate`, `scale`, `skew`, et `matrix`

<iframe height="800" style="width: 100%;" scrolling="no" title="transform" src="https://codepen.io/nsiperier/embed/qBeVjGZ?default-tab=html%2Cresult&editable=true" frameborder="no" loading="lazy" allowtransparency="true" allowfullscreen="true">
  See the Pen <a href="https://codepen.io/nsiperier/pen/qBeVjGZ">
  transform</a> by nsiperier (<a href="https://codepen.io/nsiperier">@nsiperier</a>)
  on <a href="https://codepen.io">CodePen</a>.
</iframe>

Quelques valeurs possibles pour la propriété CSS `transform` :

```css
#boite1 {
    border: 4px solid;
    width: 250px;
    height: 250px;
    font-size: 120px;
    text-align: center;
    line-height: 250px;
    background-color: red;

    transform: translateX(100px);
    transform: translateY(100px);
    transform: translate(100px, 100px); /* (translateX,translateY) */
    transform: rotateX(180deg);
    transform: rotateY(180deg);
    transform: rotateZ(180deg);
    transform: scaleX(2);     /* 1 par défaut */
    transform: scaleY(2);
    transform: scale(2, 2);   /* (scaleX,scaleY) */
    transform: skewX(45deg);
    transform: skewY(45deg);
    transform: skew(20deg, 20deg);  /* (skewX,skewY) */ 
    /* Remarque : skewX+skewY != 90° sinon, linéaire donc disparaît..attention */
    transform: matrix(1,0,0,1,0,0);
    /* matrix(scaleX, skewX, skewY, scaleY, translateX, translateY) */
    transform: matrix(1.5,0,0,1.5,200,200);
  
}
```
