# 1NSI : Unités

## Unités Absolues

| Unité | Description |
|:-:|:-:|
| `cm` | centimètres |
| `mm` | millimètres |
| `in` | inches / pouces<br/>$1 in = 96 px = 2,54 cm$ |
| `px` | pixels (1 px = 1/96 de 1 pouce = 1/96 de 1 in) |
| `pt` | points (1 pt = 1/72 de 1 pouce = 1/72 de 1 in) |
| `pc` | picas (1 pc = 12 pt) |

!!! remark
    * Les pixels (px) sont relatifs au dispositif (viewing device)
    * Pour les dispositifs à faible résolution (faibles valeur en ppp (=points par pouce) /dpi (=dots per inch)) : **1 px vaut 1 pixel d'écran (un point/dot)**.
    * Pour les imprimantes et les dispositifs à haute résolution (hautes valeurs en ppp/dpi): **1px représente plusieurs pixels d'écran**.

## Unités Relatives

| Unité | Description |
|:-:|:-:|
| `em` | Relatif à la taille de la police de l'élément<br/>(`2em` signifie $2$ fois la taille de la police courante) |
| `ex` | Relatif à la **taille en x** de la police courante (rarement utilisé) |
| `ch` | Relatif à la **largeur (width)** du caractère `0` (zéro) |
| `rem` | Relatif à la taille de la police de l'élément `root` |
| `vw` | Relatif à 1% de la **largeur (width)** du *viewport* |
| `vh` | Relatif à 1% de la **hauteur (height)** du *viewport* |
| `vmin` | Relatif à 1% de la dimension **minimale** du *viewport* |
| `vmax` | Relatif à 1% de la dimension **maximale** du *viewport* |
| `%` | (pourcentage) Relatif à l'**élément parent** |

!!! tip
    Sont conseillées les unités suivantes, car elles sont mieux *scalables* :

    * `em`
    * `rem`

!!! def
    Le <red>viewport</red> est la taille de la fenêtre du navigateur.
    Si le *viewport* est 50cm, alors 1vw = 0.5cm