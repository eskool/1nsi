# 1NSI: CSS `transition` property

<iframe height="800" style="width: 100%;" scrolling="no" title="transition color and rotate" src="https://codepen.io/nsiperier/embed/zYgPRvd?default-tab=html%2Cresult&editable=true" frameborder="no" loading="lazy" allowtransparency="true" allowfullscreen="true">
  See the Pen <a href="https://codepen.io/nsiperier/pen/zYgPRvd">
  transition color and rotate</a> by nsiperier (<a href="https://codepen.io/nsiperier">@nsiperier</a>)
  on <a href="https://codepen.io">CodePen</a>.
</iframe>