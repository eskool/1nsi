# 1NSI : CSS `animation` avec 

## `animation` avec `@keyframes` puis `from` et `to`

### `animation` lancée *automatiquement*

La propriété CSS `animation` est une <red>super-propriété</red>. On peut donc l'utiliser de deux manières :

* Avec des propriétés CSS `animation-(quelquechose)` séparées en plusieurs lignes :
    * `animation` 
    * `animation-duration` 
    * `animation-timing-function` 
    * `animation-delay`
    * `animation-iteration-count`
    * `animation-play-state`
* ou bien avec la <red>super-propriété</red> `animation` (tout en un)

<iframe height="800" style="width: 100%;" scrolling="no" title="Untitled" src="https://codepen.io/nsiperier/embed/abeVyVB?default-tab=html%2Cresult&editable=true" frameborder="no" loading="lazy" allowtransparency="true" allowfullscreen="true">
  See the Pen <a href="https://codepen.io/nsiperier/pen/abeVyVB">
  Untitled</a> by nsiperier (<a href="https://codepen.io/nsiperier">@nsiperier</a>)
  on <a href="https://codepen.io">CodePen</a>.
</iframe>

Résumé des propriétés `animation` avec `from` et `to` :

```css
#boite1 {
    /* 2°) Appliquer l'animation pré-définie */
    /* Par Propriétés Séparées */
    animation: monAnimation;        /* nom de l'animation à appliquer. DOIT ÊTRE LA PREMIERE LIGNE */
    animation-duration: 3s;         /* durée de l'animation */
    animation-timing-function: ease; /* fonction de répartition temporelle de l'animation */ 
    /* Valeurs : linear; ease-in; ease-out; ease; ou perso avec 'cubic-bezier' : https://cubic-bezier.com */
    /* animation-timing-function: cubic-bezier(0,1.03,1,.53); */
    animation-delay: 0s;             /* délai AVANT QU'ELLE SOIT LANCÉE */
    animation-iteration-count: infinite; /* nombre de fois que l'animation est répétée : 1, 2, 3,.., infinite */
    animation-play-state: running;         /* état de l'animation : par défaut 'running'. Autres valeurs: paused */

    /* super Propriété animation */
    /* animation: 3s linear 0s infinite running monAnimation; */
    /* Ordre:  duration, timing-function, delay, iteration-count, play-state, name */
}

/* 1°) Définition de l'Animation */
@keyframes monAnimation {  /* nom Unique */
    from {margin-left:100%;} /* Valeurs de Propriétés Initiales */
    to {margin-left:0%;}   /* Valeurs de Propriétés Finales */
    
}
```

### `animation` avec pseudo-classe (par ex., lancée au clic `:active`)

<iframe height="800" style="width: 100%;" scrolling="no" title="animation avec from + to , lancée au clic" src="https://codepen.io/nsiperier/embed/abeVLZE?default-tab=html%2Cresult&editable=true" frameborder="no" loading="lazy" allowtransparency="true" allowfullscreen="true">
  See the Pen <a href="https://codepen.io/nsiperier/pen/abeVLZE">
  animation avec from + to , lancée au clic</a> by nsiperier (<a href="https://codepen.io/nsiperier">@nsiperier</a>)
  on <a href="https://codepen.io">CodePen</a>.
</iframe>

```css
#boite1 {
    border: 4px solid;
    width: 250px;
    height: 250px;
    font-size: 120px;
    text-align: center;
    line-height: 250px;
    background-color: red;

}

#boite1:active {
    /* super Propriété animation */
    animation: 3s linear 0s infinite running monAnimation;
    /* Ordre:  duration, timing-function, delay, iteration-count, play-state, name */
}


/* 1°) Définition de l'Animation */
@keyframes monAnimation {  /* nom Unique */
    from {margin-left:0%;} /* Valeurs de Propriétés Initiales */
    to {margin-left:100%;}   /* Valeurs de Propriétés Finales */
    
}
```

!!! warning
    L'animation s'arrête néanmoins **si vous lâchez le clic de la souris**...

## `animation` avec pourcentages

### `rotateX` avec pourcentages

<iframe height="800" style="width: 100%;" scrolling="no" title="animation avec @keyframes et pourcentages" src="https://codepen.io/nsiperier/embed/abeVLVG?default-tab=html%2Cresult&editable=true" frameborder="no" loading="lazy" allowtransparency="true" allowfullscreen="true">
  See the Pen <a href="https://codepen.io/nsiperier/pen/abeVLVG">
  animation avec @keyframes et pourcentages</a> by nsiperier (<a href="https://codepen.io/nsiperier">@nsiperier</a>)
  on <a href="https://codepen.io">CodePen</a>.
</iframe>

### `rotateY` avec pourcentages

<iframe height="800" style="width: 100%;" scrolling="no" title="animation RotateY avec @keyframes et pourcentages" src="https://codepen.io/nsiperier/embed/YzmEYpx?default-tab=html%2Cresult&editable=true" frameborder="no" loading="lazy" allowtransparency="true" allowfullscreen="true">
  See the Pen <a href="https://codepen.io/nsiperier/pen/YzmEYpx">
  animation RotateY avec @keyframes et pourcentages</a> by nsiperier (<a href="https://codepen.io/nsiperier">@nsiperier</a>)
  on <a href="https://codepen.io">CodePen</a>.
</iframe>

### `rotateZ` avec pourcentages

<iframe height="800" style="width: 100%;" scrolling="no" title="animation RotateZ avec @keyframes et pourcentages" src="https://codepen.io/nsiperier/embed/ExqboWK?default-tab=html%2Cresult&editable=true" frameborder="no" loading="lazy" allowtransparency="true" allowfullscreen="true">
  See the Pen <a href="https://codepen.io/nsiperier/pen/ExqboWK">
  animation RotateZ avec @keyframes et pourcentages</a> by nsiperier (<a href="https://codepen.io/nsiperier">@nsiperier</a>)
  on <a href="https://codepen.io">CodePen</a>.
</iframe>

### `opacity` avec pourcentages

<iframe height="800" style="width: 100%;" scrolling="no" title="animation Opacity avec @keyframes et pourcentages" src="https://codepen.io/nsiperier/embed/QWeOavo?default-tab=html%2Cresult&editable=true" frameborder="no" loading="lazy" allowtransparency="true" allowfullscreen="true">
  See the Pen <a href="https://codepen.io/nsiperier/pen/QWeOavo">
  animation Opacity avec @keyframes et pourcentages</a> by nsiperier (<a href="https://codepen.io/nsiperier">@nsiperier</a>)
  on <a href="https://codepen.io">CodePen</a>.
</iframe>

### `scale` avec pourcentages

<iframe height="800" style="width: 100%;" scrolling="no" title="animation scale avec @keyframes et pourcentages" src="https://codepen.io/nsiperier/embed/dyxZJZV?default-tab=html%2Cresult&editable=true" frameborder="no" loading="lazy" allowtransparency="true" allowfullscreen="true">
  See the Pen <a href="https://codepen.io/nsiperier/pen/dyxZJZV">
  animation scale avec @keyframes et pourcentages</a> by nsiperier (<a href="https://codepen.io/nsiperier">@nsiperier</a>)
  on <a href="https://codepen.io">CodePen</a>.
</iframe>

### `background-color` avec pourcentages

Dans cet exemple, on anime des changements de couleur d'un élément `#boite1`, en précisant quelles sont ses valeurs de couleurs pour un certain pourcentage d'avancement de l'animation.

<iframe height="800" style="width: 100%;" scrolling="no" title="animation Background-color avec @keyframes et pourcentages" src="https://codepen.io/nsiperier/embed/bGXYarO?default-tab=html%2Cresult&editable=true" frameborder="no" loading="lazy" allowtransparency="true" allowfullscreen="true">
  See the Pen <a href="https://codepen.io/nsiperier/pen/bGXYarO">
  animation Background-color avec @keyframes et pourcentages</a> by nsiperier (<a href="https://codepen.io/nsiperier">@nsiperier</a>)
  on <a href="https://codepen.io">CodePen</a>.
</iframe>

