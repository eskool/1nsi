# 1NSI : Votre Site en Ligne sur Gitlab Pages

Pour aller plus loin dans votre projet Web, vous pouvez décider de le placer sur un serveur en ligne, afin que celui-ci soit accessible totalement publiquement et gratuitement.

## Installez et configurer le logiciel `git` sur votre ordinateur

Les liens de téléchargement sont disponibles à cette adresse :

* [https://git-scm.com/downloads](https://git-scm.com/downloads)

!!! danger
    :warning: ATTENTION :warning: ce logiciel est DÉJÀ INSTALLÉ au Lycée. Ne le réinstallez pas par mégarde.

## Créer un compte sur Gitlab

Rendez sur la page suivante et créez un compte gratuit.

* [https://gitlab.com/](https://gitlab.com/)

## Forker le répertoire suivant

Après création de votre compte Gitlab, connectez-vous sur Gitlab, puis forkez le répertoire suivant (càd faites en une copie, qui vous appartiendra à vous uniquement, sur votre compte Gitlab) :

* [https://gitlab.com/lyceeperier/siteenligne](https://gitlab.com/lyceeperier/siteenligne)


!!! method
    Il suffit pour cela de :
    
    * **cliquer sur le bouton** suivant <img src="./fork.png" />
    * Renseigner le formulaire qui s'ouvre de la manière suivante :
        * **Project name** : choisissez le nom de votre projet/site (simple)
        * **Project URL** :
            * `https://gitlab.com/ <Select a namespace>`
            * Sélectionner le `namespace` dans le menu déroulant : choisir `<votreNomUtilisateur>` (le seul `namespace` existant pour le moment, normalement)
            * Project slug : Choisissez un nom (simple), **pas d'espaces**. Un slug est un nom qui apparaîtra à la fin de votre URL, derrière le slash `/`. Il aura une importance car votre site sera accessible sur l'URL : `https://<votreNomUtilisateurGitlab>.gitlab.io/<votreSlug>`
        * **Branches to include** : `All branches` (laisser cette option par défaut)
        * **Visibility level** : `Public` (laisser cette option par défaut)
        * Enfin, cliquer sur `Fork project`

Ou bien, cloner le site avec une commande commande Git :

Une fois le logiciel git installé et configuré sur votre ordinateur (il est déjà installé au Lycée, mais je vous conseille de le faire depuis votre domicile, en raison des configurations assez personnelles, ou sur votre ordinateur personnel), vous pouvez cloner le dépôt Gitlab avec la commande :

* `git clone git@gitlab.com:lyceeperier/siteenligne.git`

## Placer tout votre site dans le dossier `public`

## Télécharger sur Gitlab

## Personnalisation de votre URL sur Gitlab

!!! warning
    Par défaut, Gitlab est configuré pour créer un site avec une URL aléatoire (pas forcément ce que voulez..), inspirée de votre nom d'utilisateur sur Gitlab, mais "aléatoire", quelque chose qui devrait ressembler à ceci :

    * `https://<NomDuProjetSurGitlab>-XXXXXXX.gitlab.io`

!!! method
    Pour connaître précisément à quelle URL est accessible votre site, et la modifier, vous pouvez devez un **paramètre important** dans les **paramètres de votre projet** sur Gitlab (menu de gauche) :

    * `Deploy -> Pages`
    * Dans la partie **Settings**, décocher **use unique domain**, puis sauvegarder avec le bouton bleu **Save Changes** (conserver le **Force https**)
    * Votre URL a d^être modifiée, vous pouvez lire l'URL à laquelle votre site est accessible publiquement dans **Access pages**. Elle devrait maintenant ressembler à quelque chose comme `https://<VotreNomUtilisateur>.gitlab.io/<NomDuProjetSurGitlab>`
    * La prise en compte de la modification prend quelques dizaines de minutes... Patientez.

