# 1NSI : Projet Web HTML & CSS

## Travail en Classe et à la maison

En ce qui concerne le Travail, en classe et à la maison, l'enseignant prendra en compte dans l'évaluation les points suivants, à hauteur de <red>$4$ points sur $20$</red>: 

* Le <rbl>travail</rbl> en classe, et sa mesure effective par l'enseignant (en classe) durant le projet
* le <rbl>sérieux</rbl>, 
* l'<rbl>autonomie</rbl> 
* l'<rbl>investissement</rbl> 
* la <rbl>prise d'initiative</rbl>
* la <rbl>volonté et la capacité d'aller plus loin</rbl> que les simples consignes minimales demandées.

## Compte Rendu de Projet

### C'est quoi ?

Un <rbl>Compte Rendu de Projet</rbl> est en pratique un **fichier texte**, à rendre <rbl>en plus de vos autres fichiers</rbl> de votre site. En ce qui concerne le format de ce fichier, vous pouvez choisir, à votre convenance (comme vous préférez) n'importe lequel des formats suivants :

* (idéalement) au format **markdown** (.md) directement sur **VisualStudio Code** 
* ou, **prioritairement**, dans un format libre  (**Onlyoffice**/**LibreOffice** `.odt`, ou autres..)
* ou, les formats propriétaires sont tolérés (**Microsoft Word** `.docx`, et/ou **Apple Pages** `.Pages`)

### Que dois-je écrire dedans ?

Le Compte Rendu de Projet doit **détailler** les éléments suivants, et/ou **répondre aux questions** suivantes:

* la **Décomposition des Tâches** au sein de votre groupe :
    * Déterminer quelles sont les différentes **tâches** et **sous-tâches** pour les fonctionnalités de votre site, au sein de votre groupe, pour venir à bout de votre projet.
    * **Répartition des tâches** dans le groupe: Qui a fait quoi (précisément) ? en sachant que **chaque élève** doit avoir codé au minimum une page HTML **ET** une partie (assez longue) du code CSS
* l'**Organisation Temporelle = Planning** : Détailler par écrit, le temps que vous a pris chaque tâche et sous-tâche, pour **chaque séance** et **chaque personne**.
    * On devra également faire un/des **[Diagrammes de Gantt](https://www.gantt.com/fr/)**

    ![Diagramme de Gantt](./gantt.jpg)
    
    * Comparaison détaillée entre le planning initialement prévu et le planning réel : Quelles différences notables avez-vous remarqué entre les deux? pourquoi ?

* Quelle est la **méthode choisie** pour partager vos codes sources ? Entre l'école et la maison ? Entre vous, lorsque vous êtes à la maison?
* Quels sont les **problèmes rencontrés** durant le projet? et comment les avez-vous surmontés?
* Quelles sont les **améliorations possibles** de votre site que vous n'avez pas eu le temps de finir ?
* Plus généralement, toute autre information détaillée permettant de se faire une idée :
    * du temps que vous avez passé à traiter (ou tenter de traiter) certaines tâches, 
    * de votre investissement et sérieux.

### Notation du Compte Rendu

* Le **Compte Rendu de Projet** est pris en compte dans la notation de projet à hauteur de <red>$4$ points sur $20$</red>.
* La **clarté de l'expression** et des **illustrations** sont également prises en compte dans la notation.

On demande un compte rendu de projet par groupe (si le groupe contient une personne, cette personne doit rendre son propre compte rendu de projet)

## Cahier des Charges du site Web

Pour l'ensemble du site web :

* Le site doit contenir un **menu de navigation**, avec le mode de positionnement **Flexbox**, avec des **sous-menus déroulants au survol** (ou **au clic**). On pourra, pour cette partie, s'inspirer de codes sources publics sur internet.
* le site doit contenir **plusieurs pages HTML**
* le site doit contenir (au moins une fois) toutes les fonctionnalités HTML et CSS vues en classe, notamment :
    * <bad>pour l'HTML</bad>
        * Un **Header** avec la balise `header`
        * un **Footer** avec la balise `footer`
        * les autres éléments de positionnement HTML5 : `nav`, `section`, aside, etc..
        * Des **balises de titres** et **sous-titres** : `h1`, `h2`, etc..
        * Des **balises de paragraphes** : `p`
        * Des **listes non ordonnées** `ul` et des **listes ordonnées** `ol`
        * des **liens hypertextes / hyperliens** avec la balise `a` :
            * des hyperliens *classiques* (menant vers des pages externes et/ou internes au site)
            * des ancres sur une très loooongue page HTML,
            * **au minimum** un **téléchargement** de fichier (pas de fichier .txt, ni d'image, plutôt un fichier `.odt`, ou `.docx`, ou un fichier compressé `.zip`, `.rar`, etc..)
        * **au minimum** une **intégration/embed** via un `iframe` :
            * une **Intégration de vidéo** située sur une plateforme de streaming (YouTube, DailyMotion, Vimeo, etc..)
            * **Plan Google Maps** ou **OpenStreetMap** (OpenSource)
            * toute autre intégration, avec un `iframe`
        * **au minimum** une **intégration** d'un fichier **audio** placé en local sur votre site, avec la balise `audio`
        * Des **images** avec la balise `img`. :info: Tout travail de **création** et/ou de **retouche d'image** devra être détaillé avec précision dans le compte rendu de projet.
        * Un **formulaire de contact** avec un **bouton d'envoi**, contenant obligatoirement :
            * Des `label` + des `input` : pour entrer des informations sous forme de texte (Prénom, Nom, adresse, etc..)
            * Des **Checkboxs** / Cases à cocher: plusieurs sélections possibles
            * des **Radios boutons** : une seule sélection possible
            * des **menus déroulants** : une seule sélection parmi une liste dans le menu
            * etc.. (il en existe d'autres)
            * Avec un bouton `button` d'envoi (la page/fichier -php/js- de réception des données est fictif)
        * un **Tableau HTML**
    * <bad>pour le CSS</bad>
        * Textes : insérez des caracères gras, des tailles différentes, des soulignements, des italiques, etc..
        : Couleurs : Différentes couleurs de textes, et de couleurs de fond, distinctes au moins sur certaines pages html.
        * Bordures : en insérer quelques unes / partout
        * Ombres : en insérer quelques unes / sur tous les encadrés / autour des images
        * Effets de survol avec `:hover`. Notamment des effets d'agrandissement d'images (ou d'autres éléments) au survol.
        * **Polices de caractère personnalisée** (gratuite) pour (au minimum) certains titres/éléments (on pourra par exemple utiliser un service comme *Google Fonts*, *Squirrel fonts*, ou tout autre service équivalent). La police personnalisée doit être intégrée (obligatoirement) par la méthode CSS `@import` (et non pas par intégration d'une balise HTML `link`)
        * Un **menu de navigation** du site en **Flexbox** obligatoirment (bis repetita)
        * le CSS de vos pages HTML doit être **Responsive Design** grâce aux **Media Queries**
        * Créer des **Animations CSS**

    * **Intégration** d'icones via une/des **librairies CSS**, telle que, par exemple :
        * [FontAwesome](https://fontawesome.com/), 
        * [Ionicons](https://ionic.io/ionicons/), 
        * ou toute autre équivalente (choisissez)

!!! remarque
    Vous penserez à détailler très précisément tout travail supplémentaire que vosu auriez fait personnellement, et qui n'apparaît pas dans la liste précédente. Par exemple, la création et/ou la retouche d'images. Avec quel.s logiciel.s ? Quels traitements précisément ? etc..

