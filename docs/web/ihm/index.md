# 1NSI : Notion d'IHM sur une page Web

## Notion d'IHM sur une page Web

## Événements sur une page Web

## Cas d'un clic sur un bouton `<button>`

```html
<button id="monbouton">Appuyer ici</button>
```

```js
const bouton = document.querySelector("#bouton");
bouton.addEventListener("clic", () => {
    console.log("Bien ! Tu as cliqué !")
})
```



