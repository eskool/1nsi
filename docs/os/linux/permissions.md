# 1NSI : Permissions / Droits des Dossiers et Fichiers Linux

Chaque dossier / fichier est la propriété :

* d'un **Utilisateur**, dit **propriétaire**
* d'un **Groupe**, dit **propriétaire**

En fait, chaque fichier/dossier dispose d'une liste de **permissions** ou **droits** qui déterminent ce qu'**un utilisateur / un groupe** a le droit de faire avec ce **fichier** / **dossier**.

## Représentation Symbolique `ugo` des Utilisateurs

Plus précisément, pour un fichier/dossier spécifique, on définit usuellement les définitions suivantes:

<center>

| Notation | Désigne... | Détails |
| :-: | :-: | :-: |
| `u` | Utilisateur Propriétaire | l'Utilisateur Propriétaire du Fichier / Dossier |
| `g` | Groupe Propriétaire | le Groupe Propriétaire du Fichier / Dossier |
| `o` | les Autres<br/>(le reste du monde) <br/>(**O**thers) :uk: | toute autre personne ou groupe,<br/> autre que `u` et <br/>ne faisant pas partie de `g` |
| `a` | Tous (**A**ll :uk:) | Tout utilisateur et/ou groupe <br/> donc l'addition des 3 précédents<br/> `a = u+g+o` |

</center>

## Représentation Symbolique `rwx` des Droits des Fichiers et Dossiers

### Notation Longue des droits d'un fichier/dossier

La notation longue détaillant les droits d'un Fichier/Dossier se décomposent en 4 parties:

  ```dot
  digraph G {
    subgraph cluster0 {
      "rwxOther" [label="3 caractères pour \n Other \n o" shape=box style=filled fillcolor=red];    
      "rwxGroup" [label="3 caractères pour \n Group \n g" shape=box style=filled   fillcolor=orange];
      "rwxUser" [label="3 caractères pour \n User \n u" shape=box style=filled fillcolor=green];
      "d" [label="1 caractère pour \n Type de Fichier \n - / d/ l / etc..." shape=box];
    }
  }
  ```

* 1 caractère pour le type de fichier/dossier
* 3 composantes: 
  * 1 composante pour `u` contenant 3 caractères, et détaillant les droits de l'Utilisateur Propriétaire du Fichier/Dossier
  * 1 composante pour `g` contenant 3 caractères, et détaillant les droits du Groupe Propriétaire du Fichier/Dossier
  * 1 composante pour `o` contenant 3 caractères, et détaillant les droits des Autres (reste du monde) sur ce Fichier/Dossier

### 1 caractère pour le type de Fichier/Dossier

Pour désigner le type de fichier/dossier, on utilise 1 caractère : 

<center>

| caractère | Signification | Détails |
|:-:|:-:|:-:|
| `-` | **(vide)** | pour désigner un **Fichier régulier** |
| `d` | **<rb>D</rb>irectory** | pour désigner un **Répertoire / Dossier** |
| `l` | **<rb>L</rb>ink** | pour désigner un **Lien Symbolique** (/raccourci) |
| `c` | **<rb>C</rb>aracter** | pour désigner un **périphérique de type caractère** |
| `b` | **<rb>B</rb>lock** | pour désigner un **périphérique de type bloc** |
| `p` | **FIFO** | pour désigner un **FIFO** |
| `s` | **<rb>S</rb>ocket** | pour désigner une **Socket** |

</center>

### 3 flags `r`, `w` et `x` pour les droits de Fichiers/Dossiers

<center>

| caractère | Signification | Détails |
|:-:|:-:|:-:|
| `r` | **<rb>R</rb>ead** <br/>= droit de **lecture**<br/>= droit d'**ouverture** | <ul style="text-align:left;"> <li>d'un fichier : comprendre ***lire le contenu du fichier avec un logiciel/Terminal*** </li> <li style="float:left;"> d'un dossier : comprendre ***afficher/lister son contenu: noms, tailles, etc.. <br/>des fichiers/sous-dossiers***</li></ul> <br/><br/> ou bien, *un tiret ```-``` pour indiquer <b>PAS de droit de lecture</b>* |
| `w` | **<rb>W</rb>rite** <br/>= droit d'**écriture** | <ul style="text-align:left;"> <li>d'un fichier : comprendre ***modifier le contenu du fichier avec un logiciel/Terminal, <br/>*** et aussi ***droit de le vider de son contenu*** </li> <li> d'un dossier : comprendre ***autorise la création, suppression, changement de nom<br/>des fichiers inclus dans le dossier - quels que soient les droits d'accès `x` au dossier*** </li></ul> ou bien, *un tiret ```-``` pour indiquer <b>PAS de droit d'écriture</b>* |
| `x` | **e<rb>X</rb>ecute** <br/>= droit d'**exécution** | <ul style="text-align:left;"> <li>d'un fichier : comprendre ***lorsque cela a un sens,*** par exemple, ***s'il s'agit d'un <br/>fichier exécutable (programme ou script)*** </li> <li> d'un dossier : comprendre ***droit d'accès/d'entrée dans le dossier, ou d'ouverture du dossier***</li></ul> ou bien, *un tiret ```-``` pour indiquer <b>PAS de droit d'exécution</b>* |

</center>

On dit quelquefois que `r`, `w`, et `x` sont des <rb>flags</rb> ou <rb>drapeaux</rb>.

<env>**Aller plus loin : Droits Spéciaux (*CULTURE*)**</env> (à la place de `x` dans `u`, `g`, `o` )  
Il existe d'autres flags, ayant une signification spéciale, lorsqu'ils remplacent `x` à sa position dans `u`, `g`, `o`

<center>

| Caractère Spécial | remplace `x` dans ... | Signification | Détails |
|:-:|:-:|:-:|:-:|
| `s` ou `S` | `u` (User) <br/>ici : **s = <rb>S</rb>etUID**<br/>Exemple : `-rwsr-xr-x` | **le fichier/dossier doit être exécuté <br/> avec les droits de l'Utilisateur Propriétaire** | Exemple :  C'est le cas du binaire (fichier exécutable) <br/>`sudo` situé dans `/usr/bin` : <br/>`-rwsr-xr-x 1 root root 158K 12 mai 15:14 sudo` |
| ^ | `g` (Group) <br/>ici : **s = <rb>S</rb>etGID**<br/>Exemple : `-rwxr-sr-x` | **le fichier/dossier doit être exécuté <br/> avec les droits du Groupe Propriétaire** | Exemple : C’est le cas du binaire (fichier exécutable) <br/>`locate` situé dans `/usr/bin` : <br/>`-rwxr-sr-x 1 root locate 43K 2 avril 19:02 locate` |
| `t` ou `T`<br/>où **t = s<rb>T</rb>icky Bit** | `o` (Others) <br/>Exemple : `drwxrwxrwt` | **seul l’Utilisateur propriétaire du<br/> fichier/dossier peut le supprimer** | Exemple :  C’est le cas du répertoire `/tmp` : <br/>`drwxrwxrwt 18 root root 660 17 juin 15:20 tmp` |

</center>

!!! exp
    Voici un répertoire/dossier (à cause de la 1ère lettre `d`) sur lequel chaque composante U, G et O a tous les droits (R+W+X = Lecture+Écriture+Exécution): ```drwxrwxrwx```

    On se donne les droits ```drwxrwxrwx``` pour un fichier/dossier.
    On peut dire que :

    * le 1er caractère égal à ```d``` $\Rightarrow$ il s'agit d'un **dossier**
    * les 3 caractères suivants sont ```rwx``` $\Rightarrow$ l'Utilisateur Propriétaire U a tous les droits sur le dossier (R+W+X = Lecture+Écriture+Exécution):
    * les 3 caractères suivants sont ```rwx``` $\Rightarrow$ le Groupe Propriétaire G a tous les droits sur le dossier (R+W+X = Lecture+Écriture+Exécution):
    * les 3 caractères suivants sont ```rwx``` $\Rightarrow$ les Autres O ont tous les droits sur le dossier (R+W+X = Lecture+Écriture+Exécution):

    ```dot
    digraph G {
      subgraph cluster0 {
        style="";
        "d" [shape=box];
        "rwxUser" [label="rwx" shape=box style=filled fillcolor=green];
        "rwxGroup" [label="rwx" shape=box style=filled fillcolor=orange];
        "rwxOther" [label="rwx" shape=box style=filled fillcolor=red];
        {
          rank=same;
          "d"->"rwxUser"->"rwxGroup"->"rwxOther" [style=invis];
        }
      }

      subgraph cluster1 {
        style="invis";
        "typeOfFile" [label="type of file: \n d/l/-" shape=box];
        "User" [shape=box fillcolor=green];
        "Group" [shape=box fillcolor=orange];
        "Other" [shape=box fillcolor=red];
      }

      "d" -> "typeOfFile";
      "rwxUser" -> User;
      "rwxGroup" -> Group;
      "rwxOther" -> Other;
    }
    ```

!!! exp
    On se donne les droits ```-rwxr-xr--``` pour un fichier/dossier.
    On peut dire que :

    * le 1er caractère égal à un tiret ```-``` $\Rightarrow$ il s'agit d'un **fichier**
    * les 3 caractères suivants sont ```rwx``` $\Rightarrow$ l'Utilisateur Propriétaire U a tous les droits sur le fichier (R+W+X = Lecture+Écriture+Exécution):
    * les 3 caractères suivants sont ```r-x``` $\Rightarrow$ le Groupe Propriétaire G a des droits sur le fichier (R+X = Lecture+Exécution):
    * les 3 caractères suivants sont ```r--``` $\Rightarrow$ les Autres O ont des droits sur le fichier (R = Lecture):

    ```dot
    digraph G {
    subgraph cluster0 {
        "-" [shape=box];
        "rwxUser" [label="rwx" shape=box style=filled fillcolor=green];
        "rwxGroup" [label="r-x" shape=box style=filled fillcolor=orange];
        "rwxOther" [label="r--" shape=box style=filled fillcolor=red];
        {
        rank=same;
        "-"->"rwxUser"->"rwxGroup"->"rwxOther" [style=invis];
        }
    }

    subgraph cluster1 {
        style="invis";
        "typeOfFile" [label="type of file: \n d/l/-" shape=box];
        "User" [shape=box fillcolor=green];
        "Group" [shape=box fillcolor=orange];
        "Other" [shape=box fillcolor=red];
    }

    "-" -> "typeOfFile";
    "rwxUser" -> User;
    "rwxGroup" -> Group;
    "rwxOther" -> Other;
    }
    ```

!!! exp
    ```bash
    $ ls -l
    [...]
    drwxr-xr--  Musique emma emma    
    ```

    * `d` donc désigne un dossier, nommé **Musique**, dont:
    * l'Utilisateur Propriétaire (U) est **emma**
    * le Groupe Propriétaire (G) est **emma**
    De plus:
    * U=RWX (les 3 premiers) => L'Utilisateur Emma a les droits de Lire ( R ) et d'Ecrire (W) et d'Exécuter ce dossier (d'entrer dedans): les 3 premiers RWX
    * G=R-X (les 3 du milieu) => Le Groupe Emma a les droits de Lire ( R ), mais PAS d'Ecrire (un - à la place du W), néanmoins il peut Exécuter ce dossier (d'entrer dedans)
    * O=R-- (les 3 derniers) => Tous Les Autres (le reste du monde) ont les droits de Lire ( R ), et c'est tout (- donc pas le droit d'écrire, et un deuxième - donc pas le droit d'exécuter non plus)

<env>Remarque</env>  
Usuellement, mais non obligatoirement, les droits d'un fichier/dossier spécifique vont dans le sens décroissant (au sens large, c'est à dire avec égalité possible) du "plus grand droit" pour User (U) vers le "plus petit droit" pour Other (O = le reste du monde). Le contraire, est informatiquement possible, mais intuitivement bizarre/à bien réfléchir...

## Représentation Octale des droits

<env>**Principe de la Représentation Octale**</env>

* La représentation Octale des droits d'un fichier/dossier est une manière de représenter les droits de ce fichier/dossier, mais **en utilisant des nombres entiers compris entre 0 et 7 inclus** (donc en base 8: en octal), au lieu des flags r,w,x.
* Chacune des 3 composantes u, g et o se traduit en un nombre entre 0 et 7.
* Les droits d'un fichier/dossier peuvent donc s'écrire en octal, avec 3 nombres entiers entre 0 et 7
Exemple: 777 ou bien 750 ou bien 644, etc..
* Plus précisément, les flags r,w et x se traduisent **de manière additive** selon la convention suivante :

<center>

|Droit|Chiffre|
|:-:|:-:|
|**r**|**4**|
|**w**|**2**|
|**x**|**1**|

</center>

d'où le **Tableau de Conversion :**

<center>

| Notation Symbolique RWX | Notation Octale | Détail Calcul | Permissions |
|:--:|:--:|:--:|:--:|
|rwx|7 |= 4+2+1| Lire, Écrire, et Éxécuter |
|rw\-|6 |= 4+2+0| Lire, et Écrire |
|r\-x|5 |= 4+0+1| Lire, et Éxécuter |
|r\-\-|4 |= 4+0+0| Lire |
|\-wx|3 |= 0+2+1| Écrire, et Éxécuter |
|\-w\-|2 |= 0+2+0| Écrire |
|\-\-x|1 |= 0+0+1| Éxécuter |
|\-\-\-|0 |= 0+0+0| (Aucun Droit) |

</center>

!!! ex
    1. Convertir en notation octale, les droits donnés en représentation symbolique `rwx` suivants, correspondant aux droits de certains fichiers/dossiers/etc..

    <center>

    |Notation Symbolique RWX|Notation Octale|Fichier? Dossier?Autre?|
    |:-:|:-:|:-:|
    |```-rwxrwxrwx```|<span hidden>777</span>|<span>&nbsp;</span>|
    |```drwxr-xr-x```|<span hidden>755</span>|<span>&nbsp;</span>|
    |```-rwxrw-r-x```|<span hidden>765</span>|<span>&nbsp;</span>|
    |```-rwxrw-rw-```|<span hidden>766</span>|<span>&nbsp;</span>|
    |```drw-rw-rw-```|<span hidden>666</span>|<span>&nbsp;</span>|
    |```-rw-r-xrw-```|<span hidden>656</span>|<span>&nbsp;</span>|
    |```lrw-r-xr-x```|<span hidden>655</span>|<span>&nbsp;</span>|
    |```drw-r-xr--```|<span hidden>654</span>|<span>&nbsp;</span>|
    |```-r--r--r--```|<span hidden>444</span>|<span>&nbsp;</span>|

    </center>

    2. Convertir en représentaion symbolique `rwx`, les droits donnés en notation octale suivants, correspondant aux droits de certains fichiers.

    <center>

    |Notation Symbolique RWX|Notation Octale|
    |:-:|:-:|
    |<span hidden>rwxrwx---</span>|770|
    |<span hidden>rwxr-xr--</span>|754|
    |<span hidden>rwxr-x---</span>|750|
    |<span hidden>rw-r--r--</span>|644|
    |<span hidden>rw-r-----</span>|640|
    |<span hidden>rw--wx-wx</span>|633|
    |<span hidden>rw--w--w-</span>|622|
    |<span hidden>rw-------</span>|600|
    |<span hidden>r---w----</span>|420|
    |<span hidden>r--------</span>|400|

    </center>

## <rb>chown</rb> = <rb>CH</rb>ange <rb>OWN</rb>er : Modification du Propriétaire de Dossiers et Fichiers

:warning: **ATTENTION :** :warning: Pour changer le propriétaire d'un fichier et/ou dossier, vous DEVEZ être ```root```

!!! exp "Modifier le Propriétaire et le Groupe Propriétaire d'un fichier"
    **Syntaxe : ```chown (droits en octal) nomFichier```**
    
    ```bash
    $ touch monfichier    # créer un fichier
    $ ls -l monfichier    # afficher les droits de monfichier
    $ su
    Mot de Passe:
    # chown newuser:newgroup monfichier       # newuser devient le nouveau propriétaire du fichier/dossier
                                            # newgroup devient le nouveau groupe propriétaire du fichier/dossier
    $ ls -l monfichier      # vérifier les nouveaux droits
    -rw-rw-rw- 1 emma emma 0 27 juin  03:05 monfichier
    ```

## <rb>chmod</rb> = <rb>CH</rb>ange <rb>MOD</rb>ification rights : Modification des droits de Dossiers et Fichiers

:warning: **ATTENTION :** :warning: Par sécurité, Il n'est possible de modifier les droits d'un fichier et/ou d'un dossier QUE :

* si vous êtes le **propriétaire** du fichier/dossier, ou bien
* si vous disposez des droits pour le faire: donc si vous faites partie du **groupe propriétaire**, ET ce groupe dispose des droits de modification (w), ou bien
* si vous faites partie des **autres** (o) du fichier ET que les *autres* (o) ont des droits de modification (w), ou bien
* si c'est ```root``` qui modifie ces droits

### chmod en Octal

!!! exp "Modifier les droits d'un fichier"
    **Syntaxe 1: ```chmod (droits en octal) nomFichier```**
    ```bash
    $ touch monfichier      # créer un fichier texte
    $ ls -l monfichier      # afficher les droits par défaut
    -rw-r--r-- 1 eleve eleve 0 27 juin  03:05 monfichier
    $ chmod 666 monfichier  # Donner des droits 666 pour 'monfichier'
    $ ls -l monfichier      # vérifier les nouveaux droits
    -rw-rw-rw- 1 eleve eleve 0 27 juin  03:05 monfichier
    ```

!!! exp "Modifier NON RÉCURSIVEMENT les droits d'un Dossier, donc SEULEMENT LE DOSSIER"
    **Syntaxe 2: ```chmod (droits en octal) mondossier```**
    Cette syntaxe modifie bien les droits d'un dossier 'mondossier', **MAIS PAS les droits de ses SOUS-DOSSIERS, NI les droits des fichiers inclus dans 'mondossier'**
    ```bash
    $ mkdir mondossier      # créer un dossier 'mondossier'
    $ ls -l                 # afficher les droits de 'mondossier' par défaut
    [...]
    drwxr-xr-x  2 eleve eleve       4096 27 juin  03:21  mondossier
    $ cd mondossier         # entrer dans "mondossier'
    [mondossier]$ mkdir sousdossier # créer un 'sousdossier' dans 'mondossier'
    [mondossier]$ touch monfichier  # créer un fichier 'monfichier' dans 'mondossier'
    [mondossier]$ ls -l             # vérifier les droits de 'sousdossier' et 'monfichier'
    [...]
    -rw-r--r-- 1 eleve eleve    0 27 juin  03:52 monfichier
    drwxr-xr-x 2 eleve eleve 4096 27 juin  03:52 sousdossier
    [mondossier]$ cd ..             # remonte dans le dossier parent /home/eleve
    $ ls -l                         # afficher les droits de 'mondossier'
    [...]
    drwxr-xr-x  3 eleve eleve       4096 27 juin  03:52  mondossier
    $ chmod 777 mondossier  # Donner des droits 777 pour 'mondossier'
    $ ls -l                 # vérifier les nouveaux droits de 'mondossier'
    [...]
    drwxrwxrwx  3 eleve eleve       4096 27 juin  04:16  mondossier
    $ ls -l mondossier         # vérifier que les droits du 'sousdossier' et 'monfichier'
                                # n'ont PAS été modifiés
    -rw-r--r-- 1 eleve eleve    0 27 juin  04:16 monfichier
    drwxr-xr-x 2 eleve eleve 4096 27 juin  04:16 sousdossier       
    ```

!!! exp "Modifier RÉCURSIVEMENT les droits d'un Dossier, donc le DOSSIER ET les SOUS-DOSSIERS ET les FICHIERS"
    **Syntaxe 3 : ```chmod -R (droits en octal) mondossier```**
    Cette syntaxe modifie RÉCURSIVEMENT ( **```-R```** ) les droits d'un dossier 'mondossier', **ET AUSSI les droits de TOUS ses SOUS-DOSSIERS, ET AUSSI les droits de TOUS les fichiers inclus dans 'mondossier'**
    ```bash
    $ chmod -R 777 mondossier  # modifier RÉCURSIVEMENT les droits de 'mondossier'
    $ ls -l mondossier      # vérifier que les droits du 'sousdossier'
                            # et 'monfichier' inclus dans 'mondossier'
                            # ONT BIEN été modifiés RÉCURSIVEMENT
    -rwxrwxrwx 1 eleve eleve    0 27 juin  03:52 monfichier
    drwxrwxrwx 2 eleve eleve 4096 27 juin  03:52 sousdossier
    ```

!!! ex
    Toutes les questions suivantes sont à faire **exclusivement** en ligne de commande (sauf le téléchargement d'images)

    1. Dans le dossier ```/home/eleve``` créer un dossier ```MesVacances```  
    2. Dans le dossier ```MesVacances```, créer un sous-dossier ```PhotosPerso```
    \+ Créer et/ou Télécharger deux Photos (libres de droits) et placez-les dans ```PhotosPerso```: image1.png et image2.png  
    3. Dans le dossier ```MesVacances```, créer un sous-dossier ```PhotosAmis```
    \+ Créer et/ou Télécharger deux Photos (libres de droits) et placez-les dans ```PhotosAmis```: image3.png et image4.png  
    4. Dans le dossier ```MesVacances```, créer un sous-dossier ```PhotosFamille```
    \+ Créer et/ou Télécharger deux Photos (libres de droits) et placez-les dans ```PhotosFamille```: image5.png et image6.png  
    5. Configurer les droits des fichiers et dossiers de la manière suivante:

    ```dot
    graph G {
    splines=polyline;
    "MesVacances" [shape=box label="MesVacances\ndroits : 777"];
    "ImagesPerso" [shape=box label="ImagesPerso\ndroits : 700"];
    "ImagesAmis" [shape=box label="ImagesAmis\ndroits : 755"];
    "ImagesFamille" [shape=box label="ImagesFamille\ndroits : 777"];
    "image1.png" [shape=none label="image1.png\ndroits : 700"];
    "image2.png" [shape=none label="image2.png\ndroits : 700"];
    "image3.png" [shape=none label="image3.png\ndroits : 755"];
    "image4.png" [shape=none label="image4.png\ndroits : 755"];
    "image5.png" [shape=none label="image5.png\ndroits : 777"];
    "image6.png" [shape=none label="image6.png\ndroits : 777"];
    "MesVacances" -- ImagesPerso;
    "MesVacances" -- ImagesAmis;
    "MesVacances" -- ImagesFamille;
    "ImagesPerso" -- "image1.png";
    "ImagesPerso" -- "image2.png";
    "ImagesAmis" -- "image3.png";
    "ImagesAmis" -- "image4.png";
    "ImagesFamille" -- "image5.png";
    "ImagesFamille" -- "image6.png";
    }
    ```

### chmod en Droits Symboliques `rwx`

Par rapport aux droits existants actuellement pour un fichier/dossier:

* $+$ : **ajouter** les droits suivants, **par rapport aux droits actuels**, ... au format `rwx`
* $-$ : **enlever** les droits suivants, **par rapport aux droits actuels**, ... au format `rwx`
* $=$ : **affecter** les droits égaux à... au format `rwx`

<env style="color:white;background-color: #e1344e;"><b>AVEC AU MOINS 1 flag `u` `g` `o` `a` $\Longrightarrow$ IL NE FAUT JAMAIS UTILISER `umask`</b></env>

Dit autrement, dans ce cas, le ***comportement est prévisible, sans surprises***.

<center>

| Notations | Signification | Exemples<br/> |
|:-:|:-:|:-:|
| `+` | **Ajoute** des droits<br/>**relativement aux droits courants** | `chmod u+x monFichier/monDossier` <br/>Ajoute les droits d'exécution `x` à l'utilisateur propriétaire `u`<br/>`chmod go+rx monFichier/monDossier` <br/>Ajoute les droits de lecture `r` et d'exécution `x`<br/> au groupe propriétaire `g` et aux autres `o` |
| `-` | **Enlève** des droits<br/>**relativement aux droits courants** | `chmod a-x monFichier/monDossier` <br/>Enlève les droits d'exécution `x` à tous `a` = `u`+`g`+`o`<br/>`chmod uo-rw monFichier/monDossier` <br/>Enlève les droits de lecture `r` et d'écriture `w`<br/> à l'utilisateur propriétaire `u` et aux autres `o` |
| `=` | **Affecter** les droits égaux à | `chmod u=x monFichier/monDossier` <br/>Affecte les droits d'exécution `--x` à l'utilisateur propriétaire `u`<br/>`chmod ug=rw monFichier/monDossier` <br/>Affecte les droits de lecture `r` et d'écriture `w`<br/> à l'utilisateur `u` et au groupe `g` propriétaires |

</center>

<env>**GÉNÉRALISATION**</env>
Notez qu'il est possible de formuler **en un seul coup, des demandes différentes pour plusieurs flags**, en séparant les demandes par des vigules `,` :
`chmod u-x,g+w,o-r monFichier/monDossier`

<env style="color:white;background-color: #e1344e;"><b>SANS AUCUN flag $\Longrightarrow$ IL FAUT TOUJOURS UTILISER `umask` $\,$ ( `0022` par défaut )</b></env> <env style="color:white;background-color: #f5a60a;"><b> ( CULTURE )</b></env>

Autrement dit, dans ce cas, l'application de `umask` provoque des **comportements "inattendus" mais en théorie souhaitables, auxquels il faut réfléchir pour bien les comprendre**.

<env>**Syntaxes Possibles et Mode Opératoire détaillé**</env>

* `chmod +(droits demandés)` ou 
* `chmod -(droits demandés)` ou 
* `chmod =(droits demandés)`

:one: Nous appellerons <rb>*droits demandés*</rb>, les droits, au format `rwx`, que l'on souhaite à priori **appliquer à CHAQUE flag** : à `u`, à `g` et à `o`  
:warning: Mais ce ne sont pas les *droits demandés* qui seront ultimement appliqués, et que nous appelerons les <rb>*droits retenus*</rb> :warning:, en effet on doit tenir compte de `umask` de la manière suivante :  
:two: D'abord : calculer <enc>**droits retenus = $($ droits demandés $-$ `umask` $)$**</enc>  
:three: Ensuite : ajouter `+`, ou soustraire `-`, ou affecter `=` **les droits retenus** (selon la commande demandée)

Rappelons que la valeur par défaut du **masque utilisateur** `umask = 0022 = - --- -w- -w-`, 
Appliquer `umask` revient donc à ce que les droits d'écriture `w` sur `g` et `o` soient tous deux ramenés à zéro : `-` (s'ils ne le sont pas déjà)

<center>

| Notations | Signification | Exemples |
|:-:|:-:|:-:|
| `+` | **Ajoute** à tous, des droits<br/>**relativement aux droits courants**<br/>mais en apppliquant d'abord `umask` | `chmod +x monFichier/monDossier` <br/>Droits demandés = Ajoute à TOUS ( `u`+`g`+`o` ) des droits d'exécution `x`<br/> Droits demandés = Ajoute `- --x --x --x` <br/>MAIS il faut commencer par leur enlever `umask` = `- --- -w- -w-` <br/> (ici, rien à faire car les `w` de `g` et `o` des droits demandés sont déjà à vide `-`) <br/> Droits retenus = Ajoute $($ droits demandés $-$ `umask` $)$ <br/> Droits retenus $=$ Ajoute `- --x --x --x` |
| ^ | ^ | `chmod +w monFichier/monDossier` <br/>Droits demandés = Ajoute à TOUS ( `u`+`g`+`o` ) des droits d'écriture `w`<br/> Droits demandés = Ajoute `- -w- -w- -w-` <br/>MAIS il faut commencer par leur enlever `umask` = `- --- -w- -w-` <br/> (les `w` de `g` et `o` des droits demandés doivent être ramenés à vide `-`) <br/> Droits retenus = Ajoute $($ droits demandés $-$ `umask` $)$ <br/> Droits retenus $=$ Ajoute `- -w- --- ---` |
| ^ | ^ | `chmod +rw monFichier/monDossier` <br/>Droits demandés = Ajoute à TOUS ( `u`+`g`+`o` ) des droits de lecture `r` et d'écriture `w` <br/> Droits demandés = Ajoute `- rw- rw- rw-` <br/>MAIS il faut commencer par leur enlever `umask` = `- --- -w- -w-` <br/> (les `w` de `g` et `o` des droits demandés doivent être ramenés à vide `-`) <br/> Droits retenus = Ajoute $($ droits demandés $-$ `umask` $)$ <br/> Droits retenus $=$ Ajout `- rw- r-- r--` |

</center>

<center>

| Notations | Signification | Exemples |
|:-:|:-:|:-:|
| `-` | **Enlève** à tous, des droits<br/>**relativement aux droits courants**<br/>mais en apppliquant d'abord `umask` | `chmod -x monFichier/monDossier` <br/>Droits demandés = Enlève des droits d'exécution `x` à tous `u`+`g`+`o`<br/> Droits demandés = Enlève `- --x --x --x` <br/>MAIS il faut commencer par leur enlever `umask` = `- --- -w- -w-` <br/> (ici, rien à faire car les `w` de `g` et `o` des droits demandés sont déjà à vide `-`) <br/> Droits retenus = Enlève de $($ droits demandés $-$ `umask` $)$ <br/> Droits retenus $=$ Enlève `- --x --x --x` |
| ^ | ^ | `chmod -w monFichier/monDossier` <br/>Droits demandés = Enlève des droits d'écriture `w` à tous `u`+`g`+`o`<br/> Droits demandés = Enlève `- -w- -w- -w-` <br/>MAIS il faut commencer par leur enlever `umask` = `- --- -w- -w-` <br/> (les `w` de `g` et `o` des droits demandés doivent être ramenés à vide `-`) <br/> Droits retenus = Enlève $($ droits demandés $-$ `umask` $)$ <br/> Droits retenus $=$ Enlève `- -w- --- ---` |
| ^ | ^ | `chmod -wx monFichier/monDossier` <br/>Droits demandés = Enlève à TOUS ( `u`+`g`+`o` ) des droits d'écriture `w` et d'exécution `x` <br/>Droits demandés = Enlève `- -wx -wx -wx` <br/>MAIS il faut commencer par leur enlever `umask` = `- --- -w- -w-` <br/> (les `w` de `g` et `o` des droits demandés doivent être ramenés à vide `-`) <br/> Droits retenus = Enlève $($ droits demandés $-$ `umask` $)$ <br/> Droits retenus $=$ Enlève `- -wx --x --x` |

</center>

<center>

| Notations | Signification | Exemples |
|:-:|:-:|:-:|
| `=` | **Affecter** à tous, des droits égaux à...<br/>mais en apppliquant d'abord `umask` | `chmod =r monFichier/monDossier` <br/>Droits demandés = Affecter des droits de lecture `r` à tous `u`+`g`+`o`<br/> Droits demandés = Affecter `- r-- r-- r--` <br/>MAIS il faut commencer par leur enlever `umask` = `- --- -w- -w-` <br/> (ici, rien à faire car les `w` de `g` et `o` des droits demandés sont déjà à vide `-`) <br/> Droits retenus = Affecter de $($ droits demandés $-$ `umask` $)$ <br/> Droits retenus $=$ Affecter `- r-- r-- r--` |
| ^ | ^ | `chmod =w monFichier/monDossier` <br/>Droits demandés = Affecter à TOUS ( `u`+`g`+`o` ) des droits d'écriture `w` <br/>Droits demandés = Affecter `- -w- -w- -w-` <br/>MAIS il faut commencer par leur enlever `umask` = `- --- -w- -w-` <br/> (les `w` de `g` et `o` des droits demandés doivent être ramenés à vide `-`) <br/> Droits retenus = Affecter $($ droits demandés $-$ `umask` $)$ <br/> Droits retenus $=$ Affecter `- -w- --- ---` |
| ^ | ^ | `chmod =rw monFichier/monDossier` <br/>Droits demandés = Affecter à TOUS ( `u`+`g`+`o` ) des droits de lecture `r` et d'écriture `w` <br/>Droits demandés = Affecter `- rw- rw- rw-` <br/>MAIS il faut commencer par leur enlever `umask` = `- --- -w- -w-` <br/> (les `w` de `g` et `o` des droits demandés doivent être ramenés à vide `-`) <br/> Droits retenus = Affecter $($ droits demandés $-$ `umask` $)$ <br/> Droits retenus $=$ Affecter les droits `- rw- r-- r--` |

</center>