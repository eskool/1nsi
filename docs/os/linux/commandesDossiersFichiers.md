# 1NSI : Commandes Linux sur les Dossiers et Fichiers

## Notations Utiles sur les Dossiers et Fichiers

!!! not "Dossier Personnel / Home `~`"
    On note **```~```** le <rb>Dossier Personnel</rb> ou <rb>Dossier Home</rb> de l'utilisateur connecté:
    
    * Pour tous les utilisateurs (sauf ```root```) il se trouve toujours dans **```/home/nomUtilisateur```**
    * Pour l'utilisateur ```root``` il se trouve dans ```/root```
    C'est le dossier par défaut lorsque l'on lance un Terminal

!!! exp "d'utilisation de `~`"
    * Si *emma* est connectée, alors (pour elle) ```~``` désigne le dossier ```/home/emma``` :
    * Pour elle, ```~/Images``` désigne donc ```/home/emma/Images```
    * Si *eleve* est connecté, alors (pour elle/lui) ```~``` désigne le dossier ```/home/eleve``` :
    * Pour lui, ```~/Images``` désigne donc ```/home/eleve/Images```

!!! not "Dossier Courant `.` & Dossier Parent `..`"
    * Notation <rb>Dossier Courant</rb> : On note **```.```** le **dossier courant** (d'un dossier, ou bien d'un fichier)
    * Notation <rb>Dossier Parent</rb> : On note **```..```** le **dossier parent** (d'un dossier, ou bien d'un fichier)

!!! not "Caractères Spéciaux `?` et `*`"
    Certains caractères ont une signification particulière dans les commandes entrées dans le Terminal :

    * `?` désigne n'importe quel **(unique) caractère**
    * `*` désigne n'importe quel **chaîne de caractères**

## Quelques Commandes Utiles pour les Dossiers et Fichiers

Supposons que l'utilisateur courant est **```eleve```**. Lancer un Terminal (combinaison des touches ```Ctrl+Alt+T``` sur Manjaro)

* **pwd** = <b>P</b>rint <b>W</b>orking <b>D</b>irectory = Affiche le répertoire courant

    !!! exp
        ```bash
        $ pwd         # (Print Working Directory) affiche le dossier courant
        /home/eleve   # nous supposons que l'utilisateur courant est 'eleve'
        ```

* **ls** = <b>L</b>i<b>St</b> / affiche les fichiers et dossiers du répertoire courant

    !!! exp
        ```bash
        $ ls               # Liste/Affiche les fichiers et dossiers du dossier courant
        $ ls monDossier/   # Liste/Affiche le contenu du dossier 'monDossier"
        $ ls monDossier    # Idem: les "/" finaux sont facultatifs pour les dossiers
        $ ls -l            # Liste les fichiers et dossiers du dossier courant, VERSION LONGUE,
                           # SANS les fichiers/dossier cachés
        $ ls -Al           # Liste les fichiers et dossiers du dossier courant, VERSION LONGUE,
                           # AVEC aussi LES FICHIERS/DOSSIERS CACHÉS commençant par un point "."
        ```

* **cd** = <b>C</b>hange <b>D</b>irectory = <b>C</b>hange de <b>D</b>ossier/Répertoire

    Par défaut, lorsqu'un Terminal est lancé (Ctrl+Alt+T sur Manjaro) le prompt se trouve dans le dossier ```/home``` de l'utilisateur courant.

    !!! exp
        ```bash
        $ cd ..     # remonte d'un cran dans l'arborescence Linux
                    # donc se déplace dans le dossier parent
        $ cd /      # se déplace à la racine
        $ cd /usr   # se déplace dans le répertoire /usr
        $ cd /usr/share # se déplace dans le répertoire /usr/share
        $ cd ~      # se déplace/revient dans le répertoire /home 
                    # de l'utilisateur courant. Equivalent à juste 'cd'
        ```

* **mkdir** = <b>M</b>a<b>K</b>e<b>DIR</b>ectory = Crée un répertoire

    !!! exp
        ```bash
        $ mkdir monDossier # remonte d'un cran dans l'arborescence Linux
        $ ls -l     # Vérifier que le dossier a bien été créé
                    # Affiche tous les Dossiers (dont le nouveau 'monDossier')
        [...]
        drwxr-xr-x  2 eleve eleve  4096 24 juin  17:15 monDossier
        [...]
        $ ls -l monDossier # Affiche SEULEMENT les détails pour 'monDossier'
        ```

* **touch** = Créer un fichier texte qui soit vide

    !!! exp
        ```bash
        $ cd                  # revenir (si besoin) dans son Dossier Personnel inclus dans /home
        $ touch monfichier    # crée le fichier monfichier dans /home/eleve
        $ ls -l monfichier    # vérifie la bonne création + affiche les détails de 'monfichier'
        -rw-r--r--  1 eleve eleve     0 24 juin  19:18 monfichier
        ```
  
* **rm** = <b>R</b>e<b>M</b>ove Directory = Supprimer RÉCURSIVEMENT ```-R``` un répertoire  
    :warning: ATTENTION DANGER : À UTILISER AVEC PRÉCAUTION!!!! :warning:

    !!! exp
        ```bash
        $ rm  monfichier   # supprime le fichier 'monfichier'
        $ rm -R monDossier # supprime RÉCURSIVEMENT le Dossier 'monDossier' ET TOUT SON CONTENU !!!
        ```

    :brain: L'oubli de l'Option récursive ```-R``` provoque l'erreur ```rm: impossible de supprimer 'monDossier/': est un dossier```, même lorsque le dossier est vide...

* **cat** = Affiche le contenu d'un fichier dans le Terminal

    !!! exp
        ```bash
            $ cat /etc/hosts     # Affiche le Fichier 'hosts' contenu dans le dossier /etc
        ```
  
* **nano** = Editeur de texte en ligne de commande (très simple), dans un Terminal
  
    !!! exp
        ```bash
            $ nano monfichier   # ouvre 'monfichier' en Ecriture dans le Terminal
                                # ...puis Taper quelquechose dans le fichier...
                                # Interactivité:
                                # Ctrl+O sauvegarde vos modifications dans le fichier. 
                                # (extension de fichier non obligatoire)
                                # Ctrl+X sort du fichier
            $ cat monfichier    # affiche 'monfichier' dans le Terminal, avec les nouvelles modifs
        ```

    <env>Remarques</env>

    * Quelques Raccourcis Clavier Utiles dans un Terminal:
        * ++ctrl+shift+c++ pour **Copier** dans un Terminal
        * ++ctrl+shift+v++ pour **Coller** dans un Terminal
        * ++ctrl+shift+up++ pour **faire défiler vers le Haut** dans un Terminal
        * ++ctrl+shift+down++ pour **faire défiler vers le Bas** dans un Terminal
    * il existe d'**autres  Editeurs de Texte** en ligne de commande, dans un Terminal, notamment:  
        * **vi** ou **vim**: Puissant éditeur de texte, sorte d'IDE en ligne de commande : coloration syntaxique de 200 langages, complétion automatique, comparaison de fichiers, recherche évoluée. Plus complexe d'utilisation néanmoins, déconseillé aux débutants.  
        * **(GNU) emacs**: Puissant éditeur, extensible et personnalisable. Sorte d'IDE également pour de nombreux langages, mais pas uniquement: c'est aussi un navigateur internet, un client de messagerie, un client irc, etc... 

* **cp** = <b>C</b>o<b>P</b>y = Créer une copie d'un fichier et/ou dossier

    !!! exp "Copier un Fichier"
        ```bash
        $ cp monfichier_source monfichier_destination
        ```

    !!! exp "Copier RÉCURSIVEMENT un Dossier"
        ```bash
        $ cp -r mondossier_source mondossier_destination
        ```
        L'oubli de l'option récursive **```-r```** provoque une erreur:
        ```cp: -r non spécifié ; omission du répertoire 'mondossier_source'```

* **mv** = <b>M</b>o<b>V</b>e = Déplacer ou Renommer un fichier et/ou dossier

    !!! exp "Déplacer/Renommer un Fichier"
        ```bash
        $ mv monfichier_source /nouveau/chemin/monfichier_nouveauNomOuPas     # Déplace le fichier
        $ mv monfichier_ancienNom monfichier_nouveauNom                       # Renomme le fichier
        ```

    !!! exp "Déplacer/Renommer un Dossier"
        ```bash
        $ mv mondossier_ancienNom/ /nouveau/chemin/mondossier_nouveauNomOuPas/  # Déplace un dossier
        $ mv mondossier_ancienNom mondossier_nouveauNom                   # Renomme le dossier
        $ mv mondossier_ancienNom/ mondossier_nouveauNom/                 # Renommer le dossier, les '/' finaux sont facultatifs
        ```
        Remarque : ici, pas besoin de l'Option récursive **```-r```**

## le masque utilisateur `umask`

Vous avez peut-être remarqué que la création d'un fichier ou d'un dossier donne automatiquement des droits par défaut à votre fichier/Dossier (pas les mêmes d'ailleurs..) :

* pour un `fichier` les droits par défaut sont `-rw-r--r--`
* pour un `dossier` les droits par défaut sont `drwxr-xr-x`

Il faut voir ceci comme **des droits par défaut à enlever** (sous-entendu, par rapport à la totalité des droits), c'est ce que l'on appelle un <rb>masque utilisateur</rb> :fr: ou <rb>user mask</rb> :gb: ou `umask`, lors de la création d'un nouveau fichier/dossier : 

:one: En général, **Par défaut** <enc>`umask = 0022 = -----w--w-`</enc> qui sont les **droits à enlever automatiquement** à toute création quelle qu'elle soit (fichier et/ou dossier)  
:two: si c'est **un dossier** qui est créé : `umask = 0022`  
`Droits par défaut Dossier = 777 - umask = (drwxrwxrwx) - (-----w--w-) = drwxr-xr-x`  
:three: Si c'est **un fichier** qui est créé : `umask = 0133`  
:warning: Le `umask` des fichiers est plus contraignant : il doit aussi enlever les droits d'exécution pour tous (`u`+`g`+`o`) : AUCUN FICHIER N'ADMET DE DROITS D'EXÉCUTION PAR DÉFAUT, POUR PERSONNE, ni pour `u` ni pour `g`, ni pour `o`, autrement dit :  
`Droits par défaut Fichier = 777 - umask = (-rwxrwxrwx) - (---x-wx-wx) = -rw-r--r--`

!!! Remarque
    La valeur par défaut de `umask` peut varier selon les distributions Linux.  
    En général : `umask = 0022`