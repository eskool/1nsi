# 1NSI : Système de Fichiers - Standard FHS

## le Standard FHS

Le *Système de Fichiers* des systèmes linux suit le *standard* **[FHS (Filesystem Hierarchy standard)](https://www.pathname.com/fhs/)** qui fixe/décrit l'organisation *hiérarchique* des différents répertoires de Linux ainsi que leur fonction. Sachez néanmoins qu'il existe de petites différences entre les distributions, pour certains répertoires voire pour certains fichiers.

## Schéma Simplifié d'une arborescence Linux (Standard FHS)

On remarquera notamment ci-dessous, les répertoires *utilisateur* dans ```/home```, qui contiennent tous les dossiers et fichiers personnels de tous les utilisateurs du système: Images, Audios, Vidéos, Documents de Travail, etc... (sauf l'utilisateur ```root```, et sauf si *root* a décidé qu'un utilisateur ne devait pas disposer d'un dossier personnel dans ```/home``` lors de la création de son compte).

!!! def
    On dit que ```/home/eleve``` est LE répertoire /home de l'utilisateur ```eleve```  
    On dit que ```/home/emma``` est LE répertoire /home de l'utilisateur ```emma```

```dot
graph G {
  size="7.5"
  splines=polyline;
  node [fontsize=12 margin=0.05 width=0.2 shape=box];
  "/" [width=0.4];
  "bin";
    "/bin/ls" [label="ls" shape=none];
    "/bin/chmod" [label="chmod" shape=none];
    "/bin/chown" [label="chown" shape=none];
  "boot";
  "dev";
  "etc";
    "/etc/hosts" [label="hosts" shape=none];
  "home";
    "/home/eleve" [label="eleve"];
      "/home/eleve/Documents" [label="Documents"];
      "/home/eleve/Images" [label="Images"];
      "/home/eleve/Musique" [label="Musique"];
    "/home/emma" [label="emma"];
      "/home/emma/Documents" [label="Documents"];
      "/home/emma/Images" [label="Images"];
      "/home/emma/Musique" [label="Musique"];
  "lib";
  "mnt";
  "opt";
  "proc";
  "root";
  "run";
  "sbin";
  "srv";
  "srv";
    "/srv/ftp" [label="ftp"];
    "/srv/http" [label="http"];
  "sys";
  "tmp";
  "usr";
    "/usr/bin" [label="bin"];
    "/usr/bin/grep" [label="grep" shape=none];
    "/usr/sbin" [label="sbin"];
    "/usr/share" [label="share"];
      "/usr/share/fonts" [label="fonts"];
  "var";
    
  "/" -- {mnt;bin;boot;dev;etc;home;lib;opt;proc;root;run;sbin;srv;sys;tmp;usr;var};
  "bin" -- "/bin/ls";
  "bin" -- "/bin/chmod";
  "bin" -- "/bin/chown";
  "etc" -- "/etc/hosts";
  "home" -- "/home/eleve";
    "/home/eleve" -- "/home/eleve/Documents";
    "/home/eleve" -- "/home/eleve/Images";
    "/home/eleve" -- "/home/eleve/Musique";
  "home" -- "/home/emma";
    "/home/emma" -- "/home/emma/Documents";
    "/home/emma" -- "/home/emma/Images";
    "/home/emma" -- "/home/emma/Musique";
  "usr" -- "/usr/bin";
  "/usr/bin" -- "/usr/bin/grep";
  "usr" -- "/usr/sbin";
  "usr" -- "/usr/share";
  "/usr/share" -- "/usr/share/fonts";
  "srv" -- "/srv/ftp";
  "srv" -- "/srv/http";
}
```

## Fonction de chaque répertoire Linux (Standard FHS)

<center>

| Répertoire | Utilisation Classique |
|:-:|:-:|
| ```/``` | LA Racine, la mère de tous les dossiers : elle contient tous les répertoires |
| ```/bin``` | **Binaires**/Programmes Exécutables essentiels au système, utilisables par tous les utilisateurs.<br/>On y trouve de nombreuses **commandes**. Dans certaines distributions: c'est un lien symbolique vers **/usr/bin** |
| ```/boot``` | Fichiers permettant à Linux de démarrer |
| ```/dev``` | Point d'entrée de tous les périphériques (CD, disque dur, écran, partitions <br/>/sdXN ex: /sda1 ou /nvmeN ex : /nvme1 pour les disques durs Non Volatiles NVM Express, consoles TTY), <br/>ainsi que les périphériques virtuels (/dev/null, /dev/random) |
| ```/etc``` | Fichiers de configuration système 'XXX.conf' pour certaines programmes, passwd, inittab, runlevels |
| ```/home``` | Répertoires personnels des utilisateurs (sauf root) |
| ```/lib``` | contient les bibliothèques partagées essentielles au système lors du démarrage. <br/>Dans certaines distributions: c'est un lien symbolique vers **/usr/lib** |
| ```/lib64``` | comme /lib mais pour les 64bits (parfois, on trouvera lib et lib32. Dans ce cas, lib = 64bits et lib32 = 32bits)
**```/mnt```** ou **```/media```** | contient les points de montage des partitions temporaires (clés USB, partitions de données (ex: veracrypt)) |
| ```/opt``` | Répertoire où sont usuellement installés les logiciels commerciaux (ou non libres), <br/>n'existant usuellement pas dans les dépôts officiels de votre distribution. <br/>Plus généralement, Répertoire générique pour l'installation de programmes <br/>compilés manuellement par l'administrateur |
| ```/proc``` | communique directement avec des variables et constantes du noyau. |
| ```/root``` | Répertoire personnel de **root** (l'administrateur). <br/>Le répertoire de root n'est pas dans /home, car quelquefois le /home est sur une partition à part. <br/>En cas d'échec de montage de /home, root doit quand même pouvoir accéder à son répertoire personnel. |
| ```/run``` | non présent dans toutes les distributions (non cité dans le FHS). <br/>Conçu pour réduire la charge imposée à la machine par les fichiers temporaires |
| ```/sbin``` | Binaires/programmes exécutables du système réservés à root/l'admin système. <br/>Dans certaines distributions: c'est un lien symbolique vers **/usr/sbin** |
| ```/srv``` | N'est pas présent dans toutes les distributions. <br/>C'est un répertoire de données pour divers services de Serveur :<br/> stockage des documents de comptes FTP (/srv/ftp), ou pages de sites web (/srv/http) |
| ```/sys``` |  similaire à **/proc**, plus récent et destiné à le remplacer |
| ```/tmp``` | Répertoire de fichiers temporaires |
| ```/usr``` | Contient des fichiers et programmes partageables entre les utilisateurs (/usr/share) et non modifiables: <br/>Des programmes installés (/usr/bin) avec leurs librairies (/usr/lib ou /usr/lib64) <br/>tels que firefox, chromium, libreoffice, ..., <br/>quelques programmes réservés à root/l'admin système (/usr/sbin) et les fichiers de code source (/usr/src) |
| ```/var``` | contient des données fréquemment réécrites - variables : <br/>fichiers de logs (/var/log), parfois les bases de données (/var/lib/mysql), <br/>pour certaines distributions des pages de site web (/var/www/html), <br/>des fichiers contenant tout ce qui est de passage en attendant d'être utilisé par un logiciel (/var/spool) <br/>(ex: files d'attente imprimantes) |

</center>