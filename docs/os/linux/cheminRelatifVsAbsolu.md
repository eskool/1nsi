# 1NSI : Linux - Chemin Relatif vs Chemin Absolu

Quand on tente d'accéder à un fichier/dossier, que l'on appellera <rb>*final*</rb>, en parcourant l'arborescence de votre système, on peut envisager deux scénarios possibles:

* ou bien accéder à ce fichier/dossier <irb>*final*</irb>, en partant <rb>depuis un fichier / dossier *initial*</rb> dans l'arborescence : on parle dans ce cas d'un <rb>chemin relatif</rb> vers le fichier/dossier final depuis un fichier/dossier initial. Partir d'un **fichier initial** revient à partir du **dossier courant (auquel il appartient)**. Intuitivement, Le chemin relatif pouvant être vu comme des explications quant au chemin à suivre le long de l'arborescence, en partant du fichier/dossier initial, pour se rendre au fichier/dossier final.
* ou bien accéder à ce fichier/dossier <irb>*final*</irb> en partant toujours <rb>depuis la racine ```/```</rb> : on parle dans ce cas d'un <rb>chemin absolu</rb> vers le fichier final. Intuitivement, Le chemin absolu pouvant être vu, comme des explications quant au chemin à suivre le long de l'arborescence, en partant toujours de la racine, pour se rendre au fichier final.

## Chemin Relatif

Disons que l'on dispose de l'arborescence Linux Simplifiée suivante:

```dot
graph G {
  size="7.5"
  splines=polyline;
  "/" [shape=box];
  "bin" [shape=box];
  "boot" [shape=box];
  "dev" [shape=box];
  "etc" [shape=box];
  "home" [shape=box];
  "lib" [shape=box];
  "mnt" [shape=box];
  "srv" [shape=box];
  "ls" [shape=none];
  "chmod" [shape=none];
  "home" [shape=box];
  "emma" [shape=box] [color=red style=bold];
  "DocumentsEmma" [label="Documents" shape=box color=red style=bold];
  "luc" [shape=box];
  "DocumentsLuc" [label="Documents" shape=box];
  "Travail" [shape=box];
  "Salaires.ods" [shape=none];
  "Images" [shape=box color=red style=bold];
  "srv" [shape=box];
  "http" [shape=box];
  "ftp" [shape=box];
  "monsite" [shape=box];
  "index.html" [shape=none];
  "img" [shape=box];
  "css" [shape=box];
  "js" [shape=box];
  "monfichier.odt" [shape=none];
  "image1.png" [shape=none];
  "image2.jpg" [shape=none];
  "bateau.png" [shape=none];
  "lac.png" [shape=none];
  "style.css" [shape=none];
  "script.js" [shape=none];
  "/" -- mnt;
  "/" -- bin;
  "/" -- boot;
  "/" -- dev;
  "/" -- etc;
  "/" -- home;
  "/" -- srv;
  "/" -- lib;
  "bin" -- ls;
  "bin" -- chmod;
  "home" -- luc;
  "luc" -- DocumentsLuc;
  "home" -- emma;
  "emma" -- DocumentsEmma [color=red style=bold];
  "monfichier.odt" [fontcolor=red]
  "image1.png" [fontcolor=red]
  "DocumentsEmma" -- "monfichier.odt" [color=red style=bold];
  "DocumentsEmma" -- "Travail";
  "Travail" -- "Salaires.ods";
  "emma" -- Images [color=red style=bold];
  "Images" -- "image1.png" [color=red style=bold];
  "Images" -- "image2.jpg";
  "srv" -- ftp;
  "srv" -- http;
  "http" -- monsite;
  "monsite" -- "index.html";
  "monsite" -- img;
  "monsite" -- css;
  "monsite" -- js;
  "img" -- "bateau.png";
  "img" -- "lac.png";
  "css" -- "style.css";
  "js" -- "script.js";
}
```

!!! exp
    Disons qu'**emma** souhaite insérer l'image **image1.png** (fichier final) dans son document **monfichier.odt** (fichier initial) situé dans son répertoire **Documents**. Le chemin à parcourir donc pourrait se décrire par les opérations:

    * remonter d'un niveau/cran l'arborescence pour remonter au répertoire **emma**.
    * redescendre d'un niveau/cran l'arborescence pour redescendre dans le répertoire **Images**.
    * puis, dans ce répertoire *Images*, on trouve bien le fichier *image1.png*

    Avec la notation ```..``` pour le dossier parent (qui représente ici le dossier `emma`, et non pas `Documents` ATTENTION), on peut dire que :

!!! def "Chemin Relatif"
    * On dit que <rb>le chemin relatif</rb> du (vers le) fichier *image1.png* **depuis** le fichier **monfichier.odt** est **```../Images/image1.png```**
    * On dit que <rb>le chemin relatif</rb> du (vers le) fichier *image1.png* **depuis** le répertoire **Documents** est **```../Images/image1.png```**

!!! ex
    Déterminer les **chemins relatifs** permettant d'accéder:

    * au fichier **style.css** depuis le fichier **index.html**
    * au fichier **script.js** depuis le fichier **index.html**
    * au fichier **style.css** depuis le fichier **script.js**
    * au fichier **bateau.png** depuis le fichier **index.html**
    * au fichier **Salaires.ods** depuis le fichier **monfichier.odt**
    * au fichier **monfichier.odt** depuis le fichier **Salaires.ods**
    * au fichier **index.html** depuis le répertoire **ftp** (On pourra supposer dans cette question, que les droits Linux sont bien configurés...)
    * au fichier **monfichier.odt** depuis le dossier **monsite**. (On pourra supposer dans cette question, que les droits Linux sont bien configurés...)

## Chemin Absolu

```dot
graph G {
  size="7.5"
  splines=polyline;
  "/" [shape=box color=red style=bold];
  "bin" [shape=box];
  "boot" [shape=box];
  "dev" [shape=box];
  "etc" [shape=box];
  "home" [shape=box color=red style=bold];
  "lib" [shape=box];
  "mnt" [shape=box];
  "srv" [shape=box];
  "ls" [shape=none];
  "chmod" [shape=none];
  "home" [shape=box];
  "emma" [shape=box color=red style=bold];
  "DocumentsEmma" [label="Documents" shape=box];
  "luc" [shape=box];
  "DocumentsLuc" [label="Documents" shape=box];
  "Travail" [shape=box];
  "Salaires.ods" [shape=none];
  "Images" [shape=box color=red style=bold];
  "srv" [shape=box];
  "http" [shape=box];
  "ftp" [shape=box];
  "monsite" [shape=box];
  "index.html" [shape=none];
  "img" [shape=box];
  "css" [shape=box];
  "js" [shape=box];
  "monfichier.odt" [shape=none];
  "image1.png" [shape=none fontcolor=red];
  "image2.jpg" [shape=none];
  "bateau.png" [shape=none];
  "lac.png" [shape=none];
  "style.css" [shape=none];
  "script.js" [shape=none];
  "/" -- mnt;
  "/" -- bin;
  "/" -- boot;
  "/" -- dev;
  "/" -- etc;
  "/" -- home [color=red style=bold];
  "/" -- srv ;
  "/" -- lib;
  "bin" -- ls;
  "bin" -- chmod;
  "home" -- luc;
  "luc" -- DocumentsLuc;
  "home" -- emma [color=red style=bold];
  "emma" -- DocumentsEmma;
  "monfichier.odt"
  "image1.png"
  "DocumentsEmma" -- "monfichier.odt";
  "DocumentsEmma" -- "Travail";
  "Travail" -- "Salaires.ods";
  "emma" -- Images [color=red style=bold];
  "Images" -- "image1.png" [color=red style=bold];
  "Images" -- "image2.jpg";
  "srv" -- ftp;
  "srv" -- http;
  "http" -- monsite;
  "monsite" -- "index.html";
  "monsite" -- img;
  "monsite" -- css;
  "monsite" -- js;
  "img" -- "bateau.png";
  "img" -- "lac.png";
  "css" -- "style.css";
  "js" -- "script.js";
}
```

Disons que l'on souhaite maintenant connaître le **chemin absolu** vers le fichier **image1.png**, donc le chemin vers `image1.png` depuis la racine `/`

Le chemin à parcourir pourrait se décrire par les opérations:

* partir de la racine `/`
* entrer dans le répertoire **home**
* entrer dans le répertoire **emma**
* entrer dans le répertoire **Images**
* puis, dans ce répertoire *Images*, on trouve bien le fichier **image1.png**

!!! def "Chemin Absolu"
    On dit que le <rb>chemin absolu</rb> du fichier *image1.png* est **```/home/emma/Image/image1.png```** (sous-entendu, en partant de la racine)

!!! exp
    Le **chemin absolu** vers le fichier `lac.png` est `/srv/http/monsite/../img/lac.png`

!!! ex
    Déterminer les **chemins absolus** permettant d'accéder:

    * au fichier **index.html**
    * au fichier **style.css**
    * au fichier **script.js**
    * au fichier **bateau.png**
    * au fichier **Salaires.ods**
    * au fichier **monfichier.odt**
    * au fichier **chmod**
