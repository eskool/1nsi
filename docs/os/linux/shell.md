# 1NSI : le Shell : Communication avec le noyau

## Terminal, Prompt, Shell

!!! def
    * le <rb>Terminal</rb> est une **interface en ligne de commande**, ou **Command Line Interface** (souvent notée **CLI**). 
    Lorsque le Terminal est prêt à recevoir une commande il l'indique par une *invite de commande*.
    * Un <rb>Prompt</rb> ou **invite de commande** est la succession d'un ou de plusieurs caractères, placés usuellement en début de la ligne de commande, servant à signaler que le Terminal est prêt à recevoir une saisie au clavier. Il est souvent fini par un caractère spécial bien connu : ```]```, ```#```, ```$```,ou ```>```

Les **commandes** Linux doivent être tapées dans un **Terminal** 
$\Longrightarrow$ Lancez un Terminal (combinaison de touches **```Ctrl+Alt+T```** sur Manjaro):

```bash
[eleve@poste1 ~]$   # (commentaire) le symbole $ signifie que 
                    # c'est l'utilisateur courant qui 'a la main' dans le Terminal
```

On reconnaît la partie ```[eleve@poste1 ~]$``` qui est le **prompt** du Terminal.  
Il se décompose de la manière suivante:

* ```eleve``` entre les [ ] est le nom de l'utilisateur courant
* ```poste1``` entre les [ ] est le nom de l'hôte courant (=nom de l'ordi)
  donc ```eleve@poste1``` signifie que l'utilisateur courant est ```eleve``` et qu'il travaille sur le ```poste1``` (`@` = at)
* le **```~```** signifie que le dossier courant est ```/home/eleve``` (=EMPLACEMENT PAR DÉFAUT LORSQU'ON LANCE LE TERMINAL)

!!! not "du Prompt `$` pour l'utilisateur courant, ou `#` pour root"
    Dans ce cours, le **prompt** sera noté plus simplement :
    
    * `$` pour l'utilisateur courant
    * `#` pour l'utilisateur `root` (le superadministrateur)

!!! def "d'un SHELL"
    Lorsque vous avez le prompt dans un Terminal, vous avez donc la main pour lancer des **commandes** qui permettent de lancer, et quelquefois d'interagir avec, des logiciels, au moyen d'un <rb>interpréteur de commandes</rb>, encore appelé un  <rb>Shell</rb> sous Linux. Le <rb>Shell</rb> est donc un programme qui sert d'interface entre l'utilisateur et le noyau Linux (càd avec les fonctionnalités internes de l'OS). 
    
!!! pte "Il existe plusieurs Shells"
    Il existe en fait plusieurs interpréteurs de commandes (/shell) sous Linux, notamment:

    * [**<rb>B</rb>ourne <rb>Sh</rb>ell (bsh** ou **sh)**](https://fr.wikipedia.org/wiki/Bourne_shell) (1975): Shell développé par Steve Bourne (UK) en 1975 (Laboratoires Bell).
    * [<rb>B</rb>ourne <rb>A</rb>gain <rb>SH</rb>ell (<rb>BASH</rb>)](https://fr.wikipedia.org/wiki/Bourne-Again_shell) (1989): **Un des plus puissants et surtout le plus fréquent** sur les machines Unix/Linux. C'est l'**interpréteur par défaut** de nombreux systèmes Unix/GNU-Linux. C'est CE Shell (le deuxième shell de Steve Bourne) que nous utiliserons dans ce cours. Basé sur sh, il apporte de nombreuses améliorations, provenant du Korn Shell (ksh) et du C Shell (csh). Il a été le Shell par défaut sur les Systèmes BSD (jusqu'à MAC OS X v10.14), ensuite remplacé par zsh (mais bash toujours présent).
    * [**<rb>C Sh</rb>ell (csh)**](https://fr.wikipedia.org/wiki/C_shell) (1978) : C shell. Evolution du Shell sh, utilisant une syntaxe plus proche du langage C. Un des avantages est la possibilité de réutilisation de l'Historique des commandes.
    * [**<rb>K</rb>orn <rb>Sh</rb>ell (ksh)**](https://fr.wikipedia.org/wiki/Korn_shell) (1983) : Shell développé par David Korn en 1983. Compatible avec le Bourne Shell (sh), et avec quelques fonctionnalités du C Shell (csh). Quelques améliorations par rapport au Bourne Shell (sh) : il supporte la rééxécution possible avec subsitution des commandes de l'historique, et l'usage des coprocessus. Depuis ksh93, les tableaux associatifs, l'arithmétique à virgule flottante. il supporte la programmation orientée objet.
    * [**<rb>T</rb>enex <rb>C Sh</rb>ell (tcsh)**](https://fr.wikipedia.org/wiki/Tcsh) (1981): version moderne du csh, et compatible avec celui-ci, il lui ajoute quelques améliorations : complétion des noms de fichiers et de commandes, ainsi que l'édition en ligne de commandes. Il a été le Shell par défaut sur les Systèmes BSD (jusqu'à MAC OS X v10.2), ensuite remplacé par bash (mais tcsh toujours présent), puis par zsh (depuis la version MAC OS X v 10.15)
    * [**<rb>Z Sh</rb>ell (<rb>ZSH</rb>)**](https://fr.wikipedia.org/wiki/Z_Shell) (1990) : puissant interpréteur : sorte de Bourne Shell étendu, reprenant les fonctions les plus pratiques de bash, ksh et tcsh. L'**interpréteur par défaut sur MAC OS X** (depuis la version 10.15) : voir [cette page d'assistance sur les Shells Apple](https://support.apple.com/fr-fr/HT208050)

## Syntaxe Générale d'une Commande Linux

!!! note "Syntaxe Générale d'une Commande"
    macommande [-Options Facultatives\] paramètres (Facultatifs, Ou Pas)

<env>**Options : Syntaxe Longue vs Syntaxe Courte**</env>

* une **Syntaxe Longue** avec `--` suivi d'**un (ou plusieurs) mots en toutes lettres** : `macommande --help` montre l'aide pour `macommande`
* une **Syntaxe Courte** avec `-` suivi d'**une seule lettre** : `macommande -h` montre l'aide pour `macommande`

<env>**Obtenir de l'Aide sur une Commande**</env>

* Obtenir de l'aide avec l'Option **```--help```** pour une commande : Que fait cette commande? Quelles Options existent? Quelle syntaxe?

    !!! exp
        ```bash
        $ macommande --help  # affiche TOUTES les Options disponibles pour CETTE commande `ls` et sa Syntaxe, dans le Terminal
        # et/ou:
        $ macommande -h      # affiche TOUTES les Options disponibles pour CETTE commande et sa Syntaxe, dans le Terminal
        ```

* la commande **```man```** : affiche le **man**uel Linux pour une commande (encore plus complet que ```macommande --help```) 

    !!! exp
        ```bash
        $ man ls     # affiche le manuel pour la commande 'ls' dans le Terminal
        # Interactivité:
        # touche 'h' (help) affiche de l'aide sur TOUTES les (autres) touches et leur signification dans l'environnement 'man'
        # flèches 'Up' et 'Down' (resp. 'PageUp' et 'PageDown') pour faire défiler la page verticalement (resp. les pages)
        # '/' + taper un mot pour le rechercher dans le 'man' de la commande + 'n' (next) pour l'occurence suivante
        # touche 'q' pour quitter l'environnement 'man', et récupérer la main dans le Terminal
        ```