# Histoire d'Unix (CULTURE GEEK :stuck_out_tongue_winking_eye:)

## Unix : Qu'est-ce que c'est ?

<clear></clear>

<div style="float:right;width:40%;">

<center>

<figure>
<img src="../img/ken_thompson_and_dennis_ritchie.jpg" class="box">
<figcaption>Ken Thompson (gauche) et Dennis Ritchie (droite), 1973</figcaption>
</figure>

</center>

</div>

**[Unix](https://fr.wikipedia.org/wiki/Unix)**, créé en $1969$ par **[Ken Thompson](https://fr.wikipedia.org/wiki/Ken_Thompson)** et **[Dennis Ritchie](https://fr.wikipedia.org/wiki/Dennis_Ritchie)**, est un OS (depuis $1970$) **multitâches** et **multi-utilisateurs**. Il a été développé dans les **Laboratoires Bell**, dits les **Bell Labs :us: (de maison mère AT&T)** durant les $1970$'s. Initialement écrit en assembleur, il est difficilement maintenable sous cette forme, Ken Thompson et Dennis Ritchie tentent donc plusieurs réécritures d'Unix, avec plusieurs langages de programmation (TMG, Fortran, BCPL, le langage B est inventé pour l'occasion en $1969$-$1970$ par Ken Thompson et Dennis Ritchie), mais c'est finalement Dennis Ritchie qui réécrit Unix en $1973$, en inventant au passage le **[langage C](https://fr.wikipedia.org/wiki/C_(langage))** (!) dès $1971$.  

Particulièrement répandu dans les milieux universitaires au début des années $1980$, il a été utilisé par beaucoup de start-ups fondées par des jeunes entrepreneurs à cette époque et a donné naissance à une famille de systèmes, dont les plus populaires à ce jour sont les variantes de **BSD** (notamment **FreeBSD**, **NetBSD** et **OpenBSD**), **GNU/Linux**, **iOS** et **macOS**. La quasi-totalité des systèmes d'exploitations PC ou mobile (à l'exception des Windows NT) est basée (du moins fonctionnellement) sur le noyau de Unix.

Unix repose sur un **interpréteur** ou **superviseur** (le <bred>shell</bred>) et de nombreux petits utilitaires, accomplissant chacun une action spécifique, commutables entre eux (mécanisme de "*redirection*") et appelés depuis la **ligne de commande**.


## (Petite) Histoire d'Unix..

Pour la petite histoire, **Unix est né... de la volonté de Ken Thompson de jouer à un jeu vidéo...**  
**Le Space Travel** [^1]  
Dès $1964$, Ken Thompson travaille sur un projet de Système d'Exploitation nommé **[Multics](https://en.wikipedia.org/wiki/Multics)**, multitâches et multi-utilisateur, développé sur un ordinateur **GE-635** (General Electric), conjointement par les *Laboratoires Bell*, le *MIT* - Massachusets Institute of Technology- et *General Electric*. 

<center>

<figure>
<img src="../img/ge635.webp" class="box">
<figcaption>un GE-635 (General Electric)</figcaption>
</figure>

</center>

Durant ce travail, Ken Thompson développe le **jeu vidéo [Space Travel](https://en.wikipedia.org/wiki/Space_Travel_(video_game))** sur Multics et l'ordinateur GE-635 :

<figure>
<img src="../img/space_travel.png" class="box">
<figcaption>Le jeu Space Travel, 1969, par Ken Thompson</figcaption>
</figure>

**Multics** est alors abandonné car jugé trop complexe et il avait des problèmes non résolus : il sera remplacé par le Système d'Exploitation **GECOS** sur **GE-635**, sur lequel Ken Thompson portera son jeu vidéo. Mais le jeu fonctionne moins bien sur l'OS GECOS sur la machine GE-635 qu'avec le couple Multics-GE-635, car le couple GECOS-GE-635 fonctionnait en mode "batch interactif", c'est-à-dire que plusieurs terminaux d'ordinateurs étaient branchés sur l'ordinateur central GE-635, et que chaque tâche de chaque ordinateur devrait être placé dans une queue, se traduisant en pratique en de longues pauses dans le jeu. Ken Thompson demande alors aux Bell Labs l'achat d'une machine DEC PDP-10 ($120\,000 \$$ de l'époque..), au prétexte de développer un nouvel OS, mais l'achat lui sera refusé en raison du désengagement des Bell Labs du projet Multics. Ken Thompson apprend alors l'existence d'une machine DEC PDP-7, de la génération précédente, appartenant à un autre département de la société, et sous-utilisé. Il décide de ne pas porter le jeu sur un OS déjà existant sur cette machine, mais au contraire, il décide de développer un OS pour le PDP-7 : **Unix était né**.  
Le nom *Unix* vient d'une déformation de **Unics** (<b>Un</b>iplexed <b>I</b>nformation and <b>C</b>omputing <b>S</b>ercice) , qui est lui-même un calembour (jeu de mot à vocation humoristique) par opposition à *Multics*. En effet le système *Unix* a été monotâche et mono-utilisateur, durant sa très courte période de développement (Unix Version 0, en $1969$), et il s'est voulu moins ambitieux que *Multics* du moins dans ses débuts : il finira néanmoins par totalement le supplanter, et devenir très rapidement mutitâches et multi-utilisateurs (dès $1970$, Unix Version 1).

<figure style="width:100%;">
<img src="../img/pdp7.jpeg" class="box">
<figcaption>un DEC PDP-7</figcaption>
</figure>

<center>

<iframe src="https://www.youtube.com/embed/pvaPaWyiuLA" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen style="width:100%;height:400px;" class="box"></iframe>
<figcaption>Démarrage (boot) et Login d'Unix version 0, sur un DEC PDP-7</figcaption>

</center>

## Une Chronologie d'Unix

### Début et Années $1970$

<env>$1969$</env> Unix a été pensé dès le départ pour être multi-tâche/multi-processus et multi-utilisateur, néanmoins pendant une courte période initiale de son développement (Unix Version 0), il a d'abord été mono-utilisateur et mono-tâche/mono-processus : Plus précisément, multi-utilisateurs et multi-processus, mais un seul utilisateur pouvait se connecter en même temps, et il disposait d'un usage exclusif de la machine. Idem, multi-processus, mais un seul pouvait être exécuté sen même temps.  
Conscient qu'un Système développé en Assembleur n'est pas du tout maintenable, Ken Thompson s'essaye dès $1969$ à de nombreuses tentatives de réécriture d'Unix dans d'autres Langages, toutes abandonnées : en Langage *TMG*, puis en *Fortran*, finalement Ken Thompson & Dennis Ritchie créent dès $1969$ le **Langage B** (inspiré de *BCPL*) spécialement pour faire évoluer *Unix*, mais le langage B est limité (pas de types de données, toutes les variables ont la même taille) et sera aussi abandonné.  
Initialement, il est prévu de n'utiliser *Unix* que de manière interne aux entreprises *Bell System* : en effet un jugement d'expédient de $1956$ interdit à AT&T de commercialiser autre chose que des équipements téléphoniques.  
<env>$1970$</env> Invention du mot UNIX (calembour original de Brian Kernighan). Unix Version 1, sur un PDP-11. *Unix* devient multi-tâche et multi-utilisateur ($8$ utilisateurs dès Unix version $1$ sur un PDP-11 (DEC), [ref](https://retrocomputing.stackexchange.com/questions/10907/was-unix-ever-a-single-user-os)).  

<figure style="width:100%;">
<img src="../img/thompsonRitchiePdp11.jpg" class="box">
<figcaption>Ken Thompson (assis) et Dennis Ritchie, sur un DEC PDP-11</figcaption>
</figure>

<env>$1971$</env> Unix version 2 à 3.  
<env>$1972$</env> Entre $1972$ et $1973$, *Dennis Ritchie* commence à développemer le Langage **New B**, qui sera renommé le **Langage C**.  
<env>$1973$</env> Ken Thompson et Dennis Ritchie présentent le premier article sur Unix au *Symposium on Operating Systems Principles (Université de Purdue)*. La version 4 d'Unix est réécrite en langage C, bien qu'elle conserve de nombreuses portions de codes dépendantes du matériel **[PDP-11](https://en.wikipedia.org/wiki/PDP-11)** de DEC, bridant ainsi sa portabilité.  
<env>$1974$</env> La version 4 d'Unix est installée à l'Université de Berkely Californie (UCB) par un étudiant de 2ème cycle.  
<env>$1975$</env> La décision fût prise par AT&T de distribuer le système Unix (Versions 6 et 7) complet avec son code source dans les universités à des fins éducatives, moyennant l'acquisition d'une licence au prix très faible. 
La version 6 d'Unix est développée à l'UCB en 1975 en collaboration avec Ken Thompson et d'autres personnees ayant rejoint le projet.  
<env>$1977$</env> La première version d'Unix BSD 1.0 - Berkeley Sofware Distribution est créée par **[Bill Joy](https://fr.wikipedia.org/wiki/Bill_Joy)** (hérite d'Unix versions 5 à 6)

<figure style="width:40%; height:50%;">
<img src="../img/bill_joy.jpg" class="box">

<figcaption>

Bill Joy,<br/>
co-fondateur de Sun Microsystems,<br/>
crée Unix BSD 1.0 en 1977

</figcaption>

</figure>


<env>$1978$</env> 2BSD, et Unix Version 7. Première portabilité d'Unix pour un **[Interdata 8/32](https://en.wikipedia.org/wiki/Interdata_7/32_and_8/32)** (les premiers ordis 32-bits à moins de 10 000\$)  
<env>$1979$</env> la version 3BSD (hérite de version 7 d'Unix)  

### Années $1980$

Au début des années $1980$, le protocole TCP/IP sera incorporé par **[Bill Joy](https://fr.wikipedia.org/wiki/Bill_Joy)**, à l'Unix BSD 4.2. (en fait BSD 4.1.a  -> 4.1.c)
Les **Bell Labs** développent parallèlement les différentes versions d'Unix Système III, puis Système V (mais sans incorporer tout de suite le protocole TCP/IP) qui seront propriétaires et payants. Ils donneront naissance aux **HP-UX**, **Solaris** et **IBM AIX**.  

<env>$1981$</env> Date de sortie du Research **Unix Système III** d'AT&T  
<env>$1982$</env> Création de la société **Sun MicroSystems** par Bill Joy, qui crée le SE commercial et propriétaire **SunOS** dérivé d'Unix Version 7.  
<env>$1983$</env> Seconde Loi antitrust de l'US D.o.J (Department of Justice) menant à l'abolition du **[Bell System](https://en.wikipedia.org/wiki/Bell_System)** :us:, libérant ainsi AT&T du décret de 1956 qui l'empêchait de commercialiser Unix. AT&T s'empresse de commercialiser la version Research **Unix Système V**, qui ne dispose néanmoins pas du protocole TCP/IP.
**Unix Système V** est bien plus contraignante pour une utilisation Académique dans les Universités, c'est pourquoi l'Université de Berkeley continue son développement non commercial parallèle d'Unix BSD. Ceci mène à une compétition accrue entre les principaux fournisseurs d'Unix, commerciaux ou pas, que l'on nommera les **Guerres Unixiennes**.
BSD 4.2 avec TCP/IP en août $1983$.  
<env>$1985$</env> Annonce de la BSD 4.3 en Juin $1985$ avec TCP/IP. Or, le contrat (à plusieurs millions de dollars) avec la DARPA prévoyait que ce soit la société **BBN - Bolt Beranek and Newman-** qui développe le protocole TCP/IP, et non Berkeley, car la DARPA n'avait pas confiance que (ce qu'ils considéraient comme) de simples étudiants de Berkeley puissent comprendre comment fonctionnent les réseaux du monde réel : Le conflit durera 1 an [^2]  

<env>$1986$</env> Sortie de BSD 4.3 en juin $1986$, par autorisation finale (après comparaison des codes entre BSD et BBN : la version BSD TCP/IP avait moins et gérait mieux les pertes de paquets)  
<env>Fin des $1980$'s</env>
Berkeley reçoit des demandes de vendeurs de PC pour incorporer juste le code TCP/IP développé à Berkeley, mais pas la licence AT&T pour tout le produit Unix (Prix de l'époque pour Licence Unix AT&T : $\approx$ 1/4 de millions de dollars)
Berkeley finit par délivrer son code **Unix BSD** avec une Licence dite BSD telle qu'on la connaît aujourd'hui. [Source : Internet Archive, Marshall Kirk McKusick, BSD Talk](https://archive.org/details/bsdtalk170) :headphones: :sound: :us:

### Années $1990$

<env>$1994$ : **Procès Historique et Guerres Unixiennes**</env>
Entre $1989$ et $1994$, BSD purge petit à petit le code copyrighté par AT&T, menant vers **386BSD** puis surtout vers **FreeBSD**. Suite au Procès historique entre **UNIX System Laboratories Inc.** et **Berkeley Sofware Design Inc.**,  le point culminant des [guerres Unixiennes](https://en.wikipedia.org/wiki/Unix_wars) : Accord est finalement donné à l'Université de Berkeley de distribuer Unix-BSD (4.4BSD-lite) avec une **Licence BSD**, moyennant une modification mineure de $6$ fichiers seulement qui posaient des problèmes de copyright avec AT&T... [^3] $^{,}$ [^4]  

Aller plus loin : [Wikipedia : History of Unix](https://en.wikipedia.org/wiki/History_of_Unix) :us: [Internet Archive, Marshall Kirk McKusick, BSD Talk](https://archive.org/details/bsdtalk170) :headphones: :sound: :us:

<clear></clear>

## La Famille Unix

Initialement conçu par les Bell Labs, **[Unix](https://fr.wikipedia.org/wiki/Unix)** a été distribué code source compris, à l'Université de Berkeley, Californie :us:, qui fera évoluer le Système Unix en y incluant ses propres modifications qui elles-mêmes donneront naissance à de nombreux OS dérivés, appelés la **famille Unix** ou **Systèmes de type Unix** ou **Systèmes Unix**, dont les principaux sont :

* la **[famille "Unix BSD" - Berkeley Software Distribution](https://fr.wikipedia.org/wiki/Berkeley_Software_Distribution)** : 
**[FreeBSD](https://fr.wikipedia.org/wiki/FreeBSD)**, **[DragonflyBSD](https://fr.wikipedia.org/wiki/DragonFly_BSD)**, **[NetBSD](https://fr.wikipedia.org/wiki/NetBSD)**, **[OpenBSD](https://fr.wikipedia.org/wiki/OpenBSD)**
* les Systèmes **GNU/Linux** : 
le noyau Linux complété par les logiciels du projet GNU
* **iOS** et **macOS**

## Licences des OS de la Famille Unix

Ces OS dérivés sont principalement **Open Source** et **gratuits**, avec néanmoins certaines exceptions notables, rendues possibles par la très faible restriction de la **[Licence BSD](https://fr.wikipedia.org/wiki/Licence_BSD)** et par sa **compatibilité** avec les logiciels **propriétaires** voire **payants**. Parmi les exceptions (versions payantes et/ou propriétaires) :

* **[Sun OS](https://en.wikipedia.org/wiki/SunOS)**, qui est **propriétaire** et **payant** (renommé **[Solaris](https://fr.wikipedia.org/wiki/Solaris_(syst%C3%A8me_d%27exploitation))** plus tard) et qui utilise du code réseau sous Licence BSD.
**SunOS** (plus tardivement **Solaris**) sont développés par **[Sun MicroSystems](https://fr.wikipedia.org/wiki/Sun_Microsystems)** (rachetés par **[Oracle Corporation](https://fr.wikipedia.org/wiki/Oracle_(entreprise))** en 2009)
* **[Mac OS X](https://fr.wikipedia.org/wiki/MacOS)** est partiellement propriétaire, mais utilise aussi certains composants Libres. En effet, MacOS X, en fait ses précurseurs **[NeXTSTEP](https://fr.wikipedia.org/wiki/NeXTSTEP)** et **[Rhapsody](https://fr.wikipedia.org/wiki/Rhapsody_(syst%C3%A8me_d%27exploitation))** (développés par la société **[NeXT](https://fr.wikipedia.org/wiki/NeXT)** créée par **[Steve Jobs](https://fr.wikipedia.org/wiki/Steve_Jobs)** en 1985, puis rachetée par **[Apple](https://fr.wikipedia.org/wiki/Apple)** en 1996), utilise des composants d' **Unix BSD v4.3** et plus tard **BSD 4.4**. Par la suite, des composants de **FreeBSD** continueront d'être d'intégrés à **Mac OS X** (comme l'**[Espace Utilisateur](https://fr.wikipedia.org/wiki/Espace_utilisateur)** / **userland utilities**, la **librairie C** ou **[kqueue](https://en.wikipedia.org/wiki/Kqueue)**). Source : **[FreeBSD myths](https://wiki.freebsd.org/Myths)**. Aller plus loin : **[Documentation Apple](https://developer.apple.com/library/archive/documentation/Darwin/Conceptual/KernelProgramming/BSD/BSD.html)** :us:

La **[Licence BSD](https://fr.wikipedia.org/wiki/Licence_BSD)** est une **licence libre** permettant de réutiliser tout ou une partie du logiciel sans restriction, pour qu'il soit intégré dans un **logiciel libre** ou **propriétaire**. 

<center>

<div style="width:90%;">

<figure>
<a href="../img/unix.png"><img src="../img/unix.png"></a>
<figcaption><b>UNIX et ses dérivés + OS <em>de type UNIX</em></b></figcaption>
</figure>

</div>

</center>

<clear></clear>

## Sitographie

* Wikipedia : [Unix](https://en.wikipedia.org/wiki/Unix) :gb: et [Unix](https://fr.wikipedia.org/wiki/Unix) :fr:
* [The evolution of the Unix Time-sharing System](http://webarchive.loc.gov/all/20100506231949/http://cm.bell-labs.com/cm/cs/who/dmr/hist.html), par Dennis Ritchie, Bell Labs, $1984$
* [History of Unix](https://landley.net/history/mirror/unix/systemvenvironment.html), inspiré du Livre "The Unix System V Environment" de Stephen R Bourne , 1987, Bell Labs, Addison-Wesley, ISBN 0-201-18484-2
* The **Unix Heritage Society** ou **TUHS** : https://www.tuhs.org/
* [Unix-History-Repo](https://github.com/dspinellis/unix-history-repo) héberge le code historique d'Unix, par Diomidis Spinelli 
* La page github [unix-v0](https://github.com/jserv/unix-v1/blob/master/README.md) héberge le code source (partiel) d' **Unix Version 0** censé tourner sur un **émulateur PDP-7** (dans un ordinateur moderne).
* La page github [unix-v1](https://github.com/jserv/unix-v1/blob/master/README.md) héberge le code source d' **Unix Version 1** censé tourner sur un **émulateur PDP-11** (dans un ordinateur moderne).

## Références

[^1]: [Wikipedia : Space Travel History](https://en.wikipedia.org/wiki/Space_Travel_(video_game)) :us:  
[^2]: [FreeBSD History](https://forums.freebsd.org/threads/history-of-freebsd-part-4-bsd-and-tcp-ip.78733/) :us:  
[^3]: [wikipedia](https://en.wikipedia.org/wiki/UNIX_System_Laboratories,_Inc._v._Berkeley_Software_Design,_Inc.) :us:  
[^4]: [Les Actes du Procès:pdf](http://www.groklaw.net/pdf/USLsettlement.pdf) :us:  


