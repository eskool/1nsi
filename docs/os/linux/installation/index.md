# 1NSI: Installation de Linux (dans une VM)

Dans ce TD, on téléchargera et installera une distribution **Archlinux**, dans une machine virtuelle **VirtualBox**.
On supposera que le logiciel VirtualBox (et/ou vmware) est déjà installé sur votre machine

## Télécharger une image ISO d'une distribution ArchLinux

* Page de Téléchargement : [https://archlinux.org/download/](https://archlinux.org/download/)
* Choisir l'un des serveurs de téléchargement, par exemple français. (ovh.net ?)

## 