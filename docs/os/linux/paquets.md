# 1NSI : Linux - Les Paquets / Packages

## Paquets / Packages

!!! def "Paquet :fr: / Package :gb:"
    Intuitivement, Un <rb>paquet</rb> :gb: / <rb>package</rb>  :gb:(quelquefois *paquetage*), est en quelque sorte un logiciel sur Linux, ou plus généralement un programme apportant certaines fonctionnalités plus ou moins spécifiques à votre système (donc aux autres logiciels/paquets).
    Informatiquement, Un paquet est une **archive** (fichier compressé) qui contient généralement :

    * du **code source**, ou bien des **fichiers binaires** (exécutables) **précompilés**
    * un **fichier de contrôle** contenant: 
        * le nom du logiciel, 
        * sa version, 
        * ses dépendances logicielles, 
        * une **empreinte numérique/somme de contrôle/checksum** (cryptographie) calculée avec une certaine **fonction de hashage** [les plus connues sont **md5** - vieillot-, **sha1**, ou **sha2** (**sha256**, **sha512**, etc..)] pour garantir l'**intégrité (données non altérées) du paquet** (volontairement ou pas): c'est-à-dire pour garantir que le paquet téléchargé localement est bien conforme à l'original sur le serveur.
    Exemple: sommes de contrôle/checksum du logiciel vscodium 1.46
    md5 : f38a954aabd456b0e41c8406e61e58e6
    sha256 : 9c6495d0f36e8213cb6f70d1f7990bc01c4c2a82166f024e09632aa6a9d31621
    * une **signature par clé asymétrique** (cryptographie) pour valider l'**authenticité** du paquet (du développeur/mainteneur): c'est-à-dire pour garantir que le développeur/mainteneur du paquet est bien celui qu'il prétend être.
    * des **fichiers de configuration**
    * des **fichiers de documentation**
    * des **scripts de pré/post installation**
    * des **scripts de pré/post désinstallation**

!!! def "Paquet dépendant (OBLIGATOIREMENT) d'un autre / Dépendances"
    Afin de ne pas réinventer la roue, dans le monde du logiciel libre, il est fréquent qu'un paquet **package1**  développé par un *dev1* utilise les fonctionnalités déjà développées par un ou plusieurs autres développeurs, dans un autre paquet **package2**. On dit que <rb>le *package1* dépend (obligatoirement) du *package2*</rb>, ou bien que <rb>le *package2* est une dépendance (obligatoire) du *package1*</rb>. Cela veut dire que pour être installé et se lancer, le *package1* aura OBLIGATOIREMENT BESOIN que le *package2* soit également (et préalablement) installé sur la machine Linux, sinon:

    * le package1 ne pourra tout simplement pas être installé, 
    * ni même être lancé (pour peu que l'étape précédente ait réussi),
    * ni encore moins fonctionner
    On crée ainsi une <rb>chaîne de dépendance des paquets</rb> les uns par rapport aux autres, sur un système Linux.

!!! def "Paquet dépendant (OPTIONNELLEMENT) d'un autre"
    Il peut arriver qu'un paquet *package1* n'utilise les fonctionnalités d'un paquet *package3* que **facultativement/optionnellement**. Dans ce cas, même si le paquet *package3* n'est pas installé, alors le paquet *package1* :

    * pourra être installé,
    * se lancera,
    * fonctionnera avec ses fonctionnalités de base, néanmoins:
        * certaines fonctionnalités (souvent) avancées (ou très spécifiques) du paquet *package1* ne fonctionneront pas.
    On dit que <rb>le paquet *package1* dépend optionnellement du paquet *package3*</rb>, ou encore que <rb>le paquet *package3* est une dépendance fonctionnelle du paquet *package1*</rb>.

## Comprendre les Paquets

* les paquets sont hébergés sur des serveurs appelés <rb>dépôts</rb> :fr: / <rb>repositories</rb> / <rb>repo</rb> :gb:.
* chaque distribution héberge ses propres <rb>dépôts officiels</rb> contenant:
    * des paquets stables, 
    * avec des licences souvent libres (GPL) et/ou très spécifiques (variables suivant la distribribution)
* pour chaque distribution, il existe également des <rb>dépôts non officiels</rb> permettant d'héberger:
    * des paquets moins stables, mais 
    * davantage à jour (on y trouve les toutes dernières versions du paquet/logiciel)
    * des paquets avec des licences non libres (logiciels propriétaires)
* pour gérer les paquets, on utilise un utilitaire **en ligne de commande** appelé un **gestionnaire de paquet**. :warning: **C'est la méthode recommandée pour installer un logiciel sur Linux** :warning:. Chaque distribution a son propre gestionnaire de paquet (donc nom de commande) différent. Les distributions filles héritent souvent des gestionnaires de paquets de la distribution mère.
* Certaines distributions offrent également (en complément) un **gestionnaire de paquets graphique** (GUI).
* les paquets (i.e. le code source qui a été empaqueté) sont souvent spécifiques/caractéristiques à chaque distribution Linux. Ainsi un paquet qui a été spécifiquement empaqueté sous un certain format pour une certaine distribution, ne fonctionnera pas (du moins pas en l'état actuel) pour une autre distribution.
Exemples:
    * **Manjaro** utilise le format **.xz** ou **.tar.xz** : paquet1.xz ou paquet1.tar.xz
    * **Debian** utilise le format **.deb** : paquet1.deb
    * **Ubuntu** ou **Mint** utilisent le format **.rpm** (Redhat Package Manager) : paquet1.rpm
    * etc..
    * Des formats différents reflètent une structure différente (entre deux distributions) aussi bien pour l'empaquetage, que pour le dépaquetage.
* Il est quelquefois possible (plus ou moins simplement) de transformer le format d'un paquet pour le rendre utilisable sur une autre distribution. Néanmoins : Ce n'est néanmoins pas la méthodologie d'installation conseillée.
* Notez également que sur Linux, bien que ce ne soit **PAS LA PROCÉDURE RECOMMANDÉE**, on peut toujours **compiler directement le code source** d'un logiciel n'existant pas dans un dépôt (qui n'a donc pas encore été empaqueté sous la forme d'un paquet pour une distribution spécifique): cela est équivalent à ***installer*** ce logiciel. Il suffit de suivre une méthodologie qui est **identique pour toute distribution Linux**, avec les commandes successives suivantes : **```source```, ```make```, ```make install```** (culture : *make* est une commande du *projet GNU*). En pratique, cette manière d'installer les logiciels est **(fortement) déconseillée pour la plupart des utilisateurs**, et est **réservée/conseillée aux utilisateurs avertis**.
* Certaines distributions sont plus ou moins compatibles entre elles: en particulier, les distributions filles héritent souvent des paquets des distributions mères, donc aussi de leur bibliothèque de programmes et/ou de tutoriels.
* Notez enfin que depuis $2007$, il existe un gestionnaire de paquets universel, **[Flatpack](https://flatpak.org/)**, qui propose des paquets **universels** donc compatibles pour toutes les disctributions. Une liste des logiciels installables est disponible sur **[Flathub](https://flathub.org/home)**.

## Gestionnaire de Paquets / Packages Manager

### Définitions

!!! def "Gestionnaire de Paquets"
    Afin de gérer les paquets (installation, désinstallation, recherche/liste des paquets, etc..), la méthode préconisée est d'utiliser un utilitaire en ligne de commande, dans un Terminal, usuellement appelé un <rb>Gestionnaire de Paquets</rb> :fr: / <rb>Package Manager</rb> :gb:. En pratique, Chaque Distribution à son propre gestionnaire de paquet (certaines s'inspirent d'autres) :

    * **Manjaro** dispose de **pacman** pour les dépôts officiels, et de **yay** (ou anciennement **yaourt**) pour le dépôt non officiel AUR (ArchLinux User Repository)
    * **Debian**, **Ubuntu** et **Mint** disposent de **apt** (Advanced Packaging Tool) (ou de son ancêtre **apt-get**)
    * **Fedora/CentOS/OpenSUSE** disposent de **rpm** (Redhat Package Manager)

!!! def "Installeurs Graphiques"
    Certaines (la plupart, de nos jours) distributions proposent également des <rb>installeurs graphiques (GUI)</rb> de paquets :
    
    * **Manjaro** dispose de **[pamac](https://wiki.manjaro.org/index.php/Pamac)** qui gère les dépôts officiels et non officiels
    * **Debian**, **Ubuntu** et **Mint** disposent de **[Synaptic](https://fr.wikipedia.org/wiki/Synaptic)**
    * **Fedora/CentOS/OpenSUSE** disposent de **[Gnome Logiciels](https://fr.wikipedia.org/wiki/GNOME_Logiciels)**, lui même basé sur [PackageKit](https://fr.wikipedia.org/wiki/PackageKit)

### L'exemple de Manjaro Linux

Voici un exemple d'utilisation d'un Gestionnaire de paquets, qui est
certes :warning: **spécifique à Manjaro Linux** :warning:, et à son gestionnaire de paquets ```pacman``` , MAIS il pourrait être assez simplement adapté à un autre gestionnaire de paquets d'une autre distribution :

* ```$ pacman -Qs motOuNomPaquet``` : Cette commande, à taper dans un Terminal, recherche (search = **```-s```**) parmi les paquets installés localement ( **```-Q```** ) ceux contenant la chaîne de caractères *motOuNomPaquet* : 
    * dans le *Nom* du paquet, ou bien 
    * dans la *Description* du paquet
* ```$ pacman -Qi nomPaquet``` : Cette commande, à taper dans un Terminal, affiche les infos ( **```-i```** ) détaillées concernant les dépendances d'un paquet précédemment installé en local **```-Q```**
    Cette commande affiche notamment les informations suivantes:
    * **Licences** = la ou les Licences du paquet *nomPaquet*
    * **Groupes** = Groupes de paquets auquel *nomPaquet* appartient (si ces groupes existent)
    * **Fournit** = (normalement) nouvelle commande qui sera disponible après installation du paquet (mal utilisé)
    * **Dépend de** = les dépendances obligatoires du paquet *nomPaquet* (s'il y en a)
    * **Dépendances opt** = les dépendances Optionnelles du paquet *nomPaquet* (s'il y en a)
    * **Requis par** = le nom des paquets pour lesquels *nomPaquet* est une dépendance obligatoire (s'il y  en a)
    * **Optionel pour** = le nom des paquet pour lesquels *nomPaquet* est une dépendance Optionnelle  (s'il y en a)
    * **Est en conflit avec** = le nom des paquet avec lesquels *nomPaquet* est en conflit (s'il y en a)
    * **Paqueteur** = le nom + email du **mainteneur** du paquet. La personne qui **maintient** le paquet.

!!! exp
    Pour visualiser les dépendances du paquet **gimp** (logiciel de retouches photo) :  

    ```bash
    $ pacman -Qi gimp
    Nom                      : gimp
    Version                  : 2.10.18-8
    Description              : GNU Image Manipulation Program
    Architecture             : x86_64
    URL                      : https://www.gimp.org/
    Licences                 : GPL  LGPL
    Groupes                  : --
    Fournit                  : --
    Dépend de                : babl  dbus-glib  desktop-file-utils  gegl  glib-networking  hicolor-icon-theme  openjpeg2  lcms2
                            libheif  libexif  libgudev  libmng  libmypaint  librsvg  libwebp  libwmf  libxmu  libxpm
                            mypaint-brushes1  openexr  poppler-data  gtk2
    Dépendances opt.         : gutenprint: for sophisticated printing only as gimp has built-in cups print support
                            poppler-glib: for pdf support [installé]
                            alsa-lib: for MIDI event controller module [installé]
                            curl: for URI support [installé]
                            ghostscript: for postscript support [installé]
    Requis par               : --
    Optionnel pour           : --
    Est en conflit avec      : gimp-plugin-wavelet-decompose
    Remplace                 : gimp-plugin-wavelet-decompose
    Taille installée         : 110,77 MiB
    Paqueteur                : Antonio Rojas <arojas@archlinux.org>
    Compilé le               : mar. 12 mai 2020 09:07:15
    Installé le              : dim. 31 mai 2020 21:36:42
    Motif d’installation     : Explicitement installé
    Script d’installation    : Oui
    Validé par               : Signature
    ```

!!! ex
    Taper les commandes suivantes dans un Terminal (comme utilisateur courant). Que font-elles / quel est le résultat obtenu?
    ```bash
    $ pacman -Qs gim            # Que fait cette commande?
    $ pacman -Qs | grep gim     # Que fait cette commande? Rem: le caractère "|" est un 'pipe' raccourci clavier : AltGr+6
                                # quelle.s différence.s avec la commande précédente?
    $ pacman -Qs | grep local/
    ```

    1. Que fait la commande ```grep``` selon vous?
    2. Quelle commande utilisant ```grep``` doit-on taper dans un Terminal, pour lister tous les fichiers et dossiers du répertoire courant, contenant la chaîne '**Do**'
    3. Quelle.s commande.s doit on taper dans un Terminal pour savoir si le logiciel ***LibreOffice*** est installé sur votre machine?
    Et si OUI: 

        * Quel est le nom précis du paquet? 
        * Quelle version est-elle actuellement installée sur la machine?
        * Quelle est la License de ce paquet?
        * Appartient-il à un groupe de paquets?
        * Quel est le nom du mainteneur?
        * De quels paquets (obligatoires) LibreOffice dépend-il?  (en citer quelque uns)
        * De quels paquets Optionnels dépend-il? (en citer quelque uns)
        * Ce paquet est-il requis par un/d'autres paquets?
        * Est-il en conflit avec un/des paquet.s?
