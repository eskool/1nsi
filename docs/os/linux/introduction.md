# 1NSI : Qu'est-ce que Linux ?

!!! def
    Le mot <rb>Linux</rb> représente, selon les contextes :

    * au sens restreint : le **noyau Linux** du Système d'Exploitation Linux
    * au sens large : tout système d'Exploitation fondé sur le **noyau Linux**.

## Le noyau Linux

<div style="float:right;width:24%;">

<center>

<figure>
<img src="../img/tux.png">
<figcaption><b>TUX, <br/>La mascotte du noyau Linux</b></figcaption>
</figure>

</center>

</div>

<div style="float:right;width:24%;">

<center>

<figure>
<img src="../img/linus_torvalds_young.jpg">
<figcaption><b>Linus Torvalds, 21 ans</b></figcaption>
<img src="../img/linus_torvalds_now.jpg">
<figcaption><b>Linus Torvalds,  aujourd'hui</b></figcaption>
</figure>

</center>

</div>

Le **[noyau Linux](https://github.com/torvalds/linux)** est un **noyau de Système d'Exploitation** de type **Unix**.
Il a été développé par **[Linus Torvald](https://fr.wikipedia.org/wiki/Linus_Torvalds)** en 1991 (à 21 ans), pour les ordinateurs personnels compatibles PC (processeurs x86), mais à l'origine, il est totalement dépourvu de logiciels qui l'accompagnent. Historiquement, le **[noyau Linux](https://fr.wikipedia.org/wiki/Noyau_Linux)** est inspiré **fonctionnellement** du Système d'Exploitation **[Unix](https://fr.wikipedia.org/wiki/Unix)** ou **[UNIX](https://fr.wikipedia.org/wiki/Unix)** ($1969$).

En revanche,  il n'y a **pas d'héritage de code source ni d'architecture entre Unix et Linux**: les sytèmes Unix ont été conçus pour fonctionner sur les **gros systèmes de l'époque** (les DEC **[PDP-7](https://fr.wikipedia.org/wiki/PDP-7)**), tandis que Linux a été conçu pour fonctionner sur les nouvelles architectures de **micro-ordinateurs** conçues à partir des nouveaux processeurs d'**Intel** de l'époque : les $80386$ etc.. (processeurs x$86$)

### Licence du noyau Linux

**Licence GPL v2**, sauf quelques **[BLOB (Binary Large Object)](https://fr.wikipedia.org/wiki/Binary_large_object)** qui sont principalement des pilotes propriétaires fournis par les industriels de matériels spécifiques.


### Android

Le noyau Linux est également utilisé avec **Android**

<clear></clear>

## Les Systèmes d'Exploitation GNU/Linux

Le **noyau Linux** se complémente parfaitement avec les logiciels du **projet GNU** pour donner ce que l'on appelle les **Systèmes (d'Exploitation) GNU/Linux**, ou quelquefois plus simplement **Linux** (à tord, ou du moins, par trop grande simplification).