# 1NSI : Groupes Linux

On peut créer des <rb>groupes d'utilisateurs</rb>, ou simplement des <rb>groupes</rb>, contenant (aucun,) un ou plusieurs utilisateurs, et affecter à ces groupes des droits spécifiques. Tous les utilisateurs appartenant à ces groupes bénéficieront ainsi de leurs droits.
En outre, lors de l'installation du système, tout un tas de **groupes système** avec des droits spécifiques ont été créés.
Un groupe peut contenir (aucun,) un ou plusieurs utilisateurs.
Un utilisateur appartient **au minimum** à un groupe: il peut donc appartenir à un ou plusieurs groupes. 

!!! def "Groupe primaire & groupes secondaires"
    Tout utilisateur appartient à un <rb>groupe primaire</rb>, qui est par définition, le groupe par défaut auquel appartiennent les fichiers et dossiers qui seront créés par cet utilisateur:
    
    * Si un utilisateur n'appartient qu'à un seul groupe, alors ce groupe est son groupe primaire.
    * Si un utilisateur appartient à plusieurs groupes, alors l'un de ces groupes est son groupe primaire, les autres sont ses <rb>groupes secondaires</rb>.

En général, mais non obligatoirement, lors de la création par root d'un utilisateur usuel *userName* dans le système, un groupe du même nom (*userName*) est créé: dans ce cas très fréquent, l'utilisateur *userName* appartient également au groupe de même nom *userName*.

!!! exp
    On pourrait créer un groupe *famille*, et un autre *amis*, et leur affecter des droits différents et spécifiques: 

    * la famille pourrait voir les photos du dossier ~/Documents/Images/famille
    * les amis pourrait voir les photos du dossier ~/Documents/Images/amis
