| Contenus | Capacités<br/>Attendues | Commentaires |
|:-:|:-:|:-:|
| Écriture d’un entier<br/>positif dans une base <br/>$b \ge 2$ | Passer de la représentation<br/>d’une base dans une autre. | Les bases $2$, $10$ et $16$ sont<br/>privilégiées. |