| Contenus | Capacités<br/>Attendues | Commentaires |
|:-:|:-:|:-:|
| Représentation<br/>approximative des<br/>nombres réels : notion<br/>de nombre flottant | Calculer sur quelques<br/>exemples la représentation de<br/>nombres réels : 0.1, 0.25 ou<br/>1/3. | 0.2 + 0.1 n’est pas égal à 0.3.<br/>Il faut éviter de tester l’égalité de<br/>deux flottants.<br/>Aucune connaissance précise de<br/>la norme IEEE-754 n’est exigible. |
