| Contenus | Capacités<br/>Attendues | Commentaires |
|:-:|:-:|:-:|
|Valeurs booléennes : 0,1.<br/>Opérateurs booléens :<br/>and, or, not.<br/>Expressions booléennes | Dresser la table d’une<br/>expression booléenne. | Le ou exclusif (xor) est évoqué.<br/>Quelques applications directes<br/>comme l’addition binaire sont<br/>présentées.<br/>L’attention des élèves est attirée<br/>sur le caractère séquentiel de<br/>certains opérateurs booléens. |
