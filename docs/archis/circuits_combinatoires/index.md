# 1NSI : Architectures Matérielles<br/>Circuits Combinatoires

D'une manière générale, les circuits électroniques possèdent plusieurs entrées et plusieurs sorties.

>**Définition :** Lorsque l’état (de la ou) des sorties à un instant donné ne dépend *directement* et *uniquement* **que** de la valeur des entrées **à cet instant** (ce n'est pas le cas pour tous les circuits), on parle d'un **circuit combinatoire** ou **circuit logique**.

En pratique, un ***circuit combinatoire*** est un ensemble de portes logiques reliées entre elles de sorte à répondre à une certaine expression algébrique, préalablement simplifiée grâce aux Lois de composition.

La construction de circuits (combinatoires ou autres) peut être vu comme un jeu de Lego dans lequel on assemble des circuits plus complexes. En général, on ne part pas de portes logiques, mais de circuits un peu plus complexes conçus à partir de portes logiques.

<br>

**Voici Quelques Exemples de circuits Combinatoires Classiques:**

* un (en)codeur vs un décodeur 
* un Multiplexeur (MUX) vs un Démultiplexeur (DEMUX) 
* des circuits arithmétiques:
  * addition
  * soustraction
  * décalage (en particulier pour multiplication et division)
* comparaison des entrées (test d'égalité, >, <, etc..)

## (En)Codeur $2^n$ entrées $\Rightarrow$ $n$ sorties

>**Définition :** Un **(en)codeur** est un circuit qui:
> 
> * reçoit une information sur une entrée (parmi $2^n$ entrées) et qui
> * code/renvoie en sortie la valeur Décimale Codée en Binaire (DCB) sur $n$ bits du numéro de l'indice de l'entrée active. Une seule entrée doit être activée à la fois (des codeurs à plusieurs entrées actives existent: *codeurs à priorité*).

**Utilisation :** On utilisera par exemple un (en)codeur pour fournir à un système de calcul le code *Décimal Codé Binaire* (DCB) correspondant à une touche enfoncée de clavier de  console ou  de  machine  à écrire.

## Décodeur $n$ entrées $\Rightarrow$ $2^n$ sorties

>**Définition :** Un **décodeur $n$ bits** est un circuit qui sélectionne une sortie à partir des entrées. Plus précisément il:
>
> * lit sur ses entrées un nombre $i$ sur $n$ bits, et qui
> * active (/met à 1) la $i$-ème sortie (parmi les $2^n$), i.e. la sortie dont le numéro $i$ est égal au nombre codé en binaire par les entrées. Il met toutes les autres sorties à 0. Une seule entrée est active à la fois.

**Utilisation :** Un décodeur est utilisé par exemple:

* pour commander chaque segment d'un afficheur à diodes électroluminescentes (pour écrire des chiffres en fonction d'une entrée codée en binaire ou en DCB)
* pour adresser un périphérique parmi plusieurs.

Par exemple, un décodeur 2 bits sur 2 entrées $(e_0,e_1)$ et 4 sorties $(s_0,s_1,s_2,s_3)$ correspond à la table de vérité suivante:

| $e_0$  | $e_1$ | $s_0$ | $s_1$ | $s_2$ | $s_3$ |
|:-:|:-:| :-:|:-:|:-:|:-:|
| 0  | 0 | 1 |0 |0 |0 |
| 0  | 1 | 0 |1 |0 |0 |
| 1  | 0 | 0 |0 |1 |0 |
| 1  | 1 | 0 |0 |0 |1 |

Un décodeur peut se définir en assemblant des portes NOT et AND, en effet en suivant la méthode générale, pour chacune des sorties:  
$s_0=\overline{e_0}.\overline{e_1}$  
$s_1=\overline{e_0}.e_1$  
$s_2=e_0.\overline{e_1}$  
$s_3=e_0.e_1$  

![Schéma d'un Décodeur 2 bits](../img/decodeur_2bits.png)


![Photo Circuit Intégré Décodeur Encodeur CD4051BE, Texas Instruments](../img/img_decodeur_encodeur.jpg){width="20%"}

## Additionneur

### Demi-Additionneur 1 bit:  

Un ***demi-additionneur*** 1 bit prend en entrée 2 bits $e_0$ et $e_1$ et il envoie :

  * sur une seule sortie $s$ la somme $e_0 + e_1$ 
  * Si ce calcul admet une retenue, alors le nombre $1$ est envoyé sur une autre sortie $c$

Table de vérité:

| $e_0$  | $e_1$ | $s$ | $c$ |
|:-:|:-:| :-:|:-:|
| 0  | 0 | 0 | 0 |
| 0  | 1 | 1 | 0 |
| 1  | 0 | 1 | 0 |
| 1  | 1 | 1 | 1 |

On a les égalités:

* $s=e_0 \oplus e_1$
* $c=e_0.e_1 = e_0 \land e_1$

On en déduit le schéma d'un demi-additionneur 1 bit.

![Schéma du Demi-Additionneur 1 bit](../img/demi_additionneur_1bit.png)

### Additionneur 1 bit complet

L'additionneur 1 bit complet prend en entrée 2 bits $e_0$ et $e_1$, ainsi que la retenue $c_0$ d'une autre addition. Il renvoie en sortie l'addition $s=e_0 \oplus e_1 \oplus c_0$, en plaçant éventuellement une retenue sur $c$.

**Avantage :**  
L'avantage de cet additionneur est de permettre le chaînage des circuits.

**Table de Vérité:**

| $e_0$  | $e_1$ | $c_0$ | $s$ | $c$ |
|:-:|:-:| :-:|:-:|:-:|
| 0 | 0 | 0 | 0 | 0 |
| 0 | 0 | 1 | 1 | 0 |
| 0 | 1 | 0 | 1 | 0 |
| 0 | 1 | 1 | 0 | 1 |
| 1 | 0 | 0 | 1 | 0 |
| 1 | 0 | 1 | 0 | 1 |
| 1 | 1 | 0 | 0 | 1 |
| 1 | 1 | 1 | 1 | 1 |

L'additionneur 1 bit complet renvoie en sortie:

* $s_0=e_0 \oplus e_1$
* $s=s_0 \oplus c_0$
* la retenue $c$ finale: $c=(e_0.e_1)+(c_0.s_0)$

d'où le schéma d'un additionneur 1 bit complet:

![Schéma de l'Additionneur Complet 1 bit](../img/additionneur_complet_1bit.png)

### Additionneur $n$ bits, ou Additioneur par propagation de retenue

On peut additionner $n$ bits en chaînant $n$ additioneurs $1$ bit.

**Exemple: Additioneur 4 bits**

![Schéma d'un Additionneur 4 Bits](../img/additionneur_4bits.png){width="90%"}

## Multiplexeur (MUX) $n$ $\Rightarrow$ $1$

>**Définition :** Un **multiplixeur (MUX)** est un circuit sélectionnant une **entrée de données** à partir des **entrées de commande** (ou **entrées de contrôle**).

Les multiplexeurs sont des composants qui possèdent un nombre variable d'entrées et une sortie. Le rôle d'un multiplexeur est de recopier le contenu d'une des entrées sur sa sortie. Bien sûr, il faut bien choisir l'entrée qu'on veut recopier sur la sortie : pour cela, notre multiplexeur contient une entrée de commande (ou entrée de contrôle) qui permet de spécifier quelle entrée doit être recopiée. Le multiplexeur le plus simple est le multiplexeur à deux entrées et une sortie.


## Démultiplexeur (DEMUX) $1$ $\Rightarrow$ $n$


![Photo (Dé)Multiplexeur MUX/DEMUX 12GM, Aja](../img/img_mux_demux.jpg){width="40%"}

# Circuits Séquentiels

>**Définition :** Dans un ***circuit séquentiel***, L’état de sortie du circuit à un instant donné dépend de la valeur des entrées à cet instant et de la valeur de la (ou des) sortie(s) **aux instants antérieurs**.

Ce type de circuits possèdent donc une capacité de mémorisation (appelée état du circuit) qui est utilisée pour construire des composants mémoire (RAM, registres, etc..). Etant plus complexes, ils ne seront pas développé dans ce cours