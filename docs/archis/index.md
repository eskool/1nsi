# Architectures Matérielles, Systèmes d'Exploitation et Réseaux

## Modèle d'Architecture Séquentielle de Von Neumann

## Ressources

* [Logique Booléenne](https://www.arte.tv/fr/videos/105438-000-A/george-boole-genie-des-maths/) Vidéo Arte :gb: (59 min 30)
* [Architectures et Paradigmes de Programmation Parallèle, Violaine Louvet, CNRS Lyon](http://plasmasfroids.cnrs.fr/IMG/pdf/ECOMOD2010_Louvet1.pdf)
* [Microprocessor-trend-data, Dépôt Github](https://github.com/karlrupp/microprocessor-trend-data)