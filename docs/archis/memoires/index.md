# 1NSI : Architectures - Mémoires

## Introduction

En informatique, la mémoire est un dispositif électronique numérique qui sert à **stocker des données**.

La mémoire est un composant essentiel, présent dans tous les ordinateurs, les consoles de jeux, les GPS et de nombreux appareils électroniques.

Les mémoires sont vendues sous forme de pièces détachées de  **composants pour PC** (matériel informatique), ou de **composants électroniques**. Les différences entre les pièces sont :

* la forme, 
* l'usage qui en est fait, 
* la technologie utilisée, 
* la capacité de stockage et 
* le rapport entre le coût et la capacité

La technologie la plus courante utilise des **semi-conducteurs** parfois associés à des composants mécaniques[réf. souhaitée]. On distingue :

* mémoire vive
* mémoire morte et 
* mémoire de masse

La **mémoire** de l'ordinateur contient à la fois les **programmes** et les **données**. On distingue habituellement deux types de mémoire:

![Mémoires](../img/memoires.svg){.center}


## la RAM - mémoire Vive - mémoire Volatile

La <bred>mémoire vive</bred>, ou <bred>mémoire volatile</bred>, ou <bred>mémoire non persistante</bred>, encore appelée <bred>mémoire  RAM</bred> pour <bred>Random-Access Memory</bred> :gb:, est celle qui perd son contenu lorsque l'ordinateur est éteint. Les donnés stockées dans la mémoire vive d'un ordinateur peuvent être lues, effacées, ou déplacées comme on le souhaite. 
<env>Avantage principal</env> sa **rapidité d'accès aux données qu'elle contient** (temps d'accès aléatoires de **quelques dizaines ou centaines de nanosecondes**), quel que soit l'emplacement mémoire de ces données.

## La ROM - mémoire morte - mémoire non volatile

La <bred>mémoire non volatile</bred> ou <bred>persistante</bred> ou <bred>rémanente</bred>, est celle qui conserve ses données quand on coupe l'alimentation électrique de l'ordinateur. Il en existe plusieurs types:

* la <bred>mémoire morte</bred> :fr:, ou <bred>mémoire ROM</bred> pour <bred>Read-Only Memory</bred> :gb:, est une mémoire non volatile **non modifiable/non programmable** qui contient habituellement des données nécessaires au démarrage d'un ordinateur ou toute autre information dont l'ordinateur a besoin pour fonctionner.
Originalement, son contenu est fixé définitivement lors de sa fabrication et n'est pas modifiable. Néanmoins, avec l'évolution des technologies, la définition d'une mémoire morte/ROM a été élargie pour inclure des mémoires non volatiles dont le contenu est fixé lors de leur fabrication, et qui ne sont pas prévues *à priori* pour être modifiées, mais qui le sont de facto pour certaines. Un utilisateur expérimenté aurait la possiblité de les modifier, avec un matériel spécial. Elles sont classées selon la possibilité de les programmer et/ou de les effacer. Exemples:  
* les [PROM](https://fr.wikipedia.org/wiki/M%C3%A9moire_morte_programmable) ou **<bred>P</bred>rogrammable <bred>ROM</bred>** sont des mémoires mortes (non volatiles) programmables/modifiables par l'utilisateur, mais une seule fois.
* les [EPROM](https://fr.wikipedia.org/wiki/EPROM_(informatique)) ou **<bred>E</bred>rasable <bred>P</bred>rogrammable <bred>ROM</bred>**, sont des mémoires mortes (non volatiles) Effaçables et Programmables par l'utilisateur
* les [UVPROM](https://fr.wikipedia.org/wiki/Ultra_Violet_Programmable_Read_Only_Memory) ou **<bred>U</bred>ltra <bred>V</bred>iolet <bred>P</bred>rogrammable <bred>ROM</bred>**, sont des mémoires mortes (non volatiles) <b>E</b>ffaçables, mais en les plaçant de manière contraignante dans des chambres à UltraViolet, qui sont aussi <b>P</b>rogrammables. Elles ont été remplacées par les EEPROM, qui ne nécessitent pas d'être extraites de l'appareil pour être reprogrammées.
* les [EEPROM](https://fr.wikipedia.org/wiki/Electrically-erasable_programmable_read-only_memory) ou **<bred>E</bred>lectrically <bred>E</bred>rasable <bred>P</bred>rogrammable <bred>ROM</bred>**, sont des mémoires mortes (non volatiles) effaçables et programmables par l'utilisateur. Plus faciles à effacer que les EPROM car elles sont effaçables électriquement donc sans manipulations physiques, et que les UVPROM. Elles constituent désormais le principal type de mémoire morte, et se retrouvent couramment dans la **mémoire Flash** :
  * La **mémoire Flash**, est un des principaux exemples de **mémoire de masse** (=de grande capacité, non volatile, et qui peut être lue et écrite) **de type EEPROM**. Contrairement à la ROM, cette mémoire est **modifiable** (un certain nombre de fois) et les informations qu'elle contient sont accessibles de manière uniforme. Contrairement à la RAM, ces mémoires sont beaucoup plus lentes, aussi bien pour lire les données, que pour les modifier.

## La mémoire Cache

XXXXXXXXXXXXXX

## La mémoire virtuelle ou <bred>swap</bred>

Une partie du disque sert à simuler un supplément de mémoire.
On parle alors de « <bred>mémoire virtuelle</bred> » ou de <bred>swap</bred>.

## Hiérarchie de mémoires : une Comparaison

Pour des raisons technologiques (l’augmentation de la taille d’une mémoire s’accompagne toujours  de  l’augmentation du  temps d’accès)  et pour des  raisons  économiques  (plus  rapide = plus cher), les constructeurs utilisent différents  types  de  mémoires. La  vitesse  d’une  mémoire (temps  d’accès  et/ou débit) est inversement proportionnel à sa taille. Le diagramme et le tableau suivants résument leurs relations :

<center>

<div style="width:40%;">

<img src="../img/vitessesmemoiresdiagramme.png">

<img src="../img/vitessesmemoirestableau.png">

</div>

</center>


## Aller Plus Loin

* Mémoire : en **[français :fr:](https://fr.wikipedia.org/wiki/M%C3%A9moire_(informatique))**, en **[anglais :gb:](https://en.wikipedia.org/wiki/Computer_data_storage)** sur wikipedia
* **[Mémoire Vive](https://fr.wikipedia.org/wiki/M%C3%A9moire_vive)** sur wikipedia


