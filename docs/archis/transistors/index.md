# 1NSI : Transistors

## Invention

### Le Transistor Américain

<clear></clear>

!!! col __60
    À la suite des travaux sur les semi-conducteurs, le <rbl>Transistor Bipolaire</rbl> (<b>TRANS</b>fer res<b>ISTOR</b>) a été réalisé pour la première fois le $23$ décembre $1947$ par les Américains **John Bardeen** :us:, **William Shockley** :us: et **Walter Brattain** :us:, chercheurs des **Laboratoires Bell**. Ces chercheurs ont reçu pour cette invention le Prix Nobel de Physique en $1956$.

!!! col __40 clear
    ![Inventeurs Transistor](./img/inventeurs-transistor.webp)

    <figcaption>
    John Bardeen (gauche), William Shockley (milieu), Walter Brattain (droite),<br/>:sk-copyright: Nokia Corporation
    </figcaption>

### Le Transistron Européen

Deux physiciens allemands, **Herbert Mataré** :de: et **Heinrich Welker** :de:, ont aussi développé parallèlement et indépendamment le « transistor français » en juin $1948$, appelé <rbl>Transistron</rbl> par la presse dès $1949$,  alors qu’ils travaillaient à la **Compagnie des Freins et Signaux** à Paris. Initialement, considéré **plus résistant et plus stable** que le transistor américain, le transistron ne sera pas soutenu par le gouvernement français, trop focalisé sur la technologie nucléaire. Petit à petit, le transistron est mis à l’écart et perd son avantage face au transistor: Une illustration des défaillances de l'Histoire Européenne en matière de Nouvelles Technologies ?

### Évolution du Tube Electronique

Le **transistor** est considéré comme un énorme progrès face au **tube électronique** : beaucoup plus petit, plus léger et plus robuste, fonctionnant avec des tensions faibles, autorisant une alimentation par piles, il fonctionne presque instantanément une fois mis sous tension, contrairement aux tubes électroniques qui demandaient une dizaine de secondes de chauffage, généraient une consommation importante et nécessitaient une source de tension élevée (plusieurs centaines de volts). 

## Qu'est-ce qu'un Transitor ?

<iframe id="one" width="560" height="315" src="https://www.youtube.com/embed/Ey1nnEoADcg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<center>

<figcaption>

Les Transistors Expliqués, Esprit Ingénieur <br/>(18'20)

</figcaption>

</center>

<iframe id="two" width="560" height="315" src="https://www.youtube.com/embed/Uqvk7x6nmfg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<center>

<figcaption>

Les Transistors, InterGalac'Geek<br/>(8'48)

</figcaption>

</center>

!!! col __70
    Un Transistor est un dispositif semi-conducteur à $3$ électrodes actives. Il existe plusieurs technologies de Transistor, mais elles ont toutes en commun de permettre le contrôle d'un **courant** ou d'une **tension** sur l'**électrode de sortie**. 
    En d'autres termes, **c'est un interrupteur contrôlé électroniquement, sans partie mécanique**.

!!! col __30 clear
    ![Transistors](./img/transistors.jpg)

* Transistor Bipolaire
* Transistor à Effet de Champ / FET - Field Effect Transistor, eux-mêmes divisés en :
    * [Transistors MOSFET](https://fr.wikipedia.org/wiki/Transistor_%C3%A0_effet_de_champ_%C3%A0_grille_m%C3%A9tal-oxyde): qui utilisent les propriétés des structures Métal/Oxyde/Semi-conducteur
    * Transistors JFET : ils utilisent les propriétés des jonctions PN.
* Transistor Unijonction : Il n’est quasiment plus utilisé, mais servait à créer des oscillateurs à relaxation.
* 



* le **collecteur** pour le transistor bipolaire, 
* le **drain** sur un transistor à effet de champ)

grâce à une électrode d'entrée

* la **base** sur un transistor bipolaire, et 
* la **grille** pour un transistor à effet de champ

Le circuit étant connecté aux bornes « collecteur » et « émetteur », le transistor est isolant sans tension sur la borne Base, et conducteur avec une tension sur la borne Base.


## Utilisations Pratiques

* $1954$ : La première application du transistor fut pour la radio en $1954$, soit $7$ ans après la découverte du transistor. 
* $1958$ : Invention du <rbl>Circuit Intégré (CI)</rbl>, groupant en un petit volume plusieurs transistors et composants, 

    <iframe width="560" height="315" src="https://www.youtube.com/embed/L_EFljkLz_M" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

    <center><figcaption>

    Les Portes Logiques<br/><a href="https://www.youtube.com/watch?v=L_EFljkLz_M">InterGalac'Geek</a>

    </figcaption></center>

* $1969$ : Invention du <rbl>microprocesseur ($\mu p$)</rbl>, permettant à des milliers de transistors de fonctionner en harmonie sur un support, ce qui est encore une fois une révolution pour l’informatique moderne. 
* $1971$ : 

    !!! col __70
        L'<rbl>Intel 4004</rbl>, premier microprocesseur commercial, sorti en mars $1971$ et commandité par **Busicom**, intègre **$2\,250$ transistors** et exécute $60\,000$ opérations par seconde.

    !!! col __30
        ![Intel C4004](./img/Intel_C4004.png)

### Loi de Moore

(à faire)

### Références

* [Transistors & Portes Logiques](https://www.irif.fr/~carton/Enseignement/Architecture/Cours/Gates/)
* [Transistor Basics](https://www.digikey.fr/fr/articles/transistor-basics)
* https://fr.m.wikipedia.org/wiki/Transistor