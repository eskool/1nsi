# 1NSI : Histoire des Ordinateurs : Les Machines Arithmétiques-<br/>La Pascaline & Machine de Leibniz


## La machine arithmétique <bred><em>Pascaline</em></bred>, de Blaise Pascal, $1645$

### Introduction

<center>

<iframe width="560" height="315" src="https://www.youtube.com/embed/GX4RQK__fQc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Additionner avec la Pascaline, $1645$ ($1$ min $11$)

<iframe width="560" height="315" src="https://www.youtube.com/embed/hSl2WFfCTD8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Mécanisme interne de la Pascaline ($2$ min $06$)

</center>

### Quelques caractéristiques techniques

!!! col __50
    ![La Pascaline](../img/pascaline.jpg)
    La Pascaline, $1645$, Blaise Pascal

Considérée comme la première machine à calculer mécanique :

* utilise des **[pignons lanternes](https://fr.wikipedia.org/wiki/Engrenage#Lanternes_et_rouets)**, empruntés aux machines de force (moulins à eau, horloges de clocher) que Pascal avait adaptés et miniaturisés pour sa machine
* un **[sautoir](https://fr.wikipedia.org/wiki/Pascaline#Le_sautoir)** : c'est une invention de Pascal, en charge de gérer les retenues
* Arithmétique : Elle calcule :
    * des additions
    * des soustractions
    * PAS de multiplications, NI de divisions : ou alors... **par répétitions** des deux opérations précédentes ...

C'est une machine arithmétique dont il eût l'idée en $1642$ (à $19$ ans..) pour soulager la tâche de son père, comptable aux impôts. Après $3$ ans de recherche, en $1645$, naquit la première *Pasqualine*, qu'il améliora plusieurs fois.

## Machine Arithmétique de Leibniz, $1694$

### Introduction

<center>

<iframe width="560" height="315" src="https://www.youtube.com/embed/_CpQGv3jdL8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

</center>

### Technologie

<clear></clear>

<div style="width:50%; margin-right: 10px; float:left;">

<center>

<figure>

<img src="../../img/machineLeibniz.png">

<figcaption>

copie d'une Machine Arithmétique, $1694$, Leibniz

</figcaption>

</figure>

</center>

</div>

Cet appareil est inspiré de la *Pascaline* de *Blaise Pascal*, mais avec une nouvelle technologie originale, le **[cylindre canelé](https://fr.wikipedia.org/wiki/Cylindre_cannel%C3%A9_de_Leibniz)** du philosophe, mathématicien, diplomate, ... , allemand :de: **[Gottfried Wilhelm Leibniz](https://fr.wikipedia.org/wiki/Gottfried_Wilhelm_Leibniz)** ($1646-1716$) permettant de réaliser les $4$ opérations arithmétiques $+,-,\times, \div$. Ce procédé représente un progrès certain par rapport à la *Pascaline* : il sera mis en oeuvre dans toutes les machines à calculer mécaniques jusqu'aux années $1960$, avant l'apparition du calcul électronique.

<clear></clear>

### Aller Plus Loin

* Machine à Multiplier TIM de Leibniz : https://journals.openedition.org/bibnum/551
