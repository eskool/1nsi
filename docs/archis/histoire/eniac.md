# 1NSI : Histoire des Ordinateurs : L'ENIAC

## ENIAC :us: $1945$

### Introduction

<center>

<figure>

<iframe width="560" height="315" src="https://www.youtube.com/embed/4MmJJGjbeqw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<figcaption>

ENIAC becomes a reality $1946$ ($13$ min $10$), 
:warning: Vidéo en anglais :gb:

</figcaption>

</figure>

</center>

### Invention

<clear></clear>

<div style="width:40%; margin-right: 10px; float:left;">

<center>

<figure>

<img src="../../img/eniac.jpg">

<figcaption>

ENIAC, $1945$

</figcaption>

</figure>

<figure>

<img src="../../img/johnMauchly.jpg">

<figcaption>

John Mauchly

</figcaption>

</figure>

<figure>

<img src="../../img/presperEckert.jpg">

<figcaption>

J. Presper Eckert

</figcaption>

</figure>

</center>

</div>

L'<bred>ENIAC</bred> (**E**lectronic **N**umerical **I**ntegrator **A**nd **C**omputer) commence à être conçu à partir de $1943$, et construit à partir de mai $1944$, à la ***Moore School of Electrical Engineering*** de l'**Université de Pennsylvanie** :us:, par le physicien américain [John William Mauchly](https://fr.wikipedia.org/wiki/John_William_Mauchly) et un jeune ingénieur de $23$ ans, [John Presper Eckert ](https://fr.wikipedia.org/wiki/John_Eckert). **[John Von Neumann](https://fr.wikipedia.org/wiki/John_von_Neumann)** sera par la suite consultant de ce projet. Mais l'*ENIAC* n'est opérationnel qu'en $1945$, et dévoilé en grande pompe au public en $1946$ (avec ajout de lumières clignotantes totalement inutiles, pour mieux marquer les esprits du public, et donnant l'impression que l'ordinateur *réfléchit*). Il sera utilisé jusqu'en $1955$. Sa construction prendra environ $2$ ans à une équipe d'une douzaine d'ingénieurs.
Longtemps considéré comme **le premier ordinateur généraliste/universel entièrement électronique**, néanmoins :
* La programmation se fait par des recâblages de *switchs* :gb: (commutateurs :fr:) pour activer/désactiver les tubes à vide, et de rebranchements (*plugs*) d'un *panneau de connexion* :fr: ou *plugboard* :gb:
* l'*ENIAC* est précédé par le *Colossus* britannique (construit en $1943$, mais dévoilé au public en $1974$), qui lui est un peu comparable en ce qui concerne ses caractéristiques.

### Utilisation

Bien que jamais utilisé durant la guerre (opérationnel fin $1945$), l'*ENIAC* a initialement été conçu dans le but de diminuer drastiquement les délais de ***calculs balistiques*** (pour la création des *tables de lancement* des missiles balistiques) utilisés durant la 2ème guerre mondiale. Durant sa présentation au public en $1946$, on compara le temps de calcul des trajectoires d'un missile ballistique mettant $30$ secondes à atteindre sa cible, entre une *"Calculatrice Humaine"* et l'*ENIAC*, ce qui termina de convaincre le gouvernement des Etats-Unis (et les Colonels et Généraux présents lors de la Démo) de l'utilité de disposer d'ordinateurs électroniques : 

* Calculatrices Humaines : un calcul réalisé par une *Calculatrice humaine*, disposant de calculatrices mécaniques de bureau, prenait préalablement entre $12h$ et $24h$ ...
* ENIAC : ... mais ne prend plus que $20$ secondes

Il sera ensuite utilisé pour des problèmes de **physique nucléaire**, et de **météorologie**.

### Quelques Caractéristiques Techniques

<env>Technologie des tubes à vide</env>

La technologie des Tubes à vide était utilisée pour réaliser les **opérations arithmétiques**. Elle était déjà connue pour être peu fiable :
* Avant de lancer le projet, certains experts en électronique estimaient qu'une telle machine avec un tel nombre de tubes serait en panne si souvent (toutes les $5$ secondes) que la machine serait de facto inutilisable. Néanmoins, l'urgence de la demande de l'armée US était telle qu'ils acceptèrent de financer le projet (sous le nom de *Projet PX*)
* Prédiction seulement partiellement correcte : de nombreux tubes brûlaient chaque jour laissant l'*ENIAC* inopérant la moitié du temps. 
* En moyenne: $1$ à $2$ pannes par jour (réduction des pannes obtenue en ne coupant jamais la machine, en effet la plupart des problèmes liés aux tubes se produisent au démarrage ou à l'arrêt de la machine, car ils sont soumis à un important stress thermique)
* Plus longue utilisation sans panne : en $1954$, il tourne $116h$ sans aucune panne
* Pour la petite histoire : Une cause fréquente de panne était la combustion d'un insecte sur un tube chaud, provoquant un stress thermique local et la rupture de l'ampoule de verre. Le terme anglais désignant un insecte, ***bug*** :bug:, devient ainsi synonyme de ***dysfonctionnement informatique***.

<env>Quelques Caractéristiques Techniques</env>

* Coût : $500\, 000\$$ ($\approx 3\, 000\, 000 \$$ actuels)
* Dimensions: $2,4 × 0,9 × 30,5 m$, il occupe une surface de $167 m^2$, et/ou un volume $\approx 30 m^3$, autrement dit, **toute une Salle**
* Configuration : $40$ panneaux en forme de $U$
* Poids : $30$ t
* Technologie : $18\, 000$ Tubes à vide **pour réaliser les opérations arithmétiques**
* Mémoire : 
  * [Bascules](https://fr.wikipedia.org/wiki/Bascule_(circuit_logique)) :fr: / [Flip-Flops](https://en.wikipedia.org/wiki/Flip-flop_(electronics)) :gb:
  * table de fonctions
  * [panneau/tableau de connexion](https://fr.wikipedia.org/wiki/Tableau_de_connexion) (recâblages) :fr: / [plugboard](https://en.wikipedia.org/wiki/Plugboard) :gb:
  * cartes
* $3000$ switches à câbler ou pas, pour activer/désactiver les Tubes à l'arrière
* $1500$ relais, $70\,000$ résistances, $10\,000$ condensateurs
* De plus, l'*ENIAC* utilise des registres *décimaux* et non *binaires*
* Arithmétique: 
  * il manipule $20$ nombres à $10$ chiffres signés pouvant chacun réaliser $5000$ additions simples/seconde. 
  soit, au total : $\approx 100\,000$ additions simples/seconde. 
  * $357$ multiplications /seconde, et 
  * $38$ divisions/seconde
* Turing-complet
* Le programme n'est pas écrit en mémoire à proprement parler, mais l'*ENIAC* est RE-programmable (pour exécuter un nouveau programme)
* Vitesse : de $100$ à $1000$ fois plus rapide qu'une machine électromécanique

### Programmation

Le programme n'est pas écrit en mémoire à proprement parler, mais l'*ENIAC* est **reprogrammable** (pour exécuter un nouveau programme) et peut résoudre tous les problèmes calculatoires (Turing-complet) , mais avec des ***switchs*** (commutateurs) et des ***plugs*** (recâblage des connexions) :
  * en **recâblant** les câbles des ***switchs*** (commutateurs)
  * et reconnectant (***plugs***) les câbles du panneau de connexion,
  * table de fonctions

Après détermination de la nouvelle bonne configuration, la reprogrammation en elle-même pouvait prendre plusieurs jours.

Entre $1944$ et $1955$, six femmes, toutes mathématiciennes, ***Kathleen Antonelli***, ***Jean Bartik***, **Betty Holberton**, **Marlyn Meltzer**, ***Frances Spence*** et ***Ruth Teitelbaum*** sont les premières personnes à programmer l'*ENIAC*, pour un calcul balistique.

### Aller Plus Loin

* Un Livre : **"Eniac: The Triumphs and Tragedies of the World's First Computer"** par **Scott MacCartney**, Juin $1999$. En anglais :gb:

<clear></clear>
