
## Résumé

<center>

| Nom | Pays | Date<br/>Sortie | Système<br/> de nombres | Mécanique | Programmation | Turing-complet |
| :-: | :-: | :-: | :-: | :-: | :-: | :-: |
| machine de Babbage | Royaume-Uni | jamais finie..<br/>de<br/>$1834 \rightarrow 1871$ | Décimale<br/> à virgule<br/> fixe | mécanique<br/>**à vapeur** | Programmation <br/>à base de cartes perforés | Oui |
| Zuse $Z3$ | Allemagne | Mai $1941$ | Binaire<br/> à virgule<br/> flottante | Électromécanique | Programmation <br/>à base de rubans perforés | Oui |
|Atanasoff–Berry<br/> Computer | États-Unis | $1942$ | Binaire | Électronique | Non programmable | Non |
| Colossus Mark $1$ | Royaume-Uni | Février $1944$ | Binaire | Électronique | Programmation <br/>à base de câblage <br/>et de commutateurs | Non |
| Harvard Mark I<br/> – IBM ASCC | États-Unis | Mai $1944$ | Décimal | Électromécanique | Programmation<br/> à base de rubans perforés | Non |
| Colossus Mark $2$ | Royaume-Uni | Juin $1944$ | Binaire | Électronique | Programmation<br/> à base de câblages<br/> et de commutateurs | Non |
| Zuse $Z4$ | Allemagne | Mars $1945$ | Binaire<br/> à virgule<br/> flottante | Électromécanique | Programmation<br/> à base de rubans perforés | Oui |
| ENIAC | États-Unis | Juillet $1946$ | Décimal | Électronique | Programmation<br/> à base de câblages<br/> et de commutateurs | Oui |
| Small-Scale<br/> Experimental Machine<br/> (Baby) | Royaume-Uni | Juin 1948 | Binaire | Électronique | Programme stocké dans <br/>un tube de Williams | Oui |
| Modified ENIAC | États-Unis | Septembre $1948$ | Décimal | Électronique | Programmation<br/> à base de câblages<br/> et de commutateurs<br/> et d'un système <br/>primitif de ROM | Oui |
| EDSAC | Royaume-Uni | Mai $1949$ | Binaire | Électronique | Programme stocké dans<br/> une mémoire à ligne de délai | Oui |
| Manchester Mark I | Royaume-Uni | Octobre $1949$ | Binaire | Électronique | Programme stocké dans<br/> un tube de Williams et<br/> dans un tambour magnétique | Oui |
| CSIRAC | Australie | Novembre $1949$ | Binaire | Électronique | Programme stocké dans<br/> une mémoire à ligne de délai | Oui |

</center>

### Aller plus Loin :

* Computer Pioneers : Vidéos en anglais :gb:
  * Part 1 ($53$ min $25$) $1935-1945$ : https://www.youtube.com/watch?v=qundvme1Tik
  * Part 2 ($54$ min $10$) $1946-1950$ : https://www.youtube.com/watch?v=wsirYCAocZk