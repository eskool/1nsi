# 1NSI : Histoire des Ordinateurs : Le Transistor

## $1^{er}$ Transistor, Décembre $1947$

À la suite des travaux sur les semi-conducteurs, le <bred>Transistor</bred> a été inventé le $23$ décembre $1947$ par les Américains :us: **John Bardeen**, **William Shockley** et **Walter Brattain**, chercheurs des **Laboratoires Bell**. Ces chercheurs ont reçu pour cette invention le prix Nobel de physique en $1956$.

### Une invention "*Européenne*" ratée :cry:... : le transistron :eu:

**Herbert Mataré** et **Heinrich Welker** deux physiciens allemands :de: ont aussi développé parallèlement et indépendamment le « **transistor français** » en juin $1948$ alors qu’ils travaillaient à la **Compagnie des Freins et Signaux** à Paris :fr:. Ils déposent leur première demande de brevets pour un transistor le $13$ août $1948$. Les études menées par les commissaires montrent qu’ils ne se sont pas appuyés sur l’annonce du transistor du laboratoire américain mais qu’ils ont bien eu l’idée en même temps. Le $18$ mai $1949$, cette invention européenne est présentée par la presse au public sous le nom de <bred>« Transistron »</bred>. L’objectif est alors de conquérir le marché mondial en premier. A l’époque, la presse technique donne l’avantage au transistron considéré plus résistant et plus stable. Néanmoins le gouvernement français étant focalisé sur la technologie nucléaire, le transitron est mis à l’écart et perd son avantage face au transistor. En $1952$, *Herbert Mataré* crée l’entreprise **Intermetall** qui est la première à produire des transistors et qui atteindra son apogée un an plus tard avec la présentation de **la première radio  à transistor** un an avant celle de **Texas Instruments**. En $1954$, **Texas Instruments** met au point son prototype de poste radio à transistor qui sera industrialisé par la société **IDEA (Industrial Development Engineering Associates)**.



Avant cela, **Herbert Mataré** avait déjà approché l’effet transistor alors qu’il travaillait pour l’armée allemande durant la seconde guerre mondiale dans le but d’améliorer les radars. L’urgence de la guerre l’empêcha de se pencher davantage sur le sujet et il qualifia ce phénomène d’« **interférences** ». Lorsque la Russie reprit le village où il travaillait en Pologne, **Herbert Mataré** dut brûler toutes ces notes de peur qu’elles tombent entre les mains de l’ennemi.

### Un progrès face au tube électronique

Le transistor est considéré comme **un énorme progrès face au tube électronique** : beaucoup plus petit, plus léger et plus robuste, fonctionnant avec des tensions faibles, autorisant une alimentation par piles, il fonctionne presque instantanément une fois mis sous tension, contrairement aux tubes électroniques qui demandaient une dizaine de secondes de chauffage, généraient une consommation importante et nécessitaient une source de tension élevée (plusieurs centaines de volts).

### Premières applications industrielles : $1952$ radio, $1953$ ordinateur

Une fois le transistor découvert, l'ouverture au grand public ne fut pas immédiate. La première application du transistor fut pour la radio en $1952$, soit $5$ ans après la découverte du transistor. Mais à partir de ce moment son influence sur la société augmenta de façon exponentielle, en particulier chez les scientifiques et les industriels. En effet, **à partir du milieu des années $1950$ , on commence à utiliser le transistor dans les ordinateurs**, les rendant assez fiables et relativement petits pour leur commercialisation :

* $1953$ : $1^{er}$ Ordinateur à transistor de l'**Université de Manchester**, avec $92$ transistors, et $550$ diodes. Il s'agissait d'un prototype pour tester la possibilité d'utiliser des transistors afin d'améliorer le **Mark $I$** 
* $1954$ : Le **[TRADIC](https://fr.wikipedia.org/wiki/TRADIC)** - **<bred>TRA</bred>nsistor <bred>DI</bred>gital <bred>C</bred>omputer** des **Bell Labs**, pour l'**US Air Force**. Premier ordinateur à transistors ($700$ transistors, $10\,000$ diodes) généraliste des États-Unis
* $1955$ : $1^{er}$ Ordinateur à transistor de l'**Université de Manchester** (grande échelle)
* $1957$ : **IBM 608 transistor calculator** , présenté en octobre $1954$, annoncé en $1955$, commercialisé à partir de décembre $1957$

A partir de $1957$ **IBM** construisait tous les nouveaux ordinateurs avec des transistors au lieu des tubes à vide.

Après l'invention du **circuit intégré** en $1958$, groupant en un petit volume plusieurs transistors et composants, en $1969$ est inventé le microprocesseur, permettant à des milliers de transistors de fonctionner en harmonie sur un support, ce qui est encore une fois une révolution pour l’informatique moderne. L'**Intel** $4004$ sorti en mars $1971$ et commandité par **Busicom**, intègre $2 250$ transistors et exécute $90\,000$ opérations par seconde.

De nos jours, le transistor est omniprésent dans la plupart des appareils de notre quotidien. Le nombre de transistors dans un microprocesseur a considérablement augmenté pendant que sa taille diminuait, suivant en cela la **Loi de Moore**, avec par exemple **$18$ milliards de transistors** pour $398\,mm^2$ en $2018$. Il a contribué au développement d’une grande variété de domaines. Il est présent dans tout ce qui contient un tant soit peu d’électronique, de notre cafetière à nos voitures en passant par les feux de signalisation. Dès qu’il y a un choix plus complexe que ouvert/fermé dans un appareil électronique, un transistor entre en jeu.

### Aller Plus Loin (avec le Transistor)

* [Liste des Ordinateurs à Transistor](https://fr.wikipedia.org/wiki/Liste_des_ordinateurs_%C3%A0_transistors)