# 1NSI : Histoire des Ordinateurs : Le Harvard Mark $I$<br/>L'architecture de Harvard

## le Harvard Mark $I$, :us: $1944$

### Introduction

<div style="width:100%;">

<center>

<figure>

<iframe width="560" height="315" src="https://www.youtube.com/embed/4ObouwCHk8w" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<figcaption>

Harry R.Lewis nous parle du Harvard Mark $I$ ($2$min $27$)
:warning: Vidéo en anglais :gb:

</figcaption>

</figure>

</center>

</div>

### Invention

<clear></clear>

<div style="width:30%; margin-right: 10px; float:left;">

<center>

<figure>

<img src="../../img/howardAikenJeune.jpg">

<figcaption>Howard Aiken, Jeune</figcaption>

</figure>

<figure>

<img src="../../img/howardAikenVieux.jpg">

<figcaption>Howard Aiken, Moins Jeune..</figcaption>

</figure>

</center>

</div>

Le **Mark $I$** a été **le premier grand calculateur électromécanique numérique** construit aux États-Unis :us:. Il est considéré comme étant l'un des premiers calculateurs universels. 
Dès $1937$, **[Howard Aiken](https://fr.wikipedia.org/wiki/Howard_Aiken)** ($1900-1973$) :us: avait réalisé que la machine analytique de *Babbage* était le type de machine à calculer qu'il voulait développer : il proposa à *IBM* de la créer et de la construire. Après une étude de faisabilité, *Thomas J. Watson* accepta de la construire en $1939$ : elle fut testée en $1943$ dans les locaux d'*IBM* et fut donnée et déménagée à l'**Université Harvard** en Février $1944$, changeant son nom initial d'**IBM ASCC** (**A**utomatic **S**equence **C**ontrolled **C**alculator) en **Harvard Mark $I$** ou **Mark $I$**. 

Dès $1943$, *Aiken* a formé une petite équipe de "*codeurs*" pour tester puis programmer la machine; rapidement s'y joignit une jeune enseignante de mathématiques recrutée par l'*US Navy*, **[Grace Hopper](https://fr.wikipedia.org/wiki/Grace_Hopper)**, considérée comme ***l'une des premières programmeuses en informatique***. 

### Quelques Caractéristiques Techniques

<div style="overflow: hidden;">

* Dimensions : $L \times H \times P = 16m \times 2,4m \times 0,5m\approx 19,2m^3$
* Poids : $4500 kg$
* Le *Mark I* lisait ses instructions sur des *bandes de cartes perforées* et exécutait l’instruction courante puis lisait la suivante
* Une *seconde bande* pouvait être installée pour fournir des données d'entrée. Les formats de bandes étaient les mêmes, mais les instructions à exécuter ne pouvaient pas venir de la bande de données d'entrée.
* $72$ nombres de $23$ chiffres décimaux chacun
* programmable, mais pas d'instruction de branchement conditionnel
* Arithmétique :
  * $3$ Additions/seconde 
  * une Multiplication prenait $6$ secondes
  * une Division prenait $15$ secondes
* Consommation : $4kW$

</div>

### Architecture de Harvard

<def> 

:warning: Cette dissociation entre **données** et **instructions** est la caractéristique principale de l'architecture matérielle dite <bred>Architecture de Harvard</bred> des ordinateurs.

</def>

Cette architecture est essentiellement celle des calculateurs mécaniques à cartes perforées, comme celles qu'*IBM* produisait depuis le début du $XX$e siècle. Mais, avec cette technologie *classique*, *Aiken* a conçu une machine beaucoup plus puissante et plus automatisée que les machines de série. 

### Aller Plus Loin

* Biographie d'Howard Aiken, en anglais :gb: : https://history-computer.com/howard-aiken-biography-history-and-inventions/