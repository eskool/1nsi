# 1NSI : Histoire des Ordinateurs : Abaques & Bouliers

!!! col __40 center
    ![table Salamine](../img/tableSalamine.jpg)  
    abaque à jetons Grec :
    Table (de Comptage) *de Salamine*,
    $V$è Siècle av JC,
    en marbre blanc, $1,5m\times 0,75m$,
    Des jetons ou des cailloux étaient disposés sur cet abaque afin de procéder aux calculs.

    ![abaque Nepohualtzitzin](../img/abaqueNepohualtzitzin.jpg)  
    abaque bracelet Mésoaméricain
    (**Nepohualtzintzin**) Olmèque/Maya puis Aztèque
    en Jade, Or et Coquilles
    **L'ordinateur portable mécanique manuel !**

    ![abaque Romain](../img/abaqueRomain.png)  
    abaque portatif Romain, en Ivoire,
    entre $V \rightarrow II$ème Siècle av JC

    ![abaque Chinois](../img/abaque.png)  
    abaque boulier Chinois :cn:
    (**Suanpan**, ch. trad. 算盤, ch.simp. 算盘), $XI$ème Siècle

    ![abaque Japonais](../img/abaqueSoroban.jpg)  
    un abaque boulier Japonais (**Soroban**),
    c. $XV$è Siècle ap JC

    ![abaque Stchoty](../img/abaqueStchoty.jpg)  
    abaque boulier Russe (**Stchoty**, Счёты), 
    $XVII$è Siècle

    ![abaque Indien](../img/abaqueIndien.jpg)  
    abaque Indien en Ivoire
    "moderne", du $XVIII$è Siècle

<!-- <div style="overflow: hidden;"> -->

!!! col __60
    <br/>
    ## Pierres et Bâtons

    Outre le fait de **compter sur ses doigts**, le plus ancien outil de calcul est probablement le **[bâton de comptage](https://fr.wikipedia.org/wiki/B%C3%A2ton_de_comptage)**, qui consiste à enregistrer des **[marques de dénombrement](https://fr.wikipedia.org/wiki/Marques_de_d%C3%A9nombrement)** (souvent des *entailles*) sur un bâton (bâton en bois, os, ...), dont l'usage remonte à la **préhistoire**.
    Dans l’**histoire de la numération**, qui précède l'histoire de l'écriture, l’écriture des nombres ne facilitait pas, en général, les calculs. 
    
    ## Origines du mot

    Les *géomètres* et les *comptables* ont donc eu besoin de véritables **instruments les aidant à calculer** :
    Un **[abaque](https://fr.wikipedia.org/wiki/Abaque_(calcul))** (très probablement de l'hébreu abaq=**אבק** signifiant « poussière » ou « sable ») est le nom donné à tout **instrument mécanique manuel**, facilitant le **calcul** : les $4$ opérations arithmétiques de base ($+-\times \div$), mais aussi la **racine carrée**, etc.. **Les abaques ont été le principal instrument de calcul de l’humanité pendant plus de $2000$ ans.** : On peut donc voir un abaque comme une forme très rudimentaire **d'ordinateur mécanique manuel**. 

    En pratique, et historiquement, les **abaques** ont pris plusieurs formes un peu différentes :

    ## Abaques de poussière
    
    Ce sont des **tablettes recouvertes par du sable/poussière** utilisées pour écrire des symboles avec un stylet (et/ou les doigts?), et pouvoir les effacer simplement
    
    ## Abaques *à jetons* 
    
    Les **abaques à jeton**, dont le principe est de poser des ***jetons*** (**cailloux**, **coquillages**, **jetons** fabriqués, etc..) sur une « **table de comptage** » ou « **table à calculer** », marquée de **lignes** et/ou de **colonnes**. Les *jetons* prenant des valeurs différentes selon leur position (dans les colonnes).
    
    ## Abaques Bouliers
    
    Les **abaques *bouliers***, appelés simplement **bouliers**, qui sont des types spécifiques d'abaque matérialisés par des **tiges parallèles** supportant des **boules** (coquillages, cailloux..),... Les premiers *bouliers* sont inventés avant $2000$ av. J.-C.

    Les études des *mathématiciens* et *historiens* montrent que plusieurs civilisations très **séparées géographiquement** et **temporellement** semblent les avoir **inventés indépendamment**, et utilisés comme aide au calcul depuis plusieurs millénaires :

    ### les Égyptiens
    
    Les Égyptiens (c. $3150 \rightarrow 30$ av JC) créent des **tables** pour les calculs usuels, pour ne pas devoir les recalculer... L'utilisation d'abaques chez les égyptiens est attesté par l'Historien grec **Hérodote** (c. $IV$è Siècle av JC, *papyrus Oxyrhynchus 2099*) qui atteste que les égyptiens "comptaient en manipulant des cailloux de droite à gauche, à l'opposé de la méthode grecque de gauche à droite". Les archéologues ont trouvé d'anciens ***disques*** de diverses tailles que l'on pense avoir servi de compteurs, mais pas de fresque permettant de confirmer leur utilité. On en sait peu de choses, mais les abaques égyptiens sont probablement des **abaques *de poussière***, ou des **abaques à jetons (tables de compte)**.
    
    ### Les Sumériens
    
    Les **Sumériens** (c. $4500 \rightarrow 1900$ av JC) et les **Babyloniens** ($1800$ av JC $\rightarrow 100$ ap JC), tous deux situés dans l'actuel Irak du Sud, Delta du Tigre et de l'Euphrate, créent aussi des **tables** pour les calculs usuels. La période c. $2700 \rightarrow 2300$ av JC voit l'apparition des premiers **abaques à jetons (*tables de compte*)** chez les *Sumériens* avec des colonnes successives représentant les nombres de le *système sexagésimal*.
    
    ### Les Olmèques / Mayas
    
    Les **Olmèques** (c. $1200 \rightarrow 400$ av JC, Golfe du Mexique), & les **Mayas** ($1800$ av JC $\rightarrow 1697$, Sud Mexique / Guatemala / Honduras/El Salvador) : L'invention de l'abaque **boulier *Nepohualtzintzin*** est attribuée aux *Mayas*, bien que l'on ait retrouvé de très vieux instruments appartenant aux *Olmèques*. Il était utilisé pour les $4$ opérations élémentaires. Plus tardivement, les **Incas** (c. $XIII \rightarrow XVI$è Siècle ap JC), dont l'empire s'étalait sur les actuelles Colombie/Argentine/Chili/Équateur/Pérou/Bolivie, utilisent des cordelettes nouées appelées [Quipus](https://fr.wikipedia.org/wiki/Quipu) pour réaliser au minimum des *additions/soustractions*.
    
    ### Les Grecs
    
    Les **Grecs** ($800 \rightarrow 31$ av JC) utilisaient des **tables** (pour $+,-,\times$) apprises par coeur comme de nos jours, attesté par *[Aristote](https://fr.wikipedia.org/wiki/Aristote)* ($IV$è Siècle av JC) - notamment une ***table de Pythagore*** pour la ***multiplication***-, ainsi que des **abaques à jetons *tables de compte*** avec des cailloux, attesté par [Aristophane](https://fr.wikipedia.org/wiki/Aristophane) ($V$è Sicèle av JC), pour les opérations plus compliquées.
    
    ### Les Étrusques

    Les **Étrusques** (c. $900 \rightarrow 100$ av JC, Italie, région Toscane) et les **Romains**, utilisaient des **abaques à jetons (*tables de compte*)*** avec des **cailloux** (du lat. ***calculi***, qui donnera naissance au mot français **calcul**), et aussi un **abaque *boulier*** portable en base décimale, dès le $II$è Siècle av JC. **Le tout premier boulier sur ce continent ?**.
    
    ### Les Indiens (d'Inde)
    
    Les **Indiens** (d'Inde). Des sources du $I$er siècle de notre ère, telles que l'**[Abhidharmakosa](https://fr.wikipedia.org/wiki/Abhidharmakosha)** et le **[Divyavadana](https://fr.wikipedia.org/wiki/Divyavadana)**, décrivent la connaissance et l'utilisation de l'abaque indien (***Ganitra***). Vers le $V$è siècle, les clercs indiens trouvaient déjà de nouvelles façons d'enregistrer le contenu de l'abaque. Les textes hindous utilisaient le terme ***shunya*** (signifie ***zéro***) pour indiquer la colonne vide sur le boulier
    
    ### Les Chinois
    
    Les **Chinois** utilisent, dès le $III$è Siècle av JC, des **[baguettes à calculer](https://fr.wikipedia.org/wiki/Baguettes_%C3%A0_calculer)** ($\approx 10cm$ de long) pour représenter les nombres en notation décimale positionnelle, et réaliser les $4$ opérations arithmétiques élémentaires et calculer des racines carrées. L'histoire de l'abaque chinois (**Suanpan**) remonte à la période des Printemps et Automnes ($770 - 476$ av JC) . Dans le célèbre long rouleau "*[le long de la rivière pendant le festival Qingming](https://upload.wikimedia.org/wikipedia/commons/8/86/Alongtheriver_QingMing.jpg)*" peint par *[Zhang Zeduan](https://fr.wikipedia.org/wiki/Zhang_Zeduan)* ($1085-1145$) au cours de la **dynastie des Song** ($960-1279$), on voit une colonne de $15$ suanpans, dans leur forme de boulier définitive, à côté d'un livre de compte et les prescriptions du médecin sur le comptoir d'un apothicaire. Plus tardivement, au $XIV$è Siècle, le boulier arrivera au japon, et donnera naissance au **[boulier Japonais (Soroban)](https://en.wikipedia.org/wiki/Soroban)**.

    ### Léonard de Pise / Fibonacci

    En $1202$, **Fibonacci** publia son célèbre **Liber abaci** ou *Livre de l’abaque*, qui fît connaître à toute l’Europe des chiffres (dits *arabes*) nouveaux à l'époque, la numération décimale positionnelle, et l'algorithme de multiplication utilisé de nos jours. 

<clear></clear>

## Aller Plus Loin

* Perspectives Historiques sur les Abaques et Bouliers, *Dominique Tournès*, $2016$, Labo Info Maths+IREM : http://revue.sesamath.net/spip.php?article891
* History of Abacus :gb: : https://kartsci.org/kocomu/computer-history/history-abacus-ancient-computing/
* History of Abacus : https://www.ee.ryerson.ca/~elf/abacus/history.html
* Ábacos de América Prehispánica, en espagnol :es: : http://dialnet.unirioja.es/descarga/articulo/3894654.pdf
* nepoualtzitzin : La sabiduría de contar en el mundo mesoamericano, en espagnol :es:, María Elena Romero Murguía, $2016$ : https://books.google.fr/books?id=yxVsCwAAQBAJ&printsec=frontcover&hl=fr&source=gbs_ge_summary_r&cad=0#v=onepage&q&f=false
* Quipus Incas (article $2020$) : https://www.nationalgeographic.fr/histoire/2020/03/les-quipus-le-code-secret-des-incas
* Table de comptage Indienne : https://www.roots.gov.sg/Collection-Landing/listing/1316948
* The use of the Abacus in India : https://www.jstor.org/stable/25189882 
(il faut créer un compte gratuit..)
* Monde du Boulier : https://monde-du-boulier.com/blogs/news/histoire-boulier
* Fabrication et Étude des instruments à Calculer : http://www.circ-ien-andolsheim.ac-strasbourg.fr/IMG/pdf/etude_instrument_cle48daff.pdf

<clear></clear>

