# 1NSI : Histoire des Ordinateurs : L'Atanosoff-Berry Computer (ABC)

## L'Atanosoff-Berry Computer (ABC), $1942$ : le grand oublié

<clear></clear>

<div style="width:40%; margin-right: 10px; float:left;">

<center>

<figure>

<img src="../../img/atanasoffBerry.jpg">

<figcaption>Machine <br/><em>Atanasoff-Berry</em></figcaption>

</figure>

</center>

</div>

Conçu en $1937$ par **John Vincent Atanasoff** (physicien, mathématicien ingénieur américain d'origine Hongroise) enseignant à l' **Iowa State College** et par son étudiant **Clifford Berry**, mais testé avec succès seulement en $1942$.
Bien que son utilisation n'ait qu'une portée limitée (il a été conçu uniquement pour résoudre des systèmes d'équations linéaires), il fut reconnu après un procès ($1973$) comme **le premier ordinateur numérique électronique** (ce que prétendait être l'**ENIAC**).

<env>Quelques caractéristiques techniques</env>

Cette machine a été la première à implanter trois concepts fondamentaux :

* utilisation du **binaire** pour représenter tous les nombres et les données ;
* **calculs** réalisés par l'**électronique** plutôt que des éléments mécaniques ;
* une organisation séparée entre la **mémoire** et l'**unité de calcul**.

Néanmoins, les éléments suivants le distinguent encore d'un ordinateur moderne :
* résultats intermédiaires de calculs stockés sur des cartes à lire/écrire, et n'a pas été amélioré
* utilisation unique (résolution d'équations linéaires), programme non modifiable ni stockable sur une mémoire/suppport

<clear></clear>