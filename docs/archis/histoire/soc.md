# 1NSI / TNSI : Soc - System On Chip - Système sur Puce

<center>

| Contenus | Capacités Attendues | Commentaires |
|:-:|:-:|:-:|
| Composants intégrés d’un Système<br/> sur Puce (SoC). | Identifier les principaux<br/>composants sur un schéma de<br/>circuit et les avantages de leur<br/>intégration en termes de vitesse et<br/>de consommation | Le circuit d’un téléphone peut<br/>être pris comme un exemple :<br/>microprocesseurs, mémoires<br/>locales, interfaces radio et<br/>filaires, gestion d’énergie,<br/>contrôleurs vidéo,<br/>accélérateur graphique,<br/>réseaux sur puce, etc. |

</center>

## Introduction

* Dans les cartes mères modernes, en particulier celle des ordinateurs portables, l'intégration est beaucoup plus poussée.  
* Dans les dispositifs nomades comme les téléphones portables (Smartphones), Tablettes ou nano ordinateurs (par ex. Raspberry Pi ou l'Arduino), cette intégration passe encore à un niveau supérieur.

<center>

<iframe width="560" height="315" src="https://www.youtube.com/embed/L4XemL7t6hg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

</center>

## Repères Historiques



## SoC - System on Chip / Système sur Puce

### Définition

!!! def "Soc - System on Chip"
    Un <red>SoC - System on Chip</red> :gb: / <red>Système sur Puce</red> :fr: est un **circuit intégré** qui accueille sur une même puce, dans une taille très réduite, de très nombreux circuits, parmi lequels :

    * un microprocesseur **CPU - Central Processing Unit**
    * de la mémoire vive **RAM - Random Access Memory** :
        * La mémoire **LPDDR - Low Power Double Data Rate** (Vitesse de données double à faible consommation) / **LPDDR SDRAM** / **DDR Mobile** / **mDDR**
    * de la mémoire statique **ROM - Read Only Memory, Flash, EPROM,..** :
        * l'**UFS- Universal Flash Storage** qui est un standard de mémoire Flash ... 
    * un circuit/chipset Graphique **GPU - Graphical Processing Unit**
    * un circuit/chipset Neuronal **NPU - Neural Processing Unit** : pour l'accélération des algorithmes de Machine Learning : 
        * Traduction Automatique
        * Reconnaissance Faciale
        * Diagnostics Médicaux
        * Détection de Fraudes
        * Voitures Autonomes
        * Prédiction de Traffic, 
        * Recommendation de Produits, etc..
    * une puce d'Image ou **ISP - Image Signal Processor** pour la **Photo** : une puce prenant en charge la création d’images numériques. Dans la réalité et de par leurs tailles minuscules, les capteurs photo de nos smartphones sont mauvais. La qualité qu’il est actuellement possible d’obtenir est intimement liée à cette puce. En effet, c’est grâce à elle que votre smartphone va traiter la prise et la création de votre photo.
    * une puce **DSP - Digital Signal Processing** pour le **Multimedia** : c'est une puce pour le traitement du signal numérique, par exemple:
        * Traitement Audio
        * Traitement Vidéo
        * (quelques) Traitements de l'Affichage
    * un **modem** 
    * des **puces de Connectivité** : 
        * Wifi
        * Bluetooth
        * 2G/3G/4G
        * Radio FM
        * LoRa[^1] :fr: $\geq 2009$ (échange de paquets pour objets connectés)
        * etc..  
    * une puce de Sécurité **SPU - Secure Processing Unit** : le bouclier de votre smartphone. Son alimentation électrique est indépendante afin de ne pas pouvoir être éteint en cas d’attaque sur celui-ci. Le SPU est d’une importance capitale, celui-ci va stocker vos données biométriques, bancaires, votre SIM ou encore vos titres de transport. C’est lui qui contient les clés de chiffrement de vos données.)
    * etc..

![Die Shot Exynos 9820, Samsung S10](../img/die-shot-exynos9820.png){.center}
<center>
Die Shot Exynos 9820, Samsung S10 :sk-copyright: [ChipRebel.com](https://www.chiprebel.com/downloads/samsung-exynos-9820/)
</center>

### Utilisations des SoC

Cette miniaturisation est idéale pour des dispositifs mobiles comme :

* les **Smartphones** (téléphones portables), 
* les **Tablettes**, 
* les **Nano-ordinateurs** : Raspberry Pi, Arduino, etc..
* les **Consoles de Jeu** : Nintendo Switch (Nvidia Tigra X1), PS5 (AMD),..
* les **Objets Connectés** / **IoT - Internet of Objects**, 
* les **Systèmes Embarqués / eMbedded Systems**, au sens large
* à noter aussi que de grands constructeurs comme Apple, veulent aussi en équiper toute leur gamme d'ordinateurs.

### Comment est-ce possible ?

Une première question que l'on peut se poser est: Comment est-ce possible de pouvoir placer autant de fonctionnalités sur si peu de place? (en particulier, pourquoi ne fait-on pas la même chose pour les ordinateurs classiques?)

* Les processeurs des SoC ne sont pas aussi puissants que ceux des PCs/Ordinateurs portables (donc ils prennent moins de place)
* Les SoC sont construits (principalement) à base d'Architectures **ARM - Advanced RISC Machine**, qui sont suffisamment puissantes, mais sans trop consommer d'énergie.
* Les OS des smartphones sont **optimisés** pour les SoCs

### Avantages des SoC

Les avantages des SoC sont :

* La **Taille** (miniaturisation) : tous les composants sont sur la même puce, donc un gain de place important
* La **Vitesse** : 
    * les distances sont réduites, donc la fréquence d'horloge peut augmenter notablement
    * Les composants et leur connexion sont plus facilement prévisibles, donc moins de variations
* **Coût** :
    * Un circuit imprimé moins complexe
    * Moins de soudage
    * Fréquence d'horloge faible sur la carte, mais importante sur puce, cela réduit les frais liés au circuit imprimé
* **Énergie** : **Consommation énergétique** très réduite (par rapport à un système classique, à puissance de calcul équivalente), en raison des distances réduites (moins de déperdition) et
* **Absence de Système de Refroidissement** lourds, volumineux et énergivores (comme par ex. des ventilateurs)

### Désavantages des SoC

Des inconvénients des SoC sont:

* si le composant souffre d'une défaillance quelconque, la seule issue sera le remplacement complet du SoC.
* Pas d'extensibilité matérielle : tous les composants sont intégrés

Les meilleurs SoC, plus fins que les microprocesseurs *classiques* de dernière génération, sont gravés en $5$ nm et comprennent des milliards de transistors.

Les SoC de dernière génération intègrent aussi des composants spécialisés:

* reconnaissance faciale
* réalité augmentée
* etc..

## Architecture d'un SoC

### Schéma de Circuit d'un SoC

![Schéma de Circuit SoC](../img/circuit-soc.svg){.center}

### Types d'Architectures de SoC : ARM vs x86

Une Architecture de SoC est avant tout basée sur l'architecture de son processeur (CPU), qui est elle-même caractérisée par son **jeu d'instruction** (du processeur). Il existe différents types d'architectures pour les SoC, principalement deux :

* <rb>Architecture ARM</rb> : Jeux d'instructions ARM. Plus simple, basée sur le modèle d'architecture de processeur appelé **RISC - Reduced Instruction Set Computer** :gb: / **Processeur à Jeu d'Instruction Réduit** :fr:
* <rb>Architecture x86</rb> : Jeux d'instructions x86. Plus Complexe, basée sur le modèle d'architecture de processeur appelé **CISC - Complex Instruction Set Computer** / **Processeur à Jeu d'Instruction Complexe**.

### Les Architectures ARM

L'Architecture ARM est fortement inspirée du modèle d'architecture de processeur appelé **RISC - Reduced Instruction Set Computer** :gb: / **Processeur à Jeu d'Instruction Réduit** :fr:. Elle dispose de $16$ registres généraux de $32$ bits. Les instructions, codées sur $32$ bits jusqu'à l'ARMv$7$ peuvent toutes disposer d'exécution conditionnelle. Sur l'architecture $64$ bits (ARMv$8$), quelques instructions seulement peuvent disposer d'exécution conditionnelle. Le jeu d'instructions a reçu des extensions au fil du temps.

#### La Société ARM Ltd et les fabricants de processeurs ARM

La propriété intellectuelle appartient à une société britannique, historiquement **Acorn Computers**, de nos jours **ARM Ltd** :gb:. En $2016$,  ARM est rachetée par la Holding japonaise **Softbank**, et en $2020$, une OPA - Offre Publique d'Achat par **NVidia** :us: sera finalement rejetée en $2021$ par la **FTC - Federal Trade Commision** :us: (les autorités de la concurrence américaine). Les processeurs ARM sont fabriqués *sous licence*, suivant le principe ditdes **IPs - Intellectual Properties** :gb: / **Propriétés Intelectuelles** :fr:, qui sont des sortes de **Bibliothèques Matérielles** que l'on peut acheter pour monter soi-même (puis commercialiser). Les processeurs ARM sont ainsi construits par différentes entreprises de par le monde (cf [liste complète sur wikipedia](https://fr.wikipedia.org/wiki/Architecture_ARM#Fabricants_de_processeurs_ARM)[^13])). Parmi les entreprises fabriquant les modèles des **Séries Cortex** (les plus avancées), la majorité se trouve en Asie (20), suivie par les États-Unis (13) et enfin par l'Europe (6). 

#### Principaux Concepteurs de SoC

* AMD : Architecture ARM
* Apple : Architecture ARM (exclusivement)
* Broadcom : Architecture ARM
* <rb>Intel : Architecture x86</rb>
* [Mediatek](https://fr.wikipedia.org/wiki/MediaTek)[^3] : Architecture ARM
* Nvidia : Architecture ARM
* [Qualcomm](https://fr.wikipedia.org/wiki/Qualcomm)[^4] [^5] : Architecture ARM
* Samsung : Architecture ARM
* Texas Instrument : Architecture ARM
* etc..[^13]

#### Parts de Marché des SoC

```mermaid
pie title Parts de Marché des SoCs (2021)
    "MediaTek" : 43
    "Qualcomm" : 28
    "Apple" : 14
    "Samsung Exynos" : 7
    "Autres" : 8
```

#### Répartition des Coûts d'une SoC

```mermaid
pie title Répartition des Coûts d'un Samsung Galaxy S9+
    "Processeur Base/Applications" :  68
    "Batterie" : 5.5
    "Caméra/Image" : 48
    "Connectivité" : 12
    "Écran Tactile" : 72.5
    "Mémoire Von Volatile" : 12
    "Mémoire Volatile" : 39
    "Non Électroniques" : 29
    "Autres" : 15
    "Alimentation / Audio" : 8.5
    "Composants Radio Fréquence" : 23.5
    "Capteurs" : 5
    "Substrats" : 19.5
    "Assemblage & Tests" : 12.5
```

<center>

(source[^10])

</center>

#### Quelques Exemples de processeurs ARM

##### par Architectures / Jeux d'Instruction

| Date | Architecture | Famille(s) |
|:-:|:-:|:-:|
| $1985$ | ARMv1 | ARM1 |
| $1987$ | ARMv2 | ARM2, ARM3 |
|  | ARMv3 | ARM6, ARM7 |
|  | ARMv4 | StrongARM, ARM7TDMI, ARM8, ARM9TDMI |
|  | ARMv5 | ARM7EJ, ARM9E, ARM10E, XScale, FA626TE, Feroceon, PJ1/Mohawk |
| $1990$ | ARMv6 | ARM11 (en) |
|  | ARMv6-M | ARM Cortex-M (ARM Cortex-M0, ARM Cortex-M0+, ARM Cortex-M1)
|  | ARMv7-A | ARM Cortex-A (Gen1 : ARM Cortex-A8, Gen2 : ARM Cortex-A9 MPCore,<br/> ARM Cortex-A5 MPCore, Gen3 : ARM Cortex-A7 MPCore, <br/>ARM Cortex-A12 MPCore, ARM Cortex-A15 MPCore, <br/>Adaptation tierce : Scorpion, Krait, PJ4/Sheeva, Swift |
| | ARMv7-M | ARM Cortex-M (ARM Cortex-M3, ARM Cortex-M4, ARM Cortex-M7) |
| | ARMv7-R | ARM Cortex-R (ARM Cortex-R4, ARM Cortex-R5, ARM Cortex-R7) |
| | ARMv8-A | ARM Cortex-A35, ARM Cortex-A50 (ARM Cortex-A53, ARM Cortex-A57),<br/>ARM Cortex-A72, ARM Cortex-A73, X-Gene, Denver, Cyclone, Exynos M1/M2 |
| | ARMv8.2-A | ARM Cortex-A55, ARM Cortex-A65, ARM Cortex-A75, ARM Cortex-A76 |
| | ARMv8.3-A | ARM Cortex-A65AE (seulement LDAPR, le reste en 8.2),<br/>ARM Cortex-A76AE (idem A65AE) |
| | ARMv8-M | ARM Cortex-M23, ARM Cortex-M33 |
| | ARMv8-R | ARM Cortex-R53 |

##### par Familles de Processeur

* Famille Cortex-A : processeur <rb>A</rb>pplicatif
* Famille Cortex-R : processeur Temps-<rb>R</rb>éel (Real Time)
* Famille Cortex-M : processeur e<rb>M</rb>barqué (eMbedded System)

## Le Cas d'un Smartphone Samsung

### Soc d'un Smartphone

![Annotated Die Shot d'un Exynos 9810, Galaxy Note 9](../img/exynos9810.png){.center}

<center>

Annotated Die Shot d'un Exynos 9810, Galaxy Note 9 (2019)
</center>

![Annotated Die Shot d'un Exynos 9820, Galaxy Note 9](../img/exynos9820.png){.center}

<center>

Annotated Die Shot d'un Exynos 9820, Samsung Galaxy S10 (2020)

</center>

### Carte Mère d'un Smartphone

![Carte Mère S10 Recto](../img/carte-mere-s10-recto.png){.center}
<center>
Carte Mère d'un Galaxy S10 (2020), Recto
</center>

![Carte Mère S10 Verso](../img/carte-mere-s10-verso.png){.center}
<center>
Carte Mère d'un Galaxy S10 (2020), Verso
</center>


![Carte Mère S9 Recto](../img/carte-mere-s9-recto.png){.center}

<center>
Carte Mère d'un Galaxy Note 9, Recto
</center>

* Samsung 82LBXS2 NFC Controler & Samsung Secure Element
* Broadcom BCM43570 : Wifi Bluetooth Module
* Heart Rate Sensor : Capteur Cardiaque
* Maxim MAX98512 Audio Amplifier : Amplificateur Audio
* Samsung S2MPB02 Camera PMIC : - Power Management Integrated Circuit - Gestion Alimentation Caméra
* Shannon 560 PMIC - Power Management Integrated Circuit : Gestion d'Alimentation
* Cirrus Logic CS47L93 Audio Codec
* Samsung S2DOS05 Display Power Management Integrated Circuit : Gestion Alimentation Écran
* Shannon 965 RF Transceiver : Radio Émetteur/Récepteur fontionne avec le modem LTE inclus dans l'Exynos 9810, pour implémenter des débits

![Carte Mère S9 Recto](../img/carte-mere-s9-verso.png){.center}
<center>
Carte Mère d'un Galaxy Note 9, Verso
</center>

* Maxim MAX98512 Audio Amplifier
* Avago AFEM-9090 Front End Module : PAM - Power Amplifier Module pour QuadBand GSM/GPRS/EDGE
* Skyworks SKY77365-11 Power Amplifier Module
* Murata fL05B Power Amplifier Module
* Shannon 735 Envelope Tracker : Amplificateur pour Radio Frequence (RF)
* SoC Samsung Exynos 9810 + Mémoire Samsung 6 GB LPDDR4X (PoP)
* Samsung KLUCG2K1EA-B0C1 NAND Flash : Mémoire Flash de type UFS - Universal Flash Storage
* Maxim MAX77705F PMIC
* Broadcom BCM47752 GNSS Receiver (Global Navigation Satellite System), càd le GPS
* IDT P9320S Wireless Charging Receiver


## Le cas d'un Raspberry Pi 4


##### ARM Broadcom & Raspberry Pi 4

Le Raspberry Pi $4$ intègre ainsi sur quelques centimètres carrés, une carte mère complète autour d'un processeur ARM Quad Core Cortex A-$72$ (ARMv$8$) à $1,5$ GHz  

![Rasberry Pi 4](../img/raspberry-pi4.svg){.center}
![Rasberry Pi 4 Détaillé](../img/raspberry-pi4-detaille.jpg){.center}

<center>
Raspberry Pi $4$
</center>

!!! col __30 right
    ![Exynos](../img/exynos.jpg)
    ![A15](../img/A15.jpg)

* Les architectures ARM, introduites à partir de $1990$ par Acorn Computers, sont développés par l'entreprise anglaise ARM Mtd, et elles sont de type RISC $32$ bits (ARMv$1$ à ARMv$7$) et $64$ bits (ARMv$8$). Elles deviennent très populaires avec l'avènement de la téléphonie mobile grand public à la fin des années $1990's$.
* Les SoC bénéficient d'une faible consommation électrique et sont fabriquées par un grand nombre de constructeurs pour équiper de très nombreux matériels dont notamment :

##### La série Broadcom pour les Raspberry Pi 4

##### La série **Snapdragon** 

(Amérique du Nord, Chine, Asie) de **Qualcomm**, commercialisés dans les Samsung

##### La série **Samsung Exynos** 

(Europe & reste du monde) de **Samsung**  
<center>
<iframe width="560" height="315" src="https://www.youtube.com/embed/lne_pNe85xk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</center>
![Exynos vs Snapdragon](../img/exynos-vs-snapdragon.png){.center}  
<center>
Exynos vs Snapdragon, 2021, Galaxy S20
</center>

##### La série A12, A13, A14, A15 Bionic d'Apple

### Architectures x86

blabla

## Applications Android/iOS

### Android

* CPU-Z
* CPU-Z Hardware Information / CPU-Z Information matérielle

### iOS

* CPU DasherX - CPU Z
* System Status - Battery & Network Manager



## Références & Notes

### Références

* [ARM Developper Documentation](https://developer.arm.com/documentation)
* Guillaume Connan, Laurent Signac, Prépabac TNSI, Hatier
* [RaspBerry Pi 4, Phonandroid](https://www.phonandroid.com/raspberry-pi-4-officiel-4-go-de-ram-4k-et-un-cpu-plus-puissant-pour-38-e.html)
* TechInsights
* ChipRebel
* AnandTech

### Notes

[^1]: [LoRa ](https://www.journaldunet.fr/web-tech/dictionnaire-de-l-iot/1197635-lora-comment-fonctionne-le-reseau-quelles-differences-avec-sigfox-20210305/)
[^2]: [SoC, Pages Perso, Vahid MEGHDADI](http://www.unilim.fr/pages_perso/vahid/SoC/CoursSystemOnChip.pdf)
[^3]: [MediaTek, page wikipedia](https://fr.wikipedia.org/wiki/MediaTek)
[^4]: [Qualcomm, page wikipedia](https://fr.wikipedia.org/wiki/Qualcomm)
[^5]: [Série Snapdragon de Qualcomm, page wikipedia](https://fr.wikipedia.org/wiki/Liste_des_microprocesseurs_Qualcomm_Snapdragon)
[^6]: [MediaTek dépasse Qualcomm et deveint le Leader des Processeurs pour Smartphones](https://www.phonandroid.com/mediatek-domine-le-marche-des-smartphones-loin-devant-qualcomm-et-apple.html)
[^7]: [Apple SoC A13, 2020](https://www.nextinpact.com/article/67716/apple-vante-ses-soc-arm-leurs-fonctionnalites-et-restauration-ses-prochains-mac)
[^8]: [Exynos 9810, anandtech.com](https://www.anandtech.com/show/13199/hot-chips-2018-samsungs-exynosm3-cpu-architecture-deep-dive/3)
[^9]: [TechInsights Exynos 9810, Samsung S9, Cartes Mères](https://www.techinsights.com/blog/samsung-galaxy-s9-teardown)
[^10]: [TechInsights Exynos 9810, Samsung S9 Teardown](https://www.techinsights.com/blog/samsung-galaxy-s9-teardown)
[^11]: [Slideshare Samsung S9 Snapdragon](https://www.slideshare.net/jjwu6266/introducing-samsung-galaxy-s9s9)
[^11]: [ifixit.com, Vue éclatée du Galaxy S9](https://fr.ifixit.com/Tutoriel/Vue+%C3%A9clat%C3%A9e+du+Samsung+Galaxy+S9+/104308)
[^12]: [Exynos 9820 blog with motherboards, chiprebel.com](https://www.chiprebel.com/galaxy-s10-teardown/)
[^13]: [Exynos 9820 blog with motherboards, chiprebel.com](https://www.chiprebel.com/galaxy-s10-teardown/)
[^14]: [Exynos 9820, anandtech.com](https://www.anandtech.com/show/13199/hot-chips-2018-samsungs-exynosm3-cpu-architecture-deep-dive/3)
[^15]: [Liste de Fabricants de processeurs ARM, wikipedia](https://fr.wikipedia.org/wiki/Architecture_ARM#Fabricants_de_processeurs_ARM)
