# 1NSI : Histoire des Ordinateurs : L'EDSAC

## EDSAC, :gb: $1949$

### Introduction

<clear></clear>

<center>

<iframe width="560" height="315" src="https://www.youtube.com/embed/Yc945sNB0uA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/nc2q4OOK6K8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/6v4Juzn10gM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

</center>

### Invention

<clear></clear>

<div style="width:30%; margin-right: 10px; float:left;">

<center>

<figure>

<img src="../../img/edsac.jpg">

<figcaption>

EDSAC, Juin $1948$

</figcaption>

</figure>

<figure>

<img src="../../img/mauriceWilkes.jpg">

<figcaption>

Maurice Wilkes, $1949$

</figcaption>

</figure>

<figure>

<img src="../../img/davidWheeler.jpg">

<figcaption>

David Wheeler, $1949$
premier PhD/doctorat
en Informatique

</figcaption>

</figure>

</center>

</div>

L'**[EDSAC - Electronic Delay Storage Calculator](https://fr.wikipedia.org/wiki/Electronic_Delay_Storage_Automatic_Calculator)**, est une machine électronique anglaise :gb:, programmable, à programme enregistré (sur ruban de papier). Conçu dès $1946$, construit et opérationnel en mai $1949$, date où sont exécutés les premiers programmes, par le mathématicien anglais ***[Maurice Wilkes](https://en.wikipedia.org/wiki/Maurice_Wilkes)*** :gb:, directeur du *Laboratoire de Mathématiques/Informatique* de l'*Université de Cambridge*. Il lit le rapport sur l'*EDVAC* de *John Von Neumann* publié en $1945$. La petite histoire dit qu'il a dû le lire en une nuit, car il devait le rendre le lendemain et il n'y avait pas de photocopies. Il s'aperçoit instantanément que ce rapport décrit l'architecture logique des ordinateurs de demain, et veut participer à la construction de telles machines. En août $1946$, il se rend en bateau aux États-Unis pour assister aux ***Conférences :fr: / Lectures :gb:*** sur ce sujet à la *Moore School of Electrical Engineering* de l'*Université de Pennsylvanie*, mais le bateau subit des retards, et il ne parvient à suivre que les deux dernières semaines de Conférences. Durant les $5$ jours de son voyage de retour en Angleterre, en $1946, $il dessine les schémas de ce qui deviendra l'*EDSAC*, en s'inspirant directement de l'*Architecture de Von Neumann*.

### Quelques caractéristiques techniques

<div style="overflow:hidden;">

* **Premier *Loader*** de l'histoire, appelé "***instructions Initiales***" :fr: / "***Initial Orders***" (v. 1) :gb:, opérationnel dès mai $1949$, qui est une sorte de **premier langage assembleur minimaliste**, développé par **[David Wheeler](https://en.wikipedia.org/wiki/David_Wheeler_(computer_scientist))** :gb:, hardcodé directement (câblé) dans des switchs, et chargé automatiquement (***loaded*** :gb:) en mémoire à l'allumage, dans des *adresses basses*. On peut considérer, à discuter, que cet événement marque **le début de l'industrie des logiciels**.
* mémoires à ligne de délai au mercure : deux batteries de mémoires, chacune contenant $512$ **mots de $18$ bits**, mais le dernier bit n'était pas utilisable pour des raisons de *timing*, donc seuls $17$ bits étaient utilisés, en notation de **complément à 2**
* Programmable : programmes d'une longueur d'environ $800$ mots
* registres : initialement, seulement :
  * un registre **accumulateur** (pour stocker les résultats intermédiaires de l'*<b>U</b>nité <b>A</b>rithmétique et <b>L</b>ogique - ALU*), sans lequel il faudrait stocker les résultats de chaque calcul dans la mémoire centrale. L'accumulateur pouvait contenir $71$ bits, signe inclus, donc permettant la multiplication de deux entiers *longs* de $35$ bits chacun (chacun d'une longueur de deux mots *[petit-boutiste](https://fr.wikipedia.org/wiki/Boutisme)* :fr: / *[little endian](https://en.wikipedia.org/wiki/Endianness)* :gb:, càd qui démarrent avec les octets de poids faible).
  * un registre **multiplicateur** qui traite les nombres, de manière inusuelle, comme fractions à virgule fixe ($-1\le x \lt 1$)
* Instructions : Elles consistent en : 
  * un code d'instruction stocké sur $5$ bits
  * $1$ bit vide
  * un opérande de $10$ bits (en général une adresse)
  * $1$ bit de contrôle, pour indiquer s'il s'agit d'un entier court ($17$ bits) ou long ($35$ bits)
  Toutes les instructions étaient représentées mnémotechniquement par une lettre : ainsi ***A*** représentait l'instruction ***Add***
* valves thermioniques/Tubes CRT pour visualiser les contenus de la mémoire RAM et des registres
* $15\,000$ opérations par minute, dont $4000$ multiplications
* Entrée : bande perforée à $5$ trous
* Sortie : téléscripteur :fr: (teleprinter, teletype ou TTY :gb: )
* Imprimante
* Consommation : $11kW$

</div>

<env>Extensions & Améliorations</env>

* $1951$ : *Maurice Wilkes* développe le principe de ***[microprogramme](https://fr.wikipedia.org/wiki/Microprogrammation)*** :fr: :gb: , ou **[microcode](https://en.wikipedia.org/wiki/Microcode)** :fr: :gb:, en se rendant compte qu'une *Unité Centrale de Calcul* :fr: / *Central Processing Unit, CPU* :gb: pourrait être contrôlé par un très petit (micro) programme, très spécialisé, et stocké dans une **mémoire morte (ROM)** à haut débit : ceci est le premier jalon intelectuel de la notion de **mémoire morte (ROM)**. L'*EDSAC 2* en $1958$, sera néanmoins le premier à en bénéficier
* En $1951$ également, ***[David Wheeler](https://en.wikipedia.org/wiki/David_Wheeler_(computer_scientist))*** (ainsi que *Maurice Wilkes* dans un article en $1953$) propose un **mode d'adressage relatif** ouvrant la voie vers des **sous-programmes** :fr: / **subroutine** :gb:. Néanmoins, **pas de récursivité** possible. En fait, Turing avait déjà discuté des sousprogrammes en $1945$, dans ses propositions de design pour la machine ***[ACE - Automatic Computing Engine](https://en.wikipedia.org/wiki/Automatic_Computing_Engine)*** (sortie : $\approx 1955$) du *[NPL - National Physical Laboratory](https://en.wikipedia.org/wiki/National_Physical_Laboratory_(United_Kingdom))* :gb: où il travailllait à cette époque, allant même jusqu'à inventer le concept de **pile d'adresse de retour**, ce qui aurait autorisé la **récursivité**... La notion de sous-programme mène naturellement à la création de la première bilbiothèque de (sous-)programmes dès $1951$ : calculs arithmétiques à virgules flottante, ...
* $1952$ lecteur/enregistreur de bande magnétique, qui ne fonctionnera jamais suffisament bien pour être vraiment utilisé
* $1953$ : ajout d'un registre d'indice. 
* $1955-1956$ : mémoire additionnelle jusqu'à $1024$ mots
* $1958$ mise en service de son remplaçant : l' *EDSAC 2*, le premier ordinateur à microprogramme (stocké en mémoire morte, ROM)

### Programmation

Le premier programme est un algorithme de calcul du carré, écrit par la canadienne *[Béatrice Worsley](https://en.wikipedia.org/wiki/Beatrice_Worsley)* en mai $1949$.
Algorithmes d'une capacité d'environ $800$ mots.
Les programmeurs préparaient leurs programmes en assembleur sur une bande de papier perforée.

### Aller Plus Loin

* EDSAC Simulator : https://www.dcs.warwick.ac.uk/~edsac/
* EDSAC Wiki, en anglais :gb: : https://en.wikipedia.org/wiki/EDSAC
* Histoire de la microprogrammation : https://www.aconit.org/histoire/calcul_mecanique/documents/Histoire_de_la_microprogrammation.pdf