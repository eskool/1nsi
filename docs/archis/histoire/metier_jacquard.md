# 1NSI : Histoire des Ordinateurs : Le Métier Jacquard

## Basile Bouchon, $1725$, et Le métier Jacquard, $1801$

### Introduction

<center>

<figure>


<iframe width="560" height="315" src="https://www.youtube.com/embed/eE5wxtaIcEY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<figcaption>

Le métier Jacquard, $1801$ ($6$ min $01$)

</figcaption>

</figure>

</center>

### Le métier Bouchon et le métier Jacquard

<clear></clear>

<div style="width:40%; margin-right: 10px; float:left;">

<center>

<figure>

<img src="../../img/metierBouchon.jpg">

<figcaption>

Le métier Bouchon, $1725$

</figcaption>

</figure>

</center>

</div>

Le lyonnais **Basile Bouchon**, en **$1725$**, ouvrier du textile Lyonnais est l'inventeur du métier à tisser semi-automatique, avec des **<bred>rubans</bred> perforés**. Son travail sera généralisé par son assistant **Jean-Baptiste Falcon** en **$1728$**, qui sera l'**inventeur du système de <bred>cartes</bred> perforées** permettant la commande des machines textiles, qui sera reprise et personnalisée par les métiers Jacquard.

<clear></clear>

<div style="width:40%; margin-right: 10px; float:left;">

<center>

<figure>

<img src="../../img/metierJacquard.jpg">

<figcaption>

Le métier Jacquard, $1801$

</figcaption>

</figure>

</center>

</div>

Le **métier Jacquard, $1801$**, est un métier à tisser, mis au point par le Lyonnais **[Joseph Marie Jacquard](https://fr.wikipedia.org/wiki/Joseph_Marie_Jacquard)**, **premier système mécanique programmable avec cartes perforées**. Il influença notablement **Charles Babbage** et sa machine analytique.
La machine Jacquard combine les techniques des aiguilles de Basile Bouchon, les cartes perforées de Falcon et du cylindre de Vaucanson. La possibilité de la programmer, par utilisation de cartes perforées, fait qu'elle est parfois considérée comme un ancêtre de l'ordinateur.

<figure style="float:left; width: 35%;">

<img src="../../img/josephMarieJacquard.jpg">

<figcaption>

Joseph Marie Jacquard

</figcaption>

</figure>

<clear></clear>