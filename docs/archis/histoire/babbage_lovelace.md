# 1NSI : Histoire des Ordinateurs : Ada Lovelace & La Machine Analytique de Charles Babbage

## La Machine Analytique de Charles Babbage, $1834$

### Introduction

<center>

<figure>

<iframe width="560" height="315" src="https://www.youtube.com/embed/_SyOigrbeQs" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<figcaption>

L'ordinateur à vapeur! La machine Analytique Babbage, ($16$ min $55$)

</figcaption>

</figure>

</center>


### la machine analytique de Babbage :gb: $1834$

<clear></clear>

<div style="width:40%; margin-right: 10px; float:left;">

<center>

<figure>

<img src="../../img/charlesBabbage.jpg">

<figcaption>

Charles Babbage, $1791$-$1871$

</figcaption>

</figure>

<figure>

<img src="../../img/machineAnalytiqueBabbage.jpg">

<figcaption>

Machine Analytique de Charles Babbage,
imaginée en $1834$

</figcaption>

</figure>

</center>

</div>

<env>**un premier projet avorté : La machine à Différences**</env>

La <bred>*machine à différences*</bred> du mathématicien et inventeur anglais :gb: <bred>*Charles Babbage*</bred> ($1791-1871$) est **une machine mécanique qui réalise certains calculs très précis**, basée sur la méthode intellectuelle/mathématique inventée un peu plus tôt par le mathématicien anglais **Isaac Newton :gb: ($1642-1727$)**, et appelée : 

>La <bred>*méthode des différences finies*</bred> d'**Issac Newton** :
>* En mathématiques, on montre qu'il est possible d'approximer l'image $f(x)$ d'une fonction $f$ compliquée (exponentielles, logarithmes, trigonométriques, etc..) par le calcul (considéré plus simple) de $P(x)$ où $P$ est un polynôme : $f(x) \approx P(x)$ où $f$ est une fonction compliquée, et $P$ est un polynôme
>* Plus le degré du polynôme $P$ est grand, et plus l'approximation $f(x) \approx P(x)$ est bonne
>* La ***méthode des différences finies*** permet fondamentalement de calculer $P(x)$ pour un certain polynôme $P$ **en utilisant uniquement des additions de manière répétée (boucles)**
>La méthode des différences finies de *Newton* permet donc de calculer des tables de valeurs (pour des fonctions polynômiales, ou des approximations pour des fonctions plus complexes)

Entre $1819$ et $1822$, *Babbage* réalise un premier prototype (dit n°$0$) très simplifié de sa ***machine à différences*** (nombres à $2$ chiffres, et degré $1$) qu'il présente en $1822$ à la *Société Royale d'astronomie* (qu'il a lui-même fondé en $1820$), qui valide son projet et demande au gouvernement britannique de le financer à hauteur de $£1500$. Cette *machine à différences* permet donc de faire des approximations de fonctions compliquées **en n'utilisant seulement que des additions de manière répétée (boucles)**.
Le gouvernement britannique y trouvera rapidement un intérêt militaire afin de réaliser des **tables de calculs balistiques** (calcul de trajectoire des missiles balistiques pour l'armée) : C'est pourquoi dès $1823$, et jusqu'en $1842$, le gouvernement britannique acceptera de financer plusieurs fois ses travaux et sa machine jusqu'à hauteur de $£17\,000$ (soit $\approx 2\,000\,000€$ d'aujourd'hui)  ([source](https://en.wikipedia.org/wiki/The_Information:_A_History,_a_Theory,_a_Flood)). Dès $1823$, motivé et subventionné, *Babbage* ambitionne que *sa machine à différences* soit la plus précise possible. Il réalise des croquis sur le papier pour une machine (à différences) qui manipulerait des nombres à $20$ chiffres et des polynômes de degré $6$, mais la précision requise pour l'usinage des pièces, ainsi que le nombre total de pièces requises, rend le projet techniquement et économiquement difficile à mener à terme : elle ne sera jamais construite. Petit succès malgré tout, en $1832$, après $9$ ans de travail en collaboration avec l'ingénieur **Joseph Clement**, l'un des meilleurs outilleurs britanniques de son temps, *Babbage* obtient (enfin) un prototype miniature (dit n°$I$), à l'échelle $\displaystyle \frac 17$, capable de manipuler "seulement" des nombres à $6$ chiffres et des polynômes de degré $2$. La *machine à différences* finale, de $20$ chiffres et de degré $6$, est abandonnée. En $1840$, il répond à l'invitation de [Giovanni Plana](https://en.wikipedia.org/wiki/Giovanni_Antonio_Amedeo_Plana) :it: et se rend à turin, italie :it:, où il présentera sa machine. Présent lors de ses présentations, le général italien ***Luigi Menabrea*** prend des notes et publie un article descriptif dans un journal suisse.

En $1849$, il abandonne l'idée d'une *machine à différences* encore plus complexe (dite n°2, à $31$ chiffres, degré $7$), inspirée de ses réflexions sur la machine analytique...

<env>Une idée géniale, mais encore avortée : <bred>*la machine analytique*</bred></env>

Dès $1834$, durant la construction d'un nouveau prototype de la *machine à différences*, *Babbage* a l'idée géniale d'une nouvelle machine, la <bred>*machine analytique*</bred>, qui serait une machine à calculer **à vapeur(!)** ***mécanique & programmable***. Il se rend rapidement compte de la supériorité de la **machine analytique** qui serait, il l'avait bien senti, bien plus "***universelle***" (au sens de Turing..) que la *machine à différences* : En effet la *machine analytique* permettait de réaliser n'importe quelles opérations, contrairement à la *machine à différences* dont les fonctionnalités était très/trop spécialisées donc limitées. Dès $1834$, *Babbage* veut abandonner totalement son ancien projet (la machine à différences) et veut tout recommencer depuis zéro pour son nouveau projet (la machine analytique). Mais après $9$ ans de travail commun qu'il estime perdus, l'ingénieur *Clement* est en désaccord et quitte le projet. *Babbage* passera néanmoins le reste de sa vie (de $1834$ à $1871$) à la concevoir *théoriquement* dans les moindres détails, en dessinant de nombreux croquis détaillés **sur le papier**. Il en fera une première description en $1837$. Le plus jeune de ses fils, *Henry Babbage*, en construira  ***le moulin*** (l'unité de calcul) et l'**imprimante** de $1880$ à $1910$.

**A sa mort en $1871$, la machine est encore un prototype inachevé. Elle ne sera jamais totalement finie d'être construite, ni de son vivant, ni pendant de longues décennies après sa mort**, pour au moins trois raisons :

* Pour des raisons économiques : En $1842$, après un total de $£17\,000$ de financements, et un prototype de *machine à différences* toujours pas fonctionnel, ni de *machine analytique* évidemment, le gouvernement britannique finira par lui refuser de nouveaux financements.
* *la machine analytique* utilise la notation ***décimale*** et donc est bien plus compliquée à réaliser qu'une machine basée sur le *binaire* (comme le sera par ex. la machine $Z3$ de l'allemand *Konrad Zuse* en $1943$). 
* Enfin, le mythe dit qu'il aurait volontairement laissé quelques erreurs dans ses croquis pour éviter le vol de propriété intellectuelle, ce qui compliqua la poursuite de son oeuvre après sa mort (pour son fils en particulier) 

**Aller Plus Loin :** Podcast : [Premières machines à Calculer](https://www.youtube.com/watch?v=zYaj41qq_ao&t=1007s)


<env>Quelques Caractéristiques Techniques</env>

La machine comprend les différentes parties suivantes dont certaines se retrouvent encore dans un ordinateur d'aujourd'hui :

* Dispositifs d'**entrée** : deux lecteurs perforateurs de **cartes perforées** (une pour les **instructions** et une pour les **données**) ; ces cartes sont inspirées et issues des techniques du métier à tisser Jacquard.
Un 3ème lecteur perforateur de cartes perforées était prévu pour transférer les nombres de l'unité de commande vers la mémoire, et réciproquement, ainsi que pour gérer les boucles.
* Un $\approx$ "*processeur*" (il n'emploie pas ce mot là) divisé en deux:
  * un **organe de commande** $\approx$ *unité de commande/contrôle* gère le transfert des nombres, leur mise en ordre des instructions pour le traitement, les embranchements conditionnels et les boucles ;
  * un **moulin** $\approx$ *unité de calcul* (inspiré de sa précédente ***machine à différence***) est chargé d'exécuter les $4$ opérations de base sur les nombres, les racines carrées et les comparaisons;
* un **magasin** $\approx$ *mémoire interne* permet de stocker les résultats intermédiaires ou finaux dont la capacité est :
  * $1000$ nombres décimaux de $40$ chiffres (léquivalent de $16,2 Ko$)
  * gérait les nombres à virgule fixe en base $10$ (décimal)
* $3$ Périphériques de Sortie:
  * Une **imprimante** à caractères
  * Une imprimante à courbe
  * une **cloche** de signalement
* Stockage Externe : un système automatisé de gestion de plusieurs dizaines de milliers de cartes perforées 
* Calculabilité : considéré **Turing-complet**, i.e que cette machine peut exécuter n'importe quel algorithme 
* Alimentation : Roues et Engrenages mus par la **Vapeur !!**
* trois types d'**imprimantes** sont prévues.
* $20000$ pièces
* Poids : $\approx 13$ t
* Taille : comparable à une locomotive

### La Collaboration et l'Apport d'Ada Lovelace

#### Introduction

<center>

<figure>

<iframe width="560" height="315" src="https://www.youtube.com/embed/PK-AeOnK16M" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<figcaption>

Ada Lovelace et la machine Analytique de Babbage ($6$ min $13$)

</figcaption>

</figure>

</center>

#### Première *codeuse* (théorique) de l'Humanité

<div style="clear:both;"></div>
<br/>

<div style="width:40%; margin-right: 10px; float:left;">

<center>

<figure>

<img src="../../img/adaLovelace2.jpg">

<figcaption>

Ada Lovelace, à $27$ ans, $1815$-$1852$

</figcaption>

</figure>

<figure>

<img src="../../img/adaLovelace.png">

<figcaption>

Ada Lovelace, $1815$-$1852$, Daguerréotype 

</figcaption>

</figure>

</center>

</div>

**[Ada Lovelace](https://fr.wikipedia.org/wiki/Ada_Lovelace)** :gb: ($1815$-$1852$), née *Ada Byron*, est la fille du célèbre poète "maudit", voyageur et Lord donc homme politique britannique ***Lord Byron***, et de la passionnée de mathématiques **Annabella Milbanke**.
Au cours du développement de la machine analytique, **[Ada Lovelace](https://fr.wikipedia.org/wiki/Ada_Lovelace)** travaille en étroite collaboration avec *Charles Babbage* : Elle formalise les idées de ce dernier au sujet de sa machine, et développe même ce que les experts considèrent aujourd'hui comme le **premier *véritable* algorithme/programme informatique *théorique*  de l'histoire**, devenant ainsi <bred>la première *codeuse (théorique)* de l'humanité</bred>.

Entre $1842$ et $1843$, *Ada LoveLace* traduit pendant $9$ mois un article suisse paru en français, écrit par le général italien ***Luigi Menabrea*** :it:, décrivant la machine de *Babbage*. Impressionné par la qualité de sa traduction qui témoigne d'une excellente connaissance de sa machine, *Babbage* (malade tout ce temps) lui demandera si elle accepterait d'ajouter plusieurs ***notes*** (de $A$ à $G$) supplémentaires détaillant sa machine, ce qu'elle acceptera de faire.
La *note* $G$ contient le ***premier véritable algorithme théorique de l'Humanité, destiné à être exécuté par une machine*** : un ***Algorithme de Calcul des Nombres de Bernouilli***. 
Au moins deux raisons justifient cette qualification:

* les algorithmes décrits jusque-là (dont certains par *Babbage* lui-même) n'étaient pas décrits avec un formalisme, dans un langage véritablement destiné à être exécuté sur une machine. De plus, 
* ce programme comporte, selon l'ingénieure française *Catherine Dufour*, **la première boucle conditionnelle** `while`, véritable concept informatique, contrairement aux *programmes séquentiels* qui avaient pu être faits auparavant par *Babbage*, ou dans les *métiers à tisser Jacquard*. 

*Babbage* et *Lovelace* travaillant en bonne intelligence entre eux, la proportion de l'apport/ la participation de *Babbage* aux notes (de $A$ à $G$) de *LoveLace*, est quelquefois soumis à débat (voire privilégié) par certains experts, mais il semblerait néanmoins que pour la note $G$ du premier premier algorithme, la participation de *Babbage* se soit limité aux formules mathématiques des nombres de Bernouilli, et que tout l'algorithme ait bien été créé par *Ada Lovelace*.
**Aller plus loin** : ["Polémique" de l'apport respectif d'Ada Lovelace vs Babbage](https://fr.wikipedia.org/wiki/Ada_Lovelace#M%C3%A9moire_sur_la_machine_de_Babbage)

<env>Remarque Historique sur la notion d'Algorithme</env> Noter que la finalité d' :warning: **être exécuté par une machine** :warning: est tout de même importante, car sinon, on trouve des "algorithmes" *théoriques* bien plus anciens :

* vers le **$III$-ème millénaire av J.C.**, les **Babyloniens** proposent des *algorithmes* qui consistent en des méthodes de calcul et de résolution d'équations, à l'aide d'exemples
* vers le **$III$-ème millénaire av J.C.**, des algorithmes de **multiplication** et **division** du **Moyen Empire** de l'**Égypte ancienne**, 
* vers **$300$ av J.C.**, le Livre $7$ des **Éléments d'Euclide**, cite l'**algorithme d'Euclide** (de calcul du PGCD). Il contient notamment une des premières **boucles** / **itérations** de l'Histoire de l'Humanité, et la première démonstration de la **correction** d'un algorithme
* vers **250 av J.C.**, Dans son texte "***De la mesure du cercle***", le savant grec **Archimède** à *Syracuse* (actuelle Sicile), propose le premier algorithme de calcul de $\pi$
* vers le **$II$-ème Siècle av J.C.**, le *texte* (écrit sur bambous) classique chinois :cn: des **Neuf Chapitres** (sur l'art mathématique), compile des résultats (probablement antérieurs) dont notamment des opérations sophistiquées comme des **boucles**/**itérations**, **assignation de variables**, **correction d'algorithmes**, etc..
* Au **$VIII$-ème Siècle ap J.C.**, le nom du savant arabe ***Al-Khwarizmi*** (ouzbékhistan actuel) donna naissance au mot ***algorithme*** (et son oeuvre donna naissance au mot ***algèbre***). Il résuma de nombreux algorithmes existants, et proposa une classification selon leurs critères de **terminaison**, mais sans pour autant les avoir les inventés.

<center>

<figure>

<img src="../../img/premierProgramme.jpg">

<figcaption>

Premier *véritable* programme/algorithme (théorique) de l'Humanité, 
par Ada LoveLace, $1843$
Algorithme de ***Calcul des Nombres de Bernouilli***

</figcaption>

</figure>

</center>

#### Première Informaticienne de l'Humanité (?)

*Babbage* avait semble-t-il une vision restreinte des possibilités sa machine : pour lui, elle était tournée vers le ***calcul numérique***, éventuellement le  ***calcul algébrique***, mais c'est tout. Au contraire, *Ada Lovelace* entrevoit des possibilités pour la machine que certains experts considèrent comme « visionnaire, même dans une perspective moderne ». 
Elle décrit explicitement des possibilités allant au-delà d'un contexte mathématique, ce qui justifie qu'on la qualifie de ***première informaticienne de l'Humanité*** :
* « *la machine pourrait composer de manière scientifique et élaborer des morceaux de musique de n'importe quelle longueur ou degré de complexité* ». 
* « *La Machine peut arranger et combiner les quantités numériques exactement comme si elles étaient des lettres, ou tout autre symbole général ; en fait elle peut donner des résultats en notation algébrique, avec des conventions appropriées.* »

Elle mourut à l'âge de $36$ ans d'un cancer de l'utérus, dans d'horribles souffrances, et criblée de dettes de jeu contractées pour financer la finalisation de la contruction de la *machine analytique* de *Babbage*, que le gouvernement britannique avait refusé de (continuer à) financer dès $1842$.

#### Plusieurs Hommages à Lovelace

* Un des premiers superordinateurs du *CNRS* s'appelle ***Ada***
* Les Certificats d'authenticité de Windows $95$ comportent un hologramme du portrait d'*Ada Lovelace*
* Un **[langage de programmation *Ada*](https://fr.wikipedia.org/wiki/Ada_(langage))** sera ainsi nommé en son honneur au début des années $1980$.

<clear></clear>
