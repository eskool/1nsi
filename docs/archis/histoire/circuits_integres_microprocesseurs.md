# 1NSI : Histoire des Ordinateurs :<br/>Des Circuits Intégrés aux Microprocesseurs


## $1^{er}$ Circuit Intégré, $1958$

En $1958$, l’Américain **Jack Kilby** invente **le premier circuit intégré**, jetant ainsi les bases du matériel informatique moderne. **Jack Kilby**, qui venait de rejoindre la compagnie **Texas Instruments**, a fait cette découverte alors que ses collègues profitaient de vacances organisées par la société. À l'époque, *Kilby* avait tout simplement **relié entre eux différents transistors en les câblant à la main**. Il ne faudra par la suite que quelques mois pour passer du stade de prototype à la production de masse de puces en silicium contenant plusieurs transistors. Ces ensembles de transistors interconnectés en circuits microscopiques dans un même bloc, permettaient la réalisation :

* de mémoires, ainsi que 
* d’unités logiques et arithmétiques. 

Ce concept révolutionnaire concentrait dans un volume incroyablement réduit, un maximum de fonctions logiques, auxquelles l'extérieur accédait à travers des connexions réparties à la périphérie du circuit. Le brevet est finalement accordé à **Texas Instruments** en $1964$. Cette découverte a valu à *Kilby* un **prix Nobel de Physique** en $2000$, alors que ce dernier siégeait toujours au directoire de Texas Instruments et détenait plus de $60$ brevets à son nom.

## Intel $4004$, Novembre $1971$ : le $1^{er}$ microprocesseur commercialisé

<div style="width:30%; min-width:200px;  margin-right: 10px; float:left;">

<center>

<figure>

<img src="../../img/intelP4004.jpg" style="width:100%;" />

<figcaption>

Intel P4004, version en Plastique
Novembre $1971$

</figcaption>

</figure>

</center>

</div>

Le $4004$ d'Intel est le premier microprocesseur commercialisé, c'est-à-dire la première intégration réussie de toutes les fonctions d'un processeur sur un seul et unique circuit intégré.

Il est d'abord produit en exclusivité pour l'industriel qui a commandité son développement, Busicom, en mars $1971$. Après avoir fait lever la clause d'exclusivité, Intel annonce sa commercialisation le $15$ novembre $1971$.

Sa réalisation est rendue possible par la toute nouvelle technologie des self-aligned gates, qui permet à son concepteur, **Federico Faggin**, d'intégrer sur un même circuit intégré $2\,250$ **transistors** bipolaires.

Avec une puissance d'exécution de $92\,600$ **opérations par seconde** à une fréquence maximale de $740 kHz$ , il est comparable à lui tout seul avec l'*ENIAC*, le premier ordinateur moderne dévoilé en $1946$, qui occupait $167\,m^2$ pour un poids total de $30$ tonnes.

Site dédié : [intel4004.com](http://www.intel4004.com) et ses [schémas originaux de connexion](http://www.intel4004.com/4004_original_schematics.htm)

## $1974$ : Intel $8080$

* Mots de $8$ bits 
* bus adresses $16$ bits, bus données $8$ bits 
* $7$ registres $8$ bits 
* $64\, k$ octets adressables 
* $6000$ transistors 
* Horloge : $2\,\, MHz$

## $1981$ : Intel $8086$

L’*Intel* $8086$ équipe le premier PC et le *MOTOROLA* $68000$ 
l’*Apple II*, début de l’informatique grand public. 

## Construction des Circuits Intégrés

<center>

<iframe width="560" height="315" src="https://www.youtube.com/embed/ee-LhNZPZ1U" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<figcaption>

Les circuits intégrés, par [Deus Ex Silicium](https://www.youtube.com/watch?v=ee-LhNZPZ1U&list=PLAHUBiYBxq_vSMt8oGm-62Fi3JpzKWg5p&index=1)

</figcaption>

</center>