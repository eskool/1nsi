# 1NSI : Histoire des Ordinateurs : Des Microcontrolleurs aux Processeurs Multicoeurs

## 1985-1990 : premiers microcontrôleurs industriels 

*Intel* $8051$ et *MOTOROLA* $68HC11$.


## IBM *POWER4* : Premier processeur multi-coeur, $2001$

On doit à IBM le premier processeur multi-cœur à avoir été commercialisé : il s'agit du POWER4, en 2001