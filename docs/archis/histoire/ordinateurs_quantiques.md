<title>1NSI : Architecture & Technologie des Ordinateurs Quantiques</title>

## Introduction

L'informatique quantique promet une rupture/révolution aussi grande que celle des premiers ordinateurs dans les années $1940-1950$.

<center>

<iframe width="560" height="315" src="https://www.youtube.com/embed/rNdWOXQ8V4A" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/bayTbt_8aNc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/2aCS5mEeiwg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

</center>

## Physique Classique & Erreurs dans les Ordinateurs Classiques

### Physique Classique

La **physique classique** décrit bien le ***monde macroscopique***, dans lequel les ***objets*** ("*classiques*") sont caractérisés par des *paramètres* auquels nous sommes habitués à l'échelle humaine dans notre quotidien, par exemple :

* objets : des vélos, des fruits, des clés, etc..
* forces : vitesse, accélération
* un état unique/**non superposition** : une lampe est : allumée ou éteinte, mais pas les deux simultanément..
* localisation/**localité** unique : position précise dans l'espace, mais pas à deux/plusieurs endroits différents simultanément...

### Erreurs & Codes Correcteurs d'Erreurs (ECC) Classiques

<env>Erreurs Discrètes</env>

Dans un **ordinateur classique** :
* **Espace Discret** : Pour chaque bit classique, il n'existe que **deux** états possibles : un $0$ ou un $1$ en notation binaire
* **Erreurs Discrètes** : Et une seule erreur possible : l'interversion d'un $0$ par un $1$ , et/ou réciproquement.

Des **erreurs** peuvent apparaître dans les **informations** :

* ou bien transmises via un **canal de communication "*peu fiable*"**, càd que les données circulant sur ce canal de communication (radio, câble coaxial, fibre optique, etc..) sont susceptibles d'être altérées, par exemple :
  * avec des **parasites sur la ligne** sur un canal *radio*
  * avec du **bruit aléatoire** (perturbations électromagnétiques, imperfection de la fibre optique, etc..) sur un canal *câble coaxial* ou *fibre optique*, etc.. 
* ou bien stockées sur **un support de stockage** :
  * CD et/ou DVD : disques rayés, etc..
  * mémoire vive (RAM) : perturbations électromagnétiques, erreur matérielle, etc..
  * ou d'autres applications où la garantie de l'**intégrité des données** est importante

<env>Codes Correcteurs d'Erreurs (ECC) Classiques</env>

<div style="width:30%; margin-right: 10px; float:left;">

<center>

<figure>

<img src="../img/ecc.jpg" />

<figcaption>

Photo avec erreurs (gauche)
Photo corrigée (droite)

</figcaption>

</figure>

</center>

</div>

La théorie classique des **[Codes Correcteurs d'Erreurs (ECC)](https://fr.wikipedia.org/wiki/Code_correcteur)** est utilisée, avec des ordinateurs classiques, pour **détecter** et **corriger ces erreurs** (du moins certaines d'entre elles).

Les méthodes usuelles de **détection** et **correction d'erreurs**, utilisent la **redondance** : on ajoute des bits supplémentaires dits "***bits de contrôle***" calculés "*intelligemment*" à partir des ***bits d'information/message***. Ensuite, on **mesure l'information transmise** et on utilise les techniques des **[Codes Correcteurs d'Erreur (ECC)](https://fr.wikipedia.org/wiki/Code_correcteur)**, qui permettent de :

<div style="overflow:hidden;">

* ***Détecter***, pour chaque bit de données transmis, s'il y a eu une erreur, ou pas (on recalcule les bits de contrôle)
* ***Corriger*** une telle erreur (s'il y en a une), toujours grâce aux bits de contrôle. En pratique, pour corriger les erreurs on utilise la théorie des **Codes Correcteurs d'Erreurs (ECC)** qui sont des méthodes pour retrouver les informations correctes dans les bits d'information, à partir des bits de contrôle. Il en existe plusieurs : 
  * [Code à répétition](https://fr.wikipedia.org/wiki/Code_de_r%C3%A9p%C3%A9tition),
  * [Codes Linéaires](https://fr.wikipedia.org/wiki/Code_lin%C3%A9aire), 
  * [Codes Cycliques CRC](https://fr.wikipedia.org/wiki/Code_cyclique), 
  * [Codes de Reed-Solomon](https://fr.wikipedia.org/wiki/Code_de_Reed-Solomon) utilisés pour les CD et DVD, et plus généralement 
  * [Codes de Gopppa](https://fr.wikipedia.org/wiki/Code_de_Goppa), 
  * etc...

</div>

Il existe une relation entre le nombre d'erreurs $d$ au maximum que l'on peut corriger, la longueur $n$ de bits d'information transmis, et la longueur $k$ des bits de contrôle. **Cette relation dépend directement du Code Correcteur d'Erreur (ECC)** choisi.

## Physique Quantique

<clear></clear>

<div style="width:30%; margin-right: 10px; float:left;">

<center>

<figure>

<img src="../img/qubitEtatsBase.jpg" style="width:100%;" />

<figcaption>

qbit avec deux états quantiques $|0\rangle$ et $|1\rangle$
séparés par une fréquence $f_{QB}$

</figcaption>

</figure>

</center>

</div>

Par opposition, la **physique quantique** (qui n'est pas nouvelle en soi, puisque ses bases théoriques datent des années $1920$) est une partie de la physique qui décrit bien le ***monde microscopique***, où **certains phénomènes contre-intuitifs/non attendus peuvent se produire** (du moins pour nous, qui sommes habitués à d'autres mondes), pour des **objets quantiques** appelés <bred>systèmes quantiques</bred> (**une particule, un groupe de particules, etc..**), 

Ci-après quelques **principes quantiques** contre-intuitifs :

<clear></clear>

### Dualité Onde-Particule :scream:

  <center>

  <iframe width="560" height="315" src="https://www.youtube.com/embed/JlsPC2BW_UI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

  </center>

### **[Superposition (quantique)](https://fr.wikipedia.org/wiki/Principe_de_superposition_quantique)** :scream: 

Un même *objet quantique* peuvent se trouver dans **plusieurs (une infinité d') états en même temps**. Une interprétation de ce principe (au monde macroscopique..) serait par exemple : un chat - dit [chat de schrödinger](https://fr.wikipedia.org/wiki/Chat_de_Schr%C3%B6dinger) - à l'intérieur d'une boîte noire, pourrait se trouver simultanément vivant ET mort à la fois... :scream: Aller Plus Loin : [Vidéo Physique Quantique et Superposition, ($5$ min $40$)](https://youtu.be/y1T463vMEfc)

  <center>

  <iframe width="560" height="315" src="https://www.youtube.com/embed/Va-WLeObSSo" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

  <iframe width="560" height="315" src="https://www.youtube.com/embed/TH-NgOeDbGs" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

  </center>

### Intrication (quantique) :fr: / Entanglement :gb:,  :scream:


Un phénomène dans lequel deux objets quantiques forment un système unique, et présentent des **états quantiques dépendant exactement l'un de l'autre** quelle que soit la distance qui les sépare. Une interprétation de ce principe (au monde macroscopique..) serait par exemple : Deux lampes séparées par une distance infinie seraient toujours dans le même état : allumer (/éteindre) l'une des lampes, allumerait (/éteindrait) automatiquement et instantanément l'autre, et réciproquement ... :scream:

### Non-Localité (quantique) / Ubiquité :scream:

Un même objet quantique peut se trouver **simultanément à plusieurs (une infinité d') endroits distincts de l'espace**. Une interprétation de ce principe (au monde macroscopique..) serait par exemple : Un trousseau de clés pourrait se trouver simultanément : sur la table ET dans votre poche ... :scream:

### Décohérence (quantique) :scream: 

Notion plus récente, introduite par *Dieter Zeh* en $1970$. C'est le fait qu'un objet quantique perde ses propriétés quantiques ce qui peut se produire classiquement du fait de ses **interactions avec son environnement** :

* champ électrique, 
* champ magnétique, 
* lumière : par ex. illuminer un objet quantique localise sa position, donc casse sa cohérence quantique :scream:
* agitation thermique 
* interactions non contrôlées entre les qubits
* par lecture de la valeur d'un des paramètres de l'objet
* etc..

<env>Remarque (CULTURE...)</env> Bien que la physique quantique ne s'applique prioritairement qu'au monde microscopique, il existe néanmoins des objets quantiques étranges dont les manifestations sont visibles à l'échelle humaine (macroscopique), par exemple : Dans un « **condensat de Bose-Einstein** », bien que les atomes qui le constituent aient conservé leur existence propre, ils ont perdu leur individualité.

### Aller Plus Loin (dans la Physique Quantique)

* [Peut-on retrouver des objets quantiques dans la vie de tous les jours ?](https://www.ficsum.com/dire-archives/volume-23-numero-2-ete-2014/sciences-peut-on-retrouver-des-objets-quantiques-dans-la-vie-de-tous-les-jours/), FICSUM, Sébastien Boisvert, $2014$

## L'élément de base : Le *qubit* (physique)

<clear></clear>

### Qu'est-ce qu'un qubit (physique) ?

<div style="width:30%; margin-right: 10px; float:left;">

<center>

<figure>

<img src="../img/qubitBloch.jpg" />

<figcaption>

Représentation d'un qubit, dans un état quantique $|\psi \rangle$, 
par une "*Sphère de Bloch*"

</figcaption>

</figure>

</center>

</div>

Par opposition, **un ordinateur quantique** est composé de **bits quantiques** ou **qubits** ou **q-bits** / **qu**antum **bits**, sous-entendu **qubits "*physiques*"**, qui sont les **unités élémentaires pouvant contenir une information quantique**. Chaque *qubit* peut être dans deux **états quantiques**, séparés par une différence d'énergie définissant sa **fréquence quantique** $f_{QB}$, notés par convention et par analogie avec les bits classiques :

<clear></clear>

* un état ***de base*** : $|0\rangle$
* un état ***excité*** : $|1\rangle$
* **ou bien dans les deux états** $|0\rangle$ et $|1\rangle$ ***"en même temps"*** :scream: : c'est le **principe de superposition** (linéaire), que l'on notera :
  * <enc>$|\psi \rangle=\alpha |0\rangle + \beta |1\rangle$</enc> $\,$ où $\alpha$ et $\beta$ sont des ***nombres complexes*** tels que $|\alpha|^2+|\beta|^2=1$, dont les modules (sorte de valeurs absolues) sont interprétables d'après **Max Born**, comme des probabilités de se trouver dans chaque état respectif ($|0\rangle$ ou $|1\rangle$)
  <env>Remarque</env> 
    * La théorie montre que l'on peut même considérer que **$\alpha$ est un réel $>0$, et que $\beta$ est complexe**, sans restreindre la généralité, ce que nous supposerons donc systématiquement dans toute la suite. (En effet : la phase du système quantique n'est pas directement mesurable, donc seule la phase relative entre les coefficients des vecteurs 2D admet une interprétation physique...)
    * Dans un souci de préparer une généralisation des notations dans le cas de plusieurs qubits, on note quelquefois :
    <enc>$|\psi \rangle=u_0 |0\rangle + u_1 |1\rangle$</enc> $\,$ où $u_0$ et $u_1$ sont des ***nombres complexes*** tels que $|u_0|^2+|u_1|^2=1$
  * <env>**Notation de Born**</env> <enc>$1$ ÉTAT QUANTIQUE d'$1$ qubit (ou de plusieurs..) $= 1$ VECTEUR COLONNE</enc>
    * Représentation vectorielle de $1$ qubit : 
    On note $|\psi \rangle =
  \begin{bmatrix}
  \alpha \\
  \beta \\
  \end{bmatrix}
  $ ou $|\psi \rangle =
  \begin{bmatrix}
  u_0 \\
  u_1 \\
  \end{bmatrix}
  $ sous forme de **vecteur colonne** (selon la notation choisie)

### Sphère de Bloch : Représentation d'un état quantique $|\psi\rangle$

L'état quantique $|\psi\rangle$ d'un qubit peut être modélisé par sa position sur **la Sphère** dite **de Bloch**, position qui est elle-même caractérisée par deux nombres réels $(\varphi, \theta)$, qui sont calculables à partir du réel $\alpha>0$ et du complexe $\beta$, grâce à des relations trigonométriques :

<center>

<enc>$\alpha = cos \left( \dfrac {\theta}2 \right)$
  $\beta=e^{i \varphi} sin \left( \dfrac {\theta}2 \right)$</enc> $\quad$ avec $e^{i\varphi} = cos(\varphi)+isin(\varphi)$

</center>

Bien que $|0\rangle$ et $|1\rangle$ soient représentés diamétralement opposés dans la Sphère de Bloch, on dit que **$|0\rangle$ et $|1\rangle$ sont *orthogonaux***.
Ces qubits génèrent encore cependant un "***bruit***" non négligeable, même en les exploitant à des températures proches du zéro absolu, ce qui provoque des **erreurs** dites **"*quantiques*"**.

<clear></clear>

### Différentes Technologies de qubits (physiques)

#### qubits Supraconducteurs

<div style="width:30%; margin-right: 10px; float:left;">

<center>

<figure>

<img src="../img/qubitSupraconducteur.png" />

<figcaption>

qubit Supraconducteur

</figcaption>

</figure>

</center>

</div>

C’est la technique la plus couramment employée aujourd’hui, notamment par ***IBM***, ***Google*** et ***Intel*** :us: pour des ordinateurs quantiques à *circuits universels*, et avec les ordinateurs quantiques *adiabatiques* du Canadien ***D-Wave*** qui utilisent un autre arrangement de qubits de qualité moins bonne du côté du bruit et du taux d’erreurs. En France, le ***CEA - Commisariat à l"Energie Atomique*** :fr: est aussi précurseur de cette technique.
Le qubit prend la forme de l’état d’un **courant supraconducteur** qui traverse une barrière très fine en s’appuyant sur l’**[effet Josephson](https://fr.wikipedia.org/wiki/Effet_Josephson)** (apparition d'un courant entre deux matériaux supraconducteurs séparés par une couche faite d'un matériau isolant ou métallique non supraconducteur). Sans trop rentrer dans les détails, il existe plusieurs types de qubits supraconducteurs : de **flux**, de **phase** et de **charge**. Dans tous les cas, il s’agit de créer une superposition de deux états bien distincts d’un courant oscillant à haute fréquence et traversant la **[jonction Josephson](https://fr.wikipedia.org/wiki/Effet_Josephson)** dans une boucle supraconductrice. L’oscillation est rendue possible par le fait que la boucle intègre l’équivalent d’une inductance et d’une résistance. L’oscillation du courant est activée par l’application de micro-ondes de fréquences situées entre $5$ et $10 \,GHz$ transmises par voie conductrice et physique. Ce ne sont pas des ondes émises par la voie “radio”. L’état du qubit est pour sa part mesuré avec un magnétomètre intégré dans le circuit. Cette technique est relativement facile à fabriquer car elle s’appuie sur les techniques de création de **circuits CMOS** même si certains des matériaux sont différents, 

#### Ions Piégés

<div style="width:30%; margin-right: 10px; float:left;">

<center>

<figure>

<img src="../img/qubitIonPiege.png" />

<figcaption>

qubit Ion Piégé

</figcaption>

</figure>

</center>

</div>

Il s’agit d’***ions d’atomes*** qui peuvent être du *calcium* et qui sont maintenus sous vide et suspendus par suspension électrostatique. Un pompage optique est réalisé pour leur initialisation. Un laser sert à la mesure et exploite le phénomène de fluorescence des ions excités par le laser. Le magnétisme est utilisé pour l’activation des portes quantiques. La startup ***IonQ*** :us: issue de l’*Université de Maryland* planche là-dessus tout comme l’*Université d’Innsbruck* en Autriche et sa spinoff ***AQT***. Dans un tel système, plusieurs ions sont piégés de manière équidistante les uns des autres. Ils sont alignés en rang d’oignons. Il est difficile de faire "*scaler*" ce genre d’arrangement au-delà d’une centaine et quelques d’ions. Par contre, ils présentent l’avantage de bien pouvoir être intriqués les uns avec les autres, ce qui est moins le cas des qubits à base de supraconducteurs.

#### Photons

<div style="width:30%; margin-right: 10px; float:left;">

<center>

<figure>

<img src="../img/qubitPhoton.png" />

<figcaption>

qubit Photon

</figcaption>

</figure>

</center>

</div>

L'état quantique des ***photons*** est leur polarisation horizontale ou verticale. Cela fait partie du champ de l’optique linéaire. Il manipule des photons individuels. Les portes quantiques sont réalisées à l’aide de dispositifs optiques avec des filtres dichroïques ou polarisants. Il faut un grand nombre de lasers pour piloter l’ensemble. C’est donc pour l’instant assez embarrassant. L’avantage est que ces qubits fonctionnent à température ambiante. Ce type de qubit n’est pour l’instant utilisé qu’en laboratoire : En Juillet $2021$, l'**USTC - Université de Sciences et Technologies de Chine** devient pour la première fois leader mondial, en développant un calculateur quantique photonique qui atteint la **Suprématie Quantique** avec $66$ qubits (dont seuls $56$ ont été réellement utilisés). Aucune société privée n'utilise cette technologie pour le moment.

#### Spin d'Électron

<div style="width:30%; margin-right: 10px; float:left;">

<center>

<figure>

<img src="../img/qubitSpinElectron.png" />

<figcaption>

qubit Spin d'électron

</figcaption>

</figure>

</center>

</div>

Le ***spin d'électron***, haut ou bas, une sorte de sens de polarisation magnétique, que l’on retrouve dans les ordinateurs à base de **quantum dots**, notamment chez **Intel** ou dans des prototypes de qubits réalisés au CEA à Grenoble. Ces qubits sont intégrés dans des circuits à base de semi-conducteurs CMOS. Ils bénéficient donc de la réutilisation de processus de fabrication de composants CMOS déjà bien maitrisés. Ces qubits sont cependant pour l’instant plutôt “bruyants”, même en les exploitant à des températures proches du zéro absolu. Le plan consiste à en aligner des batteries pour créer des qubits logiques, un concept que nous étudierons dans la prochaine partie.

<clear></clear>

#### Centres NV - Nitrogen Vacancy

<clear></clear>

<div style="width:30%; margin-right: 10px; float:left;">

<center>

<figure>

<img src="../img/qubitCentreNV.png" />

<figcaption>

qubit Centres NV

</figcaption>

</figure>

</center>

</div>

Les **centres NV** pour **Nitrogen Vacancy** : ce sont des **structures de diamant artificiel** dans lesquelles un atome de carbone a été remplacé par un atome d’azote et à proximité duquel se situe une lacune d’atome de carbone. Les électrons des atomes de carbone adjacents à cette lacune occupent l’espace vide et avec un spin variable. Bref, l’état de la lacune est instable et quantique. Cet état est excité par laser et micro-ondes. La lecture de l’état du qubit est réalisée par une mesure de brillance de fluorescence. Seule la startup ***QDTI*** planche commercialement sur cette technique. Ce n’est visiblement pas encore au point.

<clear></clear>

#### qubits Topologiques, ou Fermions de Majorana

<div style="width:30%; margin-right: 10px; float:left;">

<center>

<figure>

<img src="../img/qubitMajorana.png" />

<figcaption>

qubit de Majorana

</figcaption>

</figure>

</center>

</div>

Les **Fermions de Majorana** sont des anyons ou quasi-particules qui sont des états particuliers de nuages d’électrons organisées par paires. Pratiquement, ce sont des spins d’électrons aux deux bouts de fils supraconducteurs. On peut d’ailleurs considérer à ce titre là que c’est une variation des qubits supraconducteurs. De ce fait, ces ordinateurs quantiques doivent aussi être refroidis à une température voisine du zéro absolu, aux alentours de $10mK$ . C’est la voie choisie par ***Microsoft*** (lire [cet article](https://experiences.microsoft.fr/articles/quantique/ordinateur-quantique-topologique/) par ex.). Sachant que l’existence des fermions de Majorana est à peine prouvée. Et les quasi-particules sur lesquelles planchent les équipes de Microsoft ne seraient pas des fermions de Majorana.

#### Critères de Comparaison des Technologies de qubit

Aucune des technnologies de qubits décrites ci-dessus, n’est pour l’instant opérationnelle à grande échelle. Elles ont toutes des avantages et inconvénients en particulier concernant les notions suivantes : 

* la stabilité des qubits, 
* la possibilité de les intriquer, 
* le niveau d’erreurs dans les qubits et dans les portes quantiques qui les relient, 
* la durée de cohérence des qubits, 
* la température de fonctionnement, 
* le niveau de miniaturisation, 
* le processus de fabrication (qui est moins cher lorsque c’est du silicium en technologie CMOS)

Tous ces paramètres sont importants pour disposer d'un indice de benchmark significatif, ainsi que certains autres (notamment le nombre de qubits, etc..), comme nous le verrons dans la partie ***Volume Quantique (QV)*** sur les Microprocesseurs Quantiques.

#### État d'Avancement Relatif de chaque Technologie de qubit

<center>

<figure>

<img src="../img/qubitNiveauAvancement.jpg" />

<figcaption>

État d'Avancement Relatif de chaque technologie ($2018$)
Source : [Entwicklungsstand Quantencomputer](https://www.bsi.bund.de/SharedDocs/Downloads/DE/BSI/Publikationen/Studien/Quantencomputer/P283_QC_Studie-V_1_2.pdf;jsessionid=3DF58498EE1058C552177016FFFE75FE.internet472?__blob=publicationFile&v=1)

</figcaption>

</figure>

</center>

## Registres Quantiques

### Registres quantiques de qubits

Ils sont notés : 
* registre à $1$ qubit : $|0\rangle$, $|1\rangle$, $|\psi \rangle=u_0 |0\rangle + u_1 |1\rangle = \begin{bmatrix}
  u_0 \\
  u_1 \\
  \end{bmatrix}
  $  ou 
* registre à $2$ qubits : $|00\rangle$, $|01\rangle$, $|10\rangle$, $|11\rangle$, $|\psi \rangle=u_{00} |00\rangle + u_{01} |01\rangle + u_{10} |10\rangle + u_{11} |11\rangle = \begin{bmatrix}
  u_{00} \\
  u_{01} \\
  u_{10} \\
  u_{11} \\
  \end{bmatrix}
  $
  La notation $|01\rangle$ (par ex.) veut dire que le $1^{er}$ qubit est dans l'état $|0\rangle$, et le $2^{ème}$ est dans l'état $|1\rangle$
* Un registre à $n$ qubits admet donc une représentation vectorielle avec $2^n$ nombres complexes

### Registres Quantiques & Parallélisme massif

Avec un **registre** de $n$ *qubits*, un ordinateur aurait toujours $2^n$ états. Chaque *qubit* pourrait être dans $1$ état, ou n'importe quelle **superposition quantique** arbitraire de $2$, $3$ ou plus de ces $2^n$ états par **intrication quantique**, voire dans tous en même temps : C'est ce que l'on appelle le "**parallélisme massif**" des ordinateurs quantiques, qui permet naturellement une puissance de calcul considérable, et qui est donc obtenu grâce aux deux propriétés quantiques suivantes :

* Superposition quantique
* Intrication quantique

Par exemple, pour les $53$ *qubits* de l’ordinateur de *Google* en $2019$, cela correspond à plus de $2^{53} \approx 9 \times 10^{15}$ états $|0\rangle$ et $|1\rangle$ !

## Portes Quantiques

### Principe

En **[informatique quantique](https://fr.wikipedia.org/wiki/Informatique_quantique)**, une **[porte quantique](https://fr.wikipedia.org/wiki/Porte_quantique)** ou **porte logique quantique** est un **circuit quantique élémentaire** opérant sur **un petit nombre de qubits** : Ce sont les briques de base des **[circuits quantiques](https://en.wikipedia.org/wiki/Quantum_circuit)**, comme le sont les portes logiques classiques pour des circuits numériques classiques. Contrairement à la plupart des portes logiques classiques, **les portes quantiques sont réversibles**.

Un **[circuit quantique](https://en.wikipedia.org/wiki/Quantum_circuit)** est un modèle quantique de calcul, dans lequel le calcul quantique consiste en **une sucession de portes quantiques**.

### Représentation Matricielles des Portes Quantiques

Les portes logiques quantiques sont représentées par des **[matrices unitaires](https://fr.wikipedia.org/wiki/Matrice_unitaire)** à coefficients complexes.

<env>**Notation de Born**</env> $\,$ <enc>$1$ OPÉRATION sur des qubits $= 1$ PORTE QUANTIQUE $= 1$ MATRICE</enc> $\,\,$

Plus précisément, Si la porte quantique opére/agit sur $n$ qubits en entrée, alors sa représentation est une matrice carrée unitaire de $2^n\times 2^n$ coefficients complexes. Les nombres de qubits en entrée et en sortie de la porte doivent être égaux. 

### Résultat/Sortie du calcul après une porte quantique

>**Sortie d'une porte quantique** : L'action de la porte sur un état quantique spécifique est obtenu en multipliant le vecteur qui représente l'état par la matrice qui représente la porte. 

### Exemples de Portes Quantiques Usuelles

#### Porte de Hadamard

La porte de Hadamard agit sur un seul qubit. Elle transforme :
* l'état $|0\rangle \mapsto \dfrac {|0\rangle+|1\rangle}{\sqrt 2}$ et 
* l'état $|1\rangle \mapsto \dfrac {|0\rangle-|1\rangle}{\sqrt 2}$

Ce qui signifie que la mesure aura la même probabilité de donner $1$ ou $0$, c'est-à-dire crée une superposition quantique équilibrée. Elle représente une rotation de $\pi$ sur l'axe $\dfrac {\hat x+ \hat z}{\sqrt 2}$ . De manière équivalente, c'est la combinaison de deux rotations, $\pi$ sur l'axe des X, suivie par une rotation de $\dfrac {\pi}2$ sur l'axe des ordonnées. Ce qui est représenté par la **matrice de Hadamard**:

<center>

<div style="width:50%; float:left;">

$\displaystyle H={\frac {1}{\sqrt {2}}}{\begin{bmatrix}1&1\\1&-1\end{bmatrix}}$ 

<figcaption>

Représentation Matricielle 
de la Porte de Hadamard :
crée des superpositions

</figcaption>

</div>

<div style="width:50%; float:left;">

<figure>

<img src="../img/porteHadamard.svg" style="width:50%;"/>

<figcaption>

Représentation de 
la Porte de Hadamard

</figcaption>

</figure>

</div>

</center>

<clear></clear>

La **porte de Hadamard** est une version qubit de la **transformée de Fourier quantique**.

On peut montrer que $H$ est bien une matrice unitaire...

#### Porte Pauli-X / Porte NOT ou *bit-flip* :gb:

La **porte Pauli-X** agit sur un seul qubit. Elle transforme :

* $|0\rangle \mapsto |1\rangle$ et 
* $|1\rangle \mapsto |0\rangle$

C'est pourquoi elle est parfois appelée ***bit-flip gate*** :gb: .

**C'est l'équivalent quantique de la porte NOT des ordinateurs classiques**. Elle équivaut à une rotation de $\pi$ radians autour de l'axe $X$ dans la sphère de Bloch.

Elle est représentée par la **matrice de Pauli X** notée également **$X=\sigma_1=\sigma_x$** :

<center>

<div style="width:50%; float:left;">

$\sigma_1=\sigma_x=X={\begin{bmatrix}0&1\\1&0\end{bmatrix}}$

<figcaption>

Représentation Matricielle 
de la Porte de Pauli-X
ou Porte NOT quantique
ou Porte bit-flip

</figcaption>

</div>

<div style="width:50%; float:left;">

<figure>

<img src="../img/porteNOT_PauliX.svg" />
<img src="../img/portePauliX.png" style="width:60%;"/>

<figcaption>

Représentation de 
la Porte de Pauli-X
ou Porte NOT quantique
ou Porte bit-flip

</figcaption>

</figure>

</div>

</center>

<clear></clear>

#### Porte Pauli-Y

La **porte Pauli-Y** agit sur un seul qubit. Elle transforme

* $|0\rangle \mapsto i|1\rangle$ et 
* $|1\rangle \mapsto -i|0\rangle$

Elle équivaut à une rotation de $\pi$ radians autour de l'axe $Y$ dans la sphère de Bloch.

Elle est représentée par la **matrice de Pauli $Y$ notée également $Y=\sigma_1=\sigma_y$** :

<center>

<div style="width:50%; float:left;">

$\sigma_2=\sigma_y=Y=\begin{bmatrix}
0 & -i \\
i & 0
\end{bmatrix}$

<figcaption>

Représentation Matricielle 
de la Porte de Pauli-Y

</figcaption>

</div>

<div style="width:50%; float:left;">

<figure>

<img src="../img/portePauliY.png" style="width:60%;"/>

<figcaption>

Représentation de 
la Porte de Pauli-Y

</figcaption>

</figure>

</div>

</center>

<clear></clear>

#### Porte Pauli-Z ou *phase-flip* :gb:

La **porte Pauli-Z** agit sur un seul qubit. Elle transforme

* $|0\rangle \mapsto |0\rangle$ $\,$ (inchangé) et 
* $|1\rangle \mapsto -|1\rangle$

Elle équivaut à une rotation de $\pi$ radians autour de l'axe $Z$ dans la sphère de Bloch. C'est donc un cas particulier de la porte Changement de phase (voir ci-dessous) pour $\varphi=\pi$. De ce fait, elle est parfois appelée ***phase-flip gate*** :gb:.

Elle est représentée par la **matrice de Pauli $Z$ notée également $Z=\sigma_3=\sigma_z$** :

<center>

<div style="width:50%; float:left;">

$\sigma_3=\sigma_z=Z=\begin{bmatrix}
1 & 0 \\
0 & -1
\end{bmatrix}$

<figcaption>

Représentation Matricielle 
de la Porte de Pauli-Z

</figcaption>

</div>

<div style="width:50%; float:left;">

<figure>

<img src="../img/portePauliZ.png" style="width:60%;"/>

<figcaption>

Représentation de 
la Porte de Pauli-Z

</figcaption>

</figure>

</div>

</center>

<clear></clear>

#### Portes $\varphi$ ou Portes *Changement de Phase* $\varphi$ ou *phase shift gates* :gb:

C'est une **famille de portes** opérant sur seul qubit qui transforme :

* $|0\rangle \mapsto |0\rangle$ $\,$ (inchangé) et 
* $|1\rangle \mapsto e^{i\varphi}|1\rangle$

Cette porte ne modifie pas la probabilité de mesurer un $|0\rangle$ ou $|1\rangle$ , mais **elle modifie la phase de l'état quantique**. Ceci équivaut à tracer un cercle horizontal à la latitude de $\varphi$ radians dans la sphère de Bloch.

$R_\varphi = P_\varphi = \begin{bmatrix}
1 & 0\\
0 & e^{i\varphi}
\end{bmatrix}
$ $\,$ où $\varphi$ est le décalage de phase.

Quelques exemples courants sont :

* la **porte $T$** avec $\varphi = \dfrac {\pi}4$, donc $R_{\dfrac {\pi}4} = T = \begin{bmatrix}
1 & 0\\
0 & e^{i\dfrac {\pi}4}
\end{bmatrix} = diag \left (1,e^{i\dfrac{\pi}4} \right)
$
* la **porte $S$** ou **porte de phase**,, avec $\varphi = \dfrac {\pi}2$, donc $R_{\dfrac {\pi}2} = S = \begin{bmatrix}
1 & 0\\
0 & e^{i\dfrac {\pi}2}
\end{bmatrix} = 
\begin{bmatrix}
1 & 0\\
0 & i
\end{bmatrix}
$

* la **porte Pauli-Z** avec $\varphi = \pi$

#### Portes Contrôlées (CNOT=cX, cY et cZ)

Les **portes contrôlées** :

* agissent sur $2$ qubits ou plus,
* parmi lesquels un ou plusieurs qubits agissent comme un **contrôle** pour certaines opérations. 

<env>Porte $cX$ = Porte $CNOT$</env>

Par exemple, la <bred>porte contrôlée $NOT$</bred> / <bred>controlled-$NOT$</bred> :gb: ou <bred>porte $CNOT$</bred> ou <bred>porte $cX$</bred> :

* agit sur $2$ qubits, et 
* n'effectue l'opération *NOT* sur le second qubit :
  * que lorsque le premier qubit est $|1\rangle$ et 
  * sinon le laisse inchangé. 
  
Elle est représentéee par la matrice :

<center>

<div style="width:50%; float:left;">

$$CNOT = cX = \begin{bmatrix}
1 & 0 & 0 &0 \\
0 & 1 & 0 & 0 \\
0 & 0 & 0 & 1 \\
0 & 0 & 1 & 0
\end{bmatrix}$$

<figcaption>

Représentation Matricielle 
de la Porte $CNOT$ = Porte $cX$

</figcaption>

</div>

<div style="width:50%; float:left;">

<figure>

<img src="../img/controlledNOT.svg" style="width:60%;"/>
<img src="../img/controlledX.svg" style="width:60%;"/>

<figcaption>

Représentation de 
la Porte Contrôlée-$NOT$ / Controlled-$NOT$
ou Porte $cX$

</figcaption>

</figure>

</div>

</center>

<clear></clear>

:warning: La *porte CNOT* est généralement utilisée en informatique quantique **pour générer des états intriqués**. :warning:

<env>Porte Contrôlée U</env>

Plus généralement, si $U$ est une porte qui agit sur des qubits uniques, en représentation matricielle :

$$U = \begin{bmatrix}
u_{00} & u_{01} \\
u_{10} & u_{11}
\end{bmatrix}$$

alors la **porte contrôlée-$U$** est une porte qui fonctionne sur deux qubits de telle sorte que le premier qubit sert de contrôle. Il relie les états basiques comme suit :

* $|00\rangle \mapsto |00\rangle$
* $|01\rangle \mapsto |01\rangle$
* $|10\rangle \mapsto |1\rangle \otimes U |0\rangle = |1\rangle \otimes \left( u_{00} |0\rangle +u_{10}|1\rangle \right)$
* $|11\rangle \mapsto |1\rangle \otimes U |1\rangle = |1\rangle \otimes \left( u_{01} |0\rangle +u_{11}|1\rangle \right)$

<center>

<div style="width:50%; float:left;">

$$C(U) = \begin{bmatrix}
1 & 0 & 0 & 0 \\
0 & 1 & 0 & 0 \\
0 & 0 & u_{00} & u_{01} \\
0 & 0 & u_{10} &u_{11}
\end{bmatrix}$$

<figcaption>

Représentation Matricielle 
de la Porte Contrôlée-$U$

</figcaption>

</div>

<div style="width:50%; float:left;">

<figure>

<img src="../img/controlledU.svg" style="width:60%;"/>

<figcaption>

Représentation de 
la Porte Contrôlée-$U$

</figcaption>

</figure>

</div>

</center>

<clear></clear>

<env>Portes ($cX$), $cY$ et $cZ$</env>

Lorsque $U$ est l'une des **matrices de Pauli**, $\sigma_x$, $\sigma_y$ ou $\sigma_z$, les termes respectifs "***contrôlée-$X$***", "***contrôlée-$Y$***", ou "***contrôlée-$Z$***" sont parfois utilisés. 

<center>

<div style="width:50%; float:left;">

$$cY = \begin{bmatrix}
1 & 0 & 0 &0 \\
0 & 1 & 0 & 0 \\
0 & 0 & 0 & -i \\
0 & 0 & i & 0
\end{bmatrix}$$

<figcaption>

Représentation Matricielle 
de la Porte $cY$

</figcaption>

</div>

<div style="width:50%; float:left;">

<figure>

<img src="../img/controlledY.svg" style="width:60%;"/>

<figcaption>

Représentation de 
la Porte $cY$

</figcaption>

</figure>

</div>

</center>

<center>

<div style="width:50%; float:left;">

$$cZ = \begin{bmatrix}
1 & 0 & 0 &0 \\
0 & 1 & 0 & 0 \\
0 & 0 & 1 & 0 \\
0 & 0 & 0 & -1
\end{bmatrix}$$

<figcaption>

Représentation Matricielle 
de la Porte $cZ$

</figcaption>

</div>

<div style="width:50%; float:left;">

<figure>

<img src="../img/controlledZ.svg" style="width:60%;"/>

<figcaption>

Représentation de 
la Porte $cZ$

</figcaption>

</figure>

</div>

</center>

<clear></clear>

#### Porte Deutsch $D_\theta$

La porte Deutsch, ou $D_\theta$ (du nom du physicien anglais **[David Deutsch](https://en.wikipedia.org/wiki/David_Deutsch)**) est une porte à $3$ qubits. Elle est définie par :

$| a , b , c \rangle \mapsto \bigg \{ \begin{matrix}
icos( \theta ) | a , b , c \rangle + sin ⁡( \theta ) | a , b , 1 − c \rangle & \text{pour} \,\, a=b=1 \\
|a,b,c\rangle & \text{sinon}
\end{matrix}$

La construction d'une porte de Deutsch est malheureusement restée hors d'atteinte pour le moment, pour cause d'absence de protocole. Certains proopositions existent pour fabriquer une telle porte à partir d'interactions dipole-dipole dans des atomes neutres.

#### Résumé des principales portes Quantiques

<center>

<img src="../img/resumePortesQuantiques.png" style="width:70%;">

</center>

### Portes Quantiques Universelles

De manière informelle, un ensemble de **Portes Quantiques Universelles** est un ensemble de portes qui permet de représenter toute opération possible sur un ordinateur quantique, c'est-à-dire que toute opération unitaire peut être exprimée comme une séquence finie de portes de cet ensemble.

Un ensemble *simple* de portes quantiques universelles à deux qubits est constitué de : 
* la porte de Hadamard $H$ , 
* la porte $\dfrac {\pi}8$ ou porte $T$ : $\quad \dfrac {\pi}8 = T=R_{\left(\dfrac {\pi}4\right)} = diag\left( 1,e^{\dfrac {i\pi}4}\right)$ et 
* la porte CNOT (=contrôlée-NOT=cX).

La porte logique classique universelle, **la porte de Toffoli**, est réductible à la **porte Deutsch**, $D \left( \dfrac {\pi}2 \right)$ , montrant ainsi :

"*Toutes les opérations logiques classiques peuvent être effectuées sur un ordinateur quantique universel*"

### Fiabilité des Portes Quantiques

Pour le moment, on est capable de fabriquer des portes quantiques fiables à $99\%$ , mais pour réellement construire un ordinateur quantique, i lfaudrait une fiabilité de $99,99\%$ afin que les méthodes des Codes (Correcteurs) d'Erreurs Quantiques puissent opérer correctement.


### Aller Plus Loin (dans les Portes Quantiques)

* [Portes Quantiques](https://fr.wikipedia.org/wiki/Porte_quantique), Wikipedia :fr:
* [Quantum Logic Gates](https://en.wikipedia.org/wiki/Quantum_logic_gate) Wikipedia en anglais :gb:
* [Introduction au Calcul Quantique](https://www.college-de-france.fr/media/serge-haroche/UPL55030_SHaroche_190202.pdf), Serge Haroche, Collège de France


## Dispositifs de mesure (Lecture/Écriture) de l'état des qubits

<div style="width:30%; margin-right: 10px; float:left;">

<center>

<figure>

<img src="../img/mesureQuantique.svg" />

<figcaption>

Représentation d'une mesure quantique

</figcaption>

</figure>

</center>

</div>

Ils permettent d’obtenir le résultat des calculs à la fin du processus d’exécution séquentielle des *portes quantiques*. Dans certains types d’ordinateurs quantiques, comme les systèmes *à recuit quantique* de *D-Wave*, on peut appliquer ce cycle d’initialisation, de calculs et de mesure plusieurs fois pour évaluer plusieurs fois le résultat. On obtient alors par moyenne une valeur comprise entre $0$ et $1$ pour chaque qubit des registres de l’ordinateur quantique. Les valeurs lues par les dispositifs physiques de lecture sont ensuite converties en valeurs numériques et transmises à l’ordinateur classique qui pilote l’ensemble et permet l’interprétation des résultats. Les dispositifs de lecture sont reliés à leur électronique de contrôle via des fils supraconducteurs dans le cas des qubits d’ordinateurs supraconducteurs.

## Microprocesseurs / Chipsets Quantiques

### Plusieurs Technologies de Microprocesseurs Quantiques

Le tableau ci-dessous résume et compare différentes technologies de qubits/registres quantiques/portes quantiques/dispositifs de lecture&écriture d'états quantiques/microprocesseurs quantiques :

<center>

|  | <img src="../img/compQubitRecuitSimule.jpg"><br/>recuit<br/> quantique | <img src="../img/compQubitBouclesSupraconductrices.jpg"><br/>boucles supra-<br/>conductrices | <img src="../img/compQubitTopologique.jpg"><br/>qubits<br/>topologiques | <img src="../img/compQubitOptiqueLineaire.jpg"><br/>Optique<br/>linéaire | <img src="../img/compQubitQuantumDotsSilicium.jpg"><br/>Quantum dots<br/>Silicium | <img src="../img/compQubitIonsPieges.jpg"><br/>Ions piégés | <img src="../img/compQubitCavitesDiamants.jpg"><br/>Cavités<br/>Diamants |
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
| qubit| supraconducteur<br/>effet Josephson | supraconducteur<br/>effet Josephson | quasi-particules<br/>paire d'anyons | photons | spins d'électrons<br/>dans<br/>semi-conducteur | ions piégés |spin de<br/> noyau<br/>d'atome |
| # qubits | $2048$ | $50$ qubits (IBM)<br/>$54$ qubits (Google) | N/A | $66$ (USTC) | $49$ (Intel) | $53$ (IonQ)<br/>$51$ (MIT)<br/>$20$ (IQOQI)<br/> | $6$ qubits (QDTI) |
| état | sens du<br/>courant | phase de<br/>résonance ou<br/>sens du courant | sens du<br/>l'anyon | phase de<br/>photon | spins<br/>d'électrons | niveau<br/>énergétique<br/>de l'ion piégé | niveau<br/>d'énergie<br/>de la cavité |
| portes | micro-ondes<br/>$5\,GHz$ et<br/>effet<br/>Josephson | micro-ondes<br/>$5\,GHz$ et<br/>effet<br/>Josephson | Inversions 2D<br/>d'anyons | filtres polarisants<br/>et dichroïques | micro-ondes | laser | laser |
| mesure<br/>/lecture<br/>/écriture | magnétomètre | magnétomètre | fusion d'anyons | détecteur<br/>de photons | consersion<br/>spins to<br/>charge | fluorescence | fluorescence |
| Avantages | --- | --- | --- | Température<br/>ambiante | --- | <ul><li>Record de<br/>temps de<br/>Cohérence<br/>(plusieurs<br/>minutes)</li><li>Plus simples<br/>à intriquer</li></ul> | --- |
| Inconvénients | <ul><li>plus grande<br/>décohérence<br/>que les<br/> ions piégés</li><li>Température<br/>Zéro absolu</li><li>PAS un<br/>calculateur<br/>quantique<br/>général :<br/>limité aux<br/> calculs de<br/> <a href="https://fr.wikipedia.org/wiki/Recuit_simul%C3%A9">recuit simulé</a><br/>(optimisation)</li></ul> | <ul><li>plus grande<br/>décohérence<br/>que les<br/> ions piégés</li><li>Température<br/>Zéro absolu</li></ul> | <ul><li>plus grande<br/>décohérence<br/>que les<br/> ions piégés</li><li>Température<br/>Zéro absolu</li></ul> | <ul><li>plus grande<br/>décohérence<br/>que les<br/> ions piégés ??</li><li>XXXX</li></ul> | --- | --- | --- |

<figcaption>

Comparaison des Technologies de Microprocesseur Quantique ($2018$)

</figcaption>

</center>

<clear></clear>

### Architectures de Microprocesseurs Quantiques

<div style="width:50%; margin-right: 10px; float:right;">

<center>

<figure>

<img src="../img/archiOrdiQuantique.jpg" />

<figcaption>

Microprocesseur Quantique cryogénisé
& son Interface avec un ordi classique

</figcaption>

</figure>

</center>

</div>

Le **Chipset / Microprocesseur quantique** comprend :

* les **registres quantiques**, 
* les **portes quantiques** et 
* les **dispositifs physiques de mesure de l'état des qubits**, lorsqu'ils sont du type *supraconducteurs* ou *quantum dots*. Pour les dispositifs utilisant les *lasers* et *photons* pour l’initialisation, les portes quantiques et la mesure des qubits : ces dispositifs sont plus hétérogènes.

Les chipsets actuels ne sont pas très grands. Ils font la taille d’un capteur photo full-frame ou double-format pour les plus grands d’entre eux. Chaque qubit est relativement grand, leur taille se mesurant en microns alors que les transistors de processeurs modernes en CMOS ont des tailles maintenant inférieures à $20$ nanomètres.

### Un indice de Benchmark : le Volume Quantique (QV)

D'une part, on ne peut plus utiliser le classique **[test LINPACK](https://fr.wikipedia.org/wiki/LINPACK)** ou **[LAPACK](https://fr.wikipedia.org/wiki/LAPACK)** des supercalculateurs classiques.
D'autre part, la diversité des technologies de qubits quantiques a rapidement montré le besoin d'un indice de benchmark fiable : Le <bred>Volume Quantique</bred> ou <bred>QV</bred> pour **Quantum Volume**, est un **indice de performance quantique**, proposé par *IBM* et la *NASA* pour mieux comparer ce qui est comparable.., qui tient compte de plusieurs critères :

* nombre de qubits simultanément utilisables, 
* temps de cohérence, 
* taux d’erreurs de portes, 
* taux d’erreurs de mesures, 
* efficacité/qualité de la compilation des circuits, 
* longueur des circuits quantiques, 
* débit de connectivité, etc.

## "*Ordinateurs*" Quantiques

### Enceinte Cryogénisée

Selon la technologie de qubits utilisée (en particulier, pas les calculateurs photoniques), une **enceinte cryogénisée** maintient l’intérieur de l’ordinateur à une **température voisine du zéro absolu** ($-273,15°C$). Elle contient une partie de l’**électronique de commande (lecture et écriture)** et le ou les **chipsets quantiques** pour éviter de générer des perturbations empêchant les qubits de fonctionner, notamment au niveau de leur **intrication** et **cohérence** ainsi que pour **réduire le bruit de leur fonctionnement**. Le Graal serait de pouvoir faire fonctionner des qubits à température ambiante, mais les architectures correspondantes e sont pas encore opérationnelles, comme par exemple dans les **NV Vacancy**, ou les **cavités de diamants**.

### L'état de l'art des Calculateurs Quantiques

* Pour le moment, on dispose seulement de **Calculateurs Quantiques « Bruités »**, de tailles intermédiaires, appelés ***«NISQ*** » pour « ***Noisy Intermediate Scale Quantum*** », qui sont composés de **quelques centaines de qubits physiques** permettant de réaliser un certain nombre de calculs (plus ou moins) spécifiques.
C'est ce type de calculateurs qui a vu le jour, il y a déjà quelques années, dans des machines développées, notamment, par ***IBM***, ***Google***, l'***USTC***, ***IonQ*** et ***Rigetti*** avec quelques dizaines de qubits. Ce ne sont **même pas des calculateurs universels**, et **encore moins des ordinateurs quantiques universels** :

  * Ils sont de **taille intermédiaire** (centaine de qubits physiques)
  * Ils ont des qubits physiques **bruyants**
  * Les quelques **qubits logiques existants ne sont pas (réellement) scalables**
  * avec beaucoup de **problèmes rencontrés dans la Correction d'Erreur Quantique (QEC)**
  * des **Systèmes d'Exploitation Quantiques tout juste balbultiants**
  * Ils sont encore **Interfacés avec des Systèmes d'Exploitation Classiques**
  * Aucun n'est équivalent à une **Machine de Turing Quantique universelle**

* Par opposition, ce que l'on souhaiterait créer, ce sont des **Calculateurs Quantiques « Universels »** de puissance significative appelés « ***LSQ*** » pour « ***Large Scale Quantum*** » qui seraient composés de **milliers de qubits logiques** et aurait la possibilité de réaliser tout type de calcul quantique. Ce type de calculateur surpasserait de manière exponentielle le plus puissant des supercalculateurs actuels pour un grand nombre d’applications. Bien que fréquemment présentés comme tels par certains fabricants actuels, les premiers calculateurs « ***LSQ*** » ne sont pas attendus avant $2030$ : méfiance donc!

Aller Plus Loin :

* [Architecture Ordis Quantiques Supraconducteurs](https://www.physique.usherbrooke.ca/tremblay/theses/Algorithmes_et_architectures_16dec2002_Blais.pdf), $2002$

## Erreurs quantiques & *qubits Logiques*

### Canal de Communication Quantique

De même que dans le cas classique un code correcteur quantique utilise une procédure de codage/transmission imparfaite/décodage afin de réduire l’erreur par rapport à la même transmission sans codage au prix d’un accroissement de la taille (dans le cas quantique *la dimension*) des messages envoyés.
Envoyer l'état d'un qubit via un canal revient à envoyer deux nombres complexes $\alpha$ et $\beta$ : il existe donc beaucoup plus d'erreurs possibles dans le cas quantique.

### Erreurs Quantiques

Dans un **ordinateur quantique** :
* **Espace Continu** : Pour chaque qubit, il existe une **infinité** d'états possibles $|0\rangle$, $|1\rangle$ et/ou $|\psi \rangle = \alpha |0\rangle + \beta|1\rangle$ : on parle d'un **Espace Continu**.
* **Erreurs Continues** : Les erreurs quantiques possibles forment donc un **continuum d'erreurs** : 
  * non seulement $|0\rangle$ et $|1\rangle$ doivent être protégés, mais aussi toute superposition $|\psi \rangle=\alpha |0\rangle + \beta |1\rangle$
  * Déterminer précisément quelle erreur a eu lieu revient donc à désigner une possibilité parmi une infinité. Donc il faudrait une mesure avec une précision infinie et, par conséquent, des ressources infinies. Ceci rend la tâche d'élaborer un code quantique (QEC) techniquement difficile par rapport aux codes classiques (ECC).

Des **erreurs quantiques** peuvent apparaître dans les **informations** :

* ou bien transmises via un **canal de communication "*peu fiable*"**, en sachant qu'envoyer un qubit revient à envoyer deux nombres complexes $\alpha$ et $\beta$ correspondant à son état : il existe donc beaucoup plus d'erreurs possibles que dans le cas classique. En outre, Les données circulant sur ce canal de communication (radio, câble coaxial, fibre optique, etc..) sont susceptibles d'être altérées, par exemple :
  * avec des **parasites sur la ligne** sur un canal *radio*
  * avec du **bruit aléatoire** (perturbations électromagnétiques, imperfection de la fibre optique, etc..) sur un canal *câble coaxial* ou *fibre optique*, etc.. 
* ou bien stockées sur **un support de stockage** :
  * **registres quantiques** : interactions avec l'environnement (perturbations électromagnétiques, erreur matérielle, etc..)
  * ou d'autres applications où la garantie de l'**intégrité des données** est importante

En pratique, les erreurs quantiques sont bien plus fréquentes que ne le sont les erreurs classique.

### Corriger les Erreurs Quantiques

#### Pourquoi est-ce important ?

Pour qu’un ordinateur quantique fonctionne, il faut que ses *qubits* conservent leurs propriétés quantiques (notamment : intrication et superposition) le temps du calcul. Hors, tout pousse un système quantique à perdre ses propriétés. Et cela est d’autant plus vrai que le système contient plus de *qubits* : **La décohérence affecte l'état de superposition et perturbe le traitement quantique de l'information**. Cela conduit à des ***erreurs*** dans les systèmes de calculs quantiques, qu'il faut corriger grâce à la théorie des <bred>Codes (Correcteurs) d'Erreurs Quantiques (QEC)</bred>, qui sont des algorithmes quantiques qui **détectent** et **corrigent** des erreurs quantiques.

Alors qu’un ordinateur classique se révèle très fiable, la simple lecture d'un qubit peut provoquer des erreurs. Un ordinateur quantique ferait actuellement :

* $1$ erreur toutes les $200$ opérations (taux d'erreur de $0,5\%$ ), voire $1$ erreur toutes les $1\,000$ opérations (taux d'erreur de $0,1\%$ ) pour les meilleurs d'entre eux
* Hors, les chercheurs ont montré qu'il faudrait idéalement que le **taux d'erreur** soit de $0,01\%$ (soit $1$ erreur toutes les $10\,000$ opérations) à $0,0001\%$ (soit $1$ erreur toutes les $1\,000\,000$ opérations) pour mettre en oeuvre de telles procédures de **détection** et **correction** d’erreurs quantiques pour pallier les effets de la décohérence.

Quoi qu’il en soit, il faudrait des centaines de millions de qubits connectés de manière cohérente pour avoir un ordinateur quantique universel.

<env>**Théorème du Seuil / des calculs tolérantes aux fautes**</env> : “*Si l’erreur pour effectuer chaque porte quantique est une constante suffisamment petite, alors on peut effectuer des calculs quantiques arbitrairement longs avec une précision arbitrairement bonne*”, avec seulement un léger surcoût supplémentaire dans le nombre de portes.

En pratique, Les chercheurs ont montré que, si l’on parvenait à diminuer le taux d’erreur à environ une erreur toutes les $10\,000$ opérations, soit un taux d'erreurs de $0,0001\%$ , alors il serait possible de mettre en oeuvre de telles procédures de **détection** et **correction** d’erreur pour pallier les effets de la décohérence.

Pour surmonter les erreurs quantiques, plusieurs pistes sont explorées par les fabricants, qui dépendent aussi des technologies choisies.

#### Améliorer matériellement/physiquement la qualité des qubits (physiques)

Bien que forcément intéressante et importante, dans l'absolu, cette quête technique **ne permet néanmoins pas de détecter ni de corriger les erreurs quantiques**, car elle revient à (tenter d'éviter) qu'elles ne se produisent, en quelque sorte, ce qui une qualité mais c'est insuffisant, c'est pourquoi :

* elle est un peu en désuétude dernièrement (les améliorations purement techniques  étant stagnantes), et 
* toutes les autres techniques utilisent la théorie des **Codes (Correcteurs) d'Erreurs Quantiques (QEC)**.

#### Codes (Correcteurs) d'Erreurs Quantiques (QEC)

<env>Des difficultés théoriques et techniques supplémentaires</env>

Dans un **ordinateur quantique**, certains résultats théoriques de physique quantique compliquent, voire empêchent, l'adaptation de la théorie classique des Codes Correcteurs d'Erreur (ECC) au cas quantique :

* Chaque qubit est beaucoup plus sensible aux perturbations extérieures  (**décohérence quantique**) qu'un bit classique : **c'est le problème principal à résoudre**
* L'espace des erreurs quantiques est un **continuum d'erreurs quantiques**, comparé à l'Espace discret des erreurs sur les bits classiques
* **Toute tentative de mesure (comprendre *lecture*) d'un qubit modifie la valeur du qubit**
* **[No-cloning theorem](https://en.wikipedia.org/wiki/No-cloning_theorem) :gb: / [Impossiblité du Clonage Quantique](https://fr.wikipedia.org/wiki/Impossibilit%C3%A9_du_clonage_quantique)** :fr: (entre $1970$, et $1982$): "**Il est impossible de copier à l'identique, de manière indépendante, un état quantique d'un qubit quelconque (sans l'altérer de manière irréversible)**". Ce théorème a d'importantes conséquences en informatique quantique, par exemple :
  * Il est impossible de créer des copies de sauvegarde (backup) d'un état quantique en plein milieu d'un calcul quantique, dans l'espoir de corriger de futures erreurs quantiques de calcul.
  * Il justifie l'impossibilité d'adapter certains codes classiques connus en des codes quantiques : par ex. Il est impossible d'utiliser un **code *à répétition* quantique** (QEC), qui transforme directement $|\psi\rangle$ en $|\psi\rangle|\psi\rangle|\psi\rangle$, en s'inspirant d'un **code *à répétition*** de la théorie des codes classiques (ECC). Ceci rend la tâche d'élaborer un code quantique difficile par rapport aux codes classiques.
  
  :warning: Le ***No-cloning theorem*** semble présenter un obstacle infranchissable à la Théorie des Codes (Correcteurs) d'Erreurs Quantiques (QEC), **néanmoins il n'en est rien** : les chercheurs sont bien parvenus à détecter et corriger des erreurs quantiques :warning:
* Les **portes quantiques** peuvent aussi commettre des erreurs. Lorsque les qubits sont manipulés dans un **[circuit quantique](https://en.wikipedia.org/wiki/Quantum_circuit)** (série de **portes quantiques**), il faut donc également tenir compte des erreurs dues aux portes. On peut, de façon équivalente, considérer que celles-ci sont *parfaites*, mais suivies d’erreurs sur les qubits après action des portes, à corriger en suivant les mêmes principes (QEC)
* etc..

<env>Quelques Exemples de QEC</env>

* Historiquement, en $1995$, c'est **Peter Shor** qui, le premier, a montré la possibilité de diffuser/encoder l'information contenue dans un qubit physique, dans plusieurs qubits (physiques) intriqués.
* Toujours en $1995$, ce résultat lui permet d'inventer **le premier Code Correcteur d'Erreur Quantique (QEC)** dit **Code de Shor** : $1$ qubit (physique) est **diffusé/encodé**/stocké dans (un **qubit logique** composé de) une intrication de $9$ qubits (physiques).
* [Codes Bosoniques](https://fr.ert.wiki/wiki/Quantum_error_correction#Bosonic_codes)
* Les **Codes (quantiques) CSS** (pour **C**alderbank/**S**hor/**S**teane) généralisent le Code de Shor, eux mêmes remplacés par ...
* Les ***[Codes Stabilisateurs](https://fr.wikipedia.org/wiki/Code_stabilisateur)*** $(n,k)$ qui sont des codes quantiques autocorrecteurs qui protègent $k$ qubits (logiques) en les encodant dans $n$ qubits (physiques), avec nécessairement $n \gt k$. Le code de Shor peut donc être vu comme un code stabilisateur $(9,1)$. Les meilleurs codes stabilisateurs actuels sont des codes $(5,1)$ : $1$ qubit logique encode $5$ qubits physiques. De plus, la théorie a montré que, pour un seul qubit, on ne pouvait pas faire mieux, et l'on sait construire explicitement de tels codes, qui présentent l'avantage supplémentaire d'être **tolérants aux pannes**. Par contre, on pense qu'en juxtaposant plusieurs qubits, il serait possible de faire mieux globalement.
* [Code de Kitaev](https://fr.wikipedia.org/wiki/Code_de_Kitaev) qui est un code quantique ***topologique***, pour un ordinateur quantique ***topologique***
* etc... la recherche est active et actuelle/changeante...

#### Créer des <bred>qubits logiques</bred>

$1$ <bred>qubit logique</bred> est un ensemble **intriqué** de **plusieurs qubits physiques**. 
Plus précisément, on **encode** par **intrication quantique** un **qubit logique** contenant :

* des **qubits (physiques) de données** 
* des **qubits (physiques) de mesure** (comprendre : des **qubits de contrôle**) : ce sont ces qubits de mesure redondants qui servent à **détecter** et **corriger** des erreurs quantiques, en utilisant la théorie des **Codes (Correcteurs) d'Erreurs Quantiques (QEC)**.

C'est la voie qu'ont choisi de nombreux fabricants : Google, IonQ, Rigetti, ...

#### Créer des <bred>*ordinateurs quantiques topologiques*</bred>

La <bred>topologie</bred> est la partie des mathématiques qui s'intéresse aux **formes et leurs propriétés, lorsque celles-ci subissent des déformations continues telles la torsion, l’étirement ou la compression**.

La technologie d'un **ordinateur quantique topologique** est une sorte de pari fou fait par **Microsoft** (cf. [cet article](https://experiences.microsoft.fr/articles/quantique/ordinateur-quantique-topologique/) ) il y a environ $20$ ans : elle est basée sur l'existence supposée de **quasi-particules** que personne n'avait jamais vue, et dont on n'a finalement eu la preuve expérimentale de leur existence que $12$ ans plus tard... :

* les **fermions de Majorana** (en 3D) mais dont l'utilisation technologique est prévue en 2D seulement, ce qui est important pour la suite, c'est pourquoi elles sont appelées 
* des **anyons de Majorana** (en 2D), qui présentent les caractéristiques suivantes :
  * Informations dans des "<bred>*qubits toppologiques*</bred>" : En fait, l’information n’est pas encodée dans des qubits locaux, mais dans l’espace 2D de fusion d’un ensemble de **paires d'anyons de Majorana**
  * Calculs quantiques : Les anyons ont un comportement analogue à celui des particules mais surtout **ils ont les propriétés mathématiques requises pour former les structures topologiques en 2D (c'est important) dont on a besoin pour réaliser des calculs quantiques : à savoir "tresser des « brins » en 2D" ou "faire des tresses en 2D"** (à l'instar des *quipus* des *Mayas*...). En fait, ces brins ne sont pas matériels, puisqu’il s’agit des lignes d’univers des paires d'anyons, c’est‑à-dire de leurs « trajectoires » dans l’espace-temps.

La preuve expérimentale de l'existence des fermions/anyons de Majorana, faite d'abord par Microsoft, puis confirmée par le MIT, est très récente : $2020-2021$ .

Ces **qubits topologiques** auraient plusieurs avantages :

<center>

<figure>

<img src="../img/ordinateurTopologique.jpg" />

<figcaption>

ordinateur Topologique, source: Microsoft

</figcaption>

</figure>

</center>

Une conséquence importante de leur durée de vie allongée, serait qu'ils sont bien plus autocorrectifs, rendant ainsi les qubits physiques bien moins sensibles aux erreurs. C'est la voie choisie par ***Microsoft*** (Cf. [cet article](https://experiences.microsoft.fr/articles/quantique/ordinateur-quantique-topologique/) par ex.). De plus, Les quasi-particules sur lesquelles planchent les équipes de Microsoft ne seraient pas des fermions de Majorana. On est néanmoins sans aucune nouvelle de cette technologie depuis $2018$ .... :cry:

#### Aller Plus Loin (dans les Codes (Correcteurs) d'Erreurs Quantiques)

* [Code Quantique](https://fr.wikipedia.org/wiki/Code_quantique), Wikipedia
* [Quantum Error Correction](https://en.wikipedia.org/wiki/Quantum_error_correction), Wikipedia en anglais :gb:
* [Une intro. aux codes correcteurs quantiques](https://www.math.u-bordeaux.fr/~gzemor/Tillich.pdf), J.P. Tillich, INRIA, $2008$
* [Correction d'Erreurs Quantiques](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&ved=2ahUKEwjJ5biou-rxAhXKDWMBHRgzDZcQFjACegQIDxAD&url=http%3A%2F%2Fwww.math.ens.fr%2Fenseignement%2Ftelecharger_fichier.php%3Ffichier%3D1192&usg=AOvVaw0J8GlOO9ly-QfRFf9DXwCV), ENS
* [Informatique Quantique, Correction d'Erreurs](http://www.iro.umontreal.ca/~tappa/pages/cours/IFT6155/qecc.pdf), Univ. de Montreal
* [Microsoft et l'ordinateur quantique Topologique](https://experiences.microsoft.fr/articles/quantique/ordinateur-quantique-topologique/)
* [Décodeurs rapides pour codes topologiques quantiques](https://epiq.physique.usherbrooke.ca/pdf/GDC_Mait_These.pdf), par Guillaume Duclos-Cianci, Canada $2010$
* [Topological qubits arriving in $2018?$](https://quantumfrontiers.com/2017/08/16/topological-qubits-arriving-in-2018/)
* [Les Codes Correcteurs d'Erreurs Quantiques](https://www.college-de-france.fr/media/serge-haroche/UPL50700_SHaroche_23112004.pdf), article du Collège de France, $2004-2005$
* [AWS explore la correction d'erreur en informatique quantique](https://www.lemondeinformatique.fr/actualites/lire-aws-explore-la-correction-d-erreur-en-informatique-quantique-82597.html)

## Algorithmes Quantiques & Applications

### Algorithmes Quantiques

<center>
<figure>
<img src="../img/algorithmesQuantiques.jpg">
<figcaption>

Historique des principaux Algorithmes Quantiques

</figcaption>
</figure>
</center>

### Domaines d'Applications

* Intelligence Artificielle
* Cryptographie & CyberSécurité
* Finance
* Modélisation moléculaire
* Santé
* Météorologie
* Logistique



Aller Plus Loin :

* [Comprendre l'informatique Quantique, Algorithmes &Applications](https://www.frenchweb.fr/comprendre-linformatique-quantique-algorithmes-et-applications/332659), FrenchWeb
* [Modèles de Calculs Quantiques](https://members.loria.fr/SPerdrix/wp-content/blogs.dir/110/files/sites/110/2017/03/chapitre-InfoQ.pdf), INRIA
* [Domaines Informatique Quantique](https://www.inria.fr/fr/domaines-informatique-quantique), INRIA, $2020$

## Repères Historiques

### $1900-1927$ : Les bases théoriques de la Physique Quantique

Un certain nombre de faits expérimentaux connus à la fin du $XIX$è siècle étaient inexplicables dans le cadre de la théorie/physique classique, parmi lesquels : "*le rayonnement du corps noir*", "*l'effet photoélectrique*", "*la stabilité des atomes*" ou "*l'existence de raies spectrales*". Ces faits expérimentaux discordants ont conduit progressivement, entre $1900$ et $1927$, une dizaine de physiciens européens à proposer une nouvelle vision du monde : la **mécanique quantique** qui étudie et décrit les mouvements des objets à l'échelle **atomique** et **(des particules) sub-atomiques**, et plus généralement la **physique quantique** (ce nom n'apparaîtra qu'en $1931$, au *Planck Institut*).

* $1900$ : **[Max Planck](https://fr.wikipedia.org/wiki/Max_Planck)** détermine la *"loi de répartition spectrale du rayonnement thermique du corps noir*". Pour résoudre ce problème, il fait l'hypothèse révolutionnaire que "*l'énergie des atomes ne peut s'échanger que par multiples d'une quantité particulière*" : ***la constante de Planck*** $h\approx 6,626 \times 10^{-34} J\cdot s$, sans en maîtriser l'interprétation physique: la **théorie des quanta**, sorte de pont provisoire entre la physique classique et la future physique quantique, est née. Son étude sur la *théorie des quanta* lui vaudra **le prix nobel de Physique** en $1918$.
* $1905$ : **[Albert Einstein](https://fr.wikipedia.org/wiki/Albert_Einstein)** étudie l'**effet photo-électrique** : "un métal qui reçoit de la lumière, émet des électrons, donc provoque de l'électricité". Il en propose une explication "*simple*" : Le **rayonnement électromagnétique** (lumière) doit également être **quantifié**, càd formé de **petits paquets** appelés **quanta**, et ce en suivant les idées de **Planck** (qu'il avait initialement critiqué). Chaque **quantum de lumière**, appelé **photon** (en $1926$), est porteur d'un *quantum d'énergie* $E=h\nu$ où $h$ désigne une nouvelle constante universelle, aujourd'hui appellée **la constante de Planck**... : C'est le premier à établir, **pour un photon, l'équivalence entre la masse et l'énergie**, ce qui revient au **principe de dualité onde-corpuscule pour les photons**. Pour son étude de l'effet photoélectrique, il reçoit le **titre de Docteur**, ainsi que le **prix nobel de Physique** en $1921$
* $1912$ : **[Henri Poincaré](https://fr.wikipedia.org/wiki/Henri_Poincar%C3%A9)** publie son article *Sur la théorie des quanta*
* $1913$ : **[Niels Bohr](https://fr.wikipedia.org/wiki/Niels_Bohr)** explique les *raies spectrales de l'atome d'hydrogène* en utilisant de nouveau la **quantification** introduite par Planck.
* $1923$ : **[Louis de Broglie](https://fr.wikipedia.org/wiki/Louis_de_Broglie)** découvre "*que tous les atomes dégagent une onde*", étandant le **principe de dualité onde-corpuscule** à toute la matière (plus seulement les photons). posant ainsi les bases de la mécanique ondulatoire à l’origine de la physique quantique : Il y a équivalence entre masse et énergie pour les électrons. Cette découverte lui vaut le prix nobel de Physique en $1929$ à $36$ ans.
* $1925$ : 
  * **[Wolfgang Pauli](https://fr.wikipedia.org/wiki/Wolfgang_Pauli)** formule le **[principe d'exclusion](https://fr.wikipedia.org/wiki/Principe_d%27exclusion_de_Pauli)** stipulant que : "**Un état quantique donné ne pouvant être occupé que par au plus un électron**". Ce principe sera généralisé plus tard à tout **fermion** (spin demi-entier), qui sont soit des **[particules élémentaires](https://fr.wikipedia.org/wiki/Particule_%C3%A9l%C3%A9mentaire)** (*électron*, *neutrino*, *quarks*, ..), soit des **[particules composites](https://fr.wikipedia.org/wiki/Particule_composite)** (*proton*), ou toutes leurs **[antiparticules](https://fr.wikipedia.org/wiki/Antiparticule)**. Contrairement aux **[bosons](https://fr.wikipedia.org/wiki/Boson)** (spin entier), qui sont des **[particules élémentaires](https://fr.wikipedia.org/wiki/Particule_%C3%A9l%C3%A9mentaire)** (photons, gluons, boson Z & W, boson de Higgs, graviton, ...) ou des **[particules composites](https://fr.wikipedia.org/wiki/Particule_composite)** (mésons, ..) et quelques quasi-particules
  * **[Erwin Schrödinger](https://fr.wikipedia.org/wiki/Erwin_Schr%C3%B6dinger)** (Autriche, $1887-1961$) développe l'équation fondamentale, dite « **[Équation de Schrödinger](https://fr.wikipedia.org/wiki/%C3%89quation_de_Schr%C3%B6dinger)** », qui décrit **l'évolution dans le temps d'une particule massive non relativiste** : Ainsi, elle **définit les états stables autorisés pour un système quantique, et décrit comment les états quantiques changent dans le temps**. L'onde elle-même est décrite par une fonction mathématique connue sous le nom de « **fonction d'onde** ». Schrödinger a expliqué que la fonction d'onde permettait de prédire la probabilité des résultats des mesures. Pour cette équation, qui fonde à elle toute seule la **mécanique quantique**, il reçoit **le prix nobel de Physique** en $1933$ ainsi qu'un autre père de la mécanique quantique **[Paul Dirac](https://fr.wikipedia.org/wiki/Paul_Dirac)**
  * naissance de la **mécanique quantique** 
* $1926$ : **[Max Born](https://fr.wikipedia.org/wiki/Max_Born)** propose un modèle probabiliste de la fonction d'onde d'une particule et quasi-particule subatomique
* $1927$ : **[Werner Heisenberg](https://fr.wikipedia.org/wiki/Werner_Heisenberg)** (Allemagne, $1901-1976$) énonce son **principe d'incertitude**, ou **principe d'indétermination** qui stipule qu' "*il existe une limite fondamentale à la précision avec laquelle il est possible de connaître simultanément deux propriétés physiques d'une même particule*". C'est néanmoins [Earle Hesse Kennard](https://fr.wikipedia.org/wiki/Earle_Hesse_Kennard) (USA, $1885-1968$), qui, la même année, établit l'inégalité précise suivante dite de Heisenberg.., en notant $m$ la masse d'une particule,  $\sigma_x$ l'écart-type (intuitivement *l'incertitude*) de la position, et $\sigma_v$ l'écart-type de la vitesse, et $h$ la constante de *Planck* alors 

<center>

<enc>

$\displaystyle \sigma_x \times \sigma_v \ge \frac{h}{4\pi m}$

</enc>

</center>

### $1980's$ : La Révolution quantique

C'ets au cours des années $1980's$ que certains physiciens eurent l'idée de créer des ordinateurs quantiques :

* $1982$ : 
  * Le prix nobel de physique américain ***Richard Feynman*** :us: postule, dans son son article fondateur "[Simulating Physics with Computers](http://physics.whu.edu.cn/dfiles/wenjian/1_00_QIC_Feynman.pdf)", que "*pour simuler des systèmes quantiques de Physique, les ordinateurs classiques ne suffiront pas*". Il eût l'idée qu'un "*ordinateur dont la logique serait régie par la physique quantique devrait ridiculiser les performances des machines classiques. [Ces machines d'un nouveau genre] seraient les seules capables de simuler (et donc de mieux comprendre) ces phénomènes quantiques*". Il avait l'habitude de dire à ses étudiants : *« Si vous croyez comprendre la mécanique quantique, c'est que vous ne la comprenez pas »*. Aller Plus Loin : [R. Feynman & Ordis Quantiques](https://www.eejournal.com/article/richard-feynman-and-quantum-computing/) (en anglais :gb:)
  * Énoncé du **No-cloning theorem** / **Impossibilité de Clonage Quantique**, par *Wooters*, *Zurek* & *Dieks* : "*Il est impossible de copier à l'identique un état quantique inconnu et arbitraire*".
* $1985$ : Le pionnier britannique ***David Deutsch*** :gb: pose les bases théoriques d'un ordinateur quantique 

### $1990's$ : Premiers algorithmes théoriques et Calculateurs quantiques

Dans les années $1990's$, deux résultats théoriques (de **Shor** et **Grover**) indiquent que des ordinateurs équipés de ***circuits de calcul quantique*** pourraient aborder des problèmes hors de portée des machines de Turing, pourvu de disposer de *microprocesseurs quantiques*, processeur dont personne ne sait à l’époque à quoi il pourrait ressembler.. De petits calculateurs quantiques sont construits durant les $90's$

* $1994$ : Le mathématicien américain ***Peter Shor*** :us:, au *MIT*, invente l'***algorithme de shor*** permettant de calculer en temps polynômial (au lieu d'exponentiel!) **la décomposition en produit de facteurs premiers**, permettant ainsi de **casser rapidement toutes les méthodes cryptographiques modernes** (https, ssl, ssh, etc..), et il calcule aussi rapidement le **logarithme discret**
* $1995$ :
  * Le mathématicien indo-américain ***Lov Grover***, des *Laboratoires Bells*, invente l'***algorithme de Grover*** permettant une recherche *bien plus rapide* dans *un "espace non structuré"* : en particulier, amélioration notable de l'efficacité des algorithmes de tri, ou de recherche de données dans une base de données. Un exemple d'application de cet algorithme : Comment trouver la boule cachée sous l’un des quatre gobelets en n’utilisant qu’une fois la fonction « soulever-un-gobelet » ? Impossible classiquement car, soulever un gobelet au hasard ne découvre la boule qu’une fois sur quatre. Mais, c’est possible avec l’algorithme quantique de Grover qui soulève les quatre gobelets en un seul appel à la fonction « soulever-un-gobelet ».
  * le concept de *qubit* est formalisé par ***Benjamin Schumacher***
  * Peter Schor invente le **premier Code Correcteur d'Erreurs Quantiques**, dit ***Code de Shor***
  * Première porte quantique  : Une porte $CNOT$, par **Juan Ignacio Cirac** et **Peter Zoller**, de l'Université d'Innsbruck, Autriche
* $1997$ : **Alexei Kitaev** propose le ***calcul quantique topologique***

### $2000's$ : Premières implémentations d'algorithmes quantiques et Course aux qubits

* $2001$ : Première Réalisation physique de l' **algorithme de Shor** avec $7$ qubits, par une équipe d'*IBM* en Californie. (Reference : [nature 414, 2001](https://www.nature.com/articles/414883a)). Ce résultat lance véritablement la course au nombre de qubits
* $2003$ : ***Rainer Blatt***, de l’***Université d’Innsbruck***, en Autriche, a réalisé **la première porte logique à deux qubits** en utilisant des ions calcium, un dispositif clé pour pouvoir effectuer des opérations en couplant les qubits entre eux.
* $2004$ : Microsoft se lance dans l'**informatique quantique topologique**
* $2005$ : record passe à $8$ qubits
* $2006$ : record à $12$ qubits
* $2008$ : première réalisation d'un *qubit*
* $2009$ : l'***Université de Yale*** réalise **le premier microprocesseur quantique** solide (sur silicium) à base d'atomes artificiels (aluminium).

### $2010's$ : La course à la Commercialisation & Sécurité

* $2011$ : La société canadienne ***D-Wave (Systems)*** commercialise le premier calculateur quantique (spécialisé dans les solutions de problèmes d’optimisation) avec un microprocesseur quantique à $128$ *qubits* (dont combien intriqués?) qu'elle vend entre autres à l'entreprise *Lockheed Martin*, un industriel de l'armement. $QV=64$
* $2012$ : Une équipe de l’***Université de Bristol***, en Angleterre, qui est parvenue à factoriser $21$, soit à montrer que ce nombre se décompose en $3$ fois $7$, grâce à un dispositif photonique. Certes la performance est modeste, mais elle représente une démonstration de principe de *l’algorithme de Shor*, dont la puissance devrait se révéler pour des nombres beaucoup plus grands. **Pour les problèmes de factorisation de grands nombres, on estime qu'il faudrait coupler au minimum $1000 \rightarrow 2000$, qubits**
* $2013$ : ***Google*** et la ***Nasa*** ouvrent un laboratoire de recherche sur l'intelligence artificielle quantique. Création de la société ***Rigetti (Cloud) Computing***.
* $2014$ : ***Edward Snowden*** dévoile que la *NSA* a investi $80$ millions de dollars pour développer un ordinateur/calculateur quantique
* $2015$ : 
  * ***IBM*** et l'***Université de New South Wales*** (Australie) dévoilent des circuits quantiques capables d'être associés en grand nombre.
  * Création de la société américaine IonQ :us:
  * ***Google*** et ***D-Wave*** (Systems) publient un début de preuve des performances de calcul de la machine ***D-Wave 2***.
* $2016$ : Pour la première fois, la vie d'un qubit a été prolongée grâce à un *Code Correcteur d'Erreur (ECC) Quantique*
* $2017$ : En automne, ***Rigetti*** annonce des ordis à $8$ qubits. En Juin, création d'une plateforme de Cloud Computing (Forest 1.0)
* $2018$ : 
  * ***Google*** annonce avoir réalisé un processeur quantique constitué de $72$ qubits, mais sans préciser combien d’entre eux ont pu effectivement être intriqués.
  * ***Intel*** dévoile un processeur quantique à $17$ bits, puis $3$ mois après, un processeur à $49$ qubits : très proche du seuil mythique des $\approx 50$ qubits pour atteindre la suprémacie quantique...
* $2019$ : 
  * En Janvier, **[IBM Q System One](https://en.wikipedia.org/wiki/IBM_Q_System_One)** (Cf. [vidéo](https://www.youtube.com/watch?v=LAA0-vjTaNY)), le "*tout premier système informatique quantique approximatif universel commercial*" à $20$ qubits
  * ***Google*** abandonne l'idée de son système $72$ bits, car il a rencontré trop de difficultés, et se focalise sur son système précédent en améliorant sa gestion du bruit et des corrections d'erreurs.
  * En Septembre, ***Google*** annonce avoir atteint la "**[Suprémacie quantique](https://fr.wikipedia.org/wiki/Supr%C3%A9matie_quantique) !!**", c'est-à-dire avoir créé un ordinateur quantique exécutant un algorithme en un temps incomparable (en $3$ min $20$ secondes) par rapport à un superordinateur actuel (en $10\, 000$ ans , dixit Google). Leur puce quantique, ***Sycamore***, comporte $54$ qubits supraconducteurs dont $53$ seulement seront utilisés, dits « ***transmon*** », un type de qubit dont les états de base sont des états de charge, et qui possède une sensibilité réduite au bruit. Cette annonce a depuis été contestée sur plusieurs points par des chercheurs d'IBM (ses concurrents..) qui affirment qu’ils ont mis au point un algorithme exécuté par son supercalculateur *Summit* qui a pu reproduire la tâche accomplie par Sycamore non pas en «10.000 ans» comme l'affirmait ***Google*** **mais en 2,5 jours**, « et avec une bien meilleure fidélité » : [Référence](https://www.ibm.com/blogs/research/2019/10/on-quantum-supremacy/)
  <center>
  <figure>
  <img src="../img/sycamore.jpg">
  <figcaption>

  Processeur **Sycamore**, à ions piégés

  </figcaption>
  </figure>
  </center>

  * En Juin, [Premier Benchmark de Margaret Martonosi (Princeton Univ.)](https://arxiv.org/pdf/1905.11349.pdf) comparant les performances des ordinateurs quantiques
  * En Novembre, publication d'un **Benchmark de Simulation Chimique des Ordis Quantiques**. Reférences : [1: article de IT Business, 2020](https://www.itforbusiness.fr/un-benchmark-pour-ordinateurs-quantiques-30851) et [2: nature](https://www.nature.com/articles/s41534-019-0209-0)

### $\ge 2020's$ : Un ordinateur quantique universel? & Suprémacie Quantique ?

* $2020$ :
  * Juin : ***Honeywell System Model H0*** avec $QV=64$
  * En juillet-août : ***Honeywell*** entre dans la course, avec un calculateur quantique de $6$ qubits, $QV=64$
  * En Septembre : ***IBM*** commercialise Falcon, à $27$ qubits et $QV=64$. ***Honeywell System Model H1*** à $10$ qubits et un QV de $128$ ([référence](https://www.honeywell.com/us/en/news/2020/09/achieving-quantum-volume-128-on-the-honeywell-quantum-computer)) 
  * ***D-Wave* Advantage** à $5640$ qubits, mais seulement $15$ d'entre eux sont intriqués (qubits de "qualité moyenne")
  * En Octobre, la société ***IonQ*** commercialise un calculateur à $32$ qubits "parfaits" disponible via Google Cloud Marketplace. Selon *IonQ* : $QV=4\, 000 \, 000$, invérifiable.. Plus étonnant, ***IonQ*** affirme aussi que sa machine utilise "seulement" $13$ qubits physiques dédiés aux **corrections d'erreurs**, par opposition notable aux $10\,000$ qubits requis pour les ordis à qubits supraconducteurs
  * En Décembre : Une équipe de recherche dirigée par **Pan Jianwei** et **Lu Chaoyang**, de l'**USTC - Université de Sciences et Technologies de Chine** :cn:, met au point un **calculateur quantique photonique** appelé "**Jiuzhang**", capable de **détecter jusqu'à 76 photons**, et qui atteint **la Suprémacie Quantique** (indépendamment de Google) : Elle prend $200$ secondes pour traiter les $50$ millions d'échantillons par échantillonnage de boson gaussien (GBS), alors que le superordinateur le plus rapide au monde a besoin de 600 millions d'années. $3$ avantages par rapport à *Sycamore* (selon l'équipe) : la vitesse de calcul, la capacité d'adaptation à l'environnement (**pas besoin de réfrigération extrême**), et la puissance de calcul sur des problèmes avec de plus grands échantillons. La machine utilise des sources optiques, $300$ de diviseurs de faisceaux, $75$ mirroirs et une centaine de détecteurs de photons. [Référence](https://science.sciencemag.org/content/370/6523/1460).

* $2021$ :
  * En Mars : ***Honeywell System Model H1*** à $QV=512$
  * En Juillet : 
    * $6$ Juillet : L'équipe de recherche de **Jian-Wei Pan**, de l'**USTC - Université de Sciences et Technologies de Chine** :cn:, prend la tête de la suprématie quantique, en créant une calculateur quantique de $66$ qubits, « **ZuChongzhi** » -mathématicien chinois du $Vè$ Siècle-, dont seulement $56$ qubits ont été utilisés pour les besoins d'une expérience. L’ordinateur quantique chinois, $2$ à $3$ fois plus rapide que *Sycamore*, a effectué un test complexe en $1,2h$, une tâche qui prendrait $8$ ans à un superordinateur classique, consistant à effectuer « *un échantillonnage aléatoire de circuits quantiques pour l’analyse comparative, jusqu’à une taille de système de $56$ qubits et $20$ cycles* »

    <center>
    <figure>
    <img src="../img/zuchongzhi.jpg">
    <figcaption>

    Processeur **ZuChongzhi**, à photons

    </figcaption>
    </figure>
    </center>

      * $20$ Juillet : Google annonce qu'il serait en mesure d'améliorer la qualité de correction d'Erreurs d'un facteur $100$, pour des qubits logiques composés de $21$ qubits physiques. Selon leur étude : plus il y a de qubits physiques dans un qubit logique, et meilleure est la qualité de la Correction d'Erreur quantique.
     * $27$ Juillet : $1^{er}$ problème réel résolu par l'informatique quantique :  Google utilise ($20$ qubits de)  son processeur Sycamore pour créer une nouvelle phase de la matière, appelée un **cristal temporel**, sorte de cristal en **mouvement perpétuel** (! :scream: :scream:), qui pourrait avoir des conséquences révolutionnaires en pour les ordinateurs quantiques en  accroissant leur fiabilité et leur précision, voire peut-être des mémoires quantiques (? ou pas?). Références : article sur [arxiv](https://arxiv.org/abs/2107.13571) et 

## Aller Plus Loin - Sitographie

* [Histoire de l'Informatique Quantique](https://www.cea.fr/comprendre/Pages/nouvelles-technologies/essentiel-sur-ordinateur-quantique.aspx), CEA
* [Qubit : wikipedia](https://fr.wikipedia.org/wiki/Qubit)
* [Calculateur Quantique : wikipedia](https://fr.wikipedia.org/wiki/Calculateur_quantique)
* [Informatique Quantique : wikipedia](https://fr.wikipedia.org/wiki/Informatique_quantique)
* [D'Étranges objets quantiques (Condensats de Bose-Einstein)](https://www.pourlascience.fr/sd/physique/detranges-objets-quantiques-4483.php), *lecture partielle seulement* (pour les abonnés à *pourlascience*)
* [Quantum Computing Science, A brief History of Quantum Computing](https://slideplayer.com/slide/15418833/), en anglais :gb:
* [CNRS : Ordinateur, les promesses de l'aube quantique](https://lejournal.cnrs.fr/articles/ordinateur-les-promesses-de-laube-quantique)
* [Comprendre l'informatique quantique, les qubits](https://www.frenchweb.fr/comprendre-linformatique-quantique-qubits/330991), Frenchweb
* [Comprendre l'informatique quantique, l'ordinateur quantique](https://www.frenchweb.fr/comprendre-linformatique-quantique-ordinateur-quantique/331993), Frenchweb
* [Comprendre l'informatique Quantique, Algorithmes &Applications](https://www.frenchweb.fr/comprendre-linformatique-quantique-algorithmes-et-applications/332659), FrenchWeb
* [Informatique Quantique](https://members.loria.fr/SPerdrix/wp-content/blogs.dir/110/files/sites/110/2017/03/chapitre-InfoQ.pdf), INRIA, $2017$
* [Comment fonctionne un ordinateur quantique](https://www.inria.fr/fr/comment-fonctionne-un-ordinateur-quantique), INRIA $2020$
* [Entwicklungsstand Quantencomputer, Status of Quantum Computer Development](https://www.bsi.bund.de/SharedDocs/Downloads/DE/BSI/Publikationen/Studien/Quantencomputer/P283_QC_Studie-V_1_2.pdf;jsessionid=3DF58498EE1058C552177016FFFE75FE.internet472?__blob=publicationFile&v=1) :pdf, Federal Bureau for Information Security :de:, en anglais :gb: $266$ pages


<env>Cours en Ligne</env>

* [Cours en ligne: Introduction à l'info quantique](https://home.cern/fr/news/announcement/computing/online-introductory-lectures-quantum-computing-6-november), CERN, $2020$
* [Quantum Information Theory](https://staff.fnwi.uva.nl/m.walter/qit21/), Univ. d'Amtersdam & MasterMaths, $2021$, en anglais :gb:
* [Quantum Computing](https://homepages.cwi.nl/~rdewolf/qc21.html), Univ. d'Amsterdam & MasterMaths, $2021$, en anglais :gb: