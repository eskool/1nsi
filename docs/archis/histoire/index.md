# 1NSI : Histoire des Ordinateurs

Cette partie est une introduction à **<em>Une</em> Histoire des Ordinateurs**, ainsi qu'à **<em>Une</em> Évolution des Architectures Matérielles**.

> Je pense qu'il y a un marché mondial pour environ 5 ordinateurs
>
> Thomas Watson, 1943, Président d'IBM[^1]

<br/>

> Un particulier n’a aucune raison d’avoir un ordinateur chez lui.
>
> Ken Olsen, 1977, Président de DEC/Digital - Digital Equipment Corporation (rachetée par Compaq, puis HP)[^1]

## Introduction

!!! col __40 center
    ![IBM 651](../img/ibm651.jpg)
    ![IBM 650, 1955](../img/ibm650.jpg)

<br/>
Historiquement, le mot français **Ordinateur** :fr: (du latin *ordinator* : « celui qui met de l’ordre, ordonnateur ») est inventé très précisément dans une lettre du $16$ avril $1955$, de [Jacques Perret](https://fr.wikipedia.org/wiki/Jacques_Perret_(philologue)), professeur à la Faculté de Lettres de Paris, à *[Christian de Waldner](https://fr.wikipedia.org/wiki/Christian_de_Waldner)* PDG d'*IBM France*. À cette époque, la société *IBM France* souhaitait faire la publicité de sa nouvelle machine, l'**[IBM $650$](https://fr.wikipedia.org/wiki/IBM_650)**, mais ne parvenait pas à trouver un nom en français qui lui rende suffisamment hommage à ses yeux : Le mot anglais ***Computer*** :gb: / Calculateur ou Calculatrice :fr:, qui existait déjà, leur semblait en effet trop restrictif pour les capacités de la machine, et trop connoté scientifique ...

<clear></clear>
!!! info
    En général, les *historiens* et *informaticiens* s'accordent pour dire que les caractéristiques fondamentales suivantes sont **nécessaires** pour que la machine soit considérée comme un <bred>ordinateur</bred> :

    * qu'elle puisse exécuter les **quatre opérations élémentaires** (addition, soustraction, multiplication, division) et, "*souvent*", qu'elle puisse extraire une **racine carrée** ou adresser une table qui en contient
    * qu'elle soit **numérique** :fr: / **digital** :gb: (par opposition à **analogique**)
    * qu'elle soit **programmable**, d'une manière ou d'une autre...
    * Plus précisément, qu'elle puisse exécuter des programmes enregistrés en *mémoire* : on parle de **machines à programmes enregistrés** (en *mémoire*)
    * enfin, qu'elle soit (entièrement) **électronique**

Pour d'autres *historiens* et *informaticiens* néanmoins, une machine n'est classifiée comme un ordinateur que lorsqu'elle dispose de **caractéristiques supplémentaires**, comme par exemple :

*  que la machine ait été **vraiment construite** (qu'elle ne soit pas restée au simple stade de projet, même inachevé) et 
* qu'elle ait été **complètement opérationnelle**
* ...

L'Histoire des Ordinateurs commence donc tout naturellement avec l'**Histoire des machines calculantes**, donc avec des ***cailloux***... le mot **calcul** venant en effet du latin **calculi**, qui signifie ***caillou***.

## Références

* [History Of Information](https://www.historyofinformation.com/index.php) :gb:

### Notes

[^1]: [Citations Historiques sur le marché des Ordinateurs](http://www.actinnovation.com/citations-innovation/citation-20-deux-citations-sur-la-marche-potentiel-des-ordinateurs-2410.html)