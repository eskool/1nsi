# 1NSI : Histoire des Ordinateurs : La Manchester Baby

## Small-Scale Experimental Machine ou Manchester *Baby*, :gb: $1948$

### Introduction

<clear></clear>

<center>

<iframe width="560" height="315" src="https://www.youtube.com/embed/cozcXiSSkwE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

</center>

### Invention

La ***[Small-Scale Experimental Machine (SSEM)](https://fr.wikipedia.org/wiki/Small-Scale_Experimental_Machine)*** , surnommée affectueusement ***[Baby (« Bébé »)](https://fr.wikipedia.org/wiki/Small-Scale_Experimental_Machine)*** ou ***[Manchester Baby](https://fr.wikipedia.org/wiki/Small-Scale_Experimental_Machine)*** est un projet/*"machine expérimentale à petite échelle"* de l'**Université Victoria de Manchester** :gb:, conçu en $1948$ par **Frederic C. Williams** et **Tom Kilburn**. Cette machine ne fut pas construite pour son utilité pratique en tant qu'ordinateur, mais bien pour tester :

* principalement, le **[tube de Williams](https://fr.wikipedia.org/wiki/Tube_de_Williams)** ou **tube de Williams-Kilburn** ($1946-1947$) ou **mémoire CRT** pour *Cathodic Ray Tube memory* : Il s'agit d'un tube cathodique servant de support pour la **première vraie mémoire à accès direct (RAM)**, en binaire, d'une capacité d'environ $1000$ à $2000$ **bits** ($\approx 125Ko - 250Ko$). En effet, le stokage électronique de l'information était le problème principal à résoudre à cette époque.
* accessoirement, la faisabilité de sa conception/design : c'est **la première machine à architecture de von Neumann du monde**

### Première architecture de Von Neumann

<def> 

:warning: Disposer les **données** et les **instructions** dans une même mémoire est l'une des **caractéristiques principales** (il y en a d'autres) de l'architecture matérielle dite <bred>Architecture de Von Neumann</bred> des ordinateurs.

</def>

Il s'agit en quelque sorte, de la première machine programmable, **à programme enregistré sur un support entièrement électronique (RAM)**, avec un design/conception de **Von Neumann** : ***le premier vrai ordinateur moderne***.

* Taille : $H \times P \times L \approx 1,8m \times 1,8m \times 7m$

### Premier algorithme sur une Architecture de Von Neumann

Trois algorithmes sont développés dont le premier d'entre eux est exécuté le <enc>**$21$ juin $1948$**</enc> : 
* Il s'agit d'un algorithme qui détermine le plus grand diviseur propre du nombre $2^{18} = 262\,144$
* Mathématiquement, la réponse était connue à l'avance... : c'est $2^{17} = 131\,072$
* L'algorithme composé de $17$ instructions, et testait tous les diviseurs de $2^{18}-1$ à $1$, dans l'ordre décroissant
* nombre d'opérations : $3,5$ millions d'opérations pour ce calcul
* Temps de calcul : $52$ min

### Influence

Pour certains *historiens* et *informaticiens*, cette date marque **le début de l'âge des logiciels**.
Dès que la machine *Baby* prouve sa faisabilité, un projet fut lancé à l'université de Manchester pour la développer afin d'en faire un ordinateur plus utilisable, le **Manchester Mark $I$** :

* $3$ fois plus grand que le *Manchester Baby*
* davantage de mémoire

Le *Mark $I$* devint à son tour rapidement le prototype du ***Ferranti Mark $I$*** (Février $1951$), qui est **le premier ordinateur généraliste commercialisé au monde** ([source](http://curation.cs.manchester.ac.uk/computer50/www.computer50.org/index.html?man=true)). 

### Aller plus loin

* [History Of Information](https://www.historyofinformation.com/detail.php?id=672) : https://www.historyofinformation.com/detail.php?id=672