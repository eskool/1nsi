# 1NSI : Histoire des Ordinateurs : L'EDVAC

## EDVAC, :us: $1949$

### Introduction

<clear></clear>

<center>

<iframe width="560" height="315" src="https://www.youtube.com/embed/xavtqd9PzDQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

</center>

### Invention & Conception

L'**EDVAC** (**E**lectronic **D**iscrete **V**ariable **A**utomatic **C**omputer) est l'un des tout premiers ordinateurs entièrement électroniques dont la création a été proposée dès août $1944$ par les deux créateurs de l'ENIAC **J. Presper Eckert** et **John Mauchly**, mais aussi **John Von Neumann**, et quelques autres chercheurs de l'**Université de Pennsylvanie**, plus précisément de la **Moore School of Electrical Engineering**. Il opère **en mode binaire** contrairement à l'*ENIAC*, qui opère en décimal. Sa conception s'inspire directement du rapport de **John Von Neumann**, daté du $30$ Juin $1945$, résumant l'expérience accumulée en construisant l'*ENIAC* ("*premier brouillon du rapport sur l'*EDVAC**" ou "**[First Draft of a Report ont the EDVAC](http://web.mit.edu/STS.035/www/PDFs/edvac.pdf)**"). Ce rapport propose des améliorations importantes sur l'**architecture matérielle** et la **logique** par rapport à l'*ENIAC*, que l'on appelera par la suite **Architecture de Von Neumann**. Le contrat pour la contruction de cette machine *EDVAC* est signé en avril $1946$, entre l'*US Army* et l'*Université de Pennsylvanie*. L'*EDVAC* sera finalement délivré en $1949$ au *Laboratoire de Recherche en Balistique* *(Ballistics Research Laboratory)* de l'*US Army*, **mais ne sera néanmoins mis en service qu'en $1951$**, en raison d'un des premiers conflits concernant les droits sur le brevet de propriété intellectuelle en informatique, qui oppose l'Université et ses créateurs : en effet, de nouvelles clauses dans les contrats de travail des chercheurs, les obligeant à céder leurs droits sur leurs inventions au profit de l'Université, finirent par décider les deux créateurs *Eckert* et *Mauchly* de quitter l'Université dès mars $1946$ pour créer l'une des premières sociétés commerciales **[Eckert-Mauchly Computer Corporation (EMCC)](https://en.wikipedia.org/wiki/Eckert%E2%80%93Mauchly_Computer_Corporation)** (rachetée en $1950$ par Remington-Rand) et commercialier une machine concurrente (le *BINAC*, en $1949$ également). L'*EDVAC* sera en activité jusqu'en $1961-1962$.

### Utilisation

Comme l'*ENIAC*, l'*EDVAC* est conçu pour faire face aux besoins du *Laboratoire de Recherche en Balistique (Ballistics Research Laboratory)* de l'*US Army* par l'*Université de Pennsylvanie*.

### Quelques caractéristiques techniques

* Coût total : $\approx 500\,000\$$ $\approx$ comme l'*ENIAC* (budget initial *EDVAC* : $100\, 000\$$)
* $6000$ tubes à vide
* $12000$ diodes
* une mémoire à accès direct (RAM) à haute vitesse, inventées spécialement pour l'occasion, et nommées **[mémoires à lignes de délais](https://fr.wikipedia.org/wiki/M%C3%A9moire_%C3%A0_ligne_de_d%C3%A9lai)** acoustiques au mercure (successeurs des tubes de William) : $1024$ mots de $44$bits, et $64$ lignes de délais
* une seconde unité de mémoires à lignes de délais
* Dimensions : $45,5\,m^2$
* Poids : $\approx 7800\, Kg$
* Consommation : $56kW$
* Fonctionnement : besoin de $3$ équipes de $30$ personnes
* Addition, Soustraction, Multiplication (entière) automatiques. En moyenne, une addition prend $864$ microsecondes, et une multiplication $2900$ microsecondes.
* Division Programmable
* Capacité mémoire : $1000$ (puis $1024$) mots de $44$ bits ($\approx 5,5Ko$)
* lecteur/enregistreur de bande magnétique
* Unité de Contrôle avec un oscilloscope
* Unité de répartition, qui récupère les instructions reçues depuis l'Unité de contrôle et la mémoire, et les renvoie vers d'autres unités
* Une Unité de Calcul pour calculer des opérations Logiques sur une paire de nombres, et renvoyer le résultat à la mémoire, après vérification sur une unité de duplication
* Une Horloge
* En $1960$, il fonctionne de manière continue $20h$ par jour, et en moyenne, durant $8$ heures sans erreurs

<env>Futures Extensions</env>

* $1953$ : lecteur de cartes perforées en $1953$
* $1954$ : mémoire supplémentaire dans un tambour magnétique
* $1958$ : Unité Arithmétique à virgule flottante