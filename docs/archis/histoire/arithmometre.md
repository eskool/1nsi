# 1NSI : Histoire des Ordinateurs - L'Arithmomètre

## L'Arithmomètre, $1820$

<div style="width:40%; margin-right: 10px; float:left;">

<center>

<figure>

<img src="../../img/arithmometre.png">

<figcaption>

L'Arithmomètre de
Thomas (de Colmar), $1820$

</figcaption>

</figure>

</center>

</div>

L'**arithmomètre** est le nom donné à une **machine à calculer mécanique** inventée en $1820$ par **[Charles Xavier Thomas de Colmar](https://fr.wikipedia.org/wiki/Charles_Xavier_Thomas_de_Colmar)** ($1785$-$1870$). C'est la première du genre à avoir été produite en série et commercialisée dans le monde. Près de $5000$ exemplaires seront construits entre le début de sa commercialisation en $1850$ et $1915$. Entièrement mécanique, l'arithmomètre est capable d’effectuer les $4$ opérations de l’arithmétique avec fiabilité et promptitude. Il régnera en maître pendant pratiquement toute la seconde moitié du 19e siècle et sera cloné par de nombreux constructeurs européens.

<clear></clear>
