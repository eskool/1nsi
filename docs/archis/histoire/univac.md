# 1NSI : Histoire des Ordinateurs : L'Univac

## Univac 1, :us: mars $1951$

<clear></clear>

### Introduction

<clear></clear>

<center>

<iframe width="560" height="315" src="https://www.youtube.com/embed/j2fURxbdIZs" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

</center>

### Invention

<clear></clear>

<div style="width:40%; margin-right: 10px; float:left;">

<center>

<figure>

<img src="../../img/univac1.jpg" style="width:100%;" />

<figcaption>

Univac 1, mars $1951$

</figcaption>

</figure>

</center>

</div>

L'[Univac 1](https://fr.wikipedia.org/wiki/UNIVAC_I) (pour <bred>Univ</bred>ersal <bred>A</bred>utomatic <bred>C</bred>omputer 1) est le premier ordinateur commercialisé (le $30$ mars $1951$) aux *USA* :us:. Cette machine sera utilisée par le *Bureau du Recensement (Census Bureau)* des Etats-Unis en mars $1951$. Elle  connaîtra un succès national en Novembre $1952$, lors de l'élection présidentielle au journal télévisé, car elle sera capable de prédire pour la première fois, avant un Humain, le nom du prochain président élu des Etats-Unis dès la sortie des urnes.

<clear></clear>

### Quelques Caractéristiques Techniques

* Tubes à vides
* mémoire vive : mémoire à ligne de délai
* $1024$ mots à $12$ chiffres. Chaque mot est composé de deux adresses (*Single Adress*) à $6$ chiffres
* décimal (et non binaire)
* Entrées / Sorties :
  * Bandes magnétiques
  * Imprimantes à haut débit: $185\, 000\$$
  * convertisseurs de données hors-ligne
* Arithmétique : $1905$ opérations par seconde
* Coût : $750\, 000\$$
* Production : En $1954$, $20$ Univac ont déjà été délivrés

### Programmation

<div style="overflow:hidden;">

<center>

<figure>

<img src="../../img/univacinstructions.jpg" style="width:60%;">

<figcaption>

Univac 1 instructions

</figcaption>

</figure>

</center>

</div>

### Aller Plus Loin

* [Univac Computer History](http://archive.computerhistory.org/resources/access/text/2010/08/102722064-05-01-acc.pdf) : http://archive.computerhistory.org/resources/access/text/2010/08/102722064-05-01-acc.pdf
* [Page sur l'un des premiers programmeurs Univac](https://www.scott-a-s.com/grandfather-univac/) : https://www.scott-a-s.com/grandfather-univac/