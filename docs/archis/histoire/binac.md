# 1NSI : Histoire des Ordinateurs : Le BINAC


## BINAC, :us: $1949$

### Introduction

<iframe width="560" height="315" src="https://www.youtube.com/embed/udJUWenPK4w" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Invention

Le **BINAC** (**BIN**ary **A**utomatic **C**omputer) a été un des premiers ordinateurs. Il a été conçu pour la **Northrop Corporation** par la société **Eckert-Mauchly Computer Corporation (EMCC)** : 
* contrat signé en Octobre $1947$ 
* **délivré en Septembre $1949$** : c'est donc :warning: **la première machine à Architecture de Von Neumann** :warning:
* Quantité produite : $1$ seule machine (remplacée par son successeur : l'Univac 1)

Bien qu'ils aient commencé la conception de l'**EDVAC** à l'Université de Pennsylvanie, **John Eckert** and **John William Mauchly**, deux co-inventeurs de l'*ENIAC*, ont quitté l'université pour fonder la **Eckert–Mauchly Computer Corporation (EMCC)**, la première compagnie d'ordinateurs. Le *BINAC* a été leur premier produit :
* **le premier ordinateur à programme enregistré aux États-Unis** :us: et 
* **le premier ordinateur commercial au monde**. 

### Quelques Caractéristiques Techniques

* Coût initial: $\approx 100\, 000 \$$ (coût réel : $278\, 000\$$)
* Deux tours qui sont des CPU indépendants, chacun disposant de
* mémoire vive : [mémoires acoustiques à ligne de délai](https://fr.wikipedia.org/wiki/M%C3%A9moire_%C3%A0_ligne_de_d%C3%A9lai) en mercure : 
  * Capacité : $512$ mots, subdivisés en $16$ channels/canaux de $32$ mots de $31$ bits
  * séparés par des intervalles de $11$ bits entre les mots pour permettre la transition pour les mémoire à ligne de délai
* $1400$ tubes à vides ($700$ dans chaque CPU)
* Transmission Série des données
* Entrée/ Sortie de données et de programmes en Octal :
  * manuellement, via un clavier à $8$ touches
  * ou depuis des bandes magnétiques
* Calculs en Binaire
  * Addition : $\approx 800$ microsecondes
  * Multiplication : $\approx 1200$ microsecondes
* Longueurs Instructions : $14$ bits
* Horloge : $1 \rightarrow4,25\,MHz$
* Lecteur/Enregistreur à bande magnétique
* Machine à écrire en Entrée/Sortie
* Superficie : $\approx 12m^2$ (divisée par $13$ par rapport à l'*ENIAC*)
* Consommation : $13\, kW$

### La société EMCC

<center>

| Année | Événements Marquants | Nombre <eb/> Employés |
|:-:|:-:|:-:|
| $1946$ | Création sous le nom<br/>*<b>E</b>lectronic <b>C</b>ontrol <b>C</b>ompany (ECC)* | $7$ |
| $1948$ | (Septembre) : Première Utilisation<br/>de Semi-Conducteurs dans les ordinateurs :<br/>(Second module du BINAC,<br/>contenant des [diodes germanium](http://semiconductormuseum.com/MuseumStore/MuseumStore_1N34_Index.htm))<br/> | $40$ |
| $1949$ | :one: Premier algorithme (remplir le registre A) <br/>du premier ordi <br/>à programme enregistré aux :us:<br/>:two: Premier communiqué de Presse<br/>de vente du premier ordi commercial<br/> à programme enregistré aux :us:<br/>:three: Livraison du BINAC à Northtrop | $134$ |
| $1950$ | Rachetée par <br/>*Remington Rand* | $\,$ |
| $1951$ | *Remington Rand*<br/>choisit de développer<br/>(uniquement) son successeur : <br/>L'**Univac 1** | $\,$ |

</center>

### Aller Plus Loin

* [History Of Information](https://historyofinformation.com/detail.php?entryid=844) : https://historyofinformation.com/detail.php?entryid=844

