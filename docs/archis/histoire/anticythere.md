# 1NSI : Histoire des Ordinateurs :<br/>La Machine d'Anticythère


## La Machine d'Anticythère (c. 87 av JC - 60 av JC)

### Introduction

<center>

<iframe width="560" height="315" src="https://www.youtube.com/embed/gLzHlA33rqw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Le Mécanisme d'Anticythère, vers $87$ av J.C. ($7$min $51$)

</center>

### La machine d'Anticythère

<clear></clear>

!!! col __50
    ![La Machine d'Anticythère](../img/anticythere.jpg)
    La Machine d'Anticythère,
    vers $87$ av JC

La <bred>machine/mécanisme d'Anticythère</bred> **(vers $87$ av JC)** est considéré comme le plus vieux mécanisme à engrenages connu, autrement dit ***le premier calculateur mécanique connu de l'Humanité***. Elle servait notamment à :

* **prédire les dates et heures des éclipses (lunaires et solaires)**, ainsi que 
* **la couleur des éclipses** (certaines sont rouges). 
* la date des ***jeux antiques*** (ancêtres des JO)

Elle contient environ $2200$ lettres grecques, gravées sur le bronze ayant servi également à dater sa création (vers $100$ av JC), utilisées dans deux groupes différents :

* un texte astronomique "étrange" à l'avant du mécanisme (y apparaissent les mots **Vénus**, *Hermès/Mercure* et le **zodiaque**)
* un "mode d'emploi" de la machine à l'arrière, donnant des indications sur les roues dentées, sur les périodes de ces roues, et sur les phénomènes astronomiques

Elle été découverte en $1900$ au large de l'île grecque d'***Anticythère***, qui fait face à l'île de *Cythère*, qui se trouvait au milieu de la route des navires entre l'île grecque de *Rhodes* et *Rome*.

<clear></clear>

### Inventeur & machines semblables

Selon les écrits de ***Cicéron (de $106$ à $43$ av JC)*** , homme d'état (consul), avocat et écrivain Romain, admirateur d'***Archimède de Syracuse (de $287$ à $212$ av JC)*** (en Sicile) et ami de ***Posidonios de Rhodes (de $135$ à $51$ av JC)*** (île grecque), il existait à Rome à cette époque (vers $80$ av JC) deux **machines à engrenages** semblables (distinctes?), et avec la machine d'anticythère cela fait donc $3$ machines à engrenages semblables (toutes distinctes?):

* un <bred>planétarium mécanique</bred> ou <bred>Sphère d'Archimède</bred> sûrement contruite par ***Archimède de Syracuse*** (d'après *Cicéron*, dans *De la République, I, 14*, ou peut-être avant lui, *Thalès de Milet, vers $620$ à $545$ av JC*), auquel certains attribuent l'**invention des engrenages**. Ramenée à Rome par le général romain ***[Marcus Claudius Marcellus](https://fr.wikipedia.org/wiki/Marcus_Claudius_Marcellus_(consul_en_-222))*** après le siège de *Syracuse* en $212$ av JC, durant lequel *Archimède* trouva la mort, cette ***Sphère d'Archimède*** serait (toujours d'après *Cicéron*,  *De la République, I, 14*) "*capable de reproduire les mouvements du Soleil, de la Lune et des cinq étoiles errantes (planètes)"* ([source](http://scmsa.eu/archives/Ciceron_sphere_Archimede.pdf)) et **se trouvait donc à Rome plus d'un Siècle avant le naufrage de la machine d'Anticythère**.
* une <bred>Sphère de Posidonios</bred> sûrement construite par ***Posidonios de Rhodes*** encore appelé ***Posidonios d'Apamée***, qui était un philosophe stoïcien grec et qui fonda une École à Rhodes. Selon son ami *Cicéron* (dans *De natura deorum, livre $II$*), *Posidonios* (d'autres pensent à un de ses élèves? *geminos de Rhodes*?) aurait construit un objet ayant des similitudes avec *la Sphère d'Archimède*, qui serait nommée ***Sphère de Posidonios*** et qu'il décrit ainsi : "*Cette sphère qu'a construite naguère mon ami Posidonius et qui, dans ses révolutions successives, montre le soleil, la lune et les cinq étoiles errantes (planètes), comme ces astres le font dans le ciel.*". **Cette deuxième machine se trouvait également à Rome dans les mêmes années que le naufrage de l'anticythère**.
Noter qu'une récente inscription "*Sphère Dorée*" découverte sur *la machine d'Anticythère* incite sérieusement certains chercheurs à penser qu'il s'agirait de la célèbre ***Sphère de Posidonios*** que l'on croyait perdue.
* et la "<bred>machine d'Anticythère</bred>", qui pourrait donc tout aussi bien être :
    * la ***Sphère d'Archimède*** ? (peut-être?) 
    $\Rightarrow$ **<bred>Archimède</bred> serait alors l'inventeur de la machine d'Anticythère**
    * ou la ***Sphère de Posidonios*** ? (probablement ?)
    $\Rightarrow$ **<bred>Posidonios</bred> serait alors l'inventeur de la machine d'Anticythère**
    * ou un $3$ème objet (autre que ces deux là) ? fabriqué par un autre savant? D'autres chercheurs/historiens mentionnent également <bred>*Hipparque de Nicée*</bred> (actuelle Turquie) (de $190$ av JC à Nicée, à $120$ av JC à Rhodes), comme possible **inventeur de la machine d'Anticythère** : fondateur de la trigonométrie, et l'un des grands noms de l'astronomie antique, il aurait entre autres inventé l'*astrolabe* (sorte d'ancêtre du *sextant* des marins).

Quoi qu'il en soit, la complexité de la machine est remarquable pour l'époque, et à titre de comparaison, il faudra attendre près d'un millénaire, et la *Renaissance Italienne* (du $XIV$-ème au $XVI$-ème Siècle) avec ses ingénieurs *Francesco di Giorgio* ou *Léonard de Vinci*, pour voir apparaître des mécanismes à engrenages d'une complexité comparable.

### Quelques caractéristiques techniques

Le **mécanisme d'Anticythère** :

* il est en bronze 
* il comprend une trentaine de roues dentées identifiées, solidaires et disposées sur plusieurs plans.
* repose sur la rotation d'engrenages de tailles différentes entraînant des aiguilles indiquant la position des astres à un moment donné
* il est basé sur la **théorie géocentrique** (la terre au centre de l'Univers) en vigueur à l'époque
* **Face Avant :**
    * un cadran circulaire à **$365$ positions** pour **les jours** (représentant les $365$ jours du calendrier Egyptien)
    * un cadran indiquant les **positions de la Lune** (par rapport au Zodiaque)
    * un cadran indiquant les **positions du Soleil** (par rapport au Zodiaque)
* **Face Arrière :** Deux cadrans en spirale représentant deux calendriers astronomiques utilisés pour prédire des éclipses de la Lune et du Soleil : 
    * un cadran en spirale, représentant un calendrier astronomique, qui suit le **[cycle de Méton](https://fr.wikipedia.org/wiki/Cycle_m%C3%A9tonique)** à $235$ positions de la Lune (environ $19$ ans). Pour prédire les éclipses de Soleil et de Lune.
    * un cadran en spirale, représentant un calendrier astronomique, qui suit le **[cycle de saros](https://fr.wikipedia.org/wiki/Saros)** à $223$ positions de la Lune (environ $18$ ans). Pour prédire les éclipses de Soleil et de Lune.

<clear></clear>