# 1NSI : Histoire des Ordinateurs: Colossus vs Lorenz


## les machines *Colossus* :gb: $1943$ vs *Lorenz* :de: $1942$

### Introduction

<div style="width:100%; margin-right: 10px; float:left;">

<center>

<figure>

<iframe width="560" height="315" src="https://www.youtube.com/embed/knXWMjIA59c" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<figcaption>

Colossus, Histoire ($8$ min $40$)
:warning: Vidéo en anglais :gb:
  
</figcaption>

</figure>

<figure>

<iframe width="560" height="315" src="https://www.youtube.com/embed/g2tMcMQqSbA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<figcaption>

Aller Plus Loin (Plus technique) : Colossus ($1$ h $00$ min $26$ sec)
:warning: Vidéo en anglais :gb:
  
</figcaption>

</figure>

</center>

</div>

### Les machines Lorenz :de:

<clear></clear>

<div style="width:40%; margin-right: 10px; float:left;">

<center>

<figure>

<img src="../../img/lorenz.jpg">

<figcaption>Machine <em>Lorenz-SZ42-2</em></figcaption>

</figure>

</center>

</div>

Les **[machines de Lorenz](https://fr.wikipedia.org/wiki/Machine_de_Lorenz)** :de: sont des machines électro-mécaniques conçues dès $1941$ (SZ40) et $1942$ (SZ42) par les allemands durant la guerre.
Durant la guerre, ce sont majoritairement les machines *Enigma* qui sont utilisées pour les communications sécurisées de l'armée allemande, mais bien que moins connues, ce sont des *machines de Lorenz* qui sont utilisées par les hauts dignitaires allemands, pour échanger entre eux des **informations de haute importance** : elles utilisent un code binaire, et sont réputées inviolables. 
<env>**Cryptanalyse**</env> néanmoins, le *code Lorenz* sera finalement *cassé* par les alliés (et les suédois indépendamment), c'est-à-dire que l'on a trouvé un *algorithme* permettant son déchiffrement direct, entre $1942$ et $1943$ :

* La bonne clé produisait un plus grand nombre de $0$ que de $1$ lors de l'addition de deux lettres consécutives identiques. 
* De ce principe, la machine *Colossus* calculait une clef qui produisait plus de $0$ qu'une autre. 
Cette « clé transitoire » était appliquée plusieurs fois au texte chiffré original, 
* chaque nouvelle application de la clé dévoilant quelques nouveaux passages du texte chiffré.

<clear></clear>

### Colossus :gb: $1943$

<clear></clear>

<div style="width:40%; margin-right: 10px; float:left;">

<center>

<figure>

<img src="../../img/colossus.jpg">

<figcaption>Machine <em>Colossus</em>, 1943, à Bletchley Park</figcaption>

</figure>

</center>

</div>

Créée selon les exigences formulées par [Max Newman](https://fr.wikipedia.org/wiki/Max_Newman) et construite par une équipe dirigée par l'ingénieur anglais [Tommy Flowers](https://fr.wikipedia.org/wiki/Tommy_Flowers), la **[machine Colossus](https://fr.wikipedia.org/wiki/Colossus_(ordinateur))** :gb:, $1943$, **(Colossus Mark $1$)**, s'inspire néanmoins directement des idées de Turing : son apport pour cette machine est un procédé de cryptanalyse (appelé le **Banburisme**) basé sur les probabilités conditionnelles permettant d'inférer des informations sur la configuration la plus probable de la machine *Enigma*. Le *banburisme* a permis de ***craquer*** les clés de chiffrement de la (Kriegs)marine allemande.

<env>Quelques Caractéristiques Techniques</env>

Le **Colossus** est **le premier ordinateur (entièrement) électronique programmable du monde**. Il est fondé sur les caractéristiques suivantes, novatrices pour l'époque :

* circuits électroniques binaires
* Tubes à vide / Valves (thermioniques) qui servent à réaliser des opérations **booléennes** et **arithmétiques** : c'est **la première machine électronique programmable**, qui soit entièrement non mécanique.
* signal d'horloge unique cadençant l'ensemble des opérations de la machine. La fréquence d'horloge pouvait être échelonnée de à presque zéro pour faire du pas à pas,jusuq'à $5KHz$ (vitesse de la bande papier)
* 1ère utilisation d'un **Registre à décalage** à $5$ étages
* 1ère utilisation d'**interruptions**
* Lecteur optique de la bande papier : jusqu'à $10K$ caractères/seconde
* Isolation électrique des circuits de calcul des registres de sortie pour améliorer la fiabilité
* Calculs parallèles (jusqu'à $100$ opérations booléennes simultanées sur chaque bit d'entrée)

Néanmoins:

* il s'agit d'une machine spécialisée, destinée uniquement à du décryptage, **non généraliste/non universelle**
* la **programmation** se fait par des ***switches*** (relais électriques) et des ***plugs*** (câbles à rebrancher) et le *programme* n'est donc pas stocké en tant que tel dans la machine.

<clear></clear>

Deux versions du Colossus :

* le premier, le **Colossus Mark $1$**, construit en $11$ mois (opérationnel en Décembre $1943$) par une équipe dirigée par *Thomas "Tommy" Flowers* et installé près de Londres à **Bletchley Park**. Utilisé durant la 2ème Guerre mondiale pour casser le (pour la **cryptanalyse** du) **code Lorentz**. Il calcule $5000$ opérations/secondes.
* le Second, le **Colossus Mark $2$** lancé en juin $1944$, est cinq fois plus rapide que le premier, et servit notamment pour le lancement surprise du débarquement de Normandie.

<env>**Aller plus loin**</env> 
* Comparaison [Colossus vs ENIAC](https://shura.shu.ac.uk/9501/3/Atkinson_-_Eniac_versus_Colossus_paper_%26_url.pdf) :fa-file-pdf: :gb:
* Historique des Tubes à vide ou Valves (thermioniques) : :warning: vidéo en anglais :gb:

<div style="width:100%; margin-right: 10px; float:left;">

<center>

<figure>

<iframe width="560" height="315" src="https://www.youtube.com/embed/-P4sDnNOMZI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<figcaption>

*CSGuitars* fait un historique des Valves (thermioniques) / Tubes à vide ($6$ min $24$)
:warning: Vidéo en anglais :gb:

</figcaption>

</figure>

</center>

</div>

<clear></clear>