# 1NSI : Quelques Ressources Utiles pour la 1ère NSI

## Sitographie d'Autres Enseignants de 1ère NSI

* Eskool 1ère NSI: [eskool.gitlab.io/1nsi](http://eskool.gitlab.io/1nsi)
* Eskool Terminale NSI: [eskool.gitlab.io/tnsi](http://eskool.gitlab.io/tnsi)
* Alain BUSSER : [https://alainbusser.frama.io/NSI-IREMI-974/](https://alainbusser.frama.io/NSI-IREMI-974/)
* Guillaume Connan: [https://gitlab.com/lyceeND/1ere](https://gitlab.com/lyceeND/1ere)
* Romain Janvier : [nsi.janviercommelemois.fr](http://nsi.janviercommelemois.fr/)
* Frédéric Junier: [http://frederic-junier.org/NSI/premiere/](http://frederic-junier.org/NSI/premiere/)
* Gilles LASSUS: [https://glassus.github.io/premiere_nsi/](https://glassus.github.io/premiere_nsi/)
* [Le site de NovelClass](https://novelclass.com/nos-cours/premiere/1ere-nsi/)

