# 1NSI : Manuels Numériques Éditeurs

!!! warning "ATTENTION : Ces Ressources sont commerciales :sk-copyright:"

* [Hachette, 1ère NSI, édition 2021, accès libre](https://mesmanuels.fr/acces-libre/3813624)
* BORDAS
    * SNT 2nde, collection 3.0, par C.Declercq, S. Mauras et S. Canet
    * SNT 2nde, collection 3.0, par C. Savinas, S. Bonnaud et R. Pagès (versions élève et professeur)
    * NSI 1ère, collection 3.0, par S. Bonnaud et C. Savinas.
* ELLIPSES
    * NSI 1ère, par T. Balabonski, S. Conchon, J-C Filliâtre et K. Nguyen;
    * NSI 1ère, par S. Bays;
    * NSI 1ère, par C. Canu;
    * NSI Terminale, par T. Balabonski, S. Conchon, J-C Filliâtre et K. Nguyen;
    * NSI Terminale, Compétences attendues, par J-C Bonnefoy et B. Petit;
    * NSI 1ère, Parcours et méthodes, par D. Legrand;
    * NSI Terminale, Par S. Bays;
    * SNT 2nde par S. Albisser et S. Balny;
    * SNT 2nde par B. Petit et J-C Bonnefoy.
* Nouveauté 2022 :
    * Informatique MP2I, par L. Vercouter;
    * Informatique MP2I et MPI, par V. Barra;
    * Informatique CPGE tronc commun 1ère et 2ème année, par T. Audibert et A. Oussalah;
    * Informatique CPGE MPSI PCSI PTSI tronc commun, par S. Bays;
    * Spécialité NSI - 30 leçons avec exercices corrigés, par T. Balabonski, S. Conchon, J-C Filliâtre et K. Nguyen.
* HACHETTE
    * SNT 2nde, par D. Lagraula et K. Odiot
* HATIER - FOUCHER
    * SNT 2nde, par D. Lescar, C. Blivet, F. Danes, H. Dibesse, P. Kerner, Y. Salaun,
    * NSI prépabac 1ère, par C. Adobet, G. Connan, G. Rozsavolgyi, L. Signac.
    * NSI prépabac Terminale, par G. Connan, V. Petrov, G. Rozsavolgyi, L. Signac.
* NUMWORKS
    * Sciences Numériques et Technologie par A. Busser, D. Gluck, C. Mieszczak, C. Savinas et L. Vincent (téléchargeable [ici](https://www.numworks.com/fr/ressources/snt/book.pdf))