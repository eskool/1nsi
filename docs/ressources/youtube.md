# 1NSI - Channels YouTube pour la 1ère NSI

## Channels Généraux sur la totalité du Programme

* [Cédric GERLAND, 1NSI et TNSI](https://www.youtube.com/channel/UC-gmNqo2JEyMNoH8mTvh1oQ/videos)
* [David Latouche](https://www.youtube.com/c/DavidLatouche/videos)
* [NovelClass](https://www.youtube.com/watch?v=ZwEUa8thBeY&list=PLJrJPz8qhVidtWQO70U73OFPEtToSu2pB)
* [Les Bons Profs](https://www.youtube.com/channel/UCL7FXdd8TW0hxNHmzlPlAQQ)
* [École Hexagone](https://www.youtube.com/channel/UCOS3iEX86oB1BIAu9KP016g)


## Channels Spécifiques à Python

--8<-- "allo.md"


