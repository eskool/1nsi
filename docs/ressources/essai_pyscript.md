---
template: pyscript.html
---

# Essai

<div>
<py-repl> 2 < 3</py-repl>
<py-repl id="r1" auto-generate="true">
def indice_min(nombres):
    indice = 0
    minimum = nombres[0]
    for i in range(len(nombres)):
        if minimum > nombres[i]:
            minimum = nombres[i]
            indice = i
    return indice
</py-repl>
<py-repl id="r2" auto-generate="true">
# test 1 : doit renvoyer 2
indice_min([2, 4, 1, 1]) == 2
</py-repl>
<py-repl id="r3" auto-generate="true">
# test 2 : doit renyoyer True
indice_min([5]) == 0
</py-repl>
<py-repl id="r4" auto-generate="true">
# tests 3 : ne doit pas renvoyer de message d'erreur
assert indice_min([2, 4, 1, 1]) == 2
</py-repl>
<py-repl id="my-repl" auto-generate="true">
# Réaliser vos propres tests :
indice_min()
</py-repl>
</div>

