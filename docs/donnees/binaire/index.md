# 1NSI : Représentation des Nombres en Base 2 (Binaire)

--8<-- "docs/bo/bases.md"
--8<-- "docs/bo/binaire.md"

## Introduction

<center>

<iframe width="560" height="315" src="https://www.youtube.com/embed/LnDKfkPhuM0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

</center>

## Bits et Mots machine

Dans un ordinateur, toutes les informations (données ou programmes) sont représentées grâce à des 0 et des 1.

!!! def
    * Un <rbl>bit</rbl> (pour <rbl>BI</rbl>nary digi<rbl>T</rbl>) est un $0$ ou bien un $1$
    * Un ensemble de $8$ bits est appelé un <rbl>octet</rbl> :fr: ou un <rbl>byte</rbl> :gb:
    * Un <rbl>mot machine</rbl> ou <rbl>words</rbl> est un ensemble de $2$, $4$ ou $8$ octets

!!! exp
    Une machine de $32$ bits est un ordinateur manipulant des mots de $4$ octets ($4\times8 = 32$ bits)

C'est ce principe de regroupement en paquets de bits, ou mots machine, qui permet de donner un sens à tout le paquet de bits et ainsi pouvoir représenter et manipuler des données autres que des $0$ et des $1$ :

* des nombres entiers (positifs ou négatifs), 
* des (approximations de) nombres réels, 
* des caractères alphanumériques et 
* des textes, des images, des sons, des vidéos.

## Représentation en Base $2$ (Binaire) des Entiers Positifs

!!! def 
    **En Base $2$**, on utilise seulement **DEUX** chiffres : $0$ et $1$

!!! pte
    De manière similaire à l'encodage des nombres entiers en base $10$, On peut représenter n'importe quel nombre en base $2$, grâce à la notation binaire positionnelle, donc avec les seuls chiffres $0$ et $1$.

!!! exp
    $x= 1011$ est un nombre écrit en **notation binaire, sur 4 bits**  

De quel *nombre* (sous-entendu en base $10$) s'agit-il? nous allons le voir.  
Commençons par lever une ambigüité de notations.  

!!! notation
    Pour lever l'ambigüité entre les notations Décimale et Binaire (par ex. pour des nombres tels que $x=110$), on note les nombres en écriture binaire de l'une des manières équivalentes suivantes:  
    <center>
    $110 = (110)_2 = 110_2 = \overline{110}_2$
    </center>

Nous utiliserons principalement la notation : $110_2$  
Mais il pourra néanmoins arriver aussi que nous utilisions la *notation Python* : $0b110 = 110_2$

!!! def
    Pour un nombre binaire,

    1. le bit **le plus à gauche** dans l'écriture binaire (en base $2$) s'appelle le <rbl>bit de poids fort</rbl> :fr: ou <rbl>Most Significant Bit</rbl> (MSB) :gb:
    1. le bit **le plus à droite** dans l'écriture binaire (en base $2$) s'appelle le <rbl>bit de poids faible</rbl> :fr: ou <rbl>Least Significant Bit</rbl> (LSB) :gb:

<center>
![Poids Fort vs Poids Faible](./img/PoidsFort_PoidsFaible.png){width="40%"}
</center>

On s'intéresse maintenant aux Conversions Binaire $\Leftrightarrow$ Décimal:

* **Conversion Binaire $\rightarrow$ Décimal:** Etant donné un nombre $x$ donné en écriture binaire, comment écrire ce nombre en base $10$?
* **Conversion Décimal $\rightarrow$ Binaire :** Etant donné un nombre $x$ donné en écriture décimale, comment écrire ce nombre en base $2$?

### Conversion Binaire $\rightarrow$ Décimal

!!! mth "Conversion Binaire $\rightarrow$ Décimal"
    Pour convertir un nombre entier, donné en base $2$ (binaire), vers le décimal, il convient de :  

    * Au dessus de chaque chiffre, **de droite à gauche**, on écrit des puissances croissantes de $2$ :

    $$2^4\,\,\,\,\,2^3\,\,\,\,\,2^2\,\,\,\,\,2^1\,\,\,\,\,2^0$$

    $$1 \,\,\,\,\,\,\,\, 1 \,\,\,\,\,\,\,\, 0 \,\,\,\,\,\,\,\, 0 \,\,\,\,\,\,\,1$$

    * On multiplie chaque chiffre par la puissance de $2$ correspondante :

    $$\begin{align}
    11001 &= 1\times2^4 + 1\times2^3 + 0\times2^2 + 0\times2^1 + 1\times2^0 \\
    &= 1\times16 + 1\times8 + 0\times4 + 0\times2 + 1\times1 \\
    &= 16 + 8 + 1 \\ 
    &= 25
    \end{align}
    $$

Il s'agit d'une **convention d'écriture** de nombres entiers, en base $2$ (Binaire).

!!! python "conversion Binaire $\rightarrow$ Décimal en Python"
    ```python
    >>>int('0b11011',2)
    27
    ```

### Conversion Décimal $\rightarrow$ Binaire

!!! mth "Conversion Décimal $\rightarrow$ Binaire"
    Pour convertir un nombre décimal (en base $10$) en nombre binaire (en base $2$) il convient de:

    * réaliser la **division euclidienne** du nombre décimal (en base $10$) par 2, jusqu'à ce que le **quotient vaille 0**.
    * lire alors la suite de **tous les Restes mais à l'envers** (en commençant du bas vers la haut)

<center>
![Conversion Décimal -> Binaire](./img/Dec2Bin.png)
</center>

Dans cet exemple, on peut dire que: $83_{10} = 1010011_2$

!!! info "Conversion Décimal $\rightarrow$ Binaire"
    ```python
    >>>bin(1534)
    '0b10111111110'
    ```

### Opérations Arithmétiques sur les Nombres Binaires

On peut réaliser les Opérations Arithmétiques usuelles sur les Entiers Binaires: + , $-$ , $\times$ $\div$. Les algorithmes de Calculs que l'on réalise en "posant" les opérations, restent les mêmes qu'en base $10$.

Il faut retenir qu'en base $2$:

* Addition :  
$0 + 0 = 0$   
$0 + 1 = 1$  
$1 + 0 = 1$  
$1 + 1 = 10$ c'est à dire 2 en binaire  
**on posera donc 0, et on retient 1 dans la colonne de gauche**

* Soustraction :  
$0 - 0 = 0$  
$1 - 0 = 1$  
$1 - 1 = 0$  

* Multiplication :  
$0 \times 0 = 0$  
$1 \times 0 = 0$  
$0 \times 1 = 0$  
$1 \times 1 = 1$  

* Division :  
$\dfrac 01 = 0$ et $\dfrac 11 = 1$  

**Exemples d'Opérations Arithmétiques "Posées" en base $2$:**
(à faire)

!!! ex
    Effectuer les opérations suivantes
    
    1. Additions :

        !!! col _2
            ```bash
                1001111
            +     11011
            ------------
            =
            ```
        !!! col _2
            ```bash
                11100101
            +     110011
            ------------
            =
            ```

    1. Soustractions :

        !!! col _2
            ```bash
                10110011
            -     110110
            -------------
            =
            ```
        !!! col _2
            ```bash
                11100101
            -     110011
            ------------
            =
            ```

    1. Multiplications :

        !!! col _3
            ```bash
                10101
            x     110
            -------------
            =
            ```
        !!! col _3
            ```bash
                11101
            x     101
            -------------
            =
            ```
        !!! col _3
            ```bash
                1110011
            x     11011
            ------------
            =
            ```

### Préfixes Binaires vs Décimaux

Une norme publiée en $1998$ impose que les mesures de valeurs informatiques (capacités de mémoires, débits, etc..) utilisent des **préfixes binaires** (càd des puissances de $2$ d'octets):

<center>

| Symbole | Nom <br/>(Préfixe Binaire) | Valeur<br/>Relative | Nombre Exact<br/>Octets ($o$)<br/>(puissances de $2$) | Nombre Exact<br/>d'Octets ($o$) |
| :-: | :-: | :-: | :-: | :-: |
| $Kio$ | Kibi octet | $2^{10}$ octets | $2^{10} \, o$ | $1024$ octets |
| $Mio$ | Mébi octet | $2^{10} \, Kio$ | $2^{20} \, o$ | $1 048 576$ octets |
| $Gio$ | Gibi octet | $2^{10} \, Mio$ | $2^{30} \, o$ | $1 073 741 824$ octets |
| $Tio$ | Tébi octet | $2^{10} \, Gio$ | $2^{40} \, o$ | $1 099 511 627 776$ octets |
| $Pio$ | Pébi octet | $2^{10} \, Tio$ | $2^{50} \, o$ | $1\,125\,899\,906\,842\,624$ octets |
| $Eio$ | Exbi octet | $2^{10} \, Pio$ | $2^{60} \, o$ | $1\,152\,921\,504\,606\,846\,976$ octets |
| $Zio$ | Zébi octet | $2^{10} \, Eio$ | $2^{70} \, o$ | $1\,180\,591\,620\,717\,411\,303\,424$ octets |
| $Yio$ | Yobi octet | $2^{10} \, Zio$ | $2^{80} \, o$ | $1\,208\,925\,819\,614\,629\,174\,706\,176$ octets |

</center>

Néanmoins, certains Systèmes d'Exploitation (Windows) et la grande distribution (dans le commerce) utilisent encore fréquemment des **préfixes décimaux** (puissances de $10$ d'octets) pour mesurer les valeurs informatiques. La confusion entre préfixes binaires et préfixes décimaux est souvent source de confusion pour le client, et d'erreurs (volontaires ou pas) :

<center>

| Symbole | Nom <br/>(Préfixe Décimal) | Valeur Relative | Nombre Exact<br/>d'Octets ($o$)<br/>(puissances de $10$) |Nombre Exact<br/>d'Octets ($o$) |
| :-: | :-: | :-: | :-: | :-: |
| $Ko$ | Kilo octet  | $10^3$ octets| $10^3$ octets  | $1\,000$ octets |
| $Mo$ | Mega octet  | $10^3 \, Ko$ | $10^6 \, o$    | $1\,000\,000$ octets |
| $Go$ | Giga octet  | $10^3 \, Mo$ | $10^9 \, o$    | $1\,000\,000\,000$ octets |
| $To$ | Tera octet  | $10^3 \, Go$ | $10^{12} \, o$ | $1\,000\,000\,000\,000$ octets |
| $Po$ | Péta octet  | $10^3 \, To$ | $10^{15} \, o$ | $1\,000\,000\,000\,000\,000$ octets |
| $Eo$ | Exa octet   | $10^3 \, Po$ | $10^{18} \, o$ | $1\,000\,000\,000\,000\,000\,000$ octets |
| $Zo$ | Zetta octet | $10^3 \, Eo$ | $10^{21} \, o$ | $1\,000\,000\,000\,000\,000\,000\,000$ octets |
| $Yo$ | Yotta octet | $10^3 \, Zo$ | $10^{24} \, o$ | $1\,000\,000\,000\,000\,000\,000\,000\,000$ octets |

</center>

### Nombre de Bits nécessaires

Etant donné un nombre entier $n$ que l'on souhaite écrire en binaire.  
On peut se demander combien de bits sont nécessaires à l'écriture en base $2$ de ce  nombre ?

## Représentation en base $2$ (Binaire) des Entiers Négatifs

### Idée N°1 : Le Bit de Signe (première tentative naïve)

Une première idée simple à laquelle on pourrait penser serait de choisir le premier bit (le bit de poids fort) comme **marqueur de signe**:  

* On fixe le bit de poids fort comme **bit de signe** :
    * Les nombres binaires **commençant par 0** sont **positifs** : $0=$"$+$"
    * Les nombres binaires **commençant par 1** sont **négatifs** : $1=$"$-$"
* La *valeur (absolue)* du nombre étant déterminée par les bits restants après le premier bit de poids fort.

!!! Exemples
    Supposons que nous travaillions sur $4$ bits.  
    Avec cette convention naïve:  

    $$0101 = +5$$

    $$0100 = +4$$

    $$1101 = -5$$

    $$1011 = -3$$

Cette idée simple basée sur le bit de signe pose néanmoins deux problèmes:  

* $0$ admet deux réprésentations distinctes:
    * $0000 = +0$ représente $0$
    * $1000 = -0$ représente également $0$
* les algorithmes des opérations arithmétiques usuelles ($+,-,\times, ÷$ ) ne fonctionnent pas, par exemple on trouverait $3+(-4) = (-7)$ , en effet en posant l'addition:

<center>

$\,\,\,\,0011$  
$+1100$  
$----$  
$=1111$

</center>

Cette idée est donc trop simpliste, on a alors pensé à une autre convention, appelée le **complément à 1** :

### Idée N°2 : Le Complément à $1$

Nous allons commencer par résoudre le problème des opérations arithmétiques, grâce à la méthode dite du **complément à 1**. La convention du complément à $1$ est une deuxième tentative pour stocker les nombres entiers négatifs en mémoire.

!!! def
    Le <rbl>Complément à $1$</rbl> d'un nombre binaire est la valeur obtenue en inversant tous les bits de ce nombre (en permutant les $0$ par des $1$, et réciproquement, les $1$ par des $0$).

!!! exp
    * Le complément à $1$ du nombre binaire $1101_2$ est $\overline{1101}=\overline{1}\,\overline{1}\,\overline{0}\,\overline{1}=0010$
    * Le complément à $1$ du nombre binaire $+0=0000_2$ est $-0=\overline{0000}=\overline{0}\,\overline{0}\,\overline{0}\,\overline{0}=1111$. Ainsi, on peut voir que $+0$ et $-0$ ne sont pas stockés de la même manière avec la convention du complément à 1.

**Tableau Représentant les entiers binaires sur $4$ bits et leurs compléments à $1$:**

| Décimal | Binaire Positif	| Complément à $1$ : 1ère Tentative de Binaire Négatif	| Remarque	|
| :-: | :-: | :-: | :-: |
| $0$ | $0000$ | $1111$ | $+0\neq-0$ donc $0$<br/> admet $2$ représensations distinctes<br/>en complément à $1$ |
| $1$ | $0001$ | $1110$ | |
| $2$ | $0010$ | $1101$ | |
| $3$ | $0011$ | $1100$ | |
| $4$ | $0100$ | $1011$ | |
| $5$ | $0101$ | $1010$ | |
| $6$ | $0110$ | $1001$ | |
| $7$ | $0111$ | $1000$ | |

!!! pte  
    Avec la convention de complément à $1$ [^1]:

    * On dispose encore automatiquement du **bit de signe** sur le bit de poids fort (c'est un avantage) :
        * $0 =$ "$+$"
        * $1 =$ "$-$"
    * Sur $n$ bits, la convention du complément à $1$ est compatible avec les opérations arithmétiques. Cela résout donc l'un de nos deux problèmes précédents (c'est nouveau).
    * **Problème** : Sur $n$ bits, $+0$ et $-0$ sont stockés différemment avec la convention du complément à $1$:
        * $+0 = 00..00$ sur $n$ bits, tandis que
        * $-0 = 11..11$ sur $n$ bits (qui est donc codé comme $2^n-1$)  
        Le nombre entier $0$ admet donc encore $2$ représentations distinctes, ce qui est tout de même un **problème important**, car il force à vérifier deux fois si un résultat est nul.
    * Sur $n$ bits, le complément à $1$ du complément à $1$ d'un entier $x$ vaut $x$ : 
        <center><enc>$comp_1(comp_1(x)) = x$</enc></center>
    * Sur $n$ bits, le complément à $1$ d'un nombre $x$ est le nombre $y$ tel que $x+y = 11..11$,
        Autrement dit: c'est le nombre $y$ tel que $x + y =2^n-1$.
        Autrement dit: c'est le nombre $y$ tel que $y =2^n-1-x$.

On cherche alors une meilleure solution pour résoudre le problème de la double représentation du $0$ :

### Idée N°3 : Le Complément à $2$

Le complément à $2$ d'un nombre entier positif $x$ sert à stocker en mémoire le nombre entier négatif $-x$. Avec un codage sur $n$ bits, cette méthode permet de représenter toutes les valeurs entières de $−2^{n−1}$ à $2^{n−1} − 1$ : 

![Complément à 2, sur n=8 bits](./img/complementA2.png){width=40%}

Historiquement, c'est **[John von Neumann](https://fr.wikipedia.org/wiki/John_von_Neumann)** qui a suggéré l'utilisation de la représentation binaire par complément à deux dans son [premier projet de rapport sur la proposition **EDVAC**](https://en.wikipedia.org/wiki/First_Draft_of_a_Report_on_the_EDVAC) de $1945$ d'un ordinateur numérique électronique à programme enregistré.

!!! def
    * Le <rbl>Complément à $2$</rbl> d'un nombre binaire est le Complément à $1$, auquel **ON AJOUTE 1**.
    * **PAR DÉFINITION, LES DÉBORDEMENTS SONT IGNORÉS**.

!!! exp
    * Pour coder $-5$ sur $8$ bits:  
        * on prend le nombre positif $5 = 00000101$
        * on inverse les bits (complément à $1$) : $11111010$
        * on ajoute $1$ : $-5_2 = 11111011$ (débordement ignoré, mais ici, il n'y a pas de débordement)
    * Pour coder $-0$ sur $8$ bits:
        * on prend le nombre positif $0$ codé sur $8$ bits : $0 = 00000000$
        * on inverse les bits (complément à $1$) : $11111111$
        * on ajoute $1$ : $100000000$ qui devrait être stocké sur $9$ bits. **OR LE DÉBORDEMENT (AU DELÀ DE 8 BITS, VERS LA GAUCHE) EST IGNORÉ**, cela veut dire que le bit de poids fort (ici, le $1$) sera tout simplement ignoré/perdu/oublié, cela veut dire que le résultat final sera stocké seulement sur $8$ bits, c'est à dire $-0_2 = 00000000$. Il y a bien unicité de l'écriture de $0$ avec le convention de complément à $2$

Tableau résumant le complément à 2 des premiers entiers sur $4$ bits:

<center>

|Décimal | Binaire Positif | Complément à 2 : Binaire Négatif	| Remarque	|
| :-:	|:-: |:-:	|:--:	|
| $0$ | $0000$ | $-0 = 0000$ |$+0=-0$ donc $0$<br/> admet bien une **unique** représentation<br/> en Complément à 2	|
| $1$ | $0001$ | $-1=1111$ | |
| $2$ | $0010$ | $-2=1110$ | |
| $3$ | $0011$ | $-3=1101$ | |
| $4$ | $0100$ | $-4=1100$ | |
| $5$ | $0101$ | $-5=1011$ | |
| $6$ | $0110$ | $-6=1010$ | |
| $7$ | $0111$ | $-7=1001$ | |

</center>

!!!mth "Calcul de tête du Complément à $2$"  
    Pour **calculer de tête** le complément à 2 d'un nombre binaire, la méthode est de :

    * partir de la droite et le conserver tel quel jusqu'au premier $1$ INCLUS
    * poursuivre vers la gauche en inversant tous les chiffres

!!! exp "sur $8$ bits"
    Le Complément à 2 de $00101000$ vaut $1101$<rbl>$1000$</rbl>

!!! pte
    La convention de **complément à $2$** possède les propriétés suivantes :

    * On dispose encore automatiquement du **bit de signe** sur le bit de poids fort (c'est un avantage) :
        * $0 =$ "$+$"
        * $1 =$ "$-$"
    * Sur $n$ bits, $00...00$ est l'**unique représentation** de $0$ (c'est nouveau)
    * Sur $n$ bits, le Complément à 2 **conserve les algorithmes des opérations arithmétiques**
    * Sur $n$ bits, le complément à 2 du complément à $2$ d'un entier $x$ vaut $x$ :
    <center><enc>$comp_2(comp_2(x)) = x$</enc></center>
    * Sur $n$ bits, avec la convention du complément à $2$, le nombre $11..11$, représente l'entier $2^n-1$ en base $10$ (et non plus $0$ comme pour le complément à 1).  
    En effet: $11..11 + 1 = 100 ..00 = (2^n)_{10}$. Le résultat $100..00$ étant codé sur $n+1$ bits.  
    * Sur $n$ bits, la somme d'un entier $x$ et de son complément à 2 noté $y$ vaut : $x+y = 2^n$ (puisque le complément à 2 est le complément à $1$ **plus $1$**).
    Autrement dit,
    Sur $n$ bits, Le nombre $-x$ est stocké comme $2^n-x$.

!!! exp
    Sur $n=8$ bits :
    
    * $-1$ est noté comme $2^8-1 = 256-1 = 255 = 128+64+32+16+8+4+2+1 = 11111111$
    * $-9$ est noté comme $2^8-9 = 256-9 = 247 = 128+64+32+16+4+2+1 = 11110111$

## Notes et Références

### Notes

[^1]: [Codage des entiers, IREM de Lyon](http://math.univ-lyon1.fr/irem/Formation_ISN/formation_representation_information/nombre/codage_des_entiers_relatifs.html)

### Références
