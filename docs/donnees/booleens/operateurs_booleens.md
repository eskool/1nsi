# 1NSI : Opérateurs Booléens, Portes Logiques

--8<-- "docs/bo/booleens.md"

## Opérateurs Booléens & Opérandes

### Qu'est-ce qu'un Opérateur ?

De manière générale et intuitive, un **Opérateur** est un concept (plutôt mathématique) qui **opère**/agit sur des données en entrée, appelées **Opérandes**, et qui renvoie en sortie un (unique) résultat (dépendant usuellement des données en entrée).  
Au sens mathématique, un **Opérateur** $\otimes$ (par exemple) peut donc être vu comme une **fonction** $f$ (par exemple) **mais les notations pour la valeur de sortie ne sont pas exactement les mêmes**.  

### Opérateurs et Fonctions

Un **Opérateur** $\otimes$ (par exemple) **opère**/agit sur les **Opérandes** $x$, $y$ (par exemple) donnés en entrée, et renvoie une (unique) donnée en sortie par la fonction $\otimes=f$, que l'on préfère noter $x \otimes y$ (notation d'opérateur) plutôt que $\otimes(x,y)=f(x,y)$ (notation fonctionnelle) :

<center><enc><rbl>$x \otimes y$</rbl> $= \otimes(x,y) = f(x,y)$</enc></center>

De manière plus générale, il peut y avoir un seul ou bien plusieurs opérandes (même plus que $2$).

* Au sens d'un **Opérateur**, il s'agit d'une **opération** entre les différents **opérandes**.
* Au sens **mathématique**, on aurait pu dire que l'image de $(x,y,..)$ par la fonction $\otimes=f$ est $\otimes(x,y,..)=f(x,y,..)$  
* Au sens **informatique**, les opérandes $(x,y,..)$ peuvent être vus comme les **arguments** ou **paramètres** donnés en entrée d'une fonction $f$ (au sens informatique).

!!! exp "Opérateurs Arithmétiques"
    Les quatre **opérations arithmétiques** de base $+, -, \times, \div$ peuvent être vues comme des opérateurs à deux opérandes :

    | Opérateur $\Leftrightarrow$ Fonction<br/>($a$ et $b$ sont les opérandes) | Valeur de Sortie |
    |:-:|:-:|
    |$a+b = +(a,b)$| La Somme de $a$ et $b$ |
    |$a-b = -(a,b)$| La Soustraction de $a$ et $b$ |
    |$a\times b = \times(a,b)$| La Multiplication de $a$ et $b$  |
    |$a\div b = \div(a,b)$| La Division de $a$ et $b$ |

### Opérateurs Booléens

Dans ce contexte, Un **Opérateur Booléen** ou **Opérateur Logique**, correspond au cas particulier où :

* les données en entrée sont des **booléens** (Vrai/True/$1$ ou False/Faux/$0$)
* Le résultat en sortie est également un **booléen**

!!! def "Opérateur Booléens / Logiques & Opérandes"
    Un <rbl>Opérateur Booléen</rbl>, ou <rbl>Opérateur Logique</rbl>, est une *fonction logique* $f$ qui :

    * prend **en entrée** un ou plusieurs booléens/bits (appelés <rbl>Opérandes</rbl>), et
    * produit **en sortie** un (unique) booléen/bit de résultat

L'équivalent au sens mathématique d'un opérateur booléen/logique, serait une fonction dite <rbl>fonction booléenne</rbl> ou <rbl> fonction logique</rbl>, càd une fonction qui reçoit en entrée un/des booléens/bits ($0$ ou $1$) et qui renvoie en sortie un unique booléen/bit ($0$ ou $1$).

!!! exp "L'Opérateur Booléen de la Négation"
    L'Opérateur booléen qui consiste à renvoyer en sortie systématiquement le contraire de ce que l'on a reçu en entrée, est appelé l'**Opérateur de la Négation** :

    * `Vrai` en entrée renvoie `Faux` en sortie
    * `Faux` en entrée renvoie `Vrai` en sortie

## Fonctions Booléennes/Logiques & Tables de Vérité

### Définition

!!! def "Fonctions Booléennes/Logiques & Tables de Vérité"
    Les <rbl>fonctions booléennes</rbl> / <rbl>fonctions logiques</rbl>, sont des fonctions qui prennent en entrée un ou plusieurs bits/valeurs booléennes, et qui produisent en résultat de sortie un unique bit/valeur booléenne. Elles peuvent donc se représenter par une <rbl>Table de Vérité</rbl>.

### Exemples

#### Fonction Booléenne à 2 entrées

Une fonction booléenne $f$ avec $2$ entrées $x$, $y$, sera entièrement définie par une **Table de Vérité** de $2^2=4$ lignes. En effet, il existe $4$ manières d'arranger deux variables $x$ et $y$ prenant chacune des valeurs booléennes ($0$ ou $1$) données en entrée :  
<center>$00$, $01$, $10$ ou bien $11$</center>
Chaque valeur $f(x,y)$ étant ou bien un $0$, ou bien un $1$.

| $x$ | $y$ | $f(x,y)$<br/>($1$ bit) |
|:-:|:-:|:-:|
| $0$ | $0$ | $f(0,0)$ |
| $0$ | $1$ | $f(0,1)$ |
| $1$ | $0$ | $f(1,0)$ |
| $1$ | $1$ | $f(1,0)$ |

!!! ex
    1. Un videur de boîte de nuit a reçu comme consigne de ne laisser passer que les personnes suivant le dress code suivant :

        * une chemise ($x$)
        * ET un pantalon ($y$)

        Établir la Table de Vérité correspondant à cette situation :

        | $x$<br/>(Chemise) | $y$<br/>(Pantalon) | $f(x,y)$<br/>($1$ bit)<br/>(Puis-je entrer ?) |
        |:-:|:-:|:-:|
        | $0$ | $0$ |  |
        | $0$ | $1$ |  |
        | $1$ | $0$ |  |
        | $1$ | $1$ |  |

        ???- corr
            | $x$<br/>(Chemise) | $y$<br/>(Pantalon) | $f(x,y)$<br/>($1$ bit)<br/>(Puis-je entrer ?) |
            |:-:|:-:|:-:|
            | $0$ | $0$ | $0$ |
            | $0$ | $1$ | $0$ |
            | $1$ | $0$ | $0$ |
            | $1$ | $1$ | $1$ |

    1. Le dress code a changé :

        * une chemise ($x$)
        * OU un pantalon ($y$)

        Établir la Table de Vérité correspondant à cette situation :

        | $x$<br/>(Chemise) | $y$<br/>(Pantalon) | $f(x,y)$<br/>($1$ bit)<br/>(Puis-je entrer ?) |
        |:-:|:-:|:-:|
        | $0$ | $0$ |  |
        | $0$ | $1$ |  |
        | $1$ | $0$ |  |
        | $1$ | $1$ |  |

        ???- corr
            | $x$<br/>(Chemise) | $y$<br/>(Pantalon) | $f(x,y)$<br/>($1$ bit)<br/>(Puis-je entrer ?) |
            |:-:|:-:|:-:|
            | $0$ | $0$ | $0$ |
            | $0$ | $1$ | $1$ |
            | $1$ | $0$ | $1$ |
            | $1$ | $1$ | $1$ |

#### Fonction Booléenne à 3 entrées

Une fonction booléenne $f$ avec 3 entrées $x$, $y$ et $z$, sera entièrement définie par une **Table de Vérité** de $2^3=8$ lignes. En effet, il existe $8$ manières d'arranger trois variables $x$, $y$ et $z$ prenant chacune des valeurs booléennes ($0$ ou $1$) données en entrée :  
<center>$000$, $001$, $010$, $011$, $100$, $101$, $110$ ou bien $111$</center>
Chaque valeur $f(x,y,z)$ étant ou bien un $0$, ou bien un $1$.

| $x$ | $y$ | $z$ | $f(x,y,z)$<br/>($1$ bit) |
|:-:|:-:|:-:|:-:|
| $0$ | $0$ | $0$ | $f(0,0,0)$ |
| $0$ | $0$ | $1$ | $f(0,0,1)$ |
| $0$ | $1$ | $0$ | $f(0,1,0)$ |
| $0$ | $1$ | $1$ | $f(0,1,1)$ |
| $1$ | $0$ | $0$ | $f(1,0,0)$ |
| $1$ | $0$ | $1$ | $f(1,0,1)$ |
| $1$ | $1$ | $0$ | $f(1,1,0)$ |
| $1$ | $1$ | $1$ | $f(1,1,1)$ |

!!! ex
    1. Un videur de boîte de nuit a reçu comme consigne de ne laisser passer que les personnes suivant le dress code suivant :

        * une chemise ($x$)
        * ET (un pantalon $y$ OU une jupe $z$)

        Établir la Table de Vérité correspondant à cette situation :

        | $x$<br/>(Chemise) | $y$<br/>(Pantalon) | $z$<br/>(Jupe) | $f(x,y,z)$<br/>($1$ bit)<br/>(Puis-je entrer ?) |
        |:-:|:-:|:-:|:-:|
        | $0$ | $0$ | $0$ | |
        | $0$ | $0$ | $1$ | |
        | $0$ | $1$ | $0$ | |
        | $0$ | $1$ | $1$ | |
        | $1$ | $0$ | $0$ | |
        | $1$ | $0$ | $1$ | |
        | $1$ | $1$ | $0$ | |
        | $1$ | $1$ | $1$ | |

        ???- corr
            | $x$<br/>(Chemise) | $y$<br/>(Pantalon) | $z$<br/>(Jupe) | $f(x,y,z)$<br/>($1$ bit)<br/>(Puis-je entrer ?) |
            |:-:|:-:|:-:|:-:|
            | $0$ | $0$ | $0$ | $0$ |
            | $0$ | $0$ | $1$ | $0$ |
            | $0$ | $1$ | $0$ | $0$ |
            | $0$ | $1$ | $1$ | $0$ |
            | $1$ | $0$ | $0$ | $0$ |
            | $1$ | $0$ | $1$ | $1$ |
            | $1$ | $1$ | $0$ | $1$ |
            | $1$ | $1$ | $1$ | $1$ |

    1. Le dress code a changé :

        * Pantalon Obligatoire pour tout le monde
        * une chemise ($x$) ET des Chaussures ($z$) (avec éventuellement un maillot en dessous)
        * OU (pas de chemise, mais avec maillot ($y$) ET Sans Chaussures)

        Établir la Table de Vérité correspondant à cette situation :

        | $x$<br/>(Chemise) | $y$<br/>(Maillot) | $z$<br/>(Chaussures) | $f(x,y,z)$<br/>($1$ bit)<br/>(Puis-je entrer ?) |
        |:-:|:-:|:-:|:-:|
        | $0$ | $0$ | $0$ | |
        | $0$ | $0$ | $1$ | |
        | $0$ | $1$ | $0$ | |
        | $0$ | $1$ | $1$ | |
        | $1$ | $0$ | $0$ | |
        | $1$ | $0$ | $1$ | |
        | $1$ | $1$ | $0$ | |
        | $1$ | $1$ | $1$ | |

        ???- corr
            | $x$<br/>(Chemise) | $y$<br/>(Maillot) | $z$<br/>(Chaussures) | $f(x,y,z)$<br/>($1$ bit)<br/>(Puis-je entrer ?) |
            |:-:|:-:|:-:|:-:|
            | $0$ | $0$ | $0$ | $0$ |
            | $0$ | $0$ | $1$ | $0$ |
            | $0$ | $1$ | $0$ | $1$ |
            | $0$ | $1$ | $1$ | $0$ |
            | $1$ | $0$ | $0$ | $0$ |
            | $1$ | $0$ | $1$ | $1$ |
            | $1$ | $1$ | $0$ | $0$ |
            | $1$ | $1$ | $1$ | $1$ |

#### Fonction Booléenne à $n$ entrées

!!! pte "Nombres de Lignes d'une Table de Vérité"
    Une Table de Vérité d'une fonction booléenne avec $n$ bits en entrée (le nombre total de colonnes, sauf la dernière colonne du résultat), aura besoin de <enc>$2^n$</enc> lignes (correspondant aux $2^n$ combinaisons possibles avec les $n$ bits en entrée).
