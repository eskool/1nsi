# ANSI : Algèbre de Boole

--8<-- "docs/bo/booleens.md"

## Repères historiques

<center>

![](./img/portrait_boole.jpg)
[George Boole, 1815 - 1864](https://fr.wikipedia.org/wiki/George_Boole)

</center>

En $1847$, le britannique <rbl>George BOOLE</rbl> :gb: inventa un formalisme permettant d'écrire des raisonnements **logiques** : l'<rbl>Algèbre de Boole</rbl>. La notion même d'informatique n'existait pas à l'époque, même si les calculs étaient déjà automatisés (penser à la **Pascaline** de $1642$).

Bien plus tard, en $1938$, les travaux de l'américain <rbl>Claude SHANNON</rbl> :us: prouvèrent que des circuits électriques peuvent résoudre tous les problèmes que l'algèbre de Boole peut elle-même résoudre. Pendant la deuxième guerre mondiale, les travaux du britanique <rbl>Alan TURING</rbl> :gb: puis de l'américano-hongrois <rbl>John VON NEUMANN</rbl> :us: :hu: poseront définitivement les bases de l'informatique moderne.

## Algèbre de Boole

L'algèbre de Boole, ou calcul booléen, est une partie des mathématiques qui propose une vision algébrique/calculatoire à la logigue. 

L'Algèbre de Boole réalise des opérations dans un ensemble qui ne contient que **deux éléments** appelés <rbl>Valeurs Booléennes</rbl> / <rbl>Booléens</rbl> :

* $0$ / FAUX / False (:python:) / $\perp$
* $1$ / VRAI / True (:python:) / $\top$

L'Algèbre de Boole réalise des **Opérations entre un ou deux booléens**, qui sont représentées par 

* des <rbl>Opérateurs Booléens</rbl>, voire même 
* des <rbl>Expressions/Fonctions Booléennes</rbl> qui généralisent les opérations à plus de deux booléens.

Un Opérateur est un symbole qui modélise une opération entre un ou plusieurs éléments. Les **opérations** (booléennes) fondamentales sont :

* la <rbl>Conjonction</rbl> ("ET")
* la <rbl>Disjonction</rbl> ("OU")
* la <rbl>Négation</rbl> ("NON")

Dans toute la suite, `x` et `y` désigneront des <rbl>Booléens</rbl> quelconques, `F` désignera FAUX et `V` désignera VRAI.