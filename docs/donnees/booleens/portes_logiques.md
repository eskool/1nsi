# 1NSI : Portes Logiques

<iframe width="560" height="315" src="https://www.youtube.com/embed/iTH39L2d7bg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<center><figcaption>

Les Portes Logiques (par :youtube: codeur-pro)

</figcaption></center>

## Portes Logiques

!!! def
    Une <rbl>Porte Logique</rbl> :fr: / <rbl>Logic Gate</rbl> :gb: est une implémentation matérielle d'un **Opérateur Booléen**, càd un **circuit électronique élémentaire** implémentant la même fonctionnalité précise et distincte, que l'opérateur booléen correspondant.

Il existe plusieurs Portes Logiques, chacune d'entre elles ayant des fonctionnalités basiques, précises et distinctes. Ces Portes Logiques sont donc des circuits (électroniques) qui :

* acceptent en **entrée** un ou des **signaux logiques** ($0$ ou $1$) présentés à leurs entrées sous forme de tensions. Par exemple :
    * $5V$ pour représenter l'état logique $1$
    * $0V$ pour représenter l'état logique $0$
* renvoie en **sortie** un signal logique ($0$ ou $1$)

!!! def "Circuits Combinatoires"
    Ce type de circuits électroniques, dont la sortie ne dépend QUE des valeurs booléennes/bits en entrée est appelé un <rbl>circuit combinatoire</rbl>

Ce cours traite principalement de quelques circuits combinatoires classiques.

!!! exp
    Un simple transistor permet de réaliser le circuit électronique élémentaire appelé <rbl>Porte Logique NOT (NON)</rbl> ou <rbl>Porte Logique de la Négation</rbl>, ou <rbl>Inverseur</rbl>.

Voici les caractéristiques des **portes logiques les plus usuelles**.

## Porte NOT ( NON ) / Inverseur

### Définition

!!! def
    La Porte Logique <rbl>NOT (NON)</rbl>, ou <rbl>Inverseur</rbl>, ou Opérateur de <rbl>Négation</rbl>, est défini par la phrase suivante : 

    <center><enc>La sortie est VRAI si et seulement si l'unique entrée est FAUX</enc></center>


### Circuits et Schémas

La Porte NOT est implantée par un simple transistor : c'est la plus simple de toutes les portes, et c'est la seule **porte logique unaire** (avec une seule entrée/opérande)

![Symbole USA Porte NOT](./img/porteNOT_USA.svg){.center}
![Symbole USA Porte NOT](./img/porteNOT_Europe.svg){.center}
![Symbole USA Porte NOT](./img/porteNOT_Europe_Bis.svg){.center}
![circuit et Schémas d'une Porte NOT](./img/Porte_NOT.png){.center}
![circuit et Schémas d'une Porte NOT](./img/porteNOT_USA.png){.center}

!!! not "NOT"
    On note $S=not(A)=not$ $A=\overline{A}=\neg A$

### Table de Vérité

La Porte NOT n'admet qu'un seul bit en entrée (A) et les résultats de son unique sortie (S) sont résumés dans sa **Table de Vérité:**

| $A$  | $S=not$ $A$<br/>$=\overline{A}$<br/>$=\neg A$ |
|:-:|:-:|
| $0$ | $1$ |
| $1$ | $0$ |

### Réalisation Pratique

!!! col __70
    En pratique on regroupe souvent plusieurs portes logiques à l'intérieur de <rbl>Circuits Intégrés (CI)</rbl>, comme ici avec l'exemple du Circuit Intégré 7404 (Panasonic) qui fait partie de la **[Série des circuits imprimés 7400](https://fr.wikipedia.org/wiki/Liste_des_circuits_int%C3%A9gr%C3%A9s_de_la_s%C3%A9rie_7400)**, utilisant la technologie TTL (Transistor-Transistor Logic). Le Circuit Intégré 7404 regroupe $6$ portes NOT, deux broches d'alimentation (la terre GND sur l'une, et le courant continu en V -Vcc- sur l'autre), dans un même circuit :

!!! col __30
    ![Photo Circuit Intégré 4 Portes Logiques NOT, CI 7404, Panasonic, Japan](./img/CI_7404.jpg)
    ![Schéma de Circuit Intégré 4 Portes Logiques NOT, CI 7404, Panasonic; Japan](./img/CI_7404_schema.svg)

!!! exp "de Circuits Intégrés"
    * CMOS : 4009, 4049
    * TTL : 7404, 7405, 7406, 7416. 

## Porte AND ( ET ) / Conjonction

### Définition

!!! def
    La Porte Logique <rbl>AND (ET)</rbl>, ou Opérateur de <rbl>Conjonction</rbl>, est défini par la phrase suivante :

    <center><enc>La sortie est VRAI si et seulement si l'une ET l'autre entrées sont VRAI (simultanément)  
    $\Leftrightarrow$ les deux entrées sont VRAI (simultanément)</enc></center>

### Circuits et Schémas

Une Porte AND peut être implanté avec $2$ portes NOT et une porte NOR.

![circuit et Schémas Porte AND](./img/Porte_AND.png){.center}

!!! not "AND"
    On note $S = and(A,B) = A$ $and$ $B= A.B=$ $A\land B$

La porte NOT peut être construite à partir d'une porte NAND en reliant les deux entrées de cette porte:

![Porte NOT avec un NAND](./img/Porte_NOT_avec_NAND.png){.center}

### Table de Vérité

La Porte AND (ET) admet deux valeurs en entrée (notées $A$ et $B$), et une unique valeur de Sortie ($S$).

| $A$  | $B$ | $S=A$ $and$ $B$ <br/>$=A.B$  |
|:-:|:-:| :-:|
| $0$  | $0$ | $0$ |
| $0$  | $1$ | $0$ |
| $1$  | $0$ | $0$ |
| $1$  | $1$ | $1$ |

### Réalisation Pratique

!!! col __70
    En pratique on regroupe souvent plusieurs portes logiques à l'intérieur de <rbl>Circuits Intégrés (CI)</rbl>, comme ici avec l'exemple du Circuit Intégré 7408 (Texas Instruments) regroupant $4$ portes AND, deux broches d'alimentation (la terre GND sur l'une, et le courant continu en V -Vcc- sur l'autre), dans un même circuit :

!!! col __30 clear
    ![Photo Circuit Intégré 4 Portes Logiques AND, CI 7408, Texas Instruments](./img/CI_7408.jpg)
    ![Schéma de Circuit Intégré 4 Portes Logiques AND, CI 7408, Texas Instruments](./img/CI_7408_schema.svg)

!!! exp "de Circuits Intégrés"
    * CMOS : 4073, 4081, 4082,
    * TTL : 7408, 7409, 7411, 7415, 7421

### Propriétés

!!! pte "de la Porte AND ($.$)"
    L'opérateur AND vérifie les propriétés suivantes :

    | Propriété | Nom |
    |:-:|:-:|
    | $a.b=b.a$ | Commutatif |
    | $(a.b).c=a.(b.c)=a.b.c$ | Associatif |
    | $a.(b+c) = a.b+a.c$ | Distributif sur $+$ (OR) |

    | Identités<br/>Remarquables |
    |:-:|
    | $a.1=a$ |
    | $a.0=0$ |
    | $a.a=a$ |
    | $a.\overline{a}=0$ |

## Porte OR ( OU ) / Disjonction

### Définition

!!! def
    La Porte Logique <rbl>OR (OU)</rbl>, ou Opérateur de <rbl>Disjonction</rbl>, est défini par la phrase suivante :

    <center><enc>La sortie est VRAI si et seulement si l'une OU l'autre des entrées est VRAI  
    $\Leftrightarrow$ au moins l'une des entrées est VRAI</enc></center>

### Circuits et Schémas

![circuit et Schémas Porte OR](./img/Porte_OR.png){.center}

!!! not "OR"
    On note $S = or(A,B) = A$ $or$ $B=A+B=$ $A\lor B$

### Table de Vérité

La Porte OR (OU) admet deux valeurs en entrée (notées $A$ et $B$), et une unique valeur de Sortie ($S$).

| $A$  | $B$ | $S=A$ $or$ $B$ <br/>$=A+B$  |
|:-:|:-:| :-:|
| $0$  | $0$ | $0$ |
| $0$  | $1$ | $1$ |
| $1$  | $0$ | $1$ |
| $1$  | $1$ | $1$ |

### Réalisation Pratique

!!! col __70
    En pratique on regroupe souvent plusieurs portes logiques à l'intérieur de <rbl>Circuits Intégrés (CI)</rbl>, comme ici avec l'exemple du Circuit Intégré 7432 (Motorola) regroupant $4$ portes OR, deux broches d'alimentation (la terre GND sur l'une, et le courant continu en V -Vcc- sur l'autre), dans un même circuit :

!!! col __30
    ![Photo Circuit Intégré 4 Portes Logiques OR, CI 7432, Motorola](./img/CI_7432.jpg)
    ![Schéma de Circuit Intégré 4 Portes Logiques OR, CI 7432, Motorola](./img/CI_7432_schema.svg)

!!! exp "de Circuits Intégrés"
    * CMOS : 4071, 4072, 4075
    * TTL : 7432

### Propriétés

!!! pte "de la Porte OR ($+$)"
    L'opérateur OR vérifie les propriétés suivantes :

    | Propriété | Nom |
    |:-:|:-:|
    | $a+b=b+a$ | Commutatif |
    | $(a+b)+c=a+(b+c)=a+b+c$ | Associatif |
    | $a+(b.c) = a+b.a+c$ | Distributif sur $.$ (ET) |

    | Identités<br/>Remarquables |
    |:-:|
    | $a+1=1$ |
    | $a+0=a$ |
    | $a+a=a$ |
    | $a+\overline{a}=1$ |

## Porte NAND ( NON-ET )

### Définition

!!! def
    La Porte Logique <rbl>NAND (NOT-AND / NON-ET)</rbl>, ou Opérateur de <rbl>Négation de la Conjonction</rbl>, est défini par la phrase suivante :

    <center><enc>La sortie est VRAI si et seulement si PAS TOUTES les entrées ne sont VRAI (simultanément)  
    $\Leftrightarrow$ au plus une des entrées est VRAI  
    $\Leftrightarrow$ au moins une des entrées est FAUX</enc></center>

### Circuits et Schémas

Deux transistors en série constituent une porte NAND / NOT-AND / NON-ET.

![circuit et Schémas Porte NAND](./img/Porte_NAND.png){.center}

!!! not "NAND"
    On note $S=nand(A,B)=A$ $nand$ $B=\overline{A\land B}=\overline{A.B}=\neg (A \land B)=A \uparrow B$

### Table de Vérité

La Porte NAND (NON-ET) admet deux valeurs en entrée (notées $A$ et $B$), et une unique valeur de Sortie ($S$).

| $A$  | $B$ | $S=A$ $nand$ $B$ <br/>$=\overline{A \wedge B}$ <br/>$=\overline{A.B}$<br/>$=A \uparrow B$  |
|:-:|:-:| :-:|
| $0$  | $0$ | $1$ |
| $0$  | $1$ | $1$ |
| $1$  | $0$ | $1$ |
| $1$  | $1$ | $0$ |

### Réalisation Pratique

!!! col __70
    En pratique on regroupe souvent plusieurs portes logiques à l'intérieur de <rbl>Circuits Intégrés (CI)</rbl>, comme ici avec l'exemple du Circuit Intégré 7400 (Texas Instruments) regroupant $4$ portes NAND, deux broches d'alimentation (la terre GND sur l'une, et le courant continu en V -Vcc- sur l'autre), dans un même circuit :

!!! col __30
    ![Photo Circuit Intégré 4 Portes Logiques OR, CI 7400, Texas Instruments](./img/CI_7400.jpg)

!!! exp "de Circuits Intégrés"
    * CMOS : 4011, 4012, 4023, 4068, 4093
    * TTL : 7400, 7401, 7403, 7410, 7430, 74133

### Universalité

!!! pte "NAND est Universelle"
    La Porte Logique NAND (NON ET) est dite <rbl>universelle</rbl> / <rbl>complète</rbl>, ce qui veut dire qu'elle permet de reconstituer (à elle seule) toutes les autres portes logiques.

C'est une propriété très importante car, le circuit électronique CMOS de la fonction NAND étant des plus simples, la fonction NAND sert souvent de « brique de base » à des circuits intégrés beaucoup plus complexes.

!!! ex "Universalité de NAND ($\uparrow$)"
    1. Grâce à des Tables de Vérité, montrer les égalités suivantes :

        ||
        |:-:|
        | $\neg A = A\uparrow A$ |
        | $A . B = (A\uparrow B)\uparrow (A\uparrow B)$ |
        | $A + B = (A\uparrow A)\uparrow (B\uparrow B)$ |

    1. Quels circuits électroniques (n'utilisant que des NAND) peut-on inventer pour implanter la Porte NOT ? 

        ??? corr
            La porte NOT peut être construite à partir d'une porte NAND en reliant les deux entrées de cette porte:

            ![Porte NOT avec un NAND](./img/Porte_NOT_avec_NAND.png){.center}

    1. idem pour la Porte ET?

        ??? corr
            La porte AND peut être construite à partir d'une porte NAND en reliant les deux entrées de cette porte:

            ![Porte AND avec un NAND](./img/AND_from_NAND.png){.center}

    1. idem pour la Porte OR?

        ??? corr
            La porte OR peut être construite à partir d'une porte NAND en reliant les deux entrées de cette porte:

            ![Porte OR avec un NAND](./img/OR_from_NAND.png){.center}

### Propriétés

!!! pte "de la Porte NAND (NON-ET)"
    L'opérateur NAND vérifie les propriétés suivantes :

    | Propriété | Nom |
    |:-:|:-:|
    | $\overline{a.b}=\overline{b.a}$ | Commutatif |
    || NON ASSOCIATF |

    | Identités<br/>Remarquables |
    |:-:|
    | $\overline{a.1}=\overline{a}$ |
    | $\overline{a.0}=1$ |
    | $\overline{a.a}=\overline{a}$ |
    | $\overline{a.\neg a}=1$ |

## Porte NOR ( NON-OU )

### Définition

!!! def
    La Porte Logique <rbl>NOR (NOT-OR / NON-OU)</rbl>, ou Opérateur de <rbl>Négation de la Disjonction</rbl>, ou *NI/NI*, est défini par la phrase suivante :

    <center><enc>La sortie est VRAI si et seulement si AUCUNE des entrées n'est VRAI  
    $\Leftrightarrow$ les deux entrées sont FAUX  
    $\Leftrightarrow$ NI l'une NI l'autre des entrées n'est VRAI</enc></center>

### Circuits et Schémas

Deux transistors en parallèles constituent une porte NOR / NOT-OR / NON-OU.

![circuit et Schémas Porte NOR](./img/Porte_NOR.png){.center}

!!! not "NOR"
    On note $S = nor(A,B) =  A$ $nor$ $B=\overline{A+B}=\overline{A\lor B}=A \downarrow B$

### Table de Vérité

La Porte NOR (NON-OU) admet deux valeurs en entrée (notées $A$ et $B$), et une unique valeur de Sortie ($S$).

| $A$  | $B$ | $S=A$ $nor$ $B$ <br/>$=\overline{A \lor B}$ <br/>$=\overline{A+B}$<br/>$=a\downarrow B$  |
|:-:|:-:| :-:|
| $0$  | $0$ | $1$ |
| $0$  | $1$ | $0$ |
| $1$  | $0$ | $0$ |
| $1$  | $1$ | $0$ |

### Universalité

!!! pte "NOR est Universelle"
    La Porte Logique NOR (NON OU) est dite <rbl>universelle</rbl> / <rbl>complète</rbl>, ce qui veut dire qu'elle permet de reconstituer (à elle seule) toutes les autres portes logiques.

### Propriétés

!!! pte "de la Porte NOR (NON-OU)"
    L'opérateur NOR vérifie les propriétés suivantes :

    | Propriété | Nom |
    |:-:|:-:|
    | $\overline{a+b}=\overline{b+a}$ | Commutatif |
    || NON ASSOCIATF |

    | Identités<br/>Remarquables |
    |:-:|
    | $\overline{a+1}=0$ |
    | $\overline{a+0}=\overline{a}$ |
    | $\overline{a+a}=\overline{a}$ |
    | $\overline{a+\neg a}=0$ |

## Porte XOR (OU EXCLUSIF) / Différence

### Définition

!!! def
    La Porte Logique <rbl>XOR (Exclusive OR / OU EXCLUSIF)</rbl>, ou Opérateur de <rbl>Différence</rbl>, ou Opérateur de <rbl>Disjonction Exclusive</rbl>, est défini par la phrase suivante :

    <center><enc>La sortie est VRAI si et seulement si les entrées NE sont PAS égales entre elles  
    $\Leftrightarrow$ L'une des entrées EXCLUSIVEMENT est VRAI (pas les deux)</enc></center>

### Circuits et Schéma

![circuit et Schémas Porte XOR](./img/Porte_XOR_avec_NOR.png){.center}

![Schéma Porte XOR avec des NAND](./img/Porte_XOR_avec_NAND.png){.center}

!!! not "XOR"
    On note $S= A$ $xor$ $B = A\oplus B$

### Table de Vérité

La Porte XOR (OU EXCLUSIF) admet deux valeurs en entrée (notées $A$ et $B$), et une unique valeur de Sortie ($S$).

| $A$  | $B$ | $S=A$ $xor$ $B$<br/>$A\oplus B$ |
|:-:|:-:|:-:|
| $0$ |$0$ | $0$ |
| $0$ |$1$ | $1$ |
| $1$ |$0$ | $1$ |
| $1$ |$1$ | $0$ |

### Réalisation Pratique

!!! col __70
    En pratique on regroupe souvent plusieurs portes logiques à l'intérieur de <rbl>Circuits Intégrés (CI)</rbl>, comme ici avec l'exemple du Circuit Intégré 7486 (Texas Instruments) regroupant $4$ portes XOR, deux broches d'alimentation (la terre GND sur l'une, et le courant continu en V -Vcc- sur l'autre), dans un même circuit :

!!! col __30
    ![Photo Circuit Intégré 4 Portes Logiques OR, CI 7486, Texas Instruments](./img/CI_7486.jpg)
    ![Schéma du Circuit Intégré 4 Portes Logiques OR, CI 7486, Texas Instruments](./img/CI_7486_schema.png)

!!! exp "de Circuits Intégrés"
    * CMOS : 4030, 4070
    * TTL : 7486, 74136

### Propriétés

!!! ex "Table de Vérité"
    1. Remplir la Table de Vérité Suivante :

        | $A$  | $B$ | $A \lor B$ | $\neg (A \land B)$ | $(A \lor B)\land (\neg (A \land B))$ |
        |:-:|:-:|:-:|:-:|:-:|
        | $0$ |$0$ | $\,$ | $\,$ | $\,$ |
        | $0$ |$1$ | $\,$ | $\,$ | $\,$ |
        | $1$ |$0$ | $\,$ | $\,$ | $\,$ |
        | $1$ |$1$ | $\,$ | $\,$ | $\,$ |
    
    1. Que constatez-vous ?

!!! pte "de la Porte XOR (OU-EXCLUSIF)"
    L'opérateur XOR vérifie les propriétés suivantes :

    | Propriété | Nom |
    |:-:|:-:|
    | $a\oplus b=b\oplus a$ | Commutatif |
    | $(a\oplus b)\oplus c=a\oplus (b\oplus c$ <br/>$=a\oplus b\oplus c$ | Associatif |

    | Identités<br/>Remarquables |
    |:-:|
    | $\overline{a\oplus 1}=\overline{a}$ |
    | $\overline{a\oplus 0}=a$ |
    | $\overline{a\oplus a}=0$ |
    | $\overline{a\oplus \neg a}=1$ |

### Applications du XOR

#### Addition Binaire modulo $2$

Le XOR (OU EXCLUSIF) peut être vu comme une sorte d'addition binaire modulo $2$ :

* Les $3$ premières lignes de la Table de Vérité se comportent comme une Addition Binaire :
    * $0+0=0$
    * $0+1=1$
    * $1+0=1$
* La dernière ligne de la Table de Vérité se comporte *presque* comme une addition Binaire, mais SANS TENIR COMPTE de la retenue (on l'oublie), ce qui revient au même que dire "*modulo $2$*" :
    * $1+1 = 2_{10} = 10_2$ donc:
        * $0$ en binaire
        * mais sans tenir compte de la retenue $1$. Il revient au même de dire que $2$ modulo $2$ vaut $0$

#### Demi-Additioneur 1 bit

La propriété précédente (d'addition binaire modulo $2$) est utile et utilisée pour créer un demi-additioneur 1 bit, càd un additionneur 1 bit **SANS TENIR COMPTE DE LA RETENUE**. Cf [cette page]() du cours sur les circuits électroniques.

#### Additioneur 1 bit

La propriété précédente (d'addition binaire modulo $2$) est utile et utilisée pour créer un additioneur 1 bit, càd un additionneur 1 bit **EN TENANT COMPTE DE LA RETENUE**. Cf [cette page]() du cours sur les circuits électroniques.

#### En Cryptographie

Le XOR joue un rôle fondamental en Cryptographie car il possède une propriété très intéressante : 
$(x\land y)\land y=x$

Si $x$ est un message et $y$ une clé de chiffrage, alors $x\land y$ est le message chiffré.
Mais en refaisant un XOR du message chiffré avec la clé $y$, on retrouve donc le message $x$ initial.
En pratique, cette méthode est souvent utilisée avec une clé $y$ à usage unique, càd avec la **technique du masque jetable**.

## Porte XNOR (NON-OU EXCLUSIF) / Coïncidence

### Définition

!!! def
    La Porte Logique <rbl>XNOR (NON-OU EXCLUSIF)</rbl>, ou Opérateur de <rbl>Coïncidence</rbl>, ou Opérateur de la <rbl>Négation de la Disjonction Exclusive</rbl>, est défini par la phrase suivante :

    <center><enc>La sortie est VRAI si et seulement si les deux entrées sont identiques (coïncident)</enc></center>

Il s'agit de la Négation du XOR (il faudrait dire NXOR pour NOT-XOR)

### Circuits et Schémas

!!! col __33 center
    ![Schémas Porte XNOR Transistors](./img/porteXNOR_avec_Transistors.png){.center}
    Porte XNOR avec Transistors

!!! col __33 center
    ![Schémas Porte XNOR US - ANSI](./img/porteXNOR_ANSI.svg){.center}
    Porte XNOR USA

!!! col __33 center
    ![Schémas Porte XNOR Europe](./img/porteXNOR_Europe.svg){.center}
    Porte XNOR Europe

!!! not "XNOR"
    On note $S= A$ $xnor$ $B = A\odot B$

### Table de Vérité

La Porte XNOR (NON-OU EXCLUSIF) admet deux valeurs en entrée (notées $A$ et $B$), et une unique valeur de Sortie ($S$).

| $A$  | $B$ | $S=A$ $xnor$ $B$<br/>$A\odot B$<br/>Coïncidence |
|:-:|:-:|:-:|
| $0$ |$0$ | $1$ |
| $0$ |$1$ | $0$ |
| $1$ |$0$ | $0$ |
| $1$ |$1$ | $1$ |

### Réalisation Pratique

!!! col __70
    En pratique on regroupe souvent plusieurs portes logiques à l'intérieur de <rbl>Circuits Intégrés (CI)</rbl>, comme ici avec l'exemple du [Circuit Intégré 74HC266 (Texas Instruments)](https://www.ti.com/lit/ds/symlink/sn74hc266.pdf?HQS=dis-dk-null-digikeymode-dsf-pf-null-wwe&ts=1656245960792&ref_url=https%253A%252F%252Fwww.ti.com%252Fgeneral%252Fdocs%252Fsuppproductinfo.tsp%253FdistId%253D10%2526gotoUrl%253Dhttps%253A%252F%252Fwww.ti.com%252Flit%252Fgpn%252Fsn74hc266) utilisant la technologie TTL, et regroupant $4$ portes XNOR, deux broches d'alimentation (la terre GND sur l'une, et le courant continu en V -Vcc- sur l'autre), dans un même circuit :

!!! col __30 clear
    ![Photo Circuit Intégré 4 Portes Logiques OR, CI SN74HC266, Texas Instruments](./img/CI_74266.png)

### Propriétés

!!! pte
    XNOR est la Négation de XOR : <enc>$A\odot B=\overline{A\oplus B}$</enc>

![Schéma Porte XNOR avec des NAND](./img/porteXNOR_from_NAND.svg){.center}

<center>Porte XNOR avec des Portes NAND</center>

!!! ex "Table de Vérité"
    1. Remplir la Table de Vérité Suivante :

        | $A$  | $B$ | $A.B$ | $\overline{A}.\overline{B}$ | $A.B + \overline{A}.\overline{B}$ |
        |:-:|:-:|:-:|:-:|:-:|
        | $0$ |$0$ | $\,$ | $\,$ | $\,$ |
        | $0$ |$1$ | $\,$ | $\,$ | $\,$ |
        | $1$ |$0$ | $\,$ | $\,$ | $\,$ |
        | $1$ |$1$ | $\,$ | $\,$ | $\,$ |
    
    1. Que constatez-vous ?

!!! pte "de la Porte XNOR (NON-OU EXCLUSIF)"
    L'opérateur XNOR vérifie les propriétés suivantes :

    | Propriété | Nom |
    |:-:|:-:|
    | $a\odot b=b\odot a$ | Commutatif |
    | $a\odot (b\odot c) = (a\odot b)\odot c$<br/>$=a\odot b \odot c$ | Associatif |

    | Identités<br/>Remarquables |
    |:-:|
    | $a\odot 1=a$ |
    | $a\odot 0=\overline{a}$ |
    | $a\odot a=1$ |
    | $a\odot \overline{a}=0$ |

!!! ex
    Renseigner les Tables de Vérité suivantes :

    1. Comparer avec XOR ($\oplus$) :

        | $A$  | $B$ | $S=\overline{A\, xor\, B}$<br/>$\overline{A\oplus B}$ |
        |:-:|:-:|:-:|
        | $0$ |$0$ | $1$ |
        | $0$ |$1$ | $0$ |
        | $1$ |$0$ | $0$ |
        | $1$ |$1$ | $1$ |

        En déduire une comparaison entre $\overline{A\oplus B}$ et $A\odot B$

    1. Idem avec $AB+\overline{A}.\overline{B}$

        | $A$  | $B$ | $S=\overline{A\, xor\, B}$<br/>$\overline{A\oplus B}$ |
        |:-:|:-:|:-:|
        | $0$ |$0$ | $1$ |
        | $0$ |$1$ | $0$ |
        | $1$ |$0$ | $0$ |
        | $1$ |$1$ | $1$ |
        
        En déduire une comparaison entre $\overline{A\oplus B}$ et $A\odot B$






## Notes et Références

### Notes

### Références

* [Cours Portes Logiques, courstechinfo.be](http://www.courstechinfo.be/Techno/PortesLogiques.html)
* [Portes Logiques, Univ du Sénégal](http://unis.sn/wp-content/uploads/2019/10/04-Les-portes-logiques.pdf)
* [Les Portes et Fonctions Logiques, codeur-pro.fr](https://codeur-pro.fr/les-portes-et-fonctions-logiques/)
* Portes Logiques sur Wikipedia:
    * [Fonction NON](https://fr.wikipedia.org/wiki/Fonction_NON)
    * [Fonction NAND / NON ET](https://fr.wikipedia.org/wiki/Fonction_NON-ET)
    * [Fonction NOR / NON OU](https://fr.wikipedia.org/wiki/Fonction_NON-OU)
    * [Fonction AND / ET](https://fr.wikipedia.org/wiki/Fonction_ET)
    * [Fonction OR / OU](https://fr.wikipedia.org/wiki/Fonction_OU)
    * [Fonction XOR / OU EXCLUSIF](https://fr.wikipedia.org/wiki/Fonction_OU_exclusif)
    * [Fonction XNOR / NON-OU EXCLUSIF / Coïncidence](https://fr.wikipedia.org/wiki/Co%C3%AFncidence_(informatique))
    * [Lois de Morgan](https://fr.wikipedia.org/wiki/Lois_de_De_Morgan)
* Porte XNOR :
    * [XNOR Gate, wikipedia](https://en.wikipedia.org/wiki/XNOR_gate) :gb:
* Positron-libre:
    * [Fonctions Logiques](https://www.positron-libre.com/cours/electronique/logique-combinatoire/fonctions-logiques/)
    * [Tableau Résumé des Fonctions Logiques](https://www.positron-libre.com/cours/electronique/logique-combinatoire/fonctions-logiques/tableau-fonction-logique.php)
* Université Eiffel:
    * [Formules Logiques en Haskell](http://igm.univ-mlv.fr/~vialette/teaching/2020-2021/Haskell/project/project.pdf)
* Transistors & Portes Logiques, IRIF](https://www.irif.fr/~carton/Enseignement/Architecture/Cours/Gates/)
* [Logique Booléenne](https://www.arte.tv/fr/videos/105438-000-A/george-boole-genie-des-maths/) Vidéo Arte :gb: (59 min 30)