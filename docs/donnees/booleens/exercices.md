# 1NSI :  Exercices sur les Booléens

!!! ex
    Comprendre ce mème :
    ![mème](img/drake.webp){: .center width=50%}

!!! ex
    1. Ouvrir le [simulateur de circuits](http://dept-info.labri.fr/ENSEIGNEMENT/archi/circuits/blank-teacher.html){. target="_blank"} et créer pour chaque opération AND, OR, NOT un circuit électrique illustrant ses propriétés.

    Exemple (inintéressant) de circuit :
    ![](data/ex_circuit.png){: .center}

    2. Utiliser successivement les circuits XOR, NAND et NOR et établir pour chacun leur table de vérité.

!!! ex
    Calculer les opérations suivantes :

    ```python
      1011011
    & 1010101
    ----------
    

      1011011
    | 1010101
    ----------
    

      1011011
    ^ 1010101
    ----------
    
    ```

    ??? corr "solution"
        ```python
          1011011
        & 1010101
        ----------
          1010001
        
          1011011
        | 1010101
        ----------
          1011111
        
          1011011
        ^ 1010101
        ----------
          0001110
        ```

!!! ex "Opérateurs ET, OU, OU EXCLUSIF en Python :python:"
    Les opérateurs `&`, `|` et `^` sont utilisables directement en Python
    Testez-les syntaxes suivantes et vérifier les résultats :

    ```python
    # calcul A
    >>> 12 & 7
    4
    ```


    ```python
    # calcul B
    >>> 12 | 7
    15
    ```

    ```python
    # calcul C
    >>> 12 ^ 5
    9
    ```

    ???- corr
        Pour comprendre ces résultats, il faut travailler en binaire. Voici les mêmes calculs :

        ```python
        # calcul A
        >>> bin(0b1100 & 0b111)
            '0b100'
        ```

        ```python
        # calcul B
        >>> bin(0b1100 | 0b111)
        '0b1111'
        ```

        ```python
        # calcul C
        >>> bin(0b1100 ^ 0b111)
            '0b1011'
        ```

!!! ex "XOR et Cryptographie (Préparation du Pydéfi 'La Clé Endommagée')"
    Chiffrer (= crypter) le mot "BONJOUR" avec la clé (de même taille) "MAURIAC".  

    Protocole de chiffrage : XOR entre le code ASCII des lettres de même position.

    ???- corr
        ```python 
        msg = "BONJOUR"
        cle = "MAURIAC"

        def crypte_lettre(lm, lc):
            a = ord(lm)
            b = ord(lc)
            c = a^b
            lettre = chr(c)

            return lettre

        def crypte_mot(mot1, mot2):
            mot3 = ""
            for i in range(len(mot1)):
                car = crypte_lettre(mot1[i],mot2[i])
                mot3 = mot3 + car
            return mot3

        crypte_mot(msg, cle)
        ```
            '\x0f\x0e\x1b\x18\x06\x14\x11'

!!! ex "La Clé Endommagée, :sk-copyleft: [PyDéfis par Snarkturne](https://pydefis.callicode.fr/defis/MasqueJetable/txt)"
    Vous avez été récemment contacté par un de vos amis agent secret. Un message codé vous a été remis, sous la forme d'une liste de nombres. Ce code a été obtenu en utilisant la [méthode du masque jetable](https://fr.wikipedia.org/wiki/Masque_jetable). Voici comment l'appliquer.

    Pour chiffrer le texte "RENDEZVOUSICI" avec la clé [106, 204, 99, 53, 152, 30, 68, 27, 100, 249, 40, 94, 223], on effectue des opérations «ou exclusif» entre le code ASCII des lettres du texte et le nombre correspondant dans la clé.

    Les codes ASCII de R, E et N sont respectivement 82, 69 et 78 ([Page Ascii sur Wikipédia](https://fr.wikipedia.org/wiki/American_Standard_Code_for_Information_Interchange)). En conséquence, le premier nombre du message chiffré sera le résultat d'un ou exclusif entre 106 et 82 c'est à dire 56 ([Page Xor sur Wikipédia](https://fr.wikipedia.org/wiki/Fonction_OU_exclusif)). Le deuxième nombre sera un ou exclusif entre 204 et 69 (on trouve 137) et le troisième entre 99 et 78 (on trouve 45). En procédant ainsi pour les 13 caractères, on obtient le message chiffré : [56, 137, 45, 113, 221, 68, 18, 84, 49, 170, 97, 29, 150]

    L'opération de déchiffrement est identique. Un ou exclusif entre le premier nombre du message chiffré et le premier nombre de la clé redonne 82, qui est le code ASCII de R. Un ou exclusif entre le second nombre du message chiffré et le second nombre de la clé donne 68, le code ASCII de E...

    On peut donc chiffrer et déchiffrer tant qu'on connaît la bonne suite de nombre qui constitue la clé. La clé est très longue : aussi longue que le message à chiffrer (c'est une des caractéristiques de la méthode du masque jetable). Malheureusement pour vous, bien que conserviez habituellement très précieusement les fichiers de clés, celui-ci a été endommagé (cyber attaque ?). Un spécialiste [forensic](https://fr.wikipedia.org/wiki/Informatique_l%C3%A9gale) a néanmoins pu analyser le disque défectueux et récupérer une portion de la clé :

    ```python
    cle = [141, 78, 245, 94, 220, 246, 225, 56, 170, 28, 138, 174, 121, 18, 108, 209, 133, 205, 202, 94, 176, 15, 4, 66, 96, 4, 86, 131, 222, 175, 249, 145, 133, 88, 83, 103, 67, 252, 80, 143]
    ```

    Et le message à déchiffrer est le suivant :

    ```python
    msg = [160,68,222,209,99,2,242,45,250,206,141,103,170,129,122,115,207,169,145,242, 251,76,141,47,1,160,168,235,189,239,19,197,191,37,154,15,52,154,75,29, 21,25,56,242,251,100,68,30,81,48,207,62,99,118,221,96,75,151,158,146, 198,236,27,231,140,80,215,36,65,148,238,85,224,211,214,148,52,1,139,86,92,81,192,0,6,174,214,254,146,157,77,38,87,138,131,57,225,173,107,159,151,105,253,25,140,60,22,70,38,60,228,85,20,255,232,109,158,180,238,161,46,44,72,83,190,236,87,183,246,235,37,250,213,232,5,195,210,189,113,68,8,77,118,232,51,47,134,100,127,225,226,184,204,29,65,150,52,103,94,118,114,182,125,60,134,92,31,63,99,234,193,47,68,238,29,236,3,139,208,106,12,114,98,164,228,56,124,14,237,157,80,172,35,20,3,224,149,210,142,235,43,180,27,243,143,123,97,97,12,251,159,234,209,83,232,245,187,245,210,180,132,128,99,108,42,198,28,9,139,214,105,68,227,30,20,153,240,39,77,111,243,40,147,239,207,82,228,139,146,138,135,119,64,101,165,238,188,138,102,126,89,43,129,250,202,83,65,123,232,235,25,131,172,76,79,236,7,225,115,204,240,187,147,200,168,77,182,73,9,182,148,35,29,199,171,114,189,153,225,198,150,200,151,150,74,195,17,218,148,104,220,201,213,230,158,91,100,39,193,108,244,55,228,183,50,168,11,119,49,157,226,206,54,229,58,239,106,187,101,103,242,181,161,235,254,55,100,105,204,89,255,132,117,212,25,80,124,178,179,18,200,214,85,141,111,148,204,141,134,187,134,222,166,54,2,230,5,11,240,57,204,76,191,229,35,179,42,103,89,205,172,201,12,210,172,99,135,247,63,104,207,45,222,6,95,193,20,49,25,24,28,93,250,91,44,15,136,86,220,186,245,83,227,38,83,250,133,141,202,224,203,22,155,39,58,84,224,41,13,161,124,231,156,226,66,240,1,120,151,98,51,163,227,87,6,88,153,71,13,124,132,202,2,38,156,53,182,23,92,211,172,251,218,32,75,188,224,132,6,103,83,165,26,68,106,101,253,109,239,140,103,72,180,93,44,32,47,203,251,142,164,62,6,200,28,180,18,144,191,164,117,239,82,222,230,56,80,37,133,208,136,134,31,243,64,72,13,39,74,19,205,145,250,170,213,192,27,26,35,6,174,31,193,209,96,142,180,53,143,29,246,22,6,60,23,33,218,62,115,189,164,108,7,31,77,106,158,208,218,68,14,65,234,88,88,138,107,156,53,68,144,5,231,199,187,46,58,98,186,69,201,94,126,35,177,192,190,80,177,191,244,142,168,133,144,149,87,0,173,173,205,121,43,208,225,11,87,118,24,5,15,105,35,80,6,104,33,133,112,69,68,169,128,137,155,147,131,62,211,131,51,246,4,192,9,158,117,27,127,255,89,249,54,185,50,42,251,191,165,61,111,6,39,50,129,105,74,115,174,178,116,247,174,111,241,59,140,240,218,171,81,127,128,224,224,43,212,174,186,53,24,173,145,118,61,5,30,27,208,143,104,149,174,204,43,2,121,25,65,41,41,225,112,82,137,189,74,96,11,53,16,227,106,96,209,239,14,160,173,216,136,88,188,159,109,86,31,255,7,121,177,69,239,253,161,194,120,89,77,154,138,145,82,184,56,232,159,93,176,233,254,124,153,44,246,106,47,10,72,225,93,43,222,25,21,255,69,252,60,8,116,158,58,116,53,209,19,253,64,144,174,147,217,44,103,185,242,61,27,65,117,141,150,94,34,227,147,255,68,246,168,155,83,166,171,225,44,80,32,110,197,61,72,28,166,1,181,58,55,148,187,254,123,51,157,221,50,224,131,241,86,35,157,170,247,86,184,59,158,116,85,45,200,199,146,161,3,150,125,240,102,159,160,160,83,30,63,131,114,86,221,194,178,238,33,208,106,197,204]

    ```

    Vous avez donc à votre disposition une portion de clé notée `cle`, et l'intégralité du message chiffré noté `msg`, qui vous est donné en entrée du problème.  
    Avec ces éléments, saurez-vous déterminer le lieu du rendez-vous fixé par votre ami ? 

    <!-- 

    !!! corr
        [lien](https://gist.github.com/glassus/7aef2c4cbed5097e1857ecc851b7b740)

    -->