# 1NSI : TD Convertisseurs Électroniques : Binaires - Décimal

## Introduction

Ce TD utilise le logiciel Gratuit et Open Source **logisim**.
Ce TD propose l'émulation de certains circuits électroniques :

* Convertisseur Binaire -> Décimal
* Convertisseur Décimal -> Binaire

## Convertisseur 

## Référence

* https://www.positron-libre.com/cours/electronique/logique-combinatoire/codage-information/code-decimal-bcd.php#code_bcd