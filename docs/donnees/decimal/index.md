# Représentation Décimale d'un Entier

--8<-- "docs/bo/bases.md"

## (Rappel d') Écriture des Nombres Entiers en base $10$ (Décimal)

!!! pte
    En base $10$, il y exactement 10 <rbl>chiffres</rbl>: $0$, $1$, $2$, $3$, $4$, $5$, $6$, $7$, $8$, $9$

Considérons un nombre entier, écrit en base $10$ (en décimal), par exemple $2743$ :  

Il est usuel de transformer l'écriture de ce nombre de la manière suivante : 

$$\begin{align}
2743 &= 2000 + 700 + 40 + 3 \\
     &= 2\times1000 + 7\times100 + 4\times10 + 3\times1 \\
     &= 2\times10^3 + 7\times10^2 + 4\times10^1 + 3\times10^0
\end{align}
$$

!!! pte
    En base $10$, Tout nombre entier peut être écrit comme une somme coefficientée de puissances de $10$.

!!! mth
    Pour déterminer cette somme coefficientée :  

    * Au dessus de chaque chiffre, **de droite à gauche**, on écrit des puissances croissantes de $10$ (en commençant par $10^0$):

    $$10^3\,\,10^2\,\,10^1\,\,10^0$$
    
    $$ 2 \,\,\,\,\,\,\,\, 7 \,\,\,\,\,\,\,\, 4 \,\,\,\,\,\,\,3$$

    * On multiplie chaque chiffre (= le coefficient) par la puissance de $10$ correspondante :

    $$2743 = 2\times10^3 + 7\times10^2 + 4\times10^1 + 3\times10^0$$