# Écriture d'un Nombre Réel en Base b

--8<-- "docs/bo/bases.md"

## Chiffres en Base $b$

On généralise le Système de Numération Décimale positionnelle, en un <rbl>Système de Numération positionnel de base $b$</rbl>.

Il faut déjà commencer par choisir des notations pour un ensemble de $b$ chifres :

* Si $b\le 10$, alors (usuellement) les chiffres sont un **sous-ensemble** de $\{0,1,2,3,4,5,6,7,8,9\}$
* Si $b\gt 10$, alors (usuellement) les chiffres sont un **sur-ensemble** de $\{0,1,2,3,4,5,6,7,8,9\}$, càd que ces simples chiffres ne suffisent plus, il en faut de nouveaux :
    * Si $10\lt b \le 36$, alors (usuellement) on choisit les chiffres dans l'ensemble $\{0,1,2,3,4,5,6,7,8,9,A,B,C,..,X,Y,Z\}$
    * Si $b>36$, alors (usuellement) : 
        * Chaque chiffre peut être écrit en décimal
        * un nombre pouvant être noté $[20;37;15]_b = 20\times b^2 +37\times b^1+15\times b^0$

!!! not "Nombre $x$ en base $b$"
    $x_b = (x)_b = \overline{x}_b$ désignent le nombre $x$ écrit en base $b$

## Écriture de Nombres en Base $b$

!!! thm "Écriture des nombres en Base $b$"
    Pour tout entier $b\gt 2$,
    
    * Tout nombre $x$ peut être écrit dans le (Système de Numération Positionnel) en base $b$
    * Cette écriture est de plus **unique** à écriture périodique illimitée du dernier chiffre de la base près
    * Écriture d'un nombre entier en base $b$
    $\begin{align}
    x &= \overline{c_pc_{p-1}..c_1c_0}_b \\
      &= (c_pc_{p-1}..c_1c_0)_b \\
      &= [c_p;c_{p-1};..;c_1;c_0]_b \\
      &= c_p\times b^{p} + c_{p-1}\times b^{p-1} + \cdots + c_1\times b^{1} + c_0\times b^{0}
    \end{align}
    $
    * Cette écriture s'étend à des nombres fractionnaires, avec des exposants négatifs :
    $\begin{align}
    x &= \overline{c_pc_{p-1}..c_1c_0,f_1f_2..f_{q}}_b \\
      &= (c_pc_{p-1}..c_1c_0,f_1f_2..f_{q})_b \\
      &= [c_p;c_{p-1};..;c_1;c_0,f_1;f_2;..;f_q]_b \\
      &= c_p\times b^{p} + c_{p-1}\times b^{p-1} + \cdots + c_1\times b^{1} + c_0\times b^{0} \\
      &+ f_1\times b^{-1} + f_2\times b^{-2} + \cdots  + f_q\times b^{-q}
    \end{align}
    $

!!! not "zéro en base $b$"
    Le chiffre <rbl>zéro</rbl> s'écrit $0$ dans toutes les bases

## Conversion de bases

!!! mth "Conversion de base $a$ $\Leftrightarrow$ base $b$"
    Pour convertir un nombre $x_a$ d'une base $a$ vers une base $b$ (et/ou réciproquement), il suffit (par exemple) de passer par le décimal (base $10$):

    <center><enc>Base $a \Leftrightarrow$ Base $10 \Leftrightarrow$ Base $b$</enc></center>

!!! exp "Conversion Base $8$ $\rightarrow$ Base $2$"
    Soit $x = 1261_8$ un nombre en base $8$.  
    Comment écrire $x$ en base $2$ ?

    ??? corr
        * Conversion de $x$ en Base $10$

            $\begin{align}
            x &= (1\times 8^3+2\times 8^2+6\times 8^1+1\times 8^0)_{10} \\
            &= (512 + 128 + 48 + 1)_{10} \\
            &= 689_{10}
            \end{align}$

        * Conversion Base $10$ $\rightarrow$ Base $2$:  
        On réalise une succession de divisions euclidiennes par $2$, on conservant à chaque fois les restes :

            $689$  
            $344$ $\rightarrow$ reste $1$  
            $172$ $\rightarrow$ reste $0$  
            $86$  $\rightarrow$ reste $0$  
            $43$  $\rightarrow$ reste $0$  
            $21$  $\rightarrow$ reste $1$  
            $10$  $\rightarrow$ reste $1$  
            $5$   $\rightarrow$ reste $0$  
            $2$   $\rightarrow$ reste $1$  
            $1$   $\rightarrow$ reste $0$  
            $0$   $\rightarrow$ reste $1$  
            Conclusion : $689_{10} = 1010110001_2$

        * Conclusion : <enc>$1261_8 = 1010110001_2$</enc>

!!! mth "Conversion Directe Base $a$ $\Rightarrow$ Base $b$"
    On peut aussi convertir directement un nombre $x$ en base $a$ ($x_a$), vers une base $b$, en :
    
    * réalisant une succession de divisions de $x_a$ par $b$, jusqu'à ce que le quotient soit égal à $0$
    * en conservant les restes $r$ pour chaque division ($0\le r \lt b$)
    * Le nombre $x_b$ ($x$ en base $b$) est alors obtenu en lisant les restes à l'envers (du dernier vers le premier)