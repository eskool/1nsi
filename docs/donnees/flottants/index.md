# 1NSI : Nombres Réels et Décimaux en Base 10

--8<-- "docs/bo/flottants.md"

Cette partie présente quelques rappels utiles pour mieux appréhender cette notion.

## Nombres Réels et Nombres Décimaux en base 10

!!! thm "Développement Décimal"
    Tout nombre réel $x$ admet un <rbl>développement décimal</rbl>, càd peut s'écrire comme une somme (éventuellement infinie) coefficientée de puissances $10^p$ ($p\ge 0$ ou $p\le 0$).  
    Ce développement Décimal n'est pas forcément unique : si l'on accepte par exemple que $x = 0,99\underline{9} = 1 = 1,00\underline{0}$ sont deux développements décimaux distincts du même nombre :

    * On dit que $1,00\underline{0}$ est le <rbl>développement décimal propre</rbl> de $x$  
    (Ce cas sera toujours choisi par défaut dans toute la suite)
    * On dit que $0,99\underline{9}$ est le <rbl>développement décimal impropre</rbl> de $x$

!!! def "Nombres Décimaux, Rationnels, Irrationnels"
    Un nombre réel $x$ peut être :

    * Un <rbl>nombre Entier</rbl>, auquel cas il **peut s'écrire sans aucun chiffre après la virgule**
    * Un <rbl>nombre Décimal</rbl>, càd qu'il admet un développement décimal limité, càd avec un **nombre fini de chiffres après la virgule**
    * Un <rbl>nombre Rationnel</rbl>, càd qu'il peut s'écrire comme un quotient de deux nombres entiers, càd qu'il admet un développement décimal :
        * soit limité (par exemple $\dfrac 12=0,5$)
        * soit **illimité (nécessairement) périodique** (par exemple $\dfrac13 = 0,\underline{3} = 0,3333\cdots$) càd avec un nombre infini de chiffres après la virgule (ici la période est $3$)
    * Un <rbl>nombre Irrationnel</rbl>, càd qu'il admet un développement décimal **illimité (nécéssairement) non périodique**. Par exemple :  
    $\sqrt{2} \approx  1,414 213 562 373 095 048 801 688 724 209 698 078 569 671 875 376 948 073 176 679 737..$

!!! exp "Développement décimal (limité) d'un nombre décimal"
    $
    \begin{align}
    43,589 & = 40 + 3 + 0,5 + 0,08 + 0,009 \\
        & = 4\times10 + 3\times1+5\times0,1+8\times0,01+9\times0,001 \\
        & = 4\times10^1 + 3\times10^0+5\times10^{-1}+8\times10^{-2}+9\times10^{-3}
    \end{align}
    $

!!! pte "Approximation Décimale d'un nombre réel"
    Un nombre décimal à $p$ chiffres après la virgule peut donc être vu comme une approximation d'un réel $x$, avec une précision $p$ (chiffres après la virgule)