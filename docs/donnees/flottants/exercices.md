# 1NSI : Représentation des Nombres Flottants

## Exercice 1

Sur une feuille papier, en suivant la norme $IEEE-754$, remplir le Tableau Suivant:

<center>

| Type | Exposant<br/>$n$ | Mantisse<br/> $f$| Valeur Approchée | Écart<br/>par rapport au<br/>Précédent |
|:-:|:-:|:-:|:-:|:-:|
| $0$ |  |  |  |  |
| Plus Petit **nombre dénormalisé** |  |  |  |  |
| nombre dénormalisé suivant |  |  |  |  |
| nombre dénormalisé suivant |  |  |  |  |
| Autre nombre dénormalisé |  |  |  |  |
| Nombre Précédant le <br/>Plus Grand nombre dénormalisé |  |  |  |  |
| Plus Grand nombre dénormalisé |  |  |  |  |
| Plus Petit **nombre normalisé** |  |  |  |  |
| nombre normalisé suivant |  |  |  |  |
| presque le double |  |  |  |  |
| nombre normalisé suivant |  |  |  |  |
| nombre normalisé suivant |  |  |  |  |
| Nombre Précédant $1$ |  |  |  |  |
| $1$ |  |  |  |  |
| Nombre Suivant $1$ |  |  |  |  |
| Nombre Précédant le <br/>Plus Grand nombre normalisé |  |  |  |  |
| Plus Grand nombre normalisé |  |  |  |  |
| $\infty$ |  |  |  |  |
| Première valeur (dénormalisée)<br/>de NaN (avertisseur) |  |  |  |  |
| NaN normalisé (avertisseur) |  |  |  |  |
| Dernière valeur (dénormalisée)<br/>de NaN (avertisseur) |  |  |  |  |
| Première valeur (dénormalisée)<br/>de NaN (silencieux) |  |  |  |  |
| Dernière valeur (dénormalisée)<br/>de NaN (silencieux) |  |  |  |  |

</center>

## Exercice 2

Est-il possible de comparer des nombres flottants représentés en norme $IEEE-754$ ?
Si NON, Pourquoi? Et si OUI, comment le faire?
