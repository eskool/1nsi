# 1NSI : Écriture Scientifique en Base 10

--8<-- "docs/bo/flottants.md"

Cette partie présente quelques rappels utiles pour mieux appréhender cette notion.

## Écriture sous Forme Scientifique en Base 10

!!! pte "Écriture sous Forme Scientifique d'un nombre décimal $x$"
    Tout nombre décimal $x$ non nul peut s'écrire sous la forme :  
    <center><enc>$x=(-1)^s\times c,f\times10^n$</enc></center>
    où :

    * $s$ ($0$ ou $1$) qui représente le <rbl>signe</rbl> du nombre décimal $x$ :
        * $s=0$, correspond à $(-1)^s=+1 \gt 0$ donc à des nombres décimaux $x \gt 0$,
        * $s=1$, correspond à $(-1)^s=-1 \lt 0$ donc à des nombres décimaux $x \lt 0$
    * $m=c,f = c+f$ est appelée <rbl>mantisse</rbl> : c'est une convention de notation, où :
        * $c$ est un **chiffre** *mais pas zéro* donc $c\in\{1;2;3;4;5;6;7;8;9\}$
        * $f\in[0;1[$ est appelée <rbl>partie fractionnaire</rbl> de $m$, ou <rbl>partie décimale</rbl> de $m$, ou <rbl>partie significative</rbl> de $m$. En mathématiques, on la note $f=\{m\}$
    * $n$ est appelé l'<rbl>exposant</rbl> : c'est un entier relatif $\in\mathbb{Z}$ (positif ou négatif)

!!! mth "Comment en pratique déterminer $c$? $f$? $n$?"
    On distingue classiquement les cas (symétriques) où $x<1$ et où $x>1$.

    1. Soit $x=0,00458<1$ un nombre réel tel que $0<x<1$  
    On réalise plusieurs **multiplications par $10$ successives** jusqu'à la première fois où (le chiffre de) la partie entière ne soit plus égale à $0$. On obtient $n$ en tenant les comptes (*négativement*) du nombre de multiplications par $10$, càd que $n$ est **décrémenté** de $1$ à chaque nouvelle multiplication par $10$ (pour compenser l'opération $\times10$)  

        !!! exp "Représentation de $x=0.00458$"
            $$\begin{alignat}{2}
            0,00458 \times 10 &= 0,0458 & \,\,\, (n=-1) \\
            0,0458 \times 10  &= 0,458  & \,\,\, (n=-2) \\
            0,458 \times 10   &= 4,58   & \,\,\, (n=-3)
            \end{alignat}$$

            donc $c=4$, $\,\,f=0,58$ et $n=-3$  
            ceci prouve que $x = 0,00458 = 4,58\times10^{-3}$ on a bien écrit : $x = c,f\times10^n$ avec $c=4 \neq 0$, $f=0,58\in[0;1[$ et $n=-3$.

    2. Soit $x=368,91>1$. Dans ce cas, on réalise des **divisions par $10$ successives**, jusqu'à la première fois où il ne reste plus qu'un seul chiffre non nul devant la virgule. On obtient $n$ en tenant les comptes (*positivement* cette fois) du nombre de divisions par $10$, càd que $n$ est **incrémenté** de $1$ à chaque nouvelle division par $10$ (pour compenser l'opération $\div10$).  

        !!! exp "Réprésentation de $x=368,1$"
            $$\begin{alignat}{2}
            768,91/10 &= 76,891 & \,\,\,(n=1) \\
            76,891/10 &= 7,6891 & \,\,\,(n=2)
            \end{alignat}
            $$

            donc $c=7$, $\,\,f=0,6891$ et $n=2$  
            ceci prouve que $x = 768,91 = 7,6891\times10^2$ on a bien écrit : $x = c,f\times10^n$ avec $c=7 \neq 0$, $f=0,6891\in[0;1[$ et $n=2$