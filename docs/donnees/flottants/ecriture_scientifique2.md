# 1NSI : Écriture Scientifique en Base 2

--8<-- "docs/bo/flottants.md"

Cette partie présente quelques rappels utiles pour mieux appréhender cette notion.

## Écriture sous Forme Scientifique en base 2

L'encodage des nombres ***flottants*** est inspiré (sans être exactement identique) de l'**écriture scientifique des nombres en base $2$** encore appelée **Notation Binaire Scientifique**, elle-même inspirée de l'écriture scientifique en base $10$ :

!!! pte "Notation Binaire Scientifique"
    Tout nombre décimal $x$ non nul peut s'écrire de manière unique sous la forme suivante :  

    <center><enc>$x=(-1)^s\times 1,f \times 2^{n}$</enc></center>

    où :

    * $s$ représente le <rbl>bit de signe</rbl> de $x$ :
        * $s=0$ correspond à $(-1)^s=+1 \gt 0$ donc à des nombres décimaux $x \gt 0$, 
        * $s=1$ correspond à $(-1)^s=-1 \lt 0$ donc à des nombres décimaux $x \lt 0$,
    * le nombre $m = 1,f = 1 + f$ est appelé la <rbl>mantisse</rbl> dans laquelle :
        * en binaire, on a forcément $c = 1$
        * $f$ est la <rbl>partie fractionnaire</rbl> (binaire) de $m$, ou <rbl>partie significative</rbl> (binaire) de $m$  
        * $m$ est un **nombre binaire à virgule** (dont l'écriture en base $10$ appartient à l'intervalle $[1;2[$).  
        (ATTENTION : Quelquefois, certains auteurs appelent *mantisse* le nombre $f$, plutôt que $m$, voire quelquefois utilisent le même mot *mantisse* pour $m$ et pour $f$)
    * $n\in\mathbb{Z}$ (positif ou négatif) est l'<rbl>exposant</rbl>
