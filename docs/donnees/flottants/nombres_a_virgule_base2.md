# 1NSI : Nombres Binaires à Virgule (en Base 2)

--8<-- "docs/bo/flottants.md"

Cette partie présente quelques rappels utiles pour mieux appréhender cette notion.

## Nombres Binaires à Virgule (en base 2)

A l'instar des notations des nombres décimaux (en base $10$) qui sont des sommes de puissances de $10$ (positives ou négatives), on peut utiliser la notation de **nombres binaires à virgule (en base $2$) qui sont des sommes de puissances de $2$ (positives ou négatives)**.  

!!! exp "Conversion Nombre Binaire à Virgule $\rightarrow$ Nombre Décimal (à Virgule)"
    On se donne un nombre binaire à virgule, par exemple, $x=101,1101_2$, et on veut savoir comment l'écrire comme nombre décimal (à virgule, en base $10$) ?  
    $x=101,1101_2$ est le nombre binaire à virgule (en base 2) dont l'écriture décimale (en base 10) est:  
    $x=(1\times2^2+0\times2^1+1\times2^0+1\times2^{-1}+1\times2^{-2}+0\times2^{-3}+1\times2^{-4})_{10}$  
    $x=1\times2^2+1\times2^0+1\times2^{-1}+1\times2^{-2}+1\times2^{-4}$ donc   
    $x=4+1+0,5+0,25+0,0625$  
    $x=5,8125_{10}$

!!! exp "Conversion Nombre Décimal (à Virgule) $\rightarrow$ Nombre Binaire à Virgule"
    On se donne un nombre réel $x$ (en génral un nombre Décimal à virgule), en base $10$, par exemple, $x=57,85_{10}$, et on veut savoir comment l'écrire comme nombre Binaire à Virgule (en base $2$) ?

    * On sépare $x=57,85$ en la somme de sa partie entière $\lfloor x \rfloor = 57$ et de sa partie fractionnaire $f=\{x\} = 0,85$
    * On convertit la partie entière $\lfloor x \rfloor = 57$ en binaire avec les techniques usuelles déjà vues [^1], càd en divisant successivement par $2$ jusqu'à parvenir à un quotient $q=0$ : On conserve les restes des divisions par $2$ successives.  
    Conclusion (Partie Entière) : <center><enc>$57_{10} = 111001_2$</enc></center>  
    En effet :

        ```console
        57 | 2  -> reste 1        ∧
        28 | 2  -> reste 0        |
        14 | 2  -> reste 0        |
        7  | 2  -> reste 1        |  (sens de lecture)
        3  | 2  -> reste 1        |
        1  | 2  -> reste 1        |
        0  | 2  -> (q=0, donc     | 
            ne pas conserver)     |
        ```

    * On convertit la partie fractionnaire $f=\{x\} = 0,85$ en binaire (à virgule) en multipliant successivement par $2$. On conserve à chaque étape :
        * le $0$ ou le $1$ de la partie entière du (nouveau) résultat (partiel) après multiplication par $2$
        * On multiplie la nouvelle partie fractionnaire par $2$
        * Pour la Terminaison de ce procédé, $3$ situations distinctes peuvent se produire :  

            * Soit la partie fractionnaire $f$ finit par devenir nulle ($f=0,0$) : C'est le cas d'une écriture binaire avec un nombre limité de chiffres après la virgule ("ça tombe juste" - en binaire-, par exemple : $0,110101$)

                ```console
                Exemple : Convertir 0.8281125 en Binaire à Virgule
                Conserver '0,' puis :
                0.828125*2 = 1.65625 -> conserver 1
                0.65625*2 = 1.3125   -> conserver 1
                0.3125*2 = 0.625     -> conserver 0
                0.625*2 = 1.25       -> conserver 1
                0.25*2 = 0.5         -> conserver 0
                0.5*2 = 1.0          -> conserver 1
                Partie Fractionaire nulle, donc le procédé s'arrête et 
                le Nombre Binaire admet un nombre fini de chiffres après la virgule
                ------------
                ```
                
                Conclusion : $0,828125_{10} = 0,110101_2$

            * Soit la partie fractionnaire se répète au bout d'un certain temps, on obtient alors une période de la partie fractionnaire binaire (nombres rationnels binaires) : c'est le cas des nombres binaires avec un développement binaire périodique (par ex : $101, 11\underline{1011}$)

                ```console
                Exemple : Convertir 0.85 en Binaire à Virgule
                Conserver '0,' puis :
                0.85*2 = 1.7 -> conserver 1
                0.7*2 = 1.4  -> conserver 1
                0.4*2 = 0.8  -> conserver 0
                0.8*2 = 1.6  -> conserver 1
                0.6*2 = 1.2  -> conserver 1
                0.2*2 = 0.4  -> conserver 0
                Répétition de la mantisse 0.4 à l'indice 2
                donc le Nombre Binaire est Périodique, de période 0110
                ------------
                ```
                
                Conclusion (Partie Fractionnaire): <center><enc>$0,85_{10} = 0,11\underline{0110}_2$</enc></center>

            * Soit la partie fractionnaire ne devient jamais nulle, et ne se répète jamais (nombres binaires irrationnels, c'est possible..) : c'est le cas des nombres binaires avec un développement binaire non périodique. Dans ce cas, on choisira de s'arrêter lorsque la précision à atteindre nous semblera suffisante. Ce cas est plus compliqué, et requiert en fait de partir d'un nombre réel $x$ lui-même avec un développement décimal illimité (car $10=2\times 5$ est un multiple de $2$). Il ne sera pas utilisé en pratique.
    * Conclusion (Générale) : <center><enc>$57,85_{10} = 111001,11\underline{0110}_2$</enc></center>