from math import floor, ceil
s=input("x=")
# Entrer un nombre fractionnaire s sous la forme 0,XXXX ou 0.XXXX
sInit = s

if "," in s:
    idComma = s.index(",")
    entX = int(s.split(",")[0])
    fracX = float("0."+s.split(",")[1])
elif "." in s:
    entX = int(s.split(".")[0])
    fracX = float("0."+s.split(".")[1])
else:
    print("Erreur : x DOIT être une partie Fractionnaire avec une virgule de type 0,XXX (ou 0.XXX)")
    import sys
    sys.exit()

ent = []
frac = []
print("Conserver '0,' puis :")

while fracX !=0 and fracX not in frac:
    ent.append(entX)
    frac.append(fracX)
    fracX = fracX*2
    s = str(fracX)
    i = s.index(".")
    fracX = float("0."+str(s.split(".")[1]))
    entX = int(s.split(".")[0])
    print(f"{frac[-1]}*2 = {entX}.{str(fracX).split('.')[1]} -> conserver {entX}")

ent.append(entX)
frac.append(fracX)

if fracX ==0:
    print("Partie Fractionaire nulle, donc le Nombre Binaire admet un nombre fini de chiffres après la virgule")
    sFrac = ""
    for i in range(len(ent)):
        if i == 0:
            sFrac = str(ent[0])+","
        else:
            sFrac += str(ent[i])
    print(f"{sInit} (Base 10) = {sFrac} (Base 2)")
elif fracX in frac:
    i = frac.index(fracX)
    print(f"Nombre Binaire Périodique : Répétition de la mantisse {frac[i]} à l'indice {i}")
    ent0 = ent[:i+1] # enlever le premier zéro car appartient à la partie entière
    periode = ent[i+1:]
    # print("i=",i)
    # print("ent=",ent)
    # print("frac=",frac)
    # print("Période=", ent[i+1:])
    sFrac = ""
    for i in range(len(ent0)):
        if i == 0:
            sFrac = str(ent0[0])+","
        else:
            sFrac += str(ent0[i])
    sPeriode = ""
    for el in periode:
        sPeriode += str(el)
    print("------------")
    print(f"{sInit} (Base 10) = {sFrac}\033[4m"+f"{sPeriode}"+"\033[0m (Base 2)")
