# 1NSI : Réseaux - Décomposition en Couches

Il s'agit ici de modéliser la manière de communiquer entre deux hôtes interconnectés (directement, ou via un réseau, voire un réseau de réseaux)

##  Principe de Décomposition Par Couches

### Traitement dans l'Hôte Source

#### Les Données Applicatives

Dans l'**Hôte Source**, le Système d'Exploitation / OS - Operating System, reçoit des données depuis les Applications (<bred>données applicatives</bred>) qui sont en cours d'exécution (processus).  

#### Traitement par Couches

Pour chaque application distincte, l'OS fait subir plusieurs traitements successifs aux données applicatives, de sorte que ces données soient ultimement bien vérifiées et préparées pour être envoyées via un canal de communication. Ces différents traitements sont regroupés et modélisés en ce que l'on appelle des <bred>couches</bred> / <bred>niveaux</bred> d'abstraction:

![Modèle en Couches](../img/modele-en-couches.svg){.center style="width:90%;max-width:800px;"}

* Les couches sont indépendantes les unes des autres, mais 
* Les couches travaillent ensemble (deux couches adjacentes se rendent des services l'une l'autre) :
    * Chaque couche $i$ (inférieure) d'un hôte rend un ou des <bred>services</bred> à la couche $i+1$ directement supérieure du même hôte
    * Chaque couche $i$ (supérieure) d'un hôte utilise les <bred>services</bred> de la couche $i-1$ directement inférieure du même hôte. 
* Chaque couche $i$ inférieure est une abstraction pour masquer la complexité à la couche $i+1$ supérieure ($1$ couche $=1$ boîte noire)
* Chaque couche du modèle résout une partie des problèmes de communication
* Chaque couche $i$ d'un hôte communique avec la même couche $i$ correspondante sur l'hôte interconnecté, en utilisant un ou plusieurs <bred>protocoles</bred> **spécifiques à cette couche $i$**. Un protocole de communication est donc associé à une couche spécifique (uniquement)

Il s'agit donc d'une manière de penser, d'organiser, de modéliser la communication entre systèmes informatiques interconnectés, en plusieurs niveaux d'abstraction.

### Envoi par un Canal de Communication

#### Descente, Envoi, puis Remontée

Pour chaque communication depuis une source vers une destination: les données descendent toutes les couches de l'hôte source une par une (des supérieures vers les inférieures), puis les remontent dans l'hôte destination dans le sens contraire (des inférieures vers les supérieures). Ce processus (de descente puis remontée des couches) reste valable lorsqu'une couche de l'hôte source discute avec la couche équivalente de l'hôte destination.

![OSI Deux Ordis](../img/couches-deux-ordis.svg){.center style="width:90%;max-width:800px;"}

#### Segmentation / Fragmentation

Durant leur traversée des couches, l'OS - Système d'Exploitation découpe les données applicatives en plusieurs petits <bred>paquets</bred> de données (Segmentation / Fragmentation), il les numérote (de sorte à pouvoir les reconstruire plus tard), et les transmet à la carte réseau.

#### Multiplexage (MUX) / Démultiplexage (DEMUX)

La carte réseau reçoit des données applicatives de plusieurs applications, les envoie enfin sur le réseau (un simple câble, un réseau ou un inter-réseau), dans un ordre quelconque en offrant un peu de débit à chaque application, via un même canal de communication (multiplexage - MUX).

![multiplexage](./img/multiplexage.svg){.center style="width:90%;max-width:800px;"}
    
<center>
<figcaption>
Multiplexage (MUX) / Démultiplexage (DEMUX)<br/>
Une couleur distincte = une application distincte
</figcaption>
</center>

### Réception par l'Hôte Destinataire

* La carte réseau de l'**Hôte Destinataire** reçoit les paquets transmis, et les envoie au Système d'Exploitation (OS)
* l'OS de l'Hôte de Destination fait traverser chaque paquet reçu par toutes les couches, **dans l'ordre contraire** qu'en émission (de la couche la plus basse, càd proche de la machine, jusqu'aux couches les plus hautes, càd proches de l'utilisateur)
* l'OS détermine quelles parties du message (paquets) sont pour quelles applications (démultiplexage - DEMUX)
* l'OS reconstitue le message par application, vérifie qu'ils disposent des bonnes autorisations, qu'il n'y a pas d'erreurs de transmission, etc..
* l'OS transmet enfin le message à l'application de destination.

!!! exp "Une image de la Communication entre Couches"
    Soient 2 explorateurs français, l’un en Espagne, l’autre au Pérou.

    * Ils ne peuvent parler directement car ils ne savent pas utiliser le télégraphe.
    $\rightarrow$ canal de communication
    * Ils ont besoin d’un technicien pour envoyer et recevoir les infos.   
    $\rightarrow$ couche de niveau $1$
    * Ils ne parlent pas espagnol, ils ont besoin d’un interprète.  
    $\rightarrow$ couche de niveau $2$

### Avantages du Modèle par Couches

* Si une couche est défaillante, les autres couches ne cessent pas de fonctionner, et peut donc s'appuyer sur les couches inférieures
* Le débogage est simplfié dans un modèle en couches

## Deux Modèles: OSI vs TCP/IP

Il existe principalement deux modèles en couches:

* Le <bred>modèle OSI - Open Systems Interconnection</bred>, est une **norme ISO - International Standards Organisation** ou Organisation Internationale de Normalisation. C'est un **modèle Académique et Abstrait** (sans implémentation pratique), mais qui admet néanmoins une utilité réelle. **Le modèle OSI contient $7$ couches**.
* Le <bred>modèle TCP/IP</bred> est une **implémentation pratique**, et un standard qui s'est imposé de fait. c'est ce modèle qui est utilisé en pratique. **Le modèle TCP/IP contient $4$ couches**.

### Communication par couches, via connexion directe

<center>

<div>

<div style="width:48%; float:left;">

<figure>
<img src="../img/osi-deux-ordis.svg">
<figcaption>

Décomposition en Couches,<br/>
<bred>Modèle OSI</bred>,<br/>
Connection directe entre deux PC

</figcaption>
</figure>

</div>

<div style="width:48%; float:right;">

<figure>
<img src="../img/tcpip-deux-ordis.svg" >
<figcaption>

Décomposition en Couches,<br/>
<bred>Modèle TCP/IP</bred>,<br/>
Connection directe entre deux PC

</figcaption>
</figure>

</div>

</div>

</center>

<clear></clear>


### Communication par Couches, via un inter-Réseau

![OSI passage par Switch Routeur](../img/osi-switch-routeur.svg){.center style="width:100%;"}

<center>

<figcaption>

Communication par Couche via un Inter-Réseau

</figcaption>

</center>

### Rôle de chaque couche 

<center>

![OSI vs TCP/IP](./img/OSI-vs-TCP-IP.svg)

<figcaption>

Rôle des couches Modèle OSI vs Modèle TCP/IP

</figcaption>

</center>

<env>Le cas de TCP/IP</env>

* La couche <bred>Application</bred> du modèle TCP/IP recouvre en fait les $3$ couches supérieures du modèle OSI (Les couches Application, Présentation, Session). Ce n'est pas qu'elles n'existent pas dans TCP/IP. Le modèle OSI est juste plus détaillé que le modèle TCP/IP quant à ses couches.
* La couche <bred>Transport</bred> de TCP/IP est quelquefois appelée la <bred>couche TCP - UDP</bred> pour **TCP = Transmission Control Protocol** et/ou **UDP= User Datagram Protocol**
* La couche <bred>Réseau</bred> de TCP/IP est souvent appelée la <bred>couche IP - Internet Protocol</bred>
* La Couche <bred>Accès Réseau</bred> de TCP/IP est souvent divisée en deux: 
    * Couche Liaison de Données
    * Couche Physique 

## notes et Références

[^1]: [Outils de Couche Réseau](https://www.it-connect.fr/chapitres/les-outils-de-couche-reseau/)

