# 1NSI: Réseaux - Le Modèle TCP/IP

## Introduction

<center>

<iframe width="560" height="315" src="https://www.youtube.com/embed/s18KtOLpCg4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

</center>

## :fa-university: Histoire : Le Modèle TCP/IP et ARPANet

Le **modèle TCP/IP**, quelquefois appelé **suite TCP/IP** ou **DoD Model - Department of Defense Model**, est une suite de plusieurs protocoles dont la première publication remonte à Septembre $1973$ lors d'une conférence de l'**INWG - Internet Network Working Group**, composée notamment de :

* un protocole de Transport, de niveau $4$ du modèle OSI : <red>TCP - Transmission Control Protocol</red>, en charge du transport des données  par **paquets**, documenté par la RFC 793[^10]
* un protocole Internet, de niveau $3$ du modèle OSI : <red>IP - Internet Protocol</red>, pour identifier et nommer uniformément les Hôtes et les Réseaux interconnectés
* mais pas exclusivement, en effet de nombreux autres protocoles liés aux réseaux sont classiquement associés à cette suite, notamment:
    * ARP
    * ICMP
    * etc..

!!! col __30 center right
    ![Robert Kahn, co-créateur TCP/IP](./img/robert-khan.jpeg)  
    Robert Kahn  
    ![Vint Cerf, co-créateur TCP/IP, en 1973](./img/vint-cerf.jpg)  
    Vint Cerf en 1973
    ![TCP Survivor](./img/tcp-survivor.png)  
    I survived the TCP transition
    ![IMP - Interface Message Processor](./img/imp.jpg)
    Interface Message Processor

Dès $1969$, la **DARPA - Defense Advanced Research Projects Agency** :us: met en service son réseau militaire américain **ARPANet**, l'ancêtre d'internet. Dès $1969$ également, des tests opérationnels[^5] sur la commutation de paquets sont réalisés par la DARPA durant la grande expérimentation ARPANet. D'autres protocoles de commutation par paquets avaient déjà été développés par la DARPA (par radio, satellite et filaire), notamment le protocole **NCP - Network Control Protocol** qui utilisait l'équipement informatique au coeur d'ARPANet: les sous-réseaux à base des équipements réseaux **IMP - Interface Message Processor**. Néanmoins tous les protocoles antérieurs à TCP/IP étaient incompatibles entre eux à cette époque. Le but de TCP/IP était donc de permettre au réseau **ARPANet**, de fédérer tous ses réseaux (radio, satellite et filaire) dans un seul tuyau. De plus, outre la possibilité de connecter des réseaux hétérogènes, le réseau ARPANet devait résister à une éventuelle guerre nucléaire, contrairement au réseau téléphonique habituellement utilisé pour les télécommunications mais considéré trop vulnérable. Il a alors été convenu qu’ARPANet utiliserait la technologie de **commutation par paquet** (mode **datagramme**), une technologie émergente prometteuse.

Bien qu'ayant été un ingénieur majeur de l'ARPANet, qui utilisait alors le protocole NCP, **Bob Kahn** ne put se résoudre à l'utiliser car celui-ci devait fonctionner avec l'équipement réseau IMP et en plus n'effectuait **pas de contrôle des erreurs**. Il créa donc avec **Vint(on) Cerf** un protocole dit TCP/IP permettant de relier les réseaux entre eux.

La première publication de TCP/IP remonte à septembre $1973$ lors d'une conférence de l'INWG. Les **protocoles TCP** et **IP** furent inventés en $1974$. La DARPA signa alors plusieurs contrats avec les constructeurs (**[BBN - Bolt Beranek & Newman](https://fr.wikipedia.org/wiki/Bolt,_Beranek_and_Newman)** principalement) et l’**Université de Berkeley** qui développait un Unix (BSD) qui aida grandement à imposer ce standard, ce qui fut fait.

Le réseau **ARPANet** adopte, le $1$er janvier $1983$, la suite de protocoles TCP/IP qui sera la base d'Internet. Il doit pour cela réaliser une migration titanesque, restée célèbre, de ses $400$ serveurs : les équipes techniques (ayant survécu..) arborent fièrement leur PIN'S "**I survived the TCP Transition**".

Les années qui suivent sont marquées par l'élaboration du modèle OSI, où le français **Hubert Zimmermann** joue un rôle important, mais où les discussions sont freinées par le sentiment des opérateurs télécoms des différents pays que cette évolution peut nuire, à terme, à leurs monopoles respectifs.

## Le Modèle TCP/IP

Le modèle TCP/IP est (né d') une implémentation pratique, ayant été conçu AVANT le modèle OSI, c'est en partie la raison pour laquelle il s’est progressivement imposé comme modèle de référence en lieu et place du modèle OSI, qui est quant à lui, plutôt un modèle théorique et académique.

Il possède la particularité d'être <bred>routable</bred>, comprendre *"redirectionnable"*, via des machines appelées <bred>routeurs</bred> :fr: ou <bred>routers</bred> :gb:, et aussi grâce à des identifiants réseau appelés des <bred>adresses IP</bred> qui servent à identifier de manière unique les machines et les réseaux. Disposer de telles fonctionnalités requiert un <bred>plan d'adressage</bred> spécifique.

### Un modèle en 4 Couches

Comparé au modèle OSI qui contient $7$ couches, le modèle TCP/IP est fondé sur *seulement* $4$ couches :

![OSI vs TCP/IP](./img/modele-TCP-IP.svg){.center style="width:90%; max-width:800px;"}

### Communication entre deux Hôtes

Les données traversent toutes les couches, comme dans le modèle OSI:

* Dans l'Hôte Source : Des couches Hautes vers les couches Basses
* Dans l'Hôte de Destination : Des couches Basses vers les couches Hautes

![TCP/IP deux Ordis](./img/tcpip-deux-ordis.svg){.center}

### Encapsulation / Décapsulation

Le modèle utilise aussi le principe de l'**encapsulation/décapsulation** des PDU entre chaque couche :

* Pour transmettre du contenu d’un ordinateur à un autre, l’utilisateur va utiliser un programme qui construit un message enveloppé par un **en-tête applicatif**, HTTP par exemple. Le message subit une première encapsulation.
* Le logiciel va utiliser un protocole de couche transport correspondant pour établir la communication avec l’hôte distant en ajoutant un **en-tête TCP** ou un **en-tête UDP**.
* Ensuite, l’ordinateur va ajouter un en-tête de couche Internet, un **en-tête IPv4** ou **IPv6**, qui servira à la livraison des informations auprès de l’hôte destinataire. L’en-tête IP contient les adresses d’origine et de destination des hôtes.
* Enfin, ces informations seront encapsulées au niveau de la couche Accès Réseau qui s’occupera de livrer physiquement le message: un **en-tête Ethernet** par exemple. L'en-tête Ethernet contient les adresses MAC de l'hôte source et du destinataire.


![Encapsulation TCP/IP](./img/encapsulation-tcp-ip.svg)

```console linenums="0"
                                                                   +---------------------+
                                                                   | Données Utilisateur |
                                                                   +---------------------+

                                              +--------------------+---------------------+      +-----------------+
                                              | En-Tête Applicatif | Données Utilisateur |      |   Application   |
                                              +--------------------+---------------------+      +-----------------+
                                              <------------- PDU Application ------------>

                                +-------------+--------------------+---------------------+      +-----------------+
                                | En-Tête TCP | En-Tête Applicatif | Données Utilisateur |      |       TCP       |
                                +-------------+--------------------+---------------------+      +-----------------+
                                <------------------ Segment TCP ------------------------->

                   +------------+-------------+--------------------+---------------------+      +-----------------+
                   | En-Tête IP | En-Tête TCP | En-Tête Applicatif | Données Utilisateur |      |       IP        |
                   +------------+-------------+--------------------+---------------------+      +-----------------+
                   <------------------------------ Paquet IP ---------------------------->

+------------------+------------+-------------+--------------------+---------------------+      +-----------------+
| En-Tête Ethernet | En-Tête IP | En-Tête TCP | En-Tête Applicatif | Données Utilisateur |      |     Ethernet    |
+------------------+------------+-------------+--------------------+---------------------+      +-----------------+
<--------------------------------------- Trame Ethernet --------------------------------->
                   <------------------ 46 <= taille <= 1500 octets ---------------------->
```

### Services & Protocoles TCP/IP

Comme pour le modèle OSI, chaque couche dispose de services & protocoles spécifiques.

![TCP/IP](../img/tcp_ip_bis.gif){.center style="width:100%;"}

### Avantages du Modèle TCP/IP

* C'est un Standard de fait
* Il est plus ciblé que le modèle OSI
* Plus pratique
* Il s'est imposé par sa simplicité

## Décomposition des Couches TCP/IP

### Couche Application

Elle est la couche de communication qui s’**interface avec les utilisateurs**.
Exemples de **protocoles** et **services applicatifs** : HTTP, DNS, DHCP, FTP, …
Elle s’exécute sur les machines hôtes.

### Couche Transport : TCP - UDP

Elle est responsable du dialogue entre les hôtes terminaux d’une communication.
Les applications utiliseront :

* **TCP pour un transport fiable**, et 
* **UDP pour un service peu fiable**

Les routeurs NAT et les pare-feux opèrent un filtrage au niveau de la couche transport.

Le rôle du service de transport est de transporter les messages de bout en bout, càd de la source jusqu'à la destination, donc d'un bout à l'autre du réseau, sans se préoccuper du chemin à suivre car ce problème a déjà été traité par la couche directement inférieure réseau.

Il y a plusieurs exemples de protocoles de transport. Le choix dépend du type d'application et des services demandés.

Dans le monde Internet les plus connus sont:

#### Protocole TCP - Transmission Control Protocol

#### Protocole UDP - User Datagram Protocol

#### Protocole RTP - Realtime Transport Protocol

D'autres applications encore comme la téléphonie et la vidéoconférence sur Internet ont des contraintes de temps réel.  La transmission de la voix et de la vidéo ne peuvent pas tolérer les variations de délais, appelées gigue, dans l'acheminement des paquets car les accélérations et ralentissements qui en résulteraient dans la restitution de la voix ou de l'images nuiraient gravement à la qualité de la transmission. Le **protocole RTP**, qui est utilisé en complément du protocole UDP, traite ces problèmes. 

### Couche Internet : IP

Elle permet de **déterminer les meilleurs chemins** à travers les réseaux en fonction des adresses IPv4 ou IPv6 à portée globale.
Les routeurs transfèrent le trafic IP qui ne leur est pas destiné.

#### Protocole ICMP

Le <bred>protocole ICMP - Internet Control Message Protocol</bred> :gb: est un protocole qui permet de gérer les informations relatives aux erreurs générées au sein d’un réseau IP. Etant donné le peu de contrôles que le protocole IP réalise, il permet, non pas de corriger ces erreurs, mais de faire part de ces erreurs.
Ainsi, le protocole ICMP est utilisé par tous les routeurs, qui l'utilisent pour reporter une erreur (appelé **Delivery Problem**).  
Un exemple typique d’utilisation du protocole ICMP est la commande `ping`. Lors de l’exécution de cette commande, des informations précises peuvent être obtenues : le temps mis par un paquet pour atteindre une adresse, ou bien un éventuel problème de routage pour atteindre un hôte.

### Couche Accès au réseau : LAN/WAN

La couche Accès Réseau qui organise le flux binaire et identifie physiquement les hôtes. Elle place le flux binaire sur les supports physiques. Les commutateurs, cartes réseau, connecteurs, câbles, etc. font partie de cette couche.
TCP/IP ne s’occupe pas de la couche Accès Réseau.

Au sens du modèle TCP/IP la couche Accès Réseau est vide, car la pile des protocoles Internet (TCP/IP) est censée “inter-opérer” avec les différentes technologies qui offrent un accès au réseau.

Plus on monte dans les couches, plus on quitte les aspects matériels, plus on se rapproche de problématiques logicielles.

## Notes et Références

[^1]: [TCP/IP, wikipedia](https://fr.wikipedia.org/wiki/Suite_des_protocoles_Internet)
[^2]: [Modèles TCP/IP et Modèle OSI, Goffinet.org](https://cisco.goffinet.org/ccna/fondamentaux/modeles-tcp-ip-osi/)
[^3]: [La suite de Protocoles TCP/IP](https://web.maths.unsw.edu.au/~lafaye/CCM/internet/tcpip.htm)
[^4]: [TCP/IP fête ses 30 ans, Le monde Informatique](https://www.lemondeinformatique.fr/actualites/lire-la-suite-tcp-ip-fete-ses-30-ans-51866.html)
[^5]: [Blog, Vint Cerf, Google](https://blog.google/inside-google/googlers/marking-birth-of-modern-day-internet/) :gb:
[^10]: [RFC 793, IETF, 1981](https://datatracker.ietf.org/doc/html/rfc793)
[^11]: [RFC 1122, IETF, 1989](https://datatracker.ietf.org/doc/html/rfc1122)

