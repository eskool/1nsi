# 1NSI: Réseaux: OSI vs TCP/IP

## Schéma de Comparaison

![OS vs TCP IP](./img/OSI-vs-TCP-IP.svg)

## Adressage, identifiants et matériels

Les machines et leurs interfaces disposent d’identifiants au niveau de chaque couche.

<center>

| Couche | Identifiant | Exemple |
|:-:|:-:|:-:|
| Couche Application | Un protocole et un nom de domaine | `http://` suivi de www.cisco.com|
| Couche Transport |	Port TCP ou UDP | TCP 80 comme port par défaut pour HTTP |
| Couche Internet |	Adresse IPv$4$ et/ou IPv$6$ | 192.168.130.252/24 ou 2001:ae6::1/64 |
| Couche Accès | adresse physique (MAC 802) | 70:52:63:dc:8e:47 |

</center>

## Résumé en Tableau

| OSI | TCP/IP | Rôles | PDU | Protocoles | Matériel |
|:-:|:-:|:-:|:-:|:-:|:-:|
| 7 Application | Application | Services au plus proche<br/>des utilisateurs | Données | HTTP, DNS, DHCP | Ordinateurs |
| 6 Présentation | Application | Encode, Chiffre, Compresse<br/> les données utiles | idem | idem | idem |
| 5 Session | Application | établit des sessions<br/> entre des applications | idem | idem | idem |
| 4 Transport | Transport | établit, maintient, termine<br/>des sessions entre des hôtes d’extrémité. | Segment | TCP ou UDP | Ordinateurs, Routeurs NAT, Pare-feux |
| 3 Réseau | Internet | Identifie les hôtes et assure<br/>leur connectivité | Datagramme ou Paquet | IPv4 ou IPv6 |Routeurs |
| 2 Liaison de Données | Accès Réseau | Détermine la méthode<br/>d’accès au support,<br/>organise les bits de données | Trame | Ethernet IEEE 802.3,<br/> Wi-Fi IEEE 802.11,<br/>pontage 802.1 | Commutateurs (Switchs),<br/>cartes d’interface réseau |
| 1 Physique | Accès Réseau | s’occupe du placement<br/>physique du signal | bits | Normes physiques :<br/>xDSL (WAN),<br/>1000-BASE-TX | Câblage (UTP CAT 6)<br/>et connecteurs (RJ-45),<br/>bande fréquences<br/>(2.4 GHz, 5 GHz) |
