# 1NSI: Réseaux - Routeur :fr: ou Router :gb:

## Définition

!!! def "Routeur"
    Un <bred>Routeur</bred> / <bred>Relais</bred> :fr: ou <bred>Router</bred> :gb: est un matériel réseau qui délimite un réseau. Un routeur est requis pour permettre à un hôte, appelé <bred>hôte source</bred>, de sortir de son propre réseau, afin de communiquer avec un hôte extérieur au réseau, appelé <bred>hôte de destination</bred>.  

![Routeur Deux Réseaux]( ../../img/routeur-deux-reseaux.png){.center}

<figure>
<figcaption>

Deux Réseaux Séparés par un seul routeur

</figcaption>
</figure>

!!! pte "Routeurs & Communication intra-réseau"
    Quand un hôte source souhaite communiquer avec un hôte de destination qui se trouve **dans un même réseau (intra-réseau)**, il peut le faire directement (éventuellement via un switch) mais **sans aucun besoin de passer par un routeur**.

## Table de Routage

!!! def "Table de Routage"
    Un routeur dispose d'une <bred>Table de Routage</bred> locale, qu'il maintient à jour **statiquement** (par l'Admin Réseau), ou **dynamiquement** (par le routeur).

    * La Table de Routage contient toutes les routes connues à un instant $t$. Mais la table de routage peut être mise à jour à chaque requête (ou avec des déclencheurs spécifiques), de diverses manières.
    * L'utilité principale de la Table de Routage est de permettre au routeur, grâce à un <bred>protocole de routage</bred> de déterminer:
        * le <bred>meilleur chemin</bred> à suivre pour atteindre sa destination. 
        * En pratique (et pour commencer) : son <bred>prochain saut</bred> :fr: / <bred>next hop</bred> :gb: (le prochain routeur).      
    
!!! exp "Plusieurs Routes Possibles ?"
    Il peut arriver qu'à un instant $t$, la Table de Routage dispose de plusieurs routes (doublons) pour l'hôte de destination demandé: dans ce cas, c'est le <bred>protocole de routage</bred> qui est en charge de déterminer le <bred>meilleur chemin</bred> à suivre.

!!! def "Route/Passerelle Par Défaut d'une Table de Routage"
    Il peut arriver qu'en l'état actuel de sa Table de Routage, le routeur ne connaisse pas encore, pour l'hôte de destination demandé, quelle est l'interface de sortie pour le prochain saut: auquel cas, une table de routage dispose **en général** d'une <bred>route par défaut</bred>, ou <bred>passerelle par défaut</bred>, vers laquelle le routeur renvoie les données *dont il ne sait pas quoi faire*.  
    S'il n'y a pas de route/passerelle par défaut configurée, et qu'il ne sait pas quoi faire du paquet, alors le routeur **drop** le paquet: il le supprime.

## Protocoles de Routage

!!! def "Protocoles de Routage: Meilleur Chemin & Prochain Saut"
    Le routeur utilise un <bred>protocole de routage</bred> et les informations stockées dans la table de routage,  pour déterminer :
    
    * le <bred>meilleur chemin</bred> pour qu'un paquet reçu atteigne sa destination. 
    * En pratique (et pour commencer): l'interface de sortie vers le <bred>prochain saut</bred> :fr: ou <bred>next hop</bred> :gb:, càd le prochain routeur, du meilleur chemin vers l'hôte de destination:
    
<env>Rappel</env> En cas de doublons (plusieurs chemins possibles) c'est le rôle du <bred>protocole de routage</bred> d'estimer / de calculer quel est le <bred>meilleur chemin</bred> vers l'hôte de destination.

!!! def "Métriques d'un protocole de Routage"
    Pour déterminer le meilleur chemin vers une destination donnée, les protocoles de routage utilisent une **distance** ou <bred>métrique</bred>. Chaque protocole de routage dispose de sa *propre métrique*, et de sa propre façon de calculer le chemin le plus court, à l’aide d’un algorithme distinct.  
    Quelques exemples de paramètres pris en compte pour calculer la métrique dans les protocoles de routage sont:
        * La **Bande passante**: C’est le débit du lien entre routeurs
        * Le **Délai**: C’est la durée requise pour déplacer le paquet d’un point à un autre
        * Le **Coût**: C’est une valeur arbitraire qu'un administrateur réseau peut manuellement attribuer
        * le **Nombre de Sauts** : C'est le nombre de routeurs qu'un paquet doit traverser avant d'atteindre sa destination.  

!!! exp "de Noms de Protocoles de Routage"
    Plusieurs protocoles de routages existent, dont certains seront étudiés en Terminale, notamment :  

    * <bred>RIP (v2) - Routing Information Protocol</bred>
    * <bred>OSPF - Open Shortest Path First</bred>
    * etc..

## Principe de Routage Inter-Réseaux (sur Internet)

!!! pte "Routeurs & Communication Inter-Réseaux"
    1. Quand un hôte source souhaite communiquer avec un hôte de destination qui se trouve **dans un autre réseau (inter-réseaux)**, il doit **passer obligatoirement par (un moins) routeur**.
    2. Qui transmet au prochain routeur, et ainsi de suite, etc..
    3. jusqu'à arriver au dernier routeur (auquel le réseau de l'hôte de destination est **connecté directement**) et pouvoir finalement acheminer les données vers l'hôte de destination

!!! def "TTL - Time To Live = Nombre de Sauts"
    A chaque nouveau routeur traversé, la variable <bred>Nombre de Sauts</bred> :fr: ou <bred>TTL - Time To Live</bred> :gb: est augmentée de $1$.  
    Cela permet d'éviter les boucles ~~infernales~~ infinies dans les réseaux

![Routeur Deux Réseaux]( ../../img/principe-routage.png){.center}

<figure>
<figcaption>

Deux Réseaux Séparés par une succession de routeurs et de réseaux<br/>
(Internet)

</figcaption>
</figure>

* L'**Hôte Source** :one: envoie un paquet IP à destination du **Serveur de Destination** :six: à droite. Ce paquet est envoyé à sa *passerelle par défaut* :two: (le premier routeur de sortie de son réseau *Site CLient* à gauche)  
* La passerelle :two: (le routeur) reçoit le paquet, extrait l’adresse IP de destination (du serveur de destination :six:, à droite) et la compare à sa **table de routage**. La table lui indique d’envoyer ce paquet via l'une de ses **interfaces** au routeur voisin qu'il estime être le **meilleur chemin** suivant  : le **prochain saut** ou **next hop**  
* Idem que l’étape précédente : routeur :three: $\rightarrow$ routeur :four: 
* Idem que l’étape précédente : routeur :four:  $\rightarrow$ routeur :five: 
* Le routeur :five: (ler dernier) reçoit le paquet, il extrait l’adresse IP de destination et la compare à sa table de routage. La table lui indique cette fois-ci qu’une de ses interfaces est directement dans le réseau de destination (qui contient l’adresse IP du serveur). Le routeur envoie le paquet IP à la destination :six: (le serveur de destination, à droite) sans passer par un autre routeur

<env>Remarque</env> On remarque que l’entête IP de destination est conservée durant tout le transport. L’entête niveau 2 est quant à lui détruit à l’entrée d’un routeur et reconstruit lors de la sortie du routeur.

## Routeur & Modèle OSI

!!! pte "Routeur & Modèle OSI"
    Un Routeur est un matériel réseau **de niveau $3$ du modèle OSI** (les trois couches les plus basses: la couche Physique, la couche Liaison de Données, et la couche Réseau): En pratique, cela veut dire qu'un routeur n'a besoin de connaître que l'**adresse IP** de l'hôte de destination, pour lui envoyer les paquets (en passant éventuellement par plusieurs autres routeurs intermédiaires).
    
## Fonctionnement

Le Fonctionnement interne d'un routeur sera étudié plus précisément en classe de Terminale, en se concentrant sur le fonctionnement précis de deux protocoles de routage:

* RIP (v2)
* OSPF

## Routeur & Domaine Diffusion / Broadcast

!!! pte "Routeur & Domaines de Diffusion"
    Un Routeur considère par défaut que **chacune de ses interfaces est dans un domaine de diffusion différent**, ou ce qui revient au même, chacune de ses interfaces est dans **un réseau différent**

## Routeurs & Domaine Collision

!!! pte "Routeur & Domaines de Collision"
    Un Routeur considère par défaut que **chacune de ses interfaces est dans un domaine de collision différent**.

## Résumé

![Routeur : Domaine de Diffusion et Collision]( ../../img/routeur-diffusion-collision.png){width=80% .center}