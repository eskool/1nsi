# 1NSI: Réseaux - Hubs :gb: ou Concentrateurs :fr:

## Définition

Historiquement, le **Hub** est un des équipements réseau les plus anciens, bien avant les **bridges** (**ponts**) et les **switchs** (**commutateurs**). Cet équipement est à bannir des réseaux locaux d’aujourd’hui car il engendre beaucoup de problème de ralentissement. Nous verrons ensemble pourquoi plus tard. Il dispose de $2$ à $8$ ports RJ45 / interfaces.

!!! def "Hub ou Concentrateur"
    Un <bred>Hub</bred> :gb: ou <bred>Concentrateur</bred> :fr: est un matériel réseau qui renvoie toutes les informations (trames) reçues (sur l'un de ses ports/interfaces) vers tous ses autres ports/interfaces, donc vers tous les péripériques ayant des cartes réseaux connectées au hub :

    * **sans filtre** : Toutes les informations renvoyées sur tous les ports de sortie quels qu'ils soient
    * **sans mémoire** : le hub ne stocke pas les adresses IP des matériels déjà connectés (au moins une fois)

!!! pte "Hub Non administrable"
    Un Hub N'est PAS administrable, il est Plug & Play.
    C'est une sorte de prise multiple Ethernet (ou USB..)

![Hub Ethernet]( ../../img/hub-ethernet.png){width=80% .center}

<figure>
<figcaption>
Hub Ethernet Domestique EN104
</figcaption>
</figure>

![Hub USB]( ../../img/hub-usb.png){width=80% .center}

<figure>
<figcaption>
Hub USB StarTech.com
</figcaption>
</figure>

## Hubs & Modèle OSI

!!! pte "Hub & Modèle OSI"
    Le Hub ne connaît que le niveau $1$ du modèle OSI ((la couche la plus basse: la Couche Physique):  
    **Il renvoie TOUTE trame reçue vers chacun de ses autres ports/interfaces**.  

SIMPLE MAIS POLLUANT : En particulier, il ne sait pas interpréter les trames reçues au sens niveau $2$ du modèle OSI, donc il n'est pas capable d'interpréter l'adresse MAC du destinataire

## Domaine de Diffusion / Broadcast

Cela veut dire que lorsqu’une station $A$ envoi une trame Ethernet pour une station $B$, en fait toutes les stations connectées sur le Hub recevront aussi la trame car le Hub duplique l’information sur tous ses ports.

!!! def "Domaine de Diffusion / Broadcast"
    On dit que les stations font parties du même <bred>Domaine de Diffusion / Broadcast</bred>. Dès qu’une station émet, toutes les autres reçoivent la trame: La station $A$ envoi une trame, les stations $B$ et $C$ recevront la trame.

![Hub & Domaine de Broadcast]( ../../img/hub-et-domaine-de-diffusion.png){.center .box}

Les cartes réseaux des autres stations sont néanmoins intelligentes et savent lire les informations contenues dans l’entête de la trame Ethernet et vérifie si l’adresse MAC de destination correspond à son adresse MAC. Si ce n’est pas le cas alors la carte réseau supprime la trame car elle ne lui est pas destinée.

## Domaine de Collision

Mais que se passe-t-il lorsque deux stations émettent une trame en même temps?
Imaginons que les stations $A$ et $B$ émettent une trame en même temps:

![Hub & Domaine de Broadcast]( ../../img/hub-et-domaine-de-collision.png){.center .box}

Le Hub se contentant de tout dupliquer sur tous ses ports, les impulsions électriques vont se chevaucher et seront alors corrompues, on parle alors de <bred>Collision</bred>. 

!!! def "Domaine de Collision"
    On dit que toutes les machines connectées au Hub appartiennent au même <bred>Domaine de Collision</bred>.

Une analogie avec les transmissions audios serait par exemple le **Talkie-Walie**, dont le fonctionnement était le suivant:

* Si on veut communiquer, et utiliser le canal, on appuie sur le bouton et on parle. 
* les autres peuvent écouter, mais sans parler
* Deux personnes appuyant simultanément sur le bouton provoquent des collisions

Heureusement, il existe certaines méthodes (plus complexes), comme par exemple la <bred>méthode CSMA/CD - Carrier Sense Multiple Access with Collision Detection</bred> ou <bred>Circuit de Détection de Collision</bred> qui permet de réduire ces risques de collisions, de bloquer les PC qui posent problème pendant une période aléatoire avant qu’ils puissent retransmettre.

## Le mode Half-Duplex

!!! def "Mode Half-Duplex"
  Le <bred>mode Half-Duplex</bred> d'envoyer des données mais pas d'en recevoir simultanément. et Réciproquement.
  La communication ne peut se faire qu'en un seul sens simultanément.

!!! pte "Le Hub est Half-Duplex"
  **Le Hub fonctionne en Half-Duplex**, exclusivement, sur tous ses ports/interfaces
  On utilise la technologie CSMA/CD pour diminuer les risques de collision en Half-Duplex.

## Résumé

!!! pte "A Retenir"
    * Le Hub est un **équipement obsolète**
    * Toutes les machines connectées à un Hub sont dans un même **Domaine de Diffusion / Broadcast**
    * Toutes les machines connectées à un Hub sont dans un même **Domaine de Collision**
    * Le Hub ne fonctionne qu'en mode Half-Duplex, sur tous ses ports/interfaces
