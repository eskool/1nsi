# 1NSI: Réseaux - Passerelle ou Gateway

!!! def "Passerelle"
    D'une manière très générale:  
    1. Une <bred>Passerelle</bred> :fr: ou <bred>Gateway</bred> :gb: le **nom générique** d'un dispositif (matériel et/ou logiciel) permettant de relier deux réseaux informatiques de types différents, classiquement un réseau local et le réseau Internet.
    2. Une passerelle est différente d'un routeur et/ou d'un switch au sens que:

    * elles peuvent communiquer avec plusieurs réseaux en utilisant **plus qu'un protocole**, et 
    * peuvent opérer sur **n'importe quel niveau du modèle OSI**.

!!! pte "Types de Passerelles"
    Il existe $3$ types de passerelles:

    * un **répéteur** est une passerelle **de niveau $1$**
    * un **pont** est une passerelle **de niveau $2$**
    * un **relais** souvent appelé **routeur** est une passerelle **de niveau $3$**

Plus précisément, nous utiliserons la notion de *passerelle par défaut*:

!!! def "Passerelle Par Défaut"
    Une <bred>passerelle par défaut</bred> :fr: ou <bred>default gateway</bred> :gb:, est le noeud de communication de sortie d'un réseau, utilisant le protocole TCP/IP, servant en tant que routeur vers d'autres réseaux, **lorsqu'aucune autre route spécifiée ne coïncide l'adresse IP de destination d'un paquet** ("*par défaut*").