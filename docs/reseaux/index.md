# 1NSI : Réseaux - Repères Historiques

## ARPAnet: l'ancêtre d'internet

!!! col __70
    L'<bred>ARPANET</bred> / <bred>ARPAnet</bred> / <bred>Arpanet - Advanced Research Projects Agency Network</bred> :us: aurait eu $50$ ans cette année. Le premier "réseau à <bred>commutation de paquets</bred>" ou "réseau à transfert de paquets", développé par la <bred>[DARPA - Defense Advanced Research Projects Agency](https://fr.wikipedia.org/wiki/Defense_Advanced_Research_Projects_Agency)</bred> :us: / <bred>Agence pour les Projets de Recherche de Défense Avancée</bred> américaine, a crée tous les protocoles qui ont permis la naissance de son héritier : <bred>Internet</bred>.

!!! col __30 right clear
    [![DARPA Logo](./img/darpa.png)](https://fr.wikipedia.org/wiki/Defense_Advanced_Research_Projects_Agency)

Le $29$ octobre $1969$, il y a maintenant $50$ ans, le réseau **ARPAnet** effectuait sa première communication: l'envoi du mot "**login**". Les trois dernières lettres (GIN) de ce premier message, mettent plusieurs heures avant de parvenir à destination mais qu'importe, l'ARPAnet a rempli sa première mission : des paquets de données ont transité avec succès entre l'**Université de Californie** à Los Angeles (**UCLA**) et l'**Institut de Recherche de Stanford**. L'ancêtre d'internet (qui n'est donc pas le minitel) est né ! Et il s'agit d'une collaboration entre militaires et universitaires. 

## Un projet Militaire

En réalité, ce réseau dédié au transfert de paquets de données est envisagé depuis $1966$, lorsque la DARPA, une antenne du Département de la Défense (DoD) des États-Unis finance à hauteur d'**un million de dollars** un projet visant à relier les universités en contrat avec l'agence à un même réseau, délocalisé, pour transférer des fichiers entre ordinateurs. Le réseau téléphonique n'étant pas adapté, il est décidé d'utiliser la **commutation par paquets**, qui consiste à découper les données avant de les envoyer d'un point à un autre.

Assez rapidement, l'idée est évoquée de créer non pas un unique ordinateur dédié pour faire transiter les informations, mais un petit réseau d'ordinateurs, les **Interfaces Messages Processors** (considérés comme les ancêtres des routeurs), qui permettent d'assurer une connexion en réseau.  

Créé en pleine guerre froide, le réseau Arpanet a un autre avantage : il offre la possibilité de contrer les dégâts potentiels d'une bombe atomique, puisque l’interconnexion des différents noeuds du réseau permet à l'information de continuer à circuler malgré la destruction d'un ou plusieurs noeuds. Ce réseau est en réalité le fruit d'une collaboration multiple :

> On parle de complexe militaro-scientifico-industriel. Quand on parle des militaires c’est aussi pour rappeler le rapport de la Rand corporation de Paul Baran, qui va poser un principe qui est celui de la “patate chaude” : c’est-à-dire de ces paquets qui circulent dans le réseau. Il n’est pas le seul inventeur de la commutation de paquets mais il y contribue. Il y a un certain nombre d’éléments qui font que nier une origine complètement militaire de l’internet est absurde.
>
> Valérie Schaffer[^1], Historienne des Télécommunications

Cette imbrication est "à l'origine de la DARPA puis du projet Arpanet":

> Il y a une logique d’irrigation par des crédits, extrêmement massifs, d’équipes de recherche dont on ne sait pas, quand on commence à les financer, ce qui peut en sortir. Et ça c’est une vision assez typiquement américaine dans les années 60, des Etats-Unis qui cherchent à conserver un leadership technologique dans le cadre de conflits futurs, éventuellement, mais également dans un objectif de compétition économique. C’est faire financer des recherches dans des domaines qui sont parfois très différents, et les réseaux en ont fait partie, pour essayer de creuser l’écart. Quand on finance, on n'a pas forcément un objectif militaire au sein de ces projets et le projet Arpa c’est essentiellement un projet de liaison entre universitaires, que les militaires vont ensuite chercher à utiliser. L'origine militaire est à convoquer dans le faisceau des nombreuses origines, universitaires, liées à l’essor des télécommunications de cet ancêtre d’internet qu’est Arpanet.
>
> Benjamin Thierry[^2], Historien des Techniques et des Médias

## D'Arpanet à Internet

Lancé en $1969$, Arpanet se raccorde rapidement à de nouvelles universités : on compte $23$ "noeuds" en $1971$, puis $111$ en $1977$. Au vu de la démocratisation du réseau Arpanet, la DARPA se sépare de sa gestion en créant un réseau propre aux forces armées américaines, le **MILNet - MILitary Network**. 

<center>

![ARPANET Logical Map](./img/arpanet-logical-map.png){.box}
Architecture Logique du Réseau ARPAnet, Mars $1977$ ($111$ noeuds)

</center>

C'est la **NSF - National Science Foundation** qui prend en charge la transition vers une utilisation tout public : l'adoption d'un nouveau protocole, TCP/IP, a permis de faciliter le transfert des données et peu à peu "internet" se substitue à "Arpanet".

## D'Internet au Web - WWW

!!! col __30 right
    [![Tim Berners Lee](./img/tim-berners-lee.jpg)](./img/tim-berners-lee.jpg)
    ![Logo CERN](./img/cern.jpg){.box}

Il faut cependant attendre la fin des années $80's$ et l'invention du "**Web**" ou **WWW - WorldWideWeb** (not World Wild Web .. nuance.. :stuck_out_tongue: ) une sorte d'internet avec une interface graphique[^2], pour que le nombre d'utilisateurs explose. C'est **Tim Berners Lee** (informaticien britannique, prix Turing $2016$) :gb: qui est le réel inventeur du Web. En Mars $1989$, alors qu'il travaille au **CERN - Conseil Européen pour la Recherche Nucléaire**, il rédige son article fondateur "*WorldWideWeb : Proposition pour un projet hypertexte, basé sur internet*" (Mars $1989$) dans lequel il invente le Web, et au passage $3$ des technologies fondamentales du web:

* les URLs (UDIs, puis URLs, puis URIs)
* le protocole HTTP, et sa première implémentation (premier navigateur web)
* le Langage HTML

Ci-dessous, le premier site web de l'Humanité: [http://info.cern.ch/hypertext/WWW/TheProject.html](http://info.cern.ch/hypertext/WWW/TheProject.html)

<iframe src=" http://info.cern.ch/hypertext/WWW/TheProject.html" style="width:100%; height:700px;background-color:white;"></iframe>

<clear></clear>

## Références & Notes

[^1]: Interview Valérie Schaffer, France Culture 29/10/2019: https://www.franceculture.fr/sciences/arpanet-lancetre-dinternet-a-50-ans
[^2]: Interview Benjamin Thierry, France Culture 29/10/2019: https://www.franceculture.fr/sciences/arpanet-lancetre-dinternet-a-50-ans
[^3]: [Biography Tim Berners Lee](https://www.w3.org/People/Berners-Lee/) :gb: