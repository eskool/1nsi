# 1NSI : Réseaux - Introduction

<center>

| Contenus | Capacités Attendues |
|:-:|:-:|
|Transmission de<br/>données dans un réseau | Mettre<br/>en évidence l’intérêt du<br/>découpage des données en<br/>paquets et de leur<br/>encapsulation. |
| Protocoles de<br/>communication<br/>| Dérouler le fonctionnement d’un<br/>protocole simple de<br/>récupération de perte de<br/>paquets (bit alterné). |
| Architecture d’un réseau | Simuler ou mettre en oeuvre un réseau. |

</center>

L'ancien modèle d'un ordinateur unique qui répondrait à tous les besoins d'une personne, d'une famille ou d'une entreprise, à été supplanté (dépassé) par un nouveau modèle, dans lequel les traitements sont effectués par plusieurs machines / <bred>hôtes</bred> distincts mais **interconnectés** (càd pouvant échanger/partager des informations) via un **canal de communication**: ces systèmes sont les <bred>réseaux</bred> :fr: ou <bred>networks</bred> :gb:.

## Qu'est-ce qu'un Réseau ?

!!! def "Réseau"
    Un <bred>réseau</bred> :fr: ou <bred>network</bred> :gb:, est un ensemble d'éléments, **matériels** et **logiciels**, permettant le **partage d'informations et de ressources**, via des **canaux de communications**, et partageant (de manière sous-entendue) les mêmes **protocoles de communication** (classiquement TCP/IP).

!!! def "Hôte"
    Un <bred>hôte</bred> :fr: ou <bred>host</bred> :gb: est une **machine** ou **équipement matériel** au sens large (ordinateur au sens général -client ou serveur- téléphone, tablette, ou imprimante, etc..) appartenant à un réseau

!!! def "Canal de Communication"
    un <bred>Canal de Communication</bred> peut être:

      * un câble de cuivre (ou autre matériau), mais pas exclusivement
      * de la fibre optique
      * des micro-ondes
      * des ondes infra-rouges
      * des satellites de communication
      * etc..

## Qu'est-ce qu'un Sous-Réseau? 

!!! def "Sous-Réseau"
    Un <bred>Sous-Réseau</bred> :fr: ou <bred>Subnet</bred> :gb: est **un réseau à l'intérieur d'un réseau**.
    Ou bien, dit autrement, c'est une **subdivision logique** d'un réseau (de taille plus importante), càd une partie seulement des appareils d'un réseau, que l'on a regroupé "*logiquement*" ensemble, càd en associant à chaque appareil du même sous-réseau des identifiants numériques (des adresses IP) qui se ressemblent entre eux d'une certaine manière (cf *masques de sous-réseau*) qui permet de les reconnaître entre eux, par rapport aux autres appareils du réseau mais n'appartenant pas à ce sous-réseau.
    
!!! def "(Spoiler) Masque de Sous-Réseau (nous y reviendrons)"    
    Le <bred>Masque de Sous-Réseau</bred> :fr: ou <bred>Subnet Mask</bred> :gb: est une adresse IP qui permet de distinguer la partie de l'adresse IP commune à tous les appareils du sous-réseau (la "*partie réseau*") de celle qui varie d'un appareil à l'autre (la "*partie hôte*"). 
    
<env>Exemples</env>  
Un **sous-réseau** correspond typiquement à un **réseau local (LAN)**.  
Historiquement, on appelle également sous-réseau **chacun des réseaux connectés à Internet**. 

## Qu'est-ce qu'un Inter-réseaux?

Il existe beaucoup de réseaux dans le monde, souvent composés de matériels et logiciels différents. Les utilisateurs souhaitent fréquemment communiquer avec d'autres personnes reliées à d'autres réseaux. Pour résoudre ce problème, les différents réseaux souvent incompatibles entre eux, doivent être interconnectés

!!! def "Inter-Réseaux"
    Un ensemble de réseaux interconnectés sont appelés des <bred>inter-réseaux</bred>

!!! exp "Internet"
    <bred>Internet</bred>, qui est un réseaux de réseaux, est le plus connu des inter-réseaux. il connecte:

    * des réseaux de FAI- Fournisseur Accès Internet :fr: / ISP - Internet Service Provider :gb:
    * des réseaux d'entreprise
    * des réseaux d'établissements scolaires
    * des réseaux domestiques
    * et bien d'autres réseaux

## A quoi sert un Réseau? Quels usages?

De nos jours, bien que n'ayant pas forcément les mêmes besoins, tout le monde utilise des réseaux, aussi bien les particuliers (utilisation domestique) que les entreprises / établissements scolaires (utilisation professionnelle). Des ressemblances dans leurs usages existent donc, ainsi que quelques différences (quelquefois historiques). En pratique, les particuliers utisent souvent les réseaux comme un sous-ensemble, ou une simplification, ou une déviation de leur utilisation professionnelle initiale.

### Des Utilisations Professionnelles et Domestiques

Le but principal d'un réseau est de:

#### Échanger des informations

 (entre différents hôtes du réseau)

#### Partager des ressources

* des **équipements matériels** : le partage les rend plus économiques, et plus rapides et simples à maintenir 
    * Imprimantes
    * Disques durs (ou plus généralement Serveurs de Stockage)
    * Serveurs de Bases de données, 
    * Serveurs Webs, etc.. 
* des **données et applications logicielles** :
    * services de fichiers
    * services de gestion électronique de documents
    * services de gestion de Base de Données
    * services d'imprimantes & *spoolers*
    * services de messagerie & Travail Collaboratif
    * services d'application
    * services de stockage
    * services de sauvegarde
    * protocoles de réplication entre baies
    * etc..

Ce partage existe aussi bien pour des utilisations domestiques que professionnelles, mais vec quelques variations de technicité.

### Le VPN - Virtual Private Network

Au sein d'une petite société, les ordinateurs se trouvent fréquemment dans un même bureau, ou un même immeuble. Dans les plus grandes sociétés, les commerciaux sont souvent en itinérance de par le monde, et sont amenés à accéder à une base de données de l'entreprise (base de données clients, base de données produits, fournisseurs, factures etc..) depuis n'importe quel endroit de la planète. On utilise pour cela des <bred>Réseaux Privés Virtuels</bred> :fr: ou <bred>VPN - Virtual Private Network</bred> :gb: qui sont des sortes de <bred>tunnels virtuels sécurisés</bred> (donc privés) de communication, servant à **relier, de manière sécurisée/chiffrée, les réseaux individuels des différents sites en un seul réseau étendu**. Ainsi la distance géographique n'empêche plus un utilisateur de se connecter.

Les particuliers, quant à eux, doivent souscrire à un abonnement VPN souvent payant pour pouvoir en bénéficier (encore que certains soient gratuits, mais sont usuellement/globalement moins bons). Les particuliers utilisent les VPN principalement pour trois raisons:

#### Pour la Sécurité

Par exemple pour *sécuriser/chiffrer leurs connexions* à internet depuis des endroits non sécurisés (wifi "publics", et/ou avec un même mot de passe partagé entre les clients: halls d'aéroports, stations de gares, hôtels, etc...). Un DNS offre à un particulier les avantages suivants :

* Cryptage de niveau militaire
* Protocoles variés et sécurisés (OpenVPN, L2TP, IKEv2, etc.)
* protection contre les *fuites DNS* (exposition des données confidentielles de navigation ouvrant la voie à son piratage, et extraction de la localisation réelle et emplacement du **FAI - Fournisseur d'Accès Internet**)
* Un *Kill Switch* :gb: ou *bouton d'arrêt d'urgence* :fr: qui est capable de couper votre trafic internet dès l'instant où il détecte un problème de connexion du VPN, qui peut ne durer de quelques secondes, protégeant ainsi votre adresse IP qui restera masquée.

#### Pour contourner la géolocalisation

Pour être connectés à internet via le réseau privé distant accessible via la VPN, càd que l'adresse IP publique de l'utilisateur sera celle du réseau privé distant (au lieu de l'adresse IP publique du réseau local de l'utilisateur). Une utilisation classique des VPN pour les particuliers est le **déblocage de contenus à restrictions géographiques** (par ex. contenus/vidéos restreintes à des zones géographiques).

#### Pour masquer son adresse IP

Ceci revient techniquement à la même chose que le point précédent, mais pour certains utilisateurs, masquer son adresse IP est un but en soi :

* Lanceurs d'alerte
* Journalistes et/ou personnes engagées des régimes totalitaires
* hackers en tout genre (pour le bien et pour le mal: hackers à chapeau *"blanc"*, *"noir"*, *"gris"*, etc..)

### IRC - L'ancêtre du '*Chat*'

<bred>IRC - Internet Relay Chat</bred> ($\ge$ août $1988$) est un protocole de <bred>messagerie instantanée</bred> :fr: ou <bred>IM - Instant Messaging</bred> :gb: (communications/discussions textuelles instantanées) au travers de réseaux (internet principalement): C'est l'ancêtre du <bred>'*Chat*'</bred> !

Pour utiliser ce protocole, il est nécessaire de se connecter sur un réseau dédié grâce à un logiciel (client) appelé un <bred>client IRC</bred>. Par exemple :

* Linux: *HexChat*, *Konversation*, *Weechat* (Terminal), *Thunderbird*, 
* Windows : *Chatzilla*, *Thunderbird*, 
* Mac: *Colloquy*, *Adium*, etc...

Historiquement, $4$ grands **réseaux** cohabitaient : **IRCnet**, **DALnet**, **EFnet**, et **Undernet**, mais dans les années $2000$, deux nouveaux réseaux sont devenus particulièrement populaires : **QuakeNet** (orienté sur le *jeu vidéo*) et **freenode**, puis de nos jours, **Libera.Chat** (tous deux orientés sur le *Libre* et l'*Open Source* tant au sens informatique que liberté de parole). Mais il existe de nombreux autres réseaux, dont on peut trouver des listes sur internet, par exemple [IRC Networks - Top100](https://netsplit.de/networks/top100.php) (attention tout de même, on trouve de tout comme thématiques..).

Chaque *réseau* admet plusieurs <bred>Canaux IRC</bred> pour des discussions instantanées :fr: ou <bred>IRC channels</bred> :gb: souvent abrégés <bred>chans</bred> (quelquefois des *chat rooms*), qui sont des sortes de **Salons de Discussions Instantanées**, dont les noms commencent par un `#`

A la fin des années $1990$, l'utilisation d’*IRC* diminue avec l’arrivée des messageries instantanées propriétaires grand publics comme *ICQ*, puis *MSN Messenger*. De nos jours, *IRC* reste néanmoins utilisé dans certains milieux (geek notamment) désirant se passer d'un programme client propriétaire, et appréciant son interopérabilité et son organisation sous forme de canaux propices à la communication en groupe. De plus, La plupart des **logiciels de messagerie instantanée** implémente encore le **protocole IRC** (par exemple Miranda IM, Pidgin, Trillian).

<figure>

<center>

<img src="./img/irc.webp"/>

<figcaption>

IRC, l'ancêtre du Chat

</figcaption>

</center>

</figure>

Aller plus loin :

* [IRC, mais c'est quoi en fait ?](https://bioinfo-fr.net/irc-mais-cest-quoi-en-fait)
* [IRC - wikipedia](https://fr.wikipedia.org/wiki/Internet_Relay_Chat)
* [Liste de Réseaux IRC Francophones](http://plus.wikimonde.com/wiki/Liste_de_r%C3%A9seaux_IRC_francophones)
* [Top100 IRC Channels](https://netsplit.de/networks/top100.php)

### La VoIP - Voice over IP

Pour partager des conversations téléphoniques entre utilisateurs d'un même réseau (familles, entreprises, établissements scolaires, etc..) ceux-ci peuvent utiliser une technologie internet appelée <bred>téléphonie IP</bred>, ou <bred>voix sur IP</bred> :fr:, ou <bred>VoIP - voice over IP</bred>, qui autorise les appels en utilisant le réseau informatique, au lieu du réseau de l'opérateur téléphonique. Cela exige des **microphones** sur chaque hôte.

### Le partage d'écran/bureau

Le réseau informatique peut également être utilisé pour partager son écran / partager son bureau (d'ordi).
Cette technologie peut admettre plusieurs utilisations :

* dépannage à distance pour venir en aide aux personnes moins qualifiées
* mur partagé de type *Google Docs*, où plusieurs personnes peuvent simultanément apporter des modifications en direct, et de manière synchronisée, à un même document de travail (ou pas..).

## Le Modèle Client - Serveur

Encore une fois, nous nous inspirons des réseaux tels qu'ils existent dans les entreprises/établissement scolaires.

Pour simplifier, un <bred>Système d'Information</bred> d'entreprise / établissement scolaire, est un ensemble dans lequel on trouve:

* une ou plusieurs <bred>bases de données</bred> :fr: ou <bred>databases</bred> :gb: contenant les données ainsi que la gestion des droits d'accès y afférant
* des <bred>utilisateurs</bred> :fr: ou <bred>users</bred> :gb:, disposant de certains droits d'accès aux données et d'utilisation du réseau

### Description du modèle

<center>

![Modèle Client Serveur](./img/client_serveur_avec_requete_reponse.png) {width=70%}

<figcaption>

Le Modèle Client-Serveur

</figcaption>

</center>

* les (bases de) données sont stockées dans des ordinateurs puissants appelés <bred>Serveurs</bred>
* Les utilisateurs travaillent sur des postes/hôtes plus simples appelés <bred>Clients</bred>
* Les serveurs sont souvent regroupés physiquement et logiquement ensemble et sont gérés, ainsi que les droits des utilisateurs, par le (ou les) <bred>Admin(istrateur) Réseau</bred> ou <bred>Admin(istrateur) Système</bred> :fr: ou <bred>System Admin(istrator)</bred> :gb: souvent appelé <bred>admin</bred> ou <bred>sysadmin</bred>
* Deux <bred>processus</bred> (deux programmes qui s'exécutent en arrière plan) sont impliqués: un processus sur la machine client ou sur <bred>*le client*</bred>, et un sur la machine serveur ou <bred>*le serveur*</bred>:
    * Le processus client envoie <bred>une requête</bred> :fr: ou <bred>request</bred> :gb:, càd une demande sous forme de texte, au processus serveur, et le client attend une réponse
    * le processus serveur reçoit la requête, exécute la tâche demandée et/ou recherche les données demandées, et envoie une <bred>réponse</bred> :fr: ou <bred>answer</bred> :gb: au processus client
    * Plusieurs clients peuvent envoyer des requêtes au même serveur, qui leur répond plus ou moins en parallèle

### Avantages & Inconvénients

#### Avantages

* Plus facile à maintenir, car une seule machine à maintenir

#### Inconvénients

* Coût élevé dû à la technicité du serveur
* Le serveur est le noeud central du modèle : s'il tombe, tout le monde tombe (Architecture en étoile). Pour résoudre ce point clé, on utilise le **Système RAID** améliore grandement la tolérance aux pannes. 

## Le Modèle P2P - Peer2Peer

Dans le modèle <bred>Pair à Pair</bred> :fr:, ou d'**égal à égal**, ou <bred>Peer to Peer</bred> :gb: noté <bred>P2P - Peer2Peer</bred>, les utilisateurs forment un groupe informel au sein duquel chacun peut communiquer avec tous les autres. L'échange se fait **entre pairs sur un pied d'égalité**, d'où son nom. Chaque utilisateur est considéré comme un <bred>noeud</bred> :fr: ou <bred>node</bred> du réseau P2P.  
En pratique, pour faire partie du réseau, il faut installer sur son ordinateur un logiciel appelé un <bred>client P2P</bred>. Il en existe de nombreux, et le choix de l'un d'entre eux influence :

  * l'architecture du réseau P2P (il en existe plusieurs)
  * les protocoles de communication P2P (il en existe plusieurs également, les noms ressemblent souvent à ceux des  les clients P2P)

### Différentes Architectures P2P

Historiquement, il existe plusieurs architectures de réseau P2P, dans le sens chronologique (du plus ancien vers le plus moderne) :

#### Centralisé

<center>

![P2P Centralisé](./img/p2pcentralise.jpg)

<figcaption>

Le Modèle P2P Centralisé

</figcaption>

</center>

Dans cette architecture P2P, le contenu, services, ressources, recherche, localisation, indexation et publication sont centralisés sur un même serveur. Exemples: Napster, Audiogalaxy, eDonkey2000

!!! exp
    [Napster](https://fr.wikipedia.org/wiki/Napster), [Audiogalaxy](https://fr.wikipedia.org/wiki/Audiogalaxy), [eDonkey2000](https://fr.wikipedia.org/wiki/EDonkey2000)

#### Semi-Centralisé

<center>

![P2P Semicentralisé](./img/p2psemicentralise.jpg)

<figcaption>

Le Modèle P2P Semi-Centralisé

</figcaption>

</center>

Dans cette architecture P2P, le contenu est distribué mais les fonctions de recherche, localisation, indexation et ainsi que la publication sont centralisées.

#### Décentralisé

<center>

![P2P Décentralisé](./img/p2pdecentralise.jpg)

<figcaption>

Le Modèle P2P Décentralisé

</figcaption>

</center>

Dans cette architecture P2P, tout est décentralisé. Chaque noeud est à la fois *client* et *serveur*.

!!! exp
    [CAN](https://fr.wikipedia.org/wiki/Content_Adressable_Network), [Chord](https://fr.wikipedia.org/wiki/Chord), [Freenet](https://fr.wikipedia.org/wiki/Freenet), [GNUnet](https://fr.wikipedia.org/wiki/GNUnet), [Tapestry](https://fr.wikipedia.org/wiki/Tapestry), [Pastry](https://en.wikipedia.org/wiki/Pastry_(DHT)) et [Symphony](http://homepage.divms.uiowa.edu/~ghosh/Symphony.pdf)

#### Décentralisé *avec des super-noeuds* ou *en essaim*

<center>

![P2P Décentralisé avec Super Noeuds](./img/p2pdecentraliseavecsupernoeuds.jpg)

<figcaption>

Le Modèle P2P Décentralisé avec des Super-Noeuds

</figcaption>

</center>

Dans cette architecture P2P, les **super-noeuds** ont des fonctions de localisation et de publication des ressources. Les Noeuds en bleu foncé sont des super-noeuds. L'évolution des architectures logicielles P2P tendent vers cette vision, des noeuds ont des capacités identiques mais peuvent avoir des comportements différents, c'est le cas des super-noeuds. Les noeuds sont identiques mais ils sont configurés différemment (chaque utilisateur configure son client P2P comme il le souhaite..)

!!! exp
    [Kazaa](https://fr.wikipedia.org/wiki/Kazaa)

### Résumé: modèle P2P moderne

<center>

![P2P moderne](./img/peer2peer.png){width=70%}

<figcaption>

Le Modèle P2P - Peer2Peer

</figcaption>

</center>

Pour résumer, dans le modèle **P2P - Peer2Peer** moderne, les utilisateurs forment un groupe informel au sein duquel chacun peut communiquer avec tous les autres.

* Chaque utilisateur/pair est considéré comme un **noeud** du réseau
* Il n'y a (de nos jours) **pas de base de données centralisée**
* Il existe (usuellement) des **super-noeuds** pour les architectures *en essaim*, qu idonne un sens à la notion de **voisinnage**
* C'est le *protocole P2P* qui a la charge de trouver les utilisateurs qui possèdent les fichiers que d'autres utilisateurs souhaitent récupérer.
* Chaque pair maintient sa base de données localement à laquelle ont accès les autres pairs, et fournit une liste des autres pairs du **voisinnage** (par ex. pour un même contenu)
* chaque pair peut visiter la base de données d'un autre pair pour voir ce qu'il propose et rechercher de nouveaux noms de pairs et de nouveaux contenus
* Lorsqu'un noeud (pair) souhaite télécharger (en download) un contenu (ce noeud joue le rôle de client):
    * tout autre noeud (pair) faisant partie du voisinnage de ce contenu, joue un rôle de serveur, et envoie un morceau du contenu, appelé un <bred>bloc</bred> au client.
    * les blocs sont numérotés, et peuvent donc être envoyés dans le désordre, pour être ensuite reconstruit par le client
    * Le débit maximal d'envoi de chaque noeud serveur est configurable localement (par chaque pair serveur)
    * même en cas de faible débit d'envoi par chaque noeud serveur, s'il existe beaucoup de serveurs, alors le client récupère la somme de tous les petits débits des serveurs, qui peuvent donc donner un gros débit en réception par le client
* la communication est chiffrée
* par contre, les adresses IP des noeuds sont publiques, donc de nombreux utilisateurs les utilisent avec un VPN en complément
* En pratique, les réseaux P2P sont utilisés pour le partage de contenus très volumineux : de la musique, des vidéos, de gros logiciels, etc..

### Protocoles P2P

Il existe plusieurs protocoles P2P [^3] $^{,}$ [^4]:

BitTorrent, Gnutella, FastTrack, Freenet, ANts_P2P, OpenNAP, Kademlia, MP2P, JXTA, GNUnet, CAN, Chord, Tapestry, Pastry/FreePastry, Symphony, bwa, DCC (IRC), WinMX, ...

Ils admettent des ressemblances, certains sont des évolutions d'autres, ainsi que des différences...


### Repères Historiques & Influences

Une histoire du P2P est plus largement une histoire du partage de fichiers. Les premiers Systèmes P2P apparaissent à la fin des années $1970$[^5] avec [Usenet](https://fr.wikipedia.org/wiki/Usenet) ($1979$, abbréviation de <b>U</b>NIX U<b>se</b>r <b>Net</b>work), qui est un (système en) réseau de <bred>groupes de discussions</bred> ou <bred>Forums</bred> :fr: :gb:, ou <bred>groupes de nouvelles</bred> :fr: / <bred>newsgroups</bred> :gb:, selon un principe d'abonnement, via une série de protocoles dont NNTP (Network News Transfer Protocol). Par exemple, les *Google Groups* ($\ge 2001$) sont une interface web (*web2news*) permettant d'accéder directement aux services de messages *Usenet*, dont les archives sont conservées depuis $1981$.

!!! exp ":fa-university: Histoire de Systèmes P2P - Peer2Peer"
    * [Napster](https://fr.wikipedia.org/wiki/Napster) (de $1999$ à $2000$) était un pionnier des services de partage de fichier en P2P autorisant le partage de fichiers musicaux (en MP3 à l'époque). La base de données était encore centralisée.
    Napster fut finalement fermé après ce qui est considéré comme **un des cas les plus importants de l'Histoire**, en ce qui concerne le **non-respect des droits d'auteur**. Il a ouvert la voie à de nombreux autres programmes P2P: *KaZaa* (décentralisé), Gnutella, Freenet, Shareaza ($2004$-$2017$), Limewire, eDonkey, etc..
    * [Gnutella](https://fr.wikipedia.org/wiki/Gnutella) ($\ge 2000$) (Architecture Query Flooding) premier réseau décentralisé P2P de ce type. Le plus populaire des système en $2007$. Gnutella est aussi le nom du protocole de recherche et partage.
    * [BitTorrent](https://fr.wikipedia.org/wiki/BitTorrent) (son protocole *BitTorrent* est créé en $2001$, codé en $2002$): Plus complexe que $\mu$torrent. A facilité le téléchargement de distributions GNU/Linux très complètes au format DVD, plutôt que simples CD.
    * [Xunlei](https://fr.wikipedia.org/wiki/Xunlei) ou **Thunder** ($\ge 2003$): le logiciel client de protocole *BitTorrent* le plus utilisé au monde en $2010$.
    * [Vuze](https://www.vuze.com/) (protocole BitTorrent)
    * le courrier électronique est intrinsèquement P2P

### Légalité du P2P

L'utilisation des réseaux P2P est totalement légale.

!!! exp "d'Utilisation légale"
    * échange de fichiers musicaux ou vidéos appartenant au domaine public, en particulier (déjà Napster à l'époque) des copies de chansons difficiles à obtenir autrement, par exemple: 
        * des chansons anciennes, 
        * des enregistrements non publiés ou 
        * des enregistrements amateurs faits lors de concerts
    * échange de photos familiales volumineuses
    * échange de logiciels gratuits volumineux

### Avantages & Inconvénients

#### Avantages

Les communications sont directes

* Décentralisation
* Passage à l'échelle
* Connectivité intermittente
* La réplication, redondance des données
* Un noeud peut accéder directement à un ou plusieurs noeuds.
* Si une machine tombe en panne, cela ne remet pas en cause l'ensemble du système.
* Le réseau est faiblement couplé
* Possibilité de créer des groupes

#### Inconvénients

* Pas de QoS
* problèmes de sécurité
* Les temps de localisation sont plus longs
* Non Déterministe

### F2F - Friend to Friend

Les Réseaux <bred>F2F - Friend-to-Friend</bred> :gb: ou <bred>ami-à-ami</bred> :fr: sont un type particulier de **réseau P2P** dans lequel les utilisateurs n'établissent de connexions directe avec d'autres utilisateurs uniquement s'ils les connaissent. Cela peut être implémenté:

* soit via un serveur central
* soit en utilisant des mots de passe et clés cryptographiques pour l'authentification entre amis, et créer ainsi un réseau décentralié 

## Les Darknets

### Définition

Un <bred>Darknet</bred> :fr: :gb: est un <bred>réseau superposé</bred> ou <bred>réseau overlay</bred> :fr: :gb:, une sorte de réseau (superposé) à l'intérieur d'un réseau, qui utilise des protocoles spécifiques intégrant des fonctions d'anonymat. Il en existe plusieurs, chacun ayant ses propres spécificités, fonctionnalités techniques, principes et objectifs. En particulier chaque Darknet à sa propre manière de s'y connecter: logiciel client, protocole, ports, etc.. Il faut donc bien parler **DES/LES Darknets**, et non **pas DU/LE Darknet comme certains s'autorisent quelquefois, mais À TORD**. Certains darknets se limitent à l'échange de fichiers, d'autres permettent la construction d'un écosystème anonyme complet (web, blog, mail, irc) comme Freenet.

### Repères Historiques

Inventés à l'origine durant les années $1970$ pour désigner les réseaux qui étaient isolés d'**ARPANET** (l'ancêtre d'Internet) pour des raisons de sécurité, les **darknets** étaient capables de recevoir des données de la part d'**ARPANET** mais avaient des adresses qui n'apparaissaient pas dans les listes de réseaux et ne répondaient pas aux **ping** et autres requêtes. Un article de **Microsoft** de $2002$ officialise leur nom et les fait mieux connaître du grand public, en les citant comme obstacle principal au développement de technologies **DRM - Digital Right Management - Gestion des Droits Numériques**.

### Pourquoi utiliser un Darknet? Quels usages ?

Commençons par les mauvaises utilisations. **Les Darknets**, sont surtout connus pour leurs applications illégales. On y trouve en effet les célèbres supermarchés de la drogue comme le défunt **Silk road**, des **vendeurs d'armes** ou encore des offres de services de **tueurs à gages** ou de **pirates informatiques**. Son utilisation supposée par certains groupes terroristes en a fait un bouc émissaire idéal des services de renseignements. Ces usages restent toutefois minoritaires au sein du darknet.

En ce qui concerne de meilleures causes: Les principaux promoteurs des *Darknets* sont les grandes organisations de journalistes comme **Reporters sans frontières** qui propose un *« kit de survie numérique »* pour protéger aussi bien les **reporters de guerre** que les **journalistes d'investigation**.

Les *Darknets* sont aussi utilisés par les **lanceurs d'alerte**, les **dissidents**, ceux qui veulent **se protéger de la surveillance de masse**, mais également par ceux qui ont des comportements jugés « déviants » dans certains pays comme les **communautés LGBTQI+**.

### Darknet(s) vs Darkweb vs Deepweb

!!! def "Darkweb"
    Le <bred>Darkweb</bred> ou <bred>sites profonds</bred> est la réunion de tous les sites web se trouvant sur les **Darknets**.  
    Le darkweb est donc une (sous-) partie des darknets.

!!! def "Deepweb"
    Le <bred>Deepweb</bred> :gb: :fr: ou <bred>web profond</bred> :fr: est la partie cachée du web, càd la réunion de toutes les pages non indexées par les moteurs de recherche. Ce sont souvent des pages privées sur lesquelles on ne peut se connecter qu’en s’identifiant, comme la page d’identification d’une banque. Ces pages sont cachées non pas pour des raisons d’illégalité ou d’illégitimité, mais plutôt pour préserver leur sécurité et leur caractère privé.

### Exemples de Darknets

* Le réseau **TOR - The Onion Router** est une forme de **mixnet** (réseau de routage favorisant l'anonymat par l'usage de serveurs intermédiaires) développée au début des années $2000$ par l'armée américaine. Le réseau rassemble plus de 2 millions d'utilisateurs chaque jour. *TOR* permet de surfer anonymement sur le web ouvert et intègre un darkweb très actif. C'est là que se trouvent notamment les principaux marchés noirs, mais on y trouve aussi des sites d'expression politique, des ressources techniques, etc.
* Le réseau **Freenet** propose un écosystème anonyme complet (mails, blogs, messagerie, IRC, web) et intègre un mode <bred>F2F - Friend-to-Friend</bred> ou <bred>ami-à-ami</bred>. Dès l'origine, *Freenet* a été orienté vers la politique et on y trouve de nombreuses ressources associées.
* Le réseau **I2P - Invisible Internet Project** se comporte comme un proxy. Il intègre un **darkweb** (les DeepSites) et permet l'échange de fichiers, l'édition de blogs et une messagerie anonyme. Il permet également de surfer anonymement sur le web ouvert.
* Le réseau **GNUnet** est le système d'anonymisation proposé par le **projet GNU**. Il est essentiellement utilisé pour le partage de fichiers.
* Le réseau **Zeronet** propose de créer un web ouvert et anonyme à partir d'une technologie inspirée des **Bitcoins**.
* Le réseau **RetroShare** fonctionne d'origine en mode **F2F - Friend2Friend** ou **ami-à-ami**, toutefois il est capable de fonctionner en mode dit Darknet « définition populaire » (c'est-à-dire entre anonymes, à savoir les amis des amis) si l'on y désactive la DHT et le « Mode découverte ».
* Le réseau **SafetyGate** Invisible est une solution professionnelle commerciale.

## Le réseau TOR

[<bred>TOR</bred>](https://www.torproject.org/download/) ($1990's$) [^1] <bred>- The Onion Router</bred> :gb: ou <bred>Le  Routage en Oignons</bred> [^2], à cause de ses plusieurs couches, est **un réseau décentralisé de tunnels virtuels sécurisés (réseau décentralisé de VPN)**. Le réseau TOR est un exemple de **Darknet**, càd <bred>réseau superposé</bred> :fr: ou <bred>réseau overlay</bred> :gb:. Le réseau TOR est un réseau mondial, décentralisé, qui vous permet d’améliorer la confidentialité, la protection de vos données personnelles et votre sécurité sur Internet. Tor fonctionne en acheminant votre trafic par trois serveurs aléatoires (aussi appelés <bred>relais</bred>) dans le réseau Tor. Le dernier relais du circuit (le « relais de sortie ») envoie ensuite le trafic vers l’Internet public.

Pour pouvoir accéder au réseau TOR, un utilisateur peu technophile doit (/peut) installer le <bred>navigateur TOR</bred> ($2008$) (logiciel client) sur votre ordinateur personnel (historiquement, chose que l'on peut toujours faire, on se connectait au réseau TOR par des moyens plus techniques: au travers d'un *proxy* ou *serveur mandataire*)

### Pourquoi utiliser TOR?

Le Navigateur Tor utilise le réseau Tor **pour protéger votre vie privée, vos données personnelles et votre anonymat**. L’utilisation du réseau Tor présente deux atouts principaux :

* Votre fournisseur d’accès à Internet ni quiconque surveille votre connexion localement ne pourra pas suivre à la trace votre activité sur Internet, y compris les noms et adresses des sites Web que vous visitez.
* Les opérateurs des sites Web et des services que vous utilisez, et quiconque les surveille, verront une connexion en provenance du réseau Tor au lieu de votre adresse (IP) réelle, et ne sauront pas qui vous êtes, sauf si vous vous identifiez explicitement.

!!! exp "d'Utilisations de TOR"
    * En $2010$, les **Printemps arabes** ont bénéficié du navigateur TOR
    * En $2013$, les révélations d'Edward Snowden contre la surveillance de masse deviennent une préocupation populaire. Non seulement **TOR a contribué aux dénonciations faites par Snowden**, mais le contenu des documents confirma que TOR était à l'époque à l'épreuve de tout.
    * Pour **accéder au Dark Web**, sans aucun besoin d'utiliser un *VPN*, ni un *Proxy*

### Principe de Fonctionnement


![TOR](./img/tor.png){.center .box}

<center>

<figcaption>

Le Réseau TOR, <a href="(https://tb-manual.torproject.org/fr/about/">source</a>

</figcaption>

</center>

Dans l’image ci-dessus, un utilisateur navigue vers différents sites Web avec Tor. Les ordinateurs verts situés au milieu représentent les relais du réseau Tor, alors que les trois clés représentent les couches de chiffrement entre l’utilisateur et chaque relais.

## Éléments Réseaux

Un <bred>réseau</bred> :fr: ou <bred>network</bred> :gb:, est un ensemble d'<bred>éléments</bred> **matériels** et **logiciels** permettant le **partage d'informations et de ressources**, via des **canaux de communications**.

Il est classiquement composé de:

* d'éléments matériels, appelés <bred>matériels réseau</bred> reliés entre eux:
    * des machines, appelées <bred>hôtes</bred> :fr: ou <bred>hosts</bred> :gb:
    * contrôleur Ethernet
    * <bred>Hubs</bred> :gb: ou <bred>Concentrateurs</bred> :fr: : un matériel réseau qui renvoie toutes les informations (trames) vers tous les péripériques connectés (sur le hub)
        * **sans filtre** : Toutes les informations renvoyées sur tous les ports de sortie quels qu'ils soient
        * **sans mémoire** : le hub ne stocke pas les adresses IP des matériels déjà connectés (au moins une fois)
    * <bred>Switchs</bred> :gb: ou <bred>Commutateurs</bred> :fr: : un matériel réseau analyse toutes les informations (trames) reçues :
        * **avec filtre** : les données sont filtrées afin de les aiguiller uniquement sur les bons ports de sortie
        * **avec mémoire** : les adreses IP des matériels déjà connectés (au moins une fois) sont stockées dans une table
    On parle de <bred>commutation</bred> des trames ou de <bred>réseaux commutés</bred>.
    * <bred>Routeurs</bred> :fr: ou <bred>Routers</bred> :gb: : un matériel réseau qui délimite le réseau, et permet de sortir du réseau pour communiquer avec des dispositifs extérieurs au réseau.
* d'éléments logiciels:
    * drivers :gb: ou pilotes :fr: des interfaces
    * *firmwares* :gb: ou *microgiciels* / *micrologiciels* / *microprogrammes* :fr: des équipements
    * etc..

## Notes et Références

[^1]: [Le Projet TOR, à propos](https://tb-manual.torproject.org/fr/about/)
[^2]: [Histoire du Projet TOR](https://www.torproject.org/fr/about/history/)
[^3]: [Les Protocoles Peer-to-Peer, Telecom-Lille](https://wapiti.telecom-lille.fr/commun/ens/peda/options/ST/RIO/pub/exposes/exposesrio2005ttnfa2006/miclard-hameau/) (vous pouvez sereinement accepter l'exception de sécurité, ce qui signifie seulement dans ce cas, que l'École Télécom Lille développe ses propres certificats SSL)
[^4]: [Les Protocoles Peer-to-Peer, hal.inria.fr, 2003](https://hal.inria.fr/inria-00099498/document) pour Napster et Gnutella
[^5]: [Usenet, wikipedia](https://fr.wikipedia.org/wiki/Usenet)

