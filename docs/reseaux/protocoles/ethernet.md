# 1NSI : Le Protocole Ethernet

## Histoire

C'est au départ une technologie de réseau local permettant que toutes les machines d'un réseau soient connectées à une même ligne de communication, formée de câbles cylindriques (câble coaxial, paires torsadées). Le standard qui a été le plus utilisé dans les années 1990 et qui l'est toujours est le 802.3 de l'IEEE (maintenant aussi adopté comme norme internationale ISO/CIE 8802-3). Ce dernier a largement remplacé d'autres standards comme le Token Ring et l'ARCNET.

Le nom Ethernet vient de l'éther, milieu mythique dans lequel baigne l'Univers, et net, abréviation de réseau en anglais. Le réseau ancêtre ALOHAnet utilisait les ondes radiofréquences se propageant dans l'éther.

## RFCs



## Références & Notes

### références



### Notes

[^1]: [Entête Ethernet](https://www.frameip.com/entete-ethernet/?video=79#video-79)
[^2]: [Techno Science, Ethernet](https://www.techno-science.net/definition/1363.html)