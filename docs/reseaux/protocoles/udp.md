# 1NSI : Réseaux : Le Protocole UDP

D'autres applications comme les requêtes aux annuaires éléctroniques (pour obtenir la correspondance entre un nom d'ordinateur et son adresse), ou les applications de gestion de réseau préfèrent utiliser un protocole plus léger mais plus rapide car les messages sont typiquement très courts et en cas d'erreurs ou d'absence de réponse, ils peuvent être répétés sans problèmes. Le **protocole UDP** est typiquement utilisé dans ces cas.

## Introduction

UDP, comme TCP, s’exécute au-dessus de IP. TCP se fonde sur les services fournis par UDP.

* <bred>TCP (Transport Control Protocol)</bred> assure un service de transmission de données **fiable** avec une **détection et une correction d’erreurs** de bout en bout.
* <bred>UDP (User Datagram Protocol)</bred> offre un service de transmission de datagrammes **sans connection**.

Avec TCP ou UDP, il est possible de remettre des données à des processus d’application s’exécutant sur une machine distante. 
Ces processus d’application sont identifiés par des <bred>numéros de port</bred>. 

!!! def "Socket"
    Une <bred>Socket</bred> (historiquement développé dans Unix BSD) est un point de communication par lequel un processus peut émettre et recevoir des informations. 
    Une Socket est la combinaison d’une **adresse IP** et d’un **numéro de port**. 

!!! pte "Lien entre Sockets et connexion TCP ou échange UDP"
    La combinaison de 2 sockets définit complètement :

    * une connexion TCP ou 
    * un échange UDP.

La RFC 1060 indique les ports prédéfinis pour les services :

* 20 : FTP
* 23 : Telnet
* 25 : SMTP
* 53 : DNS
* 69 : TFTP
* 80 : HTTP
* ..

## Le Protocole UDP

Le protocole UDP permet aux applications d’accéder directement à un service de transmission de datagrammes, tel que le service de transmission qu’offre IP.

Caractéristiques de UDP :

* UDP possède un mécanisme permettant d’identifier les processus d’application à l’aide de **numéros de port UDP**.
* UDP est orienté datagrammes (sans connexion), ce qui évite les problèmes liés à l’ouverture, au maintien et à la fermeture des connexions.
* UDP est efficace pour les applications en diffusion/multidiffusion. Les applications satisfaisant à un modèle du type « interrogation-réponse » peuvent également utiliser UDP. La réponse peut être utilisée comme étant un accusé de réception positif à l’interrogation. Si une réponse n’est pas reçue dans un certain intervalle de temps, l’application envoie simplement une autre interrogation.
* UDP ne séquence pas les données. La remise conforme des données n’est pas garantie.
* UDP peut éventuellement vérifier l’intégrité des données (et des données seulement) avec une Somme de Contrôle / Checksum.
* UDP est plus rapide, plus simple et plus efficace que TCP mais il est moins robuste.

Le protocole UDP permet une transmission sans connexion, mais aussi sans sécurité. Pourtant de nombreuses applications reposent sur UDP :

- TFTP
- DNS
- NFS
- SNMP
- RIP (Protocole de Routage)

L’en-tête a une taille fixe de $8$ octets.

```console linenums="0"
+-----------------+-------------+
|   En-Tête UDP   |   Données   |
+-----------------+-------------+
```

où l'entête est composée de :

```console linenums="0"
<------------------------------------- 4 octets = 32 bits -------------------------------------->
+-----------------------------------------------+-----------------------------------------------+
|                 Port Source                   |           Port Destination                    |
|__╷__╷__╷__|__╷__╷__╷__|__╷__╷__╷__╷__╷__╷__╷__|__╷__╷__╷__╷__╷__╷__╷__|__╷__╷__╷__╷__╷__╷__╷__|
|              Longueur / Length                |      Somme de Contrôle / Checksum             |
|__╷__╷__╷__|__╷__╷__╷__|__╷__╷__╷__╷__╷__╷__╷__|__╷__╷__╷__╷__╷__╷__╷__|__╷__╷__╷__╷__╷__╷__╷__|
|                                          Data / Données                                       |
|                                              ...                                              |
|__╷__╷__╷__|__╷__╷__╷__|__╷__╷__╷__╷__╷__╷__╷__|__╷__╷__╷__╷__╷__╷__╷__|__╷__╷__╷__╷__╷__╷__╷__|
```

où :

* Le champ **Port Source** Port occupe $16$ bits. Il indique :
    * le **numéro de port** du processus émetteur (source),
    * le **numéro de port** où on peut adresser les réponses lorsque l’on ne dispose d’aucun autre renseignement.
    * si sa valeur est $0$, cela signifie qu’aucun numéro de port n’est attribué.
* Le champ **Port Destination** identifie le numéro de port du processus destinataire, correspondant à l’adresse IP de destination auquel on envoie les données UDP. UDP effectue le démultiplexage des données à l’aide de numéros de port. Lorsque UDP reçoit un datagramme sans numéro de port, il génère un message d’erreur ICMP indiquant qu’il est impossible de contacter le port et il rejette le datagramme.
* Le champ **Longueur / Length** contient la longueur du paquet UDP en octets (en-tête + données). La valeur minimale est $8$ octets, et correspond à un paquet où le champ de données est vide.

Le pseudo en-tête de préfixe de l’en-tête UDP contient l’adresse d’origine, l’adresse de destination, le protocole (UDP = 17) et la longueur UDP. Ces informations sont destinées à prévenir les erreurs de routage.

## Références et Notes

* [Les protocoles UDP et TCP, GIPSA LAB, GRENOBLE, INP (pdf)](http://www.gipsa-lab.grenoble-inp.fr/~christian.bulfone/MIASHS-L3/PDF/3-Les_protocoles_UDP_TCP.pdf)