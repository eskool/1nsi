# 1NSI: Le Protocle du Bit Alterné

## Principe

Ce protocole est un exemple simple de fiabilisation du transfert de données.

<center>

<iframe width="560" height="315" src="https://www.youtube.com/embed/OCyNI27hUEI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

</enter>

## Non réception du message

## Chevauchement des messages

## Chevauchement d'acquittements

## Références et Notes

### Références

* [Protocole du Bit Alterné](http://nsinfo.yo.fr/nsi_prem_bit_alt.html)
* 

#### Notes


