# 1NSI : Réseaux, Le Protocole IP

Le protocole IP est un protocole réseau/internet, de niveau $3$ du modèle OSI.
Il encapsule les protocoles des couche de Transport supérieures (niveau 4). 

## Encapsulation du protocole IP parmi les Protocoles Supérieurs

Le diagramme suivant montre la place du protocole IP dans la hiérarchie des protocoles:

```console linenums="0"
+------+ +-----+ +-----+     +-----+
| HTTP | | FTP | | TFTP| ... | ... |       Niveau 5
+------+ +-----+ +-----+     +-----+
    |    |          |           |
    +-----+     +-----+     +-----+
    | TCP |     | UDP | ... | ... |        Niveau 4
    +-----+     +-----+     +-----+
        |           |           |
    +--------------------------+----+
    |    Internet Protocol & ICMP   |      Niveau 3
    +--------------------------+----+
                    |
    +---------------------------+
    | Protocole de Réseau Local |          Niveau 2
    |      (e.g. Ethernet)      |
    +---------------------------+
```

## Format Général d'un Paquet / Datagramme IP

Dans le cas du protocole IP, on parle tout aussi bien de **Paquet** que de **Datagramme**. En théorie, un datagramme véhicule toutes les informations nécessaires à son routage (notamment les adresses sources et destinations), alors qu’un paquet est beaucoup plus générique et pourrait fort bien ne pas comporter ces informations. 

Le paquet IP est formé de deux grandes parties :

* l’**entête du paquet**, d’une taille minimale de $20$ octets $5\times 4$ octets, avec facultativement quelques options, constitue le PCI du protocole. C’est là que sont inscrites toutes les informations du protocole (adresse, segmentation, options, etc.).
* la partie **data**, ou **champ de données**, d’une taille maximum de : $(65536$ octets $) – ($ les octets d’entête et d’options$)$. Elle véhicule la PDU de couche supérieure (généralement un segment TCP ou un Datagramme UDP).

Un paquet IP a une taille maximale de $2^{16} = 65536$ octest. En pratique, néanmoins, sa taille moyenne est comprise entre $128$ et $256$ octets. 

## Format de l'Entête du Paquet

L'entête IP se décompose de la manière suivante :

<center>

```console linenums="0"
<------------------------------ Mots de 4 octets = 32 bits ------------------------------------->
+-----------+-----------+-----------------------+-----------------------------------------------+
|  Version  |    IHL    | TOS - Type Of Service |         Longeur Totale (du paquet IP)         |
|__╷__╷__╷__|__╷__╷__╷__|__╷__╷__╷__╷__╷__╷__╷__|__╷__╷__╷__╷__╷__╷__╷__|__╷__╷__╷__╷__╷__╷__╷__|
|                 Identificateur                |  Flags |       Décalage du Fragment           |
|__╷__╷__╷__|__╷__╷__╷__|__╷__╷__╷__╷__╷__╷__╷__|__╷__╷__╷__╷__╷__╷__╷__|__╷__╷__╷__╷__╷__╷__╷__|
|          TTL          | (Numéro de) Protocole | Somme de Contrôle (Entête) = (Header) Checksum|
|__╷__╷__╷__|__╷__╷__╷__|__╷__╷__╷__╷__╷__╷__╷__|__╷__╷__╷__╷__╷__╷__╷__|__╷__╷__╷__╷__╷__╷__╷__|
|                                           IP Source                                           |
|__╷__╷__╷__|__╷__╷__╷__|__╷__╷__╷__╷__╷__╷__╷__|__╷__╷__╷__╷__╷__╷__╷__|__╷__╷__╷__╷__╷__╷__╷__|
|                                       IP Destinataire                                         |
|__╷__╷__╷__|__╷__╷__╷__|__╷__╷__╷__╷__╷__╷__╷__|__╷__╷__╷__╷__╷__╷__╷__|__╷__╷__╷__╷__╷__╷__╷__|
|                                       Options éventuelles                                     |
|__╷__╷__╷__|__╷__╷__╷__|__╷__╷__╷__╷__╷__╷__╷__|__╷__╷__╷__╷__╷__╷__╷__|__╷__╷__╷__╷__╷__╷__╷__|
|                                  ... (données / data) ...                                     |
```

</center>

où la signification de chaque champ est la suivante :

* **Version (4 bits)** : Il s'agit de la version du protocole IP que l'on utilise (actuellement on utilise la version 4 -IPv4, donc $0100$) afin de vérifier la validité du datagramme. Elle est codée sur $4$ bits.
* **IHL = Internet Header Length (4 bits) = Longueur de l'en-tête** : Contient le nombre de mots/lignes de $32$ bits constituant l'en-tête. Ce champ est codé sur $4$ bits: ce qui prouve que l'entête peut contenir :
    * au minimum (par défaut) : $5$ mots/lignes de $32$ bits (lorsqu'aucune option n'est passée). Dans ce cas la longueur de l'entête IP vaut $5 \times 4$ octets $=20$ octets (valeur usuelle et par défaut)
    * et, au maximum : $2^4-1=15$ mots/lignes de $32$ bits.
* **TOS = Type Of Service = Type de service = Differentiated Services (8 bits)** : Ce champ, rarement utilisé, donne des indications aux équipements qu’il traverse sur son niveau de priorité et sa classe de service. Défini sur 8 bits (1 octet), il est divisé en 5 parties :
    * Les bits $0$ à $2$ indiquent la *précédence*, càd la priorité d’acheminement par rapport aux autres paquets. Cette partie du champ permet donc de définir $2^3 = 8$ niveaux de priorité d’acheminement.
    * Le bit $3$ indique si le paquet peut supporter une attente de traitement normale ou faible.
    * Le bit $4$ indique si le paquet doit être acheminé en priorité sur des liens à hauts débits ou non
    * Le bit $5$ indique si le paquet doit être acheminé sur des liens très fiables (faible taux d’indisponibilité et d’erreurs) ou normaux
    * Les bits $6$ et $7$ sont réservés et donc inutilisés.
* **Longueur totale (16 bits)** : Il indique la taille/longueur totale du datagramme, **en octets**. La taille de ce champ étant de $2$ octets $=16$ bits, la taille totale du datagramme ne peut dépasser $2^{16} = 65536$ octets. Utilisé conjointement avec la taille de l'en-tête, ce champ permet de déterminer où sont situées les données.
* **Identificateur ($16$ bits)**, Drapeaux /**Flags ($3$ bits)** et **Décalage du Fragment ($13$ bits)** sont $3$ champs qui permettent de gérer la fragmentation (/segmentation) des datagrammes/paquets IP. En effet, il est possible qu’un paquet IP n’entre pas entièrement dans une trame de niveau $2$ parce que sa taille est trop importante. Il sera nécessaire de fragmenter le paquet pour le véhiculer dans plusieurs trames. **IP est un protocole non orienté connexion**, donc la fragmentation est assez complexe à gérer. Retenons simplement qu’un mot complet de $4$ octets est réservé à la gestion de la fragmentation.
* **TTL - Time To Live (8 bits)** = Durée de vie appelée : ce champ indique le nombre maximal de routeurs à travers lesquels le datagramme peut passer. Ainsi ce champ est décrémenté à chaque passage dans un routeur, lorsque celui-ci atteint la valeur critique de $0$, le routeur détruit le datagramme. Cela évite l'encombrement du réseau par les datagrammes perdus (en particulier les boucles)
* **(Numéro de) Protocole (8 bits)** : ce champ contient un nombre entier identifiant de manière unique le protocole du datagramme. Ce nom est codé sur $8$ bits, donc on peut identifier $2^8=256$ protocoles distincts[^1] :

<center>

| Hexadécimal | Décimal | Protocole | Nom |
|:-:|:-:|:-:|:-:|
| $01$ | $1$ | ICMP | Internet Control Message Protocol |
| $02$ | $2$ | IGMP | ---- |
| $06$ | $6$ | TCP | Transmission Control Protocol |
| $08$ | $8$ | EGP | Exterior Gateway Protocol<br/>(Protocole de Routage) |
| $09$ | $9$ | IGP | Interior Gateway Protocol<br/>(Protocole de Routage) |
| $11$ | $17$ | UDP | User Datagram Protocol<br/>(Le protocole de routage RIP<br/>est identifié comme UDP :<br/> Voir [capture RIPv2](https://www.cloudshark.org/captures/e4c0d3630ab1) ) |
| $2F$ | $47$ | GRE | Generic Routing Encapsulation<br/>(Protocole de Routage Générique) |
| $58$ | $88$ | EIGRP | Enhanced Interior <br/>Gateway Routing Protocol<br/>(Protocole de Routage) |
| $59$ | $89$ | OSPF | Open Shortest Path First<br/>(Protocole de Routage)<br/>Voir [capture OSPF](https://www.cloudshark.org/captures/dd99281b2e68) |
| $7C$ | $124$ | IS-IS | intermediate System<br/>to Intermediate System<br/>(Protocole de Routage) |

</center>

* **(Header) Checksum / Somme de Contrôle de l'En-tête ($16$ bits)** : ce champ contient une valeur qui permet de contrôler l'**intégrité** de l'en-tête afin de déterminer si celui-ci n'a pas été altéré pendant la transmission, ou pas.
    * Méthode de Calcul : La somme de contrôle est le complément à un de tous les mots de 16 bits de l'en-tête (champ somme de contrôle exclu). Celle-ci est en fait telle que lorsque l'on fait la somme des champs de l'en-tête (somme de contrôle incluse), on obtient un nombre avec tous les bits positionnés à $1$. 
    * Détection , mais pas Correction : Attention, IP possède un mécanisme de détection d’erreurs, mais pas de correction d’erreurs. Autrement dit, il jette le paquet, mais n’informe personne et ne cherche pas à générer une répétition du paquet (réémission par l’émetteur). Les couches supérieures devront gérer elles-mêmes cette perte de paquet est s’occuper des demandes de réémissions éventuelles. 
    * re-calcul sur chaque équipement : comme certains champs de l’entête peuvent varier dans le temps, le checksum est recalculé par chaque équipement traversé par le paquet.
* **Adresse IP Source (32 bits)** : Ce champ représente l'adresse IP de la machine émettrice, il permet au destinataire de répondre
* **Adresse IP de Destination (32 bits)** : adresse IP du destinataire du message

## Fragmentation des paquets IP & MTU

### Fragmentation / Défragmentation

En théorie, nous avons dit que la taille maximale d'un datagramme IP est de $2^{16} = 65536$ octets. Toutefois cette valeur maximale théorique n'est jamais utilisée (sauf pour l'interface loopback) car les réseaux n'ont pas une capacité suffisante pour envoyer de si gros paquets. De plus, les réseaux sur Internet utilisent différents protocoles, si bien que la taille maximale d'un datagramme peut varier suivant le type de réseau, donc de protocole.

### MTU - Maximum Transfer Unit

!!! def "MTU - Maximum Transfer Unit"
    Le <red>MTU - Maximum Transfer Unit</red> est la taille maximale d'un paquet IP (en-têtes IP et TCP inclues) pouvant être transmis en une seule fois (sans fragmentation) sur une interface (càd sur le prochain tronçon/réseau).  
    Au delà de cette valeur, le paquet/datagramme IP sera forcément fragmenté en plusieurs trames.

!!! pte "Valeurs MTU & MTU par défaut pour Ethernet"
    Par défaut: **MTU = 1500 octets** dans les réseaux IP pour le protocole Ethernet.  
    Le MTU de l'interface de loopback vaut usuellement $65536$ octets (le maximum possible)  
    Néanmoins, le MTU dépend directement du type de réseau, et du protocole associé. Il peut varier sur le chemin menant à l'hôte destinataire.

!!! exp "Comment Connaître le MTU sur Linux"
    La commande `ip a` (ou `ip address`) permet de connaître la valeur du MTU sur Linux :  
    ```console linenums="0"
    $ ip a
    1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
    2: enp5s0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc mq state DOWN group default qlen 1000
        link/ether a5:4d:88:c6:f0:42 brd ff:ff:ff:ff:ff:ff
    3: wlo1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default qlen 1000
        link/ether 63:87:2e:47:24:6b brd ff:ff:ff:ff:ff:ff
        altname wlp0s40g5
        inet 192.168.1.21/24 brd 192.168.21.255 scope global dynamic noprefixroute wlo1
           valid_lft 81040sec preferred_lft 81040sec
        inet6 fe80::664:7ab:1be0:8e13/64 scope link noprefixroute 
           valid_lft forever preferred_lft forever
    4: vmnet1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UNKNOWN group default qlen 1000
        link/ether 00:40:63:d0:00:01 brd ff:ff:ff:ff:ff:ff
        inet 172.16.143.1/24 brd 172.16.143.255 scope global vmnet1
           valid_lft forever preferred_lft forever
        inet6 fe80::340:42ee:abd0:1/64 scope link 
           valid_lft forever preferred_lft forever
    5: vmnet8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UNKNOWN group default qlen 1000
        link/ether 00:40:63:d0:00:10 brd ff:ff:ff:ff:ff:ff
        inet 172.16.125.1/24 brd 172.16.125.255 scope global vmnet8
           valid_lft forever preferred_lft forever
        inet6 fe80::340:42ee:abd0:1/64 scope link 
           valid_lft forever preferred_lft forever
    ```

### Quand les paquets sont-ils fragmentés ?

Lorsque deux appareils informatiques initient une connexion et commencent à échanger des paquets[^2], ces paquets sont acheminés sur plusieurs réseaux. Il est nécessaire de prendre en compte non seulement le MTU des deux appareils à chaque extrémité de chaque communication, mais également l’ensemble des routeurs, commutateurs et serveurs intermédiaires. Les paquets qui excèdent le MTU à tout endroit sur le chemin du réseau sont fragmentés.

Supposons que le serveur A et l'ordinateur A soient connectés, mais que les paquets de données qu'ils s'envoient doivent transiter par les routeurs B et C. Le serveur A, l'ordinateur A et le routeur B ont tous un MTU de 1 500 octets. Cependant, le routeur C a un MTU de 1 400 octets. Si le serveur A et l'ordinateur A ne connaissent pas le MTU du routeur C et envoient des paquets de 1 500 octets, tous leurs paquets de données seront fragmentés par le routeur B en cours de route.

![Diagramme de Fragmentation MTU](../img/diagramme-fragmentation-mtu.png)

La fragmentation ajoute un petit degré de latence et d'inefficacité aux communications réseau, elle devra donc être évitée si possible. Les équipements réseau obsolètes peuvent être vulnérables aux attaques **DoS - Denial of Service (par Déni de Service)** qui exploitent la fragmentation, comme par exemple l'(ancienne) attaque DoS dite du <red>[ping de la mort](https://fr.wikipedia.org/wiki/Ping_de_la_mort)</red> :fr: / <red>[ping of death](https://en.wikipedia.org/wiki/Ping_of_death)</red> :gb:.

## Résumé du Protocole IP

IP est un protocole de niveau $3$ du modèle OSI (couche réseau) :

* non orienté connexion
* peu fiable
* L'entête d'un paquet IP contient :
    * l'adresse IP de l'Hôte Source
    * l'adresse IP de l'Hôte Destinataire

## RFCs

<center>

| RFC | Date | Description |
|:-:|:-:|:-:|
| [RFC 791](https://datatracker.ietf.org/doc/html/rfc791/) :gb:<br/> [RFC 791](http://abcdrfc.free.fr/rfc-vf/pdf/rfc0791.pdf) :fr:| $1981$ | Protocole IP |

</center>

## Références & Notes

### Références

* [Le Protocole IP, gatoux.com](https://racine.gatoux.com/lmdr/index.php/le-datagramme-ip/)
* [Introduction and IPv4 Datagram Header, geeksforgeeks.org](https://www.geeksforgeeks.org/introduction-and-ipv4-datagram-header/?ref=lbp)
* [Le Protocole IP, commentcamarche.net](https://www.commentcamarche.net/contents/530-le-protocole-ip)
* [Le protocole IP, wikipedia](https://fr.wikipedia.org/wiki/Internet_Protocol)


### Notes

[^1]: [Numéros de Protocoles dans le Protocole IP, IANA.org](https://www.iana.org/assignments/protocol-numbers/protocol-numbers.xhtml)
[^2]: [What is MTU?, Cloudflare.com](https://www.cloudflare.com/fr-fr/learning/network-layer/what-is-mtu/)
[^3]: [Introduction au MTU](https://www.commentcamarche.net/faq/7185-introduction-au-mtu)