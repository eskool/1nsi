# 1NSI : Réseaux, Le Protocole TCP<br/>= Transmission Control Protocol

## Caractéristiques Principales du Protocole TCP

TCP (qui signifie Transmission Control Protocol, soit en français: Protocole de Contrôle de Transmission) est un des principaux protocoles de la couche transport du modèle TCP/IP. Il permet, au niveau des applications, de gérer les données en provenance (ou à destination) de la couche inférieure du modèle (c'est-à-dire le protocole IP). Lorsque les données sont fournies au protocole IP, celui-ci les encapsule dans des datagrammes IP, en fixant le champ protocole à 6 (Pour savoir que le protocole en amont est TCP...). TCP est un **protocole orienté connexion**, c'est-à-dire qu'il permet à deux machines qui communiquent de contrôler l'état de la transmission.
Les caractéristiques principales du protocole TCP sont les suivantes :

* TCP permet de remettre en ordre les datagrammes en provenance du protocole IP
* TCP permet de vérifier le flot de données afin d'éviter une saturation du réseau
* TCP permet de formater les données en segments de longueur variable afin de les "remettre" au protocole IP
* TCP permet de multiplexer les données, c'est-à-dire de faire circuler simultanément des informations provenant de sources (applications par exemple) distinctes sur une même ligne
* TCP permet enfin l'initialisation et la fin d'une communication de manière courtoise

## Le but de TCP

Grâce au protocole TCP, les applications peuvent communiquer de façon sûre (grâce au système d'accusés de réception du protocole TCP), indépendamment des couches inférieures. Cela signifie que les routeurs (qui travaillent dans la couche Internet) ont pour seul rôle l'acheminement des données sous forme de datagrammes, sans se préoccuper du contrôle des données, car celui-ci est réalisé par la couche transport (plus particulièrement par le protocole TCP).

Lors d'une communication à travers le protocole TCP, les deux machines doivent établir une connexion. La machine émettrice (celle qui demande la connexion) est appelée client, tandis que la machine réceptrice est appelée serveur. On dit qu'on est alors dans un environnement Client-Serveur.
Les machines dans un tel environnement communiquent en mode connecté, c'est-à-dire que la communication se fait dans les deux sens.

Pour permettre le bon déroulement de la communication et de tous les contrôles qui l'accompagnent, les données sont encapsulées, c'est-à-dire qu'on ajoute aux paquets de données un en-tête qui va permettre de synchroniser les transmissions et d'assurer leur réception.

Une autre particularité de TCP est de pouvoir réguler le débit des données grâce à sa capacité à émettre des messages de taille variable, ces messages sont appelés segments.

## Multiplexage / Démultiplexage

TCP permet d'effectuer une tâche importante: le multiplexage/démultiplexage, c'est-à-dire faire transiter sur une même ligne des données provenant d'applications diverses ou en d'autres mots mettre en série des informations arrivant en parallèle.

![Multiplexage/Démultiplexage](../img/multiplexage.svg){.center style="width:90%;max-width:800px;"}

Ces opérations sont réalisées grâce au concept de ports (ou sockets), c'est-à-dire un numéro associé à un type d'application, qui, combiné à une adresse IP, permet de déterminer de façon unique une application qui tourne sur une machine donnée.

## En-tête d'un Segment TCP

Un segment TCP est constitué :

* d'un en-tête TCP
* la charge utile / payload (données)

```console linenums="0"
<------------------------------ Mots de 4 octets = 32 bits ------------------------------------->
+-----------+-----------+-----------------------+-----------------------------------------------+
|                   Port Source                 |              Port Destination                 |
|__╷__╷__╷__|__╷__╷__╷__|__╷__╷__╷__|__╷__╷__╷__|__╷__╷__╷__|__╷__╷__╷__|__╷__╷__╷__|__╷__╷__╷__|
|                                Numéro d'Ordre / Sequence Number                               |
|__╷__╷__╷__|__╷__╷__╷__|__╷__╷__╷__|__╷__╷__╷__|__╷__╷__╷__|__╷__╷__╷__|__╷__╷__╷__|__╷__╷__╷__|
|                  Numéro d'Accusé de Réception / Ack(nowledgement) Number                      |
|__╷__╷__╷__|__╷__╷__╷__|__╷__╷__╷__|__╷__╷__╷__|__╷__╷__╷__|__╷__╷__╷__|__╷__╷__╷__|__╷__╷__╷__|
| Décalage  |                 |U |A |P |R |S | F|                                               |
|  Données  |     Réservé     |R |C |S |S |Y | I|               Fenêtre / Window                |
|           |                 |G |K |H |T |N | N|                                               |
|__╷__╷__╷__|__╷__╷__╷__|__╷__╷__╷__|__╷__╷__╷__|__╷__╷__╷__|__╷__╷__╷__|__╷__╷__╷__|__╷__╷__╷__|
|        Somme de Contrôle / Checksum           |      Pointeur d'Urgence / Urgent Pointer      |
|__╷__╷__╷__|__╷__╷__╷__|__╷__╷__╷__|__╷__╷__╷__|__╷__╷__╷__|__╷__╷__╷__|__╷__╷__╷__|__╷__╷__╷__|
|        Options           |                     Padding / Remplissage                          |
|__╷__╷__╷__|__╷__╷__╷__|__╷__╷__╷__|__╷__╷__╷__|__╷__╷__╷__|__╷__╷__╷__|__╷__╷__╷__|__╷__╷__╷__|
|                                  Charge utile / Payload (Données)                             |
|                                              ...                                              |
|__╷__╷__╷__|__╷__╷__╷__|__╷__╷__╷__|__╷__╷__╷__|__╷__╷__╷__|__╷__╷__╷__|__╷__╷__╷__|__╷__╷__╷__|
```

où la signification des différents champs est l asuivante :

* **Port Source :fr: / Source Port :gb: (16 bits)** : Port relatif à l'application en cours sur la machine source
* **Port Destination :fr: / Destination Port :gb: (16 bits)** : Port relatif à l'application en cours sur la machine de destination
* **Numéro d'Ordre :fr: / Sequence Numer :gb: (32 bits)** : Lorsque le drapeau SYN est à 0, le numéro d'ordre est celui du premier mot du segment en cours.
Lorsque SYN est à 1, le numéro d'ordre est égal au numéro d'ordre initial utilisé pour synchroniser les numéros de séquence (ISN)
* **Numéro d'Accusé de Réception :fr: / Ack(nowledgment) Number :gb: (32 bits)** : Le numéro d'accusé de réception également appelé numéro d'acquittement correspond au numéro (d'ordre) du prochain segment attendu, et non le numéro du dernier segment reçu.
* **Décalage des Données / Data Offset :gb: (4 bits)** : il permet de repérer le début des données dans le paquet. Le décalage est ici essentiel car le champ d'options est de taille variable
* **Réservé (6 bits)** : Champ inutilisé actuellement mais prévu pour l'avenir
* **Drapeaux :fr: / Flags :gb: (6x1 bit)** : Les drapeaux représentent des informations supplémentaires :
    * **URG**: si ce drapeau est à $1$ le paquet doit être traité de façon urgente : Le récepteur doit donc regarder le champ Pointeur d'Urgence/Urgent Pointer 
    * **ACK**: si ce drapeau est à $1$ le paquet est un accusé de réception : le récepteur doit donc regarder le champ Numéro d'Accusé de Réception/Ack(nowledgment) Number
    * **PSH (PUSH)**: si ce drapeau est à $1$, le paquet fonctionne suivant la méthode PUSH: le récepteur doit transmettre le segemnt TCP en l'état, il ne recevra plus rien après ce (dernier) segment.
    * **RST**: si ce drapeau est à $1$, la connexion est réinitialisée: pour indiquer un refus de connexion avec l'hôte distant, ou mettre fin à une connexion existante
    * **SYN**: Le Flag TCP SYN indique une **demande d'établissement de connexion** (le premier du triplet SYN/SYN ACK/ ACK). Le champ Numéro d'Ordre/Sequence Number contiendra un nombre entier aléatoire initial. Aucun autre flag ne devra être défini.
    * **FIN**: si ce drapeau est à $1$ la connexion s'interrompt. Comme RST, mais en plus poli. En supposant que les Hôtes A et B soient connectés depuis un certains temps : si Hôte A veut mettre fin à une connexion, Hôte A envoie un flag FIN, puis Hôte B confirme avec un Ack. Puis B envoie un FIN à Hôte A, et Hôte A renvoie un Ack. Ceci marque la fin de la connexion.
* **Window :gb: / Fenêtre :fr: (16 bits)** : Champ permettant de connaître le nombre d'octets que l'émetteur souhaite envoyer sans accusé de réception du récepteur (durant la procédure de handshake SYN/SYN-ACK/ACK)
* **Somme de Contrôle** :fr: / **Checksum ou CRC** :gb: : La somme de contrôle est réalisée en faisant la somme des champs de données de l'en-tête, afin de pouvoir vérifier l'intégrité de l'en-tête
* **Pointeur d'Urgence :fr: / Urgent Pointer :gb: (16 bits)** : Indique le numéro d'ordre à partir duquel l'information devient urgente. En effet, certaines applications ont besoin de pouvoir transmettre à TCP que certaines parties des données sont à exécuter d'une certaine manière particulière et prioritaire: ce champ permet de gérer cette fonctionnalité. 
* **Options** (Taille variable): Des options diverses
* **Remplissage** : On remplit l'espace restant après les options avec des zéros pour avoir une longueur multiple de $32$ bits

## Fiabilité des Transferts

Le protocole TCP permet d'assurer le transfert des données de façon fiable, bien qu'il utilise le protocole IP, qui n'intègre aucun contrôle de livraison de datagramme.

En réalité, le protocole TCP possède un système d'accusé de réception permettant au client et au serveur de s'assurer de la bonne réception mutuelle des données.
Lors de l'émission d'un segment, un numéro d'ordre (appelé aussi numéro de séquence) est associé. A réception d'un segment de donnée, la machine réceptrice va retourner un segment de donnée dont le drapeau ACK est à 1 (afin de signaler qu'il s'agit d'un accusé de réception) accompagné d'un numéro d'accusé de réception égal au numéro d'ordre précédent.

![TCP ACK 1](../img/tcp-ack1.gif){.center}

De plus, grâce à une minuterie déclenchée dès réception d'un segment au niveau de la machine émettrice, le segment est réexpédié dès que le temps imparti est écoulé, car dans ce cas la machine émettrice considère que le segment est perdu...

![TCP ACK 2](../img/tcp-ack2.gif){.center}

Toutefois, si le segment n'est pas perdu et qu'il arrive tout de même à destination, la machine réceptrice saura grâce au numéro d'ordre qu'il s'agit d'un doublon et ne conservera que le dernier segment arrivé à destination...

## Établissement d'une Connexion

Etant donné que ce processus de communication, qui se fait grâce à une émission de données et d'un accusé de réception, est basé sur un numéro d'ordre (appelé généralement numéro de séquence), il faut que les machines émettrices et réceptrices (client et serveur) connaîssent le numéro d'ordre initial de l'autre machine.

L'établissement de la connexion entre deux applications se fait souvent selon le schéma suivant :

* Les ports TCP doivent être ouverts
* L'application sur le serveur est passive, c'est-à-dire que l'application est à l'écoute, en attente d'une connexion
* L'application sur le client fait une requête de connexion sur le serveur dont l'application est en ouverture passive. L'application du client est dite "en ouverture active"

Les deux machines doivent donc synchroniser leurs séquences grâce à un mécanisme communément appelé <red>three ways handshake</red> :gb: / **poignée de main en trois temps** :fr:, que l'on retrouve aussi lors de la clôture de session.

Ce dialogue permet d'initier la communication, il se déroule en trois temps, comme sa dénomination l'indique :

* Dans un premier temps la machine émettrice (le client) transmet un segment dont le drapeau SYN est à 1 (pour signaler qu'il s'agit d'un segment de synchronisation), avec un numéro d'ordre N, que l'on appelle numéro d'ordre initial du client
* Dans un second temps la machine réceptrice (le serveur) reçoit le segment initial provenant du client, puis lui envoie un accusé de réception, c'est-à-dire un segment dont le drapeau ACK est à 1 et le drapeau SYN est à 1 (car il s'agit là encore d'une synchronisation). Ce segment contient le numéro d'ordre de cette machine (du serveur) qui est le numéro d'ordre initial du client. Le champ le plus important de ce segment est le champ accusé de réception qui contient le numéro d'ordre initial du client, incrémenté de 1
* Enfin, le client transmet au serveur un accusé de réception, c'est-à-dire un segment dont le drapeau ACK est à 1, dont le drapeau SYN est à zéro (il ne s'agit plus d'un segment de synchronisation). Son numéro d'ordre est incrémenté et le numéro d'accusé de réception représente le numéro d'ordre initial du serveur incrémenté de 1

![TCP Hanshake : SYN - SYN/ACK - ACK](../img/tcp-handshake.gif){.center}

<center>
<figcaption>
Three ways Handshake - Poignée de mains en $3$ temps<br/>
SYN / SYN-HACK / HACK
</figcaption>
</center>

Suite à cette séquence comportant trois échanges les deux machines sont synchronisées et la communication peut commencer!

Il existe une technique de piratage, appelée spoofing IP, permettant de corrompre cette relation d'approbation à des fins malicieuses !

## Méthode de la fenêtre glissante

Dans de nombreux cas, il est possible de limiter le nombre d'accusés de réception, afin de désengorger le réseau, en fixant un nombre de séquence au bout duquel un accusé de réception est nécessaire. Ce nombre est en fait stocké dans le champ fenêtre de l'en-tête TCP/IP.

On appelle effectivement cette méthode "méthode de la fenêtre glissante" car on définit en quelque sorte une fourchette de séquences n'ayant pas besoin d'accusé de réception, et celle-ci se déplace au fur et à mesure que les accusés de réception sont reçus.

![TCP Fenêtre Glissante](../img/tcp-fenetre-glissante.gif){.center}

De plus, la taille de cette fenêtre n'est pas fixe. En effet, le serveur peut inclure dans ses accusés de réception en stockant dans le champ fenêtre la taille de la fenêtre qui lui semble la plus adaptée. Ainsi, lorsque l'accusé de réception indique une demande d'augmentation de la fenêtre, le client va déplacer le bord droit de la fenêtre.

![TCP Fenêtre Glissante Plus](../img/tcp-fenetre-glissante-plus.gif){.center}

Par contre, dans le cas d'une diminution, le client ne va pas déplacer le bord droit de la fenêtre vers la gauche mais attendre que le bord gauche avance (avec l'arrivée des accusés de réception).

![TCP Fenêtre Glissante Moins](../img/tcp-fenetre-glissante-moins.gif){.center}

## Fin d'une Connexion

Le client peut demander à mettre fin à une connexion au même titre que le serveur.
La fin de la connexion se fait de la manière suivante :

* Une des machines envoie un segment avec le drapeau FIN à 1, et l'application se met en état d'attente de fin, c'est-à-dire qu'elle finit de recevoir le segment en cours et ignore les suivants
* Après réception de ce segment, l'autre machine envoie un accusé de réception avec le drapeau FIN à 1 et continue d'expédier les segments en cours. Suite à cela la machine informe l'application qu'un segment FIN a été reçu, puis envoie un segment FIN à l'autre machine, ce qui clôture la connexion...

## RFC

<center>

| RFC | Date | Description |
|:-:|:-:|:-:|
| [RFC 793](https://datatracker.ietf.org/doc/rfc793/) :gb:<br/>[RFC 793](http://abcdrfc.free.fr/rfc-vf/rfc0793.html) :fr: | $1981$ | TCP<br/>Remplace RFC 761<br/>Màj par RFC 1122 ,<br/>RFC 3168<br/>RFC 6093<br/>RFC 6528 |
| [RFC 1122](https://datatracker.ietf.org/doc/html/rfc1122) | $1989$ | TCP Requirements<br/>for Internet Hosts |
| [RFC 3168](https://datatracker.ietf.org/doc/html/rfc3168) | $2001$ | TCP Explicit<br/>Congestion Notification<br/>to IP |
| [RFC 6093](https://datatracker.ietf.org/doc/html/rfc6093) | $2011$ | TCP Implementation<br/>of Urgent mechanism |
| [RFC 6528](https://datatracker.ietf.org/doc/html/rfc6528) | $2012$ | TCP Defending<br/>against Sequence<br/>Number Attacks |

</center>

## Références & Notes

* [TCP vs UDP, GIPSA LAB, Grenoble, INP](http://www.gipsa-lab.grenoble-inp.fr/~christian.bulfone/MIASHS-L3/PDF/3-Les_protocoles_UDP_TCP.pdf)
* [Le Protocole TCP](https://web.maths.unsw.edu.au/~lafaye/CCM/internet/tcp.htm)
* [Entête TCP](https://www.frameip.com/entete-tcp/)