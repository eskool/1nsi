# 1NSI : TD - Simuler un Réseau P2P Gnutella avec le logiciel *Filius*

!!! info
    La version (française) de Filius utilisée dans ce TD est $\geq$ v1.11.0.

## Introduction

Ce TD s'intéresse à :

* La configuration en statique d'un réseau P2P (Gnutella)

