# 1NSI : TD - Simuler un Réseau LAN avec le logiciel *Filius*

!!! info
    La version (française) de Filius utilisée dans ce TD est $\geq$ v1.11.0.

## Introduction

Ce TD s'intéresse à :

* la construction et la configuration d'un réseau local - LAN (Local Area Network), 
* Tous les Hôtes sont connectés via un **switch** (nullement besoin de routeur)  
* De manière **statique** :
    * on configure les Hôtes
    * On configure un Serveur DNS
    * On configure un Serveur Générique
    * On configure un Serveur Web
* De manière **dynamique**, en DHCP, on recommence et ajuste les configurations

## Connecter deux ordinateurs via un Switch

1°) En ***mode Conception / Design*** <img src="./img/modeConception.png" alt="modeConception" style="vertical-align:middle; width: 20px;" /> (qui est le mode par défaut), faire les branchements entre deux ordinateurs portables H1 (hôte 1) et H2 (hôte 2) via le switch, comme suit:
   <img src="./img/1reseau_2ordis.png" alt="simple0" style="display: block; margin-left: auto; margin-right:auto; width:70%; " />
2°) En *mode Conception*, configurer les paramètres suivants des ordinateurs portables /***Portables*** H1 et H2, par des adresses de classe C, par exemple: 192.XXX.XXX.XXX

* Hôte 1:
    * **Nom:** H1
    * **Adresse IP** : 192.168.0.1
    * **Masque** (de sous-réseau) : 255.255.255.0
* Hôte 2:
    * **Nom** : H2
    * **Adresse IP** : 192.168.0.2
    * **Masque** (de sous-réseau) : 255.255.255.0

3°) Tester la connexion de H1 vers H2

* Dans les hôtes ***H1*** et ***H2***, installer le logiciel ***Ligne de commande*** (à installer systématiquement sur chaque ordinateur par la suite):  
      **Procédure pour installer un logiciel**: en ***mode Simulation*** <img src="./img/modeSimulation.png" alt="modesimulation" style="vertical-align:middle; width: 20px;" />, cliquer sur H1 puis ***Software Installation*** puis déplacer ***Ligne de commande*** de la colonne de droite ***Disponibles*** vers la colonne de gauche ***Installed*** en cliquant sur la flèche gauche, puis ***Appliquer les modifications***  
   <img src="./img/installedSoftware_CMD_AVANT.png" alt="modesimulation" style="display: block; margin-left: auto; margin-right:auto; width:80%;" />
   <img src="./img/installedSoftware_CMD_APRES.png" alt="modesimulation" style="display: block; margin-left: auto; margin-right:auto; width:80%;" />
* Ensuite, dans le *Ligne de commande* de H1, tester la bonne connexion vers H2 en tapant:  
**ping 192.168.0.2**  
<img src="./img/pingH1H2.png" alt="modesimulation" style="display: block; margin-left: auto; margin-right:auto; width:80%;" />

## Configurer un nouvel ordi portable, via le Switch

* Hôte 3: choisir une adresse de classe B, par exemple: 172.XXX.XXX.XXX
    * **Nom** : H3
    * **Addresse IP** : 172.16.0.1
    * **Masque** (de sous-réseau) : 255.255.0.0
* Tester la bonne connexion, ou pas, de H1 et H2 vers H3, en tapant dans les *Ligne de commande* de H1 et H2:
    * **ping 172.16.0.1**  
Que Remarquez-vous? Pourquoi? Citez plusieurs solutions pour résoudre ce problème.

<img src="./img/3hotes_1switch.png" alt="modesimulation" style="display: block; margin-left: auto; margin-right:auto; width:75%; " />

## Configurer un Serveur DNS, via le Switch

1°) On décide de choisir les IP/masques suivants pour les hôtes H1, H2 et H3:

* H1 : 192.168.0.1 / 255.255.255.0
* H2 : 192.168.0.2 / 255.255.255.0 et 
* H3 : 192.168.0.3 / 255.255.255.0 

2°) Ajouter un hôte nommé ***Serveur DNS*** physique via le Switch:
<img src="./img/serveurDNSMachine.png" alt="modesimulation" style="display: block; margin-left: auto; margin-right:auto; width:80%; " />
Le paramétrer comme suit:

* **Nom** : Serveur DNS
* **Adresse IP** : 192.168.0.20
* **Masque** (de sous-réseau) : 255.255.255.0

3°) Tester la bonne connexion de H1, H2 et H3, vers le Serveur DNS avec un ping  
4°) Installer les logiciels ***Ligne de commande*** et ***Serveur DNS*** sur l'hôte ***Serveur DNS***
<img src="./img/installedServeurDNS.png" alt="modesimulation" style="display: block; margin-left: auto; margin-right:auto; width:80%; " />

5°) Ajouter un ***Domain Nom*** pour l'Hôte H1 dans l'onglet **Address (A)**:

* **Domain Nom**: H1
* **Adresse IP** : 192.168.0.1
* Cliquer sur ***Ajouter*** : Désormais, le Serveur DNS connaît l'Hôte H1, et il l'associe à l'adresse 192.168.0.1
<img src="./img/serveurDNSConfig.png" alt="modesimulation" style="display: block; margin-left: auto; margin-right:auto; width:80%; " />

6°) De même, Créer des entrées DNS pour les Hôtes H2 et H3  
7°) Démarrer le serveur DNS en cliquant sur ***Démarrer*** (il n'est pas démarré par défaut)  
8°) NE PAS OUBLIER de renseigner l'IP du serveur DNS directement dans les 3 hôtes H1, H2 et H3  
9°) Tester les bonnes configurations:

* A partir du poste H1, ***pinger*** l'hôte H2, dont le nom est maintenant résolu/connu via le Serveur DNS. Les deux commandes suivantes (depuis H1) doivent être équivalentes et fonctionnelles :
    * **ping 192.168.0.2**
    * **ping H2**
<img src="./img/serveurDNSpingH1versH2.png" alt="modesimulation" style="display: block; margin-left: auto; margin-right:auto; width:80%; " />
* Tester toutes les autres configs DNS, avec les noms de domaines (et pas seulement les Adresses IP) :
    * ping de H1 vers H3
    * ping de H3 vers H1, etc..

## Configurer un Serveur Web, via le Switch

* Ajouter un nouvel hôte nommé Serveur Web, via la Switch:
<img src="./img/serveurWebMachine.png" alt="modesimulation" style="display: block; margin-left: auto; margin-right:auto; width:80%; " />

* Paramètres:
    * **Nom**: Serveur Web
    * **Adresse IP** : 192.168.0.10
    * **Masque** (de sous-réseau) : 255.255.255.0
* Sur l'hôte *Serveur Web*, installer les logiciels suivants:
    * **Ligne de Commande**
    * **Serveur web**
    * **Explorateur de fichiers**
    * **Éditeur de textes**
* Lancer le serveur: autrement dit, sur l'hôte ***Serveur Web***, lancer le logiciel ***Serveur web***, puis cliquer sur ***Démarrer*** : vous devriez lire la réponse suivante du serveur web:  *Réception des requêtes démarrée*
* Tester la connexion vers le serveur web depuis H1 (par exemple), pour cela:
    * Sur l'hôte H1: installer un navigateur internet, i.e. installer le logiciel ***Navigateur web***
* Tester que le serveur Web soit joignable depuis l'hôte H1:
    * Sur H1 : lancer le nivagateur web
    * Dans la barre d'addresse du navigateur, taper l'adresse IP du serveur Web (http://192.168.0.10), vous devriez voir le résultat suivant (*auf deutsch* :de:, sur les anciennes versions de Filius, car c'est un logiciel Allemand...) :
<img src="./img/serveurWebTestPage.png" alt="modesimulation" style="display: block; margin-left: auto; margin-right:auto; width:80%;" />
* Personnaliser la page web d'accueil du serveur Web:
    * Sur le Serveur Web, lancer le logiciel ***Éditeur de textes***, ouvrir le fichier ***webserver/index.html***. Vous devriez voir le code suivant:
    <img src="./img/indexHTML_Initial.png" alt="modesimulation" style="display: block; margin-left: auto; margin-right:auto; width:80%; " />
    * remplacez tout simplement ce code par le code HTML que vous souhaitez, et cliquer sur ***enregistrer***
    * puis ajoutez une image que vous souhaitez: ***Explorateur de fichiers***, cliquer sur ***webserver***, puis sur **Importer**, **Parcourir** pour sélectionner l'image, puis ***Ouvrir*** (pour la télécharger), puis finalement ***Importer le fichier***. Une fois téléchargée sur le Serveur web (dans le même répertoire que **index.html**...), faire apparaître l'image dans votre page web d'accueil du serveur Web dans FILIUS  
    Dans mon cas, j'utiliserais la photo et le code HTML suivants:  
    1°) Logo du Lycée:
    <img src="./img/perier.png" alt="modesimulation" style="display: block; margin-left: auto; margin-right:auto; width:50%; " />  
    2°) code personnalisé de la page ***index.html*** (à insérer dans FILIUS)

    ```html
    <html>
    <head>
        <title>Accueil</title>
    </head>
    <body bgcolor="#ccddff" style="font-family:Verdana; text-align:center;">
        <h2> Bienvenue sur la superbe page d'accueil NSI du Lycée PÉRIER </h2>
        <p align="center"> <img src="perier.png"> </p>
        <p>créée avec le superbe logiciel OpenSource FILIUS!</p>
        <p>Rendez-vous sur nos pages : <a href="http://lyceeperier.fr">Site ATRIUM</a></p>
    </body>
    </html>
    ```

Vous devriez voir une page comme ceci:  
<img src="./img/serveurWebPagePerso.png" alt="modesimulation" style="display: block; margin-left: auto; margin-right:auto; width:80%;" />

!!! info "ATTENTION : L'Affichage de l'image peut être sensible, au premier chargement"
    L'affichage de l'image peut être *sensible* la première fois (elle ne s'affiche pas toujours bien). Si vous rencontrez des problèmes pour l'affichage de l'image, vous pouvez:

    * ne faire que patienter que l'image se charge totalement, automatiquement, toute seule au bout d'un certain temps (car si l'image que vous avez téléchargée est lourde, il faudra patienter... pas d'autre choix)
    * installer un navigateur web sur un autre poste (H2, H3?), et se rendre sur le site du serveur web: http://192.168.0.10 depuis cet autre hôte
    * désinstaller et réinstaller le navigateur web sur l'hôte 1. Re-tester.  
    En cas de ***tempête de requêtes***, vous pouvez cliquer sur le mode Conception, puis revenir sur le Simulation.

!!! ex
    1°) Notre lien vers la page **Site ATRIUM** du Lycée PÉRIER ne fonctionne pas. POURQUOI? Peut-on résoudre ce problème? et si OUI, comment?  
    2°) Maintenant que notre Serveur Web est configuré correctement, définir une entrée pour notre site (appelons-le, par exemple) **nsiperier.fr** dans le ***Serveur DNS*** (en lançant le logiciel ***Serveur DNS***). Dans l'**onglet Adresse (A)** :
    
    * Nom de domaine : nsiperier.fr
    * Adresse IP : 192.168.0.10 
    Puis cliquer sur **Ajouter**
    De cette manière, ce site sera accessible via la barre d'adresse du ***Navigateur web*** de l'hôte H1 :  
    Vous devriez pouvoir, (par exemple) depuis l'Hôte H1, accéder à votre site non plus par http://192.168.0.10 (ce qui était déjà pas mal, mais pas très *humain compatible*), mais en tapant l'URL suivante dans la barre d'adresse : **http://nsiperier.fr**.
    <img src="./img/serveurWebviaDNS.png" alt="modesimulation" style="display: block; margin-left: auto; margin-right:auto; width:80%; max-width:600px;" />

## Configurer votre réseau en DHCP - Dynamic Host Configuration Protocol

La plupart du temps, un réseau LAN familial ou d'une petite entreprise possède de nombreux Hôtes et on comprend qu'il devient vite fastidieux de configurer un par un, manuellement, chaque hôte comme nous l'avons fait jusqu'à présent. C'est pourquoi on utilise un ***Serveur DHCP*** - ***Dynamic Host Configuration Protocol*** - pour configurer **automatiquement** chaque hôte.  

Principe Théorique:

* DHCP DISCOVER : Les Clients sans adresse IP envoient en diffusion broadcast, en enoyant leur adresse MAC
* DHCP OFFER : Tout Serveur DHCP recevant l'offre répond avec une offre d'adresse IP pour le client (identifié par son MAC), et transmets également l'IP du serveur qui fait l'offre
* DHCP REQUEST : Le Client retient l'offre (ou la première offre parmi les plusieurs reçues) d'adresse IP, et difuse sur le réseau une requête DHCP pour signaler qu'il retient cette IP
* DHCP ACK : Le Serveur confirme la réception de la demande et renvoie une adresse IP

### Ajout de quelques postes, et Configuration en DHCP

* Ajouter trois nouveaux hôtes (ordis portables): H4, H5, H6  
(**ne PAS configurer manuellement les IPs, nous allons les configurer en DHCP**)
<img src="./img/serveurDHCP3portables.png" alt="modesimulation" style="display: block; margin-left: auto; margin-right:auto; width:70%; " />
* Ajouter un nouvel Hôte nommé ***Serveur DHCP*** de paramètres :
    * **Nom** : Serveur DHCP
    * **Adresse IP** : 192.168.0.5
    * **Masque** : 255.255.255.0
<img src="./img/serveurDHCPMachine.png" alt="modesimulation" style="display: block; margin-left: auto; margin-right:auto; width:70%; " /> 

* Vous êtes l'Administrateur Réseau et vous décidez que :
    * **TOUS** les **ordinateurs portables soient configurés par DHCP**, qu'ils récupèrent donc automatiquement une adresse IP : "entre" 192.168.1.100 et 192.168.1.200
    * mais PAS le Serveur web (192.168.0.10), NI le Serveur DNS (192.168.0.20), qui doivent conserver leurs adresses IP statiques
    * les **Serveurs DNS** et **Serveur Web** sont configurés **manuellement avec des adresses IP statiques** (fixes) que vous aurez choisi, par exemple (inchangées):
        * **IP serveur Web**: 192.168.0.10
        * **IP serveur DNS**: 192.168.0.20

### Configuration du Serveur DHCP

* Définir manuellement l'IP du **Serveur DNS** sur le **Serveur DHCP** : Pour cela, En **mode Conception**, Cliquer sur le **Serveur DHCP**, puis sur **Configuration du service DHCP**, dans l'onglet **Paramètres de base**, renseigner comme suit: 
    * Début de la page : 192.168.1.100
    * Fin de la page : 192.168.1.200
    * Activer la checkbox **Configuration manuelle** du ***Serveur DHCP*** et 
    * renseigner l'IP du **Serveur DNS** : 192.168.0.20
    * Activer également la checkbox ***Activer le service DHCP***
    * Ne **PAS** modifier/configurer la **Passerelle** (inexistant pour le moment). Laisser l'IP 0.0.0.0
    * puis **OK**
<img src="./img/serveurDHCPConfigBaseSettings.png" alt="modesimulation" style="display: block; margin-left: auto; margin-right:auto; width:80%; " />

* Dans l'onglet **Adressage statique**:
    * Entrer l'adresse MAC <red>de votre Serveur Web</red> (à récupérer au préalable)
    * l'IP ***statique*** choisie pour le Serveur web: 192.168.0.10
  <img src="./img/serveurDHCPStaticIPAVANT.png" alt="modesimulation" style="display: block; margin-left: auto; margin-right:auto; width:70%; " />
    * Cliquer sur **Ajouter**
    * puis **OK**
<img src="./img/serveurDHCPStaticIPAPRES.png" alt="modesimulation" style="display: block; margin-left: auto; margin-right:auto; width:70%; " />
* De même, toujours dans l'onglet **Adressage statique** :
    * entrer l'adresse MAC <red>de votre Serveur DNS</red> et 
    * son adresse IP statique (192.168.0.20)
<img src="./img/serveurDHCPStaticIPAPRESDNS.png" alt="modesimulation" style="display: block; margin-left: auto; margin-right:auto; width:70%; " />

### Configuration de chaque Hôte en DHCP

En **mode Conception**, cliquer sur **CHACUN** des Portables, et activer les deux checkbox suivantes:

* **Utiliser l'Adresse IP comme nom** (non obligatoire mais utile en DHCP)
* **Adressage automatique par serveur DHCP**  

AVANT de cliquer sur le mode Simulation, vous devriez voir quelque chose comme ceci:
<img src="./img/reseauDHCPAVANT.png" alt="modesimulation" style="display: block; margin-left: auto; margin-right:auto; width:90%; " />
APRÈS cliquer sur le mode Simulation, vous devriez voir une sorte de **tempête de requêtes**, car TOUS les hôtes demandent leur nouvelle adresse IP au serveur DHCP (recliquer une/deux fois sur le mode Simulation, si besoin). Vous devriez alors voir quelque chose comme ceci:
<img src="./img/reseauDHCPAPRES.png" alt="modesimulation" style="display: block; margin-left: auto; margin-right:auto; width:90%; " />

!!! ex
    Les Portables disposent bien d'une adresse IP en DHCP (qui *peuvent* varier lorsque l'on reclique sur le mode Simulation). Néanmoins, on pourrait se demander si la communication vers le Serveur Web a bien été préservée, ou pas.  
    1°) Depuis le **Poste 1** (192.168.1.100), *pinger* le **Serveur Web** (192.168.0.10):  
    **ping 192.168.0.10**  
    Normalement, ça devrait ... NE PAS MARCHER... en effet, on reçoit un **Timeout!**
    <img src="./img/timeout.png" alt="modesimulation" style="display: block; margin-left: auto; margin-right:auto; width:80%;" />

    2°) POURQUOI ce problème de ping vers le Serveur Web depuis un Portable? et Comment le résoudre? 
    
    !!! hint "Aide"
        On pourra adapter les masques de sous-réseau.. :
        
        * du Serveur DHCP (pourquoi?)
        * du Serveur Web (pourquoi?)

    (puis, repasser en mode Simulation, pour provoquer une nouvelle tempête de requêtes)  
    3°) Une fois le problème précédent résolu (que l'on peut *pinger* le Serveur Web depuis le Poste 1) :
    
    * Vérifier que l'on puisse toujours accéder au site du Serveur Web, depuis le Poste 1: 
    Normalement, .... seule une méthode parmi les deux fonctionne:  
        * l'accès via l'IP 192.168.0.10 fonctionne :
        <img src="./img/serveurDHCPDnsIP.png" alt="modesimulation" style="display: block; margin-left: auto; margin-right:auto; width:80%;" />
        * mais PAS l'accès via l'URL nsiperier.fr ... **Erreur** --> **Impossible de joindre le serveur!** :  
        (ATTENTION : Il faut refaire une nouvelle tempête de requêtes pour se rendre compte que le serveur est injoignable, sinon vous aurez la fausse impression que tout fonctionne, mais cela ne fonctionnera plus après une future nouvelle tempête..)
        <img src="./img/serveurDHCPetDnsNsiImpossible.png" alt="modesimulation" style="display: block; margin-left: auto; margin-right:auto; width:80%;" />
    Quel est le problème? et comment le résoudre?  
        !!! hint "Aide"
            On pourra adapter le masque de sous-réseau du Serveur DNS (pourquoi?)

    4°) Une fois ce problème résolu, vérifier que les deux URLs du site fonctionnent (http://192.168.0.10 et http://nsiperier.fr)  
    <img src="./img/serveurDHCPetDnsNsi.png" alt="modesimulation" style="display: block; margin-left: auto; margin-right:auto; width:80%;" />

    5°) Lire les échanges de données émises depuis le Poste 192.168.1.100, notamment :

    * les **paquets TCP/IP**
    * les **paquets DHCP** (DISCOVER,OFFER, REQUEST,ACK)  
    Pour cela, **en mode Simulation**, Cliquer droit sur le Poste 192.168.1.100, et choisir **Afficher les échanges de données (192.168.1.100)**

    <img src="./img/dataTCP.png" alt="modesimulation" style="display: block; margin-left: auto; margin-right:auto; width:100%; height:300px;" />

    6°) Dans les données du paquet :

    * Quelle est la version du Protocole HTTP utilisée?
    * le TTL?
    * Trouver le paquet TCP dans lequel le Navigateur web demande l'image au serveur

    **Travail à faire:**  
    Sauvegarder votre travail sous **reseauLAN_VotreNOM_VotrePrenom_VotreClasse.fls**

