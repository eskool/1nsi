% TD : Simulation de Réseaux avec le logiciel *Filius*
% Rodrigo SCHWENCKE, http://lyceeperier.fr

> **Avant-Propos :**
> 
> * Auteur: Rodrigo SCHWENCKE, Lycée PÉRIER
> * Thème (Bloc 3): Architecture, Réseaux, Systèmes d'Exploitation
> * Référence du [B.O.](http://cache.media.education.gouv.fr/file/SP1-MEN-22-1-2019/26/8/spe633_annexe_1063268.pdf), page 7: Contenus -> Architecture d'un réseau (et Protocoles de Communication). Plus précisément, commentaires -> capacités attendues: Simuler ou mettre en oeuvre un réseau.
> * Licence : ![](./cc_by_nc_sa.png){.noimgbox style="vertical-align:middle;"}
> * Durée: 2 heures (et plus si affinités)
> * TD en  Salle Informatique
> * Logiciels requis: **Filius 1.7.2, ou supérieur**
> * Scénario Pédagogique proposé: Travail par groupe de 3 élèves

# Configurer un réseau local LAN (Local Area Network)

Dans cette partie, on travaillera dans un seul réseau, donc pas besoin de routeur: seul un **switch** sera nécessaire pour les communications entre les éléments du réseau.  
Nous allons commencer par réaliser les **configurations *manuellement* sur les Hôtes**.

## Connecter deux ordinateurs via un Switch
1. En ***mode Design*** <img src="modeDesign.png" alt="modedesign" style="vertical-align:middle; width: 20px;" /> (qui est le mode par défaut), Créer une connection entre deux ordinateurs portables H1 (hôte 1) et H2 (hôte 2) via le switch, comme suit:
   <img src="1reseau_2ordis.png" alt="simple0" style="display: block; margin-left: auto; margin-right:auto; width:30%; " />
2. En *mode Design*, configurer les paramètres suivants des ordinateurs portables /***Notebooks*** H1 et H2, par des adresses de classe C, par exemple: 192.XXXX.XXX.XXX
* Hôte 1:
  * **Name (Nom Hôte):** H1
  * **IP Address** : 192.168.0.1
  * **Netmask** (masque de sous-réseau) : 255.255.255.0
* Hôte 2:
  * **Name (Nom Hôte)** : H2
  * **IP Address** : 192.168.0.2
  * **Netmask** (masque de sous-réseau) : 255.255.255.0

<br><br><br><br><br><br>

3. Tester la connexion de H1 vers H2
* Dans l'hôte ***H1***, installer le logiciel ***Command Line***:  
      **Procédure pour installer un logiciel**: en ***mode Simulation*** <img src="modeSimulation.png" alt="modesimulation" style="vertical-align:middle; width: 20px;" />, cliquer sur H1 puis ***Software Installation*** puis déplacer ***Command Line*** de la colonne de droite ***Available*** vers la colonne de gauche ***Installed*** en cliquant sur la flèche gauche, puis ***Apply Changes***  
   <img src="installedSoftware_CMD_AVANT.png" alt="modesimulation" style="width: 40%; margin-right: 30px;" />
   <img src="installedSoftware_CMD_APRES.png" alt="modesimulation" style="width: 40%;" />
* Ensuite, dans le *Command Line* de H1, tester la bonne connexion vers H2 en tapant: **ping 192.168.0.2**  
<img src="pingH1H2.png" alt="modesimulation" style="display: block; margin-left: auto; margin-right:auto; width: 40%;" />

## Ajouter un nouvel ordi portable, via le Switch{.newpage}
* Hôte 3: choisir une adresse de classe B, par exemple: 172.XXX.XXX.XXX
  * **Name (Nom Hôte)** : H3
  * **IP Address** : 172.16.0.1
  * **Netmask** (masque de sous-réseau) : 255.255.0.0
* Tester la bonne connexion de H1 et H2 vers H3, en tapant dans les *Command Line* (à installer) de H1 et H2.
  * **ping 172.16.0.1**  

<span style="color:red;">
* **Que remarquez-vous?** l'hôte H3 n'est pas joignable -> **destination not reachable**  
* **Pourquoi?**  car H3 n'est pas dans le même réseau que H1 et H2. En effet, H1 (resp. H2) ne peut découvrir que les ordinateurs dont les IP sont de la forme 192.168.0.x c'est ce que veut dire le masque de sous-réseau d'une valeur de 255.255.255.0  
* **Comment résoudre ce problème?** En modifiant les masques de sous-réseaux de H1 (resp H2) et H3, par exemple et pour simplifier, en modifiant (seulement) les premières composantes des deux masques (192 et 172) de sorte à ce que **la nouvelle première composante du masque laisse passer (valeurs à 1) les parties communes (en binaire) aux nombres 192 (11 00 00 00) et 172 (10 10 11 00), et empêche de passer (valeurs à 0) les parties non communes (en binaire) à 192 et 172**. Remarque : On pourrait étendre ce raisonnement aux deuxièmes composantes du masque (168 et 16), ce que nous ne ferons pas, par souci de simplicité initiale, en choisisant la deuxième composante à 0, on acceptera tous les hôtes correspondants (en particulier 168 et 16)...  
Comparons en binaire les premières composantes du masque:  
192 --> 11 00 00 00 et  
172 --> 10 10 11 00  
On s'aperçoit que les colonnes communes sont (de droite à gauche) d'indice 0, 1, 4 et 7, donc les masques ***possibles*** sont les nombres dont ces colonnes (0,1,4 et 7) peuvent être à 1 en binaire:
10 00 00 00 -> 128  
10 00 00 01 -> 129  
10 00 00 10 -> 130  
10 00 00 11 -> 131  
10 01 00 00 -> 144  
10 01 00 01 -> 145  
10 01 00 10 -> 146  
10 01 00 11 -> 147  
Toute autre valeur pour la première composante renvoie un **Timeout** pour le ping de H1 vers H3.
Remarquez que (bis) on pourrait étendre ce raisonnement à la deuxième composante du masque (168 et 16).
Il suffira néamnoins par simplicité
**Conclusion:** Exemples de masques de sous-réseau possibles pour H1 (resp H2) et/ou pour H3
128.0.0.0
129.0.0.0
130.0.0.0
131.0.0.0
144.0.0.0
145.0.0.0
146.0.0.0
147.0.0.0
ReTestez la connexion!
</span>

<img src="3hotes_1switch.png" alt="modesimulation" style="display: block; margin-left: auto; margin-right:auto; width:35%; " />

## Ajouter un Serveur DNS, via le Switch{.newpage}
1. Choisir des IP/masques suivants pour les hôtes H1, H2 et H3:
   H1 (192.168.0.1/255.255.255.0), H2(192.168.0.2/255.255.255.0) et H3(192.168.0.3/255.255.255.0)  
2. Ajouter un hôte nommé ***Serveur DNS*** physique via le Switch:
<img src="serveurDNSMachine.png" alt="modesimulation" style="display: block; margin-left: auto; margin-right:auto; width:40%; " />
Le paramétrer comme suit:
  * **Name (Nom Hôte)** : Serveur DNS
  * **IP Address** : 192.168.0.20
  * **Netmask** (masque de sous-réseau) : 255.255.255.0
3. Tester la bonne connexion de H1, H2 et H3, vers le Serveur Web avec un ping
4. Installer le logiciel ***DNS server*** sur l'hôte ***Serveur DNS***
<img src="installedServeurDNS.png" alt="modesimulation" style="display: block; margin-left: auto; margin-right:auto; width:40%; " />

5. Ajouter un Domain Name pour l'Hôte H1 dans l'onglet **Address (A)**:
* **Domain name**: H1
* **IP Address** : 192.168.0.1
* Cliquer sur ***Add***
<img src="serveurDNSConfig.png" alt="modesimulation" style="display: block; margin-left: auto; margin-right:auto; width:40%; " />

6. Faire de même pour les 2 Hôtes H2 et H3
7. Démarrer sur le serveur DNS en cliquant sur ***Start***  
<span style="color:red;">Réponse : Hôtes configurés et Serveur Démarré:</span>
<img src="Corrigé_Partie1_q3_serveurDNS.png" alt="modesimulation" style="display: block; margin-left: auto; margin-right:auto; width:40%; " />

8. NE PAS OUBLIER de renseigner l'IP du serveur DNS directement dans les 3 hôtes H1, H2 et H3  
<span style="color:red;">Réponse : Taper 192.168.0.20 dans la zone Domain Name Server</span>

9.  Tester les bonnes configurations:
* A partir du poste H1, ***pinger*** l'hôte H2, dont le nom est maintenant résolu/connu via le Serveur DNS, via la ligne de commande:
  * **ping H2**
* Tester les autres configs :
  * de H1 vers H3
  * de H3 vers H1, etc..

## Ajouter un Web Server, via le Switch
* Ajouter un nouvel hôte nommé Serveur Web, via la Switch:
<img src="serveurWebMachine.png" alt="modesimulation" style="display: block; margin-left: auto; margin-right:auto; width:40%; " />
* Paramètres:
  * Name (Nom d'Hôte): Serveur Web
  * IP Address : 192.168.0.10
  * Netmask (masque de sous-réseau) : 255.255.255.0
* Sur l'hôte *Serveur Web*, installer les logiciels suivants:
  * **Webserver**
  * **File Explorer**
  * **Text Editor**
* Lancer le serveur: autrement dit, sur l'hôte ***Serveur Web***, lancer le logiciel ***Webserver***, puis cliquer sur ***Start*** : vous devriez lire la réponse suivante du serveur web:  *Start accepting connections*
* Tester la connexion vers le serveur web depuis H1 (par exemple), pour cela:
  * Sur l'hôte H1: installer un navigateur internet/le logiciel ***Webbrowser***
* Tester que le serveur Web soit joignable depuis l'hôte H1:
  * Sur H1 : lancer le nivagateur web
  * Dans la barre d'addresse du navigateur, taper l'adresse IP du serveur Web, vous devriez voir le résultat suivant (*auf deutsch* car Filius est un logiciel Allemand...) :
<img src="serveurWebTestPage.png" alt="modesimulation" style="display: block; margin-left: auto; margin-right:auto; width:40%; " />
* Personnaliser la page web d'accueil du serveur Web:
  * Sur le Serveur Web, via l'Editeur de Texte/***Text editor***, ouvrir le fichier ***webserver/index.html***. Vous devriez voir le code suivant:
  <img src="indexHTML_Initial.png" alt="modesimulation" style="display: block; margin-left: auto; margin-right:auto; width:40%; " />
  * remplacez tout simplement ce code par le code HTML que vous souhaitez, 
  * puis ajoutez l'image que vous souhaitez: dans FILIUS en tant que fichier, puis la faire apparaître dans votre page web d'accueil du serveur Web dans FILIUS  
  Dans mon cas, j'utiliserais la photo et le code HTML suivants:  
1°) Logo du Lycée:
<img src="perier.png" alt="modesimulation" style="display: block; margin-left: auto; margin-right:auto; width:30%; " />  
2°)  code personnalisé de la page ***index.html*** (à insérer dans FILIUS)

```html
<html>
  <head>
    <title>Accueil</title>
  </head>
  <body bgcolor="#ccddff" style="font-family:Verdana; text-align:center;">
    <h2> Bienvenue sur la superbe page d'accueil NSI du Lycée PÉRIER </h2>
    <p align="center"> <img src="perier.png"> </p>
    <p>créée avec le superbe logiciel OpenSource FILIUS!</p>
    <p>Rendez-vous sur nos pages : <a href="http://lyceeperier.fr">Site ATRIUM</a></p>
  </body>
</html>
```

Vous devriez voir une page comme ceci:
<img src="serveurWebPagePerso.png" alt="modesimulation" style="vertical-align: middle; float right; width:27%; " />

<span style="color:red;">
**Réponse: ATTENTION (Astuce) !**  
Il FAUT OBLIGATOIREMENT importer l'image souhaitée "perier.png" sur le Serveur Web: Lancer le File Explorer --> Import --> Select file (parcourir votre arborescence en quête de l'image souhaitée) -->Import file  
Faute de quoi vous ne verrez PAS l'image sur votre site (image cassée)
</span>

**Exercice:**  
1. Notre lien vers la page **Site ATRIUM** du Lycée PÉRIER ne fonctionne pas. POURQUOI? Peut-on résoudre ce problème? et si OUI, comment?  
<span style="color:red;">
**Pourquoi?** Le logicielFILIUS ne peut pas accéder au "vrai" internet. Il reste dans FILIUS. 
Et de plus, le site "http://lyceeperier.fr" n'existe PAS sur le Serveur Web, ni dans le Serveur DNS, **DANS FILIUS**.
**Peut-on résoudre de problème?** OUI, en créant ce site dans le Serveur Web **de FILIUS**, et en configurant l'hôte "http://lyceeperier.fr" dans le Serveur DNS de FILIUS.
</span>

1. Maintenant que notre Serveur Web est configuré correctement, définir une entrée pour notre site (appelons-le par exemple) **nsiperier.fr** dans le ***Serveur DNS*** toujours via l'**onglet Adress (A)**, de sorte que ce site soit accessible via la barre d'adresse du navigateur (webbrowser) de l'hôte H1.
Vous devriez pouvoir accéder à votre site non plus par http://192.168.0.10 (ce qui était déjà un peu  compliqué), mais toujours sur l'hôte H1 (par exemple), en tapant l'URL suivante dans la barre d'adresse: **http://nsiperier.fr** du **Webbrowser**.

<img src="serveurWebviaDNS.png" alt="modesimulation" style="float:left; width:35%;" />

<img src="Corrigé_PartieI_q4_ajoutSiteNsiperier.png" alt="modesimulation" style="float:left; width:35%;" />

## Configurer votre réseau en DHCP - Dynamic Host Configuration Protocol{.newpage}

La plupart du temps, un réseau LAN familial ou d'une petite entreprise possède de nombreux Hôtes et on comprend qu'il devient vite fastidieux de configurer chaque hôte un par un, manuellement,  comme nous l'avons fait jusqu'à présent. C'est pourquoi on utilise un ***Serveur DHCP*** - ***Dynamic Host Configuration Protocol*** - pour configurer **automatiquement** chaque hôte.  

Principe Théorique:

* DHCP DISCOVER : Les Clients sans adresse IP envoient en diffusion broadcast, en envoyant leur adresse MAC
* DHCP OFFER : Tout Serveur DHCP recevant l'offre répond avec une offre d'adresse IP pour le client (identifié par son MAC), et transmets également l'IP du serveur qui fait l'offre
* DHCP REQUEST : Le Client retient l'offre (ou la première offre parmi les plusieurs reçues) d'adresse IP, et difuse sur le réseau une requête DHCP pour signaler qu'il retient cette IP
* DHCP ACK : Le Serveur confirme la réception de la demande et renvoie une adresse IP

### Ajout de quelques postes pour le réseau en DHCP :

* Ajouter 3 nouveaux hôtes (ordis portables): H4, H5, H6 (**ne PAS configurer manuellement les IPs**)
<img src="serveurDHCP3portables.png" alt="modesimulation" style="display: block; margin-left: auto; margin-right:auto; width:30%; " />
* Ajouter un nouvel Hôte nommé ***Serveur DHCP***:
<img src="serveurDHCPMachine.png" alt="modesimulation" style="display: block; margin-left: auto; margin-right:auto; width:30%; " /> de paramètres :
  * Name : Serveur DHCP
  * IP address : 192.168.0.5
  * Netmask : 255.255.255.0
* Vous êtes l'Administrateur Réseau et vous décidez que [:]{.newpage}
  * les **ordinateurs portables soient configurés par DHCP**, donc qu'ils récupèrent automatiquement une adresse IP, "entre" 192.168.1.100 et 192.168.1.200
  * les **Serveurs DNS** et **Serveur Web** sont configurés **manuellement avec des adresses IP statiques** (fixes) que vous aurez choisi, par exemple (inchangées):
    * **IP serveur Web**: 192.168.0.10
    * **IP serveur DNS**: 192.168.0.20

### Configuration du Serveur DHCP:  

* Pour cela, En **mode Design**, Cliquer sur le **Serveur DHCP**, puis sur **DHCP server setup**, dans l'onglet **Base settings**, renseigner comme suit: (activer la config manuelle du ***DNS server*** et renseigner son IP. Sélectionner également la checkbox ***Activate DHCP***)
<img src="serveurDHCPConfigBaseSettings.png" alt="modesimulation" style="display: block; margin-left: auto; margin-right:auto; width:20%; " />

* Dans l'onglet **Static Address Assignment**:
  * Entrer l'adresse MAC de **votre** Serveur Web (à récupérer au préalable, et n'oubliez pas de sauvegarder vos configs actuelles, en cliquant sur ***OK***)
  * l'IP ***statique*** choisie: 192.168.0.10
<img src="serveurDHCPStaticIPAVANT.png" alt="modesimulation" style="display: block; margin-left: auto; margin-right:auto; width:30%; " />
  * Cliquer sur **Add**
<img src="serveurDHCPStaticIPAPRES.png" alt="modesimulation" style="display: block; margin-left: auto; margin-right:auto; width:30%; " />

* Ne **PAS** modifier/configurer le **Gateway** (**passerelle par défaut**, i.e. l'IP du routeur: inexistant pour le moment)
* Faire de même pour le Serveur DNS

<br><br><br>

**Configuration de chaque hôte**  

* En **mode Design**, cliquer sur **chacun** des hôtes, et paramétrer-le ainsi:
  * **Use DHCP for configuration**, (et aussi pour changer un peu...)
  * **Use IP address as Name** (non obligatoire mais utile en DHCP)
<img src="hote1DHCP.png" alt="modesimulation" style="display: block; margin-left: auto; margin-right:auto; width:50%; " />
AVANT de passer en/cliquer sur le mode Simulation, vous devriez voir quelque chose comme ceci:
<img src="reseauDHCPAVANT.png" alt="modesimulation" style="display: block; margin-left: auto; margin-right:auto; width:40%; " />
PUIS, en cliquant sur le mode Simulation, vous devriez voir une sorte de **tempête de requêtes**, car TOUS les hôtes demandent leur nouvelle adresse IP au serveur DHCP.
APRÈS le clic, vous devriez voir quelque chose comme ceci:
<img src="reseauDHCPAPRES.png" alt="modesimulation" style="display: block; margin-left: auto; margin-right:auto; width:40%; " />

[**Exercice:**]{.newpage}

1. Tester la bonne connexion du **Poste 1** dont l'IP est 192.168.0.100 vers le **Serveur Web**. Sur le poste 1, en command Line, taper:  
**ping 192.168.0.10**  
Normalement, ça devrait ... NE PAS MARCHER... en effet, on reçoit un **Timeout!**
<img src="timeout.png" alt="modesimulation" style="display: block; margin-left: auto; margin-right:auto; width:40%; " />

2. POURQUOI ce problème de ping? et Comment le résoudre?  
<span style="color:red;">
**Pourquoi ce problème?** Parce que le Poste 1 (192.168.1.100/255.255.255.0) n'est PAS dans le même sous-réseau que le Serveur Web (192.168.0.10/255.255.255.0)
**Comment le résoudre?** 
Voici deux méthodes différentes pour résoudre ce problème:  
**1°) Méthode 1 : Modifier le masque de sous-réseau du serveur DHCP**  
En mode Design, cliquer sur le Serveur DCHP, et choisir par exemple 255.255.0.0 comme masque de sous-réseau, puis cliquer sur le mode Simulation pour broadcaster les modifications.  
**2°) Méthode 2 : Modifier l'IP statique du Serveur Web**  
-- Le Serveur DCHP conserve un masque de sous-réseau de 255.255.255.0  
-- nouvelle IP Statique du Serveur Web: 192.168.1.10 / 255.255.0.0
</span>

3. Une fois le problème résolu, et que le ping précédent du Poste 1 (192.168.1.100) vers le Serveur Web (192.168.0.10) fonctionne, on vérifie que l'on puisse toujours accéder à la page web d'accueil du Serveur Web, à partir du Poste 1: 
Normalement, .... seule une méthode parmi les deux fonctionne:  
-- l'accès via l'IP 192.168.0.10 fonctionne,
-- mais PAS l'accès via l'URL nsiperier.fr ...**Server does not exist!**  
Quel est le problème? et comment le résoudre?  
<span style="color:red;">
**Quel est le problème?** Le problème est que le masque de sous-réseau du Serveur DNS est encore défini à 2555.255.255.0 or c'est lui qui résout l'IP du site web nsiperier.fr  
**Comment le résoudre?** Il suffit de modifier le masque de sous-réseau du Serveur DNS, et choisir : 255.255.0.0
</span>

4. Une fois ce problème résolu, tester la bonne connexion de la page d'accueil du Serveur Web, à partir du poste 1, normalement les deux URLs doivent fonctionner (http://192.168.0.10 et http://nsiperier.fr)  
<img src="serveurDHCPDnsIP.png" alt="modesimulation" style="width:45%; " />
<img src="serveurDHCPetDnsNsi.png" alt="modesimulation" style="width:45%; " />

5. lire, parmi les données envoyées depuis le Poste 192.168.1.100:
* les **paquets TCP/IP**
* les **paquets DHCP** (DISCOVER,OFFER, REQUEST,ACK)
Pour cela, **en mode Simulation**, Cliquer droit sur le Poste 192.168.1.100, et choisir **Show data exchange (192.168.1.100)**

<img src="dataTCP.png" alt="modesimulation" style="display: block; margin-left: auto; margin-right:auto; width:100%; " />

6. Dans les données du paquet
* Quelle est la version du Protocole HTTP utilisée?
<span style="color:red;">
HTTP/1.1 donc HTTP version 1.1
</span>
* le TTL? <span style="color:red;">=64</span>
* Trouver le paquet TCP dans lequel le Webbrowser demande l'image au serveur  
<span style="color:red;">
C'est la ligne contenant le comment(aire) :  
GET perier.png HTTP/1.1 Host : 192.168.0.10
</span>

**Travail à faire:**  
Sauvegarder votre travail sous **reseauLAN_VotreNOM_VotrePrenom_VotreClasse.fls**

# Connecter & Configurer plusieurs Réseaux, connectés via un/des routeurs{.newpage}

On souhaite créer et configurer plusieurs réseaux de la manière suivante:
<img src="reseau_WAN.png" alt="modesimulation" style="display: block; margin-left: auto; margin-right:auto; width:40%; " />

**Question :** Combien y-a-t-il de réseaux dans cette configuration/topologie?  
<span style="color:red;">
Il y a trois (sous-)réseaux:  
1°) le sous-réseau de gauche dont les adresses IP sont en 192.168.0.xx  
2°) le sous-réseau de droite dont les adresses IP sont en 192.168.1.xx  
3°) le sous-réseau de gauche dont les adresses IP sont en 192.168.2.xx (le Serveur DNS est, pour le moment, l'unique hôte dans ce troisième sous-réseau)  
</span>


Le routeur doit être configuré de sorte qu'il dispose de 3 **interfaces (réseau)** comprendre 3 **cartes réseaux**  ou encore 3 **NIC (Network Interface Card)**  
Vous pouvez configurer ceci de deux manières:

* ou bien, au moment du dépôt du routeur (si vous savez combien il vous en faut à l'avance...). Modifier le nombre d'interfaces réseaux/NIC (2 = valeur par défaut) à **3**:
<img src="routeurNbNIC.png" alt="modesimulation" style="display: block; margin-left: auto; margin-right:auto; width:30%; " />

* ou bien, APRÈS avoir déposé le routeur (s'il vous en manque après-coup), en mode **Design**, en double-cliquant sur le routeur:
  * cliquer sur **Manage Connections**
<img src="routeurAjoutNICManageConnections.png" alt="modesimulation" style="display: block; margin-left: auto; margin-right:auto; width:90%; " />
  * puis, cliquer sur le "**+**" (en bas à droite) pour ajouter une **interface**/**NIC**, vous devriez voir ceci (ensuite cliquer sur [Close)]{.newpage}
<img src="routeurAjoutNIC.png" alt="modesimulation" style="display: block; margin-left: auto; margin-right:auto; width:40%; " />

>Note : Chaque interface réseau du routeur doit avoir sa propre IP  

Choisissons les IP suivantes comme interfaces réseau:

* Interface 1 vers Switch 1: 192.168.0.1
* Interface 2 vers Switch 2 : 192.168.1.1
* Interface 3 vers Serveur DNS : 192.168.2.1

**Exercice:**

1. Configurer chacun des sous-réseaux (celui du switch 1, celui du switch 2, et celui du Serveur DNS) ainsi que le Routeur qui les relie, de sorte que:  
   Chaque hôte (cela inclut les Serveurs Web et Serveur DNS) de chacun de ces sous-réseaux doit être configuré avec un **masque de sous-réseau 255.255.255.0**  
   Chaque hôte de l'un de ces deux sous-réseaux doit pouvoir *pinger* vers :
   * n'importe quel autre hôte de son même sous-réseau
   * vers tout hôte d'un **autre** sous-réseau
   * vers le Serveur Web
   * vers le Serveur DNS
   * Astuce : On pourra par exemple, veiller à configurer chaque hôte avec sa ***passerelle par défaut*** (de sortie du sous-réseau) ou *** (Standard) Gateway*** (=adresse IP du routeur, vu de l'intérieur du sous-réseau)
2. Configurer chacun des hôtes (de chacun des sous-réseaux) avec l'IP du Serveur DNS
3. Tester les connexions avec des **pings** de tout hôte vers tout hôte (de n'importe quel sous-réseau). Faites des **ipconfig** sur chaque poste hôte pour vérifier les IP et configs sur chaque poste
4 Faire un **traceroute**  dans le **Command Line**, pour déterminer le **nombre de hops** (de **sauts**) à partir d'un hôte quelconque vers un autre, ainsi que la route suivie. Syntaxe : traceroute IP (de destination)
5. Configurer sur le Serveur Web:
  * les logiciels nécessaires
  * une page web d'accueil nommée ***index.html***. Personnalisez-là. Avec une image personnalisée. A partir du poste 192.168.0.10, Tester l'accès au site via un Webbrowser, vers l'URL 192.168.1.50
  * **Remarque :** Si votre image ne s'affiche pas, c'est qu'elle est (probablemnt) tropp lourde, traitez là avec un logiciel de retouche d'image (Conseillé : *Gimp*)
6. Configurer le réseau, de sorte que le site sur le Serveur Web, soit accessible via un webbrowser à partir de tout autre poste, via l'URL 192.168.1.50
7. Configurer le Serveur DNS, de sorte que le site précédent soit accessible via l'URL **www.nsi.com**, à partir de n'importe quel poste.
8. Ajouter un serveur Web dans le réseau du switch 1:
  * IP : 192.168.0.50
  * nom du site: **www.newnsi.com**
  * contenant un site dont la page d'accueil se nomme *index.html*, à personnaliser (sans image)
  * accessible depuis n'importe quel hôte/ordinateur portable.
<img src="reseauWanServeurWeb2.png" alt="modesimulation" style="display: block; margin-left: auto; margin-right:auto; width:50%; " />
9. Ajouter un nouveau (sous-)réseau sur le routeur:
  * contenant 3 Hôtes (ordis portables) connectés via un **switch 3**
  * tous configurés en DHCP, dont les IPs automatiques sont comprises:
    * entre 192.168.3.100
    * et 192.168.3.200
  * le serveur DHCP a une adresse IP **statique** 192.168.3.10
  * chaque hôte sait que le Serveur DNS a pour IP: 192.168.2.10

    Vous devriez voir quelque chose comme suit:
<img src="reseauWanDHCP.png" alt="modesimulation" style="display: block; margin-left: auto; margin-right:auto; width:50%; " />
Tester toutes les connexions, vers et depuis les postes du réseau du switch 3

10. Rappeler ce qu'est une **table ARP**, et déterminez la table **ARP** contenue dans un hôte quelconque
<span style="color:red;">
La **table ARP, Address Resolution Protocol** ou **cache ARP**, est une table d'association de couples (Adresse IP, Adresse MAC) stockée dans chaque ordinateur. Cette table ARP résume en quelque sorte tous les hôtes connus, à un moment donné, par un certain hôte (un ordinateur précis).  
</span>

Internet Address (IP) | HWAddress / Pḧysical Address (Adresse MAC)
------------ | -------------
192.168.xx.xx | c0:07:3c:d9:4f:02
192.168.xx.xx | c0:07:3c:d9:4f:02

<span style="color:red;">
Pour visualiser la table ARP d'un hôte sur filius, lancer le Command Line, et taper la commande:  
</span>

```bash
root /> arp
```
<span style="color:red;">
Vous devriez voir quelque chose comme ceci:

<img src="Corrigé_PartieII_tableARP.jpg" alt="modesimulation" style="display: block; margin-left: auto; margin-right:auto; width:50%; " />

</span>

11. ***pinger*** d'un hôte vers un autre hôte ***du même réseau***, et vérifier que la table **ARP** a bien été augmentée d'une entrée (votre ping)
12. ***pinger*** vers un hôte d'***un autre réseau***. La table **ARP** a-t-elle augmentée d'une entrée? Expliquer pourquoi il NE s'agit PAS de l'adresse IP locale de l'hôte de destination.  
<span style="color:red;">
**La table ARP a-t-elle augmentée d'une entrée?**  
OUI, elle a été augmentée de l'adresse IP de l'interface réseau de la passerelle par défaut (routeur).  
**Expliquer pourquoi il NE s'agit PAS de l'adresse IP locale de l'hôte de destination:**  
Sinon on risquerait d'avoir un conflit entre deux adresses IP locales de classe C: e.g. deux adresses de la forme 192.168.0.10 pourraient être en conflit, une telle adresse dans le sous-réseau &, et une autre dans le sous-réseau 2.
</span>

**Travail Final:**  
Sauvegarder votre travail sous **plusieursReseaux_VotreNOM_Prenom_Classe.fls**