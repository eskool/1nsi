# 1NSI : TD - Simuler un Inter-Réseau avec le logiciel *Filius*

!!! info
    La version (française) de Filius utilisée dans ce TD est $\geq$ v1.11.0.

## Introduction

Ce TD s'intéresse à :

* la construction et la configuration d'un inter-réseau
* Tous les Hôtes sont connectés via un **switch** (nullement besoin de routeur)  
* De manière **statique** :
    * On configure les Hôtes
    * On configure un Serveur DNS
    * On configure un Serveur Générique
    * On configure un Serveur Web
* De manière **dynamique**, en DHCP, on recommence et ajuste les configurations

## Connecter & Configurer plusieurs Réseaux, connectés via un/des routeurs

On souhaite créer et configurer plusieurs réseaux de la manière suivante:
<img src="./img/reseau_WAN.png" alt="modesimulation" style="display: block; margin-left: auto; margin-right:auto; width:80%; " />

**Question :** Combien y-a-t-il de réseaux dans cette configuration/topologie?

Le routeur doit être configuré de sorte qu'il dispose de $3$ **interfaces locales / NIC - Network Interface Card**, càd de $3$ **cartes réseaux**  
Vous pouvez configurer ceci de deux manières:

* ou bien, **DURANT LE GLISSER-DÉPOSER** du routeur (si vous savez combien il vous en faut à l'avance...). Modifier le nombre d'interfaces réseaux/NIC (2 = valeur par défaut) à **3**:
<img src="./img/routeurNbNIC.png" alt="modesimulation" style="display: block; margin-left: auto; margin-right:auto; width:70%; " />

* ou bien, **APRÈS** avoir déposé le routeur (s'il vous manque des Interfaces, après-coup), (et, ici, après avoir ajouté deux câbles) en mode **Conception**, en double-cliquant sur le routeur:
    * cliquer sur **Gérer les connexions**
<img src="./img/routeurAjoutNICManageConnections.png" alt="modesimulation" style="display: block; margin-left: auto; margin-right:auto; width:100%; " />
    * puis, cliquer sur le "**+**" (en bas à droite) pour ajouter une **interface**/**NIC**, vous devriez voir ceci (ensuite cliquer sur [Close)]
<img src="./img/routeurAjoutNIC.png" alt="modesimulation" style="display: block; margin-left: auto; margin-right:auto; width:80%; " />

!!! warning
    Chaque interface réseau du routeur doit avoir sa propre IP  

Choisissons les IP suivantes comme interfaces réseau:

* Interface 1 vers Switch 1: 192.168.0.1
* Interface 2 vers Switch 2 : 192.168.1.1
* Interface 3 vers Serveur DNS : 192.168.2.1

!!! ex
    1°) Configurer chacun des sous-réseaux (celui du switch 1, celui du switch 2, et celui du Serveur DNS) ainsi que le Routeur qui les relie, de sorte que:  

    * Chaque hôte (y compris les Serveurs Web et Serveur DNS), de chacun de ces sous-réseaux, doit être configuré avec un **masque de sous-réseau 255.255.255.0**  
    * Chaque hôte, de l'un de ces deux sous-réseaux (celui du Switch 1 et celui du Switch 2), doit pouvoir *pinger* vers :
        * n'importe quel autre hôte de son même sous-réseau
        * vers tout hôte d'un **autre** sous-réseau
        * vers le Serveur Web
        * vers le Serveur DNS
    !!! hint "Aide"
        On pensera à configurer sur chaque hôte, la ***Passerelle (par défaut)*** / ***(Standard) Gateway*** pour sortir du réseau (=adresse IP du routeur, vu de l'intérieur du sous-réseau)  

    2°) Configurer chacun des hôtes (de chacun des sous-réseaux) avec l'IP du Serveur DNS  
    3°) Tester les connexions avec des **pings** de tout hôte vers tout hôte (de n'importe quel sous-réseau). Faites des **ipconfig** sur chaque poste hôte pour vérifier les IP et configs sur chaque poste  
    4°) Faire un **traceroute** en **Ligne de commande**, pour déterminer le **nombre de hops** (de **sauts**) à partir d'un hôte quelconque vers un autre, ainsi que la route suivie. Syntaxe : traceroute IP (de destination)  
    5°) Configurer sur le Serveur Web:

    * les logiciels nécessaires
    * une page web d'accueil nommée ***index.html***. Personnalisez-là. Avec une image personnalisée. A partir du poste 192.168.0.10, Tester l'accès au site via un Navigateur web, vers l'URL 192.168.1.50
    * **Remarque :** Si votre image ne s'affiche pas, c'est qu'elle est (probablement) trop lourde, traitez là avec un logiciel de retouche d'image (Conseillé : *Gimp*)

    6°) Configurer le réseau, de sorte que le site sur le Serveur Web, soit accessible via un Navigateur web à partir de tout autre poste, via l'URL 192.168.1.50  
    7°) Configurer le Serveur DNS, de sorte que le site précédent soit accessible via l'URL **www.nsi.com**, depuis n'importe quel poste.  
    8°) Ajouter un serveur Web dans le réseau du switch 1 :

    * IP : 192.168.0.50
    * nom du site: **www.newnsi.com**
    * contenant un site dont la page d'accueil se nomme *index.html*, à personnaliser (sans image)
    * accessible depuis n'importe quel hôte/ordinateur portable.
    <img src="./img/reseauWanServeurWeb2.png" alt="modesimulation" style="display: block; margin-left: auto; margin-right:auto; width:90%; " />  

    9°) Ajouter un nouveau (sous-)réseau sur le routeur:

    * contenant 3 Hôtes (ordis portables) connectés via un **switch 3**
    * tous configurés en DHCP, dont les IPs automatiques sont comprises:
        * entre 192.168.3.100
        * et 192.168.3.200
    * le serveur DHCP a une adresse IP **statique** 192.168.3.10
    * chaque hôte sait que le Serveur DNS a pour IP: 192.168.2.10

    Vous devriez voir quelque chose comme suit:
    <img src="./img/reseauWanDHCP.png" alt="modesimulation" style="display: block; margin-left: auto; margin-right:auto; width:90%; " />
    Tester toutes les connexions, vers et depuis les postes du réseau du switch 3

    * Rappeler ce qu'est une **table ARP**, et déterminez la table **ARP** contenue dans un hôte quelconque
    * ***pinger*** d'un hôte vers un autre hôte ***du même réseau***, et vérifier que la table **ARP** a bien été augmentée d'une entrée (votre ping)
    * ***pinger*** vers un hôte d'***un autre réseau***. La table **ARP** a-t-elle augmentée d'une entrée? Expliquer pourquoi il NE s'agit PAS de l'adresse IP locale de l'hôte de destination.

    **Travail Final:**  
    Sauvegarder votre travail sous **plusieursReseaux_VotreNOM_Prenom_Classe.fls**