# 1NSI : Exercices Listes et Tableaux

Une **liste de (sous-)listes** peut-être appelée un **Tableau** de valeurs (2D), ou **Table** de valeurs. On pourrait généraliser à plusieurs dimensions.

!!! ex "Parcourir les éléments d'un tableau (2D)"

    Soit `l = [[1,2,3,4],[5,6,7,8],[9,10,11,12]]`

    1°) <bd>Parcours Largeur 1</bd> Écrire un algorithme qui parcoure **"en largeur"** d'abord de **gauche à droite** puis **de haut en bas**, chacun des éléments de chacune des sous-listes (de la liste `l`), en l'affichant dans le Terminal.  
    :bulb: On pourra utiliser deux boucles `for` imbriquées.  

    ![Parcours Largeur 1](../img/parcoursLargeur1.png){.center}
    
    2°) <bd>Parcours Largeur 2</bd> Modifier l'algorithme précédent pour parcourir **"en largeur"** d'abord mais de **droite à gauche** et **de haut en bas**, chacun des éléments de chacune des sous-listes (de la liste `l`), donc tout à l'envers par rapport à la question précédente :  
    
    ![Parcours Largeur 2](../img/parcoursLargeur2.png){.center}
    
    3°) <bd>Parcours Profondeur 1</bd> Parcourir **"en profondeur"** d'abord **de haut en bas** et de **gauche à droite**, chacun des éléments de chacune des sous-listes (de la liste `l`) :  
    
    ![Parcours Profondeur 1](../img/parcoursProfondeur1.png){.center}
    
    4°) <bd>Parcours Profondeur 2</bd> Parcourir **"en profondeur"** d'abord **de bas en haut** et de **gauche à droite**, chacun des éléments de chacune des sous-listes (de la liste `l`), donc tout à l'envers par rapport à la question précédente :  
    
    ![Parcours Profondeur 2](../img/parcoursProfondeur2.png){.center}

!!! ex "Générer un Tableau (2D) Aléatoire"
    Créer une fonction `creer_tableau_alea(n:int,a:int,b:int)` qui :

    * reçoit en entrée trois nombres entiers: `n`, `a` et `b`
    * renvoie en sortie un tableau (une liste de (sous-)listes) tel que:
        * la taille du tableau vaut $n \times n$, càd que chaque sous-liste admet une taille `n`, et il existe `n` sous-listes
        * dont les valeurs sont toutes des entiers aléatoires compris entre `a` et `b`

