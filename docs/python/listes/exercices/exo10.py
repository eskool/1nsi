from math import sqrt

def somme(l:list)->float:
    S = 0
    for el in l:
        S += el
    return S

def somme_coeff(l:list, c:list)->float:
    S = 0
    for i in range(len(l)):
        S += c[i]*l[i]
    return S

def sommeCarres(l:list, c:list)->float:
    S = 0
    for i in range(len(l)):
        S += c[i]*( l[i] - moyenne_coeff(l,c) )**2
    return S

# def variance(l:list, c:list)->float:
#     num = sommeCarres(l, c)
#     denom = somme(c)
#     return num / denom

def variance(l:list, c:list)->float:
    return sommeCarres(l, c) / somme(c)

def ecart_type(l:list, c:list)->float:
    return sqrt(variance(l,c))

def moyenne(l:list)->float:
    return somme(l) / len(l)

def moyenne_coeff(l:list, c:list)->float:
    return somme_coeff(l, c) / somme(c)

l=[0,15,20]
c=[4,1,2]
V = variance(l,c)
print("Variance=", V)
sigma = ecart_type(l, c)
print("Ecart Type=", sigma)
