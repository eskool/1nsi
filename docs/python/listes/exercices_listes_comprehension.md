# 1NSI : Exercices sur les Listes en Compréhensions

Dans tous les exercices suivants, on utilisera une compréhension de liste pour générer les listes demandées.

!!! ex
    1. `[1,3,5,7,...,143]`
    2. `[0,5,10,15,...,125]`
    3. `[1,2,4,5,..,12,14,...,1000]`
    liste des nombres de 1 à 1000, dans laquelle on a systématiquement enlevé tous les nombres entiers terminant par `3`
    4. `[1,4,3,16,5,36,...,99]`
        mettre au carré seulement les nombres pairs
    5. `['a','aa','a','aa',...,'a','aa']` avec 40 éléments dans la liste

!!! ex
    1. `[10,20,30,40,...,170]`
    2. `[2,4,6,8,...,138]`

!!! ex
    Générer la liste

    ```python
    [[1,2,3,4,5],
    [6,7,8,9,10],
    [11,12,13,14,15],
    [16,17,18,19,20],
    [21,22,23,24,25]
    ]
    ```

!!! ex
    1. avec une compréhension de liste ET grâce au module `random`, générer une liste aléatoire de 100 nombres compris entre 1 et 10 (inclus).
    On stockera la liste ainsi obtenue dans la variable `listeInitiale`
    2. grâce à une compréhension de liste, filtrer la `listeInitiale` de sorte que la variable `nouvelleListe` contienne les éléments de la `listeInitiale` *privée des éléments inférieurs ou égaux à 3*.
    3. avec une compréhension de liste, créer une liste de 10 sous-listes, chacune d'entre elles étant :

    * obtenue par compréhension de (sous-)listes
    * composée de 10 nombres aléatoires entre 1 et 50


