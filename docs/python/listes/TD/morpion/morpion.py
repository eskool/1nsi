X = 1
O = 2

def affiche():
    for line in g:
        print(line)
    print("---------")

def joue(pion, i, j):
    assert (type(pion) is int) and (type(i) is int) and (type(j) is int), "Erreur: (pion,i, j) doivent être entier"
    assert 0 <= pion <= 2, "Erreur: le pion doit valoir 0, 1 ou 2"
    assert 0<=i<=2, f"Erreur: {i} ne vaut pas 0, 1 ou 2"
    assert 0<=j<=2, f"Erreur: {j} ne vaut pas 0, 1 ou 2"
    assert g[i][j] == 0, f"Erreur: Case {(i,j)} Non vide"
    g[i][j] = pion

def nbPionDansLigne(pion:int, i:int)->int:
    c = 0
    for cellule in ligne(i):
        if cellule == pion:
            c += 1
    return c

def nbPionDansColonne(pion:int, j:int)->int:
    c = 0
    for cellule in colonne(j):
        if cellule == pion:
            c += 1
    return c

def nbPionDiagPrincipale(pion:int)->int:
    c = 0
    for cellule in diagPrincipale():
        if cellule == pion:
            c += 1
    return c

def nbPionDiagSecondaire(pion:int)->int:
    c = 0
    for cellule in diagSecondaire():
        if cellule == pion:
            c += 1
    return c

def ligne(i:int)->list:
    return g[i]

def colonne(j:int)->list:
    col = []
    for i in range(len(g)):
        col.append(g[i][j])
    return col

def diagPrincipale():
    diag = []
    diag.append(g[0][0])
    diag.append(g[1][1])
    diag.append(g[2][2])
    return diag
    # return [g[0][0], g[1][1], g[2][2]]

def diagSecondaire():
    diag = []
    diag.append(g[2][0])
    diag.append(g[1][1])
    diag.append(g[0][2])
    return diag
    # return [g[2][0], g[1][1], g[0][2]]

def gagne(pion:int)->bool:
    assert (type(pion) is int), f"Erreur: {pion} doivent être entier"
    assert 1 <= pion <= 2, f"Erreur: {pion} doit valoir 1 ou 2"
    if diagPrincipale() == [pion, pion, pion]:
        return True
    if diagSecondaire() == [pion, pion, pion]:
        return True
    for i in range(len(g)):
        if ligne(i) == [pion, pion, pion]:
            return True
    for j in range(len(g)):
        if colonne(j) == [pion, pion, pion]:
            return True
    return False

def estSurDiagPrincipale(i:int, j:int)->bool:
    assert (type(i) is int) and (type(j) is int), f"Erreur {i} et {j} doivent être entiers"
    assert 0<=i<=2 and 0<=j<=2, f"Erreur: {i} et {j} doivent être compris entre 0 et 2"
    if i == j: # (0,0), (1,1) et (2,2) fonctionnent
        return True
    else:
        return False
    # return i == j

def estSurDiagSecondaire(i:int, j:int)->bool:
    assert (type(i) is int) and (type(j) is int), f"Erreur {i} et {j} doinvent être entiers"
    assert 0<=i<=2 and 0<=j<=2, f"Erreur: {i} et {j} doivent être compris entre 0 et 2"
    if i + j == 2:  # (2,0), (1,1) et (0,2) fonctionnent
        return True
    else:
        return False
    # return i+j == 2

def interetPositionCellule(i:int, j:int)->int:
    assert (type(i) is int) and (type(j) is int), f"Erreur {i} et {j} doinvent être entiers"
    assert 0<=i<=2 and 0<=j<=2, f"Erreur: {i} et {j} doivent être compris entre 0 et 2"
    chemin = 2
    if estSurDiagPrincipale(i, j):
        chemin += 1
    if estSurDiagSecondaire(i, j):
        chemin += 1
    return chemin

def interetPositionGrille()


g = [[X, X, X],
     [X, 0, 0],
     [O, 0, X]]

affiche()
print("X a gagné ? ", gagne(X))
print("O a gagné ? ", gagne(O))

















































def interetPositionCellule(i:int,j:int)->int:
    assert (type(i) is int) and (type(j) is int), f"Erreur {i} et {j} doinvent être entiers"
    assert 0<=i<=2 and 0<=j<=2, f"Erreur: {i} et {j} doivent être compris entre 0 et 2"
    chemin = 2
    if estSurDiagPrincipale(i, j):
        chemin += 1
