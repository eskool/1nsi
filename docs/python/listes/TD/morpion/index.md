# TD Morpion

On initialise la grille vide du morpion par la liste suivante:

```python
g = [[0, 0, 0],
     [0, 0, 0],
     [0, 0, 0]]
```

On va supposer dans ce TD qu'un **joueur 1** (Humain) joue contre un **joueur 2** (la machine, ou IA).
On adopte la convention suivante, pour les nombres entrés dans une cellule :

* `0` dans une case indique que cette case est vide : elle n'a jamais été jouée par personne
* `1` dans une case représente une croix "X" (joueur 1)
* `2` dans une case représente un cercle "O" (joueur 2, qui sera une IA - Intelligence Artificielle)

Remarque:
On peut intialiser les variables suivantes:

```python
X = 1
O = 2
```

1°) Créer une fonction `affiche()->None` qui affiche dans le Terminal la grille du morpion sous forme de lignes et de colonnes

2°) Créer une fonction `ligne(i:int)->list` telle que:

* elle reçoive en entrée un numéro `i` de ligne
* elle renvoie en sortie la ligne `i` sous forme de liste

3°) Créer une fonction `colonne(j:int)->list` telle que:

* elle reçoive en entrée un numéro `j` de colonne
* elle renvoie en sortie la colonne `j` sous forme d'une liste

4°) Créer une fonction `diagPrincipale()->list` telle que:

* Elle ne reçoit aucun paramètre en entrée
* Elle renvoie en sortie la liste de tous les éléments de la diagonale Principale (d'en haut à gauche vers en bas à droite)

5°) Créer une fonction `diagSecondaire()->list` telle que:

* Elle ne reçoit aucun paramètre en entrée
* Elle renvoie en sortie la liste de tous les éléments de la diagonale Secondaire (d'en bas à gauche vers en haut à droite)

6°) Créer une fonction `joue(joueur:int,i:int,j:int)->None` qui :

* reçoit en entrée le numéro du `joueur` qui joue, 
* qui place la valeur correspondante contenue dans la variable `joueur`, au point de coordonnées `(i,j)` :
     * Si `joueur=1`, cela veut dire que c'est le joueur 1 qui joue : on place un `X` (càd un `1`) dans la grille, au point de coordonnées `(i,j)`
     * Si `joueur=2`, cela veut dire que c'est le joueur 2 qui joue : on place un `O` (càd un `2`) dans la grille, au point de coordonnées `(i,j)`

Remarque : cette fonction ne renvoie rien : on parle donc d'une **procédure** plutôt que d'une **fonction**

7°) Grâce à l'instruction `assert`, créer tous les tests nécessaires dans la fonction `joue(joueur, i, j)` précédente

8°) Créer une fonction `nbPionDansLigne(pion:int,i:int)->int` telle que :

* elle reçoit en entrée une variable `pion` (la cible à rechercher) et une variable `i` pour la ligne (où l'on fait la recherche), ets
* qui renvoie en sortie le nombre d'occurences de `pion` à la ligne `i`

9°) Créer une fonction `nbPionDansColonne(pion:int,j:int)->int` telle que :

* elle reçoit en entrée une variable `pion` (à rechercher) et une variable `j` pour la colonne (où l'on fait la recherche), et
* qui renvoie en sortie le nombre d'occurences de `pion` à la colonne `j`

10°) Créer une fonction `nbPionDiagPrincipale(pion:int)->int` telle que :

* elle reçoit en entrée une variable `pion` (à rechercher) sur la Diagonale Principale (où l'on fait la recherche), et
* qui renvoie en sortie le nombre d'occurences de `pion` sur la Diagonale Principale

11°) Créer une fonction `nbPionDiagSecondaire(pion:int)->int` telle que :

* elle reçoit en entrée une variable `pion` (à rechercher) sur la Diagonale Secondaire (où l'on fait la recherche), et
* qui renvoie en sortie le nombre d'occurences de `pion` sur la Diagonale Secondaire

12°) Créer une fonction `gagne(pion:int)->bool` telle que:

* elle reçoit en entrée le numéro du `pion` à vérifier
* elle renvoie en sortie un booléen:
     * `True` si le `pion` a gagné
     * `False` sinon

13°) Créer une fonction `estSurDiagPrincipale(i:int, j:int)->bool` telle que:

* Elle reçoit les coordonnées `(i,j)` d'une cellule comme paramètre en entrée
* Elle renvoie un booléen :
     * `True` si la cellule `(i,j)` appartient à la Diagonale Principale
     * `False` sinon

14°) Créer une fonction `estSurDiagSecondaire(i:int, j:int)->bool` telle que:

* Elle reçoit les coordonnées `(i,j)` d'une cellule comme paramètre en entrée
* Elle renvoie un booléen :
     * `True` si la cellule `(i,j)` appartient à la Diagonale Secondaire
     * `False` sinon

15°) Créer une fonction `interetPositionCellule(i:int,j:int)->int` telle que :

* elle reçoit en entrée les coordonnées (i,j) d'une cellule
* elle renvoie en sortie un nombre entier qui modélise l'intérêt (pour le joueur suivant) de joueur dans la cellule `(i,j)`

<!-- 16°) Créer une fonction `interetPositionGrille()->list` telle que :

* elle ne reçoit aucun paramètre en entrée
* elle remplit une grille `gInteretPosition` (une liste de sous-liste) initiale de sorte que: Pour chaque couple de coordonnées `(i,j)`, `gInteretPosition[i][j]` contient l'intérêt (lié à sa position) pour le prochain joeur de joueur la celulle (i,j) -->

17°) Créer une fonction `interetStategiqueCellule(i:int,j:int)->int` telle que:

* elle reçoit en entrée les coordonnées `(i,j)` d'une cellule
* elle renvoie en sortie un `score:int` total de la cellule, qui modélise l' ***intérêt Stratégique*** de Jouer (son prochain coup) dans cette cellule.  
     * Pour chaque chemin auquel appartient la cellule `(i,j)`, On pourra calculer **le `score` du chemin** par le nombre de pions qu'à déjà joué l'IA sur ce chemin
     * Pour la cellule `(i,j)`, le **score total de la cellule `(i,j)`** sera défini par la somme de tous scores de tous les chemins passant par la cellule `(i,j)`
     * En outre, si l'un des chemins passant par la cellule contient déjà deux pions humains, alors le `score` total de la cellule, sera recalculé en `score=100`
     * En outre, si l'un des chemins passant par la cellule contient déjà deux pions robotiques (ceux de l'IA), alors le `score` total de la cellule, sera recalculé en `score=100`

18°) Créer une fonction `interetTotal()->int` qui initialise une grille `gInteret`, dont chaque cellule contient la somme, pour chaque cellule, de son intérêt de Position, et de son intérêt Stratégique