# 1NSI : Chiffrement de César

Ce TD traite du Chiffrement de César.
Créer un fichier `cesar.py`

1. Créer une fonction `majCaractere(c:str)->str` telle que:

* elle reçoit en entrée un caractère `c`
* et qui renvoie en sortie le même caractère mais en majuscule

2. Créer une fonction `maj(message:str)->str` telle que :

* elle reçoit en entrée un message en minuscules
* elle renvoie le même message, mais en majuscules

3. Créer une fonction `decale(c:str,d:int)` telle que :

* elle accepte en entrée un caractère `c` et un entier de décalage `d` avec la convention suivante:
    * si `d>=0` alors le caractère `c` doit être décalé de `d` crans vers la droite
    * si `d<0` alors le caractère `c` doit être décalé de `-d` (>0) crans vers la gauche
* elle renvoie en sortie le caractère `c` qui a été décalé comme décrit ci-dessus.





