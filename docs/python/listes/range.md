# 1NSI : la fonction `range()`

## Avec UN Paramètre: `range(n:int)`

La fonction native `range(n:int)` renvoie une liste d'entiers :

* entre `0` (inclus) et 
* et `n` (NON inclus), donc jusqu'à `n-1` inclus


```python linenums="0"
# Dans un Interpréteur Python:
>>> range(10)
# L'interpréteur affiche une information peu claire (quand on ne connaît pas)
range(0,10) 
# L'utilisation de list() force l'affichage de la liste dans le Terminal
>>> list(range(10))
[0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
>>> list(range(15))
[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14]
```

En tout, il y aura donc <enc style="margin-left:0.3em;margin-right:0.3em;">$n$</enc> éléments dans `range(n)`.  
Détail du calcul : $(n-1)-0+1=n$

<env>Remarque</env> Pour visualiser la liste dans un Terminal, il a fallu utiliser le mot-clé `list()`, mais en pratique, il n'y aura pas besoin d'ajouter le mot-clé `list()` pour utiliser `range()` dans des algorithmes: En effet, Python est capable de savoir ce que contient la liste générée par `range()`, sans avoir nullement besoin de l'afficher dans un Terminal.

!!! exp "d'Utilisation avec la boucle `for`"
    === "Code"
        ```python linenums="0"
        for i in range(10):
          print(i)
        ```
    === "Sortie Terminal"
        ```python linenums="0"
        0
        1
        2
        3
        4
        5
        6
        7
        8
        9
        ```

## Avec DEUX Paramètres: `range(n:int, m:int)`

La fonction native `range(n:int, m:int)` renvoie une liste d'entiers :

* entre `n` (inclus) et 
* et `m` (NON inclus), donc jusqu'à `m-1` inclus

```python linenums="0"
>>> list(range(12, 19))
[12, 13, 14, 15, 16, 17, 18]
>>> list(range(14, 23))
[14, 15, 16, 17, 18, 19, 20, 21, 22]
```

En tout, il y aura donc <enc style="margin-left:0.3em;margin-right:0.3em;">$m-n$</enc> éléments dans `range(n, m)`.  
Détail du calcul: $(m-1)-n+1=m-n$

!!! exp "d'Utilisation avec la boucle `for`"
    === "Code"
        ```python linenums="0"
        for i in range(12,19):
          print(i)
        ```
    === "Sortie Terminal"
        ```python linenums="0"
        12
        13
        14
        15
        16
        17
        18
        ```
## Avec TROIS Paramètres: `range(n:int, m:int, p:int)`

La fonction native `range(n:int, m:int, p:int)` renvoie une liste d'entiers :

* entre `n` (inclus) et 
* et `m` (NON inclus), donc (potentiellement) jusqu'à `m-1` inclus
* avec un **pas** ou **step** de `p` (càd tous les `p` entiers - en sautant `p` entiers)

```python linenums="0"
>>> list(range(8, 23, 2))
[8, 10, 12, 14, 16, 18, 20, 22]
>>> list(range(8, 23, 3))
[8, 11, 14, 17, 20]
>>> list(range(14, 25, 4))
[14, 18, 22]
```

En tout, il y aura donc <enc style="margin-left:0.3em;margin-right:0.3em;">$\lceil \dfrac{m-n}{p} \rceil$</enc> éléments dans `range(n, m, p)`.  
Détail du calcul: $\lceil \dfrac {(m-1)-n+1}{p} \rceil = \lceil \dfrac {m-n}{p} \rceil$

!!! def "Partie Entière supérieure"
    La notation $\lceil x \rceil$ désigne la <bred>partie entière supérieure</bred> du réel $x$, càd l'entier directement supérieur à $x$.  
    Exemples: $\lceil 2,01 \rceil = 3$ et $\lceil 5,2 \rceil = 6$ et $\lceil 4 \rceil = 4$

!!! exp "d'Utilisation avec la boucle `for`"
    === "Code"
        ```python linenums="0"
        for i in range(8,23,2):
          print(i)
        ```
    === "Sortie Terminal"
        ```python linenums="0"
        8
        10
        12
        14
        16
        18
        20
        22
        ```