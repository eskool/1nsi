# TD 1337 / leet speak

## Introduction Historique

Le <bred>leet speak</bred> (en leet speak : `1337 5|*34|<`), de l'anglais « elite speak » :uk: (littéralement, « langage des initiés / de l'élite »), est un système d'écriture utilisant les **caractères ASCII** d'une manière peu compréhensible pour le néophyte (appelé `n00b` et déclinaisons) pour s'en démarquer. On considère ainsi le leet speak comme une forme particulière d'**[ASCII Art](https://fr.wikipedia.org/wiki/Art_ASCII)**.

Le *leet speak* trouve son origine à la fin des années 1980, sous l'impulsion des programmeurs informatiques sur les __*bulletin board systems (BBS)*__, populaires entre la fin des années 1970 et la 1ère moitié des années 1990, qui sont des serveurs connectés via ou plusieurs modems, et équipés d'un logiciel offrant :

* des services d'échanges de messages
* de stockage et d'échange de fichiers
* de jeux

Initialement inventé par les professionnels de la sécurité pour échanger et partager des infos et de fichiers en ligne, et plus particulièrement pour lutter contre les hackers.., ces derniers s'emparent néanmoins progressivement du concept, étant quelquefois bannis de certains BBS, et vont lui donner ses lettres de noblesse. Le **Leet Speak** a donc principalement été utilisé par les **hackers**, les **codeurs**, les **spammeurs** (car il passe les filtres, usuellement) et les **programmeurs**. Il a depuis gagné du terrain dans la communauté des **gamers**.

Ainsi, Il existe plusieurs sortes de BBS :

* le **professionnel**, qui offre des services à ses clients (téléchargement de correctifs, forum d'assistance technique, etc).
* l'**amateur**, géré par des particuliers passionnés comme passe-temps notamment par les demomakers.
* le **pirate**, où s'échangeaient les fichiers illégaux sur la piraterie téléphonique et informatique.
* le **ludique**, site de discussion à thème, généralement axé sur une trame de jeu de rôle.

La plupart des BBS amateurs ont aujourd'hui disparu. De nos jours, de nouveaux BBS ont néanmoins vu le jour et restent joignables par les protocoles **telnet** (non sécurisé, ancien), **SSH** (**S**ecure **SH**ell, :+1: ) ou *GNUtella*.

On pourra trouver une liste partielle de BBS existants sur [cette page](https://fr.wikipedia.org/wiki/Bulletin_board_system)

On y trouve quelques noms devenus emblématiques aujourd'hui : `PLaNeT MaRS`, `GoTCHa`, etc..

Aller Plus loin : Sur [cette page - leet Story-](https://www.technewsworld.com/story/47607.html) de technewsworld.com

# L'Alphabet *leet* ou `1337`

## Plusieurs niveaux de Codage : de `1337` à `31337`

Le **leet speak** est un langage **non standardisé**, à plusieurs formes/niveaux de codages différents, c’est-à-dire qu'il y a plusieurs façons de coder un même texte en *leet*. Cela vient du fait qu'il existe plusieurs équivalents à la plupart des lettres de l'alphabet.
Par exemple, **leet speak** peut s'écrire :

* `L33T 5P34K` en **codage de base** : des Chiffres et des Lettres. Au minimum :
    * `E` remplacé par `3`
    * `O` remplacé par `0`
    * `I` et `L` remplacé par `1`
    * `S` remplacé par `5`
    * `T` remplacé par `7`
* `1337 5p34k` en **codage léger** : des chiffres des lettres en minuscules
* `1337 5|*34|<` en **codage normal** ; des chiffres, quelques symboles sans symboles diacritiques
* `£33‡ šρ3@ķ` en **codage moyen** : des chiffres, quelques symbboles y compris des symboles diacritiques
* `|_ 33¯|¯ _/¯|°3/-\|<` en **codage élevé, ou `31337` ou `uber 1337`** : uniquement des symboles divers (mais pas les signes diacritiques) et des chiffres

## Un langage atypique

* l'alphabet **leet speak** n'est **pas réellement standardisé** : **les exemples suivants figurent parmi les plus classiques, mais n'ont aucune prétention d'exhaustivité**.
* certains mots sont **systématiquement & intentionnellement mal épelés/orthographiés** :
    * `Warez` pour dire `Softwares` sous entendu piratés ou crackés
    * `t3h` ou `teh` pour `the`
    * `n00b` pour `newbie`
    * `H4x0r` ou `Haxxor` pour `Hacker`, etc..
    * `b&` pour banned / banni (d'un Forum, etc..)
* certains symboles sont **polysémiques** : ils peuvent signifier plusieurs choses différentes selon les contextes.
* Résumé : en `1337`, on fait un peu ce qu'on veut... donc un peu n'importe quoi... Pas vraiment le monde on l'on applique des "règles" prétablies, et où les appliquerait

## Alphabet `1337` / leet 

### Alphabet `1337` : Lettres -> Nombres/Symboles

<!-- for pdf export -->
<!-- <div style="font-family: Source Code Pro;font-size:6px;"> -->
<div style="font-family: Source Code Pro;font-size:10px;">

|A|B|C|D|E|F|G|H|I|J|K|L|M|N|O|P|Q|R|S|T|U|V|W|X|Y|Z|
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
| 4 <br/> @ <br/> /-\ <br/> /\_\ <br/> /\ <br/> \|-\ <br/> Д <br/> aye <br/> ∂ <br/> ^ <br/> q <br/> a | 8 <br/> 6 <br/> 13 <br/> \|3 <br/> \]3 <br/> \|} <br/> \|\: <br/> 18 <br/> \|8 <br/> \]8 <br/> \|o <br/> lo <br/> j3 <br/> !3 <br/> (3 <br/> /3 <br/> )3 <br/> ß <br/> P>  <br/> в <br/> ь | $<$ <br/> (  <br/> { <br/> [ <br/> ¢ <br/> © | \|) <br/> \|} <br/> \|]<br/> [) <br/> \|> <br/> \|o <br/> ) <br/> I> <br/> ? <br/> T) <br/> 0 <br/> </ | 3 <br/> [- <br/> \|=-<br/> & <br/> $\in$ <br/> € <br/> £ <br/> ₤ <br/> ë | \|= <br/> /= <br/> (= <br/> \|# <br/> \|"<br/> ƒ <br/> ph | 6 <br/> 9 <br/> [+ <br/> & <br/> (\_+ <br/> C- <br/> gee <br/> (γ, | 4 <br/> # <br/> \|-\| <br/> \|=\| <br/> /-/ <br/> [-] <br/> [=] <br/> ]-[ <br/> [-] <br/> ]-[ <br/> )-( <br/> (-) <br/> {-} <br/> {=} <br/> }{ <br/> }-{ <br/> :-: <br/> \|~\| <br/> ]~[ <br/> I+I <br/> н <br/> ? <br/> hèch | 1 <br/> ! <br/> ¡ <br/> \| <br/> \]\[ <br/> ] <br/> : <br/> eye <br/> 3y3 | \_\| <br/> \_/ <br/> \_7 <br/> \_) <br/> \_] <br/> \_} <br/> ¿ <br/> </ <br/> (/ <br/> ʝ <br/> ; | X <br/> \|< <br/> 1< <br/> \|{ <br/> \|( <br/> ɮ <br/> < <br/> \|\“ | 1 <br/> \| <br/> 1_ <br/> \|_ <br/> \]\[\_ <br/> \|. <br/> £ <br/> ℓ | 44 <br/> 3 <br/> \|\\/\| <br/> /\\/\\ <br/>/X\ <br/> /\|/\| <br/> (\\/) <br/> /\|\\ <br/> ^^ <br/> /^^\ <br/> \|^^\| <br/> //. <br/> .\\\\ <br/> \/\/\/ <br/> \|v\| <br/> [V] <br/> {V} <br/> (V) <br/> (u) | 11 <br/> \|\\\| <br/> /\\/ <br/> /\|/ <br/> /V <br/> \|V <br/> ^/ <br/> (\\) <br/> ]\\[ <br/> [\\] <br/> \][\\]\[ <br/> {\\} <br/> <\\> <br/> // <br/> ^ <br/> [] <br/> <span style="font-size:1.4em;">И</span> <br/> <span style="font-size:1.7em;">₪</span> | 0 <br/> () <br/> [] <br/> {} <br/> <> <br/> * <br/> $\varnothing$ <br/> $\Theta$ <br/> oh <br/> ¤ <br/> ° <br/> ([]) | \|* <br/> /* <br/> \|o <br/> \|O <br/> \|º <br/> \|> <br/> \|" <br/> \|D <br/> []D <br/> \|7 <br/> ? <br/> <span style="font-size:1.4em;">¶</span> <br/> p | (\_,) <br/> (,) <br/> ()\_ <br/> O, <br/> O\_ <br/> 0\_ <br/> °\| <br/> <\| <br/> 0. | \|2 <br/> 12 <br/> /2 <br/> \|² <br/> 2 <br/> \|? <br/> \|^ <br/> .- <br/> ,- <br/> lz <br/> [z <br/> ® <br/> Я <br/> ʁ <br/> \|°\ | 5 <br/> $ <br/> § <br/> z <br/> ehs <br/> es <br/> _/¯ | 7 <br/> + <br/> 7\` <br/> -\|- <br/> \~\|\~ <br/> 1 <br/> '\|' <br/> \'\]\[\' <br/> † <br/> ¯\|¯ | \|\_\| <br/> \\\_\\ <br/> /\_/ <br/> \\\_/ <br/> (\_) <br/> [\_] <br/> {\_} <br/> v <br/> L\| <br/> µ <br/> J | \\/ <br/> \\\\// <br/> 1/ <br/> \|/ <br/> o\|o | 3 <br/> \\/\\/ <br/> \|/\\\| <br/> vv <br/> '// <br/> \\\\\` <br/> \\^/ <br/> \\X/ <br/> (n) <br/> \\V/ <br/> \\\|/ <br/> \\ \_\| \_ / <br/> \\\_:\_/ <br/> \`^/ <br/> \\./ <br/> Ш <br/> ɰ | % <br/> * <br/> >< <br/> )( <br/> }{ <br/> Ж <br/> ecks <br/> × <br/> 8  | \`/ <br/> '/ <br/> Ч <br/> j <br/> ¥ <br/> Ψ <br/> $\varphi$ <br/> λ | 2 <br/> 7\_ <br/>  >\_ <br/> ~/\_ <br/> % <br/> =/= <br/> -\\\_ <br/> '/\_ |

</div>

### Alphabet `1337` : Nombres -> Lettres

<div style="font-family: Source Code Pro;font-size:12px;">

| 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12 | 13 <br/> 18 | 44 |
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
| O <br/> D <br/> Q | I <br/> L <br/> T | Z <br/> R | E <br/> e <br/> m <br/> w <br/> $\omega$ <br/> $\in$ <br/> $\varepsilon$ <br/> $\cap\cap$ | h <br/> A <br/> y | S | b <br/> G | T <br/> j <br/> L | B <br/> X | g | IO <br/> LO <br/> 7O | N | R | B | M |

</div>

## Juste pour le fun!

Il existe une version de Google pour les Hackers... qui parle le 1337:

Page Google : https://www.google.com/?hl=xx-hacker

# A vous de jouer : `/\ \/()(_)_/¯ |)3 _|()/_/3|2 !!`

## Comprendre le `1337`

Traduire les phrases suivantes en Anglais :uk: ou en Français :fr: :

* En codage normal:

    * `3><pl0|7` <rep> exploit </rep>
    * `3xp|.0|+` <rep> exploit </rep>
    * `)30|\||\|3 4|\||\|é3` <rep> Bonne Année </rep>
    * `8391NN32` <rep> beginner </rep>
    * `3><|>3Rt` <rep> expert </rep>
    * `n00b13` <rep> newbie </rep>
    * `h4x0rz` <rep> hacker </rep>
    * `c3n50red` <rep> censored </rep>
    * `M1C|2050|=7` <rep> Microsoft </rep>
    * `K4713 12 7h3 w1|\| @ P00L` <rep> Katie is the win at pool : Katie est une très bonne joueuse de Pool </rep>
    * `|\|0\/\/ ¥0µ @|2€ |2€@|)¥ 70 |-|@\/€ ƒµ|\| \/\/¡7|-| £€€7!` <rep> Now You are ready to have fun with leet! </rep>

* En codage élevé:

    * `1 4/\/\ 7|-|3 |<33/o3|2 ()|" 7|-|3 \_//\/1\/3|253` <rep> I am the keeper of the universe </rep>
    * `|-|^><()|z` <rep> hacker </rep>
    *  `|)/-\\/1|)` <rep> David </rep>
    * `|\|0\/\/ 15 7|-| 71M3` <rep> Now is the time </rep>
    * `1 4/\/\ 7|-|3 |<33/o3|2 ()|" 7|-|3 \_//\/1\/3|253` <rep> I am the keeper of the universe </rep>

## Parler le `1337` : Écris ton Prénom + Nom!

1. Écris ton **Prénom**, puis ton **Nom de famille**, en `1337` :

    a. en Codage de base
    b. en Codage normal
    c. en Codage élevé

2. Défie ton voisin ! Ecris un mot, ou une phrase, en `1337`, et lance un défi à ton voisin pour le décoder :metal:

## Coder le `1337` en Python

En choisissant de manière aléatoire parmi l'un des symboles possibles pour représenter une même lettre en `1337`, créer une fonction `leet(texte:str)->str` qui :

* reçoit en entrée un paramètre contenant un `texte`
* renvoie en sortie une chaîne de caractère `texteLeet` représentant une traduction possible du `texte` en `1337`

On pourra utiliser astucieusement un **dictionnaire**.

