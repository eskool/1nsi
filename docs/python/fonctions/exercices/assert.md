# Exercices Assertions

!!! def
    `assert` est une instruction python permettant de vérifier/faire l'assertion que l'algorithme se trouve dans une bonne situation donnée, SINON assert lève le message d'erreur donné sous forme de chaîne de caractère.
    ```python
    # assert TEST_A_VERIFIER, "(SINON) AFFICHER CE MESSAGE"
    assert x>0, "x est NEGATIF, mais il DOIT être POSITIF"
    ```

!!! ex "Des Nombres"
    Dans chacune des situations suivantes, dans chaque fonction considérée, écrire l'assertion demandée:

    1. On se donne la fonction `racine_carre(x:float)->float` qui calcule la racine carrée $\sqrt x$ d'un nombre `x`.
    Assertion/Vérification demandée : `x` doit être positif (ou nul)
    2. On se donne la fonction `inverse(x:float)->float` qui calcule l'inverse $\displaystyle \frac 1x$ d'un nombre `x`.
    Assertion/Vérification demandée : `x` doit être non nul
    3. On se donne la fonction `divise_par_7(x:int)->int` qui calcule la division par 7 d'un nombre `x`.
    Assertion/Vérification demandée : `x` doit être un multiple de `7`
    4. On se donne la fonction `divise_par(x:int,n:int)->int` qui calcule la division par `n` d'un nombre `x`.
    Assertion/Vérification demandée : `x` doit être un multiple de `n`
    5. On se donne la fonction `note(x:float)->None` qui reçoit une note `x`, (et qui par la suite, sera par exemple amenée à la stocker dans une Base de Donnée).
    Assertion/Vérification demandée : `x` doit être comprise entre `0` et `20` inclus
    6. On se donne la fonction `repartition(n:int)->bool` qui vérifie si OUI (True) ou Non (False) le nombre n contient autant de `1` que de `2`

!!! ex "Des Lettres"

    1. Soit `nom_fichier(filename:str)->None` une fonction qui reçoit en entrée une variable `filename` contenant le nom d'un fichier.
    Assertions demandées :
        * `filename` doit être le nom d'un fichier finissant par `".txt"`
        * `filename` doit contenir au moins 4 caractères en plus de l'extension `".txt"`
        * `filename` doit contenir au moins 2 chiffres
