# 1NSI : Exercices Fonctions

!!! ex "contient"
    Ecrire une fonction `contient_mot(mot:str, phrase:str)->bool` qui prend en entrée deux arguments `mot` et `phrase`, et qui renvoie en sortie:

    * `True` si le caractère appartient à la phrase
    * `False` sinon

!!! ex "minorite (en âge)"
    Ecrire une fonction `est_majeur(anneeNaissance:int)->bool` qui prend en entrée un seul argument `anneeNaissance:int`, et qui renvoie en sortie:

    * `True` si tu es majeur
    * `False` sinon

!!! ex "aleatoire"
    Dans cet exercice, on poura importer la fonction `randint` du module `random` de la bibliothèque standard de Python.  
    Écrire une fonction `alea5(a,b)` telle que :

    * elle prend en entrée deux nombres entiers `a` et `b`
    * et qui renvoie en sortie une liste de 5 nombres entiers aléatoires entre `a` et `b`

!!! ex "marche aléatoire (version 1)"
    Dans cet exercice, on poura importer la fonction `randint` du module `random` de la bibliothèque standard de Python.  
    Une mouche est se déplace aléatoirement sur un axe à une dimension, en effectuant des **sauts aléatoires** (d'une longueur aléatoire, entre 0 et 10 pas, vers la gauche et/ou vers la droite, aléatoirement). Initialement, la mouche se situe sur la position `0` de l'axe horizontal.  
    Écrire une fonction `marche()` qui simule le déplacement de la mouche durant 5 sauts. La fonction `marche()` est telle que :

    * elle ne prend aucun argument en entrée
    * elle affiche la nouvelle position de la mouche (un entier) après chacun des 5 sauts
    * elle renvoie en sortie le nombre entier vérifiant la position finale (après 5 sauts)







