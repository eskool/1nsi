# 1NSI : Installation de Python et Choix Technologiques

Pour programmer en Python sur notre ordinateur, en local, nous avons principalement besoin d'y installer deux logiciels :

* un Éditeur de texte : en fait **VSCODE**, qui est un Éditeur de texte avec de nombreuses fonctionnalités supplémentaires orientées développement
* le Langage de Programmation Python : en fait **Anaconda**, qui contient Python et de nombreux modules d'extension

## Éditeur de Texte vs IDE

!!! def "Éditeur de Texte"
    Un <bred>éditeur de texte</bred> est un logiciel permettant la **lecture** et la **modification** de textes

Un éditeur de texte est souvent très simple, il ne dispose pas, en général, de fonctionnalités avancées. Dans notre cas, nous utiliserons un (même) éditeur de texte pour lire et modifier des **instructions en Langage Python** bien sûr, mais aussi **pour tout autre langage au programme (HTML, CSS, JS - Javascript, PHP, SQL, etc..)**

<env>Exemple</env>  

* Sur **Windows**, le <bred>bloc-note</bred> :fr: ou <bred>notepad</bred> :gb: est un éditeur de texte extrêmement simple, et inclus par défaut dans toutes les versions de Windows.
* Sur **MacOS** et/ou **Linux**, <bred>nano</bred> est un éditeur de texte en ligne de commande assez simple
* un éditeur de texte très simple (par ex. le *bloc-note* sur Windows) est néanmoins (volontairement) limité :
    * pas de **numérotation** des lignes
    * pas de **coloration syntaxique** (instructions de code en couleurs, pour aider à la lecture et la compréhension du code)
    * pas d'**auto-complétion** (aide à la syntaxe dans un langage donné, en complétant automatiquement la ligne de code à votre place : cela vous permet d'une part, de gagner du temps de frappe, et d'autre part, de ne pas hésiter entre plusieurs syntaxes, donc vous faire encore gagner du temps dans l'écriture du code, et vous faire éviter des erreurs simples, etc..)
    * pas de **mode multiligne** (modification simultanée de plusieurs lignes)
    * pas d'interface simple pour **exécuter un programme** écrit dans un certain langage, depuis cet éditeur de texte
    etc..

<env>Remarque</env>
Un **traitement de texte** (par ex. Microsoft *Word*, ou Libre Office *Writer*) est un éditeur de texte contenant en interne, des **fonctionnalités supplémentaires orientées bureautique**.

!!! def "IDE - Integrated Development Environment"
    Un <bred>Environnement de Développement Intégré (EDI)</bred> :fr: ou <bred>Integrated Developement Environment (IDE)</bred> :gb: est un éditeur de texte contenant en interne, des **fonctionnalités supplémentaires orientées développement (de logiciels, de pages web, etc..)**, classiquement :

    * **numérotation** des lignes
    * **coloration syntaxique** (instructions de code en couleurs, pour aider à la lecture et la compréhension du code)
    * **auto-complétion** (aide à la syntaxe dans un langage donné, en complétant automatiquement la ligne de code à votre place : cela vous permet d'une part, de gagner du temps de frappe, et d'autre part, de ne pas hésiter entre plusieurs syntaxes, donc vous faire encore gagner du temps dans l'écriture du code, et vous faire éviter des erreurs simples, etc..)
    * **mode multiligne** (modification simultanée de plusieurs lignes)
    * **débogage** ou **debugage** pour détecter et résoudre les **erreurs** dans le code source ou <bred>bugs</bred> :gb: (quelques rares fois <bred>bogues</bred> en :fr:)
    * interface simple pour **exécuter un programme** écrit dans un certain langage, depuis cet éditeur de texte
    etc..
    * **Interface en Ligne de Commande (CLI)** ou **Terminal**

C'est ce type de logiciel (un IDE) qu'il nous faut choisir.

### Notre Choix : VSCODE (ou VSCODIUM)

Il existe de nombreux éditeurs de textes ou IDEs, et chaque développeur a ses petites préférences, néanmoins, il nous faudra faire un choix par souci d'harmonisation des procédures : nous avons choisi de retenir le logiciel <bred>VSCODE - Visual Studio Code</bred> car il présente plusieurs avantages, qui justifient ce choix à nos yeux :

* Il est **totalement Gratuit**, bien que développé par la société **Microsoft** :us:
* Il est à **Code Source Ouvert** :fr: ou **OpenSource** :gb: :fr: sous **Licence MIT**.  
Néanmoins, selon l'adage "*Si c'est gratuit, c'est que vous êtes le produit*" (Tim Cook, patron d'Apple), le logiciel *VSCODE* disponible en téléchargement, est disponible sous **[cette Licence NON FLOSS](https://code.visualstudio.com/license)** (càd ni Libre, ni Open Source. 
[FLOSS $=$ <b>F</b>ree / <b>L</b>ibre <b>O</b>pen <b>S</b>ource <b>S</b>oftware $=$ Logiciel Libre, ou bien Open Source](https://fr.wikipedia.org/wiki/Free/Libre_Open_Source_Software) ) : VSCode contient notamment des **éléments de télémétrie/tracking** (des données sur vous et votre utilisation du logiciel).
* Il existe également une version **totalement OpenSource** de ce logiciel (sous License MIT), appelée <bred>VSCODIUM</bred> **SANS LES ÉLÉMENTS DE TÉLÉMÉTRIE** activés par défaut dans le produit équivalent (VSCode) de Microsoft.
* il est **multi-plateformes** : disponible sur Windows, MacOS et Linux
* il possède de **nombreuses extensions gratuites** (plug-ins) utiles au développement dans de nombreux langages
* il dispose d'une **très large communauté de développeurs** : c'est l'IDE préféré de nombreux développeurs

### Téléchargement et installation de VSCODE

#### Télécharger et Installer VSCODE

Télécharger VSCODE à cette adresse :  

* [Télécharger VSCODE](https://code.visualstudio.com/download) : https://code.visualstudio.com/download

Vous avez le choix entre les versions suivantes, **conseillées dans cet ordre** :

* **System installer** (si l'installation fonctionne bien pour vous) : cette version installera le logiciel **pour tous les utilisateurs de votre Système d'Exploitation** (s'il en existe plusieurs). Version 64 bits ou 32 bits (selon l'architecture de votre ordi: si vou sne savez pas quoi choisir, prenez 64 bits, et si l'installation plante, essayez plutôt du 32 bits)
* (sinon, choisissez) **User installer** (si la précédente installation ne fonctionne pas bien) : cette version installera le logiciel uniquement **pour l'utilisateur courant de votre Système d'Exploitation**.

#### Télécharger et Installer VSCODIUM

* [Télécharger VSCODIUM](https://vscodium.com/#install) : https://vscodium.com/#install
