# 1NSI : Installation de Python

## Plusieurs choix d'installation

Comme nous l'avons indiqué en Introduction, nous avons le choix (entre autres) entre télécharger et installer :

* Une version minimaliste de Python, qui contient la **[bibliothèque/librairie Standard de Python](https://docs.python.org/fr/3/library/index.html)**.  
Cf. le site officiel de Python : https://www.python.org/downloads/
* Une version améliorée de Python, appelée <bred>Anaconda</bred>, qui contient de nombreux modules / fonctionnalités supplémentaires  
Cf. le site : https://www.anaconda.com/products/individual-d

Le logiciel **Anaconda**, bien que (un peu) plus volumineux à installer sur notre disque dur, est **bien plus complet**, et contient des **fonctionnalités intéressantes qui pourront nous être utiles ponctuellement**, c'est pourquoi :

!!! note "Notre Choix"
    **Nous faisons le choix de préférer installer Anaconda**.

## Notre Choix : Anaconda

**Anaconda** est une version plus complète que la bibliothèque standard de python, il comprend notamment :

* la **bibliothèque/librairie standard Python**, qui rappelons-le comprend déjà en soi, de nombreuses fonctionnalités et modules
* de très nombreux **autres modules optionnels intéressants**, prêts à l'emploi, et qui pourront nous être utiles ponctuellement :
    * **Numpy** : pour le calcul matriciel
    * **Scipy** : pour le calcul scientifique
    * **Jupyter** : pour créer des (jupyter-)notebooks interactifs (cellules en markdown et/ou dans un langage de programmation)
    * **Spyder** : un autre (bon) éditeur de texte, qui est en fait un Environnement de Développement Intégré (IDE), mais spécifique à Python (ce n'est néanmoins pas le choix que nous ferons)
    * **pandas** : pour créer, gérer, visualiser et analyser des données (**Data Science**)
    * **matplotlib** : pour des représentations de données, notamment le tracé de courbes mathématiques et statistiques
    * **plotly** : pour des représentations de données interactivement : numériques/scientifiques 2D et 3D, financières, géographqiues (plans). Animations de graphiques.
    * **Pytorch** : pour l'Apprentissage Profond :fr: / Deep Learning :gb:. ceci dépasse un peu le niveau attendu de 1ère et Terminale TNSI...
    * **Scikit-learn** : pour l'Apprentissage Machine :fr: / Machine Learning (ML) :gb:. ceci dépasse un peu le niveau attendu de 1ère et Terminale TNSI...
    * etc...

## Installer Anaconda

### Sur Windows

Pour Télécharger l'**installeur Anaconda** pour **Windows** :

* Page : https://www.anaconda.com/products/individual-d
* Cliquer directement sur le bouton **Download** (car Windows est le choix par défaut)  
ou bien, de manière équivalente,  
    * cliquer sur le logo de **Windows** :fa-windows: (sous le bouton **Download**)
    * Choisir Python : **3.xx 64-bit Graphical Installer ($\approx$ 477MB)** (Actuellement Python 3.8)

### Sur MacOS

Pour Télécharger l'**installeur Anaconda** pour **Mac** :

* Page : https://www.anaconda.com/products/individual-d
* Cliquer sur le logo d'**Apple** :fa-apple: (sous le bouton **Download**)
* Choisir Python : **3.xx 64-bit Graphical Installer ($\approx$ 440MB)** (Actuellement Python 3.8)

### Sur Linux

* *Certaines* distributions Linux disposent d'un paquet **Anaconda**, auquel cas il convient de privilégier l'installation du paquet **Anaconda** via le gestionnaire de paquets de votre distribution.
* Sinon, si le paquet Anaconda n'existe pas pour votre distribution, **on peut toujours installer tous les paquets d'Anaconda séparément**, un par un (ou par petits groupes) :

    * prioritairement (si possible) : via le gestionnaire de paquets de votre distribution, pauets à chercher dans les dépôts de votre distribution
    * Sinon (au cas où il manque certains paquets dans les dépôts de votre distribution) : via l'utilisation d'un **gestionnaire de paquets Python** nommé `pip`, qui ne sert qu'à **installer des modules Python** sur votre système. Il faudra alors veiller à avoir préalablement installé `pip` via le gestionnaire de paquets de votre ditribution Linux






