# 1NSI : Tests de Comparaisons

## Opérateurs de Comparaison

Allo

<center>

| Opérateur | Signification | Exemple<br/>de test | Exemple de réponse |
|:-:|:-:|:-:|:-:|
| `<` | strictement inférieur à | `2<3`<br/>`3<1` | `True`<br/>`False` |
| `>` | strictement inférieur à | `5>3`<br>`3>1` | `True`<br/>`False` |
| `<=` | inférieur ou égal à | `4<=9`<br/>`5<=3` | `True`<br/>`False` |
| `>=` | supérieur ou égal à | `7>=1`<br/>`3>=8` | `True`<br/>`False` |
| `==` | est égal à | `2==4/2`<br/>`3==2` | `True`<br/>`False` |
| `!=` | non égal à | `2!=3`<br/>`2!=4/2` | `True`<br/>`False` |

</center>

## Opérateurs `not`, `and`, `or`

<center>

| Opérateur | Signification | Exemple<br/>de test | Exemple de réponse |
|:-:|:-:|:-:|:-:|
| `not` | non<br/>(le contraire de) | `not(2<3)` $\,\,$ `not 2<3`<br/>`not(3<1)` $\,\,$ `not 3<1` | `False`<br/>`True` |
| `and` | et | `1<2 and 3>=3`<br>`2<1 and 3==3`<br/>`4>=2 and 2==1` | `True`<br/>`False`<br/>`False` |
| `or` | ou | `1<2 or 3>=3`<br>`2<1 or 3==3`<br/>`4>=2 or 2==1`<br/>`4<2 or 2==1` | `True`<br/>`True`<br/>`True`<br/>`False` |

</center>

