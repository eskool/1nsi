# 1NSI : Quelques Fonctions Natives Intéressantes

## round

`round(x,n)` arrondit le flottant `x` à `n` chiffres après la virgule

```python
>>> round(8.6351, 2)
8.64
```