from PIL import Image
from math import sqrt

im = Image.open("plage.jpg")
im = im.convert("RGB")

width, height = im.size

def dist(x0,y0,x1,y1):
    return sqrt((x0-x1)**2+(y0-y1)**2)

for x in range(height):
    for y in range(width):
        if dist(x,y,256,256) > 200:
            r,g,b = im.getpixel((x,y))
            im.putpixel((x,y), (255,255,255))
im.save("plageModifieeDisqueBlanc.png")
im.show()