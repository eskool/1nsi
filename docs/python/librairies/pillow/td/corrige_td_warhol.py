from PIL import Image
from math import sqrt

im = Image.open("plage.jpg")
im = im.convert("RGB")

width, height = im.size

def fusion(listeNomsImages:list)->None: 
    im = Image.new(mode="RGB", size=(4*512, 512))
    n = -1
    for image in listeNomsImages:
        tempIm = Image.open(image).convert("RGB")
        width, height = tempIm.size
        n += 1
        for x in range(width):
            for y in range(height):
                rgb = tempIm.getpixel((x,y))
                im.putpixel((x+n*512,y), rgb)
    return im

# listeNomsImages = ["./rouge.png","./vert.png","./jaune.png","./bleu.png"]
listeNomsImages = ["./plageModifieeInversionRG.png",
        "./plageModifieeInversionRB.png",
        "./plageModifieeInversionGB.png",
        "./plageModifieePermuteCanaux.png"
        ]

im = fusion(listeNomsImages)

# im.save("plageFiltreCouleurWarhol.png")
im.save("plageWarhol.png")

im.show()