from PIL import Image
from math import sqrt

im = Image.open("plage.jpg")
im = im.convert("RGB")

width, height = im.size
a = 4

def couleur_moyenne_carre(x0,y0,a):
    mR, mG, mB = 0,0,0
    for x in range(x0, x0+a):
        for y in range(y0, y0+a):
            mR += im.getpixel((x,y))[0]
            mG += im.getpixel((x,y))[1]
            mB += im.getpixel((x,y))[2]
    mR = int(round(mR/a**2, 0))
    mG = int(round(mG/a**2, 0))
    mB = int(round(mB/a**2, 0))
    
    return (mR, mG, mB)

def pixelliser(a):
    for x in range(0,width,a):
        for y in range(0,height,a):
            r,g,b = couleur_moyenne_carre(x,y,a)
            for xx in range(x , x+a):
                for yy in range(y , y+a):
                    im.putpixel((xx,yy), (r,g,b))

pixelliser(a)
im.save(f"plageModifieePixellisation{a}.png")
im.show()