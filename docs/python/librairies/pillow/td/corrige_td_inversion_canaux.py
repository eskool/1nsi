from PIL import Image
from math import sqrt

im = Image.open("plage.jpg")
im = im.convert("RGB")

width, height = im.size
a = 32

def inverseRG():
    for x in range(width):
        for y in range(height):
            r,g,b = im.getpixel((x,y))
            im.putpixel((x,y), (g,r,b) )

def inverseRB():
    for x in range(width):
        for y in range(height):
            r,g,b = im.getpixel((x,y))
            im.putpixel((x,y), (b,g,r) )

def inverseGB():
    for x in range(width):
        for y in range(height):
            r,g,b = im.getpixel((x,y))
            im.putpixel((x,y), (r,b,g) )

def permuteCanaux():
    for x in range(width):
        for y in range(height):
            r,g,b = im.getpixel((x,y))
            im.putpixel((x,y), (g,b,r) )

# inverseRG()
# im.save("plageModifieeInversionRG.png")

# inverseRB()
# im.save("plageModifieeInversionRB.png")

# inverseGB()
# im.save("plageModifieeInversionGB.png")

permuteCanaux()
im.save("plageModifieePermutationCanaux.png")

im.show()