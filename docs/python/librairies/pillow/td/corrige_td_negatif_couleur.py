from PIL import Image
from math import sqrt

im = Image.open("plage.jpg")
im = im.convert("RGB")

width, height = im.size
a = 32

def negatif_couleur():
    for x in range(width):
        for y in range(height):
            r,g,b = im.getpixel((x,y))
            im.putpixel((x,y), (255-r,255-g,255-b) )

negatif_couleur()
im.save("plageModifieeNegatifCouleur.png")
im.show()