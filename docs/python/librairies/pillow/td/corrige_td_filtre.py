from PIL import Image
from math import sqrt

im = Image.open("plage.jpg")
im = im.convert("RGB")

width, height = im.size
a=32

def get_gris_moyen(xy):
    r,g,b = im.getpixel(xy)
    return int(round((r+g+b)/3,0))

def get_gris_nature(xy):
    r,g,b = im.getpixel(xy)
    return int(round(0.3*r+0.59*g+0.11*b,0))

def sepia(xy):
    return filtre(xy, (174,137,100))

def filtre(xy, rgb):
    r,g,b = im.getpixel(xy)
    # gr = get_gris_moyen(xy)
    gr = get_gris_nature(xy)
    R,G,B = int(round(rgb[0]/255*gr,0)), int(round(rgb[1]/255*gr,0)), int(round(rgb[2]/255*gr,0))
    return R,G,B

def colorer_gris_moyen():
    for x in range(width):
        for y in range(height):
            gr = get_gris_moyen((x,y))
            im.putpixel((x,y), (gr,gr,gr) )

def colorer_gris_nature():
    for x in range(width):
        for y in range(height):
            gr = get_gris_nature((x,y))
            im.putpixel((x,y), (gr,gr,gr) )

def colorier_sepia():
    for x in range(width):
        for y in range(height):
            gr = get_gris_nature((x,y))
            # r,g,b = im.getpixel((x,y))
            im.putpixel((x,y), sepia((x,y)) )

def colorier_filtre(rgb:tuple)->None:
    for x in range(width):
        for y in range(height):
            gr = get_gris_nature((x,y))
            im.putpixel((x,y), filtre((x,y), rgb) )

# def couleur_moyenne_carre(x0,y0,a):
#     mR, mG, mB = 0,0,0
#     for x in range(x0, x0+a):
#         for y in range(y0, y0+a):
#             mR += im.getpixel((x,y))[0]
#             mG += im.getpixel((x,y))[1]
#             mB += im.getpixel((x,y))[2]
#     mR = int(round(mR/a**2, 0))
#     mG = int(round(mG/a**2, 0))
#     mB = int(round(mB/a**2, 0))

#     return (mR, mG, mB)

# def pixelliser(a):
#     for x in range(0,width,a):
#         for y in range(0,height,a):
#             r,g,b = couleur_moyenne_carre(x,y,a)
#             for xx in range(x , x+a):
#                 for yy in range(y , y+a):
#                     im.putpixel((xx,yy), (r,g,b))

rgb = (255,255,0)
colorier_filtre(rgb)
im.save(f"plageModifieeFiltre_{rgb[0]}_{rgb[1]}_{rgb[2]}.png")
# pixelliser(a)
im.show()