# TD Pillow

Dans ce TD, il vous est proposé de travailler avec l'image carrée du voilier, de taille $512$x$512$ suivante :

![Voilier](./voilier.png)

Afin de mieux illustrer visuellement ce qu'il vous est demandé (le rendu à obtenir), nous utiliserons dans les consignes de ce TD, une autre image, celle d'une plage, de taille $512$x$512$.

![Plage](./plage.jpg)

Vous **pouvez** utiliser une autre image téléchargée sur internet, si cela vous amuse, mais :

* votre image **DOIT ÊTRE CARRÉE**.
* il faudra adapter certaines consignes afin qu'elles coincident avec votre situation

## Nuances de Gris

On souhaite convertir notre image de couleur en gris.

### Principe Général : Comment faire ?

Rappelons que la couleur $(R,G,B)$ d'un pixel est grise, si et seulement si ses composantes $R$, $G$ et $B$ de ce pixel vérifient :

<center><enc>$R = G = B$</enc></center>

Plusieurs méthodes sont imaginables. Inventez en une, en plus des deux suivantes...

### Niveaux de Gris par la moyenne

Une méthode, parmi d'autres, pour convertir un pixel $(R,G,B)$ d'une couleur quelconque, en un pixel gris dont les 3 composantes $(R', G', B')$ sont égales entre elles, consiste à calculer $R'$, $G'$ et $B'$ de la manière suivante :

<center><enc>$R' = V' = B' = \dfrac{R+V+B}{3}$</enc></center>

<bad>Question Demandée</bad>  

1. Créer une fonction `get_gris_moyen(xy:tuple)->int` qui :

    * reçoit en entrée les coordonnées `xy` (un tuple) d'un pixel
    * renvoie en sortie un tuple `(gr,gr,gr)` où `gr = R' = G' = B'` est le **gris moyenne** calculé par la formule de la moyenne **non coefficientée**, décrite ci-dessus

2. Créer une fonction `colorer_gris_moyen()` qui convertit l'image en gris, avec la méthode du **gris moyen** détaillée ci-dessus

!!! info "Rendu Attendu"
    ![Conversion en Gris, version Moyenne](./plageModifieeMoyenne.png)

### Niveaux de Gris Naturels, version Gimp/Photoshop

La méthode précédente a l'avantage de fonctionner, néanmoins, les graphistes s'accordent à penser que les couleurs grises sont plus belles, càd **plus proches de celles de la nature**, lorsque le mode de calcul du gris utilise **une moyenne coefficientée** (car le vert est dominant dans la nature), avec les coefficients suivants :

* ROUGE = $30\%$
* VERT = $59\%$
* BLEU = $11\%$

C'est ce que font les logiciels de retouche d'images, comme GIMP/Photoshop, pour convertir une image en couleur en gris.

Autrement dit, pour convertir un pixel $(R,G,B)$ d'une couleur quelconque, en un pixel gris, avec 3 composantes $(R', G', B')$ égales entre elles, On calcule $R', G'$ et $B'$ de la manière suivante :

<center><enc>$R' = V' = B' = \dfrac{0,30\times R+0,59\times V+0,11\times B}{0,30+0,59+0,11}=0,30\times R+0,59\times V+0,11\times B$</enc></center>

<bad>Question Demandée</bad>  

1. Créer une fonction `get_gris_nature(xy:tuple)->int` qui :

    * reçoit en entrée les coordonnées `xy` (un tuple) d'un pixel
    * renvoie en sortie un tuple `(gr,gr,gr)` où `gr = R' = G' = B'` est le **gris nature** calculé par la formule de la moyenne **coefficientée** avec les coefficients `(0.3,0.59,0.11)`, décrite ci-dessus

2. Créer une fonction `colorer_gris_nature()` qui convertit l'image en gris, avec la méthode du **gris nature** détaillée ci-dessus

!!! info "Rendu Attendu"
    ![Conversion en Gris, version Nature / Gimp / Photoshop](./plageModifieeNature.png)

### Négatif Noir & Blanc

Un **négatif noir et blanc** est une image dont les tons noirs et blancs (et gris) sont "inversés":

* les noirs deviennent blancs
* les blancs deviennent noirs
* etc.. les gris très clairs deviennent des gris très sombres, et réciproquement,
* etc..

<bad>Question Demandée</bad>  

1. Si on note`gris`, l'entier compris entre $0$ et $255$, qui représente la nuance de gris d'un pixel de l'image noir et blanc initiale, alors comment calculer la nouvelle nuance de gris `grisNegatif` (encore un entier entre $0$ et $255$) du même pixel pour le négatif noir et blanc (en fonction de la variable `gris` initiale) ?

2. Créer une fonction `negatif_noir_blanc()->None` qui utilise l'image en gris transformée à la question précédente, et qui transforme cette image grise en négatif noir et blanc.

!!! info "Rendu Attendu"
    ![Négatif Noir & Blanc](./plageModifieeNegatifNoirBlanc.png)

## Jeux de Couleurs

### Filtre de Couleur

On souhaite coloriser l'ensemble de l'image avec une certaine couleur. Le principe général consiste en :

* colorier chaque pixel en gris `(gr,gr,gr)` (en version Moyenne, ou en version nature/Gimp/Photoshop, peu importe), puis 
* coloriser chaque pixel gris via le **filtre** d'une certaine couleur `(R,G,B)` choisie à l'avance. On obtient ainsi la nouvelle couleur filtrée `(R',G',B')` de ce pixel grâce à la formule :

    * $R' = gr\times \dfrac{R}{255}$
    * $G' = gr\times \dfrac{G}{255}$
    * $B' = gr\times \dfrac{B}{255}$

<bad>Question Demandée</bad>  

1. Créer une fonction `filtre(xy:tuple, rgb:tuple)->tuple` telle que :

    * elle reçoit en entrée :
        * les coordonnées `(x,y)` du pixel (un tuple), ainsi que 
        * la couleur `(R,G,B)` du filtre à appliquer, et
    * elle renvoie en sortie la nouvelle couleur `(R',G',B')` (donc un tuple de 3 entiers entre 0 et 255) après avoir appliqué le **filtre**

2. Créer une fonction `colorier_filtre(rgb:tuple)->None` qui applique le filtre de couleur `(r,g,b)` à toute l'image grâce à la fonction `filtre(rgb)` de la question précédente

!!! info "Quelques Rendus Attendus"
    !!! col __50
        ![Filtre Couleur Rouge (255,0.4,0)](./plageModifieeFiltre_255_0.4_0.png)
    !!! col __50
        ![Filtre Couleur Vert (0,255,0)](./plageModifieeFiltre_0_255_0.png)
    !!! col __50
        ![Filtre Couleur Jaune (255,255,0)](./plageModifieeFiltre_255_255_0.png)
    !!! col __50
        ![Filtre Couleur Bleu Clair (0,255,255)](./plageModifieeFiltre_0_255_255.png)

### Fusion d'Images (Colorées)

Dans cette partie, on suppose que l'on dispose déjà de 4 images colorées (comme les précédentes) sauvegardées dans votre OS (dans le même répertoire que votre algorithme Python, pour simplifier) avec des noms différents.  

<bad>Question Demandée</bad>

Créer une fonction `fusion(listeNomsImages:list)->None` qui :

* reçoit en entrée une liste de 4 noms d'images déjà créées (de taille $512$x$512$)
* crée une nouvelle image `im` avec l'instruction `im = Image.new(mode="RGB", size=(4*512, 512))`
* fusionne les 4 images colorées dans une même image (par exemple les 4 images colorées créées précédemment, en les alignant toutes les 4 sur une même ligne).

!!! info "Rendu Attendu"
    ![Fusion de 4 Images](./plageFiltreCouleurWarhol.png)

### Filtre Sepia, ou Photographie vieillie

Amusez-vous à simuler artificiellement le vieillissement de vos photos, en coloriant votre photo avec une couleur de filtre particulière.

<bad>Question Demandée</bad>  

1. Créer une fonction `sepia(xy:tuple)->tuple` qui :

    * reçoit en entrée les coordonnées `xy` (un tuple) d'un pixel
    * renvoie en sortie les composantes `(R,G,B)` (un tuple) du pixel `xy` mais filtré avec la couleur **sepia**, càd la couleur `(174,137,100)`. On pourra utiliser la fonction `filtre()` déjà créée.

2. Créer une fonction `colorier_sepia()` qui convertit l'image en une image vieillie.

!!! info "Rendu Attendu"
    ![Vieillissement de Photo, Filtre Sepia](./plageModifieeSepia.png)

### Négatif Couleur

<bad>Question Demandée</bad>

1. Si on note `(R, G, B)`, les 3 entiers compris entre $0$ et $255$, qui représentent les composantes RGB de couleur d'un pixel de l'image initiale, alors comment calculer les nouvelles composantes de couleur `(rougeNegatif, vertNegatif, bleuNegatif)` (encore 3 entiers entre $0$ et $255$) du même pixel pour le négatif couleur (en fonction des valeurs `(R,G,B)` des composantes initiales) ?

2. En s'inspirant de la fonction négatif Noir & Blanc, créer une fonction `négatif_couleur()`, qui inverse (non pas le noir et le blanc, mais) la clarté de chacun des canaux R,G et B : ce que l'on pourrait appeler un **négatif couleur**.

!!! info "Rendu Attendu"
    ![Négatif Couleur](./plageModifieeNegatifCouleur.png)

### Inversion de Composantes/Canaux

<bad>Question Demandée</bad>  

1. Créer une fonction `inversionRG()` qui, pour chaque pixel de l'image :

    * permute les canaux Rouge (R) et Vert (G)
    * ne modifie pas le canal Bleu (B)

2. Créer une fonction `inversionRB()` qui, pour chaque pixel de l'image :

    * permute les canaux Rouge (R) et Bleu (B)
    * ne modifie pas le canal Vert (G)

3. Créer une fonction `inversionGB()` qui, pour chaque pixel de l'image :

    * permute les canaux Bleus (B) et Vert (G)
    * ne modifie pas le canal Rouge (R)

4. Créer une fonction `permuteCanaux()` qui, pour chaque pixel de l'image :

    * permute circulairement les canaux Rouge (R), Vert (G) et Bleu (B) :
        * le nouveau Rouge (R) prend comme valeur l'ancien Vert (G)
        * le nouveau Vert (G) prend comme valeur l'ancien Bleu (B)
        * le nouveau Bleu (B) prend comme valeur l'ancien Rouge (R)

!!! info "Quelques Rendus Attendus"
    !!! col __50
        ![Inversion des composantes R et G](./plageModifieeInversionRG.png)
    !!! col __50
        ![Inversion des composantes R et B](./plageModifieeInversionRB.png)
    !!! col __50
        ![Inversion des composantes G et B](./plageModifieeInversionGB.png)
    !!! col __50
        ![Permutation Circulaire des Canaux](./plageModifieePermutationCanaux.png)

### Oeuvre d'Art Andy Warhol

Dans cette partie, on suppose que l'on dispose déjà des 4 images (précédentes) inversant les canaux de couleur, sauvegardées dans votre OS (dans le même répertoire que votre algorithme Python, pour simplifier) avec des noms différents.  

<bad>Question Demandée</bad>

Utiliser les 4 images (précédentes) d'inversions de canaux, et la fonction `fusion(listeNomsImages:list)->None` pour fusionner les 4 images (précédentes) dont les canaux ont été inversés, et ainsi créer une Oeuvre d'Art, en l'honneur modeste mais sincère, à l'artiste américain [Andy Warhol](https://fr.wikipedia.org/wiki/Andy_Warhol) et sa célèbre oeuvre sur Marylin Monroe :

![Marylin Monroe, de l'artiste Andy Warhol](./marylin.webp){width=40%}

!!! info "Rendu Attendu"
    ![Oeuvre d'Art, en hommage à l'artiste américain Andy Warhol](./plageWarhol.png)

## Pixelisation

1. Créer une fonction `couleur_moyenne_carre(x0,y0,a)` qui :

    * reçoit en entrée :
        * les coordonnées `(x0,y0)` du point supérieur gauche d'un petit carré
        * le petit carré admet un côté de longueur `a`
    * renvoie en sortie la couleur moyenne, représentée par le triplet `(mR, mG, mB)` des moyennes sur chaque canal  sur le carré `(x0,y0,a)`, càd le carré de coin supérieur gauche `(x0,y0)` et de côté `a`:
        * `mR` est la moyenne du canal **Rouge** dans ce carré
        * `mG` est la moyenne du canal **Vert** dans ce carré
        * `mB` est la moyenne du canal **Bleu** dans ce carré

2. Créer une fonction `pixeliser(a:int)->None` qui :

    * remplace l'image du carré `(x,y,a)` par un carré de couleur uniforme (la même) `(mR, mG, mB)`

!!! info "Quelques Rendus Attendus"
    !!! col __50
        ![Pixellisation 4 px](./plageModifieePixellisation4.png)
    !!! col __50
        ![Pixellisation 8 px](./plageModifieePixellisation8.png)
    !!! col __50
        ![Pixellisation 16 px](./plageModifieePixellisation16.png)
    !!! col __50
        ![Pixellisation 32 px](./plageModifieePixellisation32.png)

3. On pourra vérifier qu'une image en nuances de gris pixellise en carrés gris

## Disque Central

### Disque Blanc

1. Créer une fonction `dist(x0:float,y0:float,x1:float,y1:float)->float` qui renvoie la distance (euclidienne) entre les deux points de coordonnées `(x0,y0)` et `(x1,y1)`. On pourra utiliser la fonction `sqrt` du module `math`
2. Créer un algorithme qui :

    * convertit l'image en RGB (si besoin)
    * ajoute un **Disque Blanc** de rayon `200px`, centré au milieu de l'image (autour dun point de coordonnées `(256,256)` )

!!! info "Rendu Attendu"
    ![Disque Blanc](./plageModifieeDisqueBlanc.png)

### Disque Transparent

Créer un algorithme qui :

* convertit l'image en RGBA
* ajoute un **Disque Transparent** de rayon `200px`, centré au milieu de l'image (autour dun point de coordonnées `(256,256)` )

!!! info "Rendu Attendu"
    ![Disque Transparent](./plageModifieeDisqueTransparent.png)
