from PIL import Image
from math import sqrt

im = Image.open("plageModifieeMoyenne.png")
im = im.convert("RGB")

width, height = im.size
a = 32

def get_gris_moyen(xy):
    r,g,b = im.getpixel(xy)
    return int(round((r+g+b)/3,0))

def get_gris_nature(xy):
    r,g,b = im.getpixel(xy)
    return int(round(0.3*r+0.59*g+0.11*b,0))

def colorer_gris_moyen():
    for x in range(width):
        for y in range(height):
            gr = get_gris_moyen((x,y))
            im.putpixel((x,y), (gr,gr,gr) )

def colorer_gris_nature():
    for x in range(width):
        for y in range(height):
            gr = get_gris_nature((x,y))
            im.putpixel((x,y), (gr,gr,gr) )

colorer_gris_moyen()
im.save("plageModifieeGrisMoyen.png")

# colorer_gris_nature()
# im.save("plageModifieeNature.png")

im.show()