# Documention Pillow

* **Pillow** est un fork de la librairie PIL initiale, proposé par **Jeffrey A. Clark (Alex)** et contributeurs. 
* **PIL** est la **<red>L</red>ibrairie d'<red>I</red>mages <red>P</red>ython** créée par **Fredrik Lundh** et contributeurs. En 2024, **Tidelift** est un sponsor officiel de **Pillow**.

**Pillow** offre un support pour différents formats de fichiers images tels que PPM, PNG, JPEG, GIF, TIFF et BMP.

## Charger, Afficher et Sauvegarder une image

### Charger avec `open()`

`open()` charge l'image en mémoire et permet de la stocker dans une variable Python (`im` dans notre cas), pour pouvoir la manipuler depuis le code Python.

```python
from PIL import Image
im = Image.open("voilier.png")
```

### Afficher la fenêtre avec `show()`

```python
from PIL import Image
im = Image.open("voilier.png")
im.show()
```

### Sauvegarder avec `save()`

```python
from PIL import Image
im = Image.open("voilier.png")
# .. modifications ..
Image.save("voilier.png")
```

## Taille et Format d'une image

```python
from PIL import Image
im = Image.open("voilier.png")
width, height = im.size
print(width)
print(height)

print(im.format)
im.show()
```

## Modes de Couleurs

### Modes de Couleurs

Avec Pillow, il existe plusieurs manières/**modes** pour stocker les couleurs :

| Mode | Description |
|:-:|:-:|
| 1 | pixel à 1-bit, Noir et Blanc |
| L | Pixels 8-bit, càd des entiers entre `0` et `255`, pour chacun représentant des Nuances de Gris |
| P | Pixels 8-bit, càd des entiers de `0` et `255`, chacun représentant une couleur, chacune d'entre elles étant traduite vers tout autre mode, via une palette de couleur |
| RGB | Pixels 3x8bit, couleurs vraies |
| RGBA | Pixels 4x8-bit, couleurs vraies avec transparence (canal Alpha) |
| CMYK | Pixels 4x8-bit couleurs séparées |
| HSV | Pixels 3x8-bit  Espace de couleurs Hue (Teinte) - Saturation - Valeur |

Pour vérifier le mode de couleur de votre image :

```python
from PIL import Image
im = Image.open("voilier.png")
print(im.mode)
```

Référence: [color modes](https://pillow.readthedocs.io/en/latest/handbook/concepts.html#modes)

### Conversions de Modes

Nous allons utiliser le mode "RGB". Si votre image n'est pas en RGB (ça peut arriver qu'elle soit dans un autre mode), alors il faut commencer par la convertir en RGB, grâce à:

```python
from PIL import Image
im = Image.open("voilier.png")
im = im.convert("RGB")
print(im.mode)
```

## Pixels

Un <red>pixel</red> est un <red>PICT</red>ure <red>EL</red>ement, un élément (élémentaire) d'une image, intuitivement un point le plus petit possible d'une image.
Un pixel :

* est repéré sur l'image par ses coordonnées `(x,y)`
* admet la couleur des **composantes R,G,B** lorsque le mode de couleur choisi est `"RGB"`
* éventuellement une quatrième **composante Alpha** (de transparence), lorsque le mode de couleur choisi est `"RGBA"`

### Lire un pixel en R,G,B

```python
from PIL import Image
im = Image.open("voilier.png")
im = im.convert("RGB")

x , y = 20, 50
# obtient les composantes R,G,B du pixel (x,y)
r,g,b = im.getpixel((x,y))
print(r,g,b)
```

### Modifier un pixel en R,B,G

```python
from PIL import Image
im = Image.open("voilier.png")
im = im.convert("RGB")

x , y = 20, 50
r,g,b = 200,100,50
# fixe les composantes R,G,B du pixel (x,y)
im.putpixel((x,y), (r,g,b))
im.save("voilierModife.png")
im.show()
```

### Modifier un pixel en R,G,B,A

```python
from PIL import Image
im = Image.open("voilier.png")
im = im.convert("RGBA")

x , y = 20, 50
r,g,b,a = 200,100,50,128
# fixe les composantes R,G,B, A du pixel (x,y)
im.putpixel((x,y), (r,g,b,a))
im.save("voilierModife.png")
im.show()
```

