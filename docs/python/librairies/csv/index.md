# Documentation module CSV de Python

Référence officielle : [cette page](https://docs.python.org/3/library/csv.html)

Le module `csv` est un module de la **bibliothèque standard** de **Python** permettant de lire et écrire dans les fichiers csv.

Pour toute la suite, vous aurez besoin d'un fichier CSV :  
Par exemple : [Observation météorologique historiques France (SYNOP)](https://public.opendatasoft.com/explore/dataset/donnees-synop-essentielles-omm/export/?flg=fr-fr&sort=date)

## Lire un fichier CSV

Après avoir importé le module `csv`, la méthode `csv.reader()` permet de lire toutes les lignes du fichier, sous forme de liste Python.

```python
import csv
csv.register_dialect('unix', delimiter=';', quoting=csv.QUOTE_NONE)

with open('temperatures.csv', newline='') as f:
    reader = csv.reader(f, "unix")
    for row in reader:
        print(row)
```

## Écrire dans un fichier CSV

```python
import csv
csv.register_dialect('unix', delimiter=';', quoting=csv.QUOTE_NONE)

with open('temperatures.csv', 'w', newline='') as f:
    writer = csv.writer(f)
    writer.writerows(['2025-01-01T00:00:00-93', '2025-01-01', '93', "Provence-Alpes-Côte d'Azur", '1.99', '19.99', '9.99'])
```
