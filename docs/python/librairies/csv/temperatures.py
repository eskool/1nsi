import csv
csv.register_dialect('unix', escapechar=';', delimiter=';', quoting=csv.QUOTE_NONE)

with open('temperatures.csv', 'w', newline='') as f:
    writer = csv.writer(f, "unix")
    writer.writerows(["2025-01-01T00:00:00-93;2025-01-01;93;Provence-Alpes-Côte d'Azur;1.99;19.99;9.99"])

with open('temperatures.csv', newline='') as f:
    reader = csv.reader(f, "unix")
    for row in reader:
        print(row)
