# Documentation Turtle

## Résumé Historique

**TURTLE** est un module de la bibliothèque standard de Python permettant de réaliser simplement des graphiques grâce à une **tortue** (visualisée par un **curseur** triangulaire) possédant un **crayon**, directement inspiré par :

* le langage **Logo** et sa tortue développé par **Wally Feurzeig**, **Seymour Papert** and **Cynthia Solomon** dès $1967$, 
* lui -même inspiré du Langage **Lisp** développé par **John McCarthy** en $1958$.

La tortue permet de dessiner des formes complexes en utilisant un programme qui répète des actions élémentaires :

<center><iframe src="https://trinket.io/embed/python/c0db7ac40f?outputOnly=true&runOption=run&start=result" width="300" height="300" frameborder="0" marginwidth="0" marginheight="0" allowfullscreen></iframe></center>

## Site Turtle en Ligne

Voici un site en ligne que vous pouvez utiliser comme solution alternative (en cas de problème d'utilisation du module Turtle en local) : [https://trinket.io/turtle](https://trinket.io/turtle)

## Documentation Turtle

### Documentation Officielle

[Documentation Officielle (:warning: en FrAnglais :warning:) du Module Turtle](https://docs.python.org/fr/3/library/turtle.html#turtle.color) 

### Etat initial

(En **mode standard**) La Tortue part de l'état initial suivant. Elle se trouve :

* au **centre de l'écran** : càd au **point de coordonnées** `(0,0)`.
* **orientée vers l'Est** : càd avec un **angle de $0°$** par rapport à l'axe des x. Autrement dit, avancer en avant de $10$ pas (ou pixels..), revient alors à aller vers l'Est de $10$ pas.
* en **position basse d'écriture** (le stylo est posé sur la feuille)
* Couleur: **Noir**
* Epaisseur de Trait : **1px**
* Logo de Tortue : **Icone Triangulaire**

On dispose du Repère Standard des mathématiques.

![Etat Initial de la Tortue](./etat_initial.png)

### Principe du Tracé

* Lorsque le **crayon est baissé**, et que la tortue de déplace, elle **dessine un trait** derrière son passage
* Lorsque le **crayon est remonté**, et que la tortue de déplace, elle **ne dessine aucun trait** derrière son passage

### Instructions de base

=== "Déplacements & Position"
    | **Intruction**	| **Description**  	|
    |:-:	|:-:	|
    | `forward(d)` ou `fd(d)`	| avancer de la distance $d$	|
    | `backward(d)` ou `bk(d)` ou `back(d)`	| reculer de la distance $d$	|
    | `left(a)` ou `lt(a)`	| pivoter à gauche de l'angle $a$ (en degrés)	|
    | `right(a)` ou `rt(a)`	| pivoter à droite de l'angle $a$ (en degrés)	|
    | `up()` ou `penup()` ou `pu()`	| Relève la pointe du crayon : ne dessine plus lorsque la tortue se déplace	|
    | `down()` ou `pendown()` ou `pd()`	| Baisse la pointe du crayon : dessine lorsque la tortue se déplace	|
    | `dot(t)` | Affiche un point de taille `t` à la position courante |
    | `goto(x,y)` ou `setpos(x,y)`	| aller au point de coordonnées ($x$,$y$), sans modifier l'angle	|
    | `position()` ou `pos()`	| Renvoie les coordonnées courantes ($x$, $y$) de la tortue.<br/>(en tant que vecteur Vec2d)<br/>`abs(pos())` donne la distance de $(0,0)$ à la position courante |
    | `setheading(a)` ou `seth(a)`	| Positionne la tortue avec un angle de $a$ degrés, par rapport à l'axe des x (orienté) |
    | `home()`	| repositionner la tortue au centre de la fenêtre.<br/>(Utile si la tortue est sortie de l'écran)	|

=== "Style des Tracés"
    | **Intruction**	| **Description**  	|
    |:-:	|:-:	|
    | `width(e)` ou `pensize(e)`	| fixer à $e$ l'épaisseur du trait (en nombre de pixels)	|
    | `color(c)`	| sélectionner la couleur $c$ pour les traits **ET/OU** pour le remplissage d'une figure (si elle est fermée, et si le mode remplissage est activé). Trois notations pour la couleur $c$ : <br><ul style="text-align:left;"><li>avec une chaîne prédéfinie : `"black"` (défaut), `"white"`, `"grey"`, `"pink"`, `"purple"`, `"blue"`, `"green"`, `"yellow"`, `"orange"`, `"red"`, `"brown"`, etc... Liste complète : [Couleurs Tk autorisées](https://www.plus2net.com/python/tkinter-colors.php)  </li> <li>ou avec la syntaxe RVB : `color(r,v,b)` avec $r$,$v$,$b\in[0;1]$. Exemple: `color(0.2,0.4,0.8)`</li> <li> ou avec la syntaxe Hexadécimale: `color("#RRGGBB")`. Exemple: `color("#1A2BF5")`</li></ul> 	|
    | `pencolor(c)`	| sélectionner la couleur $c$ (seulement) pour le trait	|
    | `Screen().bgcolor(c)` | Colorie le fond de la fenêtre graphique avec la couleur `c` (syntaxe idem que `color(c)` ) |
=== "Remplissage Formes & Couleurs"
    | **Intruction**	| **Description**  	|
    |:-:	|:-:	|
    | `fillcolor(c)`	| Définit la couleur $c$ pour le remplissage d'une Forme Fermée	|
    | `begin_fill()`	| Activer le mode remplissage		|
    | `end_fill()`	| Désactiver le mode remplissage.<br/>Remarquer que le remplissage ne commence que lorsque la commande `end_fill()` est rencontrée		|

    !!! remarque
        `abs(pos()) < 1` est une bonne condition pour vérifier que la tortue est bien revenue à sa position initiale (`home`)

=== "Cercles & Disques"
    | **Intruction**	| **Description**  	|
    |:-:	|:-:	|
    | `circle(r)`	| Trace un **cercle** de rayon $r$. <br/><ul style="text-align:left"><li>Si $r>0$, alors Le cercle est parcouru dans le sens Trigonométrique (=Anti-Horaire).</li><li>Si $r<0$, alors il est parcouru dans le sens contraire (sens Horaire)</li></ul>	|
    | `circle(r,a)`	| Trace un **arc de cercle** d'angle $a$ (en degrés) et de rayon $r$.<br/><ul><li>Si $r>0$, alors Les arcs de cercle sont parcourus dans le sens Trigonométrique (=Anti-Horaire).</li><li>Si $r<0$, alors ils sont parcourus dans le sens contraire (sens Horaire)</li></ul>	|
    | `dot(r)`	| tracer un **disque** de rayon $r$	|
=== "Écran et Fenêtre"
    | **Intruction**	| **Description**  	|
    |:-:	|:-:	|
    | `done()` ou `mainloop()` | maintient la fenêtre graphique ouverte, sans cela, elle ne fera qu'apparaître furtivement |
    | `setup(w,h)`	| ouvre une fenêtre de taille $w$x$h$	|
    | `window_width()` et `window_height()`	| renvoient la largeur et la hauteur de la fenêtre	|
    | `speed(s)`	| définir la vitesse de déplacement de la tortue, où $s$ est un paramètre défini par : <br/><ul style="text-align: left;"><li>une des chaînes: `"slowest"`, `"slow"`, `"normal"`, `"fast"`, `"fastest"`.</li><li>ou bien un nombre entier entre $1$ (le plus lent) et $10$ (le plus rapide). La valeur spéciale 0 est la plus rapide</li></ul>	|
    | `reset()`	| effacer l'écran, remettre la tortue au centre et réinitialiser ses paramètres	|
    | `title(t)`	| Donner le titre $t$ à la fenêtre de dessin	|
    | `bye()`	| refermer la fenêtre	|
    | `exitonclick()`	| refermer la fenêtre en cas de click	|


### Code Minimal Fonctionnel / MWE

Voici un exemple de <red>Code Minimal Fonctionnel</red> :fr: / <red>Minimal Working Example (MWE)</red> :gb: pour importer le module Turtle et pour l'utiliser :

=== "Méthode 1: `from turtle import *`"
    !!! col __50
        Code Simple/Manuel:

        ```python
        from turtle import *

        forward(100)    # avance de 100 pas/pixels
        right(90)       # tourne à droite de 90°
        forward(100)
        right(90)
        forward(100)
        right(90)
        forward(100)
        right(90)

        done()          # maintient la fenêtre graphique ouverte
        ```

        Code équivalent, :warning: **à préférer** :warning:, avec une boucle `for` :

        ```python
        from turtle import *

        for _ in range(4):
            forward(100)    # avance de 100 pas/pixels
            right(90)       # tourne à droite de 90°

        done()          # maintient la fenêtre graphique ouverte
        ```
    !!! col __50
        <center><iframe src="https://trinket.io/embed/python/e7c7803896?outputOnly=true&runOption=run" width="100%" height="500" frameborder="0" marginwidth="0" marginheight="0" allowfullscreen></iframe></center>
        
=== "Méthode 2: `import turtle as t`"
    !!! col __50
        Code Simple/Manuel:

        ```python
        import turtle as t

        t.forward(100)  # avance de 100 pas/pixels
        t.right(90)     # tourne à droite de 90°
        t.forward(100)
        t.right(90)
        t.forward(100)
        t.right(90)
        t.forward(100)
        t.right(90)

        t.done()        # maintient la fenêtre graphique ouverte
        ```

        Code équivalent, :warning: **à préférer** :warning:, avec une boucle `for` :

        ```python
        import turtle as t

        for _ in range(4):
            t.forward(100)  # avance de 100 pas/pixels
            t.right(90)     # tourne à droite de 90°

        done()          # maintient la fenêtre graphique ouverte
        ```
    !!! col __50
        <center><iframe src="https://trinket.io/embed/python/e7c7803896?outputOnly=true&runOption=run" width="100%" height="500" frameborder="0" marginwidth="0" marginheight="0" allowfullscreen></iframe></center>
=== "Méthode 3: `from turtle import forward, right, done`"
    !!! col __50
        Code Simple/Manuel:

        ```python
        from turtle import forward, right, done

        forward(100)    # avance de 100 pas/pixels
        right(90)       # tourne à droite de 90°
        forward(100)
        right(90)
        forward(100)
        right(90)
        forward(100)
        right(90)

        done()          # maintient la fenêtre graphique ouverte
        ```

        Code équivalent, :warning: **à préférer** :warning:, avec une boucle `for` :

        ```python
        from turtle import forward, right, done

        for _ in range(4):
            forward(100)  # avance de 100 pas/pixels
            right(90)     # tourne à droite de 90°

        done()          # maintient la fenêtre graphique ouverte
        ```
    !!! col __50
        <center><iframe src="https://trinket.io/embed/python/e7c7803896?outputOnly=true&runOption=run" width="100%" height="500" frameborder="0" marginwidth="0" marginheight="0" allowfullscreen></iframe></center>
