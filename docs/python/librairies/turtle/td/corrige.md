# <red>Corrigé</red> du TD Turtle

!!! warning "Consigne Générale"
    Chaque question de ce TD :
    
    * doit utiliser au moins une fois la boucle `for` (sauf si la question est trop simple..)
    * peut utiliser des fonctions (informatiques) dès que possible/souhaitable

## Traits & Figures

### Traits Simples

1. :rocket: Dessiner un Trait de Longueur **200px** :

    * en allant vers l'Est/la Droite
    * en allant vers l'Ouest/la Gauche
    * en allant vers le Nord/le Haut
    * en allant vers le Sud/le Bas

    !!! col __50
        <center><iframe src="https://trinket.io/embed/python/dd64769238" width="100%" height="600" frameborder="0" marginwidth="0" marginheight="0" allowfullscreen></iframe></center>  
    !!! col __50
        <center><iframe src="https://trinket.io/embed/python/60af696751" width="100%" height="600" frameborder="0" marginwidth="0" marginheight="0" allowfullscreen></iframe></center>
    !!! col __50
        <center><iframe src="https://trinket.io/embed/python/2a4caf806a" width="100%" height="600" frameborder="0" marginwidth="0" marginheight="0" allowfullscreen></iframe></center>  
    !!! col __50
        <center><iframe src="https://trinket.io/embed/python/0e408e714a" width="100%" height="600" frameborder="0" marginwidth="0" marginheight="0" allowfullscreen></iframe></center>

1. :rocket: Dessiner un trait horizontal, allant vers l'Est, de longueur totale **200px**, en pointillés, chacun de longueur **5px** (aussi bien le trait, ainsi que les espaces entre les traits)

    <center><iframe src="https://trinket.io/embed/python/f73599c2cb" width="100%" height="600" frameborder="0" marginwidth="0" marginheight="0" allowfullscreen></iframe></center>

1. :rocket: Dessiner un Trait de **200px** avec un angle de **30°** par rapport à l'axe des x :

    <center><iframe src="https://trinket.io/embed/python/6446700d55" width="100%" height="600" frameborder="0" marginwidth="0" marginheight="0" allowfullscreen></iframe></center>

### Escalier de 7 marches

:rocket: Dessiner un escalier avec 7 marches, dont chaque marche admet :

* une hauteur de **18px**
* une longeur horizontale de **27px**

<center><iframe src="https://trinket.io/embed/python/57b2ed1548" width="100%" height="600" frameborder="0" marginwidth="0" marginheight="0" allowfullscreen></iframe></center>

### Fleur

:rocket::rocket: Dessiner une **Fleur** dont les pétales sont des Losanges

Utiliser le modèle de base suivant pour construire Une Fleur comme ci-contre, de sorte que :

!!! col __50
    * Chaque ligne admet une longueur de **60px**
    * les angles sont soit de **30°** (angle1), soit de **150°** (angle2)
!!! col __50
    ![Losange/Pétale](./losange.png)

<center><iframe src="https://trinket.io/embed/python/c9b3a56e23" width="100%" height="600" frameborder="0" marginwidth="0" marginheight="0" allowfullscreen></iframe></center>

<bad>Aller Plus loin</bad> :rocket::rocket: Ajouter une tige, et des feuilles sur la tige..

### Spirales de Traits

:rocket::rocket::rocket: Dessiner des Spirales de Traits :

=== "Spirale de Traits en Triangle"
    <center><iframe src="https://trinket.io/embed/python/95e018a2ee" width="100%" height="600" frameborder="0" marginwidth="0" marginheight="0" allowfullscreen></iframe></center>

=== "Spirale de Traits en Carrés"
    <center><iframe src="https://trinket.io/embed/python/bcf54ef52e" width="100%" height="600" frameborder="0" marginwidth="0" marginheight="0" allowfullscreen></iframe></center>

!!! hint "Aide"
    On peut commencer par se demander :

    * Si un trait est de longueur `x` à un instant donné, quelle longueur minimale (au jugé) doit faire le trait suivant (en fonction de `x`)?
    * Que peut-on dire du nombre de degrés à tourner vers la gauche (à chaque 'coin' de la spirale) ? (même une valeur très approchée, selon vous)

### Descente de Ski

Commencer par définir `200x200` comme taille de fenêtre graphique, avec la commande `Screen().setup(200,200)`. Ensuite :

:rocket::rocket: Construire une *Piste de Ski* comme sur la figure suivante :

<center><iframe src="https://trinket.io/embed/python/03bf82e979" width="100%" height="600" frameborder="0" marginwidth="0" marginheight="0" allowfullscreen></iframe></center>

!!! hint "Aide"
    * Répéter plusieurs fois la contruction d'un trait
    * les deux extrémités du trait appartiennent aux côtés horizontal (bas) et vertical (droit)
    * Etat initial : relier par un trait le point en haut à droite, au point en bas à droite
    * le point vertical descend de la même quantité que le point horizontal se déplace à gauche

<bad>Aller Plus loin</bad> Répéter 4 fois cette opération, de sorte à recouvrir chaque coin de la fenêtre graphique

### Marche Aléatoire

1. :rocket::rocket: La Marche Aléatoire de la Tortue. Dessiner :

    * une succession de **100** Pas/Traits 
    * chaque pas est de longueur fixe **10px**
    * chaque futur trait prend une nouvelle direction alatoire : EST, OUEST, NORD ou SUD
    * Facultatif : On pourra également, si vous y arrivez : 
        * calculer et afficher la distance (en nombre de pas) entre le point de départ et le tout dernier point d'arrivée
        * calculer et afficher la durée totale de parcours, grâce au module **time**, inclus dans la bibliothèque standard :

            ```python
            from time import time
            start = time()      # Top départ du chronomètre
            # ...
            # Algorithme à chronométrer
            # ...
            end = time()        # Top arrêt du chronomètre
            print("durée=", end - start)
            ```

    !!! hint "Astuce"
        * Vous pouvez vous amuser à afficher un logo de **Tortue**, au lieu d'une icone triangle, avec la commande `shape("turtle")`. Tous les logos disponibles (cf. [ici](https://docs.python.org/3/library/turtle.html#turtle.shape)) sont : “arrow”, “turtle”, “circle”, “square”, “triangle”, “classic”. 
        * Pensez à accélérer le tracé avec la commande `speed(0)` ou `speed("fastest")`

    <center><iframe src="https://trinket.io/embed/python/40ada7e7d1" width="100%" height="600" frameborder="0" marginwidth="0" marginheight="0" allowfullscreen></iframe></center>

## Polygones

### Triangles

1. :rocket: Dessiner un triangle dont les 3 sommets ont pour coordonnées `(0,0)`, `(300,100)` et `(200,50)`

    <center><iframe src="https://trinket.io/embed/python/5417b71f9c" width="100%" height="600" frameborder="0" marginwidth="0" marginheight="0" allowfullscreen></iframe></center>

1. Dessiner un triangle équilatéral dont chaque côté vaut **100px**

    === ":rocket: Triangle SANS Couleur de Fond"
        <center><iframe src="https://trinket.io/embed/python/39dac64b37" width="100%" height="600" frameborder="0" marginwidth="0" marginheight="0" allowfullscreen></iframe></center>

        ??? hint "Aide"
            Tourner à gauche de $\dfrac{360°}{3} = 120°$
    === ":rocket: Triangle AVEC Couleur de Fond"
        Par exemple avec un Bord et un Fond de couleur rouge
        <center><iframe src="https://trinket.io/embed/python/8568136eca" width="100%" height="600" frameborder="0" marginwidth="0" marginheight="0" allowfullscreen></iframe></center>
    === ":rocket::rocket: Ligne de 6 Triangles AVEC Couleurs de Fond"
        <center><iframe src="https://trinket.io/embed/python/1b7f8a1353" width="100%" height="600" frameborder="0" marginwidth="0" marginheight="0" allowfullscreen></iframe></center>

        !!! hint "Astuce"
            On pourra par exemple :

            * choisir entre **deux méthodes** pour changer de couleur entre deux formes :
                * <bad>Méthode 1</bad> 
                    * définir une variable `couleur` pour l'indice des couleurs
                    * initialiser le compteur à 0 : `couleur=0`
                    * puis, incrémenter de 1 à chaque nouvelle itération de la forme, pour changer de couleur à chaque fois. Ou bien :
                * <bad>Méthode 2</bad>
                    * Stocker les couleurs dans une liste `couleurs = ["red", "orange", "yellow", "green", "blue", "purple"]`, puis parcourir la liste des couleurs avec une boucle `for`, et dans chaque cas, tracer les formes de couleur, puis se déplacer légèrement vers l'Est de quelques pixels.
            * accélérer le tracé avec `speed(0)`

1. :rocket::rocket: Kaleidoscope de Triangles

<center><iframe src="https://trinket.io/embed/python/5aaf843cee" width="100%" height="600" frameborder="0" marginwidth="0" marginheight="0" allowfullscreen></iframe></center>

!!! hint "Astuce"
    Pensez à accélérer le tracé avec la commande `speed(0)` ou `speed("fastest")`

### Vortex de Triangles

:rocket::rocket: Dessiner un **Vortex de Triangles Équilatéraux**, tel que :

* Le triangle équilatéral initial a un côté de **50px**
* A chaque nouvelle itération de triangles équilatéraux :
    * le côté de chaque nouveau triangle équilatéral augmente légèrement
    * chaque triangle équilatéral subit une légère rotation (à droite/gauche choisissez votre camp... ) 

<center><iframe src="https://trinket.io/embed/python/c4b81a3e34" width="100%" height="600" frameborder="0" marginwidth="0" marginheight="0" allowfullscreen></iframe></center>

!!! hint "Aide"
    * En fait ce sont **presque** des triangles équilatéraux, **mais pas exactement** : il s'agit en fait d'une **illusion optique**, qui **donne l'illusion** que ce sont des triangles équilatéraux.
    * Inspirez-vous plutôt de votre travail sur les Spirales de Traits, mais en modifiant légèrement le code qui calculait l'augmentation du côté `x` entre chaque nouvelle itération de triangle équilatéral.

### Carrés

1. Un Carré de côté **100px**

    === ":rocket: Carré SANS Couleur de Fond"
        <center><iframe src="https://trinket.io/embed/python/2368b7fab8" width="100%" height="600" frameborder="0" marginwidth="0" marginheight="0" allowfullscreen></iframe></center>

        ??? hint "Aide"
            Tourner à gauche de $\dfrac{360°}{4} = 90°$
    === ":rocket: Carré AVEC Couleur de Fond"
        Par exemple avec des Bords et un Fond de couleur rouge.
        <center><iframe src="https://trinket.io/embed/python/b731558968" width="100%" height="600" frameborder="0" marginwidth="0" marginheight="0" allowfullscreen></iframe></center>
    === ":rocket::rocket: Ligne de 6 Carrés AVEC Couleurs de Fond"
        <center><iframe src="https://trinket.io/embed/python/315a97cf5e" width="100%" height="600" frameborder="0" marginwidth="0" marginheight="0" allowfullscreen></iframe></center>

        !!! hint "Astuce"
            On pourra par exemple :

            * choisir entre **deux méthodes** pour changer de couleur entre deux formes :
                * <bad>Méthode 1</bad> 
                    * définir une variable `couleur` pour l'indice des couleurs
                    * initialiser le compteur à 0 : `couleur=0`
                    * puis, incrémenter de 1 à chaque nouvelle itération de la forme, pour changer de couleur à chaque fois. Ou bien :
                * <bad>Méthode 2</bad>
                    * Stocker les couleurs dans une liste `couleurs = ["red", "orange", "yellow", "green", "blue", "purple"]`, puis parcourir la liste des couleurs avec une boucle `for`, et dans chaque cas, tracer les formes de couleur, puis se déplacer légèrement vers l'Est de quelques pixels.
            * accélérer le tracé avec `speed(0)`

1. Ampliation de Carrés

:rocket::rocket: Dessiner une *ampliation* de carrés telle que:

* Le plus petit des carrés a un côté de **10px**
* la taille (du côté) des carrés augmente de **10px**, entre deux carrés

<center><iframe src="https://trinket.io/embed/python/4900625b2b" width="100%" height="600" frameborder="0" marginwidth="0" marginheight="0" allowfullscreen></iframe></center>

1. Ligne de Carrés

:rocket: Dessiner une ligne de `10` carrés contigüs, telle que chaque carré (élémentaire) a un côté de **20px**

<center><iframe src="https://trinket.io/embed/python/4001902ebc" width="100%" height="600" frameborder="0" marginwidth="0" marginheight="0" allowfullscreen></iframe></center>

1. Grille de Carrés

:rocket::rocket::rocket: Dessiner une grille de `10x10` carrés, telle que :

* Chaque carré (élémentaire) a un côté de **20px**

<center><iframe src="https://trinket.io/embed/python/230ba39052" width="100%" height="600" frameborder="0" marginwidth="0" marginheight="0" allowfullscreen></iframe></center>

1. :rocket::rocket: Kaleidoscope de Carrés

<center><iframe src="https://trinket.io/embed/python/8aba5a60c7" width="100%" height="600" frameborder="0" marginwidth="0" marginheight="0" allowfullscreen></iframe></center>

!!! hint "Astuce"
    Pensez à accélérer le tracé avec la commande `speed(0)` ou `speed("fastest")`

1. Spirale de Carrés

:rocket::rocket::rocket: Dessiner une Spirale de Carrés telle que:

* Le côté du carré initial vaut **200px**
* La spirale contient environ une trentaine de carrés
* La taille des carrés diminue à chaque étape (par exemple, la taille diminue de $\dfrac 19$ entre chaque itération)
* chaque carré subit une légère rotation comme sur la figure Trinket ci-contre

<center><iframe src="https://trinket.io/embed/python/609a0977dc" width="100%" height="600" frameborder="0" marginwidth="0" marginheight="0" allowfullscreen></iframe></center>

### Vortex de Carrés

:rocket::rocket: Dessiner un **Vortex de Carrés**, tel que :

* Le carré initial a un côté de **50px**
* A chaque nouvelle itération de carrés :
    * le côté de chaque nouveau carré augmente légèrement
    * chaque carré subit une légère rotation (à droite/gauche choisissez votre camp... ) 

<center><iframe src="https://trinket.io/embed/python/6a85b17213" width="100%" height="600" frameborder="0" marginwidth="0" marginheight="0" allowfullscreen></iframe></center>

!!! hint "Aide"
    * En fait ce sont **presque** des carrés, **mais pas exactement** : il s'agit en fait d'une **illusion optique**, qui **donne l'illusion** que ce sont des carrés.
    * Inspirez-vous plutôt de votre travail sur les Spirales de Traits, mais en modifiant légèrement le code qui calculait l'augmentation du côté `x` entre chaque nouvelle itération de carrés


### Rectangles

1. Un Rectangle dont les côtés sont Largeur=50px, et Hauteur=100px

    === ":rocket: Rectangle Sans Fonds de Couleur"
        <center><iframe src="https://trinket.io/embed/python/6ab1a3f095" width="100%" height="600" frameborder="0" marginwidth="0" marginheight="0" allowfullscreen></iframe></center>
    === ":rocket: Rectangle Avec Fonds Couleur"
        Par exemple avec un Bord et un Fond de couleur rouge
        <center><iframe src="https://trinket.io/embed/python/bf1139d65e" width="100%" height="600" frameborder="0" marginwidth="0" marginheight="0" allowfullscreen></iframe></center>

1. :rocket: :rooster: Dessiner le Drapeau Français ! et/ou tout autre drapeau (conseil: avec des formes simples) !

    <center><iframe src="https://trinket.io/embed/python/34da30a662" width="100%" height="600" frameborder="0" marginwidth="0" marginheight="0" allowfullscreen></iframe></center>

### Pentagone

1. Un Pentagone Régulier

    === ":rocket: SANS fond de Couleur"
        <center><iframe src="https://trinket.io/embed/python/f152f56f24" width="100%" height="600" frameborder="0" marginwidth="0" marginheight="0" allowfullscreen></iframe></center>

        ??? hint "Aide"
            Tourner à gauche de $\dfrac{360°}{5} = 72°$
    === ":rocket: AVEC Couleur de Fond"
        <center><iframe src="https://trinket.io/embed/python/68c3956898" width="100%" height="600" frameborder="0" marginwidth="0" marginheight="0" allowfullscreen></iframe></center>

1. Vortex de Pentagones

:rocket::rocket: Dessiner un **Vortex de Pentagones**, tel que :

* Le pentagone initial a un côté de **50px**
* A chaque nouvelle itération de pentagones :
    * le côté de chaque nouveau pentagone augmente légèrement
    * chaque pentagones subit une légère rotation (à droite/gauche choisissez votre camp... ) 

<center><iframe src="https://trinket.io/embed/python/b2fd30bbc7" width="100%" height="600" frameborder="0" marginwidth="0" marginheight="0" allowfullscreen></iframe></center>

!!! hint "Aide"
    * En fait ce sont **presque** des pentagones, **mais pas exactement** : il s'agit en fait d'une **illusion optique**, qui **donne l'illusion** que ce sont des pentagones.
    * Inspirez-vous plutôt de votre travail sur les Spirales de Traits, mais en modifiant légèrement le code qui calculait l'augmentation du côté `x` entre chaque nouvelle itération de pentagones. 

### Etoile à 5 branches

1. :rocket: Dessiner une étoile à $5$ branches dont la longueur d'une branche vaut **200px**

    <center><iframe src="https://trinket.io/embed/python/4692545ce3" width="100%" height="600" frameborder="0" marginwidth="0" marginheight="0" allowfullscreen></iframe></center>

    ??? hint "Aide"
        Tourner à gauche de $144°$

1. :rocket::rocket: Drapeau Européen

Utiliser la question précédente pour dessiner un Drapeau Européen :eu:

<center><iframe src="https://trinket.io/embed/python/3bab1a057b" width="100%" height="600" frameborder="0" marginwidth="0" marginheight="0" allowfullscreen></iframe></center>

### Hexagone

1. :rocket: Un Hexagone Régulier

    <center><iframe src="https://trinket.io/embed/python/5a043a537d" width="100%" height="600" frameborder="0" marginwidth="0" marginheight="0" allowfullscreen></iframe></center>

    ??? hint "Aide"
        Tourner à gauche de $\dfrac{360°}{6} = 60°$

### Polygones Réguliers à n côtés

1. :rocket: Implémentez en **Python**, grâce au module **Turtle**, l'algorithme suivant qui est donné en **Pseudo-code**. 


    Cet algorithme trace des Polygones Etoilés à N (ici, N=9) Branches.

    <center><iframe src="https://trinket.io/embed/python/54952391a9" width="100%" height="600" frameborder="0" marginwidth="0" marginheight="0" allowfullscreen></iframe></center>

## Cercles & Arcs de Cercles

### Cercles et Disques de Couleur

=== ":rocket: Cercles SANS Couleur de Fond"
    Dessiner :

    * Un cercle de rayon **50px** qui tourne dans le sens **Anti-Horaire** (sens contraire des aiguilles d'une montre)

    suivi de :

    * Un cercle de rayon **50px** qui tourne dans le sens **Horaire** (sens des aiguilles d'une montre)

    <center><iframe src="https://trinket.io/embed/python/1ef0b8767c" width="100%" height="600" frameborder="0" marginwidth="0" marginheight="0" allowfullscreen></iframe></center>

=== ":rocket: Disques AVEC Couleur de Fond"
    Dessiner un disque de rayon **50px**, dont les bords ET le fond est bleu.

    <center><iframe src="https://trinket.io/embed/python/34fb114e53" width="100%" height="600" frameborder="0" marginwidth="0" marginheight="0" allowfullscreen></iframe></center>

### Lignes de Disques

:rocket::rocket: Dessiner une Ligne de 6 Disques AVEC Couleur de Fond.

<center><iframe src="https://trinket.io/embed/python/5fad7d048e" width="100%" height="600" frameborder="0" marginwidth="0" marginheight="0" allowfullscreen></iframe></center>

!!! hint "Astuce"
    On pourra par exemple :

    * choisir entre **deux méthodes** pour changer de couleur entre deux formes :
        * <bad>Méthode 1</bad>
            * définir une variable `couleur` pour l'indice des couleurs
            * initialiser le compteur à 0 : `couleur=0`
            * puis, incrémenter de 1 à chaque nouvelle itération de la forme, pour changer de couleur à chaque fois. Ou bien :
        * <bad>Méthode 2</bad>
            * Stocker les couleurs dans une liste `couleurs = ["red", "orange", "yellow", "green", "blue", "purple"]`, puis parcourir la liste des couleurs avec une boucle `for`, et dans chaque cas, tracer les formes de couleur, puis se déplacer légèrement vers l'Est de quelques pixels.
        * accélérer le tracé avec `speed(0)`

### Kaleidoscope de Cercles

:rocket::rocket: Dessiner un Kaléidoscope de Cercles :

<center><iframe src="https://trinket.io/embed/python/874e9c8714" width="100%" height="600" frameborder="0" marginwidth="0" marginheight="0" allowfullscreen></iframe></center>

!!! hint "Astuce"
    Pensez à accélérer le tracé avec la commande `speed(0)` ou `speed("fastest")`

### Disques en Arc de Cercle

:rocket::rocket::rocket: Dessiner des Disques de Couleur disposés en Arc de Cercle :

<center><iframe src="https://trinket.io/embed/python/33ec347f23" width="100%" height="600" frameborder="0" marginwidth="0" marginheight="0" allowfullscreen></iframe></center>

!!! hint "Astuce"
    On pourra par exemple :

    * choisir entre **deux méthodes** pour changer de couleur entre deux formes :
        * <bad>Méthode 1</bad>
            * définir une variable `couleur` pour l'indice des couleurs
            * initialiser le compteur à 0 : `couleur=0`
            * puis, incrémenter de 1 à chaque nouvelle itération de la forme, pour changer de couleur à chaque fois. Ou bien :
        * <bad>Méthode 2</bad>
            * Stocker les couleurs dans une liste `couleurs = ["red", "orange", "yellow", "green", "blue", "purple"]`, puis parcourir la liste des couleurs avec une boucle `for`, et dans chaque cas, tracer les formes de couleur, puis se déplacer légèrement vers l'Est de quelques pixels.
    * accélérer le tracé avec `speed(0)`

<bad>Aller Plus Loin</bad> :rocket::rocket::rocket: Et si on souhaitait dessiner plus de 6 Disques, toujours en arc de cercle :

* Comment faire pour que les couleurs soient alternées, mais se répètent en boucle tous les 6 disques ? 

<red>

On peut par exemple utiliser la méthode qui utilise un indice `couleur` (méthode 1), et incrémenter cet indice **modulo 6** (6 étant la taille du cycle de couleurs que vous avez choisie) : `couleur += 1 % 6`

</red>

* Quelle méthode (la 1 ou la 2) vous semble préférable ? et pourquoi ? 

<red>La méthode 1 semble donc préférable car plus facile à implémenter en Python.</red>

### Spirale Circulaire

:rocket::rocket: Dessiner une Spirale circulaire.

<center><iframe src="https://trinket.io/embed/python/3b86cf1b6e" width="100%" height="600" frameborder="0" marginwidth="0" marginheight="0" allowfullscreen></iframe></center>

!!! hint "Aide"
    On pourra répéter une vingtaine de fois (avec boucle `for`) les commandes suivantes :

    * tracer un arc de cercle avec `circle(r,a)` où `r` est le rayon et `a` l'angle de l'arc
    * augmenter le rayon `r` proportionnellement à chaque itération
    * choisir un angle de `a=90°`

### Yin & Yang

=== ":rocket::rocket: Yin Yang Simple SANS Couleurs de Fond"
    <center><iframe src="https://trinket.io/embed/python/a6de0e8784" width="100%" height="600" frameborder="0" marginwidth="0" marginheight="0" allowfullscreen></iframe></center>
=== ":rocket::rocket::rocket: Yin Yang Evolué AVEC Couleurs de Fond"
    <center><iframe src="https://trinket.io/embed/python/32be854666" width="100%" height="600" frameborder="0" marginwidth="0" marginheight="0" allowfullscreen></iframe></center>

### Soleil

:rocket::rocket::rocket: Dessiner un Soleil, avec les caractéristiques suivantes :

* Un Cercle central de rayon **50px**
* $120$ rayons de soleil autour du cercle
* un angle de `3°` sépare chaque rayon de soleil

<center><iframe src="https://trinket.io/embed/python/feec6f5436" width="100%" height="600" frameborder="0" marginwidth="0" marginheight="0" allowfullscreen></iframe></center>

## Dessin Algorithmique

1. :rocket::rocket: Le site [Wellesley Edu](https://cs111.wellesley.edu/labs/lab01/introTurtles) répertorie quelques exemples d'images artistiques à créer avec Turtle. Tentez de les reproduire et de les modifier.  

    ![Abeille](./im_abeille.png){.left .shadow width=31%}
    ![Bonhomme](./im_bonhomme.png){.left .shadow width=31%}
    ![Coucher de Soleil](./im_coucher_soleil.png){.left .shadow width=31%}
    ![Plante](./im_plante.png){.left .shadow width=31%}
    ![Panda](./im_panda.png){.left .shadow width=31%}
    ![Montagnes](./im_montagnes.png){.left .shadow width=31%}
    ![Elephant](./im_elephant.png){.left .shadow width=31% }
    ![Poisson](./im_poisson.png){.left .shadow width=31%}
    ![Pont](./im_pont.png){.left .shadow width=31%}

    <div style="clear:both;display:block;"></div>

1. :rocket:(:rocket:(:rocket:)) Créez vos propres oeuvres artistiques ! Seule votre imagination est la Limite. Soyez créatives et créatifs !

<!-- ## Animations

1. :rocket::rocket: Animation Terre Lune

    <center><iframe src="https://trinket.io/embed/python/0ad1963bb5" width="100%" height="600" frameborder="0" marginwidth="0" marginheight="0" allowfullscreen></iframe></center>

1. :rocket::rocket::rocket: Dessiner un Système Solaire

    <center><iframe src="https://trinket.io/embed/python/55dbcc0dbb" width="100%" height="600" frameborder="0" marginwidth="0" marginheight="0" allowfullscreen></iframe></center> -->

## Art Aléatoire

1. :rocket::rocket::rocket: Génération de Polygones Aléatoires

    <center><iframe src="https://trinket.io/embed/python/ce2ec74899" width="100%" height="600" frameborder="0" marginwidth="0" marginheight="0" allowfullscreen></iframe></center>

1. :rocket::rocket::rocket::rocket: Génération d'un Vortex de Rectangles aux Couleurs Aléatoires (l'Animation est longue - 10 min, prévoir de la lancer longtemps en avance - et finit par créer des rectangles de couleur)

    <center><iframe src="https://trinket.io/embed/python/2840408a68" width="100%" height="600" frameborder="0" marginwidth="0" marginheight="0" allowfullscreen></iframe></center>

Résultat Attendu :

![Vortex de Rectangles de Couleur](./vortex_rectangles_couleur.png)

1. Créer votre **Oeuvre d'Art Aléatoire** personnelle ! Soyez Créatives et Créatifs !

## Références

* Revue Sésamath : [Algorithmes de Polygones Étoilés](http://revue.sesamath.net/spip.php?article1312)
* Wellesley Education : [Images Artistiques avec Turtle](https://cs111.wellesley.edu/labs/lab01/introTurtles)
* Le Vortex de Rectangles de Couleur est issue de [cette page](https://pythonturtle.academy/vortex-of-squares-with-python-turtle/?amp)