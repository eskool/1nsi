# Le Module Time de la Librairie Standard

Le module `time` de la bibliothèque standard dispose de plusieurs outils permettant de mesurer le temps (=**chronométrer**) que met un algorithme pour s'éxécuter.

## La fonction `time()` du module `time`

La fonction `time()` du module `time` renvoie la **durée en secondes**, sous la forme d'un *nombre flottant* (float), depuis l'instant initial, nommé **epoch** (le 1er janvier 1970, 0h00min0s).

=== "Syntaxe `import time`"

    ```python
    import time
    start = time.time()

    # Votre algorithme à chronométrer ici...

    stop = time.time()

    duree = stop - start
    print("durée =", duree)
    ```

=== "Syntaxe `from time import time`"

    ```python
    from time import time
    start = time()

    # Votre algorithme à chronométrer ici...

    stop = time()

    duree = stop - start
    print("durée =", duree)
    ```

## La fonction `process_time()` du module `time`

La fonction `process_time()` du module `time`, renvoie (en fractions de secondes) la somme de **l'heure système** et du **temps CPU propre à l'utilisateur**, pour le *processus courant* (l'algorithme en cours d'exécution). Cette fonction ne devrait pas compter les durées durant lesquelles le CPU est occupé à exécuter d'autres tâches.

!!! info
    Cette fonction `process_time()` est **mieux adaptée** (que la fonction `time()`), et donc `process_time()` est **à préférer pour comparer la complexité des algorithmes**.

=== "Syntaxe `import time`"

    ```python
    import time
    start = time.process_time()

    # Votre algorithme à chronométrer ici...

    stop = time.process_time()

    duree = stop - start
    print("durée =", duree)
    ```

=== "Syntaxe `from time import process_time`"

    ```python
    from time import process_time
    start = process_time()

    # Votre algorithme à chronométrer ici...

    stop = process_time()

    duree = stop - start
    print("durée =", duree)
    ```

