import matplotlib.pyplot as plt

# WITH RANDOM = TRUE, arrondi au 10000-ième, pour les coeffs "magiques" normaux coeffs = [1.0.95,0.9,0.85]
# DUREES d'EXECUTION MOYENNE PAR PARTIE en fonction de la profondeur
x= [1,2,3,4,5,6]
y = [1,4,9,16,25,36]
title = "Temps d'Exécution"
fig, ax = plt.subplots(nrows=1,ncols=1)
fig.canvas.set_window_title(title)
ax.plot(x, y, "b",marker="X",markersize=11)

# ax.set(xlabel="Taille des Données en Entrée", ylabel=title,title=title, 
#         xticks=[1,2,3,4,5,6],yticks=[0,10,50,100,500,1000,5000,10000])
ax.set(xlabel="Taille des Données en Entrée", ylabel=title,title=title)
ax.grid()
fig.savefig("test.png")
# plt.tight_layout()
plt.show()


