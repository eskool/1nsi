# TD MATPLOTLIB

!!! note
    un <red>MWE</red> :gb: (<red>Minimal Working Example</red>) ou un <red>CMF</red> :fr: (<red>Code Minimal de Fonctionnement</red>) pour tracer une courbe avec Matplotlib :

```python
from matplotlib import pyplot as plt
# import matplotlib.pyplot as plt
x = [i for i in range(start, end, step)]
y = [f(x) for i in x]  # pour une certaine fonction f
plt.plot(x,y)
# plt.xlabel("x")
# plt.ylabel("quantité de premier pour x ")
plt.show()
```

!!! ex
    1. Pour chacune des fonctions $f$ suivantes, tracer les courbes de $f$ sur chaque intervalle d'étude $I$ considéré :

        a. $f(x)=x^2$ sur $I=\mathbb R$  
        b. $f(x)=x^3$ sur $I=\mathbb R$  
        c. $\displaystyle f(x)=\frac 1x$ sur $I=\mathbb R^{+}_{*}=]0;+\infty[$  
        d. $f(x)=cos(x)$ sur $I=\mathbb R$
        
    2. Modifier la couleur de la courbe en rouge (etc...)
    3. Modifier l'épaisseur et le [style](https://matplotlib.org/2.0.2/api/lines_api.html) du trait de la courbe (pointillés, etc..)
    4. Modifier la taille, la couleur et les [marqueurs](https://matplotlib.org/3.1.1/api/markers_api.html) des points
    5. Autres Paramètres de `plot()` : Voir la [page suivante](https://matplotlib.org/2.1.1/api/_as_gen/matplotlib.pyplot.plot.html) pour une liste des paramètres
    5. Ajouter des *labels* sur l'axe des $x$ et sur l'axe des $y$
    6. Donner un titre à la fenêtre graphique (pour remplacer le titre par défaut ***Figure 1***)
