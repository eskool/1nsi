# 1NSI : Exercices Tuples

!!! ex "Compréhension de Tuples"
    Proposer un tuple en compréhension pour obtenir les résultats suivants:  
    1°) `(10,20,30,...,250)`  
    2°) `(0,10,20,30,...,200)`  
    3°) `(0,3,6,9,...,60)`  
    4°) `(0,1,2,3,4,...,100)`  
    5°) `(0,2,4,...,100)`  
    (idem que 4°), mais seulement les nombres pairs )  
    5°) `(1,3,5,...,99)`  
    (idem que 4°), mais seulement les nombres impairs )  
    6°) `("pair","impair","pair","impair"...,"pair")`  
    (idem que 4°) mais en affichant la *parité*, plutôt que les *valeurs* des 101 premiers nombres )  
    7°) ```((0,1,2,3,4),  
            (5,6,7,8,9),  
            (10,11,12,13,14),  
            ...,  
            (100,101,102,103,104))```  