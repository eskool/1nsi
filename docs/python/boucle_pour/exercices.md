# Exercices sur la boucle Pour

<env>Consigne</env> :warning: Les exercices suivants **DOIVENT** utiliser la boucle **for**... :warning:

!!! ex "Suites de Valeurs :rocket:"
    Écrire un algorithme qui affiche les valeurs suivantes (en orange) dans le Terminal :  

    === "Question 1."  
        ```python
        10
        11
        12
        13
        14
        15
        16
        ```
    === "Question 2."  
        ```python
        10
        12
        14
        16
        18
        20
        22
        24
        ```
    === "Question 3."
        ```python
        30
        31
        32
        33
        34
        35
        36
        ```
    === "Question 4."
        ```python
        37
        36
        35
        34
        33
        32
        ```

!!! ex "Suites de Valeurs :rocket::rocket:"
    1. Écrire un algorithme qui calcule la somme $S=1+2+3+...+1000$
    2. Écrire un algorithme qui calcule la somme $S=1^2+2^2+3^2+...+1000^2$
    3. Écrire un algorithme qui calcule la somme $S=\dfrac 11+\dfrac 12+\dfrac 13+...+\dfrac 1{1000}$
    4. Écrire un algorithme qui calcule la somme $S=\dfrac 1{1^2}+\dfrac 1{2^2}+\dfrac 1{3^2}+...+\dfrac 1{1000^2}$

!!! ex "Compter le nombre de caractères x"
    1. On se donne une chaîne de caractères `s="bonjour maman il fait beau aujourd'hui"`
    Écrire un algorithme qui compte le nombre de caractères `"a"` dans la chaîne `s`
    2. Modifier l'algorithme précédent de sorte qu'on définisse une fonction `compte(c:str,s:str)->int` telle que:

        * cette fonction recoive en entrée deux variables : un caractère `c` et une chaîne `s`
        * qui renvoie en sortie le nombre de caractères `c` inclus dans la chaîne `s`

    3. Écrire une fonction `voyelles(s:str)->int` qui :

        * reçoit en entrée une chaîne de caractères `s`
        * renvoie en sortie le nombre de voyelles incluses dans `s`

!!! ex "mot de passe"
    1. Écrire une fonction `mot_de_passe_OK(mdp:str)->bool` qui :

        * reçoit en entrée une chaîne/string `mdp` représentant un mot de passe
        * renvoie en sortie :
            * `True` si le mot de passe contient au minimum $8$ caractères
            * `False` sinon
    2. Modifier l'algorithme précédent afin que la fonction `mot_de_passe_OK(mdp:str)->bool` renvoie :

        * `True` lorsque:
            * le mot de passe contient au minimum $8$ caractères (ET)
            * le mot de passe contient au minimum $1$ chiffre
        * `False` sinon
    3. Modifier l'algorithme précédent afin que la fonction `mot_de_passe_OK(mdp:str)->bool` renvoie :

        * `True` lorsque:
            * le mot de passe contient au minimum $8$ caractères (ET)
            * le mot de passe contient au minimum $1$ chiffre
            * le mot de passe contient au minimum $1$ majuscule
        * `False` sinon





