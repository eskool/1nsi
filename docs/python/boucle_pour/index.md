# Boucle Pour

## Méthode 1 : Élément par élément

### Boucle `Pour` / `for` : Parcourir des entiers

**Syntaxe Pseudo-Code:**

```pseudocode
Pour i variant de a à b avec un pas de 1
    Afficher i
```

**Syntaxe Python :**

```python
for i in range(a,b+1,pas):
    print(i)
```

!!! remarque
    Dans cette syntaxe Python:

    * La valeur `a` est incluse
    * ATTENTION : La valeur `b+1` N'est PAS incluse : Si le pas vaut `1`, alors la dernière valeur incluse est `b`

### Boucle `Pour` / `for`: Parcourir des chaînes de caractère

**Syntaxe Pseudo-Code:**

```pseudocode
Pour chaque caractère c de phrase:
    Afficher c
```

**Syntaxe Python :**

```python
phrase="C'est bon les kiwis"
for c in phrase:
    print(c)
```

### Boucle `Pour` / `for` : Parcourir des Listes

**Syntaxe Pseudo-Code:**

```pseudocode
Pour chaque element elt de la liste l:
    Afficher elt
```

**Syntaxe Python :**

```python
liste=[4,10,7.2,"bonjour","laura"]
for elt in liste:
    print(elt)
```

## Méthode 2 : Parcourir indice par indice

### Boucle `Pour` / `for`: Parcourir des chaînes de caractère

**Syntaxe Pseudo-Code:**

```pseudocode
Pour chaque indice i de phrase:
    Afficher l'élément/le caractère d'indice i de la phrase
```

**Syntaxe Python :**

```python
phrase="C'est bon les kiwis"
for i in range(len(phrase)):
    print(phrase[i])
```

### Boucle `Pour` / `for` : Parcourir des Listes

**Syntaxe Pseudo-Code:**

```pseudocode
Pour chaque indice i de liste:
    Afficher l'élément d'indice i de liste
```

**Syntaxe Python :**

```python
liste=[4,10,7.2,"bonjour","laura"]
for i in range(len(liste)):
    print(liste[i])
```

## Caractéristiques Principales de la Boucle `Pour` / `for`

* On entre toujours OBLIGATOIREMENT dans une boucle `Pour` / `for`
* L'initialisation de la boucle `Pour` / `for` est AUTOMATIQUE
* L'incrémentation (/décrémentation) de la boucle `Pour` / `for` est AUTOMATIQUE
* Une boucle `Pour` / `for` réalise toujours un nombre fini de tours / itérations connu à l'avance (avant de commencer) : Dans l'exemple ci-dessus, si le pas vaut `1`, alors la boucle `Pour` / `for` réalise `b-a+1` itérations
* En particulier, une boucle `Pour` / `for` n'est jamais infinie

