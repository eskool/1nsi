# 1NSI : Cours Chaînes de Caractères

## Introduction

!!! def
    Une <bred><em>Chaîne (de caractères)</em></bred> :fr: , ou une <bred><em>String</em></bred> :gb: est un *conteneur* (non modifiable/immutable) d'éléments **ordonnés**, qui sont forcément des **caractères**.

C'est une *structure de données **primitive*** de Python, qui est un ***type construit*** donc un conteneur/collections de **caractères**.

## Fonctions utiles sur les Chaînes

### Longueur d'une chaîne de caractère

```python
>>> mot="Bonjour"
>>> len(mot)  # renvoie le nombre de caractères (ici : 7)
7
```

`len` renvoie la ***longueur*** de la chaîne

### Chaîne Vide

```python
>>> mot="" # est la chaîne 'VIDE' qui est de type 'str',
           # et qui ne contient aucun élément
>>> len(mot)
0          # la longueur de la chaîne vide vaut 0
```

ou bien (équivalent pour définir une chaîne vide)

```python
>>> mot=str()
```

### Appartenance à une Chaîne avec `in`, ou pas avec `not in`

```python
>>> phrase = "bonjour maman il fait beau"
>>> 'a' in phrase
True
>>> 'A' in phrase
False
>>> 'maman' in phrase
True
>>> 'man il f' in phrase
True
# ou bien NON appartenance avec les mots-clés `not in`
>>> 'a' not in phrase
False
```

### chr(*nombre*)

```python
>>> chr(65)
'A'  # renvoie le caractère dont le code ASCII vaut 65
```

### ord(*caractère*)

```python
>>> ord("A")
65  # renvoie le code ASCII du caractère 'A'
```

## Indices / Index

### Indices positifs

```python
>>> mot="Bonjour"
>>> mot[1]  # renvoie le caractère à l'indice 1
>>> mot[0]  # renvoie le 1er caractère = le caractère à l'indice 0
```

### Indices négatifs

```python
>>> mot[-1]  # renvoie le dernier caractère
>>> mot[-2]  # renvoie l'avant-dernier caractère
```

### Tranches / Slices ou *Slicing*

#### Tranches / Slicing simple (avec 2 arguments)

```python
>>> mot[3:6]  # renvoie tous les caractères dont les indices
              # sont compris entre 3 inclus 
              # et 5 inclus (donc 6 NON inclus)
```

#### Tranches / Slicing étendu (avec un 3ème argument *pas/step* optionnel)

```python
>>> mot[3:6:2]  # renvoie tous les caractères dont les indices
                # sont compris entre 3 inclus 
                # et 5 inclus (c'est-à-dire 6 NON inclus)
                # mais seulement tous les 2 caractères, avec un pas=2
```

!!! exp
    ```python
    >>> mot[:6:2]    # tous les caractères depuis le tout début
                    # jusqu'à celui dl'indice 5 inclus (c'est-à-dire 6 NON inclus)
                    # mais seulement tous les 2 caractères, avec un pas=2
    >>> mot[3::2]    # tous les caractères depuis celui d'indice 3 inclus
                    # jusqu'à la toute fin du mot/phrase
                    # mais seulement tous les 2 caractères, avec un pas=2
    >>> mot[::2]     # tous les caractères depuis le tout début
                    # jusqu'à la toute fin du mot/phrase
                    # mais seulement tous les 2 caractères, avec un pas=2
    >>> mot[::-1]    # tous les caractères depuis le tout début
                    # jusqu'à la toute fin du mot/phrase
                    # avec un pas=-1, donc écrit à l'envers...
    >>> mot[5:2:-1]  # tous les caractères depuis celui d'indice 5
                    # jusqu'à celui d'indice 2 NON INCLUS
                    # avec un pas=-1, donc écrit à l'envers...
    ```

## Les Chaînes sont des *`Sequences`*

Le fait que l'on puisse accéder aux éléments d'une chaîne avec des **indices entiers**, et que l'on dispose d'une fonction longueur `len()` est caractéristique de ce que l'on appelle des *Sequences* en Python :

!!! def
    Les <bred><em>sequences</em></bred> :gb:, (ou <bred><em>séquences</em></bred> :fr:, ou <bred><em>suites</em></bred> :fr: ), sont des ***conteneurs*** vérifiant les propriétés suivantes:

    * les **éléments** de la sequence sont **accessibles efficacement** via des <bred>indices *entiers*</bred> ($\Leftrightarrow$ *éléments "ordonnés"*) : 
    grâce à la *méthode spéciale* `__getitem__()`,
    * On dispose d'une <bred>fonction longueur</bred> `len()` de la Sequence :
    grâce à la *méthode spéciale* `__len__()`

Le mot ***suites*** est plutôt issu de la terminologie des mathématiques, mais il représente la même idée d'une collection ordonnée d'éléments (itérables).

!!! pte
    Les chaînes de caractères sont des **séquences**

## Techniques de parcours de Chaînes

### Technique 1 : caractère par caractère, SANS indices

```python
phrase="Bonjour Maman, il fait beau aujourd'hui"
for c in phrase:
  print("caractère=",c)
```

Cette technique de parcours des éléments/caractères d'une chaîne, un par un, sans utiliser les indices est caractéristique de ce que l'on appelle les **itérables** en Python :

!!! def "Itérables"
    En Python, un <bred><em>itérable</em></bred> :fr: ou <bred><em>iterable</em></bred> :gb: désigne un objet/*conteneur* sur lequel on peut *itérer*/parcourir les **éléments** <bred><em>un par un</em></bred>

!!! pte
    Les chaînes de caractères `str` sont des **itérables**.

<env>Généralisation</env> Pour tout **itérable** en Python, on peut parcourir les éléments un par un, avec une boucle `for` **sans utiliser les indices** (comme dans l'exemple précédent)


### Technique 2 : caractère par caractère, AVEC des indices

```python 
phrase="Bonjour Maman, il fait beau aujourd'hui"
for i in range(len(phrase)):
  print("caractère=",phrase[i])
```

<env>Généralisation</env> Toutes les séquences (des *conteneurs* ayant des indices entiers, et une fonction `len`) sont forcément itérables :
On peut en effet parcourir les éléments un par un, avec une boucle `for` **en utilisant les indices entiers**
Autrement dit : <enc><b>les <bred>*séquences*</bred> sont forcément <bred>*itérables*</bred></b></enc>

## Méthodes sur les Chaînes de Caractères

### Méthodes usuelles sur les chaînes

Voici quelques exemples d'utilisation de quelques méthodes, parmi *les plus utiles*, sur les Chaînes de Caractères. 
Notons `phrase` une chaîne de caractère.
`phrase="bonjour maman il fait beau"`

* `phrase.index("m")` renvoie :
  * le 1er indice où se trouve `"m"` dans la chaîne `phrase`
  * ou bien une erreur s'il ne trouve PAS `"m"` dans `phrase`
Forme Générale: `phrase.index("m"[,start[,end]])` cherche la 1ère occurence de `"m"` dans `phrase`, pour des indices compris entre `start` (et `end`)
* `phrase.find("m")` renvoie  (presque pareil que index())
  * le 1er indice où se trouve `"m"` dans la chaîne `phrase`
  * ou bien `-1` s'il ne trouve PAS `"m"` dans `phrase`
Forme Générale: `phrase.find("m"[,start[,end]])` cherche la 1ère occurence de `"m"` dans `phrase`, pour des indices compris entre `start` (et `end`)
* `phrase.replace("m","M")` remplace dans `phrase` toutes les ***occurences*** de "m" par des "M"
Forme Générale: `phrase.replace("m", "M"[, nombre])` remplace dans `phrase` les 1ères `nombre` d'***occurences*** de "m" par des "M"

* `phrase.count("m")` compte le nombre d'occurences de `"m"` dans `phrase`
Forme Générale: `phrase.count("m"[,start[,end]])` compte le nombre d'occurences de `"m"` dans `phrase`, pour des indices compris entre `start` (et `end`)
* `phrase.format()` formatte la chaîne de caractère selon un certain format...utiliser des `{}`
Exemple : `"bonjour {} je viens dans {} jours".format("Maman",3)`

### Liste Complète de TOUTES les Méthodes sur les chaînes

#### Aide en ligne

Vous trouverez une Liste Complète de TOUTES les Méthodes opérant sur les Chaînes de Caractères, sur [cette page de la Documentation Officielle](https://docs.python.org/3/library/stdtypes.html#str.index)

#### Aide en Local (dans un Interpréteur Python)

1. Dans un interpréteur Python, `dir(str)` affiche la liste complète de toutes les méthodes disponibles sur les chaînes `str`, *y compris les méthodes magiques/spéciales* (cf ci-dessous), **mais elles ne sont pas documentées (ni signature, ni docstring)**.

2. Dans un interpréteur Python, `help(str)` affiche la liste complète de toutes les méthodes disponibles sur les chaînes `str`, *y compris les méthodes magiques/spéciales* (cf ci-dessous), **AVEC DOCUMENTATION: AVEC LEURS SIGNATURES ET LES DOCSTRINGS**.

### Méthodes magiques / Méthodes spéciales sur les Chaînes

!!! def "Méthodes magiques / Méthodes spéciales sur les Chaînes"
    Parmi toutes les méthodes disponibles affichées par `dir(str)`, *certaines* sont entourées par **deux underscores  de chaque côté** `__unCertainNom__()` : Elles sont appelées des <bred><em>méthodes magiques</em></bred> ou <bred><em>méthodes spéciales</em></bred> sur les chaînes.  
    En pratique cela signifie que :

    * elles sont accessibles via la **syntaxe normale** pour les méthodes : `nomChaine.__nomMethodeMagique__()`
    * elles sont **également** accessibles via une **syntaxe spéciale / magique** (qui dépend de la méthode en question)

!!! exp "de Méthodes magiques / Méthodes spéciales sur les Chaînes"
    On se donne deux chaînes `mot1="bonjour"` et `mot2="maman"`

    * `__len()__` : calcule la longueur d'une chaîne ...
    * **Syntaxe normale** : `mot1.__len__()` renvoie le nombre $7$
    * **Syntaxe spéciale** : `len(mot1)` renvoie le nombre $7$
    * `__eq()__` : teste l'égalité entre deux chaînes ...
    * **Syntaxe normale** : `mot1.__eq__(mot2)` renvoie `False` car `mot1` et `mot2` ne sont pas égales
    * **Syntaxe spéciale** : `mot1 == mot2` renvoie `False` (pour les mêmes raisons)
    Principe Général : **À chaque fois qu'on veut tester l'égalité entre deux chaînes avec le symbole `==`, c'est en fait la méthode magique `__eq__()` qui est appelée pour tester l'égalité**.

    Voici quelques autres méthodes magiques sur les chaînes :

    * `__ne__()` veut dire $\ne$ : "<red>N</red>ot <red>E</red>qual to" c'est-à-dire *Non égal*, donc `! =` en Python
    * `__gt__()` veut dire $\gt$ : "<red>G</red>reater <red>T</red>han" c'est-à-dire *Supérieur Strictement*
    * `__ge__()` veut dire $\ge$ : "<red>G</red>reater than or <red>E</red>qual to" c'est-à-dire *Supérieur ou égal à*
    * `__lt__()` veut dire $\lt$ : "<red>L</red>ess <red>T</red>han" c'est-à-dire *Inférieur Strictement*
    * `__le__()` veut dire $\le$ : "<red>L</red>ess than or <red>E</red>qual to" c'est-à-dire *Inférieur ou égal à*
    * `__add__()` correspond à l'opération `+` : pour la concaténation de deux chaînes
    * `__mul__()` correspond à l'opération `*` : pour la multiplication entre une chaîne et un entier
    * `__contains__()` correspond au mot-clé `in` utilisé pour tester l'inclusion d'une chaîne dans une autre
    * :warning: `__repr__()` :warning: ***représente*** une chaîne dans un interpréteur Python, c'est-à-dire qu'il affiche une chaîne dans un interpréteur Python, *sous un certain format spécifique*.
    Elle est appelée quand on tape dans l'interpréteur :
    * ou bien `>>> phrase1` $\quad$ (où `phrase1` désigne le nom d'une chaîne)
    * ou bien `>>> print(phrase1)`
    * :warning: `__str__()` :warning: ***représente*** une liste dans un interpréteur Python, c'est-à-dire qu'il affiche une liste dans un interpréteur Python, *sous un certain format spécifique*, **mais seulement pour le `print()`**
    * `>>> print(l1)`
    * etc...


## Opérations Arithmétiques sur les Chaînes

### Addition

```python
>>> a = "Bonjour"
>>> b = "Maman"
>>> a+b   # ceci est la CONCATÉNATION de a et b
"BonjourMaman"
```

ATTENTION : Pas de Soustraction

### Multiplication

```python
>>> a = "Bonjour"
>>> a*2
"BonjourBonjour"
```

ATTENTION :

* le produit de deux chaînes n'existe pas
* Pas de Division

## Les chaînes sont *immutables*

Par exemple, **on NE peut PAS modifier directement** un caractère d'une chaîne `str` ***in situ***, par affectation directe (**EN CONSERVANT LA MÊME ADRESSE MÉMOIRE**, ce que l'on appelle un ***pointeur*** vers le début de la chaîne).
On dit que les chaînes `str` NE supportent PAS l'**affection d'éléments** :fr: ou les **item assignment** :gb: :

```python
>>> mot = "Bonjour maman"
>>> id(mot) # renvoie l'adresse mémoire du début du tuple
# Exemple de réponse:
140164595891824
# affectation d'élément / item assignment IMPOSSIBLE :
>>> mot[3] = "z"
TypeError: 'str' object does not support item assignment
```

Plus généralement, **on NE peut PAS modifier** le contenu d'une variable de type `str` ***in situ***, c'est-à-dire EN CONSERVANT LA MÊME ADRESSE MÉMOIRE: Pour commencer, parce qu'il n'existe aucune méthode sur les tuples...

```python
>>> mot = "Bonjour maman"
>>> id(mot) # renvoie l'adresse mémoire du début de la chaîne 'mot'
# Exemple de réponse:
140164595891824
# 'ajout' de caractère en fin de chaîne :
>>> mot = mot + "!"
>>> id(mot) # la 'nouvelle' adresse mémoire de la chaîne 'mot' est MODIFIÉE
            # (ici, ajout de caractères en fin de chaîne)
140164595608432
>>> mot = mot[:3]+"z"+mot[4:]
>>> id(mot)
140213654109104
```

On s'aperçoit que les deux adresses mémoires, ou ***pointeurs***, AVANT et APRÈS modification de la chaîne, **NE SONT PLUS** égales.

!!! pte "Immutabilité des Chaînes"
    Les chaînes de caractères `str` sont ***immutables***.

<env>Remarque</env> Le fait que les chaînes soient immutables laisse à penser qu'il s'agit d'un type (/structure) de données Python qui **N'est PAS prévu** pour être modifié **régulièrement avec une bonne efficacité**.




