# Exercices Chaînes et Fonctions en Python

Chaque exercice ci-dessous utilise la boucle `for`.

!!! ex
    On rappelle que les voyelles sont les lettres `aeiouy` (on pourra négliger, ou pas.., les voyelles accentuées)

    Créer une fonction `voyelles(phrase:str)->str` qui :

    * reçoit en entrée une `phrase`
    * renvoie en sortie la même `phrase` à laquelle on a enlevé toutes ses voyelles

!!! ex
    Créer une fonction `nombre_dans(lettre:str, phrase:str)->int` qui :

    * reçoit en entrée une `lettre`, et une `phrase`
    * renvoie en sortie le nombre d'occurences (combien y en a t il?) de la `lettre` (passée en entrée) dans la `phrase` (passée en entrée)

!!! ex
    Créer une fonction `remplace(lettre:str, chaine:str, phrase:str)->str` qui :

    * reçoit en entrée une `lettre`, une `chaine` et une `phrase`
    * renvoie en sortie une chaine de caractères `nouvelle` dans laquelle toutes les occurences de la `lettre` (reçue en entrée) sont remplacées par `chaine` dans la `phrase`

    Par exemple, Si

    * `lettre='o'`
    * `chaîne='oha'`
    * `phrase='allo maman'`

    Alors la phrase renvoyée doit être

    `alloha maman`

!!! ex
    On rappelle les codes ASCII des principaux caractères alphabétiques :

    | Caractère | A | B | C | ... | Z |
    | :-: |:-:|:-:|:-:|:-:|:-:|
    | Code ASCII<br/>Ordinal | $65$ | $66$ | $67$ | ... | $90$ |

    et

    | Caractère | a | b | c | ... | z |
    |:-: |:-:|:-:|:-:|:-:|:-:|
    | Code ASCII<br/>Ordinal | $97$ | $98$ | $99$ | ... | $122$ |

    On rappelle également que les fonctions `chr()` et `ord()` permettent les conversion de l'une des ligne de ce tableau vers l'autre (à vous de vous en souvenir..)

    1. Créer une fonction `vers_maj(phrase:str)->str` qui :

        * reçoit en entrée une phrase, prétendument en minuscules (pon pourra faire comme si)
        * renvoie en sortie la même phrase en MAJUSCULES

    2. Modifier l'algorihtme précédent pour qu'il traite de manière neutre les caractères contenus dans la variable `neutre = '.,:!'` : cela veut dire que ces caractères ne doivent pas être modifiés, et doivent être réinsérés tels quels dans la phrase en majuscules, en sortie.

!!! ex
    Créer une fonction `freq(c:str, phrase:str)->float` qui :

    * reçoit en entrée deux variables :
        * un caractère `c:str`
        * une phrase `phrase:str`
    * renvoie en sortie la frequence `f:float` du caractère reçu en entrée, dans la `phrase` donnée en entrée.

    Par exemple, si :

    * `c = "a"`
    * `phrase = "allo maman"`

    Alors la fréquence du caractère `c="a"` dans la phrase "allo maman"` peut être calculée comme suit :

    * nombre de `a` dans la phrase = $3$ lettres `a` dans la phrase
    * longueur de la phrase = $10$ caractères dans la phrase (l'espace compte) 
    * donc la fréquence vaut $f=\dfrac {3}{10}= 0.3$. Le nombre flottant à renvoyer dans ce cas est donc $0.3$

!!! ex
    Créer une fonction `estOK(mdp:str)->bool` qui:

    * reçoit en entrée un mot de passe `mdp` (une châine de caractères)
    * renvoie en sortie :
        * `True` lorsque le mot de passe vérifie (TOUTES) les règles suivantes
            * au moins 10 caractères dans le `mdp`
            * le `mdp` doit contenir au moins une minuscule
            * le `mdp` doit contenir au moins une MAJUSCULE
            * le `mdp` doit contenir au moins un chiffre (de $0$ à $9$)
        * `False` sinon

!!! ex
    1. Créer une fonction `renverse(phrase:str)->str` qui:

        * reçoit en entrée une `phrase` (chaîne de caractères)
        * renvoie en sortie la même phrase mais écrite à l'envers

        Par exemple, la phrase initiale `allo maman` doit être renvoyée comme la chaîne `namam olla`

    2. (En déduire/) Créer une (deuxième) fonction `estPalindrome(mot:str)->bool` qui (peut utiliser la fonction précédente) :

        * reçoit en entrée un `mot` (chaîne de caractères)
        * renvoie en sortie le booléen :
            * `True` si ce mot est un **palindrome**
            * `False` sinon

        !!! def
            Rappelons qu'un palindrome est un mot (en fait, plus généralement une phrase) qui s'écrit de la même manière à l'endroit (de gauche à droite) et à l'envers (de droite à gauche). Par exemple, le mot **KAYAK** est un palindrome.