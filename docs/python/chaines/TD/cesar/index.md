# TD Chiffrement de César

## Introduction Historique

### Avant les Romains

L'une des premières techniques de Chiffrement, au $V$ème Siècle av J.C., est utilisée par les Hébreux pour les textes religieux. La méthode consiste à :

* remplacer un $A$ par un $Z$
* remplacer un $B$ par un $Y$, etc...

Cette méthode est appelée _**[atbash](https://fr.wikipedia.org/wiki/Atbash)**_, du nom des 1ères et dernières lettres de l'alphabet Hébreu : <b>a</b>leph ($\aleph=A$), <b>t</b>av (ת$\approx Z$), <b>b</b>eth ($\beth=B$), <b>sh</b>in (ש$\approx Y$).  
Il s'agit du premier exemple dans l'Histoire d'une méthode cryptographique connue de nos jours sous le nom de **chiffrement par substitution monoalphabétique** :

!!! def
    Un <red>Chiffrement par Substitution monoalphabétique</red> est une méthode cryptographique que l'on peut définir par :
    
    * On remplace/substitue une lettre par une (autre) lettre
    * une même lettre de départ étant toujours chiffrée par la même lettre d'arrivée

### Les Romains

Quelques Siècles par tard, l'armée Romaine, via son commandeur en Chef **Jules César, au $I$er Siècle av. J.C.**, utilisait le codage suivant pour communiquer avec ses troupes situées quelquefois très loin de Rome.

!!! def
    Le <red>codage de César</red>, ou <red>chiffrement de César</red>, ou <red>chiffre de César</red>, consiste à :

    * coder une lettre en une autre lettre, de la manière suivante :
    * On décale chaque lettre d'un certain nombre de **crans**, ou de **rangs**, vers la droite ou vers la gauche : c'est le <red>décalage (de chiffrement)</red>. On le note usuellement `d` ou `decalage`

    Il s'agit donc d'un autre exemple de **Chiffrement par Substitution monoalphabétique**

Par exemple `d = +3` signifie qu'on décale chaque lettre de 3 rangs vers la droite (`+` = vers la droite, `-` = vers la gauche)

<center>

<img src="img/cesar.svg" style="width:45%;">

<figcaption>Chiffrement de César/ par Décalage</figcaption>

</center>

### But de ce TD

Le but de ce TD est de créer des fonctions pour:

* ***préparer***/nettoyer un texte initial, au sens où on enleve tous les **signes diacritiques** (lettres accentuées, ç, ñ, etc..), on le met en majuscules, on lui enlève tout autre caractère
* **Cryptographie** : Chiffrer un texte avec le chiffrement de César
* **Cryptanalyse** : Déchiffrer un texte, en connaissant à l'avance, **ou pas**, le décalage `d` utilisé
* Apprendre à **lire**, et **écrire** dans des fichiers texte avec Python

## Préparation du texte

1. Créer une fonction `sans_accents(texte:str)->str` qui accepte en entrée un `texte:str` pouvant contenir des **signes diacritiques (caractères accentués, ç, ñ, etc..)**, et qui renvoie en sortie, **le même texte mais sans les accents, ni aucun signe diacritique**. 
On devra pour cela, utiliser scrupuleusement le tableau de remplacement des caractères détaillé sur [cette page](./AltCodesUTF8.md) qui résume :

    * les ***Alt Codes / Point de Code Unicode*** permettant de taper ces signes diacritiques (caractères accentués/spéciaux, etc..) dans votre IDE et votre OS favoris, et également,
    * par quel.s caractère.s précisément, la fonction `sans_accents()` doit le/les remplacer.
  
    !!! remarque
        Le but de la fonction `sans_accents()` est purement pédagogique : il manquera inévitablement de nombreux caractères accentués à remplacer (on ne va pas tous les traiter, c'est trop long), mais on pourra au moins en traiter quelques uns, comme première approche...

2. Créer une fonction `vers_maj(texte:str)` qui accepte en entrée une variable `texte:str`, et qui renvoie en sortie **le même texte en majuscules** (seules les lettres de l'alphabet minuscules doivent être transformées)

3. Créer une fonction `sans_autres_caract(texte:str)->str` qui supprime de le `texte` **tous les caractères autres que des lettres de l'alphabet**
Exemple : Suppression de `.,;:!-_*` etc...

4. Créer une fonction `preparer(texte:str)->str` qui prépare le ***texte clair non nettoyé*** :

    * supprimer les accents, et remplacer les caractères **avec signes diacritiques** (ç, ñ, etc..)
    * mettre le texte en majuscule
    * supprimer tous les autres caractères (autres ques lettres de l'alphabet)

5. Créer une fonction `afficher(texte:str)->None` qui accepte en entrée un `texte` clair déjà preparé, et qui renvoie en sortie le même texte nettoyé, mis en forme **de la manière suivante** :

    * Lettres regroupées par paquets de $5$, 
    * Les paquets de $5$ lettres (Colonnes) sont séparés par des espaces
    * le tout sur $8$ colonnes (de $5$ lettres), ensuite on doit forcer le retour à la ligne `\n`

    Exemple d'affichage attendu :
    ```python
    ABCDE XYZTE GKEID PANVL JEIDK SSEOV JZOCD AZEOF
    GFDRS FQOES FLEOF QORNV DXEOP DAPEI CNWBF QDLDP
    FPEOG QNDLG MPOEZ DLEOZ LFPEM FLEPA XLKIO FDKQO
    GKTOP FGX
    etc..
    ```

<newpage></newpage>

## Cryptographie

6. Créer une fonction `chiffrer(texte:str,decalage:int)->str` ou `coder(texte:str, decalage:int)->str`, qui :
    * accepte en entrée les paramètres suivants :
      * un texte `texte:str` à présenter, ainsi que
      * le **`decalage` de chiffrement**, qui est **le nombre de rangs décalés (vers la droite, si positif)**
    * renvoie en sortie le même **texte chiffré** (dont chaque lettre a été décalée - droite ou gauche - d'un nombre de rangs égal à `decalage`)
    Exemple : Pour `decalage=3`, la lettre `A` est chiffrée en `D` ($A \rightarrow B \rightarrow C \rightarrow D$) donc décalée de 3 rangs vers la droite

## Cryptanalyse

7. Créer une fonction `dechiffrer_connu(texteChiffre:str,decalage:int)` qui déchiffre un texte chiffré **dont on connaît à l'avance le décalage DE CHIFFREMENT**.

8. On adopte maintenant le point de vue de l'attaquant que l'on appelle la **cryptanalyse**, consistant à:
    * ne connaître QUE le texte chiffré
    * En particulier, on ne connaît plus le texte en clair, ni le décalage de chiffrement

    Quelles stratégies de décodage (**cryptanalyse**) peut-on adopter dans ce cas-là?

9. Écrire une fonction `frequences(texte:str)->list` qui :
    * reçoit en entrée un paramètre `texte` contenant une certaine chaîne de caractère
    * qui renvoie en sortie, la liste de toutes les fréquences des lettres de l'alphabet (majuscules) présentes (ou pas) dans la `texte` (qui est censée être déjà préparée, càd qu'elle ne contient QUE des lettres de l'alphabet majuscules, de `A -> Z`)
    Exemple de renvoi attendu: `[4,6,0, ..., 20]` modélise :
        * `4` occurences de la lettre `A`
        * `6` occurences de la lettre `B`
        * `0` (donc aucune) occurence.s de la lettre `C`
        ...
        * `20` occurences de la lettre `Z`

10. Créer une fonction `maxi(l:list)->float` qui a:
    * reçoit en entrée une liste d'entiers correspondant aux occurences des lettres de l'alphabet (majuscule) dans ma texte chiffré.
    * renvoie en sortie **la valeur maximale** de cette liste `l`

11. Créer une fonction `mini(l:list)->float` qui a:
    * reçoit en entrée une liste d'entiers correspondant aux occurences des lettres de l'alphabet (majuscule) dans ma texte chiffré.
    * renvoie en sortie **la valeur minimale** de cette liste `l`

12. Créer une fonction `indice_du_max_dans(l:list)->int` qui a:
    * reçoit en entrée une liste d'entiers correspondant aux occurences des lettres de l'alphabet (majuscule) dans ma texte chiffré.
    * renvoie en sortie l'**indice correspondant à la valeur maximale** de cette liste `l`

<newpage></newpage>

## Lecture & Écriture de Fichiers

13. Créer une fonction `get_texte(name:str)->str` qui :
    * Ouvre **en lecture seule** le fichier `name` et 
    * stocke son contenu dans une variable `texte` de type `str`, **préparé** (en majuscules, sans les espaces ni autres caractères non alphabétiques, etc..)

14. Créer une fonction `ecrire_dans_fichier(texte:str, name:str)->None` qui :
    * Ouvre **en écriture** le fichier `name` et 
    * écrit le contenu de la variable `texte` de type `str`, dans le fichier `name`, mais **mis en forme de la manière suivante** :
      * sans accents, en majuscules, sans caractères non alphabétiques
      * regroupement des lettres : par groupes de 5
      * 8 colonnes (de 5 lettres) en tout, ensuite retour à la ligne `\n` obligatoire 

## Cryptanalyse (suite)

15. Créer une fonction `get_caract_le_plus_frequent(texte:str)->str` qui :
    * reçoit en entrée un `texte` de type `str` et
    * renvoie en sortie le caractère le plus fréquent du `texte`

16. Créer une fonction `decalage(texteChiffre:str)->int` qui :
    * reçoit en entrée un `texteChiffre`
    * calcule puis renvoie le `decalage` (un nombre entier donc) qu'on a utilisé pour **chiffrer** ce texte

17. Créer une fonction `dechiffrer(texteChiffre:str)->str` qui :
    * reçoit en entrée une `texteChiffre`
    * renvoie le décodage/déchiffrage, appelé `texteClair`, du `texteChiffre`, grâce au décalage calculé grâce à la fonction `decalage()` précédente

18. Créer une fonction/procédure `dechiffrer_fichier(nameEntree:str, nameSortie:str)->None` qui :
    * reçoit en entrée :
        * le nom `nameEntree` du fichier (déjà existant) **contenant le texte Chiffré**, et 
        * le nom `nameSortie` du fichier (à créer), dans lequel la fonction doit écrire le texte Déchiffré 
    * crée et ouvre en écriture un fichier `nameSortie` et écrit dedans le texte Déchiffré.
    Par exemple : `nameEntree="texteInitial.txt"` et `nameSortie="texteDechiffre.txt"`

## Défie tes amis! :sunglasses: :metal:

19. Trouver sur internet un livre quelconque de ton choix, au format `.txt`, téléchargez-le, puis placer-le dans le même répertoire que votre `fichier.py`.  

    On pourra par exemple en choisir un, tombé dans le domaine public, en français, sur le site du [Projet Gutenberg.org](https://www.gutenberg.org/browse/languages/fr). Vous ne savez pas lequel choisir ? Télécharger [Une Saison en Enfer, d'Arthur Rimbaud](https://www.gutenberg.org/cache/epub/56668/pg56668.txt)

    Renommez le fichier téléchargé de votre livre en `monLivre.txt`

20. Créer une fonction `defi_jeu(filenameEntree,filenameSortie)`, qui **automatise le chiffrement de César** d'un livre stocké dans `"monLivre.txt"`, ainsi cette fonction :
    * reçoit en entrée une chaîne de caractère `name="monLivre.txt"` correspondant au nom du fichier texte qui contient ton livre quelconque téléchargé sur internet (au format `.txt`, et à l'état brut : sans aucun traitement).
    * **prépare** le texte contenu dans le fichier `name`, au sens de : enlève les signes diacritiques, mets en majuscules, enlève les autres caractères non alphabétiques, et stocke le résultat dans une variable `texteClair` de type `str`
    * Calcule un nombre entier aléatoire `decalage` compris entre `1` et `25`
     (pourquoi pas `0`? pourquoi pas plus que `25`? pourquoi pas moins de `0`? :astonished:)
     Afficher cette valeur dans le terminal (pour que vous puissiez connaître le décalage que vous utilisez...)
    * Chiffre le `texteClair` avec le `decalage` calculé aléatoirement (dans le point précédent), et 
    * stocke le résultat dans une variable `texteChiffre` de type `str`
    * Enregistre le contenu de la variable `texteChiffre` dans un fichier `"defi.txt"` (à placer dans le même répertoire que votre `fichier.py`)
  Défie tes amis !! :sunglasses: :metal: envoie-leur ton fichier et mets les au défi de le décoder !! Tu n'as pas d'amis :stuck_out_tongue_winking_eye: ? Essaye de décoder [ce livre](./texteChiffre.txt)
