def sans_accents(texte:str)->str:
  newtexte = ""
  for c in texte:
    if c in "àáâãäåā": # c est un caract A accentué
      c = "a"
    elif c in "ÀÁÂÃÄÅĀ":
      c = "A"
    elif c in "æ":
      c="ae"
    elif c in "Æ":
      c = "AE"
    elif c in "èéêẽëēĕ": # c est un caract E accentué
      c = "e"
    elif c in "ÈÉÊẼËĒĔ":
      c = "E"
    elif c in "ç": # C est une cédille
      c = "c"
    elif c in "Ç":
      c = "C"
    elif c in "ìíîĩïīĭ": # c est un caract I accentué
      c = "i"
    elif c in "ÌÍÎĨÏĪĬ":
      c = "I"
    elif c in "ñ":
      c = "n"
    elif c in "Ñ":
      c = "N"
    elif c in "òóôõöøōŏ": # c est un caract O accentué
      c = "o"
    elif c in "ÒÓÔÕÖØŌŎ":
      c = "O"
    elif c in "œ":
      c = "oe"
    elif c in "Œ":
      c = "OE"
    elif c in "ß":
      c = "ss"
    elif c in "ẞ":
      c = "SS"
    elif c in "š":
      c = "sh"
    elif c in "Š":
      c = "SH"
    elif c in "ðþ":
      c = "th"
    elif c in "ÐÞ":
      c = "TH"    
    elif c in "ùúûũüūŭ": # c est un caract U accentué
      c = "u"
    elif c in "ÙÚÛŨÜŪŬ":
      c = "U"
    elif c in "ỳýŷỹÿȳ": # c est un caract Y accentué
      c = "y"
    elif c in "ỲÝŶỸŸȲ":
      c = "Y"
    elif c in "ž":
      c = "j"
    elif c in "Ž":
      c = "J"
    newtexte += c
  return newtexte

def vers_maj(texte:str)->str:
  texte = sans_accents(texte)
  newtexte = ""
  for c in texte:
    if c in "abcdefghijklmnopqrstuvwxyz":
      c = chr(ord(c)-32)
    newtexte += c
  return newtexte

def sans_autres_caract(texte:str)->str:
  texte = sans_accents(texte)
  texte = vers_maj(texte)
  newtexte = ""
  for c in texte:
    if c not in "ABCDEFGHIJKLMNOPQRSTUVWXYZ":
      c = ""
    newtexte += c
  return newtexte

def preparer(texte:str)->str:
  texte = sans_accents(texte)
  texte = vers_maj(texte)
  texte = sans_autres_caract(texte)
  return texte

def afficher(texte:str)->str:
  compteur = 0
  newtexte = ""
  for c in texte:
    compteur += 1
    if compteur %40 == 0:
      c+="\n"
    elif compteur %5 == 0:
      c += " "
    newtexte += c
  print(newtexte)

def chiffrer(texte:str, decalage:int)->str:
  newtexte = ""
  for c in texte:
    cChiffre = chr((ord(c)-65+decalage)%26+65)
    newtexte += cChiffre
  return newtexte

def dechiffrer_connu(texteChiffre:str,decalage:int)->str:
  return chiffrer(texteChiffre,-decalage)

# def frequence_caract(caract:str,texte)->int:
#   compteur = 0
#   for c in texte:
#     if c == caract :
#       compteur += 1
#   return compteur

# def frequences(texte:str)->list:
#   freq = []
#   for caract in alphabet:
#     freq.append(frequence_caract(caract,texte))
#   return freq

# def frequences(texteChiffre:str)->list:
#   freq = []
#   for lettre in alphabet:
#     nbCaractLettre = 0
#     for c in texteChiffre:
#       if c == lettre:
#         nbCaractLettre += 1
#     freq.append(nbCaractLettre)
#   return freq

def frequences(texte:str)->list:
  freq = [0]*26
  for c in texte:
    i = ord(c)-65
    freq[i] += 1
  return freq

def maxi(l:list)->int:
  maximum = l[0]
  for el in l:
    if el > maximum:
      maximum = el
  return maximum

def mini(l:list)->int:
  minimum = l[0]
  for el in l:
    if el < minimum:
      minimum = el
  return minimum

def indice_du_max_dans(l:list)->int:
  """Indice où se trouve le maximum dans 'l'"""
  maximum = l[0]
  indice = 0
  for i in range(len(l)):
    if l[i] > maximum:
      maximum = l[i]
      indice = i
  return indice

## FICHIERS

def get_texte(name:str)->str:
  with open(name, "r") as monFichier:
    texte = monFichier.read()
  texte = preparer(texte)
  return texte

def ecrire_dans_fichier(texte:str,name:str)->None:
  texte = preparer(texte)
  # with open("texteChiffre.txt","w") as monFichier:
  with open(name,"w") as monFichier:
    compte = 0
    ligne = ""
    for c in texte:
      ligne += c
      compte += 1
      if compte % 5 == 0 and compte %40 != 0:
          ligne += " "
      if compte == 40:
        ligne += "\n"
        monFichier.write(ligne)
        compte = 0
        ligne = ""

def get_caract_le_plus_frequent(texte:str)->str:
  freq = frequences(texte)
  i = indice_du_max_dans(freq)
  lettreLaPlusFreq = chr(65+i)
  return lettreLaPlusFreq

def decalage(texteChiffre:str)->int:
  cpf = get_caract_le_plus_frequent(texteChiffre)
  d = ord(cpf) - ord("E")
  return d

def dechiffrer(texteChiffre:str)->str:
  d = decalage(texteChiffre)
  texteClair = dechiffrer_connu(texteChiffre,d)
  return texteClair

def dechiffrer_fichier(nameEntree:str, nameSortie:str)->None:
  texteChiffre = get_texte(nameEntree)
  texteClair = dechiffrer(texteChiffre)
  ecrire_dans_fichier(texteClair,nameSortie)

def defi_jeu(filenameEntree:str,filenameSortie:str)->None:
  texte = get_texte(filenameEntree)
  texte = preparer(texte)
  from random import randint
  d = randint(1,25)
  texteChiffre = chiffrer(texte,d)
  ecrire_dans_fichier(texteChiffre,filenameSortie)

alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
# defi_jeu("LivreEleve.txt","LivreEleveChiffre.txt")
dechiffrer_fichier("LivreEleveChiffre.txt","LivreEleveDechiffre.txt")


# texteChiffre = get_texte("texteChiffre.txt")
# texteClair = dechiffrer(texteChiffre)
# print(f"texteClair = {texteClair}")
# dechiffrer_fichier("texteChiffre.txt","texteClair.txt")
# defi_jeu("texteInitial.txt","texteInitialChiffre.txt")






