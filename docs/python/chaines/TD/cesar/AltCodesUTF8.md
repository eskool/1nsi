# Alt Codes et Points de Code Unicode<br/> des Principales Lettres Accentuées

<center>

|Lettre|Nom|Alt Codes Windows<br/>(en Décimal) <br/> = Entité HTML<br/> `&#----;`|Points de Code Unicode <br/>(en Hexa) (Linux/Mac) <br/> = Entité HTML Hexa <br/>`&#x--;`| À remplacer par|
|:-:|:-:|:-:|:-:|:-:|
| À / à |a grave            |Alt+`0192` / `0224`|Ctrl+Maj+u+`C0`/`E0`|A|
| Á / á |a aigu             |Alt+`0193` / `0225`|Ctrl+Maj+u+`C1`/`E1`|A|
| Â / â |a circonflexe      |Alt+`0194` / `0226`|Ctrl+Maj+u+`C2`/`E2`|A|
| Ã / ã |a tilde            |Alt+`0195` / `0227`|Ctrl+Maj+u+`C3`/`E3`|A|
| Ä / ä |a tréma/umlaut     |Alt+`0196` / `0228`|Ctrl+Maj+u+`C4`/`E4`|A|
| Å / å |A rond en chef     |Alt+`0197` / `0229`|Ctrl+Maj+u+`C5`/`E5`|A|
| Æ / æ |E dans l'A         |Alt+`0198` / `0230`|Ctrl+Maj+u+`C6`/`E6`|AE|
| Ā / ā |a macron           |Alt+`0256` / `0257`|Ctrl+Maj+u+`100`/`101`|A|
| Ă / ă |a brève            |Alt+`0258` / `0259`|Ctrl+Maj+u+`102`/`103`|A|
| Ç / ç |C cédille          |Alt+`0199` / `0231`|Ctrl+Maj+u+`C7`/`E7`|C|
| È / è |e grave            |Alt+`0200` / `0232`|Ctrl+Maj+u+`C8`/`E8`|E|
| É / é |e aigu             |Alt+`0201` / `0233`|Ctrl+Maj+u+`C9`/`E9`|E|
| Ê / ê |e circonflexe      |Alt+`0202` / `0234`|Ctrl+Maj+u+`CA`/`EA`|E|
| Ẽ / ẽ |e tilde            |Alt+`7868` / `7869`|Ctrl+Maj+u+`1EBC`/`1EBD`|E|
| Ë / ë |e tréma/umlaut     |Alt+`0203` / `0235`|Ctrl+Maj+u+`CB`/`EB`|E|
| Ē / ē |e macron           |Alt+`0274` / `0275`|Ctrl+Maj+u+`112`/`113`|E|
| Ĕ / ĕ |e brève            |Alt+`0276` / `0277`|Ctrl+Maj+u+`114`/`115`|E|

<newpage></newpage>

|Lettre|Nom|Alt Codes Windows<br/>(en Décimal) <br/> = Entité HTML<br/> `&#----;`|Points de Code Unicode <br/>(en Hexa) (Linux/Mac) <br/> = Entité HTML Hexa <br/>`&#x--;`| À remplacer par|
|:-:|:-:|:-:|:-:|:-:|
| Ì / ì |i grave            |Alt+`0204` / `0236`|Ctrl+Maj+u+`CC`/`EC`|I|
| Í / í |i aigu             |Alt+`0205` / `0237`|Ctrl+Maj+u+`CD`/`ED`|I|
| Î / î |i circonflexe      |Alt+`0206` / `0238`|Ctrl+Maj+u+`CE`/`EE`|I|
| Ĩ / ĩ |i tilde            |Alt+`0296` / `0297`|Ctrl+Maj+u+`128`/`129`|I|
| Ï / ï |i tréma/umlaut     |Alt+`0207` / `0239`|Ctrl+Maj+u+`CF`/`EF`|I|
| Ī / ī |i macron           |Alt+`0298` / `0299`|Ctrl+Maj+u+`12A`/`12B`|I|
| Ĭ / ĭ |i brève            |Alt+`300` / `301`|Ctrl+Maj+u+`12C`/`12D`|I|
| Ð / ð |eth / ed islandais |Alt+`0208` / `0240`|Ctrl+Maj+u+`D0`/`F0`|TH|
| Ñ / ñ |Egne Espagnole     |Alt+`0209` / `0241`|Ctrl+Maj+u+`D1`/`F1`|N|
| Ò / ò |o grave            |Alt+`0210` / `0242`|Ctrl+Maj+u+`D2`/`F2`|O|
| Ó / ó |o aigu             |Alt+`0211` / `0243`|Ctrl+Maj+u+`D3`/`F3`|O|
| Ô / ô |o circonflexe      |Alt+`0212` / `0244`|Ctrl+Maj+u+`D4`/`F4`|O|
| Õ / õ |o tilde            |Alt+`0213` / `0245`|Ctrl+Maj+u+`D5`/`F5`|O|
| Ö / ö |o tréma/umlaut     |Alt+`0214` / `0246`|Ctrl+Maj+u+`D6`/`F6`|O|
| Ø / ø |o barré            |Alt+`0216` / `0248`|Ctrl+Maj+u+`D8`/`F8`|O|
| Ō / ō |o macron           |Alt+`332` / `333`|Ctrl+Maj+u+`14C`/`14D`|O|
| Ŏ / ŏ |o brève            |Alt+`334` / `335`|Ctrl+Maj+u+`14E`/`14F`|O|
| Œ / œ |E dans l'O         |Alt+`0140` / `0156`|Ctrl+Maj+u+`152`/`153`|OE|
| Š / š |s caron/háček      |Alt+`0138` / `0154`|Ctrl+Maj+u+`160`/`161`|SH|
| Þ / þ |Thorn (islandais)  |Alt+`232` / `231`|Ctrl+Maj+u+`DE`/`FE`|TH|

<newpage></newpage>

|Lettre|Nom|Alt Codes Windows<br/>(en Décimal) <br/> = Entité HTML<br/> `&#----;`|Points de Code Unicode <br/>(en Hexa) (Linux/Mac) <br/> = Entité HTML Hexa <br/>`&#x--;`| À remplacer par|
|:-:|:-:|:-:|:-:|:-:|
| Ù / ù |u grave            |Alt+`0217` / `0249`|Ctrl+Maj+u+`D9`/`F9`|U|
| Ú / ú |u aigu             |Alt+`0218` / `0250`|Ctrl+Maj+u+`DA`/`FA`|U|
| Û / û |u circonflexe      |Alt+`0219` / `0246`|Ctrl+Maj+u+`DB`/`FB`|U|
| Ũ / ũ |u tilde            |Alt+`360` / `361`|Ctrl+Maj+u+`168`/`169`|U|
| Ü / ü |u tréma/umlaut     |Alt+`0220` / `0247`|Ctrl+Maj+u+`DC`/`FC`|U|
| Ū / ū |u macron           |Alt+`362` / `363`|Ctrl+Maj+u+`16A`/`16B`|U|
| Ŭ / ŭ |u brève            |Alt+`364` / `365`|Ctrl+Maj+u+`16C`/`16D`|U|
| ẞ / ß |Eszet Allemand     |Alt+ `7838` / `0223`|Ctrl+Maj+u+ `1E9E` / `DF`|SS|
| Ỳ / ỳ |y grave            |Alt+`7922` / `7923`|Ctrl+Maj+u+`1EF2` / `1EF3`|Y|
| Ý / ý |y aigu             |Alt+`0221` / `0253`|Ctrl+Maj+u+`DD` / `FD`|Y|
| Ŷ / ŷ |y circonflexe      |Alt+`374` / `375`|Ctrl+Maj+u+`176` / `177`|Y|
| Ỹ / ỹ |y tilde            |Alt+`7928` / `7929`|Ctrl+Maj+u+`1EF8` / `1EF9`|Y|
| Ÿ / ÿ |y tréma/umlaut     |Alt+`0159` / `0255`|Ctrl+Maj+u+`178` / `FF`|Y|
| Ȳ / ȳ |y macron           |Alt+`562` / `563`|Ctrl+Maj+u+`232`/`233`|Y|
| Ž / ž |z caron/háček      |Alt+`0142` / `0158`|Ctrl+Maj+u+`17D` / `17E`|J|

</center>

## En Savoir plus

* Majuscules : https://www.compart.com/fr/unicode/category/Lu
* Minuscules : https://www.compart.com/fr/unicode/category/Ll
* Tous les Caractères : https://www.compart.com/fr/unicode