# 1NSI : Exercices Chaînes de Caractères

!!! ex
    Écrire un algorithme qui :
    
    * demande en entrée une chaîne de caractère `phrase`, et qui 
    * affiche en sortie (dans le Terminal) chacun des caractères de la phrase (grâce à une boucle `for`, ou éventuellement une boucle `while`).

!!! ex
    Écrire un algorithme qui :
    
    * demande en entrée une chaîne de caractère `phrase` et qui 
    * compte (et affiche) le nombre de caractères de la chaîne (avec une boucle `for` ou `while`)

!!! ex "Occurences de caractères"
    On se donne une certaine chaîne : `phrase="bonjour, il fait beau aujourd'hui, pas vrai ?"`  
    Dans chacune des questions suivantes, Écrire un algorithme qui détermine, puis affiche en sortie (dans le Terminal) :
    
    1. l'**indice de la première occurence** de la lettre `a` dans la `phrase`
    1. Que faire si la lettre `a` n'existe pas dans la `phrase` ? Quelle réponse donner dans ce cas ? (si aucune occurence de `a`)
    1. l'**indice de la deuxième occurence** de la lettre `a` dans la `phrase`
    1. l'**indice de la dernière occurence** de la lettre `a` dans la `phrase`

!!! ex
    Écrire un algorithme qui demande en entrée une chaîne de caractère `phrase` et qui compte:

    1. le nombre de caractères 'a' minuscules. (avec une boucle *for*)
    1. le nombre de caractères en minuscules. (avec une boucle *for*)
    1. le nombre de caractères en majuscules. (avec une boucle *for*)
    1. le nombre de caractères *spéciaux* parmi la liste suivante : `*,.;:!?&#$£`

!!! ex
    Écrire un algorithme qui demande en entrée, de manière ininterrompue,  une chaîne de caractère `password`, et qui continue de le redemander tant que la réponse donnée par l'utilisateur ne vérifie pas les contraintes suivantes :

    * `password` doit contenir au minimum $10$ caractères
    * `password` doit contenir au minimum une **minuscule** (de `a` à `z`)
    * `password` doit contenir au minimum une **MAJUSCULE** (de `A` à `Z`)
    * `password` doit contenir au minimum un **chiffre** (de `0` à `9`)
    * `password` doit contenir au minimum un **caractère spécial** parmi `*,.;:!?&#$£`

    Par contre, si le mot de passe répondu par l'utilisateur vérifie TOUTES les contraintes, alors l'algorithme doit afficher en sortie "MOT DE PASSE CORRECT"

!!! ex
    Écrire un algorithme qui :

    * demande en entrée une chaîne `phrase`
    * et qui renvoie en sortie
        * la même chaîne sans la lettre `a` (ou sans la lettre `b`, sans la `,`, etc...)
        * Modifier l'algorithme précédent pour qu'il renvoie en sortie la même chaîne phrase donnée en entrée, mais en enlevant tous les caractères placés dans la chaîne `enlever = "ab*!"`

!!! ex "fonctions `ord()` et `chr()`"
    Cet exercice suppose ne pas utiliser les fonctionnalités plus avancées de Python (en particulier pas `.upper()`, ni `.lower()` )

    * Écrire un algorithme qui demande en entrée une chaîne de caractère *mot*, en minuscules, et qui la convertit caractère par caractère en majuscules. (avec une boucle *for*)
    * Écrire un algorithme qui fait le contraire : il transforme toutes les chaînes majuscules en minuscules, caractère par caractère.

