# 1NSI : Installer et Configurer Anaconda pour Python, sur Windows 10/11

## Télécharger & Installer Anaconda

* Aller sur la page officielle : [https://www.anaconda.com/download](https://www.anaconda.com/download)
* Entrer un email (personnel, ou pas..), puis cliquer sur `Submit`

    ![Page de Download](./img_all_users/1.png)

* Cliquer sur `Windows -> Python 3.12 64-Bit Graphical Installer (912.3M)` pour **télécharger**

    ![Page Download Now](./img_all_users/2.png)

* Une fois le fichier téléchargé, doucle-cliquez sur le fichier `Anaconda3-2024.10-1-Windows-x86_64.exe`

    ![Lancer l'installeur](./img_just_me/3.png)

* Welcome to Anaconda (rien à faire), aller sur la page suivante : --> `Next`

    ![Welcome to Anaconda --> Next](./img_just_me/5.png)

* Accepter la `License Agreement` --> `I agree`

    ![Accepter la License Agreement --> 'I agree'](./img_just_me/6.png)

Vous pouvez ensuite choisir d'installer Anaconda de deux manières différentes:

=== "Installation <bad>Just Me</bad> <bad @DeepSkyBlue #FFF>(**Recommandé**)</bad>"
    Si vous choisissez ce type d'installation : **seul l'utilisateur courant de votre PC pourra utiliser Anaconda**. Cette installation est **plus simple** (car les variables d'environnement seront ajoutées automatiquement), mais **plus limitée**.
    
    * Select Installation Type : Choisir `Just Me (recommended)`

        ![Select Installation Type --> Just Me (recommended) --> Next](./img_just_me/7.png)

    * Conserver le chemin d'installation par défaut proposé : `C:\Users\<tonNomDeUser>\anaconda3` , puis `Next >`

        ![Choose Install Location, ne rien changer, puis Next](./img_just_me/8.png)

    * Advanced Installation Options: 
    
        * :warning: <red>Ajouter/Cocher</red> :warning: `Add Anaconda3 to my PATH environment variable` (malgré le **NOT recommended** en rouge.. no panic)
        * **Ajouter/Cocher (facultatif)** `Clear the package cache upon completion` (supprime les fichiers inutiles après l'installation). Puis cliquer sur `Install`

        ![Advanced Installation Options: laisser tel quel, ou bien 'Clear the package cache upon completion' , puis Install](./img_just_me/9.png)

    * Après la fin, `Installation Complete`: cliquer sur `Next`

        ![Installation Complete](./img_just_me/10.png)

    * Vous devriez obtenir l'écran de fin d'installation suivant: cliquer sur `Next`

        ![Installation Complete](./img_just_me/11.png)

    * Cliquer sur `Finish`

        ![Cliquer sur Finish](./img_just_me/12.png)

    C'est fini :sunglasses::sunglasses: !!

=== "Installation <bad>All Users</bad>"
    Si vous choisissez ce type d'installation : **TOUS les utilisateurs de votre PC pourront utiliser Anaconda**. Cette installation est **plus complexe** (car les variables d'environnement NE seront PAS ajoutées automatiquement, **il FAUDRA les ajouter manuellement**), mais **plus étendue**.

    * Select Installation Type : Choisir `All Users (requires admin privileges)`

        ![Select Installation Type --> All Users (requires admin privileges) --> Next](./img_all_users/7.png)

    * Voulez vous autoriser cette application à apporter des modifications à votre appareil ?  
    (Spoiler :joy:) Réponse : `OUI`

        ![Autoriser l'appli à apporter des modifications](./img_all_users/4.png)

    * Conserver le chemin d'installation par défaut proposé : `C:\ProgramData\anaconda3` , puis `Next >`

        ![Choose Install Location, ne rien changer, puis Next](./img_all_users/8.png)

    * Advanced Installation Options: Laisser tel quel, ou bien ajouter `Clear the package cache upon completion` (supprime les fichiers inutiles après l'installation, facultatif, mais conseillé). Puis cliquer sur `Install`

        ![Advanced Installation Options: laisser tel quel, ou bien 'Clear the package cache upon completion' , puis Install](./img_all_users/9.png)

    * Après la fin, `Installation Complete`: cliquer sur `Next`

        ![Installation Complete](./img_all_users/10.png)

    * Vous devriez obtenir l'écran de fin d'installation suivant: cliquer sur `Next`

        ![Installation Complete](./img_all_users/11.png)

    * Cliquer sur `Finish`

        ![Cliquer sur Finish](./img_all_users/12.png)

    C'est fini :sunglasses::sunglasses: !!

## Configurer vos variables d'environnements

Si vous avez choisi :

=== "Installation <bad>Just Me</bad> <bad @DeepSkyBlue #FFF>(**Recommandé**)</bad>"
    Normalement, si vous avez choisi le mode d'installation `Just Me`, ET que vous avez bien coché la case `Add Anaconda3 to my PATH environment variable` lorsque l'on vous l'a demandé, **vous ne devriez pas avoir besoin de configurer les variables d'environnement**, car elles devraient déjà avoir été **ajoutées/configurées automatiquement**. Vous pouvez vérifier le bon fonctionnement de votre installation en tapant les deux commandes suivantes dans un **Windows Terminal (Powershell)** ( ++ctrl+shift+p++ ) et/ou dans un **Terminal de VScode** ( ++ctrl+j++ ): `python` et/ou `ipython`. Si tout fonctionne bien, vous devriez pouvoir accéder à l'interface en ligne de commande (CLI) de `python` et/ou de `ipython`. Dans ce cas, cela prouve que vos variables d'environnement sont OK. Si par hasard ce n'était pas le cas, voici **comment les configurer manuellement** :

    * Ouvrez une fenêtre Exécuter : ++windows+r++   
    * Tapez à la main `systempropertiesadvanced` puis `Entrée`

        ![](./img_just_me/19.png)

    * Dans la fenêtre qui s'ouvre, cliquez sur le bouton `Variables d'environnement...`

        ![Variables d'Environnement](./img_just_me/20.png)

    * Dans `Variables utilisateur pour <tonNomDeuser>` (en haut), regardez la ligne `Path`, vous devriez avoir des lignes qui ressemblent à celles_ci, **sinon** modifier-les en double-cliquant sur la variable `Path` (ou en cliquant une fois dessus, puis `Modifier..`)

        ![Path](./img_just_me/21.png)

    * Dans la nouvelle fenêtre qui s'ouvre (cf ci-dessous), cliquer sur le bouton `Nouveau` (en haut à droite) (plusieurs fois de suite), et **ajouter chacune des lignes suivantes** (sauf si elles existent déjà...)
        * `C:\User\<tonNomDeuser>\anaconda3`
        * `C:\User\<tonNomDeuser>\anaconda3\Library\mingw-w64\bin`
        * `C:\User\<tonNomDeuser>\anaconda3\Library\usr\bin`
        * `C:\User\<tonNomDeuser>\anaconda3\Library\bin`
        * `C:\User\<tonNomDeuser>\anaconda3\Scripts`

        ![Quelles lignes ajouter](./img_just_me/22.png)

=== "Installation <bad>All Users</bad>"
    * Ouvrez une fenêtre Exécuter : ++windows+r++   
    * Tapez à la main `systempropertiesadvanced` puis `Entrée`

        ![Écuter une commande : `systempropertiesadvanced`](./img_all_users/19.png)

    * Dans la fenêtre qui s'ouvre, cliquez sur le bouton `Variables d'environnement...`

        ![Variables d'Environnement](./img_all_users/20.png)

    * Dans `Variables utilisateur pour <tonNomDeuser>` (en haut), regardez la ligne `Path`, vous devriez avoir des lignes qui ressemblent à celles_ci, **sinon** modifier-les en double-cliquant sur la variable `Path` (ou en cliquant une fois dessus, puis `Modifier..`)

        ![Path](./img_all_users/21.png)

    * Dans la nouvelle fenêtre qui s'ouvre (cf ci-dessous), cliquer sur le bouton `Nouveau` (en haut à droite) (plusieurs fois de suite), et **ajouter chacune des lignes suivantes** (sauf si elles existent déjà...)
        * `C:\ProgramData\anaconda3`
        * `C:\ProgramData\anaconda3\Library\mingw-w64\bin`
        * `C:\ProgramData\anaconda3\Library\usr\bin`
        * `C:\ProgramData\anaconda3\Library\bin`
        * `C:\ProgramData\anaconda3\Scripts`

        ![Quelles lignes ajouter](./img_all_users/22.png)

    * `C:\ProgramData\anaconda3`
    * `C:\Users\<tonNomDeUser>\AppData\Roaming\Python\Python312\Scripts`
    * `C:\ProgramData\anaconda3\Scripts`

* Valider en cliquant sur `OK` (pour la fenêtre `Modifier la variable d'environnement`)
* (Re-) Valider en cliquant sur `OK` (pour la fenêtre `Variables d'environnement`)
* (Re-) Valider en cliquant sur `OK` (pour la fenêtre `Propriétés système`)

## Installer le plugin python de ms-microsoft

Voter installation n'est PAS TOTALEMENT terminée, vous avez encore besoin d'un plugin pour VScode, appelé **Python** développé par **Microsoft** :copyright: qui apporte des outils de développement en Python pour VScode : 

* **IntelliSense** ( **Pylance** ) : une aide à la complétion de code qui inclut plusieurs fonctionnalités. Cf [cet article](https://learn.microsoft.com/fr-fr/visualstudio/ide/using-intellisense?view=vs-2022)
* du **Débuggage** ( **Python Debugguer** )
* Du **Formattage**
* Du **Linting** ( **Python** ) : Le linting est une pratique qui vise à améliorer la qualité de votre code et de ce fait la reprise et la maintenabilité de celui-ci. cf [cet article](https://mindsers.blog/fr/post/linting-good-practices/)
* de la navigation de code dans votre projet
* du **refactoring**
* de l'**exploration de variables**
* des **test explorer**
* etc..

Pour installer le plugin **Python** :

* Cliquez sur l'icône **Extensions** (à gauche) <span style="position:absolute;"><img src="./img_just_me/41.png" style="width: 30px; position: relative; top:50%"></span> <span style="margin-left: 38px;"></span> de VScode
* Dans Rechercher des extensions, Tapez: `python`
* Choisissez l'extension de **Microsoft** suivante :

![Extension Python de Microsoft](./img_just_me/40.png)

* N'oubliez pas de cliquer sur `install` pour installer
* Redémarrer VScode

## Débuggage de l'installation


### Vérifier la bonne installation

Désormais, les deux commandes suivantes devraient être reconnnues par un **Terminal**, que ce soit **Windows Terminal** ( ++ctrl+shift+p++ ) et/ou le Terminal (Powershell par défaut) de **VSCode** ( ++ctrl+j++ ) :

* `python`

    ![](./img_just_me/30.png)

* `ipython`

    ![](./img_just_me/31.png)

### Relancez les logiciels

Si ce n'est pas le cas, commencez par :

* Fermer **VSCode** (s'il était ouvert), puis relancez-le
* **Redémarrer votre ordinateur pour que les variables d'environnement soient prises en compte**

### Mauvais cache de VScode

Au cas où vos commandes `python` et `ipython` fonctionnent correctement dans un **Terminal** **Windows Terminal**, mais PAS dans un Terminal VSCode :

* Désinstaller l'Extension **python** dans VScode
* Fermez, puis relancez VScode,
* puis, réinstallez l'Extension **python** de **Microsoft**
