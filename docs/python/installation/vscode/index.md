# 1NSI: Installation de VScode

## Télécharger et installer VScode

* Page de téléchargement : [https://code.visualstudio.com/download](https://code.visualstudio.com/download)
* Pour windows, cliquer sur le bouton `Download for Windows`

L'installeur téléchargé installera VSCode **seulement pour l'utilisateur courant**.  

![Page de Téléchargement](./img/0.png)

* Lancer l'installeur : double-cliquer sur `VSCodeUserSetup-x64-1.95.2`

    ![Double-cliquer sur l'installeur : `VSCodeUserSetup-x64-1.95.2`](./img/0_5.png)

En résumé, **accepter tous les paramètres par défaut** (ne rien modifier).

* **Accord de Licence**: je comprends et j'accepte les termes du contrat de licence

    ![Accord de Licence: je comprends et j'accepte ...](./img/1.png)

* **Dossier de destination**: Ne pas le modifier, laisser `C:\User\<tonNomDeuser>\AppData\Local\Programs\Microsoft VS Code`

    ![Dossier de destination: ne rien modifier](./img/2.png)

* **Sélection du dossier du menu Démarrer**: Ne pas modifier, laisser `Visual Studio Code`

    ![Sélection du dossier du menu Démarrer](./img/3.png)

* **Tâches supplémentaires**: :warning: <red>Tout ajouter/cocher</red> :warning:

    ![Tâches supplémentaires: tout ajouter/cocher](./img/4.png)

* **Prêt à installer**: Cliquer sur **Installer**

    ![Prêt à installer](./img/5.png)

* **Fin de l'installation**: cliquer sur Terminer

    ![Fin de l'installation: cliquer sur Terminer](./img/6.png)
