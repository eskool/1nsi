# 1NSI: Python - Le Module `math`

Voici quelques fonctions utiles du module `#!python math`

## ceil

```python
>>> from math import ceil
>>> ceil(2.3)
3
>>> ceil(2.7)
3
```

## floor

```python
>>> from math import floor
>>> floor(2.3)
2
>>> floor(2.7)
2
```

## sqrt

```python
>>> from math import sqrt
>>> sqrt(2)
1.4142135623730951
```
