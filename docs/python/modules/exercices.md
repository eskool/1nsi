# 1NSI : Exercices Modules

!!! ex "Module random"
    1°) Grâce au module random, générer un nombre aléatoire entre 1 et 6  
    2°) Écrire une fonction `sortie_cheval()->bool` qui simule le lancé de deux dés 6, càd qui ne reçoit rien en entrée, et qui renvoie un booléen , càd :

    * `True` si les deux "dés 6" ont fait 6
    * `False` sinon