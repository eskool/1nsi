# 1NSI : Exercices Tests Conditionnels

!!! ex "Détecter le signe d'un nombre"
    Créer un script Python qui demande en entrée un nombre (flottant) `x` et qui affiche en sortie le signe de `x` :

    * Si x<0, Alors afficher "Strictement Négatif"
    * Si x=0, Alors afficher "Nul"
    * Si x>0, Alors afficher "Strictement Positif"

!!! ex "Détecter si un nombre entier est pair ou impair"
    Créer un script Python qui demande en entrée un nombre entier `x`, et qui affiche en sortie, si ce nombre est pair, ou pas (impair)

!!! ex "Vérifier si une chaîne contient une sous-chaîne de cractère"
    Créer un script Python qui :
    
    * crée une variable phrase="une phrase qui vous plaît" (on dit "**coder en dur**")
    * demande en entrée une (sous-)chaîne de caractères
    * affiche en sortie, si OUI ou NON la (sous-)chaîne appartient à la phrase initiale.

!!! ex "Résoudre une équation du second degré"
    Créer un algorithme, qui :

    * demande en entrée les $3$ valeurs de $a$, $b$ et $c$ pour l'équation $ax^2+bx+c = 0$
    * On calcule $\Delta = b^2 - 4ac$
    * Si $\Delta > 0$ Alors afficher qu'il existe deux solutions $x_1$ et $x_2$ à l'équation du second degré, qui sont :

        $x_1 = \frac{-b-\sqrt{\Delta}}{2a}$
        $x_2 = \frac{-b+\sqrt{\Delta}}{2a}$

    * Si $\Delta = 0$ Alors afficher qu'il existe une unique solution à l'équation du second degré, qui est :

        $x_0 = \frac{-b}{2a}$

    * Sinon, Si $\Delta < 0$, Alors afficher que l'équation du second degré n'admet pas de solution réelle

