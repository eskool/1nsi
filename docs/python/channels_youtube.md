# 1NSI - Channels YouTube sur Python

Voici quelques channels YouTube pour apprendre le Python de zéro.

* :+1: :+1: [:gb: Python Crash Course, de NetNinja :gb:](https://www.youtube.com/watch?v=wdp7smAtqZI&list=PL4cUxeGkcC9goeb7U1FXFdNszWetCmhfB)

<iframe width="560" height="315" src="https://www.youtube.com/embed/wdp7smAtqZI?si=Kcss_2h9YKqASMIJ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

* [Graven : Apprendre le Python, BASES - Voir TOUTE la Playlist sur YouTube](https://www.youtube.com/playlist?list=PLMS9Cy4Enq5JmIZtKE5OHJCI3jZfpASbR)

<iframe width="560" height="315" src="https://www.youtube.com/embed/psaDHhZ0cPs?si=nE30viDvpDWeLv0X" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

:warning: ATTENTION :warning: l'IDE des vidéos de Graven n'est pas Visual Studio Code (celui que nous utilisons en classe)

* [FormationVidéo - Python - Cours](https://youtu.be/04Unwn9stCM?si=GhRkvNUv4sU2F6WE)

<iframe width="560" height="315" src="https://www.youtube.com/embed/04Unwn9stCM?si=U4FhSDnfj9F9q99v" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
<center>Un Cours très (trop?) complet sur Python, avec une introduction à la POO (programme de Terminale), à Tkinter (fenêtres graphiques), Pygame en fin de formation</center>

* [Comment Coder - Les Bases + Playlist de 6 vidéos](https://youtu.be/BHIRHv94gKQ?si=Q5Rdlw-aHe__bpdF)

<iframe width="560" height="315" src="https://www.youtube.com/embed/BHIRHv94gKQ?si=d7P8orPs4vq14WCC" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

* [Comment Coder - Apprendre Python en 1h, Cours complet pour débutant 2024](https://youtu.be/5EnpNI2iCZA?si=Zz86fGWdsw_9hecH)

<iframe width="560" height="315" src="https://www.youtube.com/embed/5EnpNI2iCZA?si=q_aqzYBBYGXVWVPL" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

* [Dosctring/Thibault Houdon : APPRENDRE PYTHON DE A à Z](https://www.youtube.com/watch?v=LamjAFnybo0)

<iframe width="560" height="315" src="https://www.youtube.com/embed/LamjAFnybo0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

:warning: ATTENTION :warning: renvoie vers une suite de cours payante