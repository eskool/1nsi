# 1NSI : Exercices Dictionnaires

!!! ex "Parcourir un dictionnaire"
    1°) créer un dictionnaire `d` dont les clés sont 10 villes françaises, et les valeurs sont leurs codes postaux  
    2°) Parcourir les clés de `d` avec la méthode `.keys()`  
    3°) Parcourir les valeurs de `d` avec la méthode `.values()`  
    4°) Parcourir les paires (clé:valeur) de `d` avec la méthode `.items()`  

!!! ex "Créer un dictionnaire en compréhension"
    Créer les dictionnaires en compréhension suivants, tels qu'ils soient composés des paires `(clés, valeurs)` suivantes :  
    1°) {0:0, 1:3, 2:6, 3:9, 4:12, ..., 20:60}  
    2°) {1:3, 2:6, 4:12, ..., 20:60}  
    (idem, mais on ne conserve pas les clés qui sont des multiples de $3$)  
    3°) {0:0, 1:1, 2:8, 3:27, 4:64, ..., 25:15625}  
    4°) {1:1, 2:8, 3:27, 4:64, ..., 24:13824}  
    (idem, mais on ne conserve pas les clés qui sont des multiples de $5$)  
    5°) {0:"pair", 1:"impair", 2:"pair", 3:"impair", 4:"pair", ..., 100:"pair"}  

!!! ex "Convertir un dictionnaire en liste"
    Soit `d={"a":"alfortville", "b":"bastia", "c":"châteaubriand", "d":"dunkerque"}`  
    1°) Créer une fonction `dico2listeCles(d:dict)->list` qui accepte en entrée un dictionnaire `d`, et qui renvoie en sortie la liste de toutes ses clés  
    2°) Créer une fonction `dico2listeValeurs(d:dict)->list` qui accepte en entrée un dictionnaire `d`, et qui renvoie en sortie la liste de toutes ses valeurs  
    3°) Créer une fonction `dico2listeItems(d:dict)->list` qui accepte en entrée un dictionnaire `d`, et qui renvoie en sortie la liste des deux listes précédentes : `[[cle1, cle2, ..], [valeur1, valeur2, ...]]`  

!!! ex
    Soit `d` le dictionnaire associant à un nom de commercial d'une société, son chiffre d'affaire réalisé (en centaines d'€):  
    ```python
    d={
        "Dumont": 20,
        "Schwarz": 4,
        "Lopez": 15,
        "Lombardo": 7,
        "Benallal": 16
    }
    ```

    1°) Écrire une fonction qui prend en entrée un tel dictionnaire `d`, et qui renvoie en sortie le nombre total de ventes de la société (en centaines d'€)  
    2°) Écrire une fonction qui prend en entrée un tel dictionnaire `d`, et qui renvoie en sortie le nom du commercial ayant réalisé le plus de ventes. Si plusieurs commerciaux sont ex-aequo sur ce critère, la fonction devra renvoyer la liste `[[nom1, nom2, ..], chiffreAffaire]` suivante:
    
    * où `[nom1, nom2, ..]` désigne la liste des noms de tous les commerciaux ex-aequos, 
    * et `chiffreAffaire` désigne leur chiffre d'affaire commun (un entier)

    3°) Écrire une fonction qui prend un entrée une chaîne de caractères comprenant, sur chaque ligne, trois champs séparés par des caractères `;`  

    * un numéro d'étudiant
    * un nom
    * un prénom

    et qui renvoie un dictionnaire dont les clés sont les numéros d'étudiant, et les valeurs associées sont la chaîne correspondant à la concaténation des prénom et nom (dans cet ordre)

    Exemple de chaîne:
    ```python
    chaine = """3945U;BERNIER;JACQUES
    3758K;DUPOND;LUCIE
    2594L;DURAND;MARC"""
    ```

    4°) Écrire une fonction qui prend en entrée un dictionnaire dont les clés sont des noms d'étudiants, et les valeurs sont des listes de leurs notes (sur 20), et qui renvoie en sortie la liste `[nom, moyenne]` composée de :  

    * le nom de la personne ayant la moyenne la plus élevée 
    * sa moyenne

    En cas d'ex-aequo, renvoyer TOUS les noms des étudiants ex-aequo dans une sous-liste, ainsi que leur moyenne correspondante `[[nom1, nom2, ..], moyenne]`

    Exemple:  
    ```python
    notes = {
      "John": [8,10,13],
      "Laura": [7,12,14],
      "Kevin": [4,8,9],
      "Juliette": [6,15,17]
    }
    ```

!!! ex "Fusion de Dictionnaires"
    Écrire une fonction qui prend en entrée 1, 2, ou 3 dictionnaires, et renvoie en sortie un dictionnaire "fusionné" de ceux reçus en entrée, de la manière suivante :

    * pour chaque clé présente dans au moins un dictionnaire, la valeur associée sela la liste des valeurs associées à cette clé dans tous les dictionnaires passés en argument à la fonction

    Exemple:  
    Pour les dictionnaires suivants :  
    ```python
    d1 = {"a":1, "d":4, "g":7}
    d2 = {"a":1, "b":2, "h":[8]}
    d3 = {"a":2, "c":3, "h":9}
    ```

    La fonction devra renvoyer
    ```python
    {"a": [1,1,2], "d":[4], "g":[7], "b":[2], "h":[[8],9], "c":[3]}
    ```