# 1NSI: Exercices Boucle While

<env>Consigne</env> :warning: Les exercices suivants **DOIVENT** utiliser la boucle **while**... :warning:

!!! ex "Suites de Valeurs :rocket:"
    Écrire un algorithme qui affiche les valeurs suivantes (en orange) dans le Terminal :  
    
    === "Question 1."  
        ```python
        30
        31
        32
        33
        34
        35
        36
        ```
    === "Question 2."
        ```python
        37
        36
        35
        34
        33
        32
        ```
    
    === "Question 3."
        ```python
        34
        36
        38
        40
        42
        44
        46
        ```
    
    === "Question 4."
        ```python
        52
        50
        48
        46
        44
        42
        ```
    
    === "Question 5."
        ```python
        1
        2
        3
        5
        8
        13
        21
        34
        ```
    === "Question 6."
        a°) Tous les inverses d'entiers, strictements supérieurs à 1/10
        ```python
        1/ 1 = 1.0
        1/ 2 = 0.5
        1/ 3 = 0.3333333333333333
        1/ 4 = 0.25
        1/ 5 = 0.2
        1/ 6 = 0.16666666666666666
        1/ 7 = 0.14285714285714285
        1/ 8 = 0.125
        1/ 9 = 0.1111111111111111
        ```
        b°) Généraliser à Tous les inverses d'entiers, strictements supérieurs à 1/1000  
        c°) arrondir les résultats des fractions à $2$ chiffres après la virgule, grâce à la fonction native `#!python round(x, p)` qui arrondit le nombre flottant `x` avec `p` chiffres après la virgule.

!!! ex "Richesse :euro: :euro: :rocket:"
    Anna dispose aujourd'hui de $48\euro$  
    Chaque jour, elle gagne la même somme: $17\euro$

    Écrire un algorithme qui détermine une solution aux problèmes suivants:  

    * Dans combien de jours disposera-t-elle de $10\,000\euro$ ?
    * Exprimer ce délai en nombre de mois ($1$ mois $=30$ jours, pour simplifier) et de jours

!!! ex "Plan d'épargne sur Livret $A$ :rocket:"
    Paul dispose aujourd'hui d'une somme de $1200\euro$, qu'il place sur son livret $A$ à un taux mensuel de $1,25\%$ (intérêts versés le 1er du mois).  
    Chaque mois, il calcule son nouveau solde par rapport à celui du mois précédent, grâce à la formule suivante:  
     <center><enc>$solde_{nouveau} = (1+1,25\%) \times solde_{ancien}$</enc></center>

    Écrire un algorithme qui détermine une solution aux problèmes suivants:

    * Dans combien de mois disposera-t-il de $10\,000\euro$ sur son livret $A$ ?
    * Exprimer ce délai en nombre d'années et de mois

!!! ex "Certifier qu'on mot de passe contient (au moins) un chiffre :rocket::rocket:"
    1. Écrire un algorithme qui demande en entrée un mot de passe, et qui continue de demander d'entrer un mot de passe tant que la réponse de l'utilisateur ne contient pas (au moins) un chiffre.
    2. Modifier l'algorithme précédent pour qu'il demande infiniment le mot de passe , tant que le mot de passe ne vérifie pas les conditions suivantes :

        * au minimum un chiffre
        * au minimum 10 caractères
        * au minimum une majuscule
        * au moins un caractère spécial `*![]{}()!:;,?`
    3. :rocket::rocket::rocket: Modifier l'algorithme précédent de sorte qu'il signale quel est le problème précisément lorsque le mot de passe ne remplit pas TOUS les critères :

        * Si le mot de passe ne contient pas de chiffres, alors afficher dans le Terminal, "Le mot de passe doit contenir un chiffre"
        * Si le mot de passe ne contient pas de Majuscules, alors afficher dans le Terminal, "Le mot de passe doit contenir une majuscule"
        * Si le mot de passe ne contient pas de caractère Spécial, alors afficher dans le Terminal, "Le mot de passe doit contenir un caractère spécial"
        * Si le mot de passe contient moins de 10 caractères, alors afficher dans le Terminal, "Le mot de passe doit contenir au minimum 10 caractères"

    Remarque: Si le mot de passe contient plusieurs problèmes, alors l'affichage dans le Terminal doit citer chaque problème (sur plusieurs lignes)
        

!!! ex "Certifier que $a>b$ :rocket::rocket:"
    Écrire un algorithme qui demande deux nombres $a$ et $b$, et qui certifie que la réponse de l'utilisateur convient bien, càd que la condition $a>b$ est vraie (avant de poursuivre, éventuellement, vers de futures instructions)  
    Plus précisément, cet algorithme doit:
    
    * demander deux nombres entiers $a$ et $b$
    * afficher "impossible" dans le Terminal, tant que l'utilisateur saisit des nombres $a$ et $b$ qui ne conviennent pas, càd que la condition $a>b$ est fausse. Auquel cas, il faudra que l'algorithme redemande d'entrer de nouveau les nombres $a$ et $b$, et ce, tant que l'utilisateur n'aura pas entré des valeurs convenables.
    * afficher "Merci!" dans le Terminal, si jamais la réponse de l'utilisateur convient (càd que la condition $a>b$ est vraie)

!!! ex "Division Euclidienne :rocket::rocket::rocket:"
    On se donne deux nombres entiers positifs $a$ et $b$. On supposera par la suite que le nombre $a$ est supérieur ou égal à $b$ (cf. exercice précédent)
    Écrire un algorithme qui calcule le quotient $q$ et le reste $r$ :

    * **uniquement grâce à des soustractions** pour déterminer le quotient:  
      <env>Rappel</env> Le quotient $q$ est le nombre maximal de fois que l'on peut soustraire l'entier $b$ de l'entier $a$ tout en restant positif ($\ge 0$)
    * On s'autorise à réaliser une multiplication pour déterminer le reste, grâce à la formule:  
    <center><enc>$r=a-q\times b$</enc></center>

!!! ex "Algorithme, dit *Babylonien*, d'approximation d'une Racine Carrée :rocket::rocket::rocket::rocket:"
    L'algorithme suivant, dit *algorithme Babylonien*, détermine une approximation $r$ de $\sqrt n$ (où $n$ est un entier donné, par exemple $n=2$), obtenue par le calcul de plusieurs approximations $r$ successives, le tout en contrôlant le résultat (l'approximation $r$ finale) avec une précision donnée:  

    * On part d'une première approximation (initialisation): $r=1$  
    (cette valeur initiale $r=1$ peut être *très lointaine* de la vraie valeur de $\sqrt n$)
    * On calcule l'approximation suivante de $r$, par l'**affectation** suivante:  
    <center><enc>$r=\dfrac 12 \left( r+\dfrac nr \right)$</enc></center>
    * puis on réitère cette même affectation autant de fois que nécessaire
    * Il n'est pas (du tout) évident que les valeurs successives de $r$ ainsi obtenues s'approchent de plus en plus de la vraie valeur de $\sqrt n$ (les maths, c'est un métier :stuck_out_tongue_winking_eye:), mais nous accepterons ce résultat. Par contre, nous souhaitons contrôler à l'avance la **précision** de notre approximation : Quelle **condition d'arrêt** de la boucle while de cet algorithme proposez-vous pour que la précision de l'approximation finale $r$ de $\sqrt n$ soit (au pire) d'un millième? (on pourra utiliser la fonction valeur absolue `#!python abs()` qui est une fonctionnalité native de la Bibliothèque Standard)

!!! ex "Logarithme *Binaire* (en base 2) :rocket::rocket:"
    1. Écrire une fonction `#!python log2(n:int)->int` qui :  

        * accepte en entrée un entier `n`
        * renvoie en sortie le premier entier `p` tel que $2^p > n$
    Cette valeur $p$ sera notée <enc style="margin-right:0.5em;">$p=\lceil \log_2(n) \rceil +1$</enc> par la suite  
    2. Combien de bits sont-ils nécessaires pour coder un nombre $n$ ?  
    (ici, on attend une formule en fonction de la variable $n$)  
    3. Par exemple, combien de bits précisément sont nécessaires pour coder le nombre $1\,534\,876\,925$ ?

!!! ex "Jeu du nombre cible :rocket::rocket::rocket:"
    1. Créer un algorithme qui :

        * Crée un nombre entier aléatoire entre 1 et 20 (le nombre **cible**)
        * Demande en entrée (grâce à `input`) une **tentative** pour tenter de le deviner, puis qui :
            * Affiche "**Trop grand**"" si votre **tentative** de réponse est strictement supérieure au nombre **cible**
            * Affiche "**Trop petit**"" si votre **tentative** de réponse est strictement inférieure au nombre **cible**
            * Affiche "**Gagné**" si le nombre cible a été trouvé. Et arrête le jeu.

    2. Modifier l'algorithme pour commence par demander entre combien (a) et combien (b) le nombre cible aléatoire doit être créé

    3. Modifier l'algorithme pour que le jeu ne s'arrête jamais, càd qu'après avoir deviné le nombre cible, un nouveau nombre cible est généré, et le joueur doit rejouer.

!!! ex "Suite de Syracuse :rocket::rocket::rocket:"
    La suite de Syracuse est une succession de plusieurs (/une infinité de) valeurs entières, toutes calculées depuis un nombre entier `a` (par exemple entre 1 et 100 pour la première valeur de `a`)

    La méthode qui permet de déterminer le prochain entier `a` lorsqu'on connaît la valeur courante de `a`, est basée sur la logique suivante :

    * Si (la valeur courante de) `a` est un nombre entier pair, alors (la valeur suivante de) `a` est divisé par 2 (par rapport à la valeur précédente)
    * sinon, si (la valeur courante de) `a` est un entier impair, alors la valeur suivante de `a` est `3*a+1`

    **Questions:**

    1. Créer une fonction `syracuse(a:int, n:int)->int` telle que :

        * Elle reçoit **en entrée** un entier `a`, et 
        * qui renvoie **en sortie** la valeur de `a` obtenue après `n` itérations du processus décrit précédemment

    2. Il existe une **conjecture**, dite conjecture de syracuse, qui dit que : "Quel que soit le nombre entier `a` dont on est parti initialement, alors il existe un certain entier `v` (pour `vol`) tel que la valeur de `a` finisse par retomber à 1, après `v` itérations précédentes".

        Créer une fonction `vol(a:int)->int` qui :

        * reçoit en entrée un nombre entier `a`
        * renvoie en sortie le premier nombre entier `v` pour lequel la nouvelle valeur de `a` de la suite de syracuse vaut 1.

!!! ex "Jeu d'Echecs : Une Légende de L'Inde Ancienne... :rocket::rocket:"
    Une légende de l'Inde ancienne raconte que le jeu d'échecs a été inventé par un vieux sage, que son roi voulut remercier en lui affirmant qu'il lui accorderait n'importe quel cadeau en récompense.
    Le vieux sage demanda qu'on lui fournisse simplement un peu de riz pour ses vieux jours et plus précisément un nombre de grains de riz suffisant pour que:
    
        * l'on puisse en déposer 1 seul sur la première case du jeu qu'il venait d'inventer, 
        * deux sur la suivante, 
        * quatre sur la troisième 
        * et ainsi de suite jusqu'à la 64e case.

    1. Écrivez un programme Python qui affiche le nombre de grains à déposer sur chacune des 64 cases du jeu.
    Calculez ce nombre de deux manières :

        * le nombre exact de grains (nombre entier)
        * le nombre de grains en notation scientifique (nombre réel)

    2. Un grain de riz Basmati (très utilisé en Inde) pèse en moyenne **0,022 g/grain**. Comparer le poids de riz reçu par le vieux sage, par rapport à la production mondiale de riz en 2025 : 539,4 Millions de tonnes de riz

!!! ex "Nombre de fois qu'un entier est divisible par un autre entier"
    Considérons par exemple le nombre `61600`.
    On peut le diviser plusieurs fois consécutives par `2`:

    | $n$ | $\%2$ | nombre de divisions par $2$ |
    |:-:|:-:|:-:|
    | $61600$ | $\%2$ | $0$ |
    | $30800$ | $\%2$ | $1$ |
    | $15400$ | $\%2$ | $2$ |
    | $7700$ | $\%2$ | $3$ |
    | $3850$ | $\%2$ | $4$ |
    | $1925$ | $\%2$ | $5$ |
    | $962.5$ | -- STOOP -- | -- |

    D'après ce tableau, le nombre $61600$ peut être divisé $5$ fois par $2$.

    1. Ecrire une fonction `nb_fois_div_par(n:int,d:int)->int` tel que:

    * elle reçoit en entrée le nombre `n` à diviser un certain nombre de fois par `d`
    * elle renvoie en sortie combien de fois le nombre `n` peut être divisé par le nombre `d`

!!! ex "Les sauts de puce aléatoire :rocket::rocket::rocket:"
    Une puce se déplace en une seule dimension, sur un axé orienté, de $-\infty$ à $+\infty$. **Initialement**, elle situe dans une position aléatoire entre $-10$ et $10$
    
    1. La puce effectue des sauts, aléatoirement vers la droite ou vers la gauche, d'une longueur 1. (1 cran vers la droite ou 1 cran vers la gauche). Ecrire un algorithme python qui simule la position de la puce, en affichant sur le Terminal le numéro de sa position, jusqu'à ce que la puce sorte de la zone autorisée (de $-10$ à $10$)

    2. Modifier l'algorithme précédent en ajoutant un compteur du nombre de saut

    3. Modifier l'algorithme précédent pour modéliser la position de la puce (sur un même ligne) avec le caractère "X", précédée d'un certain nom de caractères à gauche. Chaque nouveau saut donne lieu à l'affichage d'une nouvelle ligne où la position de "X" a été recalculée.