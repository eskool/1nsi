# Boucle Tant Que / While

## Syntaxe

```python
compteur = 0
while compteur<5:
  print(compteur)
  compteur += 1
```

**Caractéristiques Principales de la boucle *Tant Que* :**

* On N'entre PAS TOUJOURS OBLIGATOIREMENT dans une boucle *Tant Que*
* L'initialisation du problème doit être faite AVANT la boucle *Tant Que*
* L'incrémentation (/décrémentation) de la boucle *Tant Que* N'est PAS AUTOMATIQUE
* On NE connaît PAS TOUJOURS par avance le nombre d'itérations réalisées par une boucle *Tant Que* :
* En particulier, une boucle *Tant Que* PEUT être infinie

## Les Pièges

### Piège N°1 : ne jamais SORTIR de la boucle

```python
a = 0
b = 0
while a < 10 :
    a = a * b
    print("flood")
    a = a + 1
print("ce texte ne s'écrira jamais")
```

### Piège N°2 : ne jamais ENTRER dans la boucle

```python
a = 0
b = 0
while a > 10 :
    print("ce texte non plus ne s'écrira jamais")
    a = a + 1
print("fini") 
```

## Sortie Définitive d'une Boucle While avec `break`

## Passer à l'itération suivante avec `continue`

## Terminaison
