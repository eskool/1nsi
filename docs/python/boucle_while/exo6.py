def log2(n:int)->int:
  p = 0
  while 2**p<=n:
    p += 1
  return p
reponse = log2(10)
print(f"p={reponse}")